const { NODE_ENV } = process.env;
const DEFAULT_ENVIRONMENT = 'production';

function getValue(property) {
  if (typeof property === 'function') {
    return property();
  }

  return property;
}

function cherryPack(environments) {
  if (environments.hasOwnProperty(NODE_ENV)) {
    return getValue(environments[NODE_ENV]);
  }

  if (environments.hasOwnProperty(DEFAULT_ENVIRONMENT)) {
    return getValue(environments[DEFAULT_ENVIRONMENT]);
  }
}

module.exports = cherryPack;
