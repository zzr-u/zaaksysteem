/**
 * @param {string} modulePath
 * @return {string}
 */
const getModuleBaseName = modulePath =>
  modulePath
    .split('/')[0];

module.exports = getModuleBaseName;
