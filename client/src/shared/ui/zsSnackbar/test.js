import angular from 'angular';
import 'angular-mocks';
import SomaEvents from 'soma-events';
import snackbarServiceModule from './snackbarService';
import zsSnackbarModule from '.';

describe('zsSnackbar', () => {
	let $rootScope;
	let $compile;
	let scope;
	let el;
	let ctrl;
	let snackbarService;

	beforeEach(angular.mock.module(zsSnackbarModule, snackbarServiceModule));

	beforeEach(angular.mock.inject([
		'$rootScope', '$compile', 'snackbarService',
		( ...rest ) => {
			[$rootScope, $compile, snackbarService] = rest;
			scope = $rootScope.$new();
			el = angular.element('<zs-snackbar/>');
			$compile(el)(scope);
			ctrl = el.controller('zsSnackbar');
		}
	]));

	test('should return the snacks from the service', () => {
		expect(ctrl.getSnacks()).toEqual(snackbarService.getSnacks());
	});

	test('should remove a snack when clicked', () => {
		let snack = snackbarService.info('foo');
		let event = new SomaEvents.Event('click');

		ctrl.handleCloseClick(snack, event);

		expect(snackbarService.getSnacks()).not.toContain(snack);
	});

	test('should expand', () => {
		ctrl.expand();

		expect(ctrl.isExpanded()).toBe(true);
	});

	test('should contract', () => {
		ctrl.contract();

		expect(ctrl.isExpanded()).toBe(false);
	});

	test('should not be expanded by default', () => {
		expect(ctrl.isExpanded()).toBe(false);
	});

	test('should call the click handler of an action and remove the snack', () => {
		let action = {
			label: 'foo',
			id: 'foo',
			click: jasmine.createSpy('click')
		};
		let snack = snackbarService.info('foo', {
			actions: [
				action
			]
		});

		ctrl.handleActionClick(snack, action);

		expect(action.click).toHaveBeenCalled();
		expect(snackbarService.getSnacks()).not.toContain(snack);
	});

	test('should not return a style object when expanded', () => {
		let snack = snackbarService.info('foo');

		ctrl.expand();

		expect(ctrl.getSnackStyle(snack, 0)).toBeUndefined();
	});

	test('should return a style object when contracted', () => {
		let snack = snackbarService.info('foo');

		ctrl.contract();

		let style = ctrl.getSnackStyle(snack, 0);

		expect(style).toBeDefined();
		expect('transform' in style).toBe(true);
	});
});
