import angular from 'angular';
import zsModalModule from './..';
import propCheck from './../../../util/propCheck';

export default angular
  .module('createModal', [
    zsModalModule
  ])
  .factory('createModal', [
    '$compile', 'zsModal',
    ($compile, zsModal) => {
      return options => {
        propCheck.throw(
          propCheck.shape({
            scope: propCheck.object,
            controller: propCheck.object.optional,
            template: propCheck.string,
            title: propCheck.string
          }),
          options
        );

        let scope = options.scope.$new();

        scope.$ctrl = options.controller;

        let modal = zsModal({
          title: options.title,
          el: $compile(options.template)(scope)
        });

        let closeModal = () => {
          scope.$destroy();
          modal.close();
        };

        modal.open();

        options.scope.$on('$destroy', closeModal);

        modal.onClose(() => {
          closeModal();

          return false;
        });

        return modal;
      };
    }])
  .name;
