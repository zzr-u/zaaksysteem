import angular from 'angular';
import positioner from './../zsPositionFixed/controller';
import template from './template.html';
import assign from 'lodash/assign';
import defaultsDeep from 'lodash/defaultsDeep';
import capabilitiesModule from './../../util/capabilities';
import pointInTriangle from 'point-in-triangle';
import take from 'lodash/take';
import values from 'lodash/values';
import sortBy from 'lodash/sortBy';
import './zstooltip.scss';

export default
	angular.module('zsTooltip', [
		capabilitiesModule
	])
		.directive('zsTooltip', [ '$rootScope', '$timeout', '$animate', '$document', 'capabilities', ( $rootScope, $timeout, $animate, $document, capabilities ) => {

			const HOLD_DELAY = 500;

			let el = angular.element(template),
				visible = false,
				actuallyVisible = true,
				text,
				textElement = el[0].querySelector('.zs-tooltip-content'),
				tracking;

			let getOpts = ( preferredOptions ) => {

				return defaultsDeep({}, preferredOptions, {
					attachment: 'center bottom',
					target: 'center top',
					offset: { x: 0, y: -2 },
					flip: true,
					sticky: false
				});

			};

			let setVisibility = ( ) => {

				let actual = !!(visible && text);

				if (actual !== actuallyVisible) {
					actuallyVisible = actual;

					if (actual) {
						el.css('display', 'block');
					} else {
						el.css('display', 'none');
					}
				}
			};

			let hide = ( ) => {
				visible = false;
				setVisibility();
			};

			let show = ( ) => {
				visible = true;
				setVisibility();
			};

			let setText = ( txt ) => {
				text = txt;

				if (text) {
					textElement.innerHTML = txt;
				}

				setVisibility();
			};

			let attach = ( element, preferredOptions = { } ) => {

				let positionEngine,
					options = getOpts(preferredOptions);

				positionEngine = positioner($rootScope, el, $document, $animate, assign({ reference: element }, options));

				show();

				positionEngine.enable();

				tracking = element;

				return ( ) => {

					if (tracking === element) {

						positionEngine.disable();

						hide();

						tracking = null;

					}

				};
			};

			hide(el);

			$document.find('body').append(el);

			return {
				restrict: 'A',
				link: ( scope, element, attrs ) => {

					let enabled = false,
						unobserve,
						unattach,
						trackzone;

					// Add aria-label attribute when it's not defined
					if (!element.attr('aria-label')) {
						element.attr('aria-label', attrs.zsTooltip);
					}

					let enable = ( ) => {

						if (!enabled) {

							unobserve = attrs.$observe('zsTooltip', ( ) => {
								setText(attrs.zsTooltip);
							});

							setText(attrs.zsTooltip);

							unattach = attach(element, scope.$eval(attrs.zsTooltipOptions));

							enabled = true;

						}

					};

					let disable = ( ) => {

						if (enabled) {

							unobserve();
							unattach();

							unattach = null;

							enabled = false;
						}

					};

					let onDocumentTouch = ( ) => {
						disable();
						$document.unbind('touchstart', onDocumentTouch);
					};

					let onMouseLeave = ( ) => {

						stopMouseTracking(); // eslint-disable-line

						disable();

					};

					let onMouseMove = ( event ) => {
						
						if (!pointInTriangle([ event.clientX, event.clientY ], trackzone)) {
							disable();
							stopMouseTracking(); //eslint-disable-line
						}

					};

					let onMouseEnter = ( ) => {

						$document.unbind('mousemove', onMouseMove);

					};

					let startMouseTracking = ( ) => {

						let toolTipBounds = el[0].getBoundingClientRect(),
							elementBounds = element[0].getBoundingClientRect(),
							center = [ elementBounds.left + elementBounds.width / 2, elementBounds.top + elementBounds.height / 2 ],
							points = {
								tl: [ toolTipBounds.left, toolTipBounds.top ],
								tr: [ toolTipBounds.right, toolTipBounds.top ],
								bl: [ toolTipBounds.left, toolTipBounds.bottom ],
								br: [ toolTipBounds.right, toolTipBounds.bottom ]
							},
							closest = take(
								sortBy(
									values(points),
									( point ) => {

										let x1 = point[0],
											x2 = center[0],
											y1 = point[1],
											y2 = center[1];

										return Math.sqrt( (x1 - x2 ) * (x1 - x2) + (y1 - y2) * (y1 - y2));
									}
								),
								2
							);

						trackzone = closest.concat([ center ]);

						$document.bind('mousemove', onMouseMove);
						el.bind('mouseenter', onMouseEnter);
						el.bind('mouseleave', onMouseLeave);

					};

					let stopMouseTracking = ( ) => {

						$document.unbind('mousemove', onMouseMove);
						el.unbind('mouseenter', onMouseEnter);
						el.unbind('mouseleave', onMouseLeave);

					};

					if (!capabilities().touch) {
						element.bind('mouseenter', ( ) => {
							enable();
						});

						element.bind('mouseleave', ( ) => {

							let opts = getOpts(scope.$eval(attrs.zsTooltipOptions));

							if (opts.sticky) {
								startMouseTracking();
							} else {
								disable();
							}
						});
					} else {

						element.bind('touchstart', ( ) => {

							let timeoutP;

							let onEarlyRelease = ( ) => {
								
								$timeout.cancel(timeoutP);

								element.unbind('touchend', onEarlyRelease);

							};

							let onRelease = ( event ) => {

								event.stopPropagation();
								event.preventDefault();

								element.unbind('touchend', onRelease);

							};

							timeoutP = $timeout(( ) => {

								enable();

								element.unbind('touchend', onEarlyRelease);

								element.bind('touchend', onRelease);

								$document.bind('touchstart', onDocumentTouch);


							}, HOLD_DELAY, false);

							element.bind('touchend', onEarlyRelease);

						});

					}

					element.bind('focus', enable);
					element.bind('blur', disable);

					scope.$on('$destroy', ( ) => {

						$document.unbind('touchstart', onDocumentTouch);
						
						stopMouseTracking();

						disable();
					});
				}
			};

		}])
		.name;
