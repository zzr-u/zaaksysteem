import angular from 'angular';
import uiViewTransitionModule from './../../../ui/uiViewTransition';
import sortBy from 'lodash/sortBy';
import indexOf from 'lodash/indexOf';
import get from 'lodash/get';
import first from 'lodash/head';

export default
	angular.module('uiViewTransitionDirection', [
		uiViewTransitionModule
	])
		.directive('uiViewTransitionDirection', [ 'uiViewTransitionService', '$rootScope', ( uiViewTransitionService, $rootScope ) => {

			let transitions = [];

			$rootScope.$on('$stateChangeStart', ( event, to, toParams, from, fromParams ) => {
				transitions = [ { to, toParams, from, fromParams } ];
			});

			return {
				restrict: 'A',
				link: ( scope, element, attrs ) => {

					let setDirection = ( ) => {
						let settings = scope.$eval(attrs.uiViewTransitionDirection) || { order: [], orderBy: '' },
							{ to, toParams, from, fromParams } = first(transitions) || {},
							ordered = sortBy([ { route: from, params: fromParams }, { route: to, params: toParams } ], ( route ) => {
								return indexOf(settings.order, get(route.params, settings.orderBy));
							});

						if (ordered[0].params === toParams) {
							element.attr('view-direction', 'before');
						} else {
							element.attr('view-direction', 'after');
						}
					};

					scope.$watch(transitions, setDirection);

					scope.$on('$destroy', $rootScope.$on('$stateChangeStart', setDirection));
						
				}
			};

		}])
		.name;
