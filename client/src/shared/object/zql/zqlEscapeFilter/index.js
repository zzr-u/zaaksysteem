import angular from 'angular';
import filter from './filter';

export default
	angular.module('shared.object.zql.zqlEscapeFilter', [
	])
		.filter('zqlEscape', [ ( ) => {

			return filter;

		}])
		.name;
