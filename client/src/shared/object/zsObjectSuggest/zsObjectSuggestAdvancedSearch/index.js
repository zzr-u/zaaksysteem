import angular from 'angular';
import composedReducerModule from './../../../api/resource/composedReducer';
import resourceModule from './../../../api/resource';
import vormFieldsetModule from './../../../vorm/vormFieldset';
import vormValidatorModule from './../../../vorm/util/vormValidator';
import zsSuggestionSubjectListModule from './zsSuggestionSubjectList';
import zsAdvancedSearchResultListModule from './zsAdvancedSearchResultList';
import snackbarServiceModule from './../../../ui/zsSnackbar/snackbarService';
import zsPaginationModule from './../../../ui/zsPagination';
import convertValidationException from './../../../zs/vorm/convertValidationException';
import sessionServiceModule from '../../../user/sessionService';
import person from './person';
import company from './company';
import casetype from './casetype';
import seamlessImmutable from 'seamless-immutable';
import template from './template.html';
import get from 'lodash/get';
import merge from 'lodash/merge';
import shortid from 'shortid';
import './styles.scss';

export default
	angular.module('zsObjectSuggestAdvancedSearch', [
		composedReducerModule,
		vormFieldsetModule,
		resourceModule,
		zsSuggestionSubjectListModule,
		zsAdvancedSearchResultListModule,
		vormValidatorModule,
		snackbarServiceModule,
		zsPaginationModule,
		sessionServiceModule
	])
		.directive('zsObjectSuggestAdvancedSearch', [
			'$rootScope', '$document', '$http', '$compile', '$interpolate', '$q', 'resource', 'composedReducer', 'vormValidator', 'sessionService', 'snackbarService',
			( $rootScope, $document, $http, $compile, $interpolate, $q, resource, composedReducer, vormValidator, sessionService, snackbarService ) => {

			let moduleResource = resource(
				'/api/v1/sysin/interface/get_by_module_name/stufconfig',
				{ scope: $rootScope }
			);

			return {
				restrict: 'E',
				template,
				scope: {
					type: '&',
					onSelect: '&',
					onClose: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( scope ) {

					let ctrl = this,
						values = seamlessImmutable({}),
						formatted = values.merge({}),
						fetchedResults = null,
						runningRequest,
						viewMode = 'form',
						httpValidation,
						rowsPerPage = 10,
						page = 1,
						loading = false,
						totalRows = NaN,
						userResource = sessionService.createResource(scope),
						controllerReducer =
							composedReducer({ scope }, ctrl.type, moduleResource, userResource)
							.reduce( ( type, modules, user ) => {

								let controller;

								switch (type) {
									case 'natuurlijk_persoon':
									controller = person;
									break;

									case 'bedrijf':
									controller = company;
									break;

									case 'casetype':
									controller = casetype;
									break;
								}
								
								return controller({ modules, user });
							}),
						fieldReducer =
							composedReducer(
								{ scope },
								controllerReducer
							)
								.reduce( ( controller ) => controller.fields),
						validityReducer =
							composedReducer( { scope }, fieldReducer, ( ) => values, ( ) => httpValidation)
								.reduce( ( fields, vals, httpValid ) => {

									return httpValid || vormValidator(fields, vals);

								}),
						resultReducer = composedReducer( { scope }, ( ) => fetchedResults)
							.reduce(( results ) => {

								return (results || seamlessImmutable([])).map(
									result => {
										return result.merge({ id: shortid() });
									}
								);

							}),
						columnReducer = composedReducer( { scope }, controllerReducer)
							.reduce( controller => {

								let columns = seamlessImmutable(controller.columns);

								if ($interpolate.startSymbol() !== '{{') {
									columns = columns.map(
										col => {

											return col.merge({
												template:
													col.template.replace(/{{/g, $interpolate.startSymbol())
														.replace(/}}/g, $interpolate.endSymbol())
											});

										}
									);
								}

								return columns;

							});

					let fetchResults = ( ) => {

						let opts;

						if (runningRequest) {
							runningRequest.resolve();
						}

						runningRequest = $q.defer();

						opts = merge(
							controllerReducer.data().request(formatted),
							{
								timeout: runningRequest.promise,
								params: {
									rows_per_page: rowsPerPage,
									page
								}
							}
						);

						loading = true;

						return $http(opts)
							.then(( data ) => {

								fetchedResults = seamlessImmutable(data.data.result.instance.rows)
									.map(row => {

										return row.reference ?
											row
											: row.merge({ reference: shortid() });

									});

								totalRows = data.data.result.instance.pager.total_rows;

							})
							.catch(( err ) => {

								let msg = 'De resultaten konden niet worden geladen. Neem contact op met uw beheerder voor meer informatie.';

								if (get(err, 'data.result.type') === 'validationexception') {
									msg = 'Niet alle velden zijn correct ingevuld. Corrigeer deze velden en probeer het opnieuw.';
									httpValidation = convertValidationException(err.data.result.instance);
								}

								let errorType = get(err, 'data.result.instance.type');

								if (errorType === 'br/subject/search/remote_failure') {
									msg = 'Er zijn meer dan 10 resultaten gevonden. Verklein uw zoekopdracht en probeer het opnieuw.';
									httpValidation = convertValidationException(err.data.result.instance);
								} else if (errorType === 'sysin/modules/stufnps/search_nps/no_processed_result') {
									msg = 'Uw BRP en/of gegevensmakelaar heeft niet op tijd gereageerd. Neem contact op met uw gegevensbeheerder.';
									httpValidation = convertValidationException(err.data.result.instance);
								} else if (errorType === 'sysin/modules/stufprs/search_nps/no_processed_result') {
									msg = 'Uw BRP en/of gegevensmakelaar heeft niet op tijd gereageerd. Neem contact op met uw gegevensbeheerder.';
									httpValidation = convertValidationException(err.data.result.instance);
								}

								console.error(err);

								snackbarService.error(msg);

								return $q.reject(err);

							})
							.finally(( ) => {

								loading = false;

							});

					};

					ctrl.handleChange = ( name, value ) => {

						let controller = controllerReducer.data();

						values = values.merge( { [name]: value });

						formatted = values;

						if (typeof controller.format === 'function') {
							formatted = controller.format(formatted);
						}

						httpValidation = null;

					};

					ctrl.handleSearchSubmit = ( event ) => {

						fetchResults()
							.then(( ) => {

								viewMode = 'results';

							});

						if (event) {
							event.stopPropagation();
						}
						

					};

					ctrl.handleSelect = ( object ) => {

						let controller = controllerReducer.data(),
							obj = seamlessImmutable(object);

						if (typeof controller.format === 'function') {
							obj = controller.format(obj);
						}

						ctrl.onSelect({
							$object: obj
						});

					};

					ctrl.getValues = ( ) => values;

					ctrl.getFormattedValues = ( ) => formatted;

					ctrl.getFields = fieldReducer.data;

					ctrl.getValidity = validityReducer.data;

					ctrl.getResults = resultReducer.data;

					ctrl.getColumns = columnReducer.data;

					ctrl.getViewMode = ( ) => viewMode;

					ctrl.moveBack = ( ) => {

						if (viewMode === 'form') {
							ctrl.onClose();
						} else {
							viewMode = 'form';
						}
					};

					ctrl.handleBackClick = ctrl.moveBack;

					ctrl.canSubmit = ( ) => !loading && get(validityReducer.data(), 'valid', false);

					ctrl.isSubjectType = ( ) => !!(ctrl.type() === 'natuurlijk_persoon' || ctrl.type() === 'bedrijf');

					ctrl.getNumRows = ( ) => totalRows;

					ctrl.getRowsPerPage = ( ) => rowsPerPage;

					ctrl.getPage = ( ) => page;

					ctrl.hasNext = ( ) => !isNaN(totalRows) && page < Math.ceil(totalRows / rowsPerPage);

					ctrl.hasPrev = ( ) => !isNaN(totalRows) && page > 1;

					ctrl.handlePageChange = ( p ) => {
						page = p;
						fetchResults();
					};

					ctrl.handleLimitChange = ( limit ) => {
						rowsPerPage = limit;
						fetchResults();
					};

					ctrl.isLoading = ( ) => loading;

					let onKeyUp = ( event ) => {

						if (event.keyCode === 27) {

							event.stopPropagation();

							scope.$evalAsync( ( ) => {

								ctrl.moveBack();

							});

						}

					};

					$document[0].addEventListener('keyup', onKeyUp, true );

					scope.$on('$destroy', ( ) => {

						$document[0].removeEventListener('keyup', onKeyUp, true);

					});

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
