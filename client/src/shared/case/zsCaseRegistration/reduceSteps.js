import seamlessImmutable from 'seamless-immutable';
import propCheck from './../../util/propCheck';
import getGroupsFromPhase from './../../../intern/views/case/zsCaseView/zsCasePhaseView/getGroupsFromPhase';
import normalizePhaseName from './../../../intern/views/case/zsCaseView/zsCasePhaseView/normalizePhaseName';
import createConfirmFields from './createConfirmFields';
import createContactFields from './createContactFields';
import createStepFromCaseDocuments from './createStepFromCaseDocuments';
import assign from 'lodash/fp/assign';
import flatten from 'lodash/flatten';
import findIndex from 'lodash/findIndex';
import includes from 'lodash/includes';
import omit from 'lodash/omit';
import find from 'lodash/find';
import findLast from 'lodash/findLast';
import map from 'lodash/map';
import get from 'lodash/get';
import keyBy from 'lodash/keyBy';
import omitBy from 'lodash/omitBy';

export default ( phase, settings, fieldsByName, requestor, fHidden, fDisabled, pauseApplicationData, currentStep, maxStep, documents, $state ) => {

	let props =
		omitBy({
			phase,
			settings,
			fieldsByName,
			requestor,
			fHidden,
			fDisabled,
			pauseApplicationData,
			currentStep,
			maxStep,
			documents,
			$state
		}, ( val ) => val === null || val === undefined);

	propCheck.throw(
		propCheck.shape({
			phase: propCheck.shape({
				fields: propCheck.array
			}),
			settings: propCheck.shape({
				intake_show_contact_info: propCheck.bool,
				allow_take_on_create: propCheck.bool,
				allow_assign_on_create: propCheck.bool,
				allow_subjects_on_create: propCheck.bool,
				show_confidentiality_on_create: propCheck.bool
			}),
			requestor: propCheck.shape({
				instance: propCheck.shape({
					subject_type: propCheck.oneOf([ 'employee', 'person', 'company' ])
				})
			}),
			fHidden: propCheck.object,
			fDisabled: propCheck.object,
			pauseApplicationData: propCheck.shape({
				attribute_names: propCheck.array
			}).optional,
			currentStep: propCheck.string.optional,
			maxStep: propCheck.string.optional,
			documents: propCheck.arrayOf(
					propCheck.shape({
					name: propCheck.string,
					extension: propCheck.string
				})
			).optional,
			$state: propCheck.shape({
				href: propCheck.func
			})
		}),
		props
	);

	let groups = getGroupsFromPhase(seamlessImmutable(phase).asMutable({ deep: true })),
		selectedStep,
		indexOfSelectedStep,
		indexOfMaxStep,
		attributesByName = keyBy(
			flatten(map(groups, 'fields')),
			'magic_string'
		),
		steps;

	// add a URL-safe name property to every group

	steps = groups.map(
		group => {
			return assign(group, { name: normalizePhaseName(group.label) });
		}
	);

	// convert attribute fields to fieldsets w/ form fields

	steps = steps.map(
		group => {
			return omit(
				assign(
					group,
					{
						fieldsets: [
							{
								name: group.name,
								label: group.label,
								description: group.description,
								fields: group.fields
							}
						]
					}
				),
				'fields'
			);
		}
	);

	// add a confirm step w/ all fields included

	steps = steps.concat({
		name: 'bevestig',
		label: 'Afronden',
		fieldsets: flatten(
			steps.map(group => group.fieldsets)
		)
	})
	.map(
		group => {

			return assign(
				group,
				{
					fieldsets:
						group.fieldsets.map(fieldset => {

							return assign(fieldset, {
								fields:
									fieldset.fields
										// map attributes to form fields
										.map(field => fieldsByName[field.magic_string])
										// remove hidden fields
										.filter(field => fHidden[field.name] !== true)
										// disable fields disabled by rules
										.map(field => {
											return assign(
												field,
												{
													disabled: (
														fDisabled[field.name]
													)
												}
											);
										})
							});

						})
				}
			);
		}
	)
		.map(group => {

			let fieldsets = group.fieldsets,
				added = [];

			if (group.name === 'bevestig') {

				// disable fields in confirm step and dont show text_blocks either
				fieldsets = fieldsets.map(
					fieldset => assign(fieldset, {
						fields: fieldset.fields
							.map(field => {
								if (field.type === 'geolatlon') {
									return assign(field, { disabled: true, data: { addressType: 'coordinate', hideCoords: true } } );
								}
								return assign(field, { disabled: true } );
							})
							.filter(field => field.type !== 'text_block')
					})
				);

				// add contact fields if requestor is not an employee and casetype tells us to
				if (requestor.instance.subject_type !== 'employee' && settings.intake_show_contact_info) {
					added = added.concat({
						name: 'contact_information',
						label: 'Contactgegevens',
						description: 'De contactgegevens van de aanvrager. Wijzigingen worden ook in het contactdossier verwerkt.',
						fields: createContactFields()
					});
				}

				// add confirm fields (allocation, related subjects, confidentiality)
				added = added.concat({
					name: 'actions',
					label: 'Zaakacties',
					description: '',
					fields: createConfirmFields(settings)
				});

			}

			// disable fields if disabled by rule (allocation)
			added = added.map(
				fieldset => {
					return assign(fieldset, {
						fields: fieldset.fields.map(
							field => assign(field, { disabled: fDisabled[field.name] })
						)
					});
				}
			);

			fieldsets = fieldsets.concat(added);

			return assign(
				group,
				{
					fieldsets
				}
			);
		});

	// if a document is given, add a document step to the beginning
	if (documents) {

		steps = [
			createStepFromCaseDocuments(
				phase.fields.filter(field => field.type === 'file')
					.map(field => {
						return { name: field.magic_string, label: field.label };
					})
			)
		].map(step => {

			// add an extension to file field
			// too much indents, need some kind of function which takes a path
			// and calls a func with the value at that path,
			// then returns a modified object w/ the returned value

			return assign(step, {
				fieldsets: step.fieldsets.map(
					fieldset => {
						return assign(fieldset, {
							fields: fieldset.fields.map(
								field => {
									return field.name === '$files' ?
										assign(field, {
											data: {
												fields: field.data.fields.map(
													f => {
														return f.name === 'name' ?
															// hardcoded, breaks when we want to support more than one document
															assign(f, { data: { extension: documents[0].extension, isArchivable: !!(get(documents[0], 'filestore_id.is_archivable', 1)) } })
															: f;
													}
												)
											}
										})
										: field;
								}
							)
						});
					}
				)
			});

		}).concat(steps);

	}

	// add URLs to steps
	steps = steps.map(step => {
		return assign(step, {
			url: $state.href('register', { step: step.name })
		});
	});

	// if rule engine tells us the registration should be paused..

	if (pauseApplicationData) {

		// find the last attribute (including system attributes) which triggers the pause
		let lastAttribute =
			findLast(
				flatten(
					map(groups, 'fields')
				),
				field => includes(pauseApplicationData.attribute_names, field.magic_string)
			),
			lastAttrName = get(lastAttribute, 'magic_string');

		steps = steps.map(
			step => {

				let fields = flatten(step.fieldsets.map(fieldset => fieldset.fields)),
					indexOfField = findIndex(fields, field => field.name === lastAttrName),
					group = find(groups, { id: step.id }),
					hasPauseApplicationAttribute;

				// if step is not a casetype fieldset, return step as-is
				if (!group) {
					return step;
				}

				// check if step has an attribute that triggers the pause application rule
				hasPauseApplicationAttribute = !!includes(group.fields, lastAttribute) || !lastAttribute;

				// if so, remove fields after this attribute from fieldset
				// this code assumes there's only one fieldset per step (which is true for casetype fieldsets)

				return hasPauseApplicationAttribute ?
					assign(
						step,
						{
							fieldsets: step.fieldsets.map(fieldset => {
								return assign(
									fieldset,
									{
										fields: fieldset.fields.filter(field => fields.indexOf(field) <= indexOfField)
									}
								);
							}),
							// add the pauseApplicationData for use in a UI component
							paused: pauseApplicationData
						}
					)
					: step;
			}
		);
	}

	// hide system attributes and remove steps without fields
	steps = steps.map(
		step => {

			let fieldsets =
				step.fieldsets.map(
					fieldset => {

						return assign(fieldset, {
							fields: fieldset.fields.filter(field => {

								let isNotSystemAttribute =
									!attributesByName[field.name] || !attributesByName[field.name].is_system;

								return isNotSystemAttribute;
							})
						});
					}
				)
					.filter(fieldset => fieldset.fields.length);

			return assign(
				step,
				{ fieldsets }
			);
		}
	).filter(step => step.fieldsets.length || step.paused);

	// add navigation information to steps

	selectedStep = currentStep || steps[0].name;

	indexOfSelectedStep = steps.map(group => group.name).indexOf(selectedStep);
	indexOfMaxStep = steps.map(group => group.name).indexOf(maxStep);

	steps = steps.map(
		( group, i ) => {

			let active = i === indexOfSelectedStep,
				completed = i < indexOfSelectedStep,
				disabled = i > indexOfMaxStep;

			return assign(group, {
				classes: {
					active,
					completed,
					disabled
				},
				url: !disabled	?
					group.url
					: null
			});
		}
	);

	return steps;

};
