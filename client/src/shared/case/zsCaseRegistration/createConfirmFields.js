import omit from 'lodash/omit';
import includes from 'lodash/includes';
import confidentialityOptions from '../../zs/case/confidentiality';

export default ( settings ) => {

	let types = [];

	if (settings.allow_take_on_create) {
		types = types.concat('me');
	}

	if (settings.allow_assign_on_create) {
		types = includes(types, 'me') ? types.concat('org-unit', 'coworker') : types.concat('me', 'org-unit', 'coworker');
	}

	return [
		{
			name: '$allocation',
			label: 'Toewijzing',
			template: 'allocation',
			data: {
				types
			},
			hide: !types.length
		},
		{
			name: '$related_subjects',
			label: 'Betrokkenen',
			template: 'related-subject',
			hide: !settings.allow_subjects_on_create,
			limit: -1,
			description: 'Het kan voorkomen dat er naast de aanvrager meerdere personen/bedrijven gerelateerd zijn aan deze zaak. Deze relaties kun je hieronder toevoegen.'
		},
		{
			name: '$confidentiality',
			label: 'Vertrouwelijkheid',
			template: 'select',
			data: {
				options: confidentialityOptions.map( field => {
					return { label: field.label, value: field.name };
				})
			},
			hide: !settings.show_confidentiality_on_create
		}
	]
	.filter(field => !field.hide)
	.map(field => omit(field, 'hide'));

};
