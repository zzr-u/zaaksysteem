import angular from 'angular';
import resourceModule from '../../shared/api/resource';
import caseDetailViewModule from './caseDetailView';
import first from 'lodash/first';
import snackbarServiceModule from './../../shared/ui/zsSnackbar/snackbarService';
import auxiliaryRouteModule from './../../shared/util/route/auxiliaryRoute';
import onRouteActivateModule from './../../shared/util/route/onRouteActivate';
import zsModalModule from './../../shared/ui/zsModal';
import viewTitleModule from './../../shared/util/route/viewTitle';
import template from './index.html';
import seamlessImmutable from 'seamless-immutable';
import flattenAttrs from '../shared/flattenAttrs';
import configServiceModule from '../shared/configService';
import sortBy from 'lodash/sortBy';
import filter from 'lodash/filter';
import './styles.scss';

export default {

	moduleName:
		angular.module('Zaaksysteem.mor.caseDetail', [
			resourceModule,
			caseDetailViewModule,
			snackbarServiceModule,
			auxiliaryRouteModule,
			onRouteActivateModule,
			zsModalModule,
			viewTitleModule,
			configServiceModule
		])
		.name,
	config: [
		{
			route: {
				url: '/melding/:caseID/',
				title: [ 'caseItem', ( caseItem ) => {

					return `${caseItem.data().instance.number}: ${caseItem.data().instance.subject_external || caseItem.data().instance.casetype.name}`;

				}],
				resolve: {
					caseItem: [ '$rootScope', '$stateParams', '$q', 'resource', 'snackbarService', ( $rootScope, $stateParams, $q, resource, snackbarService ) => {

						let caseResource = resource(
							{ url: '/api/v1/case', params: { zql: `SELECT {} FROM case WHERE case.number = ${$stateParams.caseID}` } },
							{ scope: $rootScope }
						)
							.reduce( ( requestOptions, data ) => {

								return first( (data || seamlessImmutable([])).map(flattenAttrs) );

							});

						return caseResource
							.asPromise()
							.then( ( ) => caseResource)
							.catch( ( err ) => {

								snackbarService.error('De melding kon niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

								return $q.reject(err);
							});
					}],
					caseType: [ '$rootScope', '$q', 'resource', 'snackbarService', 'caseItem', ( $rootScope, $q, resource, snackbarService, caseItem ) => {

						let caseTypeResource = resource(
							{
								url: `/api/v1/casetype/${caseItem.data().instance.casetype.reference}`,
								params: {
									version: caseItem.data().instance.casetype.instance.version
								}
							},
							{ scope: $rootScope }
						)
							.reduce( ( requestOptions, data ) => {

								return first( (data || seamlessImmutable([])).map(flattenAttrs) );

							});

						return caseTypeResource
							.asPromise()
							.then( ( ) => caseTypeResource)
							.catch( ( err ) => {

								snackbarService.error('Het zaaktype kon niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

								return $q.reject(err);
							});
					}],
					documents: [ '$rootScope', '$stateParams', '$q', 'resource', 'snackbarService', 'caseItem', ( $rootScope, $stateParams, $q, resource, snackbarService, caseItem ) => {

						return resource(
							{ url: `/api/case/${caseItem.data().instance.number}/directory/tree` },
							{ scope: $rootScope }
						)
							.asPromise()
							.then( ( data ) =>
									sortBy(
										filter(
											first(data).files,
											( file ) => file.deleted_by === null
										),
										( file ) => file.name.toLowerCase()
									)
							)
							.catch( ( err ) => {

								snackbarService.error('De meldingsdocumenten konden niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

								return $q.reject(err);

							});

					}]
				},
				auxiliary: true,
				onActivate: [ '$state', '$timeout', '$rootScope', '$window', '$document', '$compile', 'viewTitle', '$stateParams', 'zsModal', 'caseItem', 'documents', 'caseType', ( $state, $timeout, $rootScope, $window, $document, $compile, viewTitle, $stateParams, zsModal, caseItem, documents, caseType ) => {

					let scrollEl = angular.element($document[0].querySelector('.body-scroll-container'));

					scrollEl.css({ overflow: 'hidden' });

					let openModal = ( ) => {

						let modal,
							unregister,
							scope = $rootScope.$new(true);

						scope.caseItem = caseItem;
						scope.caseType = caseType;
						scope.documents = documents;

						modal = zsModal({
							el: $compile(angular.element(template))(scope),
							classes: 'detail-modal'
						});

						modal.open();

						modal.onClose( ( ) => {
							$window.history.back();
							return true;
						});

						unregister = $rootScope.$on('$stateChangeStart', ( ) => {

							$window.requestAnimationFrame( ( ) => {

								$rootScope.$evalAsync(( ) => {

									if ($state.current.name === 'caseList') {

										modal.close()
											.then(( ) => {

												scope.$destroy();

												scrollEl.css({
													overflow: '',
													height: ''
												});

											});

										unregister();

									}

								});

							});
							
						});

					};


					$window.requestAnimationFrame( ( ) => {
						$rootScope.$evalAsync(openModal);
					});

				}]

			},
			state: 'caseDetail'
		}
	]
};
