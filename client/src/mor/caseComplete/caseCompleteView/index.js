import angular from 'angular';
import template from './template.html';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import angularUiRouterModule from 'angular-ui-router';
import configServiceModule from '../../shared/configService';
import auxiliaryRouteModule from '../../../shared/util/route/auxiliaryRoute';
import sessionServiceModule from '../../../shared/user/sessionService';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import appServiceModule from '../../shared/appService';
import find from 'lodash/find';
import zsMapModule from '../../../shared/ui/zsMap';
import assign from 'lodash/assign';
import first from 'lodash/first';
import last from 'lodash/last';
import omit from 'lodash/omit';
import filter from 'lodash/filter';
import get from 'lodash/get';
import every from 'lodash/every';
import isArray from 'lodash/isArray';
import capitalize from 'lodash/capitalize';
import zsModalModule from '../../../shared/ui/zsModal';
import vormFieldsetModule from '../../../shared/vorm/vormFieldset';
import radioModule from '../../../shared/vorm/types/radio';
import textareaModule from '../../../shared/vorm/types/textarea';
import checkboxListModule from '../../../shared/vorm/types/checkboxList';
import seamlessImmutable from 'seamless-immutable';
import caseAttrTemplateCompilerModule from '../../../shared/case/caseAttrTemplateCompiler';
import vormValidatorModule from '../../../shared/vorm/util/vormValidator';
import attrToVorm from '../../../shared/zs/vorm/attrToVorm';
import actionsModule from './actions';
import './styles.scss';

export default
		angular.module('Zaaksysteem.mor.caseCompleteView', [
			composedReducerModule,
			configServiceModule,
			angularUiRouterModule,
			auxiliaryRouteModule,
			sessionServiceModule,
			snackbarServiceModule,
			appServiceModule,
			zsMapModule,
			zsModalModule,
			vormFieldsetModule,
			radioModule,
			textareaModule,
			checkboxListModule,
			caseAttrTemplateCompilerModule,
			vormValidatorModule,
			actionsModule
		])
		.directive('caseCompleteView', [ '$q', '$sce', '$window', '$document', '$http', '$compile', '$timeout', '$state', 'dateFilter', 'composedReducer', 'configService', 'auxiliaryRouteService', 'sessionService', 'snackbarService', 'zsModal', 'caseAttrTemplateCompiler', 'vormValidator', 'appService', ( $q, $sce, $window, $document, $http, $compile, $timeout, $state, dateFilter, composedReducer, configService, auxiliaryRouteService, sessionService, snackbarService, zsModal, caseAttrTemplateCompiler, vormValidator, appService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					case: '&',
					caseType: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						caseSpecificAttributesReducer,
						caseAttributesReducer,
						fieldReducer,
						values = seamlessImmutable({}),
						defaultsReducer,
						valueReducer,
						finalFieldReducer,
						validityReducer,
						requiredFieldReducer;


					caseSpecificAttributesReducer = composedReducer( { scope: $scope }, configService.getCaseSpecificAttributes, ctrl.case() )
						.reduce( ( caseTypes, caseItem ) => {

							return get(
									find(caseTypes, ( caseType ) => {
										return caseType.casetype_reference === caseItem.instance.casetype.reference;
									}),
									'completion_attributes');

						});

					requiredFieldReducer = composedReducer( { scope: $scope }, ctrl.caseType(), caseSpecificAttributesReducer )
						.reduce( ( caseType, attributes ) => {

							let requiredFields =
											filter(
												get(
													last(caseType.instance.phases),
													'fields'
												),
												( field ) => field.required === true
											);

							return every(requiredFields, ( field ) => {
								return find(attributes, ( el ) => el.object.column_name.replace('attribute.', '') === field.magic_string);
							});

						});

					ctrl.getCorrectConfig = requiredFieldReducer.data;

					ctrl.getSupportLink = ( ) => configService.getSupportLink();

					caseAttributesReducer = composedReducer( { scope: $scope }, ctrl.caseType() )
						.reduce( ( caseType ) => {
							return last(caseType.instance.phases).fields;
						});

					fieldReducer = composedReducer({ scope: $scope }, caseSpecificAttributesReducer, caseAttributesReducer, ctrl.caseType())
						.reduce(( fields, fieldConfig, caseType ) => {

							let resultOption = [{
									name: 'result',
									label: 'Beoordeling van de melding *',
									template: 'radio',
									data: {
										options: caseType.instance.results.map( ( result ) => {
											return {
												label: result.label || capitalize(result.type),
												value: result.resultaat_id
											};
										})
									},
									required: true
								}];

							let coupledFields =
								fields && fields.length ?
									fields.map( ( field ) => {

										let requiredField = find(fieldConfig, ( caseTypeField ) => caseTypeField.magic_string === field.object.column_name.replace('attribute.', '')),
											formField,
											label,
											required = get(requiredField, 'required') || false;

										if (required) {
											label = `${(field.public_label || field.label)} *`;
										} else {
											label = field.public_label || field.label;
										}

										formField = attrToVorm({
											id: field.id,
											magic_string: field.object.column_name.replace('attribute.', ''),
											label,
											description: '',
											required: get(requiredField, 'required') || false,
											type: field.object.value_type,
											values: field.object.values
										});

										if (formField.data && formField.data.options) {
											formField = seamlessImmutable(formField)
												.merge(
												{
													data: {
														options: formField.data.options.map(option => {
															return option.active === 1 ?
																{
																	label: option.value,
																	value: option.value,
																	active: true,
																	sort_order: option.sort_order
																} : option;
														}).filter(
															option => {
																return option.active;
															}
														)
													}
												},
												{
													deep: true
												}
											).asMutable({ deep: true });
										}

										return formField;

									})
									: null;

							return coupledFields ? coupledFields.concat(resultOption) : resultOption;

						});

					defaultsReducer = composedReducer( { scope: $scope }, ctrl.case(), caseAttributesReducer, fieldReducer)
						.reduce(( caseObj, fields, formFields ) => {

							let valuesObj = seamlessImmutable({});

							let formatValue = ( field, source ) => {
								return field.formatters.reduce(( current, reducer ) => {
									return reducer(current);
								}, source);
							};

							formFields.forEach( ( field ) => {

								let attribute = field.name,
									attributeValue = caseObj.instance.attributes[attribute],
									formattedValue = field.hasOwnProperty('formatters') && field.formatters.length ? formatValue(field, attributeValue) : attributeValue;

								valuesObj = valuesObj.merge(
									{ [attribute]: formattedValue }
								);

							});

							return valuesObj;

						});


					valueReducer = composedReducer( { scope: $scope }, defaultsReducer, ( ) => values, fieldReducer)
						.reduce(( defaults, userValues ) => {

							return defaults.merge(userValues);

						});

					finalFieldReducer = composedReducer({ scope: $scope }, fieldReducer, valueReducer)
						.reduce(( formFields, userValues ) => {

							return formFields.map(
								field => {

									let val = isArray(userValues[field.name]) ? first(userValues[field.name]) : userValues[field.name];

									if (field.name === 'result') {
										val = parseInt(val, 10);
									}

									let options =
										field.hasOwnProperty('data') && field.data.hasOwnProperty('options') && field.data.options.length ?
										field.data.options.map( ( option ) => {

											return {
												label: option.label,
												value: option.value,
												classes: {
													active: val === option.value
												}
											};
										})
										: field.data;

									let newData = {
										data: {
											options
										}
									};

									return seamlessImmutable(field).merge(newData);

								}
							);

						});

					validityReducer = composedReducer( { scope: $scope }, finalFieldReducer, valueReducer)
						.reduce( ( fields, userValues ) => {

							return vormValidator(fields, userValues);

						});

					ctrl.getCompletionFields = finalFieldReducer.data;

					ctrl.handleFieldChange = ( name, value ) => {

						values = values.merge({ [name]: value });

					};

					ctrl.getValues = valueReducer.data;

					ctrl.isValid = ( ) => validityReducer.data().valid;

					ctrl.getCompiler = ( ) => caseAttrTemplateCompiler;

					ctrl.handleComplete = ( ) => {

						let parsedValues =
							finalFieldReducer.data().map( field => {

							let val = values[field.name],
								parseFunction = first(field.parsers),
								parsedValue;

							if (parseFunction) {
								parsedValue = parseFunction(val);
							} else {
								parsedValue = val;
							}

							if (!isArray(parsedValue)) {
								parsedValue = [ parsedValue ];
							}

							return { [field.name]: parsedValue };

						});

						let parsedValuesObj = assign({}, ...parsedValues),
							caseRef = ctrl.case().data().reference,
							updateValues = { values: omit(parsedValuesObj, ['result']) },
							result = { result_id: isArray(values.result) ? first(values.result) : values.result };

						return ctrl.case().mutate('MOR_UPDATE_CASE', {
							caseRef,
							updateValues
						})
						.asPromise()
						.finally( ( ) => {

							return ctrl.case().mutate('MOR_TRANSITION_CASE', {
								caseRef,
								result
							})
							.asPromise()
							.then( ( ) => {

								appService.dispatch('reload_case_groups');
								$state.go('^');

							});

						});

					};

					ctrl.goBack = ( ) => {
						$window.history.back();
					};

				}],
				controllerAs: 'caseCompleteView'

			};
		}
		])
		.name;
