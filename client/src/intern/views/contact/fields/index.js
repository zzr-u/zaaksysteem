import company from './company';
import person from './person';
import employee from './employee';

export default {
	company,
	person,
	employee
};
