import angular from 'angular';
import template from './template.html';
import composedReducerModule from './../../../../../shared/api/resource/composedReducer';
import vormInvokeModule from './../../../../../shared/vorm/vormInvoke';
import zsTooltipModule from './../../../../../shared/ui/zsTooltip';
import zsModalModule from './../../../../../shared/ui/zsModal';
import confidentialityOptions from './../../../../../shared/zs/case/confidentiality';
import auxiliaryRouteModule from './../../../../../shared/util/route/auxiliaryRoute';
import get from 'lodash/get';
import shortid from 'shortid';
import assign from 'lodash/assign';
import capitalize from 'lodash/capitalize';
import mapValues from 'lodash/mapValues';
import first from 'lodash/head';
import './styles.scss';

export default
	angular.module('zsCaseSummary', [
		composedReducerModule,
		vormInvokeModule,
		zsTooltipModule,
		zsModalModule,
		auxiliaryRouteModule
	])
		.directive('zsCaseSummary', [ '$state', '$window', '$compile', 'vormInvoke', 'composedReducer', 'dateFilter', 'zsModal', 'auxiliaryRouteService', ( $state, $window, $compile, vormInvoke, composedReducer, dateFilter, zsModal, auxiliaryRouteService ) => {

			let dateIf = ( date ) => {
				
				if (date) {
					return dateFilter(date, 'dd-MM-yyyy');
				}

				return '-';
			};

			let getCaseStatus = ( caseObj ) => caseObj.instance.status;

			const getPaymentStatusIcon = ( paymentStatus ) => {
				let icon = null;

				switch (get(paymentStatus, 'original')) {
					case 'offline':
						icon = 'timer-sand';
						break;
					case 'pending':
					case 'failed':
						icon = 'close-circle';
						break;
					case 'success':
						icon = 'check-circle';
						break;
				}

				return icon;
			};

			return {
				restrict: 'E',
				template,
				scope: {
					case: '&',
					casetype: '&',
					canChangeConfidentiality: '&',
					onConfidentialityChange: '&',
					onSelfAssign: '&',
					isCollapsed: '&',
					canAssign: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						linkReducer,
						confidentialityReducer,
						confOptionsReducer,
						statuses = {
							resolved: 'Afgehandeld',
							stalled: 'Opgeschort',
							new: 'Nieuw',
							open: 'In behandeling'
						};

					let openConfidentialityModal = ( ) => {

						let scope = $scope.$new(),
							modal;

						scope.handleConfidentialityClick = ( confidentiality ) => {
							ctrl.onConfidentialityChange( { $confidentiality: confidentiality });
							modal.close();
						};

						modal = zsModal({
							title: 'Vertrouwelijkheid wijzigen',
							el: $compile(
								`
								<div class="confidentiality-warning">
									<zs-icon icon-type="alert"></zs-icon>
									Let op! Als u het vertrouwelijkheidsniveau wijzigt naar een niveau waarop u geen rechten heeft op de zaak, kunt u dit niet terugzetten.
								</div>
								<ul class="modal-confidentiality">

									<li ng-repeat="option in vm.getConfidentialityOptions()">
										<label>
											<input type="radio" ng-checked="vm.getConfidentiality()===option.name" ng-click="handleConfidentialityClick(option.name)">
											{{::option.label}}
										</label>
									</li>

								</ul>`
							)(scope)
						});

						modal.open();

					};

					linkReducer = composedReducer( { scope: $scope }, ctrl.case, ctrl.casetype, ctrl.canChangeConfidentiality, ctrl.isCollapsed, ctrl.canAssign, ( ) => auxiliaryRouteService.getCurrentBase())
						.reduce( ( caseObj, casetype, canChangeConfidentiality, isCollapsed, canAssign, currentState ) => {

							const assignee = caseObj.instance.assignee;
							const status = getCaseStatus(caseObj);
							const paymentStatus = get(caseObj.instance.payment_status, 'original');

							let links = [
								{
									icon: 'briefcase-check',
									name: 'result',
									title: 'Resultaat',
									label: get(caseObj, 'instance.result_description') || capitalize(caseObj.instance.result),
									visible: caseObj.instance.status === 'resolved'
								},
								{
									name: 'status',
									title: 'Status',
									label: statuses[status],
									icon: ` icon-status-${status}`,
									style: mapValues(statuses, ( value, key ) => key === status)
								},
								{
									name: 'requestor',
									title: 'Aanvrager',
									label: get(caseObj, 'instance.requestor.instance.first_names') ?
											`${capitalize(first(get(caseObj, 'instance.requestor.instance.first_names')))}. ${get(caseObj, 'instance.requestor.instance.surname')}`
											: `${get(caseObj, 'instance.requestor.instance.name')}`,
									url: caseObj.instance.requestor ?
											`/betrokkene/${caseObj.instance.requestor.instance.id.match(/(\d+)$/)[0]}?gm=1&type=${caseObj.instance.requestor.instance.subject_type}`
											: null,
									openExternal: true,
									icon: 'account',
									type: 'link'
								},
								{
									name: 'recipient',
									title: 'Ontvanger',
									label: get(caseObj, 'instance.recipient.instance.first_names') ?
											`${capitalize(first(get(caseObj, 'instance.recipient.instance.first_names')))}. ${get(caseObj, 'instance.recipient.instance.surname')}`
											: `${get(caseObj, 'instance.recipient.instance.name')}`,
									url: caseObj.instance.recipient ?
											`/betrokkene/${caseObj.instance.recipient.instance.id.match(/(\d+)$/)[0]}?gm=1&type=${caseObj.instance.recipient.instance.subject_type}`
											: null,
									openExternal: true,
									icon: 'account-check',
									visible: !!caseObj.instance.recipient,
									type: 'link'
								},
								{
									name: 'assignee',
									title: 'Behandelaar',
									type: !assignee ? 'button' : 'link',
									label: !assignee ?
											(canAssign ?
												'In behandeling nemen'
												: 'Geen behandelaar')
											: `${capitalize(first(get(caseObj, 'instance.assignee.instance.first_names')))}. ${get(caseObj, 'instance.assignee.instance.last_name')}`,
									url: caseObj.instance.assignee ?
											`/betrokkene/${caseObj.instance.assignee.instance.id}?gm=1&type=medewerker`
											: null,
									openExternal: true,
									icon: 'account-star-variant',
									click: ctrl.onSelfAssign,
									disabled: !!assignee || !canAssign,
									style: {
										assigned: !!assignee,
										unassigned: !assignee
									}
								},
								{
									name: 'department',
									title: 'Afdeling',
									label: get(caseObj, 'instance.casetype.instance.department'),
									url: null,
									icon: 'source-fork'
								},
								{
									name: 'registration_date',
									title: 'Registratiedatum',
									label: dateIf(get(caseObj, 'instance.date_of_registration')),
									url: null,
									icon: 'calendar-plus'
								},
								{
									name: 'target_date',
									title: 'Streefafhandeldatum',
									label: dateIf(get(caseObj, 'instance.date_target')),
									url: null,
									icon: 'calendar-check',
									style:
										(new Date(get(caseObj, 'instance.date_target', 0)).getTime() < new Date().getTime()) ?
										{ 'passed-due-date': true }
										: null
								},
								{
									name: 'date_of_completion',
									title: 'Afhandeldatum',
									label: dateIf(get(caseObj, 'instance.date_of_completion')),
									icon: 'calendar-check',
									visible: caseObj.instance.status === 'resolved'
								},
								{
									name: 'case_location',
									url: $state.href('case.location', null, { inherit: true }),
									openExternal: false,
									title: 'Zaakadres',
									icon: 'map-marker',
									label: get(caseObj.instance.location, 'instance.nummeraanduiding.human_identifier')
										|| get(caseObj.instance.location, 'instance.openbareruimte.human_identifier'),
									visible: !!caseObj.instance.location
								},
								{
									name: 'confidentiality',
									type: 'button',
									title: 'Vertrouwelijkheid',
									label: caseObj.instance.confidentiality.mapped,
									url: null,
									icon: caseObj.instance.confidentiality.original === 'confidential' ?
										'eye-off'
										: 'eye',
									click: openConfidentialityModal,
									disabled: !canChangeConfidentiality,
									style: {
										confidential: caseObj.instance.confidentiality.original === 'confidential'
									}
								},
								{
									name: 'payment_amount',
									title: `Betaalstatus: ${get(caseObj.instance.payment_status, 'mapped', 'Onbekend')}`,
									label: caseObj.instance.price,
									icon: 'currency-eur',
									secondaryIcon: getPaymentStatusIcon(caseObj.instance.payment_status),
									visible: (!!caseObj.instance.price && !!caseObj.instance.payment_status),
									style: {
										'payment-status-failed':  paymentStatus === 'failed',
										'payment-status-offline': paymentStatus === 'offline',
										'payment-status-pending': paymentStatus === 'pending',
										'payment-status-success': paymentStatus === 'success'
									}
								},
								{
									name: 'about',
									title: 'Meer informatie',
									label: 'Meer informatie',
									url: $state.href(auxiliaryRouteService.append(currentState, 'case.info'), null, { inherit: true }),
									openExternal: false,
									icon: 'dots-horizontal'
								}
							];

							links =
								links.filter(link => link.visible === undefined || link.visible)
									.map(
										link => {
											return assign({
												id: shortid(),
												tooltip: isCollapsed ?
													`${link.title}: ${link.label}`
													: link.title
											}, link);
										}
									);

							return links;

						});

					confidentialityReducer = composedReducer( { scope: $scope }, ctrl.case)
						.reduce( ( caseObj ) => get(caseObj, 'instance.confidentiality'));

					confOptionsReducer = composedReducer( { scope: $scope }, confidentialityOptions, confidentialityReducer)
						.reduce( ( options, confidentiality ) => {

							return options.map(( option ) => {
								return assign(
									{},
									option,
									{
										classes: {
											selected: option.name === confidentiality.original
										},
										click: ( ) => {
											ctrl.onConfidentialityChange( { $confidentiality: option.name });
										}
									}
								);
							});

						});

					ctrl.getLinks = linkReducer.data;

					ctrl.getConfidentialityOptions = confOptionsReducer.data;

					ctrl.getConfidentiality = ( ) => get(confidentialityReducer.data(), 'original');
					ctrl.getConfidentialityLabel = ( ) => get(confidentialityReducer.data(), 'mapped');

					ctrl.openInNewWindow = ( link, event ) => {
						// Dirty way to open a window but $window.open(url, target, 'noopener')
						// opens a new window instead of a new tab on Chrome
						let linkElement = document.createElement('a');
						linkElement.target = '_blank';
						linkElement.href = link;
						linkElement.rel = 'noopener';
						document.body.appendChild(linkElement);
						linkElement.click();
						linkElement.parentNode.removeChild(linkElement);

						event.preventDefault();
						event.stopPropagation();
					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
