import angular from 'angular';
import composedReducerModule from './../../../../../../shared/api/resource/composedReducer';
import rwdServiceModule from './../../../../../../shared/util/rwdService';
import get from 'lodash/get';
import shortid from 'shortid';
import findIndex from 'lodash/findIndex';
import find from 'lodash/find';
import first from 'lodash/head';
import last from 'lodash/last';
import sortBy from 'lodash/sortBy';
import zsIconModule from './../../../../../../shared/ui/zsIcon';
import template from './template.html';
import normalizePhaseName from './../normalizePhaseName';
import './styles.scss';

export default
	angular.module('zsCasePhaseNav', [
		composedReducerModule,
		rwdServiceModule,
		zsIconModule
	])
		.directive('zsCasePhaseNav', [ '$state', 'composedReducer', 'rwdService', ( $state, composedReducer, rwdService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					phases: '&',
					activePhase: '&',
					selectedPhase: '&',
					pendingChanges: '&',
					isPhaseValid: '&',
					onPhaseClick: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						phaseReducer;

					phaseReducer = composedReducer({ scope: $scope }, ctrl.phases, ctrl.activePhase, ctrl.selectedPhase, ctrl.isPhaseValid, rwdService.getActiveViews, ctrl.pendingChanges)
						.reduce( ( phases, activePhase, selectedPhase, isPhaseValid, activeViews, pendingChanges ) => {

							let left = [],
								right = [],
								visible = [],
								phaseButtons,
								indexActive = findIndex(phases, ( phase ) => {
									return normalizePhaseName(phase.name) === activePhase;
								}),
								indexSelected = findIndex(phases, ( phase ) => {
									return normalizePhaseName(phase.name) === selectedPhase;
								}),
								views = [ 'xl-only', 'large-and-up', 'medium-small', 'medium-and-up', 'small-and-down' ],
								numVisible,
								view;

							view = find(
								views,
								v => rwdService.isActive(v)
							);

							numVisible = Math.min(phases.length, [ 9, 7, 5, 3, 1 ][views.indexOf(view)]);

							let at = indexSelected,
								diff = 0,
								indexes = [];

							while (indexes.length < numVisible) {
								
								if (at >= 0 && at < phases.length) {
									indexes.push(at);
								}

								if (Math.abs(diff) === diff) {
									diff += 1;
								} else {
									diff -= 1;
								}

								diff *= -1;

								at += diff;

							}

							indexes = sortBy(indexes);

							let start = first(indexes),
								end = last(indexes);

							phaseButtons = (phases || []).map( ( phase, index ) => {

								let phaseName = normalizePhaseName(phase.name),
									active = phaseName === activePhase,
									resolved = index < indexActive || indexActive === -1,
									icon = '';

								if (resolved) {
									icon = 'check';
								} else if (active) {
									icon = 'play';
								}

								return {
									id: shortid(),
									name: phase.name,
									label: phase.name,
									type: 'link',
									link: $state.href('case.phase', { phaseName }),
									selected: phaseName === selectedPhase,
									resolved,
									active,
									iconClass: icon,
									count:
										phase.fields.filter(( field ) => {
											return !!pendingChanges[field.magic_string];
										}).length
								};

							});

							left = phaseButtons.slice(0, start);

							visible = phaseButtons.slice(start, end + 1);

							right = phaseButtons.slice(end + 1);

							return {
								left,
								right,
								visible
							};

						});

					ctrl.getDroppedPhasesLeft = ( ) => get(phaseReducer.data(), 'left');

					ctrl.getVisiblePhases = ( ) => get(phaseReducer.data(), 'visible');
					
					ctrl.getDroppedPhasesRight = ( ) => get(phaseReducer.data(), 'right');

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
