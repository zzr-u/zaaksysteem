import angular from 'angular';
import composedReducerModule from './../../../../../../../shared/api/resource/composedReducer';
import controller from './CaseCheckListController';
import template from './template.html';

export default angular
	.module('zsCaseCheckList', [
		composedReducerModule
	])
	.component('zsCaseCheckList', {
		bindings: {
			items: '&',
			checklistLoading: '&',
			onItemToggle: '&',
			onItemRemove: '&',
			onItemAdd: '&',
			disabled: '&'
		},
		controller,
		template
	})
	.name;
