import angular from 'angular';
import angularUiRouter from 'angular-ui-router';
import caseActions from './../../../../shared/case/caseActions';
import composedReducerModule from './../../../../shared/api/resource/composedReducer';
import auxiliaryRouteModule from './../../../../shared/util/route/auxiliaryRoute';
import zsConfirmModule from './../../../../shared/ui/zsConfirm';
import assign from 'lodash/assign';
import get from 'lodash/get';
import partition from 'lodash/partition';
import template from './template.html';
import './../../../../shared/styles/_contextual-settings.scss';
import './styles.scss';

export default
	angular.module('zsCaseSettings', [
		composedReducerModule,
		angularUiRouter,
		auxiliaryRouteModule,
		zsConfirmModule
	])
		.directive('zsCaseSettings', [ '$state', 'zsConfirm', 'auxiliaryRouteService', 'composedReducer', ( $state, zsConfirm, auxiliaryRouteService, composedReducer ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					caseResource: '&',
					casetypeResource: '&',
					user: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						expanded,
						optionReducer = composedReducer({ scope }, ctrl.caseResource(), ctrl.casetypeResource(), ( ) => auxiliaryRouteService.getCurrentBase(), ( ) => expanded, ctrl.user)
							.reduce( ( caseObj, casetype, currentState, isExpanded, user ) => {

								let actions = caseActions({ caseObj, casetype, $state, user, acls: [] }),
									menuOptions;

								menuOptions = actions.map(
									( action, index, arr ) => {

										let state = auxiliaryRouteService.append(currentState, 'admin'),
											type = action.type || 'link',
											button = {
												name: action.name,
												label: action.label,
												type,
												context: action.context
											};

										if (type === 'confirm') {
											button = assign(button, {
												type: 'button',
												click: ( ) => {

													zsConfirm(action.confirm.label, action.confirm.verb)
														.then(( ) => {

															let mutation = action.mutate();

															ctrl.caseResource().mutate(mutation.type, mutation.data).asPromise();

														});
												}
											});
										} else {
											assign(button,
												{
													link: $state.href(state, { action: action.name })
												}
											);
										}

										assign(
											button,
											{
												classes: {
													admin: action.context === 'admin',
													'first-admin': action.context === 'admin'
														&& get(arr[index - 1], 'context') !== 'admin'
												}
											}
										);

										return button;

									}
								)
									.filter(( button ) => {
										return isExpanded
											|| button.context !== 'admin';
									});

								if (actions.filter(action => action.context === 'admin').length > 0) {

									let split = partition(menuOptions, { context: 'admin' });

									menuOptions =
										split[1]
											.concat({
												name: 'expand',
												type: 'button',
												icon: expanded ? 'chevron-up' : 'chevron-down',
												label: `Beheeracties${expanded ? ' inklappen' : ''}`,
												click: ( ) => {
													expanded = !expanded;
												}
											})
											.concat(split[0]);

								}

								return menuOptions;

							});

					ctrl.getOptions = optionReducer.data;

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
