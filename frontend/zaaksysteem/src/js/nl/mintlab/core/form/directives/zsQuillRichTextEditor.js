/*global angular, _*/
(function ( ) {
	angular.module('Zaaksysteem.form')
		.directive('zsQuillRichTextEditor', [ '$timeout', '$interpolate', function ($timeout, $interpolate) {

			return {
				restrict: 'E',
				scope: {
                    editorId: '&',
                    editorDisabled: '&',
                    editorFeatures: '&'
				},
                template: 
                    '<div class="rich-text-toolbar" id="{{zsQuillRichTextEditor.getEditorId}}-toolbar">' +
                        '<ul>' +
                            '<li ng-repeat="button in zsQuillRichTextEditor.getButtons() track by button.name" ng-switch="button.type">' +
                                '<select ng-class="button.class" ng-switch-when="header"></select>' +
                                '<button ng-class="button.class" value="{{button.value}}" ng-switch-when="list"></button>' +
                                '<button ng-class="button.class" ng-switch-default></button>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<div class="rich-text-editor" id="{{zsQuillRichTextEditor.getEditorId}}-editor"></div>',
				bindToController: true,
				require: [ 'zsQuillRichTextEditor', 'ngModel' ],
				controller: [ '$scope', '$element', '$document', function ( $scope, $element, $document ) {

					var ctrl = this,
                        ngModel,
                        quill,
                        loaded = false,
                        features = [
                            {
                                name: 'header',
                                class: 'ql-header ql-picker',
                                type: 'header'
                            },
                            {
                                name: 'bold',
                                class: 'ql-bold'
                            },
                            {
                                name: 'italic',
                                class: 'ql-italic'
                            },
                            {
                                name: 'underline',
                                class: 'ql-underline'
                            },
                            {
                                name: 'bullet',
                                class: 'ql-list',
                                value: 'bullet',
                                type: 'list'
                            },
                            {
                                name: 'ordered',
                                class: 'ql-list',
                                value: 'ordered',
                                type: 'list'
                            },
                            {
                                name: 'link',
                                class: 'ql-link'
                            }
                        ],
                        instanceFeatures = ctrl.editorFeatures() ? ctrl.editorFeatures().split(' ') : [];

                    ctrl.getEditorId = ctrl.editorId();

                    ctrl.getButtons = function() {                           

                        return instanceFeatures.length ?

                            features.filter( function( feature ) {
                                return _.find(instanceFeatures, function(item) {
                                    return item === feature.name;
                                })
                            })

                            : features;
                    };

					ctrl.setNgModel = function ( model ) {
						ngModel = model;
					};

					function loadEditor() {

                        quill = new Quill('#'+ctrl.editorId()+'-editor', {
                            modules: {
                                toolbar: {
                                    container: '#'+ctrl.editorId()+'-toolbar'
                                }
                            },
                            theme: 'snow'
                        });

                        quill.on('text-change', function(delta, oldDelta, source) {

                            if (source === 'user') {
                                ngModel.$setViewValue(quill.container.children[0].innerHTML);
                            }

                        });

                        if (ctrl.editorDisabled() ){
                            quill.disable();
                        }

                        loaded = true;

                    };

                    if (!loaded) {
                        $timeout( function() {

                            loadEditor();

                        }, 0);
                    }

					ctrl.setEditorHtml = function( html ) {
                        if (loaded){
    						quill.pasteHTML(html);
                        }
					};

                    $scope.$watch(ngModel, function( newModel ){

                        if (ngModel.$viewValue) {
                            $timeout( function() {
                                ctrl.setEditorHtml(ngModel.$viewValue);
                            }, 0);
                        }

                    });

					return ctrl;
				}],
				controllerAs: 'zsQuillRichTextEditor',
				link: function ( scope, element, attrs, controllers ) {

					var zsQuillRichTextEditor = controllers[0],
						ngModel = controllers[1];

                    zsQuillRichTextEditor.setNgModel(ngModel);

				}
			};
		}]);
})();