/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsAlwaysVisibleReference', [ function ( ) {
			
			return {
				require: '^zsAlwaysVisibleParent',
				link: function ( scope, element, attrs, parent ) {
					parent.setElement(element);
					
					scope.$watch(function ( ) {
						parent.trigger();
					});
					
					scope.$on('$destroy', function ( ) {
						if(parent.getElement() === element) {
							parent.setElement(null);
						}
					});
				}
			};
			
		}]);
	
})();