/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.dom')
		.service('dropManager', [ function ( ) {
			
			var indexOf = window.zsFetch('nl.mintlab.utils.shims.indexOf'),
				intersects = window.zsFetch('nl.mintlab.utils.dom.intersects'),
				generateUid = window.zsFetch('nl.mintlab.utils.generateUid');
			
			function DropManager ( ) {
				
				var droppables = [],
					drops = {},
					sorted = false,
					currentDrop;
					
				function invalidateSorting ( ) {
					sorted = false;
				}
				
				function getNestingDepth ( element ) {
					var depths = [],
						p = element[0],
						index;
						
					while(p) {
						index = p.parentElement ? indexOf(p.parentElement.childNodes, p) : 0;
						depths.push(index);
						p = p.parentElement;
					}
					
					return depths;
				}
				
				function sort ( ) {
					droppables.sort(function ( a, b ) {
						var depthsA = a.data('zs-drop-manager-nesting-depth'),
							depthsB = b.data('zs-drop-manager-nesting-depth'),
							i = 0,
							l = Math.min(depthsA.length, depthsB.length);
							
						for(; i < l; ++i) {
							if(a[i] !== b[i]) {
								return a[i] > b[i] ? -1 : 1;
							}
						}
						return 0;
					});
					
					sorted = true;
				}
				
				function getSortedList ( ) {
					if(!sorted) {
						sort();
					}
					return droppables;
				}
				
				this.register = function ( element ) {
					
					element.data('zs-drop-manager-nesting-depth', getNestingDepth(element));
					
					if(indexOf(droppables, element) === -1) {
						droppables.push(element);
					}
					invalidateSorting();
				};
				
				this.unregister = function ( element ) {
					var index = indexOf(droppables, element);
					if(index !== -1) {
						droppables.splice(index, 1);
					}
					element.removeData('zs-drop-manager-nesting-depth', getNestingDepth(element));
					invalidateSorting();
				};
				
				this.createDrop = function ( mimetype, data ) {
					var dropId = generateUid(),
						drop = {
							mimetype: mimetype,
							data: data
						};
						
					drops[dropId] = drop;
					currentDrop = drop;
					return dropId;
				};
				
				this.getDrop = function ( dropId ) {
					return drops[dropId];
				};
				
				this.destroyDrop = function ( dropId ) {
					var drop = drops[dropId];
					if(drop === currentDrop) {
						currentDrop = null;
					}
					delete drops[dropId];	
				};
				
				this.getCurrentDrop = function ( ) {
					return currentDrop;	
				};
				
				this.getDroppable = function ( pos, draggable ) {
					var list = getSortedList(),
						i = 0,
						l = list.length,
						el,
						mimetype = draggable.attr('data-ng-drag-mimetype');
					
					for(; i < l; ++i) {
						el = list[i];
						if(el !== draggable && el.attr('data-ng-drop-mimetype') === mimetype && intersects(el[0], pos)) {
							//if(el.scope().validateDrop(dropData)) {
								return el;
							//}
						}
					}
					
					return null;
					
				};
				
			}
			
			
			return new DropManager();
			
		}]);
})();
