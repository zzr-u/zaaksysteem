/*global angular,_*/
angular
  .module('Zaaksysteem.docs')
  .controller('nl.mintlab.docs.EmailAttachmentsController', [
    '$scope', '$q', '$http', 'smartHttp', 'translationService', 'systemMessageService', 'snackbarService',
    function ($scope, $q, $http, smartHttp, translationService, systemMessageService, snackbarService) {
      var indexOf = _.indexOf,
        forEach = _.forEach,
        ccVisible = false,
        bccVisible = false,
        recipients,
        loading = false;

      $scope.template = null;
      $scope.templates = [];

      function getRecipientParams() {
        var type,
          id,
          name;

        switch ($scope.typeRecipient) {
          case 'coworker':
            id = $scope.recipient.id;
            name = $scope.recipient.naam;
            type = 'medewerker_uuid';
            break;

          case 'appealer':
            type = 'aanvrager';
            break;
          case 'coordinator':
            type = 'coordinator';
            break;
          case 'authorized':
            type = 'gemachtigde';
            break;
          case 'other':
            id = $scope.recipientAddress;
            type = 'overig';
            break;
        }

        return {
          notificaties_rcpt: type,
          notificaties_email: id,
          notificaties_cc: $scope.ccAddress,
          notificaties_bcc: $scope.bccAddress,
          betrokkene_naam: name
        };
      }

      function setTemplate() {
        var tpl = $scope.template,
          notification = tpl ? tpl.bibliotheek_notificaties_id : null;

        if (tpl) {
          $scope.typeRecipient = tpl.rcpt;
          $scope.roleRecipient = tpl.betrokkene_role;
        }

        // email attachments have been manually selected if context is docs
        if ($scope.context !== 'docs') {
          if ($scope.attachments) {
            $scope.attachments.length = 0;
          }

          _.each(tpl ? tpl.bibliotheek_notificaties_id.attachments : null, function (attachment) {
            var caseDoc = _.find($scope.caseView.getCase()['case'].case_documents, function (c) {
              return c.bibliotheek_kenmerken_id.id === attachment.bibliotheek_kenmerk_id;
            });

            if (caseDoc) {
              $scope.attachments.push({
                id: caseDoc.id,
                naam: caseDoc.bibliotheek_kenmerken_id.naam
              });
            }
          });
        }

        $scope.emailSubject = notification ? notification.subject : '';
        $scope.emailContent = notification ? notification.message : '';
      }

      function getDataFromActionItem(action) {
        var data = action ? action.data : {},
          attachments = [],
          rcpt = data.rcpt || 'overig',
          tpl;

        if (data.case_document_attachments) {
          forEach(data.case_document_attachments, function (caseDoc) {
            attachments.push({
              id: caseDoc.case_type_document_id,
              naam: caseDoc.naam
            });
          });
        }

        if (data.bibliotheek_notificaties_id !== undefined) {
          tpl = _.find($scope.templates, function (template) {
            return template.bibliotheek_notificaties_id.id === data.bibliotheek_notificaties_id;
          });

          if (tpl && tpl !== $scope.template) {
            $scope.template = tpl;
            setTemplate();
          }
        }

        $scope.emailSubject = data.subject || '';
        $scope.emailContent = data.body || '';

        $scope.ccAddress = data.cc || '';
        $scope.bccAddress = data.bcc || '';

        if ($scope.ccAddress) {
          ccVisible = true;
        }

        if ($scope.bccAddress) {
          bccVisible = true;
        }

        $scope.recipient = null;
        $scope.recipientAddress = '';

        if (rcpt === 'aanvrager') {
          $scope.typeRecipient = 'appealer';
        } else if (rcpt === 'betrokkene') {
          $scope.typeRecipient = 'betrokkene';
        } else if (rcpt === 'overig') {
          $scope.typeRecipient = 'other';
          $scope.recipientAddress = data.email;
        } else if (rcpt === 'gemachtigde') {
          $scope.typeRecipient = 'authorized';
        } else {
          $scope.typeRecipient = 'coworker';
          $scope.recipient = angular.isObject(data.rcpt) ? data.rcpt : {
            object_type: 'medewerker',
            id: data.behandelaar,
            naam: data.betrokkene_naam
          };
        }
      }

      function getTemplates() {
        return smartHttp
          .connect({
            method: 'GET',
            url: 'zaak/' + $scope.caseId + '/get_sjablonen',
            params: {
              type: 'notifications'
            }
          })
          .then(function (response) {
            return response.data.result;
          });
      }

      function getRoles() {
        return $http
          .get('/api/v1/subject/role')
          .then(function (response) {
            return response
              .data
              .result
              .instance
              .rows
              .map(function (item) {
                var label = item.instance.label;

                return {
                  label: label,
                  value: label
                };
              });
          });
      }

      function getEmailData() {
        var data,
          recipient,
          roleRecipient,
          typeRecipient,
          cc,
          bcc,
          subject,
          body,
          caseId,
          attachments = [],
          fileAttachments = [],
          caseTypeDocumentAttachments = [],
          templateAttachments;

        switch ($scope.typeRecipient) {
          case 'coworker':
            recipient = $scope.recipient
              ? $scope.recipient
                .map(function(recipient) {
                  return recipient.email
                })
                .join(',')
              : '';
            typeRecipient = 'overig';
            break;
          case 'appealer':
            typeRecipient = 'aanvrager';
            break;
          case 'betrokkene':
            typeRecipient = 'betrokkene';
            roleRecipient = $scope.roleRecipient;
            break;
          case 'authorized':
            typeRecipient = 'gemachtigde';
            break;
          case 'other':
            recipient = $scope.recipientAddress;
            typeRecipient = 'overig';
            break;
        }

        subject = $scope.emailSubject;
        body = $scope.emailContent;
        caseId = $scope.caseId;
        cc = $scope.ccAddress;
        bcc = $scope.bccAddress;

        attachments = [];

        forEach($scope.selectedAttachments, function (value) {
          attachments.push(value.id);
        });

        data = {
          recipient: recipient,
          recipient_type: typeRecipient,
          betrokkene_role: roleRecipient,
          cc: cc,
          bcc: bcc,
          subject: subject,
          body: body,
          case_id: caseId,
          log_error: 0
        };

        if ($scope.template) {
          (function () {
            var bn = $scope.template.bibliotheek_notificaties_id;

            templateAttachments = bn.attachments;

            forEach(templateAttachments, function (attachment) {
              caseTypeDocumentAttachments.push(attachment.bibliotheek_kenmerk_id);
            });

            if (bn.sender_address) {
              data.sender_address = bn.sender_address;
            }

            if (bn.sender) {
              data.sender = bn.sender;
            }

          })();
        }

        if (attachments.length || caseTypeDocumentAttachments.length) {
          if ($scope.context === 'docs') {
            fileAttachments = attachments;
          } else {
            caseTypeDocumentAttachments = caseTypeDocumentAttachments.concat(attachments);
          }
        }

        if (fileAttachments.length) {
          data.file_attachments = fileAttachments;
        }

        if (caseTypeDocumentAttachments.length) {
          data.case_document_ids = caseTypeDocumentAttachments;
        }

        return data;
      }

      function getPreviewFromValues(values) {        
        var to = values.recipient_type === 'overig'
          ? values.recipient_address
          : '';
                
        return {
          to: to,
          recipient_type: values.recipient_type,
          recipient_role: values.betrokkene_role,
          cc: values.cc,
          bcc: values.bcc,
          subject: values.subject,
          body: values.body
        };
      };

      function updatePreview() {
        var data;
        var attachments = $scope.selectedAttachments.map(function(item) {
          return [item.name, item.extension]
            .filter(Boolean)
            .join('');
        });
        
        try {
          data = getEmailData();
          $scope.previewData = _.extend({
            attachments: attachments
          }, getPreviewFromValues(data));
        } catch (e) {
          $scope.previewData = {}
        }
      };

      $scope.previewActive = false;
      
      $scope.togglePreview = function () {
        $scope.previewActive = !$scope.previewActive;
      };

      $scope.isPreviewActive = function () {
          return $scope.previewActive;
      };

      $scope.reloadData = function () {
        return $q.all({
          templates: getTemplates(),
          roles: getRoles()
        })
          .then(function (data) {
            $scope.templates = data.templates;
            $scope.template = null;
            $scope.roles = data.roles;
          });
      };

      $scope.sendEmail = function () {
        var data = getEmailData();
        loading = true;

        snackbarService
          .wait('E-mail wordt verstuurd', {
            promise:
              smartHttp.connect({
                method: 'POST',
                url: 'zaak/send_mail',
                data: data
              }),
            then: function () {
              return 'E-mail verstuurd';
            },
            catch: function (response) {
              var msg = 'Er ging iets fout bij het versturen van de e-mail. Probeer het later opnieuw.',
                error = _.get(response, 'data.result[0]', {});

              // exception for file size limits
              if (error.type === 'case/email/size') {
                msg = error.messages[0].replace(/(\d+:)/, '');
              }

              return msg;
            }
          })
          .then(function () {
            $scope.closePopup();
          })
          .finally(function () {
            loading = false;
          });
      };

      $scope.saveTemplate = function () {
        var params;

        params = {
          id: $scope.action.id,
          action_type: 'email',
          uniqueidr: '',
          update: 1,
          rownumber: '',
          notificaties_rcpt: undefined, // merge from converter
          notificaties_email: undefined, // merge from converter
          notificaties_subject: $scope.emailSubject,
          notificaties_body: $scope.emailContent,
          notificaties_case_document_attachments: _.map($scope.selectedAttachments, function (attachment) {
            return attachment.id;
          })
        };

        angular.extend(params, getRecipientParams());
        loading = true;

        $http({
          method: 'POST',
          url: '/zaak/' + $scope.caseId + '/action/data',
          data: params,
          blocking: false
        })
          .success(function (response) {
            var action = response.result[0];

            for (var key in action) {
              $scope.action[key] = action[key];
            }

            getDataFromActionItem($scope.action);

            systemMessageService.emitSave();
            $scope.closePopup();
          })
          .error(function ( /*response*/) {
            systemMessageService.emitSaveError('uw wijzigingen');
          })
          .finally(function () {
            loading = false;
          });
      };

      $scope.detach = function (attachment) {
        var index = indexOf($scope.selectedAttachments, attachment);

        if (index !== -1) {
          $scope.selectedAttachments.splice(index, 1);
        }
      };

      $scope.isAttached = function (attachment) {
        return indexOf($scope.selectedAttachments, attachment) !== -1;
      };

      $scope.showCc = function () {
        ccVisible = true;
      };

      $scope.showBcc = function () {
        bccVisible = true;
      };

      $scope.isCcVisible = function () {
        return ccVisible;
      };

      $scope.isBccVisible = function () {
        return bccVisible;
      };

      $scope.init = function () {

        if (!$scope.context) {
          throw new Error('Context not defined for EmailAttachmentsController');
        }

        $scope
          .reloadData()
          .then(function () {
            if ($scope.context === 'actions') {
              getDataFromActionItem($scope.action);
            }

            if (!$scope.typeRecipient) {
              $scope.typeRecipient = 'coworker';
            }

            if (!$scope.attachments) {
              $scope.attachments = [];
            }

            $scope.selectedAttachments = $scope.attachments.concat();
          });
      };

      $scope.handleTypeRecipientChange = function () {
        $scope.recipientAddress = '';
      };

      $scope.handleTemplateChange = setTemplate;

      $scope.showTemplateSelection = function () {
        return $scope.templates
          && $scope.templates.length
          && $scope.context !== 'actions';
      };

      $scope.showCcOptions = function () {
        return true;
      };

      $scope.isSaveDisabled = function () {
        var isValid = ($scope.emailForm && $scope.emailForm.$valid),
          disabled = !isValid;

        if (!isValid && $scope.emailForm) {
          disabled = !_.find($scope.emailForm.$error.required, function (control) {
            return control.$name === 'recipientAddress';
          });
        }

        return disabled;
      };

      $scope.getRecipients = function () {
        if (!recipients) {
          recipients = [
            {
              value: 'coworker',
              label: 'Collega'
            },
            {
              value: 'appealer',
              label: 'Aanvrager'
            },
            {
              value: 'authorized',
              label: 'Gemachtigde'
            },
            {
              value: 'betrokkene',
              label: 'Betrokkene'
            },
            {
              value: 'other',
              label: 'Overig'
            }
          ];
        }

        return recipients;
      };

      $scope.isLoading = function () {
        return loading;
      };

      $scope.$watch('newAttachment', function (nw/*, old*/) {
        var attachment,
          i,
          l;

        if (nw) {
          for (i = 0, l = $scope.attachments.length; i < l; ++i) {
            attachment = $scope.attachments[i];
            if (attachment.id === nw.id) {
              return;
            }
          }

          $scope.attachments.push(nw);
          $scope.selectedAttachments.push(nw);
        }

        $scope.newAttachment = null;
      });

      $scope.$watch('previewActive', function (current) {
        if (current) {
          updatePreview();
        }
    });
    }
  ]);
