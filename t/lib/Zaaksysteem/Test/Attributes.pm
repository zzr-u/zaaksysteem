package Zaaksysteem::Test::Attributes;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::Attributes;

sub test_attributes_pm {

    my $count = 361;
    my @things = Zaaksysteem::Attributes->predefined_case_attributes;
    is(@things, $count, "We have $count predefined case attributes");

    my @secrets = map { $_->name }
        sort { $a->name cmp $b->name } grep { $_->is_sensitive } @things;


    my @sensitive = sort qw(
        case.requestor.a_number
        case.requestor.bsn
        case.requestor.date_of_birth
        case.requestor_snapshot.a_number
        case.requestor_snapshot.bsn
        case.requestor_snapshot.date_of_birth
    );
    cmp_deeply(\@secrets, \@sensitive, "Found all the sensitive bits");

}

sub test_predefined_case_attributes_by_name {

    my $found = Zaaksysteem::Attributes->find_predefined_case_attribute(
        'case.price'
    );
    isa_ok($found, "Zaaksysteem::Object::Attribute");
    is('case.price', $found->name, "... and we have proven the name works");


    $found = Zaaksysteem::Attributes->find_predefined_case_attribute(
        'zaak_bedrag',
    );
    isa_ok($found, "Zaaksysteem::Object::Attribute");
    is(
        'case.price',
        $found->name,
        "... and we have proven the bwcompat_name works"
    );

    $found = Zaaksysteem::Attributes->find_predefined_case_attribute(
        'made_up',
    );
    is(undef, $found, "Made up magic strings don't return a value");


    lives_ok(
        sub {
            my $found = Zaaksysteem::Attributes->get_predefined_case_attribute(
                'case.price'
            );
            isa_ok($found, "Zaaksysteem::Object::Attribute");
            is('case.price', $found->name, "... and the get works too");
        },
        "get_predefined works shiney",
    );

    throws_ok(
        sub {
            Zaaksysteem::Attributes->get_predefined_case_attribute('made_up');
        },
        qr#case/system_attributes/not_found#,
        "Made up magic strings die horrible deaths"
    );
}

sub test_predefined_case_attributes_by_grouping {


    my $count = 14;
    my $found = Zaaksysteem::Attributes->find_predefined_case_attribute_grouping(
        'result'
    );
    isa_ok($found, "ARRAY");
    is(@$found, $count, "Found all $count result attributes");


    $found = Zaaksysteem::Attributes->find_predefined_case_attribute_grouping(
        'made_up',
    );
    is(undef, $found, "Made up groupings don't return a value");


    lives_ok(
        sub {
            my $found = Zaaksysteem::Attributes->get_predefined_case_attribute_grouping(
                'result'
            );
            isa_ok($found, "ARRAY");
            is(@$found, $count, "Found all $count result attributes");
        },
        "get_predefined works shiney",
    );

    throws_ok(
        sub {
            Zaaksysteem::Attributes->get_predefined_case_attribute_grouping('made_up');
        },
        qr#case/system_attributes/grouping/not_found#,
        "Made up magic strings die horrible deaths"
    );
}
__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Attributes - Test ZS::Attributes

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Attributes

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
