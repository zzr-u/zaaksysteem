package Zaaksysteem::Test::API::v1::Object::Session;

use Zaaksysteem::Test;

use Zaaksysteem::Test::Mocks;
use Zaaksysteem::API::v1::Object::Session;

use URI;

=head1 NAME

Zaaksysteem::Test::API::v1::Object::Session - Test session object construction

=head1 DESCRIPTION

Test queue model

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::API::v1::Object::Session

=head2 test_run

=cut

sub test_new_from_catalyst {
    my $self = shift;

    my $uexists = 0;
    my $c = mock_one(
        user        => { has_legacy_permissions => 0 },
        user_exists => sub { return $uexists },
        req         => {
            uri => sub { return URI->new('http://localhost/api/v1/') }
        },
        config            => { gemeente_id => 'template' },
        get_customer_info => {
            naam      => 'Mintlab B.V.',
            naam_lang => 'Mintlab B.V.',
            naam_kort => 'mintlab',
            email     => 'info@mintlab.nl',
        },
        model => sub {
            my $model = shift;
            if ($model eq 'DB::Config') {
                return mock_one(get => 0);
            }
            elsif ($model eq 'Redis') {
                return mock_one(get_or_set_json => \[]);
            }
            elsif ($model eq 'DB::Interface') {
                return mock_one(get_whitelisted_interfaces => sub { return () }
                );
            }
        },
    );

    $uexists = 0;
    my $session
        = Zaaksysteem::API::v1::Object::Session->new_from_catalyst($c);

    ok(!$session->logged_in_user, 'Found no logged in user');
    is($session->hostname, 'localhost', 'Found correct hostname');

    isa_ok($session->account,
        'Zaaksysteem::API::v1::Object::Session::Account');

    ok($session->account->$_, "Got value for $_") for qw/company email/;

    $uexists = 1;
    $session = Zaaksysteem::API::v1::Object::Session->new_from_catalyst($c);

    ok($session->logged_in_user, 'Found a logged in user');
    is($session->hostname, 'localhost', 'Found correct hostname');

    isa_ok($session->account,
        'Zaaksysteem::API::v1::Object::Session::Account');

    ok($session->account->$_, "Got value for $_") for qw/company email/;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
