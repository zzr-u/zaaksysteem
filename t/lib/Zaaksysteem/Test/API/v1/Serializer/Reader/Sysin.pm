package Zaaksysteem::Test::API::v1::Serializer::Reader::Sysin;

use Moose;

extends 'Zaaksysteem::Test::Moose';

=head1 NAME

Zaaksysteem::Test::API::v1::Serializer::Reader::Sysin - Serializer tests for
C<sysin/transaction> and C<sysin/transaction/record> objects

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::API::v1::Serializer::Reader::Sysin

=cut

use Zaaksysteem::Test;
use DateTime;
use Zaaksysteem::API::v1::Serializer;
use BTTW::Tools::RandomData qw[generate_uuid_v4];

sub test_object_groks {
    my $now = DateTime->now;
    my $yesterday = DateTime->now;

    my $record_id = generate_uuid_v4;
    my $interface_id = generate_uuid_v4;
    my $txn_id = generate_uuid_v4;

    my $serializer = Zaaksysteem::API::v1::Serializer->new;

    my $record = mock_one(
        'X-Mock-ISA' => 'Zaaksysteem::Backend::Sysin::TransactionRecord::Component',
        uuid => $record_id,
        input => 'abc',
        output => 'foo',
        is_error => 0,
        date_executed => $yesterday,
        preview_string => 'abc: foo',
    );

    my $interface = mock_one(
        uuid => $interface_id,
        name => 'bar_interface',
    );

    my $txn = mock_one(
        'X-Mock-ISA' => 'Zaaksysteem::Backend::Sysin::Transaction::Component',
        uuid => $txn_id,
        direction => 'outgoing',
        date_created => $yesterday,
        date_deleted => undef,
        date_next_retry => $now,
        date_last_retry => undef,
        interface_id => $interface,
        transaction_records => mock_one(
            all => sub { return ($record) }
        ),
    );

    is_deeply $serializer->read($record), {
        type => 'sysin/transaction/record',
        reference => $record_id,
        preview => 'abc: foo',
        instance => {
            date_executed => sprintf('%sZ', $yesterday->iso8601),
            input => 'abc',
            output => 'foo',
            preview_string => 'abc: foo',
            is_error => \0
        }
    }, 'serializer reader output for txn/records';

    cmp_deeply $serializer->read($txn), {
        type => 'sysin/transaction',
        reference => $txn_id,
        instance => {
            direction => 'outgoing',
            date_created => sprintf('%sZ', $yesterday->iso8601),
            date_deleted => undef,
            date_next_retry => sprintf('%sZ', $now->iso8601),
            date_last_retry => undef,
            interface => {
                type => 'interface',
                instance => undef,
                reference => $interface_id,
                preview => 'bar_interface',
            },
            records => {
                type => 'set',
                reference => undef,
                preview => ignore(),
                instance => {
                    rows => ignore()
                }
            }
        },
    }, 'serializer reader output for txn';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
