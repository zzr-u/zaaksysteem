package Zaaksysteem::Zaken::Model;

use Moose;
use namespace::autoclean;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Zaken::Model - A model to interact with for cases

=head1 DESCRIPTION

This model works with old school case DB objects.
This is an abstraction so we can use similar tactics for API/v1
and other places.

=head1 SYNOPSIS

    use Zaaksysteem::Zaken::Model;

    my $model = Zaaksysteem::Zaken::Model->new(
        queue_model => $queue_model,
    );

    $model->execute_phase_actions($zaak, { phase => 1 });

=cut

use BTTW::Tools;
use Clone qw(clone);
use List::Util qw(first uniq any);
use Moose::Util::TypeConstraints qw[enum union];
use URI;
use Zaaksysteem::Constants qw(
    ZAAKSYSTEEM_CONSTANTS
    CASE_PAYMENT_STATUS_FAILED
    CASE_PAYMENT_STATUS_SUCCESS
    CASE_PAYMENT_STATUS_PENDING
    CASE_PAYMENT_STATUS_OFFLINE
);
use Zaaksysteem::Types qw(
    ACLCapability
    ACLEntityType
    ACLScope
    BSN
    Boolean
    CaseConfidentiality
    EmailAddress
    KvK
    MobileNumberSloppy
    NonEmptyStr
    TelephoneNumberSloppy
    UUID
);

=head1 ATTRIBUTES

=head2 base_uri

A L<URI> object, required.

=head2 schema

A L<Zaaksysteem::Schema> object, required.

=head2 rs_zaak

A L<Zaaksysteem::Zaken::Resultset> object, required

=head2 object_model

A L<Zaaksysteem::Object::Model> object, required.

=head2 subject_model

A L<Zaaksysteem::BR::Subject> object, required.

=head2 queue_model

A L<Zaaksysteem::Object::Queue::Model> object, required.

=head2 document_model

A L<Zaaksysteem::Document::Model> object, required.

=head2 user

Holds a reference toi the effective user interacting with the model.

=cut

has base_uri => (
    is => 'rw',
    isa => 'URI',
    required => 1
);

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

has object_model => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Object::Model',
    required => 1,
);

has subject_model => (
    is       => 'ro',
    isa      => 'Zaaksysteem::BR::Subject',
    required => 1,
);

has queue_model => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Object::Queue::Model',
    required => 1,
);

has rs_zaak => (
    is  => 'ro',
    isa => 'Zaaksysteem::Zaken::ResultSetZaak',
    required => 1,
);

has document_model => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Document::Model',
    required => 1,
);

has user => (
    is  => 'ro',
    isa => 'Zaaksysteem::Backend::Subject::Component'
);

=head1 METHODS

=head2 execute_phase_actions

    $self->execute_phase_actions($zaak, { phase => 1 });

Execute action for a given phase

Named arguments

=over

=item phase

The ID of the phase/milstone.

=item skip_assignment

Skip the allocation assignment, booleanish value

=back

=cut

define_profile execute_phase_actions => (
    required => { phase           => 'Int', },
    optional => { skip_assignment => 'Bool', loop_protection_counter => 'Int', },
);

sig execute_phase_actions => 'Zaaksysteem::Zaken::ComponentZaak, HashRef';

sub execute_phase_actions {
    my $self = shift;
    my $zaak = shift;
    my $args = assert_profile(shift)->valid;

    my $phase = $args->{ phase };

    # Handle phase actions
    my $actions;
    if ($phase == 1) {
        $actions = $zaak->registratie_fase->case_actions->search(
            { case_id => $zaak->id })->sorted;

    }
    else {
        $actions = $zaak
            ->zaaktype_node_id
            ->zaaktype_statussen
            ->search({ status => $phase })
            ->first
            ->case_actions
            ->search({ $zaak->id })
            ->sorted;
    }

    $actions->apply_rules({
        case      => $zaak,
        milestone => $args->{phase},
    });

    # Handwired rule execution... sigh
    my $rules_result = $zaak->execute_rules({ status => $args->{phase} });

    # Part 1: execute the things that templates and emails may depend on.
    if ($rules_result) {
        if ($rules_result->{wijzig_registratiedatum}) {
            try {
                $zaak->process_date_of_registration_rule(
                    %{ $rules_result->{wijzig_registratiedatum} });
            }
            catch {
                $self->log->error($_);
            }
        }

        if ($rules_result->{wijzig_afhandeltermijn}) {
            try {
                $zaak->process_date_target_rule(
                    %{ $rules_result->{wijzig_afhandeltermijn} });
            }
            catch {
                $self->log->error($_);
            }
        }
    }

    my @case_actions = $actions->all;

    # Get them by execution ordering (sorted just gives the ordering per type)
    my @ordered_actions = (
        (grep { $_->type eq 'subject' } @case_actions),
        (grep { $_->type eq 'allocation' } @case_actions),
        (grep { $_->type eq 'template' } @case_actions),
        (grep { $_->type eq 'case' } @case_actions),
        (grep { $_->type eq 'email' } @case_actions)
    );

    my @items;

    for my $action (@ordered_actions) {
        next unless $action->automatic;

        $self->log->debug(sprintf(
            'Running case registration phase action %d ("%s")',
            $action->id, $action->label
        ));

        # ZS-13337: Skip allocation actions if a specific assignee is provided
        if ($action->type eq 'allocation' && $args->{skip_assignment}) {
            $self->log->debug('Allocation action overridden by specific assignee');
            next;
        }

        # Prevent loops with start subcase actions
        # See also Zaaksysteem::Zaken::Roles::FaseObjecten
        if ($action->type eq 'case') {
            my $data = $action->data;
            $data->{loop_protection_counter} = $args->{loop_protection_counter} // 1;
            $action->data($data);
            $action->update();
        }

        push @items, $zaak->fire_action(
            action       => $action,
            current_user => $self->user
        );
    }

    # Bundle the phase transition actions and setup for meta-item broadcast
    $self->queue_model->queue_item(
        $self->queue_model->create_ordered_item_set($self->user, @items)
    ) if scalar @items;

    # Part 2 of rule execution (external system messages may depend on
    # generated templates, so do it afterwards)
    if ($rules_result) {
        if ($rules_result->{send_external_system_message}) {
            try {
                $zaak->send_external_system_messages(
                    base_url => $self->base_uri,
                    rules_result => $rules_result->{send_external_system_message}
                );
            }
            catch {
                $self->log->error($_);
            }
        }
    }

    return 1;
}

=head2 assert_case_id

Assert if the case ID is valid for a case create call

=cut


sig assert_case_id => 'Int';

sub assert_case_id {
    my ($self, $id) = @_;

    my $val = $self->schema->storage->dbh_do(
        sub {
            my ($storage, $dbh, @args) = @_;
            my $sth = $dbh->prepare('select last_value from zaak_id_seq');
            $sth->execute();
            my @result = $sth->fetchrow_array;
            return $result[0];
        }
    );

    throw(
        "case/sequence/too_high",
        "Case sequence number is too high",
    ) if $id > $val;
}

=head2 add_case_files

Add files to a case

=cut

sig add_case_files => 'Zaaksysteem::Model::DB::Zaak, ?HashRef';

sub add_case_files {
    my ($self, $case, $docs) = @_;

    foreach (values %{$docs}) {
        $self->_add_case_file($case, $_);
    }
    return scalar keys %$docs;
}

=head2 generate_case_id

Get a new case id from the database

=cut

sub generate_case_id {
    my $self = shift;
    my $id   = $self->schema->resultset('Zaak')->generate_case_id;
    $self->_log_case_id($id);
    return $id;
}

=head2 update_case_files

Update files for a case

=cut

sig update_case_files => 'Zaaksysteem::Model::DB::Zaak, ?HashRef';

sub update_case_files {
    my ($self, $case, $docs) = @_;

    foreach (values %{$docs}) {
        $self->_update_case_file($case, $_);
    }
    return scalar keys %$docs;
}

=head2 create_case

Create a case

=cut

sig create_case => 'HashRef';

sub create_case {
    my ($self, $params) = @_;

    return try {
        my $zaak;

        $self->schema->txn_do(sub {
            $zaak = $self->_create_case($params);

            # Update the contact details for the requestor
            $self->update_contact_details($zaak, $params->{contact_details});

            # Add the subject relations to the case
            $self->relate_case_subjects($zaak, $params);

            # Put the case into the correct group/route/whatefs.
            $self->set_case_allocation($zaak, $params);

            $zaak->create_default_directories();

            # Add files to the case
            $self->add_case_files($zaak, $params->{documents}{add});
            $self->update_case_files($zaak, $params->{documents}{update});
        });

        $self->execute_phase_actions(
            $zaak,
            {
                phase           => 1,
                skip_assignment => $params->{assignee} ? 1 : 0,
            }
        );

        if (!$ENV{ZS_FEATURE_FLAG_SPLIT_BRAIN}) {
            $zaak->update_case_properties(@{ $zaak->object_attributes });
        }
        $zaak->discard_changes;

        return $zaak;
    } catch {
        # Ensure we log the failure, it might get filtered later
        $self->log->error($_);
        die $_;
    };
}

=head2 relate_case_subjects

Relate case subjects

=cut

sig relate_case_subjects => 'Zaaksysteem::Model::DB::Zaak, ?Hashref';

sub relate_case_subjects {
    my ($self, $zaak, $params) = @_;

    foreach my $subject_like_object (@{$params->{related}}) {
        $self->_try_catch_case_action(
            sub {
                $zaak->betrokkene_relateren($subject_like_object);
                $self->send_pip_email($zaak, $subject_like_object);
            }
        );
    }
    return 1;
}

=head2 set_case_allocation

Allocate the case to the correct person/department/group/role

=cut

sig set_case_allocation => 'Zaaksysteem::Model::DB::Zaak, HashRef';

define_profile set_case_allocation => (
    optional => {
        assignee              => "Zaaksysteem::Object::Types::Subject",
        coordinator           => "Zaaksysteem::Object::Types::Subject",
        routing               => 'HashRef',
        open                  => 'Bool',
        send_assignment_email => 'Bool',
    },
);

sub set_case_allocation {
    my ($self, $zaak, $params) = @_;

    $self->log->trace('Zaaksysteem::Zaken::Model.set_case_allocation');

    my $role  = $params->{routing}{role};
    my $group = $params->{routing}{group};

    if ($params->{coordinator}) {
        $self->_try_catch_case_action(
            sub {
                $zaak->set_coordinator(
                    $params->{coordinator}->old_subject_identifier
                );
            }
        );
    }

    if ($params->{assignee}) {
        my $subject_id = $params->{assignee}->old_subject_identifier;
        $self->_try_catch_case_action(
            sub {
                $zaak->set_behandelaar($subject_id);

                if ($params->{send_assignment_email}) {
                    $self->send_case_assignment_mail($zaak);
                }
            }
        );

        # Handle the "Ook afdeling wijzigen" checkbox
        if ($role && $group) {
            $self->_try_catch_case_action(
                sub {
                    $zaak->wijzig_route(
                        {
                            route_ou   => $group->group_id,
                            route_role => $role->role_id,
                            change_only_route_fields => 1,
                        }
                    );
                }
            );
        }

        if ($params->{open}) {
            $self->_try_catch_case_action(
                sub {
                    if (!$zaak->coordinator) {
                        $zaak->set_coordinator($subject_id);
                    }
                }
            );

            $self->_try_catch_case_action(
                sub {
                    $zaak->open_zaak($params->{assignee});
                }
            );
        }
    }
    elsif ($group && $role) {
        $self->_try_catch_case_action(
            sub {
                $zaak->case_actions->hijack_allocation_action(
                    {
                        group => $group,
                        role  => $role
                    }
                );
            }
        );
    }
    return;
}

=head2 update_contact_details

Update the contact details of the case requestor. Skips employees.

=cut

sig update_contact_details => 'Zaaksysteem::Model::DB::Zaak, ?HashRef';

sub update_contact_details {
    my ($self, $zaak, $contact_details) = @_;

    return unless $contact_details;

    my $requestor = $zaak->aanvrager_object;

    return if $requestor->isa('Zaaksysteem::Betrokkene::Object::Medewerker');

    my @fields;

    if (($requestor->email // "") ne ($contact_details->{email_address} // "")) {
        $requestor->email($contact_details->{email_address});

        push @fields, "email";
    }

    if (($requestor->telefoonnummer // "") ne ($contact_details->{phone_number} // "")) {
        $requestor->telefoonnummer($contact_details->{phone_number});

        push @fields, "telefoonnummer";
    }

    if (($requestor->mobiel // "") ne ($contact_details->{mobile_number} // "")) {
        $requestor->mobiel($contact_details->{mobile_number});

        push @fields, "mobiel";
    }

    return unless @fields;

    $self->schema->resultset("Logging")->trigger('subject/update_contact_data', {
        component => "betrokkene",
        created_for => $requestor->betrokkene_identifier,
        component_id => $requestor->id,
        data => {
            subject_id => $requestor->betrokkene_identifier,
            fields => \@fields,
        }
    });

    return;
}

=head2 get_casetype_object

Get the casetype object from the object store

=cut

sub get_casetype_object {
    my ($self, $uuid) = @_;

    my $casetype = $self->_find_casetype_by_uuid($uuid);

    return $casetype if $casetype;

    throw(
        'case/casetype_not_found',
        sprintf(
            'Casetype "%s" could not be found, unable to continue', $uuid
        ),
        { http_code => 400 }    # sic, 400 indicates client fault
    );
}

=head2 assert_casetype

Get the casetype from the database and check if it is active

=cut

sub assert_casetype {
    my ($self, $casetype) = @_;

    my $ct = $self->_find_casetype_by_id($casetype->casetype_id);

    return $ct if $ct->active;

    throw('api/v1/case/casetype/node/inactive',
        'The casetype was found, but is inactive, unable to create cases');
}

=head2 assert_values

Check if all the values are wrapped in an array.

=cut

sig assert_values => '?HashRef';

sub assert_values {
    my $self = shift;
    my $values = shift // {};

    if (my $key = first { ref $values->{$_} ne 'ARRAY' } keys %$values) {
        throw(
            'case/create/values/non_array',
            sprintf(
                'Every attribute value must be wrapped in an array. Validation for attribute "%s" failed.',
                $key),
            { http_code => 400 }
        );
    }
    return %{$values};
}

=head2 send_case_assignment_mail

Send the case allocation mail towards the assignee

=cut

sig send_case_assignment_mail => 'Zaaksysteem::Zaken::ComponentZaak';

sub send_case_assignment_mail {
    my ($self, $case) = @_;
    my $template = $self->get_assignment_notification_template;

    $self->log->trace('send_case_assignment_mail');

    $self->send_case_email(
        case     => $case,
        subject  => $case->behandelaar_object->as_object,
        template => $template,
    );

    return 1;
}

=head2 send_case_email

Send an email to a subject from a case

=cut

define_profile send_case_email => (
    required => {
        case     => 'Zaaksysteem::Zaken::ComponentZaak',
        subject  => 'Zaaksysteem::Object::Types::Subject',
        template => 'Zaaksysteem::DB::Component::BibliotheekNotificaties',
    },
);

sub send_case_email {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    if (my $address = $params->{subject}->subject->email_address) {
        $self->log->debug("Sending email to $address");
        $params->{case}->mailer->send_case_notification(
            {
                notification => $params->{template},
                recipient    => $address,
            }
        );
    }
    else {
        throw(
            "case/notifcation/recipient_email/missing",
            "Unable to send case notification, no e-mail address configured for subject: "
                . $params->{subject}->id
        );
    }
}

=head2 send_pip_email

Send mails pip authorization e-mails

=cut

sub send_pip_email {
    # Subject like object tyvm
    my ($self, $case, $subject) = @_;

    if (!$subject->{send_auth_confirmation} && !$subject->{pip_authorized}) {
        $self->log->trace(
            "Will not send case authorization e-mail: not authorized and no auth confirmation set"
        );
        return;
    }

    # Subject has a reference.. get it
    my $s = $self->_get_object_model_subject($subject->{subject});

    my $template = $self->get_pip_notification_template();

    $self->send_case_email(
        case     => $case,
        subject  => $s,
        template => $template,
    );
    return 1;

}

=head2 assert_subjects

Validate the all the subjects in the call and returns a hash in list context to the caller. 
Dies when one of the underlying checks fails.
This function is also responsible for the check if the case assignment e-mail will be sent.

=head3 datastructure

    (
        requestor => L <Zaaksysteem::Object::Types::Subject>,
        assignee  => L <Zaaksysteem::Object::Types::Subject>,
        recipient => L <Zaaksysteem::Object::Types::Subject>,
        related   => [L <Zaaksysteem::Object::Types::Subject>, ..],
        send_assignment_email => Boolean value
    );

=cut

sig assert_subjects => 'Zaaksysteem::Schema::ZaaktypeNode, HashRef';

my %_subject_mapper = (
    requestor => "Aanvrager",
    recipient => "Ontvanger",
    assignee  => "Behandelaar",
);

sub assert_subjects {
    my ($self, $node, $params) = @_;

    my @subject_data;
    my $cleaner = sub {
        my $data = shift;
        my %ret;

        if ($data->{type} eq 'subject') {
            $ret{subject} = $data;
        }
        else {
            $ret{subject_type} = $data->{type};
            $ret{subject_id}   = $data->{id};

            if ($data->{type} eq 'requestor') {
                $ret{send_assignment_confirmation}
                    = $data->{send_assignment_confirmation};
            }
        }

        $ret{send_assignment_confirmation}
            = $data->{send_assignment_confirmation};

        return %ret;
    };

    # In preperation of deprecating the old "subject" parameters, we translate it
    # to the new subjects format.
    foreach my $type (keys %_subject_mapper) {
        next unless $params->{$type};
        push(
            @subject_data,
            {
                $cleaner->($params->{$type}),
                magic_string_prefix => $type,
                role                => $_subject_mapper{$type},
                type                => $type,
            }
        );
    }

    if (exists $params->{subjects}) {
        # Allow 'subjects => { %subject_data }' and
        # 'subjects => [ { %subject_data }, { ... }, ... ]' call styles
        push @subject_data,
            ref $params->{subjects} eq 'ARRAY'
            ? @{ $params->{subjects} }
            : $params->{subjects};
    }

    assert_profile(
        $_,
        profile => {
            required => {
                magic_string_prefix => NonEmptyStr,
                role                => NonEmptyStr
            },
            optional => {
                subject_type   => enum([qw[person company employee]]),
                subject_id     => union([qw[Str HashRef]]),
                subject        => 'HashRef',
                type           => enum([qw[requestor assignee recipient]]),
                pip_authorized => Boolean,
                send_auth_confirmation       => Boolean,
                send_assignment_confirmation => Boolean,
            },
            require_some => { subject => [1, qw[subject subject_id]], },
            dependencies => { subject_id => ['subject_type'] }
        }
    ) for @subject_data;

    my %case_subjects;

    my $skip_related;
    for my $subject_args (@subject_data) {
        $skip_related = 0;

        my $subject = $self->_get_subject($subject_args);

        if (my $type = $subject_args->{type}) {
            $case_subjects{$type} = $subject;

            if ($type eq 'requestor' || $type eq 'recipient') {
                $skip_related = 1;
            }
            elsif ($type eq 'assignee' && $subject_args->{send_assignment_confirmation}) {
                $case_subjects{send_assignment_email} = 1;
            }
        }

        if (!$skip_related) {
            # Final parameter mangling, the data below gets passed into
            # $zaak->betrokkene_relateren(...)
            my $relate_subject_args = {
                %{$subject_args}, betrokkene_identifier => $subject->old_subject_identifier
            };

            $relate_subject_args->{rol} = delete $relate_subject_args->{role};
            push(@{$case_subjects{related}}, $relate_subject_args);
        }
    }
    $case_subjects{related}   //= [];
    $case_subjects{requestor} //= $self->_assert_requestor($node);

    if (!$case_subjects{assignee}) {
        my $assignee = $self->_find_assignee($node);
        if ($assignee) {
            $case_subjects{assignee}    = $assignee;
            $case_subjects{coordinator} = $assignee;
        }
    }

    return %case_subjects;
}

=head2 assert_documents

Assert all document related params from the JSON

=cut

sub assert_documents {
    my ($self, $attrs, $values, $files, $zaak) = @_;

    my $documents = $self->_assert_file_attributes($attrs, $values, $zaak);
    return $documents unless $files;
    return $self->_assert_files($documents, $files, $zaak);
}

=head2 assert_routing

Check if the routing can be setup correctly.
Returns a list of the group and role objects.

=cut

sub assert_routing {
    my ($self, $params) = @_;

    my ($group, $role);

    if (exists $params->{route}) {
        my $route_args = assert_profile(
            $params->{route},
            profile => {
                required => {
                    group_id => 'Int',
                    role_id  => 'Int'
                }
            }
        )->valid;

        $group = $self->_get_group($route_args->{group_id});
        $role  = $self->_get_role($route_args->{role_id});
    }
    return ($group, $role);
}


=head2 get_case_attributes

Get all the attributes of a case

=cut

sub get_case_attributes {
    my ($self, $rs) = @_;
    return $rs->search_rs(
        {
            'bibliotheek_kenmerken_id'            => { '!=' => undef },
            'bibliotheek_kenmerken_id.value_type' => { '!=' => 'file' }
        },
        { prefetch => 'bibliotheek_kenmerken_id' }
    );
}


=head2 get_document_attributes

Get all the document attributes of a casetype

=cut

sig get_document_attributes => 'Zaaksysteem::DB::ResultSet::ZaaktypeKenmerken';

sub get_document_attributes {
    my ($self, $rs) = @_;

    return $rs->search_rs(
        { 'bibliotheek_kenmerken_id.value_type' => 'file' });
}


=head2 normalize_attribute_value

Make sure all the attribute values are what they need be

=cut

sig normalize_attribute_value => 'Zaaksysteem::DB::Component::ZaaktypeKenmerken, ?ArrayRef';

sub normalize_attribute_value {
    my ($self, $attr, $value) = @_;

    return $value unless $attr->bibliotheek_kenmerken_id->can_have_multiple_values_per_value;

    if (!defined $value->[0]) {
        return $value->[0];
    }
    elsif (ref $value->[0] eq 'ARRAY') {
        return [@{ $value->[0] }];
    }

    throw(
        'api/v1/case/incorrect_values',
        'You need to wrap multiple options (checkboxes) into an array: "foo" : [ [ "value", "value2" ] ]',
    );

}

=head2 assert_case_attributes

Assert maybe a wrong word, normalize the case attributes to something create_zaak understands

=cut

sub assert_case_attributes {
    my ($self, $rs, $values) = @_;

    $rs = $self->get_case_attributes($rs);
    # Unroll the intersection of supplied key-value-pairs and kenmerken in the
    # registration phase, and create an array of hashrefs of id => value pairs
    # Also, we don't yet support 'opplusbare velden' attributes, so deref the
    # first POST'ed value explicitly.
    my @normal_values;
    while (my $attr = $rs->next) {
        my $value = $self->normalize_attribute_value($attr,
            $values->{ $attr->magic_string });
        push(@normal_values, { $attr->get_column('bibliotheek_kenmerken_id') => $value });
    }
    return \@normal_values;
}

=head2 assert_contact_details

Check if the contact details of a user are correct

=cut

define_profile assert_contact_details => (
    optional => {
        phone_number  => TelephoneNumberSloppy,
        mobile_number => MobileNumberSloppy,
        email_address => EmailAddress
    }
);

sub assert_contact_details {
    my ($self, $contact_details) = @_;
    return assert_profile($contact_details // {})->valid;
}

=head2 generate_rule_engine

Generate a hash structure with rule engine properties.
See L<Zaaksysteem::Backend::Rules#generate_object_params> for more information.

=cut

define_profile generate_rule_engine => (
    required => {
        node      => 'Defined',
        channel   => 'Defined',
        requestor => 'Defined',
    },
    optional => { attributes => 'HashRef', },
    defaults => { attributes => sub { {} }, },
);

sub generate_rule_engine {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    return Zaaksysteem::Backend::Rules->generate_object_params(
        {
            'case.casetype.node'      => $opts->{node},
            'case.channel_of_contact' => $opts->{channel},
            'case.number_status'      => 1,
            'case.requestor'          => $opts->{requestor},
        },
        {
            include_kenmerken => $opts->{attributes},
            engine            => 1,
            validation        => 1,
        }
    );
}

=head2 assert_required_attributes

Play the rules and be nice for the first phase of the case

=cut

define_profile assert_required_attributes => (
    required => {
        node      => 'Defined',
        channel   => 'Defined',
        requestor => 'Defined',
    },
    optional => { attributes => 'HashRef', },
    defaults => { attributes => sub { {} }, },
);


sub assert_required_attributes {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $rules = $self->generate_rule_engine(
        node       => $opts->{node},
        requestor  => $opts->{requestor},
        channel    => $opts->{channel},
        attributes => $opts->{attributes},
    );

    my $validation = $rules->{validation};
    my $rv = $validation->process_params({
        keyformat => 1,
        values => $opts->{attributes},
        engine => $rules->{rules},
    }
    );

    my $status = $opts->{node}->zaaktype_statussen->search(
        { status => 1 }
    )->first;

    my $zt_attr = $opts->{node}->zaaktype_kenmerken->search(
        {
            zaak_status_id  => $status->id,
            value_mandatory => 1,
        },
        { prefetch => 'bibliotheek_kenmerken_id' },
    );

    my @active_attrs = map { my $foo = $_; $foo =~ s/^attribute\.//; $foo }
        @{ $validation->active_attributes };

    while(my $a = $zt_attr->next) {
        my $bib   = $a->bibliotheek_kenmerken_id;
        my $id    = $bib->id;
        my $label = $bib->magic_string;

        if (!exists $rv->{$id} && any { $_ eq $label } @active_attrs) {
            throw("zaak/create/required/attributes/missing",
                "Unable to proceed, required attribute $label is missing");
        }
    }
    return 1;
}

=head2 prepare_case_arguments

Get the JSON as a Perl datastructure and transform it to something for the C<create_case> function.

=cut

define_profile prepare_case_arguments => (
    required => {
        casetype_id => UUID,
        source      => enum(ZAAKSYSTEEM_CONSTANTS->{contactkanalen}),
    },
    optional => {
        values          => 'HashRef',
        requestor       => 'HashRef',
        recipient       => 'HashRef',
        assignee        => 'HashRef',
        route           => 'HashRef',
        confidentiality => CaseConfidentiality,
        open            => Boolean,
        contact_details => 'HashRef',
        subjects        => 'HashRef',
        files           => 'HashRef',
        number          => 'Int',
    },
    defaults => {
        values          => sub { return {} },
        open            => 0,
        confidentiality => 'public',
    }
);

sub prepare_case_arguments {
    my $self   = shift;
    my $params = assert_profile(shift)->valid;

    $self->log->debug(
        "Preparing case create arguments " . dump_terse($params));

    my %rv;

    foreach (qw(source confidentiality open)) {
        $rv{$_} = $params->{$_};
    }

    $rv{case_id} = $params->{number} if $params->{number};

    my $ct_object = $self->get_casetype_object($params->{casetype_id});
    my $casetype  = $self->assert_casetype($ct_object);
    my $node      = $casetype->zaaktype_node_id;

    my %values   = $self->assert_values($params->{values});

    $rv{casetype} = $casetype;

    my %case_subjects = $self->assert_subjects($node, $params);
    foreach (keys %case_subjects) {
        $rv{$_} = $case_subjects{$_};
    }


    # Decide on the assignee/route
    my ($group, $role) = $self->assert_routing($params);
    $rv{routing} = { group=> $group, role => $role};

    my $attributes = $self->_get_case_attributes(
        node => $node,
        magic_strings => [keys %values]
    );

    $rv{values}    = $self->assert_case_attributes($attributes, \%values);
    $rv{documents} = $self->assert_documents(
        $self->get_document_attributes($attributes), \%values, $params->{files});

    # I'm not sure which this is here? But the query will never be
    # executed. So it's here.
    my $geolatlon_kenmerken = $attributes->search_rs(
        { 'bibliotheek_kenmerken_id.value_type' => 'geolatlon' });

    # Only update contact details if the key is specified.
    if ($params->{contact_details} && ref $params->{contact_details} eq 'HASH') {
        $rv{contact_details} = $self->assert_contact_details($params->{contact_details});
    }

    return \%rv;
}


=head2 create_delayed_case

Create a delayed case queue item

=cut

sig create_delayed_case => 'HashRef';

sub create_delayed_case {
    my ($self, $args) = @_;

    my $case_id = $self->generate_case_id();
    my $queue = $self->queue_model;

    my $item = $queue->create_item({
        type  => 'create_case',
        label => "Create case $case_id",
        subject => $self->user,
        data  => {
            create_args => $args,
            case_id     => $case_id,
        }
    });

    $queue->queue_item($item);

    return $item;
}

=head2 update_specific_acls

Update case-specific ACLs for a case, and potentially related cases.

=cut

sig update_specific_acls => 'Zaaksysteem::Zaken::ComponentZaak, ArrayRef, HashRef';

sub update_specific_acls {
    my ($self, $case, $acl_values, $cascade_to) = @_;

    # Validate each given acl, and split the capabilities
    my @acls = _preprocess_acl_data($acl_values);

    my @zaken = $case;

    if ($cascade_to->{related}) {
        $self->log->trace("Cascade to related cases");
        push @zaken, map {
            $_->case
        } $case->zaak_relaties;
    }

    if ($cascade_to->{partial}) {
        $self->log->trace("Cascade to partial cases");
        push @zaken, $case->zaak_children;
    }

    if ($cascade_to->{continuation}) {
        $self->log->trace("Cascade to continuation cases");
        push @zaken, $case->zaak_vervolgers;
    }

    # Push the changes into our database
    try {
        $self->schema->txn_do(sub {
            for my $zaak (@zaken) {
                $self->log->trace(sprintf("Updating ZaakAuthorisation for '%s'", $zaak->id));

                # We fancy in place updates, let's compare.
                my $current_acls = $zaak->zaak_authorisations->search();
                # Find entries in the db, and delete the entries we did not find.
                my %got_indexes;
                while (my $dbacl = $current_acls->next) {
                    my $found_acl = 0;

                    for (my $i = 0; $i < @acls; $i++) {
                        my $acl = $acls[$i];

                        next unless (
                            $dbacl->capability eq $acl->{capability} &&
                            $dbacl->entity_id eq $acl->{entity_id} &&
                            $dbacl->entity_type eq $acl->{entity_type} &&
                            $dbacl->scope eq $acl->{scope}
                        );

                        $found_acl++;
                        $got_indexes{$i} = 1;
                    }

                    $dbacl->delete if !$found_acl;
                }

                # Create the entries not already in the DB
                for (my $i = 0; $i < @acls; $i++) {
                    next if $got_indexes{$i};
                    my $acl = $acls[$i];

                    $zaak->zaak_authorisations->create({
                        %$acl,
                        zaak_id => $zaak->id,
                    });
                }

                # Prepare a delayed touch for the (now modified) case so the ZaakAuthorisation
                # records are synced to object ACLs
                $zaak->touch();
            }
        });
    } catch {
        $self->log->error("Error updating ACLs: $_");

        throw('api/v1/case/acl/update/db_error', sprintf(
            'Problem inserting given data in our database'
        ));
    };

    $case->_touch;

    return;
}

=head2 update_payment_status

Update the payment status of a case.

=cut

define_profile update_payment_status => (
    required => {
        zaak => 'Zaaksysteem::Zaken::ComponentZaak',
        payment_status => enum([
            CASE_PAYMENT_STATUS_FAILED,
            CASE_PAYMENT_STATUS_SUCCESS,
            CASE_PAYMENT_STATUS_PENDING,
            CASE_PAYMENT_STATUS_OFFLINE,
        ]),
    },
);

sub update_payment_status {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    return $opts->{zaak}->set_payment_status(
        $opts->{payment_status},
        $opts->{zaak}->payment_amount,
    );
}

=head2 get_pip_notification_template

Get the PIP notification template from the database

=cut

sub get_pip_notification_template {
    my ($self) = @_;
    $self->_get_template_from_library('subject_pip_authorization_confirmation_template_id');
}

=head2 get_assignment_notification_template

Get the case assignment notification template from the database

=cut

sub get_assignment_notification_template {
    my ($self) = @_;
    return $self->_get_template_from_library('allocation_notification_template_id');
}

=head2 update_case

Update a case

=cut

sig update_case => 'Zaaksysteem::Model::DB::Zaak';

define_profile update_case => (
    optional => {
        values       => 'HashRef',
        files        => 'Defined',    # arrayref
        user         => 'Defined',
        payment_info => 'HashRef',
    },
    defaults => {
        values => sub { return {} },
    }
);

sub update_case {
    my $self = shift;
    my $case = shift;
    my $params = assert_profile({@_})->valid;

    my %values = %{$params->{values}};
    my $user = delete $params->{user};

    my %rv;
    my $node = $case->zaaktype_node_id;

    my $attributes = $self->_get_case_attributes(
        node          => $node,
        magic_strings => [keys %values],
        $user ? (user => $user) : (),
        phase         => $case->milestone + 1,
    );

    $rv{values} = $self->assert_case_attributes($attributes, \%values);

    $rv{documents} = $self->assert_documents(
        $self->get_document_attributes($attributes),
        \%values,
        $params->{files}
    );

    my $documents_added = $self->add_case_files($case, $rv{ documents }{ add });
    my $documents_updated = $self->update_case_files($case, $rv{ documents }{ update });
    my $fields_updated = 0;

    if ($rv{ values }) {
        # Unpack values from [ { ... }, { .... }, { .... } ] to
        # a merged hash of the value entries.
        my %nv = map { %{ $_ } } @{ $rv{ values } };

        $fields_updated = $case->zaak_kenmerken->update_fields({
            new_values => \%nv,
            zaak       => $case
        });
    }

    # Only create notifications if changes where made by an external process
    if (not defined $user or $user->is_external_api) {
        if ($fields_updated > 0) {
            $case->create_message_for_behandelaar(
                event_type => 'api/v1/update/attributes',
                message => sprintf(
                    "%d kenmerk%s aangepast door een extern proces",
                    $fields_updated,
                    ($fields_updated > 1 ? 'en' : '')
                ),
            );
        }

        if ($documents_added > 0) {
            $case->create_message_for_behandelaar(
                event_type => 'api/v1/update/documents',
                message => sprintf(
                    '%d document%s toegevoegd door een extern proces',
                    $documents_added,
                    ($documents_added > 1 ? 'en' : '')
                )
            );
        }

        if ($documents_updated > 0) {
            $case->create_message_for_behandelaar(
                event_type => 'api/v1/update/documents',
                message => sprintf(
                    '%d document%s aangepast door een extern proces',
                    $documents_updated,
                    ($documents_updated > 1 ? 'en' : '')
                )
            );
        }
    }

    if ($params->{payment_info}) {
        my $event = $self->update_payment_status(
            zaak           => $case,
            payment_status => $params->{payment_info}{payment_status},
        );
        $case->create_message_for_behandelaar(
            message => sprintf("Betaalstatus voor zaak %d is aangepast", $case->id),
            log        => $event,
            event_type => $event->event_type,
        );
    }
    return 1;

}

=head2 get_by_serial_number

Get the case based on the sequence number

=cut

sig get_by_serial_number => 'Int';

sub get_by_serial_number {
    my ($self, $id) = @_;

    my $rs = $self->rs_zaak->search_extended({ 'me.id' => $id });

    my $case = $rs->first;
    return $case if $case;

    $self->assert_case_id($id);
    return;
}

=head1 PRIVATE METHODS

=head2 _try_catch_case_action

Try catch everything for case actions that may or may not fail, but
aren't supposed to stop the case creation process.
All errors are logged in INFO.

=cut

sub _try_catch_case_action {
    my $self = shift;
    my $sub  = shift;

    try {
        $sub->();
    }
    catch {
        $self->log->info($_);
    };
    return;
}

=head2 _create_case

The most basic create case call

=cut

define_profile _create_case => (
    required => {
        casetype  => 'Defined',
        requestor => 'Zaaksysteem::Object::Types::Subject',
        source    => 'Defined',
    },
    optional => {
        trigger                  => enum([qw(intern extern internextern)]),
        recipient                => 'Zaaksysteem::Object::Types::Subject',
        values                   => 'Any',
        registration_date        => 'DateTime',
        case_id                  => 'Int',
        confidentiality          => CaseConfidentiality,
        skip_required_attributes => 'Bool',
    },
    defaults => {
        trigger           => 'extern',
        confidentiality   => 'public',
        values            => sub { [] },
        registration_date => sub { DateTime->now() },

        # Skip required fields, we currently don't have something in the
        # frontend that passes the skip required fields. And we potentially
        # break api/v1 consumers when introducing this globally
        skip_required_attributes => 1,
    },
);

sub _create_case {
    my $self   = shift;
    my $params = assert_profile(shift)->valid;

    my %args = (
        zaaktype_id      => $params->{casetype}->id,
        aanvraag_trigger => $params->{trigger},
        confidentiality  => $params->{confidentiality},
        contactkanaal    => $params->{source},
        registratiedatum => $params->{registration_date},
        kenmerken        => $params->{values},
        aanvragers       => [
            {
                betrokkene  => $params->{requestor}->old_subject_identifier,
                rol         => 'Aanvrager',
                verificatie => 'n/a',
            }
        ],
        skip_required_attributes => $params->{skip_required_attributes},
    );

    if ($params->{recipient})  {
        $args{ontvanger} = $params->{recipient}->old_subject_identifier;
    }

    if ($params->{case_id}) {
        $self->assert_case_id($params->{case_id});
        $args{override_zaak_id} = 1;
        $args{id}               = $params->{case_id};
    }

    return try {
        return $self->schema->resultset('Zaak')->create_zaak(\%args);
    }
    catch {

        # Special case for stuf-zkn
        if ($_ =~ /duplicate key value violates unique constraint "zaak_pkey"/) {
            throw(
                'case/create_failed/case_id/exists',
                "Case creation failed, case number $params->{case_id} already exists",
                { original_error => $_ }
            );
        }

        $self->log->error($_);

        throw(
            'case/create_failed',
            'Case creation failed, unable to continue.',
            { original_error => $_ }
        );
    };
}

=head2 _add_case_files

Add files to a case

=cut

sub _add_case_file {
    my ($self, $case, $doc) = @_;
    my $fs = $doc->{filestore};

    my %create = (
        disable_message => 1,
        name            => $doc->{name} // $fs->original_name,
        db_params       => {
            accepted     => 1,
            filestore_id => $fs->id,
            created_by   => $self->user->betrokkene_identifier,
            case_id      => $case->id
        }
    );

    if ($doc->{attribute}) {
        $create{case_document_ids} = [ $doc->{attribute}->id ];
    }
    if ($doc->{metadata}) {
        $create{metadata} = $doc->{metadata};
    }

    if ($doc->{number}) {
        $create{db_params}{id} = $doc->{number};
    }

    try {
        $self->schema->resultset('File')->file_create(\%create);
    }
    catch {
        # Special case for stuf-zkn
        if ($_ =~ /duplicate key value violates unique constraint "file_pkey"/) {
            throw(
                'file/create_failed/file_id/exists',
                "file creation failed, file number $doc->{file_id} already exists",
                { original_error => $_ }
            );
        }

        $self->log->error($_);

        throw(
            'file/create_failed',
            'File creation failed, unable to continue.',
            { original_error => $_ }
        );
    };
    return 1;
}

=head2 _update_case_file

Update the case files, includes metadata updating

=cut

sub _update_case_file {
    my ($self, $case, $doc) = @_;

    my %update = (
        accepted => 1,
        case_id => $case->id,
        subject => $self->user->as_object
    );

    if ($doc->{attribute}) {
        $update{case_document_ids} = [ $doc->{attribute}->id ];
    }
    if ($doc->{name}) {
        $update{name} = $doc->{name};
    }

    $doc->{file}->update_properties(\%update);

    if ($doc->{metadata}) {
        $doc->{file}->update_metadata($doc->{metadata});
    }
    return 1;
}

sig _get_template_from_library => 'Str';

sub _get_template_from_library {
    my ($self, $key) = @_;

    my $id = $self->schema->resultset('Config')->get($key);
    my $template;
    if ($id) {
        $template = $self->schema->resultset('BibliotheekNotificaties')->find($id);
        return $template if $template;
    }
    $id //= 'id not found';
    throw("case/template/failed/id_not_found",
        "Unable find notification template by key $key [$id]");
}

=head2 _find_casetype_by_uuid

Go ask the object model to find the casetype for us.

=cut

sub _find_casetype_by_uuid {
    my ($self, $uuid) = @_;
    return try {
        $self->object_model->retrieve(uuid => $uuid);
    }
    catch {
        $self->log->info($_);
        throw('case/casetype/retrieval_fault',
            'Fault during casetype retrieval, unable to continue: ' . $_);
    };

}

=head2 _find_casetype_by_id

Find the casetype database object from the database.

=cut

sub _find_casetype_by_id {
    my ($self, $id) = @_;

    return try {
        $self->schema->resultset('Zaaktype')->find($id);
    }
    catch {
        $self->log->info($_);
        throw('api/v1/case/casetype/node/retrieval_fault',
            'The casetype was found, but could not be retrieved from backend, unable to continue.'
        );
    };

}

=head2 _assert_document_ids

Asserts the document IDS references, uniqifies and does UUID typechecking.

=cut

sub _assert_document_ids {
    my ($self, $ids) = @_;
    my @uuid;
    my $ref = ref $ids;
    if (!$ref) {
        @uuid = ($ids) if defined $ids;
    }
    elsif ($ref eq 'ARRAY') {
        @uuid = @{$ids};
    }
    else {
        throw("case/documents/unknown_data",
            "Unable to determine the data type for documents");
    }
    @uuid = grep { defined($_) && length($_) } uniq(@uuid);
    foreach my $uuid (@uuid) {
        if (!UUID->check($uuid)) {
            throw('case/file/uuid/syntax', "'$uuid' is not a UUID");
        }
    }
    return \@uuid;
}

=head2 _assert_file_attribute

Check if a file is found in the filestore, and if we need to update or
add the file

=cut

sub _assert_file_attribute {
    my ($self, $zaak, $attr, $values, $documents) = @_;

    my $document_ids = $values->{ $attr->bibliotheek_kenmerken_id->magic_string };
    my $uuids        = $self->_assert_document_ids($document_ids);

    foreach my $uuid (@$uuids) {
        my ($filestore, $file) = $self->_get_filestore_by_uuid($uuid, $zaak);
        if ($file) {
            $documents->{update}{$uuid} = { filestore => $filestore, file => $file, attribute => $attr };
        }
        else {
            $documents->{add}{$uuid} = { filestore => $filestore, attribute => $attr };
        }
    }
}

=head2 _assert_file_attributes

Assert the file attributes

=cut

sub _assert_file_attributes {
    my ($self, $attrs, $values, $zaak) = @_;

    my %documents;
    while (my $doc = $attrs->next) {
        $self->_assert_file_attribute($zaak, $doc, $values, \%documents);
    }
    return \%documents;
}

=head2 _assert_files

Assert non-case-document uploads. Tries to add/update the documents
which are already processed as case documents and allows updating
metadata. This is used by the documentintake, but can also be used by
other API consumers.

=cut

sub _assert_files {
    my ($self, $documents, $files, $zaak) = @_;

    foreach my $f (@$files) {
        my $ref   = $f->{reference};
        my $metadata = $self->_assert_file_metadata($f->{metadata});
        my $file_id = $f->{number};

        if ($file_id) {
            $self->document_model->assert_document_serial_number($file_id);
        }

        my $data = $documents->{add}{$ref};
        if (!$data) {
            $data = $documents->{update}{$ref};
            if (!$data) {
                my ($filestore, $file) = $self->_get_filestore_by_uuid($ref, $zaak);
                if ($file) {
                    $data = { filestore => $filestore, file => $file };
                    $documents->{update}{$ref} = $data;
                }
                else {
                    $data = {
                        filestore => $filestore,
                        $file_id ? (number => $file_id) : (),
                    };
                    $documents->{add}{$ref} = $data;
                }
            }
        }

        # We rely on being a reference, so no updating required
        $data->{name} //= $f->{name};
        $data->{metadata} //= $metadata;

    }
    return $documents;
}

=head2 _assert_file_metadata

Asserts the file metadata properties and transforms the date.

=cut

define_profile _assert_file_metadata => (
    optional => [
        qw/description trust_level origin document_category pronom_format appearance structure origin_date/
    ],
    field_filters => {
        origin_date => sub {
            my $date = DateTime::Format::DateParse->parse_datetime(pop, 'UTC');
            $date->set_time_zone('Europe/Amsterdam');
            return $date->ymd;
        }
    },
);

sub _assert_file_metadata {
    my $self = shift;
    my $md = assert_profile(shift//{})->valid;
    return $md if keys %$md;
    return;
}

=head2 _get_filestore_by_uuid

Get the filestore object by UUID

=cut

sub _get_filestore_by_uuid {
    my ($self, $uuid, $zaak) = @_;

    if (!UUID->check($uuid)) {
        throw('case/filestore/uuid/syntax', "'$uuid' is not a UUID");
    }

    my $filestore = $self->schema->resultset('Filestore')
        ->find({ uuid => $uuid });

    if (!$filestore) {
        throw(
            'case/file/reference/not_found',
            "Unable to find uuiderence $uuid"
        );
    }

    # Check if file is coming from doc_intake
    my $rs = $filestore->files->search_rs({ case_id => $zaak ? $zaak->id : undef });
    my $file = $rs->next;
    if ($file && $rs->next) {
        $self->log->error(
            "Found multiple references from 'file' table to filestore($uuid)"
        );
        throw(
            'api/v1/case/create/multiple_file_references',
            "Case creation failed, too many file references for $uuid",
        );
    }
    return ($filestore, $file);
}

=head2 _get_subject

Get the subject from the various stores

=cut

sub _get_subject {
    my ($self, $subject_args) = @_;

    if ($subject_args->{subject}) {
        return $self->_get_object_model_subject($subject_args->{subject});
    }
    elsif ($subject_args->{subject_type} eq 'employee') {
        return $self->_get_employee_subject($subject_args->{subject_id});
    }
    else {
        return $self->_get_betrokkene_model_subject($subject_args->{subject_type}, $subject_args->{subject_id});
    }
}

=head2 _get_employee_subject

Get employees as and return the L<Zaaksysteem::Object::Types::Subject> for it.

=cut

sub _get_employee_subject {
    my ($self, $id) = @_;

    my $subject = $self->schema->resultset('Subject')->find($id);
    return $subject->as_object if $subject;

    throw('api/v1/case/subject_not_found',
        sprintf('Subject could not be found for provided ID "%s"', $id));
}

=head2 _get_object_model_subject

Get a subject from the object model and return the L<Zaaksysteem::Object::Types::Subject> for it.

=cut

sub _get_object_model_subject {
    my ($self, $subject) = @_;

    my $ref = $self->object_model->inflate_from_hash($subject);

    # Fix instantiator with subject bridge passthru
    $ref->instantiator(
        sub {
            $self->subject_model->find(shift->id);
        }
    );

    $subject = try {
        return $ref->_instance;
    }
    catch {
        $self->log->info($_);
        throw(
            'api/v1/case/subject_not_found',
            sprintf('Subject could not be found for provided ID "%s"',
                $ref->id)
        );
    };
    return $subject if $subject;

    throw('api/v1/case/subject_not_found',
        sprintf('Subject could not be found for provided ID "%s"', $ref->id));
}


=head2 _get_subject_by_legacy_id

Get the subject from the subject model by ID

=cut

sub _get_subject_by_legacy_id {
    my ($self, $id) = @_;
    return $self->subject_model->get_by_old_subject_identifier($id);
}


=head2 _get_betrokkenen_model_subject

Get a subject from the Subject Bridge

=cut

sub _get_betrokkene_model_subject {
    my ($self, $type, $subject_search) = @_;

    my %args;
    $args{subject_type} = $type;

    if ($type eq 'company') {
        my $numbers = assert_profile(
            $subject_search,
            profile => {
                required => { kvk_number    => KvK, },
                optional => { branch_number => 'Int' }
            }
        )->valid;

        $args{subject}{coc_number} = $numbers->{kvk_number};

        if ($numbers->{branch_number}) {
            $args{subject}{coc_location_number} = $numbers->{branch_number};
        }
    }
    elsif ($type eq 'person') {
        my $numbers = assert_profile(
            { bsn => $subject_search },
            profile => {
                required => { bsn => BSN },
            }
        )->valid;

        $args{subject} = { personal_number => $numbers->{bsn} };
    }

    my ($subject) = $self->subject_model->search(\%args);
    return $subject if $subject;

    throw("case/get_subject",
        "Unable to find subject via supplied params: "
            . dump_terse(\%args));
}

=head2 _assert_requestor

Assert if the default requestor is available on the casetype.
Returns the L<Zaaksysteem::Object::Type::Subject> when available.
Dies when there is no default requestor.

=cut

sub _assert_requestor {
    my ($self, $node) = @_;

    my $definition = $node->zaaktype_definitie_id;
    if (my $id = $node->zaaktype_definitie_id->preset_client) {
        return $self->_get_subject_by_legacy_id($id);
    }
    throw(
        'api/v1/case/requestor_unresolvable',
        sprintf(
            'No (valid) requestor was provided, and no preset requestor available'
        ),
        { http_code => 400 }
    );

}

=head2 _find_assignee

If the casetype has a default assignee return that subject, otherwise, returns undef

=cut

sub _find_assignee {
    my ($self, $node) = @_;
    my $id = $node->properties->{preset_owner_identifier};
    return if !$id;
    return $self->_get_subject_by_legacy_id($id);
}

=head2 _get_group

Get a group object by id from the database

=cut

sub _get_group {
    my ($self, $id) = @_;
    return $self->schema->resultset('Groups')->find($id)->object;
}


=head2 _get_role

Get a role object by id from the database

=cut

sub _get_role {
    my ($self, $id) = @_;
    return $self->schema->resultset('Roles')->find($id)->object;
}

=head2 _get_case_attributes

Get all the case attributes of the first phase

=cut

define_profile _get_case_attributes => (
    required => {
        node          => 'Defined',
    },
    optional => {
        magic_strings => 'Defined', # arrayref
        user          => 'Defined',
        phase         => 'Int',
    },
    defaults => {
        magic_string => sub { return [] },
    }
);

sub _get_case_attributes {
    my ($self) = shift;
    my $args = assert_profile({@_})->valid;

    my $rs = $args->{node}->zaaktype_kenmerken->search_by_magic_strings(@{$args->{magic_strings}});
    my $user = delete $args->{user};

    $rs = $rs->search_rs(
        {
            # 'specifieke behandelrechten' always off-limits for API users
            # TODO: Fix this some way or another
            required_permissions => [ undef, '{}' ],
        },
        { prefetch => [qw(bibliotheek_kenmerken_id)]  }
    );
}

=head2 _log_case_id

Log the requested case ID

=cut

sub _log_case_id {
    my ($self, $generated_id) = @_;

    $self->schema->resultset('Logging')->trigger(
        'case/id_requested',
        {
            component    => 'zaak',
            component_id => $generated_id,
            data         => {
                remote_system => 'Zaaksysteem',
                interface     => 'api/v1',
            },
        }
    );
    return;
}

=head2 _preprocess_acl_data

Pre-process ACL data from API/v1 into a format that C<update_specific_acls> can use.

=cut

sub _preprocess_acl_data {
    my $raw_acl_values = shift;

    my @acls;
    for my $rawacl (@$raw_acl_values) {
        my $rawacl = assert_profile($rawacl, profile => {
            required => {
                capabilities => ACLCapability,
                entity_id    => sub {
                    my $val = shift;

                    return 1 if $val =~ /^[0-9]+|[0-9]+$/;
                    return;
                },
                entity_type  => ACLEntityType,
                scope        => ACLScope,
            }
        })->valid;

        # Skip types and other than position entity_types
        next unless ($rawacl->{scope} eq 'instance' && $rawacl->{entity_type} eq 'position');

        my $capabilities = $rawacl->{capabilities};
        if (any { $_ eq 'manage' } @$capabilities) {
            $capabilities = [qw(manage write read search)];
        }
        elsif (any { $_ eq 'write' } @$capabilities) {
            $capabilities = [qw(write read search)];
        }
        elsif (any { $_ eq 'read' } @$capabilities) {
            $capabilities = [qw(read search)];
        }
        elsif (any { $_ eq 'search' } @$capabilities) {
            $capabilities = [qw(search)];
        }

        for my $capability (@$capabilities) {
            my $acl = clone $rawacl;
            $acl->{capability} = $capability;

            delete($acl->{capabilities});
            push(@acls, $acl);
        }
    }

    return @acls;
}

__PACKAGE__->meta->make_immutable; ## no critic (RequireEndWithOne)

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
