package Zaaksysteem::Zaken::Roles::FaseObjecten;
use Moose::Role;

use BTTW::Tools;
use DateTime::Format::ISO8601;
use DateTime;
use Email::Valid;
use List::Util qw(all any none);
use Moose::Util::TypeConstraints qw[enum];
use Zaaksysteem::API::v1::Message::Case::PhaseTransition;
use Zaaksysteem::Backend::Tools::Term qw(calculate_term);
use Zaaksysteem::Environment;
use Zaaksysteem::ZTT;
use Zaaksysteem::Zaken::AdvanceResult;

use Zaaksysteem::Backend::Tools::WorkingDays qw(
    add_working_days
    diff_working_days
);
use Zaaksysteem::Constants qw(
    ZAAKSYSTEEM_OPTIONS
    ZAAKSYSTEEM_NAMING
    BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION
);

with 'Zaaksysteem::Zaken::Roles::ZaakSetup';

# Do not create more than four sub cases otherwise you end up with
# mayhem
my $LOOP_PROTECTION_MAX = 4;

sub zaak_cache {
    my $self           = shift;
    my $caching_object = shift;

    my $calling_sub = [caller(1)]->[3];

    if ($caching_object) {
        $self->{_zaak_cache} = {}
          unless $self->{_zaak_cache};

        return ($self->{_zaak_cache}->{$calling_sub} = $caching_object);
    }

    if ($self->{_zaak_cache}) {
        return $self->{_zaak_cache}->{$calling_sub}
          if $self->{_zaak_cache}->{$calling_sub};
    }

    return;
}

sub flush_cache {
    my $self            = shift;

    return unless $self->{_zaak_cache};

    delete($self->{_zaak_cache});
}


sub set_volgende_fase {
    my $self            = shift;

    my $volgende_fase   = $self->volgende_fase;
    return unless $volgende_fase;

    my $milestone = $volgende_fase->status;
    return if !$self->_set_fase($milestone);

    if ($self->is_afhandel_fase) {
        $self->log->trace("Closing case (is_afhandel_fase is true)");
        $self->set_gesloten;
    } else {
        $self->log->trace("Log phase change (is_afhandel_fase is false)");
        $self->trigger_logging('case/update/milestone', {
            component => 'zaak',
            data => {
                case_id => $self->id,
                phase_id => $self->huidige_fase->id
            }
        });
    }

    $self->flush_cache;
    return 1;
}


sub set_vorige_fase {
    my $self            = shift;

    my $vorige_fase     = $self->vorige_fase;

    return unless $vorige_fase;

    return unless $self->can_vorige_fase;

    if ( $self->is_afgehandeld) {
        $self->set_heropen;
    }

    $self->flush_cache;
    $self->_set_fase($vorige_fase->status);
}

sub set_heropen {
    my $self            = shift;

    my $status = $self->status;

    return if none { $status eq $_ } qw(resolved stalled);

    if ($status eq 'resolved') {
        $self->afhandeldatum(undef);
        $self->vernietigingsdatum(undef);
        $self->archival_state(undef);

        $self->_set_fase($self->milestone - 1) if $self->is_closed_by_phase;
    }

    $self->update_status('open');

    $self->trigger_logging('case/reopen', { component => 'zaak', data => {
        case_id => $self->id
    }});

}

sub update_status {
    my ($self, $status) = @_;

    my $old = $self->status;
    return if $old eq $status;

    $self->flush_cache;
    $self->update({status => $status});
    $self->discard_changes;
    $self->ddd_update_system_attribute('case.status');
    return 1;
}


sub set_gesloten {
    my $self = shift;
    my $time = shift;

    $time ||= DateTime->now;

    $self->afhandeldatum($time);
    $self->set_vernietigingsdatum;

    $self->update_status('resolved');

    my $result = $self->get_zaaktype_result();

    my $resultaat = '';
    if ($result && $result->label) {
        $resultaat = sprintf("%s (%s)", $result->label, $result->resultaat);
    } elsif ($result) {
        $resultaat = sprintf("%s", $result->resultaat);
    }

    $self->trigger_logging(
        'case/close',
        {
            component => 'zaak',
            data      => {
                case_id     => $self->id,
                timestamp   => $time->datetime,
                case_result => $resultaat,
            }
        }
    );

    if (my $parent = $self->pid) {
        $self->log->debug(
            sprintf(
                "Subcase %d was marked 'closed'. Sending notification to parent.",
                $self->id,
            )
        );

        my $event = $parent->trigger_logging(
            'case/close_child',
            {
                component => 'zaak',
                data      => {
                    case_id       => $parent->id,
                    child_case_id => $self->id,
                    timestamp     => $time->datetime,
                    case_result   => $resultaat,
                }
            }
        );

        $parent->create_message_for_behandelaar(
            message => sprintf(
                "Deelzaak '%d' afgehandeld.",
                $self->id,
            ),
            event_type => 'case/close_child',
            log        => $event,
        );
    }
}

=head2 set_vernietigingsdatum

Apply a destruction date on a case.

If there is no result the destruction date is 1 year in the future.
If there is a result the vernietigingsdatum will be set according to the rules of the resulttype.

=head3 RETURNS

undef if there is no 'afhandeldatum'.
Returns a DateTime object if successful.
Dies in case of an error.

=cut

sub set_vernietigingsdatum {
    my $self = shift;
    my ($force_archival) = @_;
    my $afhandeldatum = $self->afhandeldatum;

    if (!$afhandeldatum) {
        $self->log->trace(sprintf(
            "Zaak %d: No afhandeldatum. Not setting vernietigingsdatum.", $self->id
        ));

        return;
    }

    my $casetype_result = $self->get_zaaktype_result;

    # Geen resultaat: 1 year default
    if (!$casetype_result) {
        $self->log->warn(sprintf(
            "Zaak %d: No result. Setting vernietigingsdatum to now + 1 year.", $self->id
        ));

        return $self->vernietigingsdatum($afhandeldatum->clone()->add(years => 1));
    }

    # This result doesn't trigger archiving
    if (!$casetype_result->trigger_archival && !$force_archival) {
        $self->log->trace(sprintf("Zaak %d: Result doesn't have trigger_archival flag. Not setting vernietigingsdatum.", $self->id));

        return;
    }

    my $dt = $afhandeldatum->clone();
    $dt->add(days => $casetype_result->bewaartermijn);

    if ($self->log->is_trace) {
        $self->log->trace(sprintf(
            "Zaak %d: Bewaartermijn: '%s' (%s) -> %s",
            $self->id,
            $casetype_result->bewaartermijn,
            ZAAKSYSTEEM_OPTIONS->{BEWAARTERMIJN}->{$casetype_result->bewaartermijn},
            $dt->dmy,
        ));
    }

    my $bewaartermijn = ZAAKSYSTEEM_OPTIONS->{ BEWAARTERMIJN }{ $casetype_result->bewaartermijn };

    if ($bewaartermijn eq 'Bewaren' && ($self->archival_state // '' ne 'overdragen')) {
        $self->log->trace(sprintf("Zaak %d: Setting archival state 'overdragen'.", $self->id));
        $self->archival_state('overdragen');
    } else {
        $self->log->trace(sprintf("Zaak %d: Setting archival state 'vernietigen'.", $self->id));
        $self->archival_state('vernietigen');
    }

    my $vd = $self->vernietigingsdatum;
    if (!$vd || $dt ne $vd) {
        $self->log->trace(sprintf("Zaak %d: Setting vernietigingsdatum to %s", $self->id, $dt->dmy));

        $self->vernietigingsdatum($dt);
        $self->trigger_logging('case/update/purge_date', {
                component => 'zaak',
                data      => {
                    purge_date => $dt->dmy,
                    purge_date_type => ($bewaartermijn eq 'Bewaren' ? 'bewaren' : 'termijn'),
                    case_id    => $self->id,
                },
            }
        );
    }

    return $self->vernietigingsdatum;
}


sub fasen {
    my $self    = shift;

    return $self->zaaktype_node_id->zaaktype_statussen(
        undef,
        {
            order_by    => { -asc   => 'status' }
        }
    );
}


sub huidige_fase {
    my $self    = shift;

    return $self->zaak_cache if $self->zaak_cache;

    return $self->zaak_cache($self->zaaktype_node_id->zaaktype_statussen->search({
        status  => $self->milestone,
    })->first);
}


sub volgende_fase {
    my $self    = shift;

    return $self->zaak_cache if $self->zaak_cache;

    return $self->zaak_cache($self->zaaktype_node_id->zaaktype_statussen->search({
        status  => ($self->milestone + 1)
    })->first);
}


sub vorige_fase {
    my $self    = shift;

    return $self->zaak_cache if $self->zaak_cache;

    return $self->zaak_cache($self->zaaktype_node_id->zaaktype_statussen->search({
        status  => ($self->milestone - 1)
    })->first);
}


sub registratie_fase {
    my $self    = shift;

    # TODO: no return $self->zaak_cache here?

    return $self->zaak_cache($self->zaaktype_node_id->zaaktype_statussen->search(
        undef,
        {
            order_by    => { -asc => 'status' },
            rows        => 1,
        }
    )->first)
}


sub afhandel_fase {
    my $self    = shift;

    return $self->zaak_cache if $self->zaak_cache;

    return $self->zaak_cache($self->zaaktype_node_id->zaaktype_statussen->search(
        undef,
        {
            order_by    => { -desc => 'status' },
            rows        => 1,
        }
    )->first);
}

=head2 is_in_phase($phase)

my $bool = $case->is_in_phase('registratie_fase');

Check if a zaak is in the specified fase (phase).

=head3 ARGUMENTS

=over

=item phase [REQUIRED]

=back

=head3 RETURNS

A boolean, 1 if true, 0 if false

=cut

sub is_in_phase {
    my ($self, $phase) = @_;

    if (!$self->can($phase)) {
        throw(
            'case/phase',
            "Unable to determine phase '$phase' for $self"
        );
    }

    return $self->$phase->status eq $self->huidige_fase->status;
}

sub is_afhandel_fase {
    my $self    = shift;
    return $self->is_in_phase('afhandel_fase');
}

sub is_closed_by_phase {
    my $self = shift;
    return $self->afhandel_fase->status eq $self->milestone;
}

sub is_afgehandeld {
    my $self    = shift;

    return 1 if $self->status eq 'resolved';
    return $self->is_closed_by_phase;
}


sub is_open {
    my $self    = shift;

    return 1 if ($self->status =~ /new|open/);
    return;
}



sub is_volgende_afhandel_fase {
    my $self    = shift;

    return unless $self->volgende_fase;

    if ($self->afhandel_fase->status eq $self->volgende_fase->status) {
        return 1;
    }

    return;
}


=head2 can_volgende_fase

Determine wether the case is ready for the next move. This routine is expanded (using
around) throughout the other roles for the class. The results are bundled together in
an object, as to allow specific feedback on which items are not ready yet.

=cut

sub can_volgende_fase {
    my $self = shift;
    my $mutation_checker = shift;

    my $advance_result = Zaaksysteem::Zaken::AdvanceResult->new;

    if ($self->is_volgende_afhandel_fase && not $self->resultaat) {
        $advance_result->fail('result_complete', 'No result set');
    } else {
        $advance_result->ok('result_complete');
    }

    # Key2Finance calls us, but has no way of accessing a model instance. A
    # reference to the model can't be hidden in the default_attributes of the
    # schema either , that'll cause circuclar references, and cleanup is a
    # mess.
    # XXX Find workaround
    unless (defined $mutation_checker) {
        warn "can_volgende_fase called without mutation checker, not all checks have executed.";

        $advance_result->ok('object_mutations_complete');

        return $advance_result;
    }

    $advance_result->ok('object_mutations_complete');

    for my $mutation ($self->object_data->object_mutation_lock_object_uuids) {
        try {
            $mutation_checker->($mutation);
        } catch {
            $self->log->warn(sprintf(
                'Exception during mutation validation: %s',
                $_
            ));

            $advance_result->fail('object_mutations_complete');
        };
    }

    return $advance_result;
}

=head2 handle_status_triggers

Given an L<event|Zaaksysteem::DB::Component::Logging> instance, inspects the
event and executes the associated faked 'event listeners'.

=cut

sig handle_status_triggers => 'Zaaksysteem::DB::Component::Logging';

sub handle_status_triggers {
    my ($self, $event) = @_;

    return unless $event->does(
        'Zaaksysteem::DB::Component::Logging::Case::Update::Status'
    );

    if ($event->event_data->{ status } ne 'resolved') {
        $self->_set_fase($self->milestone -1) if $self->is_closed_by_phase;
    }

    return;
}

before can_volgende_fase => sub {
    my $self = shift;
    $self->log->trace("!! Start can_volgende_fase");
};

after can_volgende_fase => sub {
    my $self = shift;
    $self->log->trace("End can_volgende_fase");
};

sub can_vorige_fase {
    my $self    = shift;

    return 1;
}

=head2 advance

Move to the next phase, or already in last phase, close case.
Then perform phase transition actions.

This sub is written to accomodate for the following scenario:
- a subcase if finished
- it signals its parent that it's finished
- the parent case is advanced to the next phase
- all default phase actions, as configured in the 'zaaktype' are fired.

=cut

define_profile advance => (
    required => {
        object_model     => 'Zaaksysteem::Object::Model',
        current_user     => 'Zaaksysteem::Schema::Subject',
    },
);

sub advance {
    my ($self, %params) = @_;
    my $args = assert_profile(\%params)->valid;

    my $advance_result = $self->can_volgende_fase(sub {
        return $args->{object_model}->validate_mutation(@_)
    });

    unless ($advance_result->can_advance) {
        throw(
            'case/advance',
            'Unable to advance case due to can_volgende_fase checks',
            $advance_result
        );
    }

    # This sets some basic "next stage" stuff, like "milestone" and the date of
    # completion (if the next stage is the last stage).

    my $rules_result = $self->execute_rules({ status => $self->milestone + 1 });

    $self->set_volgende_fase;

    if ($rules_result) {
        if ($rules_result->{send_external_system_message}) {
            my $base_url = URI->new(
                $self->result_source->schema->resultset('Config')->get('instance_base_url')
            );

            $self->send_external_system_messages(
                base_url     => $base_url,
                rules_result => $rules_result->{send_external_system_message},
            );
        }
        if ($rules_result->{wijzig_registratiedatum}) {
            my $rule = $rules_result->{wijzig_registratiedatum};
            $self->process_date_of_registration_rule( %$rule );
        }
    }

    my @items = $self->_fire_phase_actions($args->{ current_user });
    $self->ddd_update_system_attributes;
    return @items;
}


=head2 send_external_system_messages

Before advancing, see if the rule demand messages to be sent in this phase.

To test:
Create a casetype with a external message rule.
Create a case.
Create an interface.

$case->send_external_system_messages;

=cut

define_profile send_external_system_messages => (
    required => {
        base_url     => 'URI',
        rules_result => 'HashRef',
    }
);

sub send_external_system_messages {
    my ($self, %params) = @_;

    my $opts = assert_profile(\%params)->valid;

    for my $message (@{ $opts->{rules_result} }) {
        my ($module, $id) = split m[_], $message->{ type };

        my $where = {
            module => $module
        };

        if ($id) {
            $where->{ id } = $id;
        }

        # Be backward compatible with the old style:
        # 'buitenbeter' and 'api_<number>'. The code is rewritten to only
        # have the ID of the interface. This allows multiple interfaces of
        # the same kind to be used.
        if ($module =~ /^\d+$/ && !$id) {
            $where = { id => $module };
            $module = undef;
        }

        my $interfaces = $self->result_source->schema->resultset('Interface');
        my $interface = $interfaces->search_active($where)->first;

        unless (defined $interface) {
            $self->log->warn(sprintf(
                'Case was configured to trigger a %sprocess%s, but no interface could be found.',
                ($module ? "\"$module\""          : ''),
                ($id     ? " (interface \"$id\")" : ''),
            ));

            next;
        }

        $self->log->info(sprintf(
            'Processing case generic send_external_system_message trigger for "%s" (%d)',
            $interface->name,
            $interface->id
        ));

        try {
            if ( defined $module && $module eq 'api' ) {

                my $logging_data = {
                    component => $module,
                    data => {
                        interface_name      => $interface->name,
                        interface_id        => $interface->id,
                    }
                };

                $self->trigger_logging( 'case/post_message' => $logging_data );

                return $interface->process_trigger(
                    'post_message',
                    {
                        object => Zaaksysteem::API::v1::Message::Case::PhaseTransition ->new(
                            base_url => $params{base_url},
                            case_id  => $self->object_data->uuid,
                        ),
                    }
                );
            }

            if ($interface->module eq 'key2burgerzakenverhuizing') {
                return $interface->process_trigger(request_verhuizing => {
                    case => $self
                });
            }

            # Interfaces that support external system messages will need to have a
            # trigger that supports this API
            return $interface->process_trigger('PostStatusUpdate', {
                case_id    => $self->id,
                kenmerken  => $self->field_values,
                message    => $message,
                base_url   => $params{ base_url }->as_string,
                statusText => $message->{ message },
                statusCode => $message->{ status },
            });
        } catch {
            $self->log->warn(sprintf(
                'Caught exception during "%s" (%d) trigger: %s',
                $interface->name,
                $interface->id,
                $_
            ));
        };
    }
}

=head2 process_date_target_rule

Arguments: %params

Returns: none

    $zaak->process_date_target_rule(
        type   => ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WERKDAGEN},
        amount => 4,
    );

Process the "set target date" rule, and set the new "streefafhandeldatum" on the case.

=cut

define_profile process_date_target_rule => (
    required => {
        type => enum([
            ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WERKDAGEN},
            ZAAKSYSTEEM_NAMING->{TERMS_TYPE_KALENDERDAGEN},
            ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WEKEN},
            ZAAKSYSTEEM_NAMING->{TERMS_TYPE_EINDDATUM},
        ]),
        termijn => 'Int',
    }
);

sub process_date_target_rule {
    my ($self, %params) = @_;
    my $opts = assert_profile(\%params)->valid;

    my $new_end_date = calculate_term({
        start  => $self->registratiedatum,
        amount => $opts->{termijn},
        type   => $opts->{type},
    });

    $self->wijzig_streefafhandeldatum({
        streefafhandeldatum => $new_end_date
    });

    return;
}

=head2 process_date_of_registration_rule

Arguments: %PARAMS

Returns: none

    $zaak->process_date_of_registration_rule(
        bibliotheek_kenmerken_id => 44,
        recalculate => 'on'
    );

Will recalculate the registration and termination date according to a given
date kenmerk

=cut

define_profile process_date_of_registration_rule => (
    required => {
        bibliotheek_kenmerken_id => 'Int',
    },
    optional => {
        recalculate              => 'Str',
    },
);

sub process_date_of_registration_rule {
    my ($self, %params) = @_;
    my $opts = assert_profile(\%params)->valid;

    my $kenmerken = $self->field_values({
        bibliotheek_kenmerken_id => $opts->{bibliotheek_kenmerken_id}
    });

    return unless exists $kenmerken->{ $opts->{bibliotheek_kenmerken_id} };

    my ($new_date_of_registration) = @{$kenmerken->{ $opts->{bibliotheek_kenmerken_id} }};

    return unless $new_date_of_registration;

    my $old_date_of_registration = $self->registratiedatum;
    my $old_date_target          = $self->streefafhandeldatum;

    {
        # Dates are stored without prefixed zeroes. Fix that.
        my ($d, $m, $y) = split /-/, $new_date_of_registration;
        $new_date_of_registration = DateTime->new(
            year   => $y,
            month  => $m,
            day    => $d,
            hour   => $old_date_of_registration->hour,
            minute => $old_date_of_registration->minute,
            second => $old_date_of_registration->second,
            time_zone => $old_date_of_registration->time_zone,
        );
    }

    $self->wijzig_registratiedatum({
        registratiedatum => $new_date_of_registration
    });

    my %map = (
        weken => 'weeks',
        kalenderdagen => 'days'
    );
    if (defined $opts->{recalculate} && $opts->{recalculate} eq 'on') {
        my $casetype_definition = $self->zaaktype_node_id->zaaktype_definitie_id;
        my $norm_type = $casetype_definition->servicenorm_type;
        my $new_date_target;
        if ($norm_type eq ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WERKDAGEN}) {
            my $delta = diff_working_days({
                date1 => $old_date_of_registration,
                date2 => $old_date_target,
            });

            $new_date_target = add_working_days({
                datetime  => $self->registratiedatum,
                working_days => $delta,
            });

            $self->log->debug(sprintf(
                "Resetting streefafhandeldatum, based on 'business days', delta = %d, norm = '%s', node = %d",
                $delta,
                $norm_type,
                $self->get_column('zaaktype_node_id'),
            ));
        }
        elsif (my $timeframe = $map{$norm_type}) {
            my $delta = $casetype_definition->servicenorm;
            $new_date_target = $new_date_of_registration->clone->add(
                $timeframe => $delta
            );

            $self->log->debug(sprintf(
                "Resetting streefafhandeldatum, based on 'calendar %s', delta = %d, norm = '%s', node = %d",
                $timeframe,
                $delta,
                $norm_type,
                $self->get_column('zaaktype_node_id'),
            ));
        }
        else {
            $self->log->info("Unable to set streefafhandeldatum, fixed enddate");
            return;
        }

        $self->wijzig_streefafhandeldatum({
            streefafhandeldatum => $new_date_target
        });

    }

    return;
}

=head2 fire_action

Executes a single L<Zaaksysteem::Backend::Case::Action::Component> instance
in the current phase.

=cut

define_profile fire_action => (
    required => {
        action => 'Zaaksysteem::Backend::Case::Action::Component'
    },
    optional => {
        current_user => 'Zaaksysteem::Schema::Subject',
        change_only_route_fields => 'Bool'
    },
);

sub fire_action {
    my ($self, %opts) = @_;
    my $args = assert_profile(\%opts)->valid;

    my $action = $args->{ action };

    unless ($action->type) {
        throw('case/actions/type_required', sprintf(
            'Processing case action "%s" (id %s) requires an action type',
            $action->label,
            $action->id,
        ), { case_action_id => $action->id });
    }

    my %type_map = (
        email           => 'send_email',
        template        => 'create_case_document',
        case            => 'create_case_subcase',
        object_mutation => 'mutate_object',
        allocation      => 'allocate_case',
        subject         => 'add_case_subject',
    );

    my %data = %{ $action->data };
    my %metadata = (target => 'backend');

    if (exists $args->{ current_user }) {
        $metadata{subject_id} = $args->{ current_user }->id;
    }
    else {
        $metadata{disable_acl} = 1;
    }

    if (exists $args->{ change_only_route_fields }) {
        $data{ change_only_route_fields } = $args->{ change_only_route_fields };
    }

    return $self->object_data->queues->create_item(
        $type_map{ $action->type },
        {
            metadata  => \%metadata,
            object_id => $self->object_data->uuid,
            label     => $action->label,
            data      => \%data
        }
    );
}

define_profile start_subcase => (
    required => {
        action_data      => 'HashRef',
        betrokkene_model => 'Zaaksysteem::Betrokkene',
        case_model       => 'Zaaksysteem::Zaken::Model',
    },
    optional => {
        current_user => 'Zaaksysteem::Schema::Subject',
        context      => 'Zaaksysteem',
    },
);

sub start_subcase {
    my ($self, %params) = @_;
    my $args = assert_profile(\%params)->valid;

    my $c           = $args->{context};
    my $schema      = $self->result_source->schema;

    # odd, but some of the fields differ in naming (e.g. automatisch_behandelen)
    my $settings    = { %{ $args->{ action_data } } };
    my $action_data = { %{ $args->{ action_data } } };

    # these are the exceptions and the checks
    $settings->{ou_id}                      = $action_data->{ou_id} or die "need ou_id";
    $settings->{role_id}                    = $action_data->{role_id} or die "need role_id";
    $settings->{type_zaak}                  = $action_data->{relatie_type} or die "need relatie_type";
    $settings->{aanvrager_type}             = $action_data->{eigenaar_type} or die "need eigenaar_type";
    $settings->{aanvrager_id}               = $action_data->{eigenaar_id} if $action_data->{ eigenaar_type } eq 'anders';
    $settings->{actie_kopieren_kenmerken}   = $action_data->{kopieren_kenmerken};
    $settings->{zaaktype_id}                = $action_data->{relatie_zaaktype_id} or die "need relatie_zaaktype_id";
    $settings->{actie_automatisch_behandelen} = $action_data->{automatisch_behandelen};

    $action_data->{loop_protection_counter} //= 1;
    if ($action_data->{loop_protection_counter} > $LOOP_PROTECTION_MAX) {
        $self->log->info(qq{
Possible loop in subcase creation. The user triggered over
$LOOP_PROTECTION_MAX cases by a create subcase call. Aborting actions
to prevent infinite loops});
        return;
    }

    $action_data->{loop_protection_counter}++;

    # If you create an automatic subcase in the first phase (registration), and the case
    # is created without setting the behandelaar, there's a doom scenario. There's a few workarounds,
    # this one at least created the subcase. When the case gets a behandelaar, the behandelaar is responsible
    # for taking ownership of that case.
    # A better fix is to make it impossible to create this scenario, however that is a substantial change
    # in Zaaktypebeheer, and hardly feasible without the current workflow.
    if($settings->{aanvrager_type} eq 'behandelaar' && !$self->behandelaar) {
        $c->push_flash_message("Deelzaak kon niet worden aangemaakt met 'behandelaar = aanvrager' omdat behandelaar niet is ingesteld.")
            if ($c);

        $settings->{aanvrager_type} = 'aanvrager';
    }

    $settings->{onderwerp} = $self->onderwerp;

    my $current_user = $args->{current_user};
    if ($args->{ current_user }) {
        $settings->{ current_user } = $args->{ betrokkene_model }->get(
            { extern => 1, type => 'medewerker' },
            $args->{ current_user }->uidnumber
        );
    }


    my $cases = $schema->resultset('Zaak')->create_relatie(
        $self, # fishy ## noshit
        %$settings
    );

    foreach my $subcase (@$cases) {
        $self->_start_subcase(
            case_model       => $args->{case_model},
            betrokkene_model => $args->{betrokkene_model},
            sub_case         => $subcase,
            parent_case      => $self,
            action_data      => $action_data,
            current_user     => $current_user,
            settings         => $settings,
            args             => $args,
        );
    }

    $self->touch();

    # our callers are calling us in void, but that may change or not
    return 1;
}


=head2 mail_action

Perform a case mail action, either schedule or send directly.

    $case->mail_action({
        send_date => '27-01-2014',
        bibliotheek_notificaties_id => 27
        ...,
        case => $subcase,
    });

=cut

define_profile mail_action => (
    required => {
        action_data => 'HashRef',
        case => 'Zaaksysteem::Schema::Zaak', # could be a different case, e.g. subcase
    }
);

sub mail_action {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $action_data = $params->{action_data};

    my $subject;
    if (my $id = $action_data->{_subject_id}) {
        my $schema  = $params->{case}->result_source->schema;
        $subject    = $schema->resultset('Subject')->find($id);
    }

    if(my $send_date = $action_data->{send_date}) {
        my $send_date_dt = $action_data->{schedule_test} ?
            DateTime->from_epoch(epoch => $send_date) :
            DateTime::Format::ISO8601->parse_datetime($send_date);

        $self->result_source->schema->resultset('ScheduledJobs')->create_zaak_notificatie({
            bibliotheek_notificaties_id => $action_data->{bibliotheek_notificaties_id},
            zaaktype_notificatie_id => $action_data->{zaaktype_notificatie_id},
            scheduled_for   => $send_date_dt,
            recipient_type  => $action_data->{rcpt},
            behandelaar     => $action_data->{behandelaar},
            email           => $action_data->{email},
            zaak_id         => $self->id,
        });

        return "E-mail ingepland";
    }

    my $recipient = $self->notification_recipient({
        recipient_type  => $action_data->{rcpt},
        behandelaar     => $action_data->{behandelaar},
        email           => $action_data->{email},
        betrokkene_role => $action_data->{betrokkene_role},
    });

    my ($cc, $bcc);
    if ($action_data->{ cc }) {
        try {
            $cc = $self->notification_recipient({
                recipient_type => 'overig',
                email => $action_data->{ cc }
            });
        };
    }

    if ($action_data->{ bcc }) {
        try {
            $bcc = $self->notification_recipient({
                recipient_type => 'overig',
                email => $action_data->{ bcc }
            });
        };
    }

    my $attachments = $action_data->{case_document_attachments};
    my @zaaktype_kenmerken_ids = map { $_->{case_document_ids} }
        grep { $_->{selected} } @$attachments;

    $self->log->debug(sprintf(
        "Sending email from %s->%s",
        __PACKAGE__,
        'mail_action',
    ));

    my $ok = $self->mailer->send_from_case({
        recipient       => $recipient,
        cc              => $cc,
        bcc             => $bcc,
        sender_address  => $action_data->{sender_address},
        sender          => $action_data->{sender},
        subject         => $action_data->{subject},
        body            => $action_data->{body},
        attachments     => \@zaaktype_kenmerken_ids,
        sender_subject  => $subject,
        request_id      => $Zaaksysteem::Enviroment::REQUEST_ID,
    });

    return $ok ? "E-mail verstuurd" : "E-mail is niet verstuurd";
}

define_profile template_action => (
    required => {
        action_data => 'HashRef',
    },
    optional => {
        current_user => 'Zaaksysteem::Schema::Subject',
    },
);

sub template_action {
    my ($self, %params) = @_;
    my $args = assert_profile(\%params)->valid;

    my $action_data = $args->{ action_data };

    my $bibliotheek_sjablonen_id = $action_data->{bibliotheek_sjablonen_id}
        or die "need bibliotheek_sjablonen_id";

    my $sjabloon = $self->result_source->schema->resultset('BibliotheekSjablonen')->find($bibliotheek_sjablonen_id)
        or die "need sjabloon";

    my $case_sjabloon = $self->zaaktype_node_id->zaaktype_sjablonen->search({
        bibliotheek_kenmerken_id => $action_data->{bibliotheek_kenmerken_id}
    })->single;


    # Pick the action data filename, else fall back to the external
    # template name else filestore_id name
    my $name = $action_data->{filename} // $sjabloon->template_external_name // $sjabloon->filestore_id->name_without_extension;

    my %file_create_opts = (
        name          => $name,
        case          => $self,
        subject       => defined($args->{current_user})
            ? $args->{current_user}->as_object
            : $self->aanvrager_object->as_object,

        target_format => $action_data->{target_format} || $case_sjabloon->target_format,
    );

    if($action_data->{ bibliotheek_kenmerken_id }) {
        my $case_type_attribute = $self->result_source->schema->resultset('ZaaktypeKenmerken')->search(
            zaaktype_node_id => $self->get_column('zaaktype_node_id'),
            bibliotheek_kenmerken_id => $action_data->{ bibliotheek_kenmerken_id }
        )->first;

        if ($case_type_attribute) {
            $file_create_opts{ case_document_ids } = $case_type_attribute->id;
        }
    }

    if ($sjabloon->interface_id) {
        my $rv = $sjabloon->create_file_from_external_template(\%file_create_opts);

        my $result;
        if ($sjabloon->interface_id->module eq 'xential') {
            # All other status are considered errors
            my $type = 'error';
            if (ref($rv) && $rv->{status} eq 'done') {
                $type = 'pending';
            }
            elsif (ref($rv) && $rv->{status} eq 'complex_build') {
                $type = 'redirect';
            }

            $result = {
                id   => undef,
                type => $type,
                data => $rv,
            };
        }
        elsif ($sjabloon->interface_id->module eq 'stuf_dcr') {
            $result = $rv;
        }

        return $result;
    } else {
        return $sjabloon->file_create(\%file_create_opts);
    }
}

#
# For every phase a new allocation can be automatically set.
#
sub allocation_action {
    my ($self, $options) = @_;

    my $role_id = $options->{role_id}   or die "need role_id";
    my $ou_id   = $options->{ou_id}     or die "ou_id";

    # Next time? There won't be no next time
    my $volgende_fase = $self->volgende_fase;

    if (!$volgende_fase) {
        throw("ZS/Z/R/FO", "Aint no next phase, no point");
    }

    $self->wijzig_route({
        route_ou    => $ou_id,
        route_role  => $role_id,
        change_only_route_fields => $options->{ change_only_route_fields }
    });
}



=head2 notification_recipient

Determine email recipient based on case context

=cut

define_profile notification_recipient => (
    required => [qw/recipient_type/],
    optional => [qw/email behandelaar betrokkene_role/],
    constraint_methods => {
        recipient_type => sub {
            my ($dfv, $value) = @_;

            return any { $value eq $_ } qw[
                aanvrager
                behandelaar
                zaak_behandelaar
                medewerker_uuid
                medewerker
                coordinator
                gemachtigde
                betrokkene
                overig
            ];
        }
    }
);
sub notification_recipient {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $recipient_type  = $params->{recipient_type};
    my $behandelaar     = $params->{behandelaar} || '';

    if ($recipient_type eq 'behandelaar') {
        unless ((ref($behandelaar) && @$behandelaar) || $behandelaar =~ m/\-\d+$/) {
            throw(
                'case/notification_recipient/no_user_supplied',
                'Geen collega geselecteerd in e-mailsjabloon'
            );
        }
    }

    my $email = $params->{email};

    my $betrokkene_sub = sub {
        if (!ref $behandelaar) {
            # Old-style: straight behandelaar-id
            $behandelaar = [{id => $behandelaar}];
        }

        my @emails;

        for my $betrokkene (map { $_->{id} } @$behandelaar) {
            my ($betrokkene_id) = $betrokkene =~ m/^(?:betrokkene-medewerker-)?(\d+)$/;

            my $betrokkene_object = $self->result_source->schema->betrokkene_model->get({
                    extern  => 1,
                    type => 'medewerker'
                },
                $betrokkene_id
            ) or throw('case/notification_recipient/subject_not_found', "Betrokkene $betrokkene_id niet gevonden");

            push @emails, $betrokkene_object->email
                if defined($betrokkene_object->email) && length($betrokkene_object->email);
        }

        return join(";", @emails);
    };

    my $dispatch_table = {
        aanvrager       => sub { $self->aanvrager_object->email },
        behandelaar     => $betrokkene_sub,
        medewerker_uuid => $betrokkene_sub,
        medewerker      => $betrokkene_sub,
        gemachtigde     => sub {
            my @authorized = $self->pip_authorized_betrokkenen;
            return '' if !@authorized;
            return join(";", map { $_->email } @authorized);
        },
        coordinator => sub {
            my $co = $self->coordinator_object;
            return '' if !defined $co;
            return $co->email;
        },
        zaak_behandelaar => sub {
            my $behandelaar = $self->behandelaar;

            return '' unless $behandelaar;

            my $bo = $self->_load_betrokkene_object($behandelaar);

            return '' unless $bo;

            return $bo->email;
        },
        betrokkene => sub {
            my @betrokkenen = $self->get_betrokkene_objecten_by_role($params->{betrokkene_role});
            my @email_addresses = grep { length($_) } map { $_->email } @betrokkenen;
            return join(";", @email_addresses);
        },
        overig => sub {
            my $ztt = Zaaksysteem::ZTT->new;
            $ztt->add_context($self);

            return $ztt->process_template($email)->string;
        },
    };

    my $recipient = $dispatch_table->{$recipient_type}->();

    if ($recipient) {
        my @emails = grep { length $_ } split /\s*[;,]\s*/, $recipient;

        throw ("backend/email/notification/invalid_email", "Ongeldig e-mail adres: " . $recipient)
            unless all { Email::Valid->address($_) } @emails;

        return join ';', @emails;
    }
    else {
        throw ("backend/email/notification/no_email", "Geen e-mail adres voor $recipient_type");
    }

    return;
}

=head1 PRIVATE METHODS

=head2 _fire_phase_actions

Executes all active phase actions for the current phase.

=cut

sub _fire_phase_actions {
    my ($self, $subject) = @_;

    my $actions_rs = $self->case_actions_cine->current->active->sorted;

    my @queued_items;

    while(my $action = $actions_rs->next()) {
        push @queued_items, $self->fire_action(
            action => $action,
            current_user => $subject
        );
    }

    return @queued_items;
}

=head2 _set_fase

Update logic for setting a new phase (milestone).

=cut

sub _set_fase {
    my $self        = shift;
    my $milestone   = shift;

    return unless $milestone;

    $self->milestone($milestone);
    $self->flush_cache;
    $self->update;
    $self->ddd_update_system_attribute('case.phase');

    return 1;
}

sub _relate_betrokkene_to_case {
    my ($self, $case, $data, $betrokkene_model) = @_;

    $case->betrokkene_relateren({
        betrokkene_identifier  => $data->{betrokkene_id},
        magic_string_prefix    => $data->{betrokkene_prefix},
        rol                    => $data->{betrokkene_role},
        pip_authorized         => $data->{betrokkene_authorized} // 0,
        send_auth_confirmation => $data->{betrokkene_notify} // 0,
    });
    $self->_maybe_notify_authorized_subject($data, $betrokkene_model);
}

sub _start_subcase {
    my ($self, %opts) = @_;

    my $case_model       = $opts{case_model};
    my $betrokkene_model = $opts{betrokkene_model};
    my $subcase          = $opts{sub_case};
    my $parent_case      = $opts{parent_case};
    my $action_data      = $opts{action_data};
    my $current_user     = $opts{current_user};
    my $settings         = $opts{settings};
    my $args             = $opts{args};

    my $schema = $self->result_source->schema;

    if ($action_data->{betrokkene_id}) {
        $self->log->info(sprintf(
            'Relating configured subject "%s" to subcase',
            $action_data->{ betrokkene_id }
        ));
        $self->_relate_betrokkene_to_case($subcase, $action_data, $betrokkene_model);
    }

    $case_model->execute_phase_actions($subcase, {
        phase                   => 1,
        loop_protection_counter => $action_data->{loop_protection_counter}
    });

    if ($settings->{required} && $settings->{relatie_type} eq 'deelzaak') {
        $self->register_required_subcase({
            subcase_id             => $subcase->id,
            required               => $settings->{required},
        });
    }

    $subcase->_touch();

    $self->trigger_logging('case/subcase', {
        component => 'zaak',
        data      => {
            subcase_id => $subcase->id,
            type       => $action_data->{relatie_type}
        }
    });

    return 1;
}

sub _maybe_notify_authorized_subject {
    my ($self, $action_data, $betrokkene_model) = @_;

    return unless $action_data->{ betrokkene_notify };
    return unless $action_data->{ betrokkene_authorized };

    my $schema = $self->result_source->schema;

    my $template_id = $schema->resultset('Config')->get(
        'subject_pip_authorization_confirmation_template_id'
    );

    unless ($template_id) {
        $self->log->warn(
            'Subcase is configured to send notification to authorized subject, but "subject_pip_authorization_confirmation_template_id" is not configured'
        );

        return;
    }

    my $template = $schema->resultset('BibliotheekNotificaties')->find($template_id);

    unless ($template) {
        $self->log->warn(sprintf(
            'Configured "subject_pip_authorization_confirmation_template_id" (%d) could not be resolved to an (active) template.',
            $template_id
        ));

        return;
    }

    my $subject = $betrokkene_model->get_by_string($action_data->{ betrokkene_id });

    unless (defined $subject) {
        $self->log->warn(sprintf(
            'Could not resolve subject identifier "%s" to an (active) subject, not sending notification',
            $action_data->{ betrokkene_id }
        ));

        return;
    }

    unless ($subject->email) {
        $self->log->warn(sprintf(
            'Subject "%s" (%s) does not have an e-mail adress registered, not sending notification',
            $subject->display_name,
            $action_data->{ betrokkene_id }
        ));

        return;
    }

    try {
        $self->mailer->send_case_notification({
            notification => $template,
            recipient    => $subject->email
        });
    } catch {
        $self->log->warn(sprintf(
            'Sending e-mail to authorized subject for subcase failed: %s',
            $_
        ));
    };

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_OPTIONS

TODO: Fix the POD

=cut

=head2 afhandel_fase

TODO: Fix the POD

=cut

=head2 allocation_action

TODO: Fix the POD

=cut

=head2 can_vorige_fase

TODO: Fix the POD

=cut

=head2 fasen

TODO: Fix the POD

=cut

=head2 flush_cache

TODO: Fix the POD

=cut

=head2 huidige_fase

TODO: Fix the POD

=cut

=head2 is_afgehandeld

TODO: Fix the POD

=cut

=head2 is_afhandel_fase

TODO: Fix the POD

=cut

=head2 is_open

TODO: Fix the POD

=cut

=head2 is_volgende_afhandel_fase

TODO: Fix the POD

=cut

=head2 registratie_fase

TODO: Fix the POD

=cut

=head2 set_gesloten

TODO: Fix the POD

=cut

=head2 set_heropen

TODO: Fix the POD

=cut

=head2 set_volgende_fase

TODO: Fix the POD

=cut

=head2 set_vorige_fase

TODO: Fix the POD

=cut

=head2 start_subcase

TODO: Fix the POD

=cut

=head2 template_action

TODO: Fix the POD

=cut

=head2 volgende_fase

TODO: Fix the POD

=cut

=head2 vorige_fase

TODO: Fix the POD

=cut

=head2 zaak_cache

TODO: Fix the POD

=cut

