package Zaaksysteem::Zaken::Roles::ZaakSetup;

use Moose::Role;
use Data::Dumper;

use File::Copy;

use Zaaksysteem::Constants qw/
    ZAKEN_STATUSSEN
    ZAKEN_STATUSSEN_DEFAULT
    LOGGING_COMPONENT_ZAAK
    ZAAK_CREATE_PROFILE
/;
use BTTW::Tools;
use Zaaksysteem::BR::Subject;
use DateTime::Format::DateParse;

### Roles
with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Zaken::Roles::BetrokkenenSetup
    Zaaksysteem::Zaken::Roles::BagSetup
    Zaaksysteem::Zaken::Roles::KenmerkenSetup
    Zaaksysteem::Zaken::Roles::RelatieSetup
    Zaaksysteem::Roles::Timer
);

has 'z_betrokkene'   => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        shift->result_source->schema->betrokkene_model;
    }
);


{

    sub find_as_session {
        my $self            = shift;
        my $zaak            = shift;

        unless (
            $zaak &&
            (
                ref($zaak) ||
                $zaak =~ /^\d+$/
            )
        ) {
            die('Kan niet dupliceren, geen zaak of zaaknr meegegeven');
        }

        unless (ref($zaak)) {
            $zaak = $self->find($zaak)
                or die('Geen zaak gevonden met nummer: ' .  $zaak);
        }

        my $pp_profile      = Params::Profile->get_profile(
            method  => 'create_zaak'
        );

        ### _get_zaak_as_session
        my %zaak_data   = $zaak->get_columns;
        my %zaak_copy;
        for my $col (keys %zaak_data) {
            next if grep( { $_ eq $col } qw/
                id
                pid
                relates_to
                vervolg_van
                related_because
                vervolg_because
                child_because

                zaaktype_node_id

                last_modified
                created

                days_perc
                days_running
                days_left

                aanvrager
                coordinator
                behandelaar
                locatie_zaak
                locatie_correspondentie
            /);

            $zaak_copy{$col} = $zaak_data{$col};
        }

        ### _get_betrokkenen
        for my $btype (qw/behandelaar coordinator aanvrager/) {
            next unless $zaak->$btype;


            my $btype_p     = $btype . 's';

            my $get_sub     = $btype . '_object';

            next unless($zaak->$get_sub);
            $zaak_copy{$btype_p} = [{
                betrokkene_type => $zaak->$get_sub->btype,
                betrokkene      => $zaak->$get_sub->betrokkene_identifier,
                verificatie     => ($zaak->$btype->verificatie || 'medewerker'),
            }]
        }

        ### _get_locatie
        for my $locatie (qw/locatie_zaak locatie_correspondentie/) {
            next unless $zaak->$locatie;

            $zaak_copy{$locatie}    = {
                bag_type        => $zaak->$locatie->bag_type,
                bag_id          => $zaak->$locatie->bag_id,
            }
        }

        ### _get_kenmerken
        my @kenmerken;
        $zaak_copy{kenmerken} = \@kenmerken;

        my $kenmerken = $zaak->field_values();
        foreach (keys %$kenmerken) {
            push(@kenmerken, { $_ => $kenmerken->{$_} });
        }

        return \%zaak_copy;
    }

    define_profile create_zaak => %{ZAAK_CREATE_PROFILE()};
    sub create_zaak {
        my ($self, $opts, $case_model) = @_;
        $opts = assert_profile($opts)->valid;

        $self->log->debug('Create zaak called with the following options: ' . dump_terse($opts));

        my $casetype_node = $self->_validate_zaaktype($opts);

        my $schema = $self->result_source->schema;

        my %attrs = map { my ($id, $val) = each %$_; $id => $val }
            @{$opts->{kenmerken}};
        $opts->{kenmerken} = \%attrs;

        $opts->{kenmerken} = $casetype_node->zaaktype_kenmerken->mangle_defaults(
            $opts->{kenmerken}
        ) // {};

        if ($case_model && !$opts->{skip_required}) {

            my $requestor = $opts->{aanvragers}[0]{betrokkene};
            my $bm = $schema->betrokkene_model;

            $requestor = $bm->get({ intern => 1 }, $requestor);

            $case_model->assert_required_attributes(
                node       => $casetype_node,
                requestor  => $requestor,
                channel    => $opts->{contactkanaal},
                attributes => $opts->{kenmerken},
            );
        }

        my $zaak;
        $schema->txn_do(
            sub {
                $zaak = $self->_create_zaak($opts);

                $schema->resultset('Checklist')->create_from_case($zaak);
                $schema->resultset('CaseAction')->create_from_case($zaak);

                $zaak->trigger_logging(
                    'case/create',
                    {
                        component => LOGGING_COMPONENT_ZAAK,
                        data      => { case_id => $zaak->id }
                    }
                );

                $zaak->_bootstrap($opts);
            }
        );

        $zaak->discard_changes;
        return $zaak;
    }

    sub _validate_zaaktype {
        my ($self, $opts) = @_;
        my ($ztn);

        if ($opts->{zaaktype_node_id}) {
            $ztn = $self->result_source->schema->resultset('ZaaktypeNode')->find(
                $opts->{zaaktype_node_id},
                {
                    prefetch    => 'zaaktype_id'
                }
            );

            unless ($ztn) {
                throw('validate_zaaktype/node/not_found', "Cannot find zaaktype_node_id for $opts->{zaaktype_node_id}");
            }

            $opts->{zaaktype_id} = $ztn->get_column('zaaktype_id');
        } else {
            my $zt = $self->result_source->schema->resultset('Zaaktype')->find(
                $opts->{zaaktype_id},
                {
                    prefetch    => 'zaaktype_node_id'
                }
            );

            unless ($zt) {
                throw('validate_zaaktype/not_found', "Cannot find zaaktype_id for $opts->{zaaktype_id}");
            }

            $opts->{zaaktype_node_id} = $zt->get_column('zaaktype_node_id');

            $ztn = $self->result_source->schema->resultset('ZaaktypeNode')->find(
                $opts->{zaaktype_node_id}
            );
        }

        if (!$ztn->zaaktype_id->active) {
            throw('validate_zaaktype/offline', sprintf("Casetype '%s' is offline", $ztn->titel));
        }

        return $ztn;
    }

    sub _create_zaak {
        my ($self, $opts) = @_;
        my (%create_params);

        if ($opts->{registratiedatum} && !ref($opts->{registratiedatum})) {
            $opts->{registratiedatum} = DateTime::Format::DateParse->parse_datetime($opts->{registratiedatum});
        }
        $opts->{registratiedatum} ||= DateTime->now();

        $create_params{ $_ } = $opts->{ $_ }
            for $self->result_source->columns;
        # TODO: define_profile..
        # The code above defaults to undef, which results in an error because
        # confidentiality is not allowed to be undef. the database has the proper
        # default value. a more constructive solution is to avoid the above construct
        # altogether.

        ### Delete autoincrement integer, UNLESS IT IS SET and OVERRIDE
        ### zaak_nr is true
        unless ($opts->{override_zaak_id}) {
            delete($create_params{id});
        }

        $create_params{created} = $create_params{registratiedatum};

        my $zaak = $self->create(
            \%create_params
        );

        $self->log->info(
            sprintf('Created zaak with id: %d', $zaak->id)
        );

        $zaak->discard_changes;
        $zaak->object_data;
        return $zaak;
    }
}

{
    Params::Profile->register_profile(
        method  => 'create_relatie',
        profile => {
            required        => [ qw/
                zaaktype_id
                type_zaak
            /],
            'optional'      => [ qw/
                subject
                start_delay
                actie_kopieren_kenmerken
                actie_automatisch_behandelen
                role_id
                ou_id
                behandelaar_id
                behandelaar_type
                current_user
                eigenaar_role
                copy_subject_role
                subject_role
            /],
            'require_some'  => {
                'aanvrager_id_or_aanvrager_type' => [
                    1,
                    'aanvrager_id',
                    'aanvrager_type'
                ],
            },

            # this doesn't seem to have influence
            'constraint_methods'    => {
                type_zaak => qr/^gerelateerd|vervolgzaak|vervolgzaak_datum|deelzaak|wijzig_zaaktype$/,
            },
            defaults => {
                copy_subject_role => 0,
            }
        }
    );

    sub create_relatie {
        my ($self, $zaak, %opts) = @_;
        my ($nid, $aanvrager_id, $behandelaar_id);

        ### VALIDATION
        my $dv = Params::Profile->check(
            params  => \%opts,
        );

        my $valid = $dv->valid;

        throw "create_relatie: invalid options:" . Dumper($dv) unless $dv->success;
        my @aanvragers;

        my $aanvrager_type = $valid->{aanvrager_type};
        ### Aanvrager information
        if (!$aanvrager_type || $aanvrager_type eq 'anders') {
            push(@aanvragers, $valid->{aanvrager_id});
        }
        elsif ($aanvrager_type eq 'aanvrager') {
            push(@aanvragers, $zaak->aanvrager_object->betrokkene_identifier);
        }
        elsif ($aanvrager_type eq 'behandelaar') {
            unless (defined $zaak->behandelaar_object) {
                throw('case/create_relation/subject_not_found', sprintf(
                    "Attempted to create relation with originator case's (%d) assignee subject, but it could not be found",
                    $zaak->id
                ));
            }

            push(@aanvragers, $zaak->behandelaar_object->betrokkene_identifier);
        }
        elsif ($aanvrager_type eq 'ontvanger') {
            unless (defined $zaak->ontvanger_object) {
                throw('case/create_relation/subject_not_found', sprintf(
                    "Attempted to create relation with originator case's (%d) receiver subject, but it could not be found",
                    $zaak->id
                ));
            }
            push(@aanvragers, $zaak->ontvanger_object->betrokkene_identifier);
        }
        elsif ($aanvrager_type eq 'betrokkene') {
            my $role = $valid->{eigenaar_role};
            if (!defined $role) {
                throw('case/create_relation/role/set', "No role set in arguments");
            }
            my @objects = $zaak->get_betrokkene_objecten({'LOWER(rol)' => lc($role)});
            if (!@objects) {
                throw(
                    'case/create_relation/role/not_found',
                    sprintf(
                        "Unable to find relations with role %s for case %s",
                        $role, $zaak->id
                    )
                );
            }
            foreach (@objects) {
                push(@aanvragers, $_->betrokkene_identifier);
            }
        }

        # very defensive programming, should break in the previous
        # if/else block
        unless (@aanvragers) {
            throw("sub_case/creation/no_requestor",
                "Unable to create case when you don't have a requestor!");
        }


        my $behandelaars    = [];
        if ($dv->valid('behandelaar_type')) {
            if ($zaak->behandelaar_object && $dv->valid('behandelaar_type') eq 'behandelaar') {
                $behandelaar_id = $zaak->behandelaar_object->betrokkene_identifier;
            }
        } elsif ($dv->valid('behandelaar_id')) {
            $behandelaar_id = $dv->valid('behandelaar_id');
        }

        if ($behandelaar_id) {
            $behandelaars   = [{
                betrokkene          => $behandelaar_id,
                verificatie         => 'medewerker',
            }];
        }

        my $type_zaak_msg  = ucfirst($valid->{type_zaak});
        if ($type_zaak_msg =~ /Vervolgzaak/) {
            $type_zaak_msg = 'Vervolgzaak';
        }

        my $subject = $valid->{subject} || sprintf("%s van zaaknummer: %d", $type_zaak_msg, $zaak->id);

        my $registratiedatum = $self->determine_registratiedatum(
            $valid->{type_zaak},
            $valid->{start_delay},
        );

        $valid->{current_user} = $valid->{actie_automatisch_behandelen} && $valid->{current_user} ? $valid->{current_user} : undef;

        my @cases;
        foreach (@aanvragers) {
            push(
                @cases,
                $self->_create_other_case(
                    betrokkene        => $_,

                    registration_date => $registratiedatum,
                    assignee          => $behandelaars,
                    case_subject      => $subject,
                    case              => $zaak,

                    relation_type     => $valid->{type_zaak},
                    zaaktype_id       => $valid->{zaaktype_id},
                    role_id           => $valid->{role_id},
                    ou_id             => $valid->{ou_id},
                    copy_subject_role => $valid->{copy_subject_role},
                    subject_role      => $valid->{subject_role},


                    $valid->{current_user}
                        ? (assign_user => $valid->{current_user})
                        : (),
                    actie_kopieren_kenmerken =>
                        $valid->{actie_kopieren_kenmerken},
                )
            );

        }
        return \@cases;
    }
}

sub _create_other_case {
    my $self = shift;
    my $params = {@_};

    my %zaak_opts = (
        zaak_id          => $params->{case}->id,
        zaaktype_id      => $params->{zaaktype_id},
        behandelaars     => $params->{assignee},
        onderwerp        => $params->{case_subject},
        registratiedatum => $params->{registration_date},
        relatie          => $params->{relation_type},
        contactkanaal    => $params->{case}->contactkanaal,
        aanvraag_trigger => $params->{case}->aanvraag_trigger,
        route_role       => $params->{role_id},
        route_ou         => $params->{ou_id},
        aanvragers       => [
            {
                betrokkene  => $params->{betrokkene},
                verificatie => 'medewerker',
            }
        ],
        actie_kopieren_kenmerken => $params->{actie_kopieren_kenmerken},
        copy_subject_role        => $params->{copy_subject_role},
        subject_role             => $params->{subject_role},
    );

    my $nieuwe_zaak = $self->create_zaak(\%zaak_opts);
    $nieuwe_zaak->open_zaak($params->{assign_user}) if $params->{assign_user};
    return $nieuwe_zaak;
}

=head2 determine_registratiedatum

Follow-up cases (vervolgzaken) can be created with a delay of either
a set date (relatie_type: vervolgzaak_datum) or an interval in days (relatie_type: vervolgzaak).

=cut

sub determine_registratiedatum {
    my ($self, $relatie_type, $start_delay) = @_;

    $start_delay //= 0;

    if ($relatie_type eq 'vervolgzaak' && $start_delay =~ m|^\d+$|) {
        return DateTime->now->add(days => $start_delay);
    }

    if ($relatie_type eq 'vervolgzaak_datum' && $start_delay =~ m|^\d+-\d+-\d+$|) {

        my ($day, $month, $year) = $start_delay =~ m|^(\d+)-(\d+)-(\d+)$|;

        return DateTime->new(
            year    => $year,
            day     => $day,
            month   => $month,
        );
    }

    return DateTime->now();
}



sub duplicate {
    my ($self, $zaak, $opts) = @_;

    unless ($zaak && (ref($zaak) || $zaak =~ /^\d+$/)) {
        throw("ZaakSetup", 'Kan niet dupliceren, geen zaak of zaaknr meegegeven');
    }

    unless (ref($zaak)) {
        $zaak = $self->find($zaak)
            or throw('ZaakSetup', "Geen zaak gevonden met nummer: $zaak");
    }

    ### Retrieve aanvragers / kenmerken / bag data etc
    my $new_zaak_session    = $self->find_as_session($zaak);

    $opts ||= {};
    delete $new_zaak_session->{ $_ } for qw/uuid duplicate_prevention_token/;

    ### Change behandelaar to current user
    $opts->{behandelaars}   = [
        {
            betrokkene  => $self->current_user->betrokkene_identifier,
            verificatie => 'medewerker',
        }
    ];

    ### Commit new zaak
    my ($new_zaak);

    eval {
        $self->result_source->schema->txn_do(sub {
            if ($opts->{simpel}) {
                $opts->{registratiedatum}   = DateTime->now();
                $opts->{streefafhandeldatum}= undef;
            }

            $new_zaak = $self->create_zaak({
                %{$new_zaak_session},
                %{$opts}
            });

            if ($opts->{simpel}) {
                $new_zaak->milestone(1);
                $new_zaak->set_heropen;
            } else {

                $zaak->duplicate_checklist_items({target => $new_zaak});
                $zaak->duplicate_relations({target => $new_zaak});

                ### Copy files
                my $case_files  = $zaak->files->search(
                    {},
                    {
                        order_by => { '-asc' => 'id' }
                    }
                );
                $case_files->update({case_id => $new_zaak->id});

                # when changing case type, old case is deleted, new case is created.
                # on deletion nothing may be preserved, so don't copy logging either.
                unless($opts->{dont_copy_log}) {
                    ### Copy logboek
                    my $zaak_logging  = $zaak->logging->search(
                        {},
                        {
                            order_by => { '-asc' => 'id' }
                        }
                    );
                    while (my $logging = $zaak_logging->next) {
                        $logging->copy(
                            {
                                zaak_id => $new_zaak->id
                            }
                        );
                    }
                }
            }
        });
    };

    if ($@) {
        my $msg = sprintf("Failed duplicating case %d: %s", $zaak->id, $@);
        $self->log->error($msg);
        throw("Zaaksetup", $msg);
        return;
    }

    $zaak->trigger_logging('case/duplicate', { component => LOGGING_COMPONENT_ZAAK, data => {
        duplicate_id => $new_zaak->id,
        case_id => $zaak->id
    }});

    $new_zaak->trigger_logging('case/duplicated', { component => LOGGING_COMPONENT_ZAAK, data => {
        case_id => $new_zaak->id,
        duplicated_from => $zaak->id
    }});

    return $new_zaak;
}


=head2 duplicate_relations

Copy relations of case to target case

=cut

define_profile duplicate_relations => (
    required => [qw/target/]
);
sub duplicate_relations {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $target = $params->{target};
    my $rs = $self->result_source->schema->resultset('CaseRelation');

    $rs->add_relation($target->id, $_->case_id) for $rs->get_sorted($self->id);
}


=head2 duplicate_checklist_items

A case can have a checklist for each phase (why not link them to the phase?).
When duplicating a case, copy all items to the new case. The checklist is
already assumed to be duplicated.

To test:
Create two cases.
Add a checklist to the first, populate it with items.
Call this routine, after that the second case should have the same.

Much room for improvement there is.
The duplication process should be delegated to the checklist resultset.

=cut

define_profile duplicate_checklist_items => (
    required => [qw/target/]
);
sub duplicate_checklist_items {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $target = $params->{target};

    my $schema = $self->result_source->schema;
    my $checklist_rs = $schema->resultset('Checklist');
    my $checklists = $self->checklists->search;

    my $rs_items = $schema->resultset('ChecklistItem');

    while (my $checklist = $checklists->next) {

        # get the corresponding checklist_id in the new case
        my $new_checklist = $checklist_rs->search({
            case_id => $target->id,
            case_milestone => $checklist->case_milestone
        })->first;

        # the addition of new checklists happens elsewhere, right now
        # i will only account for those that have been added.
        if ($new_checklist) {
            my $new_id = $new_checklist->id;

            my @items = $rs_items->search({ checklist_id => $checklist->id })->all;

            foreach my $item (@items) {
                my $columns = {$item->get_columns};
                delete $columns->{id};
                $columns->{checklist_id} = $new_id;
                $rs_items->create($columns);
            }
        }
    }
}



sub _copy_document {
    my $self        = shift;
    my $document    = shift;
    my $nieuwe_doc  = shift;

    return 1 if $document->documents_mails->count;

    my $files_dir   = $self->config->{files} . '/documents';

    die('File not found: ' . $files_dir . '/' . $document->id)
        unless ( -f $files_dir . '/' . $document->id);

    die('Failed copying: ' . $files_dir . '/' . $document->id
       . ' TO ' . $files_dir . '/' . $nieuwe_doc->id
    ) unless copy(
        $files_dir . '/' . $document->id,
        $files_dir . '/' . $nieuwe_doc->id
    );

    return 1;
}

sub wijzig_zaaktype {
    my ($self, $zaak, $opts) = @_;

    unless($opts && $opts->{zaaktype_id}) {
        throw "wijzig_zaaktype: need zaaktype_id";
    }

    my $schema   = $self->result_source->schema;
    my $zaaktype = $schema->resultset('Zaaktype')->find($opts->{zaaktype_id}) or
        throw("change/case_type/no_valid_id/$opts->{zaaktype_id}", "Zaaktype ID is niet gevonden");

    my $node_id = $zaaktype->get_column('zaaktype_node_id');

    my $rs = $zaak->case_documents->search(undef, { prefetch => 'case_documents' });

    $self->_update_case_document_zaaktype_node_ids($schema, $rs, $node_id);

    my $old_case_type = $zaak->zaaktype_id;
    $zaak->update({
        zaaktype_node_id => $node_id,
        zaaktype_id      => $opts->{zaaktype_id},
    });

    $zaak->_bootstrap();
    $zaak->case_actions->delete;
    $zaak->set_vernietigingsdatum();

    # Re-set checklists.
    my $items = $schema->resultset('ChecklistItem')->search({
        checklist_id => {
            -in => $zaak->checklists->get_column('id')->as_query
        }
    });

    $items->delete;
    $zaak->checklists->delete;

    $zaak->zaak_kenmerken->update_fields(
        {
            force_rules       => 1,
            new_values        => {},
            zaak              => $zaak,
            ignore_log_update => 1
        }
    );

    # Cleanup out of date case->casetype embedded relation
    $zaak->object_data->object_relation_object_ids->search({
        name => 'casetype',
    })->delete;

    $schema->resultset('Checklist')->create_from_case($zaak);

    $zaak->trigger_logging(
        'case/update/case_type',
        {
            component => LOGGING_COMPONENT_ZAAK,
            data => {
                case_id       => $zaak->id,
                old_case_type => $old_case_type->id,
                casetype_id   => $zaaktype->id,
            }
        }
    );
    return $zaak;
}


sub execute_rules {
    my ($self, $options) = @_;

    ### What are we doing here...
    return if $self->is_afgehandeld;

    my $status = $options->{ status }; # optional

    my $field_values_params = {};

    $field_values_params->{ fase } = $status if $status;
    my $rules_result = $self->zaaktype_node_id->rules($options)->execute(
        {
            kenmerken       => $self->field_values($field_values_params),
            aanvrager       => $self->aanvrager_object,
            contactchannel  => $self->contactkanaal,
            payment_status  => $self->payment_status,
            casetype        => $self->zaaktype_node_id,
            confidentiality => $self->confidentiality,
            case_result     => $self->resultaat,
        }
    );

    if( my $set_case_result_action = $rules_result->{set_case_result} ) {
        try {
            my $result_type = $self->zaaktype_node_id->zaaktype_resultaten
                ->find_by_natural_index(
                    $set_case_result_action->{value}
                );

            if ($self->set_result_by_type($result_type)) {
                $self->update;
            }
        }
        catch {
            $self->log->warn(
                sprintf(
                    "Error in case %s: trying to apply rule to set result,"
                    . " with non existing (natural) index: %d. Message: '%s'",
                    $self->id, $set_case_result_action->{value}, $_,
                )
            );
        };
    }

    return $rules_result;
}

sub result_info {
    my $self = shift;

    if ($self->resultaat_id) {
        return $self->zaaktype_node_id->zaaktype_resultaten->find(
            $self->resultaat_id);
    }
    elsif ($self->resultaat) {
        return
            $self->zaaktype_node_id->zaaktype_resultaten->find_by_result_name(
            $self->resultaat);
    }

    return
}


sub notifications {
    my ($self) = @_;

    return scalar $self->zaaktype_node_id->zaaktype_notificaties->search({}, {
        prefetch => 'bibliotheek_notificaties_id',
        order_by => {'-asc' => 'me.id'},
    });
}

=head2 _update_case_document_zaaktype_node_ids

Update all case documents, to use the new C<zaaktype_kenmerk>-ids.

If the new case type does not have the C<zaaktype_kenmerk>, the label removed.

=cut

sub _update_case_document_zaaktype_node_ids {
    my $self = shift;
    my $schema = shift;
    my $rs = shift;
    my $node_id = shift;

    while (my $file = $rs->next) {
        my @cds = $file->case_documents->all;
        for my $cd (@cds) {
            my $id = $cd->case_document_id->get_column('bibliotheek_kenmerken_id');
            my $ztk = $schema->resultset('ZaaktypeKenmerken')->search_rs({
                    bibliotheek_kenmerken_id => $id,
                    zaaktype_node_id => $node_id
            })->first;
            if ($ztk) {
                $cd->update({case_document_id => $ztk->id});
            }
            else {
                $cd->delete;
            }
        }
    }

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 LOGGING_COMPONENT_ZAAK

TODO: Fix the POD

=cut

=head2 ZAAK_CREATE_PROFILE

TODO: Fix the POD

=cut

=head2 create_relatie

TODO: Fix the POD

=cut

=head2 create_zaak

TODO: Fix the POD

=cut

=head2 duplicate

TODO: Fix the POD

=cut

=head2 execute_rules

TODO: Fix the POD

=cut

=head2 find_as_session

TODO: Fix the POD

=cut

=head2 notifications

TODO: Fix the POD

=cut

=head2 result_info

TODO: Fix the POD

=cut

=head2 wijzig_zaaktype

TODO: Fix the POD

=cut

