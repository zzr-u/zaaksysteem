package Zaaksysteem::Zaken::ResultSetZaakKenmerk;
use Moose;

extends 'DBIx::Class::ResultSet';

=head1 NAME

Zaaksysteem::Zaken::ResultSetZaakKenmerk - The resultset class of ZaakKenmerk

=cut

use Array::Compare;
use BTTW::Tools;
use Clone qw/clone/;
use DateTime::Format::DateParse;
use DateTime::Format::ISO8601;
use DateTime::Format::Strptime;
use List::Util qw(any);
use Params::Profile;
use Zaaksysteem::Constants;
use Zaaksysteem::Types qw(UUID);
use Zaaksysteem::Object::Types::Location;

extends 'DBIx::Class::ResultSet';

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Roles::Timer
);

has numeric_types => (
    is      => 'ro',
    isa     => 'HashRef',
    default => sub {
        return { map { $_ => 1 }
                qw(numeric valuta valutain valutaex valutain6 valutaex6 valutain21 valutaex21)
        };
    },
    lazy => 1,
);

=head2 _format_date

Format a date value from ISO8601 to dd-mm-yyy

=cut

sub _format_date {
    my $self = shift;
    my $str = shift;

    unless ($str) {
        $self->log->trace("No value in date kenmerk");
        return "";
    }

    my $f = DateTime::Format::Strptime->new(pattern => '%d-%m-%Y');

    my $dt = try {
        $f->parse_datetime($str);
    }
    catch {
        $self->log->trace("Unable to parse $str with Strptime: $_");
        return;
    };

    if (!$dt) {
        my $i = DateTime::Format::ISO8601->new();
        $dt = try {
            $i->parse_datetime($str);
        }
        catch {
            $self->log->warn("Unable to parse $str with ISO8601: $_");
            return;
        };
    }

    if ($dt) {
        $dt->set_formatter($f);
        return $dt . "";
    }
    return "";
}


=head2 create_kenmerk

Create an attribute with a value, deprecated. And scheduled for removal.

=cut


{
    Params::Profile->register_profile(
        method  => 'create_kenmerk',
        profile => {
            required => [ qw/zaak_id bibliotheek_kenmerken_id values/],
        }
    );

    sub _sanitize_attribute {
        my $self = shift;
        my $params = {@_};

        my $schema = $self->result_source->schema;
        my $bibliotheek_kenmerk = $schema->resultset('BibliotheekKenmerken')->find($params->{bibliotheek_kenmerken_id});
        if (!$bibliotheek_kenmerk) {
            throw("sanitize_attribute/attribute/not_found",
                "Unable to find attribute with ID '$params->{bibliotheek_kenmerken_id}'"
            );
        }
        $self->_sanitize_single_attribute($bibliotheek_kenmerk, $params->{values});
    }

    sub _sanitize_single_attribute {
        my ($self, $attribute, $value) = @_;

        my ($value_type, $values) = ('', undef);
        if ($attribute) {
            $value_type = $attribute->value_type;
        }

        if ($self->numeric_types->{$value_type}) {
            $values = $attribute->filter($value);
        }
        elsif ($value_type eq 'date') {
            my $ref = ref $value;
            if ($ref eq ''|| $ref eq 'DateTime') {
                $value = [ $value ];
            }
            foreach (@{$value}) {
                push(@$values, $self->_format_date($_));
            }
        }
        else {
            $values = $value;
        }

        $values = [ $values ] if ref $values ne 'ARRAY';

        ### Make sure we only save the first entry UNLESS type_multiple="meervoudige kenmerken" or
        ### veldoptie[multiple] is set (checkboxes for instance)
        if (!$attribute || !$attribute->can_have_multiple_values) {
            $values = [ $values->[0] ];
        }

        my @values = grep { length($_) && !ref $_ } @$values;
        if (@values) {
            return \@values;
        }
        return;
    }

    sub create_kenmerk {
        my ($self, $params) = @_;
        tombstone('20190627', 'wesleys');
        $self->replace_kenmerk($params);
    }
}

=head2 upsert_kenmerken

Given a case number and map of magicstring<->value pairs, this method will
update the table to reflect the merged state of the update and existing
value rows.

    $zaak_kenmerken_rs->upsert_kenmerken($zaak_id, {
        my_magic_string => 'some value',
        woz_price => 1000.32,
        multiple_choicey => [ 'foo', 'bar', 'baz' ]
        empty_this => undef || '' || [],
    });

=cut

sig upsert_kenmerken => 'Zaaksysteem::Schema::Zaak, HashRef';

sub upsert_kenmerken {
    my $self = shift;
    my $zaak = shift;
    my $attributes = shift;

    my @upsert_attributes;
    my @cleanup_attributes;

    my @attribute_names = keys %{ $attributes };

    return unless scalar @attribute_names;

    my $rs = $self->result_source->schema->resultset("BibliotheekKenmerken");

    my %attrs = map { $_->magic_string => $_ }
        $rs->search_rs({ magic_string => { -in => \@attribute_names } })->all;

    for my $name (@attribute_names) {
        my $value = $self->_sanitize_single_attribute($attrs{$name}, $attributes->{ $name });

        push @upsert_attributes, {
            zaak_id => $zaak->id,
            name => $name,
            value => ($value ? $value : [])
        };

        push @cleanup_attributes, $name if not defined $value;
    }

    my $rows = $self->_upsert_kenmerken(@upsert_attributes);

    for my $attribute (@upsert_attributes) {
        next if exists $rows->{ $attribute->{ name } };

        $self->log->warn(sprintf(
            'Update for attribute was ignored: "%s"',
            dump_terse($attribute)
        ));
    }

    $self->_cleanup_kenmerken($zaak->id, @cleanup_attributes);

    $zaak->clear_field_values;

    return unless defined wantarray;

    my %kenmerken = map {
        $_ => $self->new_result($rows->{ $_ });
    } keys %{ $rows };

    $_->in_storage(1) for values %kenmerken;

    return \%kenmerken;
}

sub log_field_update {
    my ($self, $params) = @_;

    my $values = $params->{values} || [];
    # value_string is always defined as the grep returns an empty array
    my $value_string = join(", ", grep { defined($_) } @$values);

    my $raw_data;

    # Convert nummeraanduiding to human readable.
    if ($value_string =~ /^(nummeraanduiding|openbareruimte|woonplaats)-(\d+).*$/) {
        $raw_data = $value_string;

        $value_string = $self->get_bag_hr(@$values);
    }

    my $match_date = qr[\d{4}\-\d{2}\-\d{2}T\d{2}:\d{2}:\d{2}(?:Z|\+\d{4})];

    if ($value_string =~ m/^($match_date);($match_date);\d+$/) {
        my $date1 = $1;
        my $date2 = $2;

        my $bibliotheek_kenmerken = $self->result_source->schema->resultset('BibliotheekKenmerken');
        my $bibliotheek_kenmerk = $bibliotheek_kenmerken->find($params->{bibliotheek_kenmerken_id});

        if ($bibliotheek_kenmerk->value_type eq 'calendar') {
            $raw_data = $value_string;

            my $start_date = DateTime::Format::DateParse->parse_datetime($date1);
            my $start_time = DateTime::Format::DateParse->parse_datetime($date2);

            # Ensure local time
            $start_date->set_time_zone('Europe/Amsterdam');
            $start_time->set_time_zone('Europe/Amsterdam');

            my $fmt_date = DateTime::Format::Strptime->new(pattern => '%d-%m-%Y');
            my $fmt_time = DateTime::Format::Strptime->new(pattern => '%R');

            $value_string = sprintf(
                'Afspraak op %s om %s',
                $fmt_date->format_datetime($start_date),
                $fmt_time->format_datetime($start_time),
            );
        }
        elsif ($bibliotheek_kenmerk->value_type eq 'calendar_supersaas') {
            $raw_data = $value_string;

            my $start = DateTime::Format::DateParse->parse_datetime($date1);
            my $end   = DateTime::Format::DateParse->parse_datetime($date2);

            # Ensure local time
            $start->set_time_zone('Europe/Amsterdam');
            $end->set_time_zone('Europe/Amsterdam');

            my $fmt = DateTime::Format::Strptime->new(
                pattern => '%d-%m-%Y %R'
            );

            $value_string = sprintf(
                '%s - %s',
                $fmt->format_datetime($start),
                $fmt->format_datetime($end)
            );
        }
        # Not qmatic, AND not SuperSaaS? Then it must be a manual entry. Don't bother manipulating the value.
    }

    my $data = {
        case_id         => $params->{zaak}->id,
        attribute_id    => $params->{bibliotheek_kenmerken_id},
        attribute_value => $value_string,
        $raw_data ? (attribute_value_raw => $raw_data) : (),
    };

    my $logging = $self->result_source->schema->resultset('Logging');

    my $event = $logging->find_recent(
        {
            event_type   => 'case/attribute/update',
            component    => 'kenmerk',
            component_id => $data->{attribute_id},
            zaak_id      => $data->{case_id},
        }
    );

    if ($event) {
        $event->data($data);
        $event->restricted(1) if ($params->{zaak}->confidentiality ne 'public');
        $event->update;
    }
    else {
        $event = $params->{zaak}->trigger_logging(
            'case/attribute/update',
            {
                component    => 'kenmerk',
                data         => $data,
                component_id => $data->{attribute_id},
                zaak_id      => $data->{case_id},
            }
        );
    }
    return $event;
}

{
    Params::Profile->register_profile(
        method  => 'replace_kenmerk',
        profile => 'Zaaksysteem::Zaken::ResultSetZaakKenmerk::create_kenmerk',
    );

    sub replace_kenmerk {
        my ($self, $params) = @_;

        try {
            $self->result_source->schema->txn_do(
                sub {

                    my $values = $self->_sanitize_attribute(%$params);

                    my %args = (
                        zaak_id                  => $params->{zaak_id},
                        bibliotheek_kenmerken_id => $params->{bibliotheek_kenmerken_id},
                    );

                    my $rs    = $self->result_source->resultset();
                    my $found = $rs->search_rs(\%args)->first;

                    if ($found) {
                        if ($values) {
                            $found->update({ 'value' => $values });
                        }
                        else {
                            $found->delete;
                        }
                    }
                    else {
                        if ($values) {
                            $rs->create(
                                {
                                    %args,
                                    'value' => $values,
                                }
                            );
                        }
                        else {
                            $self->log->debug("Will not create empty attributes with undef values");
                        }
                    }
                }
            );
        }
        catch {
            $self->log->info(
                sprintf(
                    "Unable to update attributes %s: %s",
                    dump_terse($params), $_
                )
            );
            throw(
                "replace_attribute/update/error",
                "Unable to update attribute"
            );
        };
        return 1;
    }
}




{
    Params::Profile->register_profile(
        method  => 'get',
        profile => {
            required => [ qw/bibliotheek_kenmerken_id/ ]
        }
    );


    sub get {
        my ($self, $params) = @_;

        my $dv = Params::Profile->check(params  => $params);
        die "invalid options for get" unless $dv->success;

        my $bibliotheek_kenmerken_id = $params->{bibliotheek_kenmerken_id};

        my $kenmerken   = $self->search(
            {
                bibliotheek_kenmerken_id => $bibliotheek_kenmerken_id
            },
            {
                prefetch        => [
                    'bibliotheek_kenmerken_id'
                ],
            }
        );

        return $kenmerken->first();
    }
}

=head2 update_fields_authorized

    my $success = $zaak->zaak_kenmerken->update_fields_authorized(
        {
            new_values  => {
                text_value     => 'New value',
                radio_beer     => 'Heineken'
            },
            zaak        => $schema->resultset('Zaak')->find(44),
        }
    );

Will only update values when user has proper rights to edit this case.

=cut

define_profile update_fields_authorized => (
    required => {
        new_values => 'HashRef',
        zaak => 'Zaaksysteem::Zaken::ComponentZaak',
    },
    optional => {
        user => 'Any',
        ignore_log_update => 'Bool'
    },
);

sub update_fields_authorized {
    my $self        = shift;
    my $params      = assert_profile(shift)->valid;
    my $new_values  = $params->{ new_values };
    my $zaak        = $params->{ zaak };
    my $user        = ($params->{ user } || $self->result_source->schema->current_user);

    my @groups      = @{ $user->primary_groups };
    my @roles       = @{ $user->roles };

    ### 1: Retrieve all kenmerken according to given magic strings
    my @kenmerken   = $zaak->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            'bibliotheek_kenmerken_id.magic_string' => { -in => [ keys %$new_values ] },
        },
        {
            join    => 'bibliotheek_kenmerken_id',
        }
    )->all;

    ### 2: Loop over the database kenmerken, to check for valid permissions. Also, prepare
    ### the new_values hash.
    my %fields;
    for my $kenmerk (@kenmerken) {
        my $permissions     = $kenmerk->format_required_permissions;

        ### First check for propery permission on the attribute when attribute is set
        if (@$permissions) {
            my $ok;
            for my $permission (@$permissions) {
                ### Check for group
                if (!$permission->{group} || !grep { $permission->{group}->id == $_->id } @groups) {
                    next;
                }

                ### Now check for role
                if (!$permission->{role} || !grep { $permission->{role}->id == $_->id } @roles) {
                    next;
                }

                $ok = 1
            }

            if (!$ok) {
                throw(
                    'case/update_fields_authorized/field_forbidden',
                    "You do not have the proper rights to update kenmerk: " . $kenmerk->magic_string
                );
            }
        }

        ### Permissions check out, continue
        ### We do not support 'opplusbare velden' yet, deref first item.
        $fields{ $kenmerk->get_column('bibliotheek_kenmerken_id') } = $new_values->{ $kenmerk->magic_string }->[0];
    }

    return $self->update_fields(
        {
            %$params,
            new_values  => \%fields,
        }
    );
}

=head2 _process_rules_updated_fields

    $self->_process_rules_updated_fields({
        case    => $schema->resultset('Zaak')->find(24),
        values  => {
            '2280' => [
                        'Vul 3 in'
                      ],
            '2281' => [
                        'Onwijzigbaar 3'
                      ]
        },
    })

Will get all current fields in this case via C<field_values> and merges it with the given C<values>. It will
feed it to the "ruler", and make sure the "hidden fields" will be emptied in the C<$return_values>. It will
also fill values with "vul_waarde_in" when the rule engine requests it.

=cut

define_profile _process_rules_updated_fields => (
    required => {
        case   => 'Zaaksysteem::Model::DB::Zaak',
        values => 'HashRef',
    },
    optional => { set_values_except_for_attrs => 'Any', },
    defaults => { set_values_except_for_attrs => sub { return [] }, }
);

sub _process_rules_updated_fields {
    my $self    = shift;
    my $params  = assert_profile(shift || {})->valid;

    my $rules = $params->{case}->rules;

    if (!$rules->has_rules) {
        $self->log->trace("No rules to process");
        return $params->{values};
    }

    my $case_values = $params->{case}->field_values;

    my $old_values = $self->_process_rules(
        rules  => $rules,
        values => $case_values
    );

    my $values = { %$case_values, %{$params->{values} }};
    my %copy = %{$values};

    # Exclude user input fields from being updated. It makes no sense to
    # update the field according to a rule when the attribute you want
    # to save it touched by a rule.
    push(@{$params->{set_values_except_for_attrs}}, keys %{$params->{values}});

    my $new_values = $self->_process_rules(
        rules  => $rules,
        values => \%copy,
        set_values_except_for_attrs => $params->{set_values_except_for_attrs},
    );

    # Validate the results of the rule execution:
    my $result = $new_values->{'case.result'};
    $result = $result->[0] if ref $result eq 'ARRAY';
    if (
        blessed $result
        &&
        $result->isa('Zaaksysteem::Model::DB::ZaaktypeResultaten')
    ) {

        $new_values->{'case.result_obj'} = $result;
        $new_values->{'case.result_id'}  = $result->id;
        $new_values->{'case.result'}     = $result->resultaat;
    }

    # If the old values do not match the case values
    # and the new values equals the old value, the value may
    # not be changed.
    my ($old, $new, $case);
    foreach (keys %$new_values) {
        $old  = $old_values->{$_} // '';
        $new  = $new_values->{$_} // '';
        $case = $case_values->{$_};

        if ($case && $old eq $new && $old ne $case || $case && $case eq $new) {
            delete $new_values->{$_};
        }
    }

    if (!$params->{force_rules} && $self->strip_identical_fields($params->{case}, $new_values)) {
        return;
    }
    return $new_values;
}

define_profile _process_rules => (
    required    => {
        rules  => 'Zaaksysteem::Backend::Rules',
        values => 'HashRef',
    },
    optional    => {
        set_values_except_for_attrs => 'Any',
    },
    defaults => {
        set_values_except_for_attrs => sub { return [] },
    }
);

sub _process_rules {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my $rules  = $params->{rules};
    my $values = $params->{values};

    my $val_obj = $rules->validate({
        %{$rules->rules_params},
        %{$values}
    });

    return $val_obj->process_params({
        engine    => $rules,
        values    => $values,
        keyformat => 'bibliotheek_kenmerken_id',
        set_values_except_for_attrs =>
            $params->{set_values_except_for_attrs},
    });
}

define_profile update_fields => (
    required => {
        new_values => 'HashRef',
        zaak       => 'Zaaksysteem::Zaken::ComponentZaak',
    },
    optional => {
        ignore_log_update           => 'Bool',
        set_values_except_for_attrs => 'Any',
        skip_touch                  => 'Bool',
        force_rules                 => 'Bool',
    },
    defaults => {
        skip_touch        => 0,
        force_rules       => 0,
        ignore_log_update => 0,
    }
);

sub update_fields {
    my $self = shift;

    my $params = assert_profile(shift)->valid;

    my $zaak = $params->{ zaak };
    my $zaak_id = $zaak->id;

    my $new_values = $self->_process_value_types($zaak, $params->{new_values});

    # Empty updates...
    if (!$params->{force_rules} && $self->strip_identical_fields($zaak, $new_values)) {
        $self->log->debug("Nothing to update, skipping updates: rules are not enforced");
        return;
    }

    $new_values = $self->_process_rules_updated_fields(
        {
            case   => $zaak,
            values => $new_values,
            set_values_except_for_attrs =>
                $params->{set_values_except_for_attrs},
            force_rules => $params->{force_rules},
        }
    );

    $self->log->debug(
        sprintf(
            "Updating attributes %s for case %d changed by rules to: %s",
            dump_terse($params->{new_values}), $zaak_id, dump_terse($new_values)
        )
    );

    if (!$new_values) {
        $self->log->debug("Nothing to update, skipping updates");
        return;
    }

    # If there is no user, it comes from /form and this is from a create case
    # call. PIP users cannot call this function.
    my $user    = $self->result_source->schema->current_user;
    $user     //= $zaak->aanvrager_object;
    $user       = $user->as_object;

    my @events;

    my $confidential = $self->_get_new_values_str($new_values, 'case.confidentiality');
    push(@events, $zaak->ddd_update_confidentiality($user, $confidential)) if $confidential;

    my @keys = grep { $_ =~ /^[0-9]+$/ } keys %$new_values;

    my $rs = $zaak->zaaktype_node_id->zaaktype_kenmerken->search_related(
        'bibliotheek_kenmerken_id',
        { 'bibliotheek_kenmerken_id.id' => { -in => \@keys } },
        {
            columns => [
                qw(
                    bibliotheek_kenmerken_id.magic_string
                    bibliotheek_kenmerken_id.id
                )
            ],
            distinct => 1
        }
    );

    my %data;
    while (my $attr = $rs->next) {
        $data{$attr->magic_string} = $new_values->{$attr->id};
    }

    push(@events, $zaak->ddd_update_attributes($user, \%data));

    $self->_process_case_location($zaak, \%data);

    $zaak->update_referential_attributes_to_child([keys %data]);

    my $price = $self->_get_new_values_str($new_values, 'case.price');
    $zaak->ddd_update_price($user, $price) if $price;

    my $result = $self->_get_new_values_str($new_values, 'case.result_obj');

    if (!$result) {
        my $result_str = $self->_get_new_values_str($new_values, 'case.result');
        my $result_id  = $self->_get_new_values_str($new_values, 'case.result_id');

        my $rs = $zaak->zaaktype_node_id->zaaktype_resultaten;

        if ($result_id) {
            $result = $rs->find($result_id);
            throw(
                "case/set_result/result_id/not_found",
                "Unable to set the result to result_id '$result_id'",
            ) unless $result;
        }
        elsif ($result_str) {
            $result = $rs->find_by_result_name($result_str);
            throw(
                "case/set_result/result/not_found",
                "Unable to set the result to result '$result_str'",
            ) unless $result;

        }
    }

    push(@events, $zaak->ddd_update_result($user, $result)) if $result;
    $zaak->clear_field_values;

    return @events;
}

=head2 strip_identical_fields

Strip the identical fields from the input so we know if we need to update the case or not.
Returns a boolean value:

1 if the all the identical fields are stripped
0 if there are fields left for updating

=cut

sub strip_identical_fields {
    my ($self, $zaak, $new_values) = @_;

    my $values = $zaak->field_values;

    if (my $result = $zaak->resultaat_id) {
        $values->{'case.result'}     = [$result->label];
        $values->{'case.result_id'}  = [$result->id];
        $values->{'case.result_obj'} = [$result];
    }
    elsif ($zaak->resultaat) {
        $values->{'case.result'}        = [$zaak->resultaat];
    }

    $values->{'case.confidentiality'} = [$zaak->confidentiality];

    $values->{'case.price'}           = $zaak->payment_amount;

    my $compare = Array::Compare->new();

    foreach (keys %$new_values) {
        my $old_values = $values->{$_};
        my $value      = $new_values->{$_};
        # Sometimes values are not what they seem to be
        if (ref $value ne 'ARRAY') {
            $value = [ $value ];
            $new_values->{$_} = $value;
        }

        if (defined $old_values) {
            if (ref $old_values) {
                if ($compare->compare($old_values, $value)) {
                    delete $new_values->{$_};
                }
            }
            elsif ($old_values eq $value) {
                delete $new_values->{$_};
            }
        }
        # if the first element is undef and case value is also undef,
        # delete it, because.. no updates needed, same for scalar values
        elsif (!defined $value) {
            delete $new_values->{$_};
        }
        elsif (ref $value && !defined $value->[0]) {
            delete $new_values->{$_};
        }
    }

    return keys %$new_values ? 0 : 1;
}


=head2 get_bag_hr

Convert bag strings to something human readable.

=cut

sub get_bag_hr {
    my ($self) = shift;
    my @values = @_;

    my @hr;
    my $value_string;
    for my $bag_id (@values) {
        my ($type, $id) = split /-/, $bag_id;

        my $result;
        if ($type && $id) {
            $result = $self->result_source->schema->bag_model->get($type => $id);
        }

        if ($result) {
            push @hr, $result->to_string;
        } else {
            push @hr, "Onbekend BAG-object: '$bag_id'";
        }
    }

    $value_string = join("; ", grep { defined($_) } @hr);

    return $value_string;
}


=head2 delete_fields

The use case for this method is fields that are being hidden by rules.
We receive a list of field_ids (bibliotheek_kenmerken) and we look
if any values are stored. If so, these are deleted, and the fields that
are actually affected are gathered and put in a log.

=cut

sub delete_fields {
    my ($self, $arguments) = @_;

    tombstone('20190627', 'wesleys');

    my $bibliotheek_kenmerken_ids = $arguments->{bibliotheek_kenmerken_ids} or die "need bibliotheek_kenmerken_ids";

    throw('delete_fields/missing_argument', 'ZaakKenmerk::delete_fields needs a "zaak" argument')
        unless $arguments->{zaak};

    my $zaak_id = $arguments->{zaak}->id;

    if ($self->log->is_trace) {
        $self->log->trace("Hiding fields in case $zaak_id " . join ", ", @$bibliotheek_kenmerken_ids);
    }

    my $current = $self->search({
        zaak_id => $zaak_id, # ensure only this case is affected
        bibliotheek_kenmerken_id => {
            '-in' => $bibliotheek_kenmerken_ids
        }
    });

    # if there are no values stored that need deletion, we're done. nothing to do here
    my @current = $current->all
        or return;

    # a list with removed attributes. because checkboxes are stored as separate values
    # abuse a hash to ensure uniqueness.
    # using get_column to avoid subqueries - performance tweak
    my $deleted = { map { $_->get_column('bibliotheek_kenmerken_id') => 1 } @current };

    $current->delete;

    my $schema = $self->result_source->schema;

    my $logging = $schema->resultset('Logging');
    my $bibliotheek_kenmerken = $schema->resultset('BibliotheekKenmerken');

    # get the names of the deceased attributes
    my $attributes = join ", ", map { $_->naam } $bibliotheek_kenmerken->search({
        id => {
            '-in' => [keys %$deleted]
        }
    })->all;

    # if you delete a bunch of fields, the logging line get too long
    # we need to solve this in the logging module, danger, danger
    $attributes = substr($attributes, 0, 100) . '...' if length $attributes > 100;

    $arguments->{zaak}->trigger_logging('case/attribute/removebulk', {
        component => 'kenmerk',
        data => {
            attributes => $attributes,
            reason => 'verborgen door regels'
        }
    });
}

define_profile _process_appointment_fields => (
    required => {
        values => 'HashRef',
        case   => 'Zaaksysteem::Zaken::ComponentZaak'
    },
);

sub _process_appointment_fields {
    my ($self, $rs, $values) = @_ ;

    while(my $attribute = $rs->next) {
        my $id = $attribute->get_column('bibliotheek_kenmerken_id');
        my $val = $values->{$id};
        if (ref $val eq 'ARRAY') {
            foreach (@$val) {
                if (defined $_ && $_ ne '' && !UUID->check($_)) {
                    throw('case/attribute/appointment/invalid_value', "'$_' is not a valid UUID");
                }
            }
        }
        elsif (defined $val && $_ ne '' && !UUID->check($val)) {
            throw('case/attribute/appointment/invalid_value', "'$val' is not a valid UUID");
        }
    }
}

sub _process_value_types {
    my ($self, $case, $values) = @_;

    my @keys = grep(/^\d+$/, keys %{$values});

    my $rs = $case->zaaktype_node_id->zaaktype_kenmerken->search(
        { 'bibliotheek_kenmerken_id.id' => { -in => \@keys } },
        { prefetch => 'bibliotheek_kenmerken_id' }
    );

    $self->_process_uppercase_fields(
            $rs->search_rs({ 'bibliotheek_kenmerken_id.value_type' => 'text_uc' }),
            $values
    );
    $self->_process_appointment_fields(
        $rs->search_rs({ 'bibliotheek_kenmerken_id.value_type' => 'appointment' }),
        $values
    );
    return $values;
}

sub _process_uppercase_fields {
    my ($self, $rs, $values) = @_ ;

    while(my $attribute = $rs->next) {
        my $id = $attribute->get_column('bibliotheek_kenmerken_id');
        my $val = $values->{$id};
        if (ref $val eq 'ARRAY') {
            $val = [ map { uc($_) } @$val ];
        }
        else {
            $val = uc($val);
        }
        $values->{$id} = $val;
    }

}

sub _process_case_location {
    my ($self, $case, $data) = @_ ;

    my $schema = $case->result_source->schema;
    my $case_location_jsonb = $schema->encode_jsonb({ map_case_location => "1" });

    # Sanitize all data
    my %values;
    foreach (keys %$data) {
        my $v = $data->{$_};
        # location values don't have multiples
        my $value = ref $v eq 'ARRAY' ? $v->[0] : $v;
        # falsy values are skipped
        next unless $value;
        $values{$_} = $value;
    }
    return unless %values;

    my @magic_strings = keys %values;

    my $rs = $case->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            'bibliotheek_kenmerken_id.magic_string' => { -in => \@magic_strings },
            '-or' => [
                {
                    'bibliotheek_kenmerken_id.value_type' => {
                        '-in' => [
                            qw(
                                bag_adres
                                bag_adressen
                                bag_openbareruimte
                                bag_openbareruimtes
                                bag_straat_adres
                                bag_straat_adressen
                                googlemaps
                            )
                        ]
                    },
                    'me.bag_zaakadres' => 1,
                },
                {
                    'bibliotheek_kenmerken_id.value_type' => 'geolatlon',
                    'me.properties::jsonb' => { '@>' => \$case_location_jsonb },
                },
            ],
        },
        { prefetch => 'bibliotheek_kenmerken_id' }
    );

    while (my $zt_kenmerk = $rs->next) {
        my $attribute = $zt_kenmerk->bibliotheek_kenmerken_id;
        my $value = $values{ $attribute->magic_string };

        my $old_bag = $case->locatie_zaak;

        my $type = $attribute->value_type;

        my $zaak_bag;
        if ($type eq 'googlemaps') {
            $zaak_bag = $self->_create_zaak_bag_googlemaps($case->id, $value);
            $case->update_zaak_location($zaak_bag);
        }
        elsif ($type eq 'geolatlon') {
            my ($lat, $lon) = split m[,], $value;

            my $location = Zaaksysteem::Object::Types::Location->new(
                latitude => $lat,
                longitude => $lon
            );
            $case->update_location($location);
        }
        else {
            $zaak_bag = $self->_create_zaak_bag($case->id, $value);
            $case->update_zaak_location($zaak_bag);
        }

    }

    return;
}

sub _lookup_nearest_bag_object {
    my $self = shift;
    my $value = shift;

    my $schema = $self->result_source->schema;
    my $bag_model = $schema->bag_model;

    my $bag_query = $bag_model->parse_search_term($value);
    return unless $bag_query;

    my $bag_object = try {
        $bag_model->get_exact(%$bag_query);
    } catch {
        $self->log->error("Error retrieving BAG entry: '$_'");
        return;
    };

    return $bag_object;
}

sub _create_zaak_bag_googlemaps {
    my $self    = shift;
    my $case_id = shift;
    my $value   = shift;

    my $bag_object = try {
        $self->_lookup_nearest_bag_object($value);
    } catch {
        $self->log->debug("No nearest BAG object found for '$value' - $_");
        return;
    };
    return if not $bag_object;

    return $self->_create_zaak_bag(
        $case_id,
        $bag_object->nummeraanduiding_id,
    ); 
}

sub _create_zaak_bag {
    my $self    = shift;
    my $case_id = shift;
    my $value   = shift;

    return if not defined $value;

    my @ALLOWED = qw/nummeraanduiding openbareruimte/;
    my ($bag_type, $bag_id) = $value =~ m/^(\w+)-(\d+)$/;

    return unless $bag_type && $bag_id && any { $bag_type eq $_ } @ALLOWED;

    my $table_reference = "bag_${bag_type}_id";
    my $zaakbag = $self->result_source->schema->resultset('ZaakBag')->create_bag(
        {
            zaak_id          => $case_id,
            bag_type         => $bag_type,
            $table_reference => $bag_id,
            bag_id           => $bag_id,
        }
    );

    return $zaakbag;
}

sub _cleanup_kenmerken {
    my ($self, $zaak_id, @kenmerken) = @_;

    return unless $zaak_id;
    return unless scalar @kenmerken;

    $self->search(
        {
            zaak_id => $zaak_id,
            'bibliotheek_kenmerken_id.magic_string' => { -in => \@kenmerken }
        },
        {
            join => 'bibliotheek_kenmerken_id'
        }
    )->delete;

    return;
}

sub _upsert_kenmerken {
    my ($self, @kenmerken) = @_;

    my $source = $self->result_source;
    my $maker = $source->schema->storage->sql_maker;

    my @value_sql;
    my @value_bind;

    # Manually unpack attributes to feed into _insert_value, since
    # _insert_valueS always puts the keyword 'VALUES' in front of the
    # placeholder list
    # This loop produces @value_sql = [ \'( ?, ?, ... )', ... ] and
    # corresponding binds
    for my $attribute (@kenmerken) {
        my @attribute_sql;

        for my $column (sort keys %{ $attribute }) {
            my ($sql, @bind) = $maker->_insert_value(
                $column,
                $attribute->{ $column }
            );

            push @attribute_sql, $sql;
            push @value_bind, @bind;
        }

        push @value_sql, sprintf('( %s )', join(', ', @attribute_sql));
    }

    # Procuces the column definition for our CTE in the order SQL::Abstract
    # adheres to (sort is here for explicitly signifying that).
    my @upsert_columns = map { \$_ } sort qw[zaak_id name value];
    my @kenmerk_columns = map { \$_ } qw[
        zaak_id
        bibliotheek_kenmerken_id
        value
    ];

    my @join_columns = qw{
        upsert_rows.zaak_id::int
        bibliotheek_kenmerken.id::int
        upsert_rows.value::text[]
    };

    # Produces 'WITH upsert_rows (col1, col2, colN)
    my ($with_sql, @with_bind) = $maker->generate(
        'with',
        \'upsert_rows',
        \@upsert_columns
    );

    # Produces 'VALUES ( ?, ?, ...), ( ... ), ...'
    my ($values_sql) = $maker->generate('values', \join(', ', @value_sql));

    # 'AS ( VALUES ( ?, ?, ...), ( ... ), ...'
    my ($table_sql, @table_bind) = $maker->generate('as', [ \$values_sql ]);

    # Bind the WITH and AS SQL
    my $cte_sql = join ' ', ($with_sql, $table_sql);
    my @bind = (@with_bind, @table_bind, @value_bind);

    my @inference_columns = map { \$_ } $source->unique_constraint_columns(
        'zaak_kenmerk_bibliotheek_kenmerken_id'
    );

    my ($insert_sql) = $maker->generate(
        'insert into',
        \$source->name,
        \@kenmerk_columns
    );

    my ($select_sql) = $maker->generate(
        'select distinct',
        \join(', ', @join_columns),
        'from',
        \'upsert_rows'
    );

    my $join_sql = <<EOS;
JOIN zaak ON zaak.id = upsert_rows.zaak_id::int
JOIN bibliotheek_kenmerken ON upsert_rows.name = bibliotheek_kenmerken.magic_string
JOIN zaaktype_kenmerken ON zaaktype_kenmerken.zaaktype_node_id = zaak.zaaktype_node_id
                       AND zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id
EOS

    my ($conflict_sql) = $maker->generate(
        'on conflict',
        \@inference_columns,
        'do update set'
    );

    my ($set_sql) = $maker->_update_set_values({
        value => \'EXCLUDED.value'
    });

    my $returning_sql = <<EOS;
RETURNING *, (
    SELECT magic_string AS name
    FROM bibliotheek_kenmerken
    WHERE bibliotheek_kenmerken.id = bibliotheek_kenmerken_id
)
EOS

    my $sql = join "\n", (
        $cte_sql,
        $insert_sql,
        $select_sql,
        $join_sql,
        $conflict_sql,
        $set_sql,
        $returning_sql
    );

    my $retval;

    $source->storage->dbh_do(sub {
        my ($storage, $dbh) = @_;

        my @unbind = map { $_->[ 1 ] } @bind;

        $retval = $dbh->selectall_hashref($sql, 'name', undef, @unbind);

        # Remove name field so we can cleanly new_result() later, it is
        # preserved using the keys of the returned hash.
        delete $_->{ name } for values %{ $retval };
    });

    return $retval;
}

sub _get_new_values_str {
    my ($self, $new_values, $key) = @_;

    return unless exists $new_values->{$key};
    my $value = delete $new_values->{$key};
    return ref $value eq 'ARRAY' ? $value->[0] : $value;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 get

TODO: Fix the POD

=cut

=head2 log_field_create

TODO: Fix the POD

=cut

=head2 log_field_update

TODO: Fix the POD

=cut

=head2 replace_kenmerk

TODO: Fix the POD

=cut

=head2 update_field

TODO: Fix the POD

=cut

=head2 update_fields

TODO: Fix the POD

=cut

