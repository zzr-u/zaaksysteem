use utf8;

package Zaaksysteem::Controller::Plugins::PIP;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::Controller' }
with qw(
    Zaaksysteem::Roles::Controller::Auth::Public
);

use BTTW::Tools;
use File::stat;
use List::Util qw[first];
use Zaaksysteem::BR::Controlpanel;
use Zaaksysteem::Constants qw/RGBZ_LANDCODES/;
use Zaaksysteem::Zaken::Subcase;
use List::Util qw(none any);

sub _clear_session {
    my ($self, $c, $msg) = @_;
    $c->log->debug($msg);
    delete $c->session->{pip};
    return;
}

sub is_pip_logged_in {
    my $self = shift;
    my $c    = shift;


    unless ($c->session->{pip}) {
        return $self->_clear_session($c, "No PIP session found");
    }

    my $requestor = $c->session->{pip}{ztc_aanvrager} // '';

    if ($requestor =~ /^betrokkene-natuurlijk_persoon-/) {
        return 1 if $c->session->{_saml}{success};
        return 1 if $c->session->{_twofactor}{authenticated_id} && $c->session->{_twofactor}{subject_type} eq 'person';
    }
    elsif ($requestor =~ /^betrokkene-bedrijf-/) {
        return 1 if $c->model('Plugins::Bedrijfid')->succes;
        return 1 if $c->session->{_saml}{success} && $c->session->{_saml}{used_profile} =~ m[eherkenning|spoof];
        return 1 if $c->session->{_twofactor}{authenticated_id} && $c->session->{_twofactor}{subject_type} eq 'company';
    }
    return $self->_clear_session($c, "Invalid PIP session");
}


sub base : Chained('/') : PathPart('pip'): CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{pip}                  = 1;
    $c->session->{multi_tenant_state} = {};
    my $interfaces = $c->model('DB::Interface');

    my $saml_state = $c->session->{ _saml } || {};

    if ($c->req->params->{ template }) {
        $c->session->{ multi_tenant_state }{ template } = $c->req->params->{ template };
    }

    if (!$self->is_pip_logged_in($c)) {
        if ($c->req->action !~ /pip\/login/) {
            $c->response->redirect($c->uri_for('/pip/login', $c->session->{multi_tenant_state}));
            $c->detach;
        }
        return;
    }

    # All the stuf below is only needed when we have a betrokkene, otherwise
    # skip it
    $c->stash->{betrokkene} = $c->model('Betrokkene')->get(
        {},
        $c->session->{pip}{ztc_aanvrager}
    );
    return unless $c->stash->{betrokkene};

    $c->stash->{show_controlpanel} = 0;
    $c->stash->{edit_controlpanel} = 0;
    $c->stash->{show_alt_auth}     = 0;

    $c->stash->{show_woz} = $interfaces->search_active({ module => 'woz' })->count ? 1 : 0;
    $c->stash->{gws4all_interface} = $interfaces->find_by_module_name('gws4all');


    if ($c->stash->{ betrokkene }) {
        my $datab = first {
            $_->jpath('$.subject_type') eq $c->stash->{ betrokkene }->btype
        } $c->model('DB::Interface')->search_active({ module => 'multichannel' });

        $c->stash->{ show_woz_datab } = defined $datab;
        $c->stash->{ datab_interface } = $datab;
    }

    my $two_fa = $interfaces->find_by_module_name('auth_twofactor');

    if ($c->stash->{betrokkene}->isa('Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon')) {
        if ($two_fa) {
            my $enabled = $two_fa->get_interface_config->{enable_for};
            if (any { $enabled eq $_ } qw(person both)) {
                $c->stash->{show_alt_auth}       = 1;
                $c->stash->{twofactor_interface} = $two_fa;
            }
        }

    }
    else {
        my $cp = $c->model('DB::Interface')->search_active({ module => 'controlpanel'})->first;
        if ($cp && $cp->get_interface_config->{pip}) {
            $c->stash->{"show_controlpanel"} = 1;
            $c->stash->{"edit_controlpanel"} = $cp->get_interface_config->{pip_write} // 0;

            my $br = Zaaksysteem::BR::Controlpanel->new_from_betrokkene(
                schema        => $c->model('DB')->schema,
                betrokkene_id => $c->session->{pip}{ztc_aanvrager},
            );
            if ($br->controlpanel->read_only) {
                $c->stash->{"edit_controlpanel"} = 0;
            }
        }

        if ($two_fa) {
            my $enabled = $two_fa->get_interface_config->{enable_for};
            if (any { $enabled eq $_ } qw(company both)) {
                $c->stash->{show_alt_auth}       = 1;
                $c->stash->{twofactor_interface} = $two_fa;
            }
        }
    }

    $c->stash->{pip_session} = 1;
    $c->stash->{template_layout} = 'plugins/pip/layouts/pip.tt';
}

=head2 zaak_base

Abstract.

Load zaak from database. if fishy, redirect to main page.
Then load phase, execute rules.

=cut

sub zaak_base : Chained('base') : PathPart('zaak'): CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    my $zaak = $c->stash->{zaak} = $c->model('DB::Zaak')->find($id);

    my $access_check = sub {
        return unless defined $zaak;
        return if $zaak->is_deleted;
        return if $zaak->zaaktype_node_id->prevent_pip;

        my (undef, $type, $id) = split m[\-], $c->session->{ pip }{ ztc_aanvrager };

        return unless defined $zaak->aanvrager && $zaak->aanvrager_object;
        return unless $zaak->aanvrager_object->betrokkene_identifier eq $c->session->{ pip }{ ztc_aanvrager } ||
                      $zaak->zaak_betrokkenen->search({
                          pip_authorized => 1,
                          betrokkene_type => $type,
                          gegevens_magazijn_id => $id
                      })->count;

        return 1;
    };

    unless($access_check->()) {
        $c->log->warn(sprintf(
            'Possible out-of-bounds access violation, user %s attempted to access case %d',
            $c->session->{ pip }{ ztc_aanvrager },
            $id
        ));

        $c->res->redirect($c->uri_for(
            '/pip',
            $c->session->{ multi_tenant_state }
        ));

        $c->detach;
    }

    ### Find fase
    my $fase = $c->req->params->{fase};
    if ($fase && $fase =~ /^\d+$/) {
        $c->stash->{requested_fase} = $zaak->zaaktype_node_id->zaaktype_statussen->search({ status  => $fase})->first;
    } else {
        $c->stash->{requested_fase} = $zaak->volgende_fase || $zaak->huidige_fase;
    }

    $c->stash->{is_array} = sub { my $argument = shift; return ref $argument && ref $argument eq 'ARRAY' };

    $self->show_subcase_in_pip($c);

    $c->forward('/zaak/_execute_regels');
}


sub zaak : Chained('zaak_base') : PathPart(''): Args() {
    my ($self, $c) = @_;

    $c->stash->{template_available} = $c->model('DB::Config')->get('feedback_email_template_id') ? 1 : 0;
    $c->stash->{aanvrager_email}    = $c->stash->{zaak}->aanvrager_object->email;

    $c->stash->{rule_engine}        = sub {
        my $opts    = shift || {};
        my $case    = shift;

        my $rv      = Zaaksysteem::Backend::Rules->generate_object_params(
            {
                'case' => $case,
                %{ $opts }
            }
        );

        if ($rv->{rules}) {
            ### Loop over statussen

            my @statussen = $c->stash->{zaak}->zaaktype_node_id->zaaktype_statussen->search(
                {}, {order_by => 'id'},
            );

            if (@statussen) {
                $rv->{active_attributes_per_status} = {};
            }

            for my $status (@statussen) {
                my $validation = $rv->{rules}->validate_from_case($case, { 'case.number_status' => $status->status });

                $rv->{active_attributes_per_status}->{$status->status} = $validation->active_attributes;
                $rv->{active_text_blocks_per_status}->{$status->status} = $validation->active_text_blocks;
                $rv->{hidden_groups} = $validation->hidden_groups;
            }

            # $rv->{active_attributes} = $validation->active_attributes;
        }

        return $rv;
    };

    $c->stash->{template} = 'plugins/pip/case/view.tt';
    $c->stash->{ page_title } = $c->stash->{ zaak }->zaaktype_node_id->titel
}



sub view_element : Chained('zaak_base'): PathPart('view_element'): Args(1) {
    my ($self, $c, $element) = @_;

    $c->forward('/zaak/view_element', [ $element ]);
}

sub overview : Chained('zaak') : PathPart(''): Args() {
    my ($self, $c) = @_;



    $c->stash->{template} = 'plugins/pip/overview.tt';
}


sub index : Chained('base') : PathPart(''): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{current_page} = 'zaken';
    $c->stash->{template}     = 'plugins/pip/index.tt';

    $c->stash->{onafgeronde_zaken} = $c->model('DB::ZaakOnafgerond')->search({
        betrokkene => $c->session->{pip}->{ztc_aanvrager},
    },
    {
        page                    => 1,
        rows                    => 10000, # template needs a pager :)
        order_by => { -desc => 'create_unixtime' }
    });

    $c->stash->{display_fields_onafgerond} = [qw/titel/];

    my %args = (
        page => 1,
        rows => 10000,    # template needs a pager :)
        betrokkene_type      => $c->stash->{betrokkene}->btype,
        gegevens_magazijn_id => $c->stash->{betrokkene}->ex_id,
        type_zaken           => 'open',
    );

    $c->stash->{zaken} = $c->model('Zaken')->zaken_pip({
        %args,
        as_aanvrager => 1
    });

    $c->stash->{afgehandelde_zaken} = $c->model('Zaken')->zaken_pip({
        %args,
        type_zaken   => [qw[resolved]],
        as_aanvrager => 1,
    });

    $c->stash->{gemachtigde_zaken} = $c->model('Zaken')->zaken_pip({
        %args,
        as_betrokkene => 1
    });

    $c->stash->{gemachtigde_afgehandelde_zaken} = $c->model('zaken')->zaken_pip({
        %args,
        type_zaken    => [qw[resolved]],
        as_betrokkene => 1,
    });

    $c->stash->{display_fields} = $c->model('SearchQuery')->get_display_fields({pip => 1});
}

sub alternative_auth : Chained('base') : PathPart('altauth'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{current_page} = 'altauth';
    $c->stash->{template}     = 'plugins/pip/alternative_auth.tt';
    $c->stash->{page_title}   = 'Authenticatiegegevens';

    my $params = $c->req->params;
    return unless %$params;

    my $model = $c->model('Auth::Alternative');
    my $subject = $model->find_subject_by_betrokkene($c->stash->{betrokkene});

    $params->{email} //= $params->{username};

    $model->update_subject($subject, $params);
}


sub contact : Chained('base') : PathPart('contact'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{current_page} = 'contact';

    my $res = $c->stash->{betrokkene} // $c->model('Betrokkene')->get(
        {},
        $c->session->{pip}->{ztc_aanvrager}
    );

    if (exists($c->req->params->{update})) {
        $res->mobiel($c->req->params->{'npc-mobiel'});
        $res->email($c->req->params->{'npc-email'});
        $res->telefoonnummer($c->req->params->{'npc-telefoonnummer'});
    }

    $c->stash->{'betrokkene'} = $res;

    $c->stash->{template} = 'plugins/pip/contact.tt';
    $c->stash->{ page_title } = 'Mijn gegevens';
    $c->stash->{landcodes} = $self->landcodes($c);
}

sub instances : Chained('base') : PathPart('instances'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{current_page} = 'instances';

    $c->stash->{template} = 'plugins/pip/instances.tt';
}

=head2 uitkeringen

Show PIP uitkeringen

=cut

sub uitkeringen : Chained('base') : PathPart('uitkeringen'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{current_page} = 'uitkeringen';

    $c->stash->{template} = 'plugins/pip/uitkeringen.tt';
}

sub login : Chained('base') : PathPart('login'): Args() {
    my ($self, $c, $type) = @_;
    my ($bsn, $kvknummer, $samlid);

    $c->session->{pip_login} = 1;

    my $saml_state = $c->session->{ _saml } || {};
    my $multi_tenant_state = $c->session->{ multi_tenant_state } || {};

    my $redirect_args = {
        %{ $multi_tenant_state },
        failure_endpoint => $c->uri_for('/pip/login', $multi_tenant_state),
    };

    my $twofactor = $c->model('DB::Interface')->search_active({ module => 'auth_twofactor' })->first;

    ### Type natuurlijk_persoon or bedrijf
    if (!$type) {

        # Move this so we can use two-factor with eherkenning, digid and eidas
        if ($twofactor && defined($twofactor->jpath('$.enable_for'))) {
            $redirect_args->{ success_endpoint } = $c->uri_for('/pip/login/twofactor', $multi_tenant_state);

            $c->session->{pip_login} = 1;

            $c->res->redirect($c->uri_for('/auth/twofactor', $redirect_args));
            $c->detach;
        }

        $c->stash->{template} = 'plugins/pip/layouts/pip.tt';
        $c->stash->{ page_title } = 'Inloggen';
        $c->stash->{login_type_page} = 1;

        if ($c->session->{_saml_error}) {
            $c->stash->{saml_error} = $c->session->{_saml_error};
        }

        ### Zorg voor een schone start
        $c->logout;
        $c->delete_session;

        # Setup IDPs
        $c->stash->{ citizen_idps } = [ $c->model('DB::Interface')->search_module('samlidp', '$.login_type_citizen') ];
        $c->stash->{ company_idps } = [ $c->model('DB::Interface')->search_module('samlidp', '$.login_type_company') ];

        $c->detach;
    }
    if (none { $type eq $_ } qw(bedrijf natuurlijk_persoon twofactor)) {
        $c->res->redirect($c->uri_for('/pip', $redirect_args));
        $c->detach;
    }

    if (   !$c->session->{pip}
            && !$saml_state->{success}
            && !$c->session->{_twofactor}{authenticated}
            && !$c->model('Plugins::Bedrijfid')->succes)
        {

        if ($type eq 'natuurlijk_persoon') {
            my $idp_id = '';
            if ($c->req->param('idp_id') =~ m/\A([0-9]+)\z/) {
                $idp_id = $1;
            }

            $redirect_args->{ success_endpoint } = $c->uri_for(
                '/pip/login/natuurlijk_persoon',
                $multi_tenant_state
            );

            $c->res->redirect($c->uri_for("/auth/saml/$idp_id", $redirect_args));
            $c->detach;
        }
        elsif ($type eq 'bedrijf') {
            $redirect_args->{ success_endpoint } = $c->uri_for(
                '/pip/login/bedrijf',
                $multi_tenant_state
            );

            $c->res->redirect($c->uri_for('/auth/bedrijfid', $redirect_args));
            $c->res->body('');
            $c->detach;
        }
        elsif ($type eq 'twofactor') {
            $redirect_args->{ success_endpoint } = $c->uri_for(
                '/pip/login/twofactor',
                $multi_tenant_state
            );


            $c->res->redirect(
                $c->uri_for('/auth/twofactor', $redirect_args)
            );
            $c->res->body('');
            $c->detach;
        }
    }

    if ($type eq 'natuurlijk_persoon') {
        if ($saml_state->{used_profile} eq 'eidas') {
            $samlid = $saml_state->{nameid};
        }
        else {
            $bsn = $saml_state->{uid};
        }
    }
    elsif ($type eq 'bedrijf') {
        if ($c->model('Plugins::Bedrijfid')->succes) {
            $kvknummer = $c->model('Plugins::Bedrijfid')->login;
        }
        elsif ($saml_state->{success}) {
            $samlid = $saml_state->{uid};
        }
    }
    elsif ($type eq 'twofactor') {
        if ($c->session->{_twofactor}{subject_type} eq 'company') {

            # It's not really SAML, but it's KvK + vestigingsnummer in 1 value.
            $samlid = $c->session->{_twofactor}{authenticated_id};
            $type = 'bedrijf';
        }
        else {
            $type = 'natuurlijk_persoon';
            $bsn  = $c->session->{_twofactor}{authenticated_id};
        }
    }

    # Logged in, set our session
    $c->session->{pip} = { logged_in => 1 };

    my $betrokkene = $self->get_betrokkene_from_authentication_method(
        $c,
        type => $type eq 'bedrijf' ? 'niet_natuurlijk_persoon' : $type,
        id => (
            $saml_state->{uid} // $c->session->{_twofactor}{authenticated_id}
                // $c->session->{_bedrijfid}{login}
        ),
        auth_module => ($saml_state->{used_profile} // 'twofactor'),
        saml_data   => $saml_state,
    );

    if ($betrokkene && $betrokkene->gmid) {
        $c->session->{pip}{ztc_aanvrager} = $betrokkene->betrokkene_identifier;
        $c->session->{pip}{user_uuid} = $betrokkene->uuid;
        $c->response->redirect($c->uri_for('/pip', $redirect_args));
        $c->detach;
    }

    if ($type eq 'natuurlijk_persoon') {
        $c->session->{digid_error} = q{U bent succesvol aangemeld, maar
        helaas kunnen wij geen zaken vinden in ons systeem. Om
        veiligheidsredenen bent u uitgelogd.};

        $c->res->redirect($c->uri_for('/auth/digid/logout', $redirect_args));
        $c->res->body('');
        $c->detach;
    }
    # Allow companies in when control panel is enabled
    elsif (!$c->model('DB::Interface')
        ->search_active({ module => 'controlpanel' })->count)
    {
        $c->session->{digid_error} = q{U bent succesvol aangemeld, maar
        helaas kunnen wij geen zaken vinden in ons systeem. Om
        veiligheidsredenen bent u uitgelogd.};

        $c->res->redirect(
            $c->uri_for('/auth/bedrijfid/logout', $redirect_args));
        $c->res->body('');
        $c->detach;
    }

}

sub logout : Chained('base') : PathPart('logout'): Args(0) {
    my ($self, $c) = @_;

    my $multi_tenant_state = $c->session->{ multi_tenant_state } || {};
    $c->delete_session;
    $c->response->redirect($c->uri_for('/pip', $multi_tenant_state));
}

sub zaaktypeinfo : Chained('zaak_base'): PathPart('zaaktypeinfo'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ nowrapper } = 1;
    $c->stash->{ template } = 'zaak/zaaktypeinfo.tt'
}

sub update_feedback : Chained('zaak_base'): PathPart('update/feedback') : Arg(0) {
    my ($self, $c) = @_;

    $c->res->redirect($c->req->referer);
    $c->detach unless $c->req->params->{note};

    $c->model('DB::Contactmoment')->contactmoment_create({
        type         => 'note',
        subject_id   => $c->stash->{ betrokkene }->betrokkene_identifier,
        created_by   => $c->stash->{ betrokkene }->betrokkene_identifier,
        case_id      => $c->stash->{ zaak }->id,
        medium       => 'webformulier',
        message      => $c->req->params->{ note },
    });

    my $event = $c->model('DB::Logging')->trigger('subject/contactmoment/create', {
        component => 'zaak',
        zaak_id => $c->stash->{ zaak }->id,
        created_by => $c->stash->{ betrokkene }->betrokkene_identifier,
        created_for => $c->stash->{ betrokkene }->betrokkene_identifier,
        data => {
            case_id => $c->stash->{ zaak }->id,
            content => $c->req->params->{ note },
            subject_id => $c->stash->{ betrokkene }->betrokkene_identifier,
            contact_channel => 'webformulier'
        }
    });
}


sub update_calendar_field : Chained('zaak_base') : PathPart('update_calendar_field') : Args() {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    $c->stash->{zaak}->zaak_kenmerken->update_fields({
        new_values => {
            $params->{bibliotheek_kenmerken_id} => $params->{value}
        },
        zaak => $c->stash->{zaak},
    });

    $c->forward('update_field_message', ['Kalenderafspraak gewijzigd']);

    $c->response->body('OK');
    $c->detach();
}


sub update_field_message : Private {
    my ($self, $c, $description) = @_;

    my $case = $c->stash->{zaak} or die "need case";

    if ($case->behandelaar) {
        my $betrokkene_identifier = $case->behandelaar->betrokkene_identifier;

        $c->model('DB::Message')->message_create({
            message     => 'Kenmerk aangepast vanuit PIP: ' . $description,
            case_id     => $case->id,
            event_type  => 'case/pip/updatefield',
            subject_id  => $betrokkene_identifier,
        });
    }

}


sub request_attribute_update : Chained('zaak_base') : PathPart('request_attribute_update') {
    my ($self, $c) = @_;

    $c->stash->{ subject_identifier } = $c->session->{ pip }{ ztc_aanvrager };

    $c->forward('/api/case/request_attribute_update');
}

sub woz : Chained('base') : PathPart('woz'): Args() {
    my ($self, $c, $subpage, $id) = @_;

    $c->stash->{current_page} = 'woz';

    my $res = $c->stash->{betrokkene} // $c->model('Betrokkene')->get(
        {},
        $c->session->{pip}->{ztc_aanvrager}
    );

    if (exists($c->req->params->{update})) {
        $res->mobiel($c->req->params->{'npc-mobiel'});
        $res->email($c->req->params->{'npc-email'});
        $res->telefoonnummer($c->req->params->{'npc-telefoonnummer'});
    }

    $c->stash->{betrokkene} = $res;

    # load woz specific settings
    my $settings = $c->model('DB::Settings')->filter({
        filter => 'woz_'
    });

    foreach my $setting (@$settings) {
        $c->stash->{
            $setting->key
        } = $setting->value;
    }

    if($subpage && $id) {

        my $params = $c->req->params;

        $c->stash->{woz_objects} = $c->stash->{woz_objects}->search({
            owner => $params->{owner},
            object_id => $params->{object_id},
            id => $id
        });

        if($subpage eq 'report') {
            $c->stash->{extra_body_class} = ' woz-body';
            $c->stash->{layout_type} = 'simple';
            $c->stash->{woz_report} = 1;
            $c->stash->{template} = "plugins/woz/report.tt";
        }
        elsif ($subpage eq 'report_pdf') {
            $self->woz_report_pdf($c);
        }
        else {
            $c->stash->{template} = 'plugins/pip/woz.tt';
        }
    } else {
        $c->stash->{template} = 'plugins/pip/woz.tt';
    }
}

=head2 woz_report_pdf

Internal function, used to create the PDF version of the WOZ taxation report.

=cut

sub woz_report_pdf {
    my $self = shift;
    my ($c) = @_;

    my $settings = $c->model('DB::Settings')->filter({ filter => 'woz_' });
    my %settings;
    for my $setting (@$settings) {
        $settings{ $setting->key } = $setting->value;
    }

    my $bibliotheek_sjabloon = $c->model('DB::BibliotheekSjablonen')->find(
        $settings{woz_sjabloon}
    );

    my $context = Zaaksysteem::ZTT::Context::WOZ->new(
        woz_object => $c->stash->{woz_objects}->first,
        settings   => \%settings,
        betrokkene => $c->stash->{betrokkene},
    );

    my $pdf_data = $c->model('PDFGenerator')->generate_pdf(
        template => $bibliotheek_sjabloon,
        context  => $context
    );

    $c->res->content_type('application/pdf');
    $c->res->body($pdf_data);
}

=head2 woz_datab

Wrapper page for opening an external session for the user. May redirect the
user back to C</pip> when no C<multichannel> interface exists, or something
went wrong while requesting the session URL.

=head3 URL

C</pip/woz_datab>

=cut

sub woz_datab : Chained('base') : PathPart('woz_datab'): Args() {
    my ($self, $c) = @_;

    $c->stash->{ current_page } = 'datab';
    $c->stash->{ template } = 'plugins/pip/woz_datab.tt';

    my $interface = first {
        $_->jpath('$.subject_type') eq $c->stash->{ betrokkene }->btype
    } $c->model('DB::Interface')->search_active({ module => 'multichannel' });


    unless (defined $interface) {
        $c->res->redirect($c->uri_for('/pip', $c->session->{ multi_tenant_args } || {}));
        $c->detach;
    }

    my $betrokkene = $c->stash->{betrokkene} // $c->model('Betrokkene')->get(
        {},
        $c->session->{pip}->{ztc_aanvrager}
    );

    $c->stash->{ multichannel_explanation } = $interface->jpath('$.explanation');

    try {
        $c->stash->{ multichannel_session_url } = $interface->process_trigger(
            'get_session_url',
            {
                subject   => $betrokkene,
                useragent => $c->req->header('User-Agent'),
            }
        );
    } catch {
        $c->log->error($_);
        $c->res->redirect($c->uri_for('/pip', $c->session->{ multi_tenant_args } || {}));
        $c->detach;
    };

    return;
}

=head2 show_subcase_in_pip

Show links to subcase creation in the PIP

=cut

sub show_subcase_in_pip {
    my ($self, $c, @opts) = @_;

    my $model = $c->model('Subcase');

    my $subcases = $model->get_pip_subcases(
        case       => $c->stash->{zaak},
        betrokkene => $c->stash->{betrokkene},
    );

    $c->stash->{pip_subcases} = $subcases if $subcases;
}

sub landcodes {
    my ($self, $c) = @_;
    my $landcodes = [];
    {
        my %remap = reverse %{ RGBZ_LANDCODES() };
        for my $country (sort keys %remap) {
            push(@{ $landcodes },
                {
                    value   => $remap{$country},
                    label   => $country,
                }
            );
        }
    }
    return $landcodes;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2017 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

=head2 contact

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

=head2 login

TODO: Fix the POD

=cut

=head2 logout

TODO: Fix the POD

=cut

=head2 overview

TODO: Fix the POD

=cut

=head2 request_attribute_update

TODO: Fix the POD

=cut

=head2 update_calendar_field

TODO: Fix the POD

=cut

=head2 update_feedback

TODO: Fix the POD

=cut

=head2 update_field_message

TODO: Fix the POD

=cut

=head2 view_element

TODO: Fix the POD

=cut

=head2 woz

TODO: Fix the POD

=cut

=head2 zaak

TODO: Fix the POD

=cut

=head2 zaaktypeinfo

TODO: Fix the POD

=cut

=head2 instances

TODO: Fix the POD

=cut
