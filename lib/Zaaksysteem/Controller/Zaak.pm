package Zaaksysteem::Controller::Zaak;
use Moose;

use Clone qw(clone);
use Encode;
use File::Basename;
use File::stat;
use HTML::TagFilter;
use MIME::Lite;
use OpenOffice::OODoc;
use Time::localtime;

use Zaaksysteem::ZTT;
use Zaaksysteem::Constants;
use BTTW::Tools;
use Zaaksysteem::Attributes;
use Zaaksysteem::Backend::Rules;
use List::Util qw(any);

BEGIN { extends 'Zaaksysteem::Controller' }

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_CONSTANTS
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR
    ZAAKSYSTEEM_AUTHORIZATION_ROLES
    ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM
    ZAAKSYSTEEM_CONTACTKANAAL_BALIE

    ZAAK_CREATE_PROFILE
    MIMETYPES_ALLOWED
    PUBLIC_CASE_FINISH_TITLE
    PUBLIC_CASE_FINISH_MESSAGE

    :SAML_TYPES
/;

# an attempt at English code
use constant SAVED_CASE_PROPERTIES => '_SAVED_CASE_PROPERTIES';

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched Zaaksysteem::Controller::Zaak in Zaak.');
}

=head2 is_api_environment

    $self->is_api_environment($c);

Returns C<true> when call is requested for API environment. In other words, when
C<< $c->user->usersettings->lab->case_version >> is version 2.

=cut

sub is_api_environment {
    my ($self, $c) = @_;

    return 1 if ($c->user->usersettings->lab->case_version eq 2);
    return;
}


sub base : Chained('/') : PathPart('zaak'): CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    return unless $id =~ /^\d+$/;

    ### New or old style url?
    if ($c->action->private_path eq '/zaak/view' && $self->is_api_environment($c)) {
        $c->res->redirect($c->uri_for('/intern/zaak/' . $id));
        $c->detach;
    }

    $c->stash->{'template'}     = 'zaak/view.tt';

    ### Retrieve zaak
    $c->stash->{'zaak'} = $c->model('DB::Zaak')->find($id);

    if (!$c->stash->{zaak} || $c->stash->{zaak}->is_deleted || $c->stash->{ zaak }->status eq 'deleted') {
        # In case there is some kind of issue with object_data, make sure we
        # touch the case so any search request is handled correctly: we don't
        # need to touch it manually. .
        if ($c->stash->{zaak}) {
            $c->stash->{zaak}->object_data;
        }
        $c->push_flash_message('Geen zaak gevonden met dit nummer');
        $c->response->redirect($c->uri_for('/'));
        $c->detach;
    }

    if ($c->user_exists) {
        $c->assert_any_zaak_permission('zaak_read','zaak_beheer','zaak_edit');
    }

    my $params = $c->req->params;

    # Mark a message as read if it is the 'lead in' for this case.
    if ($params->{message_id}) {
        # Find subject
        my $subject;
        if ($c->user_exists) {
            $subject = 'betrokkene-medewerker-'.$c->user->uidnumber;
        }
        elsif ($c->req->params->{'ztc_aanvrager_id'}) {
            $subject = $c->req->params->{'ztc_aanvrager_id'};
        }
        else {
            $subject = $c->stash->{zaak}->aanvrager_object->rt_setup_identifier;
        }

        my ($message) = $c->model('DB::Message')->find($params->{message_id});
        if ($message) {
            $message->update_properties({
                is_read    => 1,
                subject_id => $subject
            });
        }
    }

    ### Find fase
    my $fase;
    if (($fase = $params->{fase}) && $fase =~ /^\d+$/) {
        my $fases = $c->stash->{zaak}
            ->zaaktype_node_id
            ->zaaktype_statussen
            ->search({ status  => $fase });

        $c->stash->{requested_fase} = $fases->first if $fases->count;
    } else {
        $c->stash->{requested_fase} = (
            $c->stash->{zaak}->volgende_fase ||
            $c->stash->{zaak}->huidige_fase
        );
    }
}


sub case_information : Chained('base') : PathPart('info') :Args() {
    my ($self, $c, $item) = @_;

    $c->stash->{nowrapper} = 1;

    if($item && $item eq 'geolocatie') {
        $c->stash->{template} = 'zaak/elements/view_maps.tt';
    } else {
        $c->stash->{template} = 'zaak/elements/case.tt';
    }
}

sub preprocess_attributes {
    my ($self, @attributes) = @_;

    return [map {{
        name => $_->name,
        bwcompat_name => [$_->get_bwcompat_name]
    }} @attributes];
}

sub test_magic_strings : Chained('base') : PathPart('test_magic_strings') {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{system_attributes} = [Zaaksysteem::Attributes::predefined_case_attributes];

    $c->stash->{process_magic_string} = sub {
        my ($input) = @_;

        return "image not supported on test page"
            if $input && $input =~ m/behandelaar_handtekening/;

        my $ztt = Zaaksysteem::ZTT->new;
        $ztt->add_context($c->stash->{zaak});

        return $ztt->process_template($input)->string;
    };

    my $test_content = $c->req->params->{test_content} || '';
    $c->stash->{test_content_processed} = $c->stash->{process_magic_string}->($test_content);

    $c->stash->{template} = 'zaak/test_magic_strings.tt';
}



my $SPIFFY_SPINNER_DEFINITION = {
    'mode'      => 'timer',
    'title'     => 'Een moment geduld a.u.b. ...',
    'checks'    => [
        {
            'naam'  => 'kenmerk',
            'label' => 'Verzenden van formulier',
            'timer' => 2000,
        },
        {
            'naam'  => 'sjabloon',
            'label' => 'Aanmaken van documenten',
            'timer' => 2000,
        },
        {
            'naam'  => 'notificatie',
            'label' => 'Verzenden van notificatie',
            'timer' => 2000,
        },
        {
            'naam'  => 'vervolgzaak',
            'label' => 'Aanmaken van zaak',
            'timer' => 2000,
        },
    ],
};

sub create_redirect : Chained('/') : PathPart('zaak/create'): Args(0) {
    my ($self, $c, $wizard_stap) = @_;

    my $url = '/zaak/create/balie';
    if ($c->is_externe_aanvraag) {
        $url = '/zaak/create/webformulier';
    } else {
        $c->session->{_zaak_create} = {};
    }

    $c->res->redirect($c->uri_for($url, $c->req->params));
}


{

    define_profile create => %{ZAAK_CREATE_PROFILE()};

    sub create_base : Chained('/') : PathPart('zaak/create'): CaptureArgs(1) {
        my ($self, $c, $aangevraagd_via) = @_;

        my $params = $c->req->params();

        $c->forward('_spiffy_spinner', [ $SPIFFY_SPINNER_DEFINITION ]);

        ### New style dashboard: create and via_dashboard set
        if (
            $c->req->params->{via_dashboard} &&
            $c->req->params->{create} &&
            scalar keys %$params
        ) {
            $c->session->{_zaak_create} = {
                map(
                    {
                        $_ => $c->req->params->{$_}
                    }

                    qw/zaaktype_id contactkanaal ztc_aanvrager_id aanvraag_trigger ztc_trigger bestemming ontvanger/

                ),
            };

            $c->forward('_create_validation_acties');

            $c->stash->{zapi} = [{
                success         => 1,
                redirect_url    => $c->uri_for('/zaak/create/balie', { via_dashboard => 1 })->as_string,
            }];

            $c->detach('View::ZAPI');
        }

        if ($c->user_exists) {
            if ($params->{tooltip}) {

                my $json = new JSON;
                #$json = $json->allow_blessed([1]); # Nodig om een HASH in JSON om te zetten :-S

                if($c->session->{remember_zaaktype}) {
                    my $remember_zaaktype = clone $c->session->{remember_zaaktype};
                    $c->stash->{remember_zaaktype_json} = $json->encode($remember_zaaktype);
                }
                $c->stash->{'template'} = 'widgets/zaak/create.tt';
                $c->stash->{'nowrapper'} = 1;
            } else {
                $c->stash->{'template'} = 'zaak/annuleer.tt';
            }
        } else {
            $c->stash->{'template'} = 'forbidden.tt';
        }

        ### Start van aanvraag, delete create session
        if ($c->user_exists && !scalar keys %$params && !$params->{via_dashboard}) {
            $c->forward('_zaak_create_aanmaak_meldingen');

            $c->log->debug('Wiping zaak create session, no params');
            $c->session->{_zaak_create} = {};
            $c->detach;
        }

        if ($params->{sessreset}) {
            $c->log->info('Wiping zaak create session, session reset asked');
            $c->session->{_zaak_create} = {};
        }

        $c->session->{_zaak_create}{zaaktype_id} //= $params->{zaaktype_id};
        $c->session->{_zaak_create}{aangevraagd_via} = $aangevraagd_via;

        if (  !$c->session->{_zaak_create}{ztc_aanvrager_type}
            && $params->{ztc_aanvrager_type}) {
            my $type = $params->{ztc_aanvrager_type};
            if (any { $type eq $_ }
                qw(medewerker niet_natuurlijk_persoon natuurlijk_persoon bedrijf unknown)
                )
            {
                $c->session->{_zaak_create}{ztc_aanvrager_type} = $type;
            }
            else {
                $c->log->error("Unknown requestor type: '$type'");
            }
        }

        if (defined $params->{remembered_zaaktype}) { # hacky - to see if the form was actually submitted
            if($params->{remember_zaaktype}) {
                $c->session->{remember_zaaktype}->{
                   $params->{ztc_trigger}
                } = {
                    zaaktype_id     => $c->req->params->{zaaktype_id},
                    zaaktype_titel  => $c->req->params->{zaaktype_name}, # discrepancy in naming, TODO
                };
            } else {
                $c->session->{remember_zaaktype} = {};
            }
        }

        if (
            defined($c->req->params->{prefill_zaaktype_id}) &&
            defined($c->req->params->{prefill_zaaktype_name}) &&
            $c->req->params->{prefill_zaaktype_id} &&
            $c->req->params->{prefill_zaaktype_name}
        ) {
            my $json = new JSON;
            my $prefill = {
                zaaktype_id         => $c->req->params->{prefill_zaaktype_id},
                zaaktype_titel      => $c->req->params->{prefill_zaaktype_name},
            };

            my $trigger = $c->req->params->{aanvraag_trigger} || 'extern';
            my $obj;

            if($trigger eq 'internextern') {
                $obj = {
                    'intern' => $prefill,
                    'extern' => $prefill
                };
            } else {
                $obj = {
                    $trigger => $prefill
                };
            }

            $c->stash->{prefill_zaaktype} = $json->encode($obj);
        }

        if ($c->req->params->{related_object}) {
            $c->session->{_zaak_create}{related_object} = $c->req->params->{related_object}
        }

        my @actions = qw(
            _create_verify_security
            _create_verify_externe_data
            _create_validation
            _set_double_click_security
            _create_load_and_validate_zaaktype
            _create_load_stash
            _create_load_preset_client
            _create_load_externe_data
            _create_load_saved_case_properties
            _create_load_prefill_case_properties
        );
        my $registratiedatum;
        foreach (@actions) {

            if ($ENV{ZS_DEBUG_ZAAK}) {
                $c->log->debug(
                    "Entering $_ with session data: "
                        . dump_terse($c->session->{_zaak_create}));
            }
            $c->forward($_);

            if ($ENV{ZS_DEBUG_ZAAK}) {
                if ($c->session->{_zaak_create}{registratiedatum}) {
                    $c->log->debug("$_ set the registration date");
                    if (!$registratiedatum) {
                        $registratiedatum = delete $c->session->{_zaak_create}{registratiedatum};
                    }
                    else {
                        $c->log->debug("deleting registratiedatum again");
                        delete $c->session->{_zaak_create}{registratiedatum};
                    }
                }
            }
        }


        if ($ENV{ZS_DEBUG_ZAAK}) {
            $c->log->debug("We leave with session data: "
                . dump_terse($c->session->{_zaak_create}));
            $c->session->{_zaak_create}{registratiedatum} = $registratiedatum;
        }


        $c->stash->{nowrapper} = 1 if $c->req->is_xhr;
    }

    sub _set_double_click_security : Private {
        my ($self, $c) = @_;

        if (
            defined($c->session->{_zaak_create}) &&
            !defined($c->session->{_zaak_create}->{dbl_click_token})
        ) {
            $c->session->{_zaak_create}->{dbl_click_token} =
                $c->model('DB::Zaak')->_generate_uuid;
        }
    }

    sub _check_double_click_security : Private {
        my ($self, $c) = @_;

        if (
            defined($c->session->{_zaak_create}) &&
            defined($c->session->{_zaak_create}->{dbl_click_token}) &&
            !$c->session->{_zaak_create}->{dbl_click_token}
        ) {
            if ($c->user_exists) {
                $c->res->redirect($c->uri_for('/'));
            } else {
                $c->res->redirect($c->uri_for('/pip'));
            }

            $c->detach;
        }
    }

    sub _clear_double_click_security : Private {
        my ($self, $c) = @_;

        if (
            defined($c->session->{_zaak_create}) &&
            defined($c->session->{_zaak_create}->{dbl_click_token})
        ) {
            $c->session->{_zaak_create}->{dbl_click_token} = 0;

            ### Early Session SAVE
            $c->_save_session;

        }
    }


    sub _create_load_preset_client : Private {
        my ($self, $c) = @_;

        if (  !$c->user_exists
            && $c->stash->{zaaktype_node}->zaaktype_definitie_id->preset_client
            && !$c->session->{_twofactor}{authenticated}
            && !$c->session->{_saml}{uid}
            && !$c->model('Plugins::Bedrijfid')->login
            && !$c->session->{_zaak_create}->{extern}->{verified})
        {
            my $preset_client = $c->session->{_zaak_create}->{aanvrager}
                = $c->stash->{zaaktype_node}
                ->zaaktype_definitie_id->preset_client;

            my ($aanvrager_type, $id)
                = $preset_client =~ m|^betrokkene-(\w*)-(\d*)$|is;
            $c->session->{_zaak_create}->{extern}->{aanvrager_type}
                = $aanvrager_type;
            $c->session->{_zaak_create}->{extern}->{verified}
                = 'preset_client';
            $c->session->{_zaak_create}->{extern}->{id} = $id;

            $c->stash->{logged_in_by}
                = $c->session->{_zaak_create}->{extern}->{verified};
            $c->stash->{aanvrager_type} = 'preset_client';
        }
    }

    sub _zaak_create_aanmaak_meldingen : Private {
        my ($self, $c) = @_;

        if ($c->req->params->{actie} && $c->req->params->{actie} eq 'doc_intake') {
            $c->push_flash_message('Document toevoegen aan zaak') unless $self->is_api_environment($c);
        }

    }

    sub _create_load_externe_data : Private {
        my ($self, $c) = @_;

        ### Only for externe aanvragen
        return 1 if $c->user_exists;

        return 1
            if (
               $c->stash->{zaaktype_node}->zaaktype_definitie_id->preset_client
            && $c->session->{preset_client});


        unless (
            $c->session->{_zaak_create}->{extern} &&
            $c->session->{_zaak_create}->{extern}->{aanvrager_type}
        ) {
            $c->detach('/form/aanvrager_type');
        }

        if ($c->session->{_saml} && $c->session->{_saml}->{uid}) {
            if (
                (
                    $c->session->{_zaak_create}{ ztc_aanvrager_type } eq 'natuurlijk_persoon' &&
                    (
                        $c->session->{_saml}->{used_profile} ne SAML_TYPE_LOGIUS &&
                        $c->session->{_saml}->{used_profile} ne SAML_TYPE_SPOOF
                    )
                ) ||
                (
                    $c->session->{_zaak_create}{ ztc_aanvrager_type } eq 'niet_natuurlijk_persoon' &&
                    (
                        $c->session->{_saml}->{used_profile} ne SAML_TYPE_KPN_LO &&
                        $c->session->{_saml}->{used_profile} ne SAML_TYPE_SPOOF
                    )
                )
            ) {
                $c->stash->{template}   = 'form/aanvraag_error.tt';
                $c->stash->{error}      = {
                    titel   => 'Helaas, deze zaak kan niet worden aangevraagd',
                    bericht => 'Wij kunnen uw aanvraag helaas niet uitvoeren'
                };
                $c->detach;
            }
        }

        $c->forward('_create_secure_aanvrager_bekend');

        ### Fallback, NOT AUTHORIZED
        unless (
            $c->session->{_zaak_create}->{extern} &&
            $c->session->{_zaak_create}->{extern}->{verified}
        ) {
            $c->detach('/form/aanvrager_type');
        }

        $c->session->{_zaak_create}->{aanvraag_trigger} = 'extern';

        # Ensure we don't create duplicate entries in the
        # natuurlijk_persoon/bedrijf tables when the user is stress-testing our
        # system by performing a "next/previous" loop.
        return if $c->session->{_zaak_create}{ztc_aanvrager_id};

        # Create the new natuurlijk_persoon/bedrijf entry
        $c->forward('/plugins/twofactor/_zaak_create_load_externe_data');
        $c->forward('/plugins/digid/_zaak_create_load_externe_data');
        $c->forward('/plugins/bedrijfid/_zaak_create_load_externe_data');
    }


    sub _create_load_prefill_case_properties : Private {
        my ($self, $c) = @_;

        my $params = $c->req->params();

        my $PREFIX = 'prefill_';

        # extract prefilled magic_string parameters
        my $magic_strings = [map /^$PREFIX(.*)/, grep /^$PREFIX/, keys %$params];

        # avoid querying if there's no prefill anyway.
        return unless scalar @$magic_strings;

        # find the bibliotheek_kenmerken_id for the prefilled kenmerken by magic string
        my $fields_rs = $c->stash->{zaaktype_node}->zaaktype_kenmerken->search({
            'bibliotheek_kenmerken_id.magic_string' => {
                '-in' => $magic_strings
            }
        }, {
            join    => [qw/bibliotheek_kenmerken_id/],
        });

        my $case_properties = $c->session->{_zaak_create}->{form}->{kenmerken} ||= {};

        # assign the prefilled values using the found bibliotheek_kenmerken_id's.
        map {
            $case_properties->{
                $_->get_column('bibliotheek_kenmerken_id')
            } = $params->{
                $PREFIX . $_->magic_string
            };
        } $fields_rs->all;
    }



    # when using Regels (Rules) to redirect to another zaaktype, the entered data can be
    # retained. it is restored here.
    #
    sub _create_load_saved_case_properties : Private {
        my ($self, $c) = @_;

        my $saved_case_properties = $c->session->{SAVED_CASE_PROPERTIES};
        my $case_properties = $c->session->{_zaak_create}->{form}->{kenmerken} ||= {};

        foreach my $property_id (keys %$saved_case_properties) {
            $case_properties->{$property_id} = $saved_case_properties->{$property_id};
        }

        delete $c->session->{SAVED_CASE_PROPERTIES};
    }


    sub _create_secure_aanvrager_bekend : Private {
        my ($self, $c) = @_;

        ### Not logged in, in any way
        unless ($c->session->{_zaak_create}->{extern}->{verified}) {
            $c->detach;
        }

    }

    sub _create_verify_externe_data : Private {
        my ($self, $c) = @_;

        ### Only for externe aanvragen
        return 1 if $c->user_exists;

        ### Verify bussumid or digid
        my $params          = $c->req->params();

        my $aanvrager_type  = $c->session->{_zaak_create}{ztc_aanvrager_type};

        unless($aanvrager_type && $aanvrager_type eq 'unknown') {
            $c->forward('/plugins/twofactor/_zaak_create_security');
            $c->forward('/plugins/digid/_zaak_create_secure_digid');
            $c->forward('/plugins/bedrijfid/_zaak_create_security');
        }
    }

    sub create : Chained('create_base') : PathPart(''): Args() {
        my ($self, $c, $wizard_stap) = @_;

        $c->stash->{ page_title } = $c->stash->{ zaaktype_node }->titel;

        ### Dispatch to form
        if ($c->user_exists) {
            $wizard_stap ||= 'zaakcontrole';
        } else {
            $wizard_stap ||= 'aanvrager';
        }

        $c->forward('/form/' . $wizard_stap);

        $c->detach unless $c->stash->{publish_zaak};

        $c->forward('_check_double_click_security');


        ### Publish zaak
        $c->forward('_create_zaak', [ $c->session->{_zaak_create} ]);
    }

    sub handle_offline_payment : Chained('/') : PathPart('zaak/handle_offline_payment') : Args(0) {
        my ($self, $c) = @_;

        throw("payment/invalid_request_method") unless $c->req->method eq 'POST';

        my $case_id = $c->session->{payment_case_id}
            or throw(
                "payment/no_case_id",
                "Can't set payment status: no payment in progresss in current session"
            );

        my $zaak = $c->model('DB::Zaak')->find($case_id)
            or throw(
                "payment/no_case",
                "Can't find case with id '$case_id'"
            );

        if ($zaak->payment_status ne CASE_PAYMENT_STATUS_PENDING) {
            throw(
                'payment/wrong_state',
                'Case payment is not in state "pending"',
            );
        }

        $zaak->set_payment_status(
            CASE_PAYMENT_STATUS_OFFLINE,
            $zaak->payment_amount,
        );

        $c->stash->{zaak}      = $zaak;
        $c->stash->{zaaktype}  = $zaak->zaaktype_node_id;
        $c->stash->{nowrapper} = 1;
        $c->stash->{betaling}  = 0;

        # Payment handled. Done.
        delete $c->session->{payment_case_id};

        $c->forward('/zaak/finish');
    }


    # 1) if a rule determines the price, use that value, return that.
    # 2) otherwise use zaaktype_definitie->pdc_tarief, unless:
    # 3) if a specific contactchannel has been configured, use that.
    sub _create_zaak_set_offline_payment_info : Private {
        my ($self, $c) = @_;

        my $rules_amount    = $c->stash->{rules_payment_amount};
        my $contactkanaal   = $c->session->{_zaak_create}->{contactkanaal};
        my $node            = $c->stash->{zaak}->zaaktype_node_id;
        my $casetype_amount = $node->zaaktype_definitie_id->pdc_tarief;

        if ($contactkanaal ne ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM && $node->properties) {
            $casetype_amount = $node->properties->{'pdc_tarief_' . $contactkanaal};
        }

        if (ref $rules_amount eq 'CODE') {
            $rules_amount = $rules_amount->($c->stash->{ zaak });
            $rules_amount =~ s|,|.|gis;
        }

        my $payment_amount = $rules_amount || $casetype_amount || 0;

        if ($payment_amount) {
            $c->stash->{zaak}->payment_amount($payment_amount);
            $c->stash->{zaak}->update();
        }
    }

    sub _create_handle_payment : Private {
        my ($self, $c)  = @_;

        if (
            $c->session->{_zaak_create} &&
            $c->forward('_create_determine_online_payment') &&
            (
                !$c->user_exists ||
                exists($c->session->{behandelaar_form})
            )
        ) {
            $c->session->{payment_case_id} = $c->stash->{zaak}->id;
            $c->res->redirect($c->uri_for('/plugins/ogone/betaling'));
            $c->res->body("");
            $c->detach();
        } else {
            $c->forward('_create_zaak_set_offline_payment_info');
            $c->forward('_create_zaak_fire_phase_actions', [ 'email' ]);
        }
    }

=head2 _create_determine_online_payment

Rules take precedence over casetype settings. If no rule speaks
up, the casetype is consulted.

=cut

sub _create_determine_online_payment : Private {
    my ($self, $c) = @_;

    # In the first step of public case creation, no rules have been
    # executed and thus we can't establish wether online payment will
    # happen. The online payment step marker will show up during the
    # case creation. The default position is 'no internet payment':
    return unless $c->stash->{regels_result} && $c->stash->{zaaktype};

    my $rules_result = $c->stash->{regels_result};

    return exists $rules_result->{set_online_payment} ?
        $rules_result->{set_online_payment} :
        $c->stash->{zaaktype}->online_betaling;
}

    sub _create_handle_finish : Private {
        my ($self, $c)  = @_;

        if ($c->session->{_zaak_create}) {
            ### Logged in
            if ($c->user_exists) {
                if (!$c->stash->{zaak}) {
                    my $errmsg  = 'Er is iets misgegaan bij het aanmaken '
                        . 'van de zaak.';
                    $c->log->warn($errmsg);
                    $c->push_flash_message($errmsg) unless $self->is_api_environment($c);

                    $c->res->redirect(
                        $c->uri_for('/')
                    );
                    $c->detach;
                }
            }

            my $redirect = '/intern/';

            if ($c->user_exists && !exists($c->session->{behandelaar_form})) {
                my %query_params = (
                    flash_message_case_id   => $c->stash->{zaak}->id,
                    flash_message_action    => 'create_case',
                );
                if ($c->req->params->{ actie_automatisch_behandelen }) {
                    # Only redirect to the zaak if we actually have a assignee.
                    # Lacking one implies $zaak->open() failed, probably due to
                    # authorization failures (search for check_permissions_for_assignee)
                    if ($c->stash->{ zaak }->behandelaar) {
                        $redirect = sprintf(($self->is_api_environment($c) ? '/intern/' : '/') . 'zaak/%d', $c->stash->{ zaak }->nr);

                        $c->push_flash_message('Zaak is door u in behandeling genomen') unless $self->is_api_environment($c);
                    } else {
                        $query_params{flash_message_assign_result} = 'failed';
                    }
                } elsif ($c->req->params->{ assignee_id } && (not $c->stash->{ zaak }->behandelaar)) {
                    $query_params{flash_message_assign_result} = 'failed';
                }

                $c->response->redirect($c->uri_for($redirect, \%query_params));
                $c->detach;
            } elsif (!$c->user_exists) {
                ### External user
                $c->forward('finish');
            } else {
                return 1;
            }

            delete($c->session->{_zaak_create});
        }
    }

=head2 finish

Stash attributes: [preset_client]

    $c->forward('/zaak/finish');

Will delete the session entirely, and show the C<form/finish.tt> template telling the user
his case is succesfully registered by mentioning their case number

=cut

    sub finish : Private {
        my ($self, $c)  = @_;

        $c->stash->{preset_client} = $c->stash->{zaaktype}->zaaktype_definitie_id->preset_client;

        ### Generate finish text
        my $ztt     = Zaaksysteem::ZTT->new;

        my $title   = $c->stash->{zaaktype}->properties->{public_confirmation_title} || PUBLIC_CASE_FINISH_TITLE;
        my $message = $c->stash->{zaaktype}->properties->{public_confirmation_message} || PUBLIC_CASE_FINISH_MESSAGE;

        $ztt->add_context($c->stash->{zaak});

        $self->log->trace('Title:   ' . $title);
        $self->log->trace('Message: ' . $message);

        $c->stash->{finish_title}   = $ztt->process_template($title)->string;
        $c->stash->{finish_message} = $ztt->process_template($message)->string;

        # Template should survive the session being cleared (ZS-13444).
        my $current_template = $c->session->{ custom_template };
        my $redirect_args = {};

        if ($current_template) {
            $redirect_args->{ template } = $current_template;
        }

        $c->stash->{ pip_url } = $c->uri_for('/pip', $redirect_args);

        ### Logout user
        if ($c->session->{preset_client}) {
            $c->delete_session('User finished case creation');
            $c->stash->{is_preset_client} = 1;
        }

        $c->session->{ custom_template } = $current_template;

        $c->stash->{template} = 'form/finish.tt';
    }

    sub _create_zaak : Private {
        my ($self, $c, $params)  = @_;

        ### Fix kenmerken
        {
            $params->{kenmerken} = [];

            unless($params->{raw_kenmerken} && %{ $params->{raw_kenmerken} }) {
                $params->{raw_kenmerken} = $params->{form}->{kenmerken};
            }

            my @kenmerk_ids = keys %{ $params->{raw_kenmerken} };

            my $rs = $c->model('DB::BibliotheekKenmerken')->search_rs(
                {
                    id         => \@kenmerk_ids,
                    value_type => 'valuta',
                }
            );

            my %mapping;
            while (my $kenmerk = $rs->next) {
                $mapping{$kenmerk->id} = $kenmerk->value_type;
            }

            for my $kenmerk (@kenmerk_ids) {
                my $value = $params->{raw_kenmerken}->{$kenmerk};
                if ($mapping{$kenmerk} && $value) {
                    if ($mapping{$kenmerk} eq 'valuta') {
                        $value = Zaaksysteem::Constants::_numeric_fix_filter(undef, $value);
                    }
                }
                push( @{ $params->{kenmerken} },
                    { $kenmerk => $value },
                )
            }
        }

        ### Fix betrokkene
        if ($params->{ztc_aanvrager_id} && $params->{ztc_aanvrager_id} =~ /betrokkene-.*?-.*/) {
            $params->{aanvragers} = [{
                'betrokkene'        => $params->{ztc_aanvrager_id},
                'verificatie'       => (
                    (
                        $c->session->{_zaak_create}->{aangevraagd_via} eq
                            ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM
                    )
                        ? $c->session->{_zaak_create}->{extern}->{verified}
                        : 'medewerker'
                )
            }];
        }

        ### REMOVE DUMMY
        if ($params->{aanvragers}) {
            for (my $i=0; $i < scalar(@{ $params->{aanvragers} }); $i++) {
                if ($params->{aanvragers}->[$i]->{betrokkene} =~ /dummy/) {
                    delete($params->{aanvragers}->[$i]);
                }
            }
        }

        $params->{confidentiality} = $c->req->params->{actie_confidentiality} || 'public';

        if ($c->session->{_zaak_create}->{dbl_click_token}) {
            $params->{duplicate_prevention_token} = $c->session->{_zaak_create}->{dbl_click_token};
        }

        my $parent_case;
        if ($c->session->{_zaak_create}{type_zaak}) {
            my $case_id  = $c->session->{_zaak_create}{zaak_relatie_id};
            my $rs =$c->model('DB::Zaak')->search_extended({ 'me.id' => $case_id});

            if ($parent_case = $rs->first) {
                $params->{relatie} = $c->session->{_zaak_create}{type_zaak};
                $params->{zaak_id} = $parent_case->id;
            }
            else {
                $c->log->info("Case with id '$case_id' not found, probably deleted...");
                delete $params->{$_} foreach qw(type_zaak zaak);
            }
        }

        my $zaak;
        try {
            # Put the case creation, which includes the touch, into a
            # transaction. When a touch fails for whatever reason we
            # don't create messed up cases..

            # Here be dragons: Add the case model to the create zaak so we can
            # trigger a rule execution.
            $params->{skip_required_attributes} = 0;
            $c->model('DB')->txn_do(
                sub {
                    $zaak = $c->model('DB::Zaak')->create_zaak($params, $c->model("Zaak"));
                    $zaak->create_default_directories();
                    $zaak->_touch();
                }
            );
            delete $params->{skip_required_attributes};
        }
        catch {
            if ($_ =~ /duplicate key value violates unique constraint "zaak_duplicate_prevention_token_key"/) {
                $c->log->warn(
                    sprintf(
                        'Duplicate case creation, fetching case with prevention token %s',
                        $params->{duplicate_prevention_token})
                );
                if (
                    $c->stash->{zaak} = $c->model('DB::Zaak')->search({
                        duplicate_prevention_token =>
                            $params->{duplicate_prevention_token}
                    })->first) {
                    $c->detach('_create_handle_finish');
                }
            }
            $c->log->error($_);
            $c->stash->{foutmelding}    = 'Zaak kon niet worden aangemaakt';
            $c->stash->{template}       = 'foutmelding.tt';
            $c->detach;
        };

        $self->link_2fa_subject_to_betrokkene($c);

        if ($c->session->{_zaak_create}{related_object}) {
            $c->log->debug("Relating object to case: " . $c->session->{_zaak_create}{related_object});
            my $case_obj    = $c->model('Object')->inflate_from_row($zaak->object_data);
            my $related_obj = $c->model('Object')->retrieve(uuid => $c->session->{_zaak_create}{related_object});

            $related_obj->relate($case_obj);
            $c->model('Object')->save(object => $related_obj);
        }
        else {
            $c->log->debug("No object to relate to case.");
        }

        # If we have a parent case, see if we need to relate the case
        if ($parent_case) {
            $c->model('Subcase')->relate_subcase(
                type     => $params->{type_zaak},
                parent   => $parent_case,
                child    => $zaak,
            );
        }

        # Only try to delete them if we have a "external" requestor.
        # The if is there for internal case registrations for employees
        # TODO: Internal case registration should go to /intern
        $c->model("ConceptCase")->delete_concept_case(
            $c->session->{_zaak_create}{zaaktype_id},
            $c->session->{_zaak_create}{ztc_aanvrager_id},
        ) if $c->session->{_zaak_create}{ztc_aanvrager_id};

        $c->stash->{zaak} = $zaak;

        $c->forward('/zaak/handle_fase_acties', [ $params ]);

        return $zaak;
    }

    sub link_2fa_subject_to_betrokkene {
        my ($self, $c) = @_;

        my $id = $c->session->{_twofactor}{subject_id};
        return 0 unless $id;

        if (my $subject = $c->model('DB::Subject')->find($id)) {
            my $model = $c->model('Auth::Alternative');
            if (!$model->get_subject_link($subject)) {
                if (my $betrokkene = $model->find_betrokkene_by_subject($subject)) {
                    $model->set_subject_link($subject, $betrokkene);
                    return 1;
                }
            }
        }
        return 0;
    }

    sub handle_fase_acties : Private {
        my ($self, $c, $params) = @_;

        $c->forward('_create_zaak_handle_uploads', [ $params ]);
        $c->forward('_create_zaak_handle_contact', [ $params ]);
        $c->forward('_create_zaak_handle_acties', [ $params ]);

        $c->forward('_create_zaak_fire_phase_actions', [ 'subject' ]);
        $c->forward('_create_zaak_fire_phase_actions', [ 'allocation' ]);
        $c->forward('_create_zaak_fire_phase_actions', [ 'template' ]);
        $c->forward('_create_zaak_fire_phase_actions', [ 'case' ]);


        my $rules_result = $c->stash->{zaak}->execute_rules({ status => 1 });

        if ($rules_result) {
            if ($rules_result->{send_external_system_message}) {
                $c->stash->{zaak}->send_external_system_messages(
                    base_url     => $c->uri_for('/'),
                    rules_result => $rules_result->{send_external_system_message},
                );
            }
            if ($rules_result->{wijzig_registratiedatum}) {
                my $rule = $rules_result->{wijzig_registratiedatum};
                $c->stash->{zaak}->process_date_of_registration_rule( %$rule );
            }
        }

        $c->forward('_create_handle_payment');
        $c->forward('_create_handle_finish');
    }

    sub _create_zaak_fire_phase_actions : Private {
        my ($self, $c, $type) = @_;

        return unless $type;

        my $actions = $c->stash->{ zaak }->registratie_fase->case_actions->search({
            case_id => $c->stash->{ zaak }->id,
            type => $type
        })->sorted;

        $actions->apply_rules({
            case => $c->stash->{ zaak },
            milestone => 1
        });

        for my $action ($actions->all) {
            next unless $action->automatic;

            $c->log->debug('Running action: ' . $action->label);

            # Only change route fields when case is automatically being assigned. This
            # will leave behandelaar and status alone.
            my %opts = (
                action => $action
            );

            if ($c->req->params->{actie_automatisch_behandelen}) {
                $opts{ change_only_route_fields } = 1;
            }

            if ($c->user_exists) {
                $opts{ current_user } = $c->user;
            }

            try {
                my %queue_opts = (
                    !$c->user_exists ? (DisableACL => 1) : (),
                );

                $c->model('Queue', %queue_opts)->run($c->stash->{ zaak }->fire_action(%opts));
            }
            catch {
                if ($type eq 'email') {
                    $c->log->info('We prevented ZSOD because of case creation for e-mail error: ' . $_);
                } else {
                    die $_;
                }
            };
        }
    }

    sub _create_zaak_handle_acties : Private {
        my ($self, $c, $params)  = @_;

        ### This is here, because we can call this method from status/next,
        ### and then we do not want to run this routine.
        return unless ($c->session->{_zaak_create});

        my $req_params = $c->req->params;

        if ($c->session->{_zaak_create}->{acties}->{doc_intake}) {
            my $doc_intake  = $c->session->{_zaak_create}
                                ->{acties}
                                ->{doc_intake};

            my $file = $c->model('DB::File')->find($doc_intake->{component_id});


            my $subject = $req_params->{ztc_aanvrager_id};

            if (!$subject && $c->session->{_zaak_create}->{aanvragers}) {
                $subject  = $c->session->{_zaak_create}->{aanvragers}->[0]->{betrokkene};
                $subject  = undef if $subject =~ /dummy/;
            } elsif (!$subject) {
                $subject  = $c->session->{_zaak_create}->{ztc_aanvrager_id};
            }

            my %optional;
            if ($req_params->{'intake_document_catalogus'}) {
                $optional{case_document_ids} = [$req_params->{'intake_document_catalogus'}];
            }

            my $origin_date;
            if (   defined($req_params->{'ztc_document_datum'})
                && length($req_params->{'ztc_document_datum'})
            ) {
                $origin_date = assert_date($req_params->{'ztc_document_datum'});
            }

            my $properties = {
                case_id  => $c->stash->{zaak}->id,
                accepted => 1,
                metadata => {
                    description =>
                        $req_params->{'intake_document_help'},

                    defined $req_params->{'ztc_document_richting'} ? (origin => $req_params->{'ztc_document_richting'}) : (),
                    defined $origin_date ? (origin_date => $origin_date) : (),
                },
                %optional
            };

            if (defined $subject) {
                $properties->{ subject } = $c->model('BR::Subject')->find_by_old_subject_identifier($subject);
            }

            $file->update_properties($properties);
        }

        if ($req_params->{actie_automatisch_behandelen}) {
            try {
                $c->stash->{ zaak }->open_zaak;
            } catch {
                $c->log->trace('Case open threw exception: %s', $_);
            };
        }

        if ($req_params->{ assignee_id }) {
            try {

                my $zaak = $c->stash->{zaak};
                $zaak->wijzig_behandelaar({
                    betrokkene_identifier => $req_params->{ assignee_id }
                });

                if ($req_params->{change_allocation_dept}) {
                    my $betrokkene_model = $c->model('Betrokkene');
                    my $user = $betrokkene_model->get({}, $req_params->{assignee_id});

                    my $route_ou = $user->ldap_rs->primary_groups->[-1]->id;

                    my $role = $c->user->find_role_by_name('Behandelaar');
                    $zaak->wijzig_route(
                        {
                            route_ou                 => $route_ou,
                            # In the weird situation that there is no
                            # "behandelaar", default to the current role
                            route_role               => $role ? $role->id : $zaak->route_role,
                            change_only_route_fields => 1,
                        }
                    );
                }

            } catch {
                $c->log->warn("Case assign threw exception: $_");
            };
        }

        # manual settings override the rule behaviour
        my $actie_ou_id   = $c->req->params->{actie_ou_id}   || $c->session->{create_case_allocation_rule}->{ou_id};
        my $actie_role_id = $c->req->params->{actie_role_id} || $c->session->{create_case_allocation_rule}->{role_id};

        if ($actie_ou_id || $actie_role_id) {
            my $action = $c->stash->{ zaak }->case_actions->search({
                type => 'allocation',
                casetype_status_id => $c->stash->{ zaak }->registratie_fase->id
            })->first;

            if($action) {
                my $data = $action->data;

                unless($data->{ ou_id } eq $actie_ou_id && $data->{ role_id } eq $actie_role_id) {
                    $c->log->debug(sprintf(
                        'Updating action %d with new allocation information from intake form (%d, %d)',
                        $action->id,
                        $actie_ou_id,
                        $actie_role_id
                    ));

                    $data->{ ou_id } = $actie_ou_id;
                    $data->{ role_id } = $actie_role_id;
                    $data->{ change_only_route_fields } = 1;

                    $action->label("$actie_ou_id, $actie_role_id");
                    $action->data($data);
                    $action->data_tainted(1);
                }

                # in casetype management screens this isn't set by default - which it should
                # the UI shows a disabled checkbox which is checked. this behaviour is implemented
                # here.
                # the cleanest solution would mean to implement this in case type admin (beheer/zaaktypen/milestone_definitie/ajax_table.tt)
                # and then run a migration script on ALL databases -- this is the part i'm concerned with.
                # maybe combine this with database migration solution -- so that every instance of Zaaksysteem that runs
                # new software version gets the updates once.
                $action->automatic(1);
                $action->state_tainted(1);
                $action->update;
            }

            delete $c->session->{create_case_allocation_rule};
        }
    }


=head2 _create_zaak_handle_contact

When creating a case, set the contact details

=cut

sub _create_zaak_handle_contact : Private {
    my ($self, $c, $create_session)  = @_;

    return unless $create_session;

    my $params = Data::FormValidator->check(
        $create_session, VALIDATION_CONTACT_DATA
    )->valid;

    for my $raw_key (qw/npc-email npc-telefoonnummer npc-mobiel/) {
        my $key = $raw_key;
        $key =~ s/^npc-//;

        my $value = $params->{$raw_key};

        # work around Data::FormValidators omission of optional empty string fields
        $value = '' if exists $create_session->{$raw_key} && $create_session->{$raw_key} eq '';

        if (defined $value) {
            $c->stash->{zaak}->aanvrager_object->$key($value);
        }
    }
}

    sub _create_zaak_handle_uploads : Private {
        my ($self, $c, $params)  = @_;

        return 1 unless ($params && $params->{uploads});

        while (my ($bibliotheek_kenmerken_id, $upload_info) = each %{ $params->{uploads} }) {

            for my $file (@{$upload_info}) {

                my ($path, $filename);
                my $optionals = {};
                if (defined $file->{filestore_id}) {
                    $optionals->{filestore_id} = $file->{filestore_id};
                    $filename = $file->{name};
                }
                elsif ($file->{upload}) {
                    $path     = $file->{upload}->tempname;
                    $filename = $file->{upload}->filename;
                }
                else {
                    next;
                }

                # this will bork when a field is present in multiple phases
                # this is officialy not supported, but in practice "gedoogd"
                # ** kill it with fire **
                # solution: either make it impossible to add a field more than once
                # or go over the codebase and solve these issues.
                my $zaaktype_kenmerken = $c->stash->{zaak}
                    ->zaaktype_node_id
                    ->zaaktype_kenmerken
                    ->search(
                        {
                            'bibliotheek_kenmerken_id.id'           => $bibliotheek_kenmerken_id,
                            'bibliotheek_kenmerken_id.value_type'   => 'file',
                        },
                        {
                            join    => 'bibliotheek_kenmerken_id'
                        }
                    );

                my $zaaktype_kenmerk = $zaaktype_kenmerken->next;
                if (!$zaaktype_kenmerk) {
                    next;
                }
                if ($zaaktype_kenmerken->next) {
                    $self->log->warn("potential problem, ambiguity");
                }

                # Internal files are accepted right away, external files always go into a queue.
                unless ($c->is_externe_aanvraag || $c->check_queue_coworker_changes) {
                    $optionals->{accepted} = 1;
                }

                # Find subject
                my $subject;
                if ($c->user_exists) {
                    $subject = 'betrokkene-medewerker-'.$c->user->uidnumber;
                }
                elsif ($c->req->params->{'ztc_aanvrager_id'}) {
                    $subject = $c->req->params->{'ztc_aanvrager_id'};
                }
                else {
                    $subject = $c->stash->{zaak}->aanvrager_object->rt_setup_identifier;
                }

                # Find document publish type
                my $publish_pip = $zaaktype_kenmerk->pip ? 1 : 0;

                # Create the file
                try {
                    $c->stash->{upload_result_file} = $c->model('DB::File')->file_create({
                        db_params => {
                            created_by              => $subject,
                            case_id                 => $c->stash->{zaak}->id,
                            %$optionals,
                        },
                        case_document_ids => [$zaaktype_kenmerk->id],
                        name              => $filename,
                        file_path         => $path,
                        publish_type_name => $publish_pip,
                    });

                } catch {
                    $params->{upload_error} = $_;
                };
            }
        }
        ### Delete uploads from session
        delete($c->session->{_zaak_create}->{uploads});
    }

    sub _create_load_stash : Private {
        my ($self, $c)  = @_;

        my $aanvragers = $c->stash->{zaaktype_node}->zaaktype_betrokkenen->search;

        $c->stash->{type_aanvragers}    = [];
        while (my $aanvrager = $aanvragers->next) {
            push(
                @{ $c->stash->{type_aanvragers} },
                $aanvrager->betrokkene_type
            );
        }

        $c->stash->{fields}         = $c->stash->{zaaktype}
            ->zaaktype_kenmerken
            ->search(
                {
                    zaak_status_id  => $c->stash->{zaak_status}->id,
                },
                {
                    prefetch    => ['bibliotheek_kenmerken_id', 'zaak_status_id'],
                    order_by    => 'me.id'
                }
            );

        ### Load aanvrager gegevens
        my $betrokkene_id = $c->req->params->{ztc_aanvrager_id};

        if (!$betrokkene_id && $c->session->{_zaak_create}->{aanvragers}) {
            $betrokkene_id  =
                $c->session->{_zaak_create}->{aanvragers}->[0]->{betrokkene};

            if (defined $betrokkene_id && $betrokkene_id =~ /dummy/) {
                $betrokkene_id  = undef;
            }
        } elsif (!$betrokkene_id) {
            $betrokkene_id  = $c->session->{_zaak_create}->{ztc_aanvrager_id};
        }

        if ($betrokkene_id) {
            $c->stash->{aanvrager} = $c->model('Betrokkene')->get(
                {},
                $betrokkene_id
            );

            ### A dirty place to implement, because of the lack of possibilities
            ### in this old Betrokkene object.
            if ($c->stash->{aanvrager} && !$c->stash->{aanvrager}->has_valid_address) {
                my $message = 'Zaak kan niet worden aangevraagd. Geen adres aanwezig.';
                $c->log->error($message);
                $c->push_flash_message($message) unless $self->is_api_environment($c);
                $c->res->redirect($c->uri_for('/'));
                $c->detach;
            }
        }

        # If TwoFactor puts its prefill values in, it's just a hash ref, and
        # we can't figure out the name anyway.
        if ($c->stash->{aanvrager} && blessed($c->stash->{aanvrager})) {
            $c->stash->{aanvrager_naam} = $c->stash->{aanvrager}->naam;
        } elsif (
            $c->session->{_zaak_create}->{aanvrager_update} &&
            $c->session->{_zaak_create}->{aanvrager_update}->{'np-geslachtsnaam'}
        ) {
            my $aanvrager_sess =
                $c->session->{_zaak_create}->{aanvrager_update};

            $c->stash->{aanvrager_naam} = $aanvrager_sess->{'np-voornamen'}
                . ($aanvrager_sess->{'np-voorvoegsel'} ?
                    ' ' . $aanvrager_sess->{'np-voorvoegsel'} : ''
                ) . ' ' . $aanvrager_sess->{'np-geslachtsnaam'};
        }

        ### Load aangevraagd via
        $c->stash->{aangevraagd_via} =
            $c->session->{_zaak_create}->{aangevraagd_via};

        $c->stash->{zaak_acties} =
            $c->session->{_zaak_create}->{acties}
                if $c->session->{_zaak_create}->{acties};

        $c->stash->{aanvraag_trigger} =
            $c->session->{_zaak_create}->{aanvraag_trigger}
                if $c->session->{_zaak_create}->{aanvraag_trigger};

        $c->stash->{aanvrager_type} =
            $c->session->{_zaak_create}->{extern}->{aanvrager_type};

        $c->stash->{logged_in_by} =
            $c->session->{_zaak_create}->{extern}->{verified};

        if (
            $c->session->{_zaak_create}->{acties}->{doc_intake}
        ) {
            $c->stash->{doc_intake} = $c->session
                ->{_zaak_create}
                ->{acties}
                ->{doc_intake};
        }

        $c->stash->{contactchannel} = $c->session->{_zaak_create}->{contactkanaal};


    }

    sub _log_error_and_detach_to_aanvraag_form {
        my ($self, $c, %options) = @_;

        $c->log->warn($options{error});

        $c->stash->{template}   = 'form/aanvraag_error.tt';
        $c->stash->{error}      = {
            titel   => $options{titel} // 'Helaas, deze zaak kan niet worden aangevraagd',
            bericht => $options{human_readable},
        };
        $c->detach;
    }

=head2 _create_load_and_validate_zaaktype

Loads the zaaktype from the session and does validation on it.

=cut

    sub _create_load_and_validate_zaaktype : Private {
        my ($self, $c)  = @_;

        ### Geen zaaktype? detach to list
        my $zaaktype_id = $c->session->{_zaak_create}->{zaaktype_id};
        if (!$c->user_exists && !$zaaktype_id) {
            $c->detach('/logout');
        }
        if ($c->user_exists && !$zaaktype_id) {
            $c->detach();
        }

        my $zaaktype = $c->model('DB::Zaaktype')->find($zaaktype_id);

        if (!$zaaktype) {
            $self->_log_error_and_detach_to_aanvraag_form(
                $c,
                error          => "No Zaaktype ID $zaaktype_id found in create call",
                human_readable => 'Wij kunnen uw aanvraag helaas niet uitvoeren, omdat dit zaaktype niet gevonden kan worden',
            );
        }
        elsif ($zaaktype->deleted) {
            $self->_log_error_and_detach_to_aanvraag_form(
                $c,
                error          => "Casetype with id $zaaktype_id is deleted",
                human_readable => 'Wij kunnen uw aanvraag helaas niet uitvoeren, omdat dit zaaktype inactief is.',
            );
        }
        elsif (!$zaaktype->active) {
            $self->_log_error_and_detach_to_aanvraag_form(
                $c,
                error          => "Casetype with id $zaaktype_id is inactive",
                human_readable => 'Wij kunnen uw aanvraag helaas niet uitvoeren, omdat dit zaaktype inactief is.',
            );
        }

        my $zaaktype_node = $zaaktype->zaaktype_node_id;

        unless ($zaaktype_node) {
            delete($c->session->{_zaak_create}->{zaaktype_id});
            $c->detach;
        }

        if (!$c->user_exists && !$zaaktype_node->webform_toegang) {
            $self->_log_error_and_detach_to_aanvraag_form(
                $c,
                error          => "Casetype with id $zaaktype_id is not enabled for the PIP",
                human_readable => 'Wij kunnen uw aanvraag helaas niet uitvoeren, omdat dit zaaktype niet aanvraagbaar is.',
            );
        }

        my $reg_phase = $zaaktype_node->zaaktype_statussen->search({ status => 1, })->first;

        if (!$reg_phase) {
            $self->_log_error_and_detach_to_aanvraag_form(
                $c,
                error          => "Casetype with id $zaaktype_id is invalid, no phases defined",
                human_readable => 'Wij kunnen uw aanvraag helaas niet uitvoeren, omdat dit zaaktype incompleet is.',
            );
        }

        $c->stash->{zaak_status} = $reg_phase;

        $c->stash->{zaaktype_node}      = $c->stash->{zaaktype}
                                        = $c->stash->{definitie}
                                        = $zaaktype_node;

        $c->stash->{zaaktype_node_id}   = $c->stash->{zaaktype_node}->id;

    }

    sub _create_validation_aanvragers : Private {
        my ($self, $c, $params) = @_;

        ### FIX: Make sure aanvrager validatie werkt, alleen voor interne
        ### aanvraag

        if ($c->req->params->{ztc_aanvrager_id}) {
            $params->{aanvragers} = [{
                'betrokkene'        => $params->{aanvrager_id},
                'verificatie'       => (
                    (
                        $c->session->{_zaak_create}->{aangevraagd_via} eq
                            ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM
                    )
                        ? $c->session->{_zaak_create}->{extern}->{verified}
                        : 'medewerker'
                )
            }];
        } elsif (
            $c->session->{_zaak_create}->{aanvrager_update} &&
            $c->session->{_zaak_create}->{aanvrager_update}->{create} &&
            $c->session->{_zaak_create}->{extern}->{verified}
        ) {
            $params->{aanvragers} = [{
                'create'            =>
                    $c->session->{_zaak_create}->{aanvrager_update},
                'betrokkene_type'   =>
                    $c->session->{_zaak_create}->{extern}->{aanvrager_type},
                'verificatie'       =>
                        $c->session->{_zaak_create}->{extern}->{verified},
            }];

            $c->session->{_zaak_create}->{aanvragers} =
                $params->{aanvragers};

        } elsif ($c->session->{_zaak_create}->{aanvrager_update}) {
            ### XXXX !!!!!!DUMMY!!!!!!
            $params->{aanvragers} = [{
                'betrokkene'        => 'betrokkene-dummy-99999',
                'verificatie'       => (
                    (
                        $c->session->{_zaak_create}->{aangevraagd_via} eq
                            ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM
                    )
                        ? $c->session->{_zaak_create}->{extern}->{verified}
                        : 'medewerker'
                )
            }];
        }

    }

    sub _create_validation_acties : Private {
        my ($self, $c, $params) = @_;

        ### If doc_intake session, check for extra parameters
        if (
            $c->req->params->{doc_intake_update} &&
            $c->session->{_zaak_create}->{acties}->{doc_intake}
        ) {
            $c->session->{_zaak_create}->{acties}->{doc_intake}->{document_catalogus}
                    = $c->req->params->{intake_document_catalogus};

            $c->session->{_zaak_create}->{acties}->{doc_intake}->{document_help}
                    = $c->req->params->{intake_document_help};

            my $v;
            foreach (qw(richting datum)) {
                $v = "ztc_document_$_";
                $c->session->{_zaak_create}->{acties}->{doc_intake}->{$v} = $c->req->params->{$v};
            }

        }


        ### Fill from params
        if ($c->req->params->{actie}) {
            $c->session->{_zaak_create}->{acties} = {};

            $c->session->{_zaak_create}->{acties}->{
                $c->req->params->{actie}
            } = {
                    component       => $c->req->params->{actie},
                    onderwerp       => $c->req->params->{actie_description},
                    component_id    => $c->req->params->{actie_value},
            };
        }
    }

    my $_create_validation_deprecation_map = {
        ztc_trigger         => 'aanvraag_trigger',
        betrokkene_type     => 'betrokkene_type',
        zaaktype            => 'zaaktype_node_id',
        ztc_aanvrager_id    => 'aanvrager_id',
        ztc_contactkanaal   => 'contactkanaal'
    };

    sub _create_validation : Private {
        my ($self, $c)  = @_;
        my $params      = {};

        ### Keys to validate?
        if (scalar(keys( %{ $c->req->params }))) {
            $params        = { %{ $c->req->params } };

            ### Translation
            for my $key (keys %{ $_create_validation_deprecation_map }) {
                $params->{ $_create_validation_deprecation_map->{ $key } }
                    = $params->{$key};

                delete($params->{$key});
            }
        }


        $c->forward('_create_validation_aanvragers', [ $params ]);
        $c->forward('_create_validation_acties', [ $params ]);

        ### Merge session data into params:
        $params->{ $_ } = $c->session->{_zaak_create}->{ $_ }
            for keys %{ $c->session->{_zaak_create} };

        ### Fix registratiedatum
        $params->{registratiedatum} = DateTime->now()
            unless ($params->{registratiedatum} && $c->user_exists);

        my $dv          = Params::Profile->check(
            params  => $params,
            method  => 'Zaaksysteem::Controller::Zaak::create'
        );

        my $validated_options   = $dv->valid;

        ### Depending on xml request (do_validation) or create, we detach
        if ($c->req->is_xhr &&
            $c->req->params->{do_validation} &&
            $c->req->params->{create_entry}
        ) {
            $c->zvalidate($dv);
            $c->detach;
        }

        ### Because we need extra variabled in session, we loop
        ### over the params keys.
        for my $key (keys %{ $validated_options }) {
            $c->session->{_zaak_create}->{ $key } =
            $validated_options->{ $key }
        }

        ###
        ### Betrokkenen
        ###
        for (qw/npc-email npc-telefoonnummer npc-mobiel/) {
            ### ZS-2738 Fix: this fix will always prefer request parameters over
            ### the already set params. Params is, as you can see above, a
            ### merge with $c->session->{_zaak_create}. If a key in this hash
            ### exists, it will overwrite the parameters in $params, and blow
            ### away any user user input given to params (which is a clone of
            ### c.req.params)...
            ###
            ### The real fix is fixing the problem in the merge, but this has
            ### a lot of impact. Scope of this fix is making less problems by fixing
            ### the problem only in contact handling
            if (exists $c->req->params->{$_}) {
                $c->session->{_zaak_create}->{$_} = $c->req->params->{$_};
            } elsif (defined $params->{$_}) {
                $c->session->{_zaak_create}->{$_} = $params->{$_};
            }
        }

        ### Add kenmerken
        $c->session->{_zaak_create}->{raw_kenmerken}
            = $c->session->{_zaak_create}->{form}->{kenmerken};

    }

    sub _create_verify_security : Private {
        my ($self, $c) = @_;

        ### Aangevraagd via correct
        {
            unless (
                grep (
                    { $c->session->{_zaak_create}{aangevraagd_via} eq $_ }
                    ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM,
                    ZAAKSYSTEEM_CONTACTKANAAL_BALIE
                )
            ) {
                $c->log->warn(sprintf(
                    'Aangevraagd_via not one of: %s (but "%s")' .
                    join (', ',
                        ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM,
                        ZAAKSYSTEEM_CONTACTKANAAL_BALIE
                    ),
                    $c->session->{_zaak_create}{aangevraagd_via}
                ));
                $c->detach;
            }

            unless (
                (
                    $c->session->{_zaak_create}->{aangevraagd_via} eq
                        ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM &&
                    !$c->user_exists
                ) ||
                (
                    $c->session->{_zaak_create}->{aangevraagd_via} ne
                        ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM &&
                    $c->user_exists
                )
            ) {
                $c->log->error(
                    $c->session->{_zaak_create}->{aangevraagd_via}
                    . ' aanvraag niet via logged in user, impossible.'
                );
                $c->detach;
            }

            if ($c->is_externe_aanvraag) {
                $c->session->{_zaak_create}->{aangevraagd_via} =
                    $c->session->{_zaak_create}->{contactkanaal} =
                        ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM;
            }
        }
    }
}

sub _spiffy_spinner : Private {
    my ($self, $c, $definition) = @_;

    return 1 unless ($c->req->is_xhr &&
        $c->req->params->{spiffy_spinner}
    );

    $c->stash->{json} = {
        'spinner'    => $definition,
    };
    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

sub duplicate : Local {
    my ($self, $c, $zaakid) = @_;

    unless ($c->req->param('confirmed')) {
        $c->stash->{ confirmation }{ message } = sprintf(
            'Weet u zeker dat u deze zaak (%d) wilt kopi&euml;ren?',
            $zaakid
        );

        $c->stash->{confirmation}{type} = 'yesno';
        $c->stash->{confirmation}{uri} = $c->uri_for(sprintf('/zaak/duplicate/%d', $zaakid));

        $c->detach('/page/confirmation');
    } else {
        my $zaak = $c->model('DB::Zaak')->duplicate($zaakid, { simpel  => 1 });
        $zaak->create_default_directories();

        if (!$zaak) {
            my $msg = 'De zaak kon niet gekopie&euml;rd worden.';

            $c->stash->{zapi} = Zaaksysteem::ZAPI::Error->new(
                type     => 'zaak/duplicate',
                messages => $msg,
            );
            $c->detach($c->view('ZAPI'));
        }

        # Small real object like output, with minimal info
        $c->stash->{zapi} = [
            {
                instance => {
                    number => $zaak->id,
                },
                type      => 'case',
                reference => $zaak->object_data->id
            }
        ];
        $c->detach($c->view('ZAPI'));
    }

    $c->detach;
}

# TODO rebase this as chained from the 'base' action in this module
sub meta_info : Chained('/'): PathPart('zaak/get_meta'): Args(1) {
    my ($self, $c, $id) = @_;

    return unless $id =~ /^\d+$/;

    ### Retrieve zaak
    $c->stash->{nowrapper} = 1;
    $c->stash->{'zaak'} = $c->model('DB::Zaak')->find($id);

    $c->stash->{template} = 'zaak/metainfo.tt';
}

sub start_nieuwe_zaak : Chained('/'): PathPart('zaak/start_nieuwe_zaak'): Args() {
    my ($self, $c, $zaaktype_id, $copy_properties) = @_;

    my $zaaktype = $c->model('DB::Zaaktype')->find($zaaktype_id);

    my $new_params = {
        'create'                => '1',
        'create_entry'          => '1',
        'sessreset'             => 1,
        'zaaktype_id'           => $zaaktype->id,
        'zaaktype_name'         => $zaaktype->zaaktype_node_id->titel,
        'ztc_contactkanaal'     => $c->session->{_zaak_create}->{contactkanaal},
        'jstrigger'             => $c->session->{_zaak_create}->{aanvraag_trigger},
        'ztc_trigger'           => $c->session->{_zaak_create}->{aanvraag_trigger},
        'ztc_aanvrager_id'      => $c->session->{_zaak_create}->{ztc_aanvrager_id},
        'betrokkene_type'       => $c->session->{_zaak_create}->{aanvragers}->[0]->{verificatie},
        'ztc_aanvrager_type'    => $c->session->{_zaak_create}->{ztc_aanvrager_type}
    };

    if(
        exists $c->session->{_zaak_create}->{extern} &&
        $c->session->{_zaak_create}->{extern}->{id} &&
        $c->session->{_zaak_create}->{extern}->{aanvrager_type} &&
        $c->session->{_zaak_create}->{extern}->{verified}
    ) {

        if ($c->session->{_zaak_create}->{extern}->{verified} eq 'preset_client') {
            $new_params->{aanvrager} = $zaaktype->zaaktype_node_id->zaaktype_definitie_id->preset_client;
            $new_params->{ztc_aanvrager_type} = 'unknown';

        } else {
             $new_params->{ztc_aanvrager_type} =
                 $c->session->{_zaak_create}->{extern}->{aanvrager_type};

            if (
                $c->session->{_zaak_create}->{extern}->{id} &&
                $c->session->{_zaak_create}->{extern}->{id} =~ /^betrokkene-/
            ) {
                $new_params->{ztc_aanvrager_id} =
                    $c->session->{_zaak_create}->{extern}->{id};
            }

            $new_params->{verified} =
                $c->session->{_zaak_create}->{extern}->{verified};

            if($new_params->{ztc_aanvrager_type} eq 'natuurlijk_persoon') {
                $new_params->{authenticatie_methode} = 'digid';
            } elsif($new_params->{ztc_aanvrager_type} eq 'niet_natuurlijk_persoon') {
                $new_params->{authenticatie_methode} = 'bedrijfid';
            }
        }
     }

    if (
        !$new_params->{ztc_aanvrager_id} &&
        $c->session->{_zaak_create}->{aanvragers} &&
        $c->session->{_zaak_create}->{aanvragers}->[0]->{betrokkene} &&
        $c->session->{_zaak_create}->{aanvragers}->[0]->{betrokkene} =~ /^betrokkene-/
    ) {
        $new_params->{ztc_aanvrager_id} = $c->session->{_zaak_create}->{aanvragers}->[0]->{betrokkene};
    }


    if($copy_properties) {
        $c->log->debug("copying properties forward");
        unless($c->session->{SAVED_CASE_PROPERTIES}) {
            $c->session->{SAVED_CASE_PROPERTIES} = clone $c->session->{_zaak_create}->{form}->{kenmerken};
        }
    }

    my $aangevraagd_via = $c->session->{_zaak_create}->{aangevraagd_via};

    $c->res->redirect($c->uri_for('/zaak/create/' . $aangevraagd_via, $new_params));
    $c->detach;
}

sub _execute_regels : Private {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    my $status = $params->{status} || $params->{fase};

    my $rules_result = $c->stash->{regels_result} = $c->stash->{zaak}->execute_rules({
        status => $status,
        cache  => 0,
    });

    # Will contain a map of bibliotheek_kenmerken_id's mapped to values the
    # items should be set to.
    my %new_values;
    my $current_values = $c->stash->{ zaak }->field_values;

    if(my $vul_waarde_in = $rules_result->{ vul_waarde_in }) {
        while(my ($id, $action) = each %$vul_waarde_in) {
            $new_values{ $id } = $action->{ value };
        }

    }

    if (my $kenmerk_formule = $rules_result->{ vul_waarde_in_met_formule }) {
        while (my ($id, $code) = each %$kenmerk_formule) {
            $new_values{ $id } = $code->($c->stash->{ zaak });
        }
    }

    # Cleanup non-updates
    for (keys %new_values) {
        next unless exists $current_values->{ $_ };
        delete $new_values{ $_ } if $new_values{ $_ } eq $current_values->{ $_ };
    }

    # Only update if we must.
    if (scalar keys %new_values) {
        $c->stash->{ zaak }->zaak_kenmerken->update_fields({
            new_values => \%new_values,
            zaak       => $c->stash->{ zaak },
        });
    }

    my $referential_rs = $c->stash->{zaak}
        ->zaaktype_node_id
        ->zaaktype_kenmerken
        ->search(
        {
            referential     => { '-not' => undef },
            zaak_status_id  => $c->stash->{requested_fase}->id,
        }
    );

    if($c->stash->{zaak}->get_column('pid') && $referential_rs->count) {
        # so we have referential fields in this phase. let's see which fields are
        # visible and hidden in the parent case, so we can do the same for this case

        my $parent_case = $c->stash->{zaak}->pid;

        ### Loop over all statussen of parent_case
        $c->stash->{parent_visible_fields} = {};
        for my $parent_status ($parent_case->zaaktype_node_id->zaaktype_statussen->all) {
            my $status_visible_fields = $parent_case->visible_fields({
                field_values    => $parent_case->field_values(),
                phase           => $parent_status,
                include_documents => 1
            });

            $c->stash->{parent_visible_fields}->{$_} = 1 for keys %{ $status_visible_fields };
        }
    }
}

sub fields : Chained('base'): PathPart('fields') {
    my ($self, $c, $options) = @_;

    $c->forward('_execute_regels');

    $c->forward('empty_hidden_fields') if $options->{empty_hidden_fields};

    $c->forward('check_lock_registration_phase');

    $self->show_subcase_in_pip($c);

    $c->stash->{milestone} = $c->req->params->{fase};

    $c->stash->{nowrapper}  = 1;
    $c->stash->{template}   = 'zaak/fields.tt';
}


=head2 check_lock_registration_phase

There is a setting per case type "Zaak acties > Registratiefase vergrendelen", the purpose
is to avoid unintended changes to subject data. The worst culprit would be changing an
empty radio buttons to a setting, since there is no way to change it back.

This shows the registration phase in a view-mode, and the user has to unlock it
to change the fields.

This can be overridden in the session.

=cut

sub check_lock_registration_phase : Private {
    my ($self, $c) = @_;

    my $params = $c->req->params;
    my $case = $c->stash->{zaak};

    # unlock the registration phase, store in session.
    # effective immediately
    if ($params->{unlock_registration_phase}) {
        $c->session->{unlock_registration_phase}->{$case->id} = 1;
    }

    # re-lock, after making changes
    if ($params->{lock_registration_phase}) {
        delete $c->session->{unlock_registration_phase}->{$case->id};
    }

    # determine lock status
    if (
        $params->{fase} eq '1' &&
        $case->zaaktype_property('lock_registration_phase')
    ) {
        if ($c->session->{unlock_registration_phase}->{$case->id}) {
            $c->stash->{registration_phase_unlocked} = 1;
        } else {
            $c->stash->{registration_phase_locked} = 1;
        }
    }
}

#
# when a field value is changed, update it into the database
#
sub update_field : JSON : Chained('base'): PathPart('update_field'): Args(0) {
    my ($self, $c) = @_;

    my $params  = $c->req->params;
    my $field       = $params->{field};
    my $rule_field  = $params->{rule_field};
    my $fields;
    my %attributes;

    ### Prevent unnecessary post
    die "unconfirmed post" unless lc($c->req->method) eq 'post' && $params->{confirmed_post};

    if(!$field || $field eq 'undefined') {
        $c->stash->{json} = { result => 1 };
        $c->detach('Zaaksysteem::View::JSON');
    } else {
        if(ref($field) ne 'ARRAY') {
            $fields = [ $field ];
        } else {
            $fields = $field->[0];
        }
    }

    foreach my $f ($fields) {
        # result can be updated even if the case is closed
        if($f eq 'system_kenmerk_resultaat') {

            die "can't change case" unless
                $c->check_any_zaak_permission('zaak_beheer') || $c->can_change;

            my $value = $params->{$f} or die "need result value";

            $c->stash->{zaak}->set_resultaat($value); # XXX dit moet op basis van result_id, eigenlijk

            $c->stash->{zaak}->update;

        } elsif($f =~ m|^kenmerk_id_\d+$|) {

            my ($bibliotheek_kenmerken_id) = $f =~ m|^kenmerk_id_(\d+)|;

            my @values = $c->req->param($f);

            $attributes{$bibliotheek_kenmerken_id} = \@values;
        }
    }

    if(scalar(keys %attributes)) {
        $c->stash->{zaak}->zaak_kenmerken->update_fields(
            {
                new_values  => \%attributes,
                zaak        => $c->stash->{zaak},
            }
        );
    }

    if ($rule_field) {
        $c->detach('fields', [{
            empty_hidden_fields => 1
        }]);
    }

    $c->stash->{zaak}->touch;

    $c->stash->{json} = { result => 1 };

    $c->forward('Zaaksysteem::View::JSON');
}



sub empty_hidden_fields : Private {
    my ($self, $c) = @_;

    my $rules_result = $c->stash->{regels_result} or die "need rules executed at this point";

    if (my $hidden_fields = $rules_result->{verberg_kenmerk}) {

        $c->stash->{zaak}->zaak_kenmerken->delete_fields({
            bibliotheek_kenmerken_ids => [keys %$hidden_fields],
            zaak => $c->stash->{zaak},
        });

        $c->stash->{zaak}->touch;
    }
}

=head2 open

Allow a user to become the owner of a case, and set the status to 'open'.

=cut

sub open : Chained('base'): PathPart('open'): Args(0) {
    my ($self, $c) = @_;

    # Only open(/assign) a zaak if it isn't already closed. This fixes
    # a case being reopened by accident. (With double tabs, for example.)
    if (!$c->stash->{zaak}->is_afgehandeld) {
        $c->assert_any_zaak_permission('zaak_read', 'zaak_edit', 'zaak_beheer');

        my $logging_id = $c->stash->{zaak}->open_zaak;
        $c->push_flash_message(
            'Zaak is door u in behandeling genomen (<a href="/zaak/' . $c->stash->{zaak}->id .
            '/undo/' . $logging_id . '">Ongedaan maken</a>)'
        );

    }

    $c->response->redirect('/zaak/' . $c->stash->{zaak}->nr);
    $c->detach;
}

sub undo_open : Chained('base'): PathPart('undo'): Args() {
    my ($self, $c, $logging_id) = @_;

    if($c->stash->{zaak}->undo({ logging_id => $logging_id })) {
        $c->push_flash_message('Zaak in behandeling nemen ongedaan gemaakt.');
    }

    $c->response->redirect('/');
    $c->detach;
}



sub unrelate : Chained('base') : PathPart('unrelate') : Args(1) {
    my ($self, $c, $related_case_id) = @_;

    $c->assert_any_zaak_permission('zaak_edit', 'zaak_beheer');

    $c->stash->{ zaak }->unrelate($related_case_id);

    $c->push_flash_message('Zaak relatie verwijderd');

    $c->response->redirect('/zaak/' . $c->stash->{ zaak }->nr);
    $c->detach;
}


sub view : Chained('base'): PathPart(''): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{page_title} = 'Zaak ' . $c->stash->{zaak}->id;

    ### Find fase
    {
        my $fase = $c->req->params->{fase} || '';
        if ($fase =~ /^\d+$/) {
            my $fases = $c->stash->{zaak}->fasen->search(
                {
                    status  => $fase
                }
            );

            $c->stash->{requested_fase} = $fases->first if $fases->count;
        }
    }

    $c->stash->{rule_engine} = sub {
        my $params = shift;
        return Zaaksysteem::Backend::Rules->generate_object_params($params)
    };

    for my $message ($c->stash->{zaak}->display_flash_messages) {
        $c->push_flash_message($message);
    }

    ### Get external document generator. The module is hardcoded: a better method is needed when moving
    ### the documents to the new "zaakdossier"
    my $ext_generator = $c->model('DB::Interface')->search_active({ 'module' => ['xential'] })->first;


    if ($ext_generator && $ext_generator->jpath('$.editservice')) {
        $c->stash->{external_document_generators} = $ext_generator->module;
    }
}


{
    my $ELEMENT_MAP = {
        'zaak-elements-notes'       =>
            'zaak/elements/notes.tt',
        'zaak-elements-status'      =>
            'zaak/elements/status.tt',
        'load_algemene_zaakinformatie'      =>
            'zaak/elements/view_algemene_zaakinformatie.tt',
        'load_element_maps'      =>
            'zaak/elements/view_maps.tt',
    };

    sub view_element : Chained('base'): PathPart('view_element'): Args(1) {
        my ($self, $c, $element) = @_;

        unless ($ELEMENT_MAP->{$element}) {
            $c->res->redirect($c->uri_for(
                '/zaak/' . $c->stash->{zaak}->nr
            ));

            $c->detach;
        }

        $c->stash->{nowrapper}  = 1;
        $c->stash->{template}   = $ELEMENT_MAP->{$element};
    }
}

sub intake : Chained('/'): PathPart('zaak/intake'): Args(0) {
    my ($self, $c) = @_;

    my $view    = $c->req->params->{view};

    $c->stash->{'template'} = 'zaak/intake.tt';

    my $bid     = $c->user->uidnumber;

    $c->stash->{'dropped_documents'} = $c->model('DB::File')->search({
        accepted => 0,
        case_id => undef,
        subject_id => undef
    }, {
        order_by => { '-desc' => 'id'}
    });
}

sub zaaktypeinfo : Chained('base'): PathPart('zaaktypeinfo'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'zaak/zaaktypeinfo.tt'
}

sub fresh : Chained('/') : PathPart('fresh') : Args() {
    my ($self, $c, $nowrapper) = @_;

    $c->stash->{template} = 'zaak/fresh.tt';
    $c->stash->{nowrapper} = $nowrapper;
}

sub case_documents : JSON : Chained('base') : PathPart('case_documents') {
    my ($self, $c) = @_;
    my $case = $c->stash->{zaak};

    my $case_type_properties = $case->zaaktype_node_id->zaaktype_kenmerken;

    my $case_documents = $case_type_properties->search(
        { 'bibliotheek_kenmerken_id.value_type' => 'file', },
        {
            join     => ['bibliotheek_kenmerken_id',],
            prefetch => [
                'zaaktype_node_id',
                'bibliotheek_kenmerken_id'
            ]
        }
    );

    $c->stash->{json} = $case_documents;
    $c->forward('Zaaksysteem::View::JSON');
}

sub get_sjablonen : JSON : Chained('base') : PathPart('get_sjablonen') {
    my ($self, $c) = @_;
    my $sjablonen = [];
    my $case = $c->stash->{zaak};

    if (
        $c->req->params->{type} &&
        $c->req->params->{type} eq 'notifications'
    ) {
        $sjablonen = $c->model('DB::ZaaktypeNotificatie')->search_rs(
            {
                zaaktype_node_id => $case->get_column('zaaktype_node_id')
            },
            {
                prefetch => ['bibliotheek_notificaties_id'],
            }
        );
    } else {
        # my $case_type_sjablonen = $case->zaaktype_node_id->zaaktype_sjablonen;

        $sjablonen = $c->model('DB::ZaaktypeSjablonen')->search_rs(
            {
                zaaktype_node_id => $case->get_column('zaaktype_node_id')
            },
            {
                prefetch => 'bibliotheek_sjablonen_id'
            }
        );
    }

    $c->stash->{json} = $sjablonen;

    {
        no warnings 'redefine';

        local *Zaaksysteem::DB::Component::ZaaktypeNotificatie::TO_JSON = sub {
            my $self    = shift;

            return {
                (map { $_ => ($self->get_column($_) // undef) } keys %{ { $self->get_columns } }),
                bibliotheek_notificaties_id => {
                    (map { $_ => ($self->bibliotheek_notificaties_id->get_column($_) // undef) } keys %{ { $self->bibliotheek_notificaties_id->get_columns } })
                },
                betrokkene_naam => $self->betrokkene_naam,
            };

        };

        local *Zaaksysteem::DB::Component::ZaaktypeSjablonen::TO_JSON = sub {
            my $self    = shift;

            return {
                (map { $_ => ($self->get_column($_) // undef) } keys %{ { $self->get_columns } }),
                bibliotheek_sjablonen_id => {
                    (map { $_ => ($self->bibliotheek_sjablonen_id->get_column($_) // undef) } keys %{ { $self->bibliotheek_sjablonen_id->get_columns } })
                }
            };

        };

        $c->forward('Zaaksysteem::View::JSON');
    }
}

sub _get_subject {
    my ($c) = @_;
    return $c->model('Betrokkene')->get(
        {
            intern  => 0,
            type    => 'medewerker',
        },
        $c->user->uidnumber,
    );
}

sub _resolve_recipient {
    my ($c, $recipient, $recipient_type, $case, $role) = @_;

    # 'Resolve' the recipient to a usable address
    if ($recipient_type eq 'aanvrager') {
        $recipient = $case->aanvrager_object->email;
    }

    if ($recipient_type eq 'coordinator') {
        $recipient = $case->coordinator_object->email;
    }

    if ($recipient_type eq 'gemachtigde') {
        $recipient = join(";", map { $_->email } $case->pip_authorized_betrokkenen);
    }

    if ($recipient_type eq 'betrokkene') {
        $recipient = join(";", map { $_->email } $case->get_betrokkene_objecten({'LOWER(rol)' => lc($role)}));
    }

    if ($recipient_type eq 'medewerker_uuid') {
        $recipient = $c->model('Betrokkene')->get(
            {
                intern  => 0,
                type    => 'medewerker',
            },
            $recipient,
        )->email;
    }

    if ($recipient_type eq 'overig') {
        my $ztt = Zaaksysteem::ZTT->new;
        $ztt->add_context($case);

        $recipient = $ztt->process_template($recipient)->string;
    }

    return $recipient;
}

sub deelzaken : Chained('base'): PathPart('deelzaken') {
    my ($self, $c) = @_;

    $c->stash->{nowrapper}  = 1;
    $c->stash->{template}   = 'zaak/elements/deelzaken.tt';
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 SAVED_CASE_PROPERTIES

TODO: Fix the POD

=cut

=head2 VALIDATION_CONTACT_DATA

TODO: Fix the POD

=cut

=head2 base

TODO: Fix the POD

=cut

=head2 case_documents

TODO: Fix the POD

=cut

=head2 case_information

TODO: Fix the POD

=cut

=head2 create

TODO: Fix the POD

=cut

=head2 create_base

TODO: Fix the POD

=cut

=head2 create_redirect

TODO: Fix the POD

=cut

=head2 deelzaken

TODO: Fix the POD

=cut

=head2 duplicate

TODO: Fix the POD

=cut

=head2 eenheid

TODO: Fix the POD

=cut

=head2 empty_hidden_fields

TODO: Fix the POD

=cut

=head2 fields

TODO: Fix the POD

=cut

=head2 fresh

TODO: Fix the POD

=cut

=head2 get_sjablonen

TODO: Fix the POD

=cut

=head2 handle_fase_acties

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

=head2 intake

TODO: Fix the POD

=cut

=head2 list

TODO: Fix the POD

=cut

=head2 meta_info

TODO: Fix the POD

=cut

=head2 own

TODO: Fix the POD

=cut

=head2 preprocess_attributes

TODO: Fix the POD

=cut

=head2 start_nieuwe_zaak

TODO: Fix the POD

=cut

=head2 test_magic_strings

TODO: Fix the POD

=cut

=head2 undo_open

TODO: Fix the POD

=cut

=head2 unrelate

TODO: Fix the POD

=cut

=head2 update_field

TODO: Fix the POD

=cut

=head2 view

TODO: Fix the POD

=cut

=head2 view_element

TODO: Fix the POD

=cut

=head2 zaaktypeinfo

TODO: Fix the POD

=cut

