package Zaaksysteem::Controller::Zaak::Notes;

use Moose;

use Zaaksysteem::Constants qw/LOGGING_COMPONENT_NOTITIE/;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

sub index : JSON : Chained('/zaak/base') : PathPart('notes'): Args(0) {
    my ( $self, $c ) = @_;

    ### TODO, depending on reuqest, show notes wrapper
    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'zaak/elements/notes.tt';
}

=head1 create_note

Create a note on a case object.

=head2 URL construction

C</zaak/B<[case_id]>/notes/create>

=head2 Parameters

=over 4

=item content (string, I<required>)

The text content of the note to be created

=back

=head2 Responses

=over 4

=item HTTP 201

Note was succesfully created. JSON body may be ignored.

=item HTTP 405

Incorrect request method, this URL requires HTTP POST.

=item HTTP 400

No content specified

=back

=head2 Events

=over 4

=item C<case/note/create>

=back

=cut

sub create_note : Chained('/zaak/base') : PathPart('notes/create') : Args(0) {
    my ($self, $c) = @_;

    unless($c->req->method eq 'POST') {
        throw('request/method', "Invalid request method, should be POST.");
    }

    unless($c->req->param('content')) {
        throw('request/parameter/missing', "Missing the `content` request parameter.");
    }

    my $event = $c->stash->{zaak}->trigger_logging(
        'case/note/create',
        {
            component => 'zaak',
            data      => {
                case_id => $c->stash->{zaak}->id,
                content => $c->req->param('content')
            }
        }
    );
    $c->stash->{json} = $event->discard_changes;

    $c->stash->{zaak}->create_message_for_behandelaar(
        log        => $c->stash->{json},
        event_type => 'case/note/create',
        message    => "Notitie toegevoegd",
    );

    $c->detach('Zaaksysteem::View::JSON');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 create_note

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

