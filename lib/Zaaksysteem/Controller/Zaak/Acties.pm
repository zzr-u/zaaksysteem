package Zaaksysteem::Controller::Zaak::Acties;
use Moose;

use HTML::Entities qw(encode_entities);

use Zaaksysteem::Backend::Tools::Term qw/calculate_term/;
use Zaaksysteem::Constants;
use Zaaksysteem::Email::Planned;
use Zaaksysteem::Profiles;
use BTTW::Tools;
use Zaaksysteem::ZAPI::Error;

BEGIN { extends 'Zaaksysteem::Controller' }

use constant BULK_UPDATE_SIMPLE_TABLE_CONFIG         => {
    'header'    => [
        {
            label   => 'Kenmerk',
            mapping => 'kenmerk_naam',
        },
        {
            label   => 'Waarde',
            mapping => 'kenmerk_value',
        },
    ],
    'options'   => {
        data_source         => '/bulk/update/kenmerken',
        row_identifier      => 'kenmerk_identifier',
        has_delete_button   => 1,
        add_class           => 'ezra_bulk_update_kenmerken_add',
        search_action       => '/beheer/bibliotheek/kenmerken/search',
        init                => 1,
        add                 => {
            label   => 'Kenmerk toevoegen',
            popup   => 1,
        },
        add_class           => 'ezra_bulk_update_kenmerken_add',
        dialog              => '#searchdialog',
    }
};

#### ACTIES VOOR OVERZICHT
{
    Params::Profile->register_profile(
        method  => 'weiger',
        profile => {
        }
    );

    sub weiger : Chained('/zaak/base'): PathPart('actie/weiger'): Args(0) {
        my ($self, $c) = @_;

        $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');


        ### VAlidation
        if ($c->req->is_xhr &&
            $c->req->params->{do_validation}
        ) {
            $c->zvalidate;
            $c->detach;
        }

        ### Post
        if (
            %{ $c->req->params } &&
            $c->req->params->{confirmed}
        ) {
            $c->res->redirect(
                $c->uri_for('/')
            );

            ### Confirmed
            #my $dv;
            #return unless $dv = $c->zvalidate;

            if ($c->stash->{zaak}->behandelaar) {
                $c->log->info(
                    'Zaak::Acties->weiger ['
                    . $c->stash->{zaak}->nr . ']: behandelaar removed'
                );

                if (
                    $c->stash->{zaak}->coordinator &&
                    $c->stash->{zaak}->coordinator->gegevens_magazijn_id eq
                        $c->stash->{zaak}->behandelaar->gegevens_magazijn_id
                ) {
                    $c->stash->{zaak}->coordinator(undef);
                    $c->stash->{zaak}->coordinator_gm_id(undef);
                }

                $c->stash->{zaak}->behandelaar_gm_id(undef);
                $c->stash->{zaak}->behandelaar(undef);
                $c->stash->{zaak}->update;
            }

            my $start_config = $c->customer_instance;

            if (
                defined $start_config->{deny_zaak} &&
                $start_config->{deny_zaak}->{organizationalUnit} &&
                $start_config->{deny_zaak}->{posixGroup}
            ) {
                $c->log->debug(
                    'Change route_ou and route_role because of deny_zaak'
                );

                my $route_ou    = $c->model('Users')->find(
                    $start_config->{deny_zaak}->{organizationalUnit}
                );
                my $route_role  = $c->model('Users')->find(
                    $start_config->{deny_zaak}->{posixGroup}
                );

                if ($route_ou && $route_role) {
                    $c->stash->{zaak}->route_ou($route_ou->get_value('l'));
                    $c->stash->{zaak}->route_role($route_role->get_value('gidNumber'));
                    $c->stash->{zaak}->update;
                }

                $c->log->info(
                    'Zaak::Acties->weiger ['
                    . $c->stash->{zaak}->nr . ']: route_ou_role removed'
                );
            }

            $c->detach;
        }

        $c->stash->{confirmation}->{message}    =
            'Weet u zeker dat u deze zaak wilt weigeren?';

        $c->stash->{confirmation}->{type}       = 'yesno';
        $c->stash->{confirmation}->{uri}        =
            $c->uri_for(
                '/zaak/'
                . $c->stash->{zaak}->nr
                . '/actie/weiger'
            );


        $c->forward('/page/confirmation');
        $c->detach;
    }
}


#### ACTIES
{
    Params::Profile->register_profile(
        method  => 'wijzig_route',
        profile => {
        }
    );

    sub wijzig_route : Chained('/zaak/base'): PathPart('update/afdeling'): Args(0) {
        my ($self, $c) = @_;

        $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

        if (!defined $c->req->params->{ou_id} || !defined $c->req->params->{role_id} || !$c->req->params->{confirmed}) {
            $c->stash->{nowrapper} = 1;
            $c->stash->{template}  = 'zaak/widgets/wijzig_route.tt';
            $c->detach;
        }
        my $group = $c->user->find_group_by_id($c->req->params->{ou_id});
        my $role  = $c->user->find_role_by_id($c->req->params->{role_id});

        if (!$group || !$role) {
            $c->stash->{nowrapper} = 1;
            $c->stash->{template} = 'zaak/widgets/wijzig_route.tt';
            $c->detach;
        }

        my $route = $c->stash->{zaak}->wijzig_route(
            $c->req->params->{ou_id},
            $c->req->params->{role_id}
        );

        if ($route) {
            my $event = $c->stash->{ zaak }->logging->trigger('case/update/department', {
                component => 'zaak',
                data => {
                    case_id => $c->stash->{ zaak }->id,
                    ou_dn => $group->name,
                    role_dn => $role->name
                }
            });

            $c->push_flash_message($event->onderwerp);
            $c->res->redirect('/zaak/' . $c->stash->{zaak}->nr);
        }
        $c->detach;
    }
}

{
    sub set_betrokkene_suggestion : Chained('/zaak/base'): PathPart('update/betrokkene/suggestion'): Args(0) {
        my ($self, $c) = @_;

        my $suggestion = $c->stash
            ->{zaak}
            ->betrokkenen_relateren_magic_string_suggestion(
                $c->req->params
            );

        unless ($suggestion) {
            $c->res->body('NOK');
            return;
        }

        $c->res->body($suggestion);
    }


    sub set_betrokkene : Chained('/zaak/base'): PathPart('update/betrokkene'): Args(0) {
        my ($self, $c) = @_;

        $c->stash->{rollen} = [
            @{ BASE_RELATION_ROLES() },
            @{ $c->model('DB::Config')->get_value('custom_relation_roles') || [] }
        ];

        my $dv = $c->forward('/page/dialog', [{
            validatie       => BETROKKENE_RELATEREN_PROFILE,
            permissions     => [qw/zaak_beheer zaak_edit/],
            template        => 'widgets/betrokkene/create_relatie.tt',
            complete_url    => $c->uri_for('/zaak/'. $c->stash->{zaak}->id)
        }]);

        return unless $dv;

        my $params = $dv->valid;

        my $event = $c->stash->{ zaak }->betrokkene_relateren($params);

        if($event) {
            $c->push_flash_message($event->onderwerp);
        } else {
            $c->push_flash_message({ type => 'error', message => sprintf(
                'Betrokkene kon niet gerelateerd worden.'
            )});
        }

        return unless $params->{ send_auth_confirmation } && $params->{ pip_authorized };

        my $template_id = $c->model('DB::Config')->get('subject_pip_authorization_confirmation_template_id');

        unless($template_id) {
            $c->push_flash_message({ type => 'error', message => sprintf(
                'Kon geen e-mail notificatie template vinden, geen notificatie verstuurd'
            )});

            return;
        }

        my $template = $c->model('DB::BibliotheekNotificaties')->find($template_id);

        unless($template) {
            $c->push_flash_message({ type => 'error', message => sprintf(
                'Notificatie template kon niet gevonden worden, geen e-mail notificatie verstuurd'
            )});

            return;
        }

        my $subject = $c->model('Betrokkene')->get_by_string($params->{ betrokkene_identifier });

        unless(defined $subject) {
            throw('case/add_subject', sprintf(
                'Subject with identifier "%s" could not be found, this can\'t be right.',
                $params->{ betrokkene_identifier }
            ));
        }

        unless($subject->email) {
            $c->push_flash_message({ type => 'error', message => sprintf(
                'Geen e-mail adres gevonden voor "%s", geen notificatie verstuurd',
                $params->{ betrokkene_identifier }
            )});

            return;
        }

        $c->stash->{ zaak }->mailer->send_case_notification({
            notification => $template,
            recipient => $subject->email
        });
    }
}

sub wijzig_vernietigingsdatum : Chained('/zaak/base'): PathPart('update/vernietigingsdatum'): Args(0) {
    my ($self, $c) = @_;

    if (
        my $dv = $c->forward('/page/dialog', [{
            validatie       => ZAAK_WIJZIG_VERNIETIGINGSDATUM_PROFILE,
            permissions     => [qw/zaak_beheer/],
            template        => 'zaak/widgets/wijzig_vernietigingsdatum.tt',
            complete_url    => $c->uri_for('/zaak/'. $c->stash->{zaak}->id)
        }])
    ) {
        my $params  = $dv->valid;

        if (
            $c->stash->{zaak}->wijzig_vernietigingsdatum(
                $params
            )
        ) {
            my $event = $c->stash->{ zaak }->logging->trigger('case/update/purge_date', {
                component => 'zaak',
                data => {
                    case_id => $c->stash->{ zaak }->id,
                    purge_date => $params->{ vernietigingsdatum }->dmy,
                    reason => $params->{ reden }
                }
            });

            $c->push_flash_message($event->onderwerp);

            $c->log->info(
                'Zaak[' . $c->stash->{zaak}->id . '] wijzig vernietigingsdatum: ' . $event->onderwerp
            );
        }
    }
}


{
    Params::Profile->register_profile(
        method  => 'wijzig_zaaktype',
        profile => {
        }
    );

    sub wijzig_zaaktype : Chained('/zaak/base'): PathPart('update/zaaktype'): Args(0) {
        my ($self, $c) = @_;

        $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

        if ($c->req->params->{zaaktype_id} && $c->req->params->{confirmed}) {
            my $zaak = $c->stash->{zaak}->wijzig_zaaktype({
                zaaktype_id    => $c->req->params->{zaaktype_id}
            });
            if ($zaak) {
                $c->push_flash_message(sprintf('Zaaktype %s succesvol gewijzigd, nieuw zaaknummer: %d',
                    $c->stash->{ zaak }->nr,
                    $zaak->id
                ));

                $c->res->redirect('/zaak/' . $zaak->nr);
            } else {
                $c->res->redirect('/zaak/' . $c->stash->{zaak}->nr);
            }
            $c->detach;
        } else {
            $c->stash->{nowrapper} = 1;
            $c->stash->{template} = 'zaak/widgets/wijzig_zaaktype.tt';
            $c->detach;
        }
    }
}


Params::Profile->register_profile(
    method  => 'set_allocation',
    profile => {
        required => [qw[selection selected_case_ids]],
        optional => [qw[notify]],
        dependencies => {
            change_allocation   => sub {
                my $dfv     = shift;
                my $type    = shift;

                if ($type eq 'behandelaar') {
                    return [ 'betrokkene_id' ];
                } else {
                    return [ 'ou_id' , 'role_id' ];
                }
            }

        }
    },
);
sub set_allocation : Chained('/zaak/base'): PathPart('update/allocation'): Args(0) {
    my ($self, $c) = @_;

    $self->log->trace('Zaaksysteem::Controller::Zaak::Acties.set_allocation');

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    my $params = $c->req->params;
    if ($params->{ change_allocation } eq 'behandelaar') {
        try {
            $c->stash->{zaak}->assert_assignee(betrokkene_id => $params->{ betrokkene_id });
        }
        catch {
            $c->stash->{ json } = { auth_error => \1 };
            $c->log->error("Got error assigning to subject " . $params->{betrokkene_id} . ": " . $_);
            $c->res->status(500);
            $c->detach('Zaaksysteem::View::JSONlegacy');
        };
    }

    $c->stash->{ action }    = 'allocation';
    $c->stash->{ selection } = 'one_case';
    $c->stash->{ template_available } = $c->model('DB::Config')->get('allocation_notification_template_id') ? 1 : 0;
    $c->stash->{ context } = encode_entities($params->{context}) // 'index';

    $c->forward('bulk_update');
}


Params::Profile->register_profile(
    method  => 'bulk_update_allocation',
    profile => {
        required => [ qw/selection/],
        require_some => {
            toewijzing => [1, qw/betrokkene_id role_id ou_id/],
        }
    },
);
sub bulk_update_allocation : Chained('/') : PathPart('bulk/update/allocation') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }
    $c->stash->{ template_available } = $c->model('DB::Config')->get('allocation_notification_template_id') ? 1 : 0;
    $c->stash->{action} = 'allocation';
    $c->forward('bulk_update');
}


Params::Profile->register_profile(
    method  => 'bulk_update_set_settings',
    profile => PROFILE_BULK_UPDATE_SET_SETTINGS,
);
sub bulk_update_set_settings : Chained('/') : PathPart('bulk/update/set_settings') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    $c->session->{bulk_update_kenmerken} = [];
    $c->stash->{table_config} = BULK_UPDATE_SIMPLE_TABLE_CONFIG;

    $c->stash->{action} = 'set_settings';
    $c->forward('bulk_update');
}



Params::Profile->register_profile(
    method  => 'bulk_update_verlengen',
    profile => {
        required     => [qw/reden selection/],
        require_some => { amount_or_date => [1, qw(datum amount)] },
    },
);

sub bulk_update_verlengen: Chained('/') : PathPart('bulk/update/verlengen') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    $c->stash->{action} = 'verlengen';

    $c->forward('bulk_update');
}

Params::Profile->register_profile(
    method  => 'bulk_update_owner',
    profile => {
        required => [ qw/selection/],
    },
);
sub bulk_update_owner: Chained('/') : PathPart('bulk/update/owner') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    $c->stash->{action} = 'owner';

    $c->forward('bulk_update');
}

define_profile verlengen => (
    required           => [qw(reden)],
    require_some       => { amount_or_date => [1, qw(datum amount)] },
    constraint_methods => {
        datum => sub {
            my $r   = shift;
            my $val = shift;

            my $dt = eval {
                DateTime::Format::Strptime->new(pattern => '%d-%m-%Y',)
                    ->parse_datetime($val);
            };

            return defined $dt;
        },
        amount => sub {
            my $val = pop;
            return 0 unless defined $val;
            return $val =~ /^\d+$/;
        },
    }
);

sub verlengen: Chained('/zaak/base'): PathPart('update/verlengen'): Args(0) {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    $c->stash->{action}    = 'verlengen';
    $c->stash->{selection} = 'one_case';

    $c->forward('bulk_update');
}




#
# these changes can apply to 3 different sets:
# - a single case
# - a selection of cases
# - all cases in the search results
#
sub bulk_update : Private {
    my ($self, $c) = @_;

    $self->log->trace('Zaaksysteem::Controller::Zaak::Acties.bulk_update');

#    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    my $action = $c->stash->{action} || '';

    my $permitted_bulk_actions = {
        map { $_ => 1 }
        qw/allocation verlengen opschorten resume relatie set_settings destroy owner/
    };

    unless($permitted_bulk_actions->{$action}) {
        die "action can only be in " . dump_terse($permitted_bulk_actions);
    }

 #   die "only acceptable through xmlhttprequest" unless($c->req->is_xhr);

    my $params = $c->req->params();

    $c->stash->{action} = $action;
    my $selection = $c->stash->{selection} ||= $params->{selection};

    if($params->{commit}) {
        if($selection eq 'selected_cases') {
            my @case_ids = sort {$b <=> $a} split /,/, $params->{selected_case_ids};
            foreach my $case_id (@case_ids) {
                $self->_execute_action($c, {case_id => $case_id, action=>$action});
            }

            ### Single case, show all push messages
            if ($c->stash->{_updated_cases} && $c->stash->{_messages} && @{ $c->stash->{_updated_cases} } == 1) {
                $c->push_flash_message($_) for @{ $c->stash->{_messages} };
            }
        } elsif($selection eq 'search_results') {

            $c->stash->{search_query_id} = encode_entities($params->{search_query_id});
            my $search_query = $self->_search_query($c);
            my $resultset = $search_query->results({
                c => $c
            })->search({}, {order_by => {'-desc' => 'me.id'}});

            while(my $case = $resultset->next()) {
                $self->_execute_action($c, {case_id => $case->id, action=>$action, skip_messages => 1});
            }
        } elsif($selection eq 'zql') {
            my $zql = Zaaksysteem::Search::ZQL->new($params->{zql});
            if (!$zql->cmd->isa('Zaaksysteem::Search::ZQL::Command::Select')) {
                throw(
                    "bulk/zql/not_select",
                    "ZQL query is not a SELECT",
                );
            }

            my $resultset = $zql->apply_to_resultset($c->model('Object')->rs);

            while(my $object = $resultset->next()) {
                my $case = $object->get_source_object();
                $self->_execute_action($c, {case_id => $case->id, action=>$action, skip_messages => 1 });
            }
        } elsif($selection eq 'one_case') {

            my $case_id = $params->{selected_case_ids};
            $self->_execute_action($c, {case_id => $case_id, action=>$action});

            if($action eq 'behandelaar') {
                my ($betrokkene_type, $uidnumber) = $params->{betrokkene_id} =~ /betrokkene-(\w+)-(\d+)$/;

                if (
                    $betrokkene_type eq 'medewerker' &&
                    $uidnumber eq $c->user->uidnumber
                ) {
                    $c->stash->{json}->{redirect} = '/zaak/' . $case_id;
                } else {
                    $c->stash->{json}->{redirect} = '/';
                }
            } elsif($action =~ m/^(verlengen|opschorten|resume|relatie|set_settings)$/) {
                $c->stash->{ json }{ redirect } = sprintf('/zaak/%d', $case_id);
            }
        }

        if ($c->stash->{_updated_cases} && @{ $c->stash->{_updated_cases} } > 0) {
            if (@{ $c->stash->{_updated_cases} } == 1 && $c->stash->{_messages}) {
                $c->push_flash_message($_) for @{ $c->stash->{_messages} };
            } elsif (@{ $c->stash->{_updated_cases} } < 20) {
                $c->push_flash_message('Actie(s) uitgevoerd op zaken: ' . join(', ', @{ $c->stash->{_updated_cases} }));
            } elsif (@{ $c->stash->{_updated_cases} } > 0) {
                $c->push_flash_message('Actie(s) uitgevoerd op ' . scalar(@{ $c->stash->{_updated_cases} }) . ' zaken.');
            }
        } else {
            $c->push_flash_message('Geen acties uitgevoerd, mogelijk i.v.m onvoldoende rechten.');
        }

        $c->stash->{no_redirect} = $params->{no_redirect} ? 1 : 0;
        if ($c->stash->{no_redirect}) {

            delete $c->stash->{ json }{ redirect };
            delete $c->stash->{ json }{ reload };

            $c->stash->{ json }{ messages } = delete $c->flash->{ result };
        }

        $c->stash->{ json }{ success } = 1;
        $c->detach('Zaaksysteem::View::JSONlegacy');
    }


    if($selection eq 'selected_cases') {
        $c->stash->{selected_case_ids} = encode_entities($params->{selected_case_ids});
    }
    elsif($selection eq 'search_results') {
        $c->stash->{search_query_id} = encode_entities($params->{search_query_id});
        my $search_query = $self->_search_query($c);
        my $resultset = $search_query->results({
            c => $c
        })->with_progress({}, {page=>1});
        $c->stash->{search_query} = $resultset;
    } elsif ($selection eq 'zql') {
        my $zql = Zaaksysteem::Search::ZQL->new($params->{zql});
        if (!$zql->cmd->isa('Zaaksysteem::Search::ZQL::Command::Select')) {
            throw(
                "bulk/zql/not_select",
                "ZQL query is not a SELECT",
            );
        }

        my $resultset   = $zql->apply_to_resultset($c->model('Object')->rs);
        $resultset      =$resultset->search({}, { page => 1});
        $c->stash->{search_query} = $resultset;
    } else {
        die "need zaak_id" unless $c->stash->{zaak}->id;
        $c->stash->{selected_case_ids} = $c->stash->{zaak}->id;
    }

    $c->stash->{no_redirect} = $params->{no_redirect} ? 1 : 0;

    $c->stash->{template} = 'zaak/widgets/management.tt';
    $c->stash->{nowrapper} = 1;
}

sub _set_case_allocation_subject {
    my ($self, $c, $case, $params) = @_;

    $self->log->trace('Zaaksysteem::Controller::Zaak::Acties._set_case_allocation_subject');

    my $ok;
    try {
        $ok = $case->assert_assignee(betrokkene_id => $params->{betrokkene_id});
    }
    catch {
        $c->stash->{json} = { auth_error => \1 };
        $c->log->error(
            sprintf(
                "Got error assigning to subject '%s': %s",
                $params->{betrokkene_id}, $_
            )
        );
        push(
            @{ $c->stash->{_messages} },
            "De behandelaar heeft onvoldoende rechten om de zaak te behandelen"
        );
    };

    return unless $ok;

    my @msg = (sprintf("Toewijzing voor zaak %d is gewijzigd: ", $case->id));
    my $assignee        = $c->user->as_object;
    my $is_current_user = 0;

    if ($assignee->old_subject_identifier eq $params->{betrokkene_id}) {
        $is_current_user = 1;
    }
    else {
        $assignee = $c->model("BR::Subject")->get_by_old_subject_identifier($params->{betrokkene_id});
    }

    $c->model("DB")->txn_do(
        sub {

            $case->set_behandelaar($assignee->old_subject_identifier);
            $case->discard_changes;
            $case->wijzig_status({ status => $is_current_user ? 'open' : 'new' });

            # this can be influenced with a checkbox
            my ($group, $role, $route_ou);
            if ($params->{change_department}) {
                $route_ou = $case->behandelaar_object->org_eenheid->id;
                $role     = $c->user->find_role_by_name('Behandelaar');
            }
            else {
                $route_ou = $case->route_ou;
                $role     = $c->user->find_role_by_id($case->route_role);
            }
            $group = $c->user->find_group_by_id($route_ou);

            $case->wijzig_route({
                    route_ou => $route_ou,
                    # In the weird situation that there is no
                    # "behandelaar", default to the current role
                    route_role => $role ? $role->id : $case->route_role,
                    change_only_route_fields => 1,

                    exists $params->{comment} ?
                        (comment => $params->{comment}) : (),

                    subject => $assignee,
                }
            );
            push(
                @msg,
                join("/",
                    $group ? $group->name : $route_ou,
                    $role  ? $role->name  : $case->route_role)
            );
    });

    if ($params->{selection} eq 'one_case') {
        $c->stash->{json}->{redirect} = $is_current_user ? '/zaak/' . $case->id : '/';
    }
    push(@{ $c->stash->{_messages} }, join(", ", @msg));
    return 1;
}

define_profile _execute_action => (
    required => {
        case_id => 'Int',
        action => 'Str'
    }
);

sub _execute_action {
    my ($self, $c, $args) = @_;

    my $opts = assert_profile($args)->valid;

    my $case_id = $opts->{ case_id };
    my $action  = $opts->{ action };

    my $params  = $c->req->params;
    my $case    = $c->model('DB::Zaak')->find($case_id);

    $c->stash->{ zaak } = $case;

    ### Return unless at least beheer and edit rights are set
    return unless $c->check_any_zaak_permission('zaak_beheer','zaak_edit');

    $c->stash->{_messages} //= [];
    $c->stash->{_updated_cases} //= [];

    $c->log->debug('Starting action: ' . $action . ' on case: ' . $case->id);

    if($action eq 'allocation') {

        if($case->is_afgehandeld) {
            push(@{$c->stash->{_messages}}, 'Zaak is reeds afgehandeld, kan toewijzing niet wijzigen');
        } else {
            if ($params->{change_allocation} eq 'behandelaar') {
                $self->_set_case_allocation_subject($c, $case, $params);
            }
            elsif ($params->{change_allocation} eq 'group' && $params->{ou_id} && $params->{role_id}) {
                my $allocation_message = 'Toewijzing voor zaak '. $case->id . ' is gewijzigd: ';

                $case->wijzig_route(
                    {
                        route_ou    => $params->{ou_id},
                        route_role  => $params->{role_id},
                        exists $params->{comment} ?
                        ( comment                  => $params->{comment} ) : (),
                    }
                );

                my $group = $c->user->find_group_by_id($params->{ou_id});
                my $role  = $c->user->find_role_by_id($params->{role_id});

                $allocation_message .= "afdeling is " . $group ? $group->name : $params->{ou_id};
                $allocation_message .= ', ';
                $allocation_message .= 'rol is ' . $role ? $role->name : $params->{role_id};

                if($c->stash->{ context } eq 'case' || $params->{selection} eq 'one_case') {
                    $c->stash->{ json }{ redirect } = '/';
                } else {
                    $c->stash->{ json }{ reload } = 1;
                }

                push(@{$c->stash->{_messages}}, $allocation_message);
            } elsif(!$params->{betrokkene_id}) {
                push(@{$c->stash->{_messages}}, "Zaak is niet toegewezen");
            }
        }

        unless($c->stash->{json}->{redirect}) {
            $c->stash->{json}->{reload} = 1;
        }

        $case->update();
        push (@{ $c->stash->{_updated_cases} }, $case->id);

        # Send notification after update, in case the save doesn't pan out.

        if($params->{ notify }) {
            unless($params->{ betrokkene_id }) {
                $c->stash->{ json }{ reload } = 1;
                return;
            }

            my $template_id = $c->model('DB::Config')->get('allocation_notification_template_id');

            unless($template_id) {
                push(@{$c->stash->{_messages}}, 'Kon geen e-mail notificatie template vinden, geen notificatie verstuurd');
                $c->stash->{ json }{ reload } = 1;
                return;
            }

            # TODO remove hardwired ID
            my $template = $c->model('DB::BibliotheekNotificaties')->find($template_id);

            unless($template) {
                $c->stash->{ json }{ reload } = 1;
                return;
            }

            unless($case->behandelaar_object->email) {
                push(@{$c->stash->{_messages}}, sprintf(
                    'Geen e-mail adres gevonden voor behandelaar "%s", geen notificatie verstuurd',
                    $params->{ betrokkene_naam }
                ));

                $c->stash->{ json }{ reload } = 1;
                return;
            }

            my %args = (
                notification => $template,
                recipient => $case->behandelaar_object->email,
            );

            if ($params->{comment}) {
               $args{additional_ztt_context} = {
                    assignment_reason => $params->{comment}
                },
            }

            $case->mailer->send_case_notification(\%args);
        }

        unless($c->stash->{json}->{redirect}) {
            $c->stash->{json}->{reload} = 1;
        }

    } elsif($action eq 'owner') {

        if($case->is_afgehandeld) {
            push(@{$c->stash->{_messages}}, 'Zaak is reeds afgehandeld, kan zaak niet in behandeling nemen');
        }
        else {
            push (@{ $c->stash->{_updated_cases} }, $case->id);

            # 1. Set the new assignee of the case
            # 2. Set new status of the case (Which handles setting coordinator)
            # 3. Log assignment change

            my $current_user = $case->result_source
                ->schema
                ->resultset('Zaak')
                ->current_user;

            unless ($case->behandelaar) {
                $case->set_behandelaar($current_user->betrokkene_identifier);
            }

            $case->wijzig_status({status => 'open'});

            my $event = $case->logging->trigger('case/accept', { component => 'zaak', data => {
                case_id => $case->id,
                acceptee_name => $current_user->naam,
            }});

            push(@{$c->stash->{_messages}}, $event->onderwerp);
        }

    } elsif($action eq 'verlengen') {
        my $event = $c->forward('_verleng', [$case_id]);

        push (@{ $c->stash->{_updated_cases} }, $case->id);

        if ($event) {
            push(@{$c->stash->{_messages}}, $event->onderwerp);
        }
    } elsif($action eq 'opschorten') {
        if($case->is_afgehandeld) {
            push(@{$c->stash->{_messages}}, 'Zaak is reeds afgehandeld, kan zaak niet opschorten');
        } else {
            my $options = {
                status => 'stalled',
                reason => $params->{reden},
            };

            if ($params->{schedule_resume}) {
                $options->{suspension_term_amount} = $params->{suspension_term_amount};
                $options->{suspension_term_type}   = $params->{suspension_term_type};
            }

            # If the status is actually changed we get an event, otherwise the
            # status is the same and we don't get an event.
            my $event = $case->wijzig_status($options);
            if ($event) {
                push (@{ $c->stash->{_updated_cases} }, $case->id);
                push(@{$c->stash->{_messages}}, $event->onderwerp);
                $case->touch();
            }
        }
    } elsif($action eq 'resume') {
        my $options = {
            status => 'open',
            reason => $params->{reden},
        };

        foreach my $stall (qw(stalled_since stalled_until)) {
            if ($params->{$stall}) {
                $options->{$stall} = assert_date($params->{$stall});
            }
        }

        my $event = $case->wijzig_status($options);
        if ($event) {
            push (@{ $c->stash->{_updated_cases} }, $case->id);
            push(@{$c->stash->{_messages}}, $event->onderwerp);
            $case->touch();
        }

    } elsif($action eq 'relatie') {
        my $msg = $self->_relate_case($c, {
            zaaknr  => $params->{zaaknr},
            case    => $case
        });

        push (@{ $c->stash->{_updated_cases} }, $case->id);

        push(@{$c->stash->{_messages}}, $msg) if $msg;
    } elsif($action eq 'set_settings') {
        $self->_set_settings($c, {
            case => $case,
            skip_messages => ($args->{skip_messages} || undef)
        });
    } else {
        throw('case/bulk_update/unknown_action', sprintf(
            'Action "%s" could not be executed because it does not exist.',
            $action
        ));
    }
}

# TODO could use some rework - this code exists on multiple places. should be a model
sub _search_query {
    my ($self, $c) = @_;

    unless($c) {
        throw('object/actions/_search_query', 'Unable to continue, App instance required');
    }

    my $search_query = $c->model('SearchQuery');
    my $search_query_id = $c->req->params->{search_query_id};
    my $model = $c->model(SEARCH_QUERY_TABLE_NAME);

    my $record;

    if($search_query_id) {
        my $record = $model->find($search_query_id);

        if($record) {
            $search_query->unserialize($record->settings);

            $c->stash->{'search_query_name'} = $record->name;
            $c->stash->{'record_ldap_id'} = $record->ldap_id;
            $c->stash->{'search_query_access'} = $search_query->access();

            return $search_query;
        } else {
            throw('object/actions/_search_query', 'Invalid search_query_id provided, could not find it\'s row');
        }
    } else {
        $c->stash->{'search_query_name'} = 'Naamloze zoekopdracht';
        $c->stash->{'search_query_access'} = 'private';
    }

    if($c->session->{SEARCH_QUERY_SESSION_VAR()}) {
        $search_query->unserialize($c->session->{SEARCH_QUERY_SESSION_VAR()});
    }

    return $search_query;
}

define_profile _relate_case => (
    required => {
        case => 'Zaaksysteem::Zaken::ComponentZaak',
        zaaknr => 'Int'
    }
);

sub _relate_case {
    my ($self, $c, $params) = @_;

    my $opts = assert_profile($params)->valid;

    my $case    = $opts->{ case };
    my $zaaknr  = $opts->{ zaaknr };

    my $error = 0;

    if ($zaaknr eq $case->id) {
        push(@{$c->stash->{_messages}}, 'Kan geen relatie aanmaken met hetzelfde zaaknummer als huidige zaak.');

        $error = 1;
    }

    my $related_case = $c->model('DB::Zaak')->find($zaaknr);

    unless($related_case) {
        push(@{$c->stash->{_messages}}, sprintf('Er is geen zaak met nummer %d.', $zaaknr));

        $error = 1;
    }

    return if $error;

    my ($message);
    try {
        $case->set_relatie({
            relatie => 'gerelateerd',
            relatie_zaak => $related_case
        });
        $message = sprintf('Zaak %d (%s) gerelateerd',
            $related_case->id,
            $related_case->zaaktype_node_id->titel
        );
    }
    catch {
        if (   blessed($_)
            && $_->isa('BTTW::Exception::Base')
            && $_->type eq 'case_relation/duplicate_add'
        ) {
            $message = sprintf(
                'Zaak %d (%s) was al gerelateerd met %d',
                $related_case->id,
                $related_case->zaaktype_node_id->titel,
                $case->id,
            );
        }
        else {
            $self->log->error(sprintf(
                "Error relating cases %d/%d: %s",
                $case->id,
                $related_case->id,
                $_,
            ));

            # Something else went wrong.. rethrow
            die $_;
        }
    };

    return $message;
}

# XXX TODO: These constraintmethods are simply not true
# refactor to use Moose::Util::TypeConstraints::union pseudo-type

define_profile _verleng => (
    required => [qw/reden prolongation_type/],
    optional => [qw/datum amount term_type/],
    constraint_methods => {
        term_type => qr/kalenderdagen|werkdagen|weken|einddatum/,
        prolongation_type => qr/fixedDate|changeTerm/,
        datum => qr/\d+-\d+-\d+/
    }
);

sub _verleng : Private {
    my ($self, $c, $case_id) = @_;
    my $params = assert_profile($c->req->params)->valid;

    # be prepared for bulk
    my $case = $c->model('DB::Zaak')->find($case_id);

    my $givendate = $params->{prolongation_type} eq 'fixedDate' ?
        $self->_calculate_fixed_date($params->{datum}) :
        calculate_term({
            start  => $case->registratiedatum,
            amount => $params->{amount},
            type   => $params->{term_type}
        });

    $case->set_verlenging($givendate);

    my $event = $case->logging->trigger('case/update/settle_date', { component => 'zaak', data => {
        case_id     => $case->id,
        settle_date => $givendate->dmy,
        reason      => $params->{reden}
    }});

    return $event;
}


sub _calculate_fixed_date {
    my ($self, $date) = @_;

    my ($day, $month, $year) = split /-/, $date;

    return DateTime->new(
        year  => $year,
        month => $month,
        day   => $day,
    );
}

sub set_eigenaar : Chained('/zaak/base'): PathPart('update/eigenaar'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{betrokkene_type} = 'medewerker';
    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');
    if (!%{ $c->req->params }) {
        $c->stash->{nowrapper} = 1;
        $c->stash->{template} = 'zaak/widgets/set_eigenaar.tt';
        $c->detach;
    } elsif ($c->req->params->{'ztc_behandelaar_id'}) {
        $c->stash->{zaak}->set_coordinator($c->req->params->{'ztc_behandelaar_id'});
    }
}




Params::Profile->register_profile(
    method  => 'bulk_update_relatie',
    profile => {
        required => [ qw/zaaknr/ ],
    },
);
sub bulk_update_relatie: Chained('/') : PathPart('bulk/update/relatie') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    $c->stash->{action} = 'relatie';
    $c->forward('bulk_update');
}



Params::Profile->register_profile(
    method  => 'bulk_update_kenmerken',
    profile => {
        required => [ qw//],
    },
);
sub bulk_update_kenmerken: Chained('/') : PathPart('bulk/update/kenmerken') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    my $params = $c->req->params();

    my $action = $params->{action} || '';

    my $bulk_update_kenmerken = $c->session->{bulk_update_kenmerken} || [];

    # get new serial id
    my $new_id = 1;
    foreach my $item (@$bulk_update_kenmerken) {
        $new_id = $item->{id} if($item->{id} > $new_id);
    }
    $new_id++;


    if($action eq 'add') {
        my $bibliotheek_kenmerken_id = $params->{simpletable_new_item_id} or die "need new item id";

        my $kenmerk = $c->model('DB::BibliotheekKenmerken')->find($bibliotheek_kenmerken_id);
        my $new_record = {
            id                          => $new_id,
            bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id,
            kenmerk_naam                => $kenmerk->naam,
        };

        push @$bulk_update_kenmerken, $new_record;

    } elsif($action eq 'remove') {
        my $remove_id = $params->{remove_id} or die "need remove id";

        $bulk_update_kenmerken = [grep { $_->{id} ne $remove_id } @$bulk_update_kenmerken];
    }

    foreach my $kenmerk (@$bulk_update_kenmerken) {
        my $key = 'bibliotheek_kenmerk_' . $kenmerk->{bibliotheek_kenmerken_id};
        $c->log->debug("key: " . $key);
        if($params->{$key}) {
            $kenmerk->{value} = $params->{$key};
        }
    }

    $c->session->{bulk_update_kenmerken} = $bulk_update_kenmerken;


    $c->stash->{table_config} = BULK_UPDATE_SIMPLE_TABLE_CONFIG;
    $c->stash->{table_config}->{rows} = $bulk_update_kenmerken;

    $c->stash->{action}     = 'kenmerk';
    $c->stash->{template}   = 'widgets/general/simple_table.tt';

    $c->stash->{nowrapper}  = 1;
}



Params::Profile->register_profile(
    method  => 'relatie',
    profile => {
        required => [ qw/zaaknr/],
    }
);
sub relatie : Chained('/zaak/base'): PathPart('update/relatie'): Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    my $params = $c->req->params();

    if($params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    $c->stash->{action}    = 'relatie';
    $c->stash->{selection} = 'one_case';

    $c->forward('bulk_update');
}

my $settings_mapping = {
    coordinator_id      => {
        method  => 'wijzig_coordinator',
        user_permissions => [qw/zaak_beheer/],
        zaak_permissions => [qw/zaak_beheer/],
        mapping => {
            coordinator_id  => 'betrokkene_identifier',
        },
        label   => 'Wijzigen coordinator',
    },
    aanvrager_id        => {
        method  => 'wijzig_aanvrager',
        user_permissions => [qw/zaak_beheer/],
        zaak_permissions => [qw/zaak_beheer/],
        mapping => {
            aanvrager_id  => 'betrokkene_identifier',
        },
        label   => 'Wijzigen aanvrager',
    },
    ou_id               => {
        method      => 'wijzig_route',
        user_permissions => [qw/zaak_beheer/],
        zaak_permissions => [qw/zaak_beheer/],
        mapping     => {
            ou_id                       => 'route_ou',
            role_id                     => 'route_role',
            change_only_route_fields    => 'change_only_route_fields',
            wijzig_route_force          => 'force',
        },
        change_check => sub {
            my ($zaak, $opts) = @_;
            return unless ($opts->{route_role} && $opts->{route_ou});

            if (
                $zaak->route_role   eq $opts->{route_role} &&
                $zaak->route_ou     eq $opts->{route_ou}
            ) {
                return;
            }

            return 1;
        },
        label       => 'Wijzigen afdeling',
    },
    streefafhandeldatum  => {
        method      => 'wijzig_streefafhandeldatum',
        user_permissions => [qw/zaak_beheer/],
        zaak_permissions => [qw/zaak_beheer/],
        mapping     => {
            streefafhandeldatum     => 'streefafhandeldatum',
        },
        label       => 'Wijzigen streefafhandeldatum',
    },
    registratiedatum  => {
        method      => 'wijzig_registratiedatum',
        user_permissions => [qw/zaak_beheer/],
        zaak_permissions => [qw/zaak_beheer/],
        mapping     => {
            registratiedatum     => 'registratiedatum',
        },
        label       => 'Wijzigen registratiedatum',
    },
    vernietigingsdatum_type  => {
        method      => 'wijzig_vernietigingsdatum',
        user_permissions => [qw/zaak_beheer/],
        zaak_permissions => [qw/zaak_beheer/],
        mapping     => {
            vernietigingsdatum_type        => 'vernietigingsdatum_type',
            vernietigingsdatum             => 'vernietigingsdatum',
            vernietigingsdatum_recalculate => 'vernietigingsdatum_recalculate',
            vernietigingsdatum_reden       => 'reden',
        },
        label       => 'Wijzigen vernietigingsdatum',
    },
    milestone           => {
        method      => 'wijzig_fase',
        user_permissions => [qw/zaak_beheer/],
        zaak_permissions => [qw/zaak_beheer/],
        mapping     => {
            milestone       => 'milestone',
        },
        change_check => sub {
            my ($zaak, $opts) = @_;

            return unless $opts->{milestone};

            if (
                $zaak->milestone == $opts->{milestone}
            ) {
                return;
            }

            return 1;
        },
        label       => 'Wijzigen fase',
    },
    status              => {
        method      => 'wijzig_status',
        user_permissions => [qw/zaak_beheer/],
        zaak_permissions => [qw/zaak_beheer/],
        mapping     => {
            status          => 'status'
        },
        change_check => sub {
            my ($zaak, $opts) = @_;

            return unless $opts->{status};

            if (
                $zaak->status eq $opts->{status}
            ) {
                return;
            }

            return 1;
        },
        hook => sub {
            my ($zaak, $opts) = @_;

            $zaak->set_gesloten if $opts->{status} eq 'resolved';
        },
        label       => 'Wijzigen status',
    },
    zaaktype_id     => {
        method      => 'wijzig_zaaktype',
        user_permissions => [qw/zaak_beheer zaak_edit/],
        zaak_permissions => [qw/zaak_beheer zaak_edit/],
        mapping     => {
            zaaktype_id     => 'zaaktype_id',
        },
        label       => 'Wijzigen zaaktype',
    },
    resultaat => {
        method => 'set_result_by_id',
        user_permissions => [qw/zaak_beheer zaak_edit/],
        zaak_permissions => [qw/zaak_beheer zaak_edit/],
        mapping => {
            'resultaat' => 'id',
        },
        label => 'Wijzigen resultaat'
    },
};



Params::Profile->register_profile(
    method  => 'set_settings',
    profile => PROFILE_BULK_UPDATE_SET_SETTINGS,
);
sub set_settings : Chained('/zaak/base'): PathPart('update/set_settings'): Args(0) {
    my ($self, $c) = @_;

    unless (
        $c->check_any_user_permission('admin', 'zaak_beheer') ||
        $c->check_any_zaak_permission('zaak_beheer')
    ) {
        $c->detach('/forbidden');
    }

    my $params = $c->req->params();

    if($params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    $c->session->{bulk_update_kenmerken} = [];

    $c->stash->{action}    = 'set_settings';
    $c->stash->{selection} = 'one_case';

    $c->stash->{table_config} = BULK_UPDATE_SIMPLE_TABLE_CONFIG;

    $c->forward('bulk_update');
}


define_profile _set_settings => (
    required => {
        case => 'Zaaksysteem::Zaken::ComponentZaak'
    }
);

sub _set_settings : Private {
    my ($self, $c, $args) = @_;

    my $zaak = assert_profile($args)->valid->{ case };

    my @set_settings    = ();
    my $valid           = 1;
    my $params          = $c->req->params();

    $c->stash->{_messages} //= [];

    while (my ($parameter, $config) = each %{ $settings_mapping }) {
        my $method  = $config->{method};
        my $opts = {};

        foreach my $key (keys %{ $config->{mapping} } ) {
            if (
                defined $params->{ $key } &&
                defined $config->{mapping}->{$key}
            ) {
                my $opts_key = $config->{mapping}->{ $key };
                my $opts_val = $params->{$key};

                $opts->{$opts_key} = $opts_val;
            }
        }

        if ($config->{change_check}) {
            next unless $config->{change_check}->($zaak, $opts);
        } else {
            next unless $params->{$parameter};
        }

        push(@set_settings,
            {
                method  => $method,
                opts    => $opts,
                config  => $config,
            }
        );
    }

    my @results = ();
    my $have_settings_changed = 0;
    for my $settings (@set_settings) {
        my $method  = $settings->{method};
        my $opts    = $settings->{opts};
        my $label   = $settings->{config}->{label};
        my $config  = $settings->{config};

        my $user_permissions = $config->{user_permissions};
        my $zaak_permissions = $config->{zaak_permissions};

        next unless (
            $c->check_any_user_permission(@$user_permissions) ||
            $c->check_any_zaak_permission(@$zaak_permissions)
        );

        $c->log->debug(
            "Running method: '$method', with params: " . dump_terse($opts));

        my $msg = $label;

        $have_settings_changed = 1;

        if ($settings->{hook}) {
            $settings->{hook}->($zaak, $opts);
        }

        if (my $result = $zaak->$method($opts)) {
            ### These instructions are here for a proper redirect after a zaaktype
            ### wijziging. Yes, it's not as generic as we would have liked.
            if (blessed($result) && $result->isa('Zaaksysteem::Model::DB::Zaak')) {
                $zaak = $result;
            }
            push(@{$c->stash->{_messages}}, $label . ' voor zaak ' . $zaak->id . ' gelukt.') unless $args->{skip_messages};
        } else {
            push(@{$c->stash->{_messages}}, $label . ' voor zaak ' . $zaak->id .' mislukt.') unless $args->{skip_messages};
        }
    }

    if (
        $c->check_any_user_permission('admin', 'zaak_beheer') ||
        $c->check_any_zaak_permission('zaak_beheer')
    ) {

        # Do a single update of attributes
        my %values;
        foreach my $param (keys %$params) {
            my ($id) = $param =~ m|^bibliotheek_kenmerk_(\d+)$|;
            next unless $id;
            $values{$id} = ref $params->{$param} eq 'ARRAY' ? $params->{$param} : [ $params->{$param} ];
        }

        my $updated =  $zaak->zaak_kenmerken->update_fields(
            {
                new_values => \%values,
                zaak       => $zaak,
                skip_touch   => 1,
            }
        );
        $have_settings_changed ||= $updated;

    }

    if ($have_settings_changed) {
        $c->stash->{_updated_cases} //= [];
        push (@{ $c->stash->{_updated_cases} }, $zaak->id);
        $zaak->ddd_update_system_attributes;
        $zaak->touch();
    }
}

Params::Profile->register_profile(
    method  => 'bulk_update_resume',
    profile => {
        required => [ qw/reden selection stalled_until stalled_since/],
        constraint_methods => {
            stalled_until => sub { eval { assert_date(pop) }; if ($@) { return 0 } else { return 1 } },
            stalled_since => sub { eval { assert_date(pop) }; if ($@) { return 0 } else { return 1 } },
        },
    },
);
sub bulk_update_resume: Chained('/') : PathPart('bulk/update/resume') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    $c->stash->{action} = 'resume';
    $c->forward('bulk_update');
}


Params::Profile->register_profile(
    method  => 'bulk_update_opschorten',
    profile => {
        required => [ qw/reden selection/],
    },
);
sub bulk_update_opschorten: Chained('/') : PathPart('bulk/update/opschorten') {
    my ($self, $c) = @_;

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    $c->stash->{action} = 'opschorten';
    $c->forward('bulk_update');
}



define_profile opschorten => (
    required => [qw/reden suspension_term_type/],
    optional => [qw/stalled_since/],
    constraint_methods => {
        suspension_term_amount => sub {
            my $r = shift;
            my $val = shift;

            return 1 unless $r->get_filtered_data->{ suspension_term_type } eq 'einddatum';

            my $dt = eval {
                DateTime::Format::Strptime->new(
                    pattern => '%d-%m-%Y'
                )->parse_datetime($val);
            };

            return defined $dt;
        }
    },

    dependencies => {
        schedule_resume => {
            1 => [ qw/suspension_term_amount/]
        }
    }
);
sub opschorten : Chained('/zaak/base'): PathPart('update/opschorten'): Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    $c->stash->{action}    = 'opschorten';
    $c->stash->{selection} = 'one_case';
    $c->stash->{current_date} = DateTime->now->strftime('%d-%m-%Y');

    $c->forward('bulk_update');
}



Params::Profile->register_profile(
    method  => 'resume',
    profile => {
        required => [qw/reden stalled_until stalled_since/],
        constraint_methods => {
            stalled_until => sub { eval { assert_date(pop) }; if ($@) { return 0 } else { return 1 } },
            stalled_since => sub { eval { assert_date(pop) }; if ($@) { return 0 } else { return 1 } },
        },
    },
);

sub resume : Chained('/zaak/base'): PathPart('update/resume'): Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    if($c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    $c->stash->{action}    = 'resume';
    $c->stash->{selection} = 'one_case';
    $c->stash->{current_date} = DateTime->now->strftime('%d-%m-%Y');

    $c->forward('bulk_update');
}


sub vorige_status : Chained('/zaak/base'): PathPart('update/vorige_status'): Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_zaak_permission('zaak_beheer');

    if (!exists($c->req->params->{update})) {
        $c->stash->{nowrapper}  = 1;
        $c->stash->{template}   = 'zaak/widgets/vorige_status.tt';
        $c->detach;
    } elsif ($c->req->params->{'update'}) {
        if ($c->stash->{zaak}->set_vorige_fase) {
            $c->push_flash_message('Zaak succesvol omgezet naar vorige fase');
        }
    }

    $c->res->redirect('/zaak/' . $c->stash->{zaak}->nr
        . '#zaak-elements-status'
    );
    $c->detach;
}

Params::Profile->register_profile(
    method  => 'afhandelen',
    profile => {
        required => [ qw/
            reden
        /],
        require_some => {
            resultaat_or_id => [ 1 => qw/ system_kenmerk_resultaat system_kenmerk_resultaat_id / ],
        },
    }
);

sub afhandelen : Chained('/zaak/base'): PathPart('update/afhandelen'): Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    my $params = $c->req->params();

    unless ($params->{update}) {
        $c->stash->{nowrapper} = 1;
        $c->stash->{template} = 'zaak/widgets/afhandelen.tt';
        $c->detach;
    }


    if ($c->req->is_xhr && $params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    my $result_type;

    if ( defined $params->{'system_kenmerk_resultaat_id'} ) {
        $c->stash->{zaak}->set_result_by_id($params->{'system_kenmerk_resultaat_id'});
    } elsif ( defined $params->{'system_kenmerk_resultaat'} ) {
        $c->stash->{zaak}->set_resultaat($params->{'system_kenmerk_resultaat'});
    } else {
        throw('case/early_settle',
            sprintf(
                "Can not settle case early, missing 'resultaat' or 'resultaat_id'",
            )
        )
    }

    $c->stash->{zaak}->set_gesloten( DateTime->now() );

    $c->stash->{zaak}->logging->trigger('case/early_settle', {
        component => 'zaak',
        data => {
            case_id           => $c->stash->{ zaak }->id,
            reason            => $params->{ reden },
            case_result       => $result_type->resultaat,
            case_result_label => $result_type->label,
        }
    });

    $c->res->redirect('/zaak/' . $c->stash->{zaak}->nr);
    $c->detach;
}



sub deelzaak : Chained('/zaak/base'): PathPart('update/deelzaak'): Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    if (!exists($c->req->params->{update})) {
        $c->stash->{nowrapper} = 1;
        $c->stash->{template} = 'zaak/widgets/deelzaak.tt';
        $c->detach;
    } else {
        $c->forward('/zaak/status/start_subzaken');

        $c->res->redirect('/zaak/' . $c->stash->{zaak}->nr
            . '#zaak-elements-status'
        );
        $c->detach;
    }
}


sub delete_job : Chained('/zaak/base') : PathPart('delete_job') {
    my ($self, $c) = @_;

    my $job_id = $c->req->params->{job_id}
        or die "need job_id";

    my $scheduled_job = $c->model('DB::ScheduledJobs')->find($job_id)
        or die "could not find job";

    $scheduled_job->delete;

    $c->forward('scheduled_jobs');
}

sub reschedule : Chained('/zaak/base') : PathPart('reschedule') {
    my ($self, $c) = @_;

    $c->stash->{zaak}->reschedule_notifications();

    $c->forward('scheduled_jobs');
}


sub scheduled_jobs : Chained('/zaak/base') : PathPart('scheduled_jobs') {
    my ($self, $c) = @_;

    unless ($c->has_client_type) {
        $c->stash->{ template } = 'zaak/widgets/reschedule.tt';
        $c->stash->{ nowrapper } = 1;
        $c->detach;
    }

    $c->stash->{ zapi } = try {
        my $pe = Zaaksysteem::Email::Planned->new(
            case => $c->stash->{zaak},
        );

        return $pe->get_pending_emails,
    } catch {
        return Zaaksysteem::ZAPI::Error->new_from_error(
            $_,
            'zaak/scheduled_email/unknown_error'
        );
    };

    $c->detach($c->view('ZAPI'));
}

__PACKAGE__->meta->make_immutable;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BETROKKENE_RELATEREN_PROFILE

TODO: Fix the POD

=cut

=head2 BULK_UPDATE_SIMPLE_TABLE_CONFIG

TODO: Fix the POD

=cut

=head2 PROFILE_BULK_UPDATE_SET_SETTINGS

TODO: Fix the POD

=cut

=head2 SEARCH_QUERY_SESSION_VAR

TODO: Fix the POD

=cut

=head2 SEARCH_QUERY_TABLE_NAME

TODO: Fix the POD

=cut

=head2 ZAAK_WIJZIG_VERNIETIGINGSDATUM_PROFILE

TODO: Fix the POD

=cut

=head2 afhandelen

TODO: Fix the POD

=cut

=head2 bulk_update

TODO: Fix the POD

=cut

=head2 bulk_update_allocation

TODO: Fix the POD

=cut

=head2 bulk_update_kenmerken

TODO: Fix the POD

=cut

=head2 bulk_update_opschorten

TODO: Fix the POD

=cut

=head2 bulk_update_owner

TODO: Fix the POD

=cut

=head2 bulk_update_relatie

TODO: Fix the POD

=cut

=head2 bulk_update_resume

TODO: Fix the POD

=cut

=head2 bulk_update_set_settings

TODO: Fix the POD

=cut

=head2 bulk_update_verlengen

TODO: Fix the POD

=cut

=head2 deelzaak

TODO: Fix the POD

=cut

=head2 delete_job

TODO: Fix the POD

=cut

=head2 opschorten

TODO: Fix the POD

=cut

=head2 relatie

TODO: Fix the POD

=cut

=head2 reschedule

TODO: Fix the POD

=cut

=head2 resume

TODO: Fix the POD

=cut

=head2 scheduled_jobs

TODO: Fix the POD

=cut

=head2 set_allocation

TODO: Fix the POD

=cut

=head2 set_betrokkene

TODO: Fix the POD

=cut

=head2 set_betrokkene_suggestion

TODO: Fix the POD

=cut

=head2 set_eigenaar

TODO: Fix the POD

=cut

=head2 set_settings

TODO: Fix the POD

=cut

=head2 verlengen

TODO: Fix the POD

=cut

=head2 vorige_status

TODO: Fix the POD

=cut

=head2 weiger

TODO: Fix the POD

=cut

=head2 wijzig_route

TODO: Fix the POD

=cut

=head2 wijzig_vernietigingsdatum

TODO: Fix the POD

=cut

=head2 wijzig_zaaktype

TODO: Fix the POD

=cut

