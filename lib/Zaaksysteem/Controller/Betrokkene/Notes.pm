package Zaaksysteem::Controller::Betrokkene::Notes;

use Moose;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 create_note

Create a note on a subject object.

=head2 URL construction

B</betrokkene/betrokkene-[type]-[gmid]/notes/create>

=head2 Parameters

=over 4

=item content (string, I<required>)

The text content of the note to be created

=back

=head2 Responses

=over 4

=item HTTP 201

Note was succesfully created. JSON body may be ignored.

=item HTTP 405

Incorrect request method, this URL requires HTTP POST.

=item HTTP 400

No content specified

=back

=head2 Events

=over 4

=item C<subject/note/create>

=back

=cut

sub create_note : Chained('/betrokkene/betrokkene') : PathPart('notes/create') : Args(0) {
    my ($self, $c) = @_;

    $c->assert_post;

    unless($c->req->param('content')) {
        throw('request/parameter/missing', "Missing request parameter `content`.");
    }

    my $event = $c->model('DB::Logging')->trigger('subject/note/create', {
        component => 'betrokkene',
        created_for => $c->stash->{ betrokkene }->betrokkene_identifier,
        data => {
            subject_id => $c->stash->{ betrokkene }->betrokkene_identifier,
            content => $c->req->param('content')
        }
    });

    $event->discard_changes;

    $c->stash->{ betrokkene }->create_message(
        event_type => $event->event_type,
        message    => 'Notitie toegevoegd aan contactdossier',
        log        => $event,
    );

    $c->stash->{ json } = $event;
    $c->detach('Zaaksysteem::View::JSON');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 create_note

TODO: Fix the POD

=cut

