package Zaaksysteem::Controller::API::User::Password;

use strict;
use Data::Dumper;

use Zaaksysteem::Constants qw/LOGGING_COMPONENT_USER/;

use Moose;

BEGIN { extends 'Catalyst::Controller::REST' }

# this sets up the other verbs
sub password : Path : ActionClass('REST') {
    my ($self, $c) = @_;

    # since this somehow bypasses the normal loggin
    $c->log->debug("---------------\nREST request - api/user/password");
}

sub password_POST {
    my ($self, $c)     = @_;

    my $params          = $c->req->params;

    my $username = $c->user->username;

    $c->detach('response', [500, {
        type    => 'user/password/cant_change_admin_password',
        message => 'Not allowed to change admin password'
    }]) if $username eq 'admin';

    my $reauthenticated = $c->authenticate({
        username => $username,
        password => $params->{old_password}
    });

    $c->detach('response', [500, {
        type    => 'user/password/wrong_credentials',
        message => 'Incorrect password given, could not authenticate'
    }]) unless $reauthenticated;


    my $result = $c->user->update_password(
        $c->user->login_entity,
        {
            password => $params->{new_password}
        }
    );

    if ($result) {

        $c->model('DB::Logging')->trigger('user/password/update', {
            component => LOGGING_COMPONENT_USER,
            data => {}
        });

        $c->detach('response', [200, {
            type    => 'user/password/success',
            message => 'Password has been changed'
        }]);

    } else {

        $c->detach('response', [500, {
            type    => 'user/password/unknown_error',
            message => 'Could not change password'
        }]);

    }
}


sub response : Private {
    my ($self, $c, $status, $entity) = @_;

    $c->response->status($status);
    $self->_set_entity($c, {result => [$entity]});
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 LOGGING_COMPONENT_USER

TODO: Fix the POD

=cut

=head2 password

TODO: Fix the POD

=cut

=head2 password_POST

TODO: Fix the POD

=cut

=head2 response

TODO: Fix the POD

=cut

