package Zaaksysteem::Controller::API::v0::Case;

use Moose;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::v0::Case

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use List::Util qw[all];
use Zaaksysteem::Types qw[CaseNumber];

=head1 ACTIONS

=head2 base

Reserves the C<api/v0/case> URI namespace.

=cut

sub base : Chained('/api/base') : PathPart('v0/case') : CaptureArgs(0) {
    my ($self, $c) = @_;

    return;
}

=head2 instance_base

Reserves the C<api/v0/case/:number> URI namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $number) = @_;

    CaseNumber->check($number) or throw(
        'api/v0/case/invalid_case_number',
        sprintf('"%s" is not a valid case number, must be serial id', $number),
        { http_code => 400 },
    );

    $c->set_log_context('zs.case.number', $number);
    $c->stash->{ case_number } = $number;

    my $zaak = $c->model('DB::Zaak')->find_v0($number);

    try {
        $zaak->ddd_assert_user_authorizations(
            $c->user,
            sub { $c->check_any_given_zaak_permission(@_) },
            '_',
            'read'
        );
    } catch {
        $c->log->info("$_");

        throw('api/v0/case/not_found', sprintf(
            'Case "%d" not found',
            $number
        ), { zapi => 1, http_code => 404 });
    };

    $c->stash->{ case_v0 } = $zaak;

    return;
}

=head2 attribute_base

Reserves the C</api/v1/case/:number/attribute> URI namespace.

=cut

sub attributes_base : Chained('instance_base') : PathPart('attributes') : CaptureArgs(0) { }

=head2 create

Creates a case.

=head3 URI Path

C</api/v0/case/create>

=cut

sub create : Chained('base') : PathPart('create') : Args(0) {
    my ($self, $c) = @_;

    my $model = $c->model('Zaak');

    my $args = $model->prepare_case_arguments($c->req->params);
    my $case = $model->create_case($args);

    $c->model('DB')->schema->default_resultset_attributes->{ delayed_touch }->async(1);

    $c->stash->{ zapi } = [{
        number => $case->id,
        status => $case->status
    }];
}

=head2 get

Retrieves a 'v0' style serialization of a case given by the case number.

Note that this serialization is a stripped down variant of the API found on
the C</api/case/:number> endpoint for the purpose of enhancing the response
times of this often used call.

=head3 URI Path

C</api/v0/case/:number>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    my $case_data = $c->stash->{ case_v0 }->TO_JSON_V0;

    # Following code does some Zaaksysteem-Catalyst instance dependent
    # injections

    # This injection relies on prefetched zaak_authorisations
    $case_data->{ case }{ permissions } = $c->retrieve_list_of_zaak_permissions($c->stash->{ case_v0 });

    $c->stash->{ zapi } = [ $case_data ];
}

=head2 touch

Forces a 'touch' of the case.

=cut

sub touch : Chained('instance_base') : PathPart('touch') : Args(0) {
    my ($self, $c) = @_;

    $self->_assert_case_authorizations($c, 'meta', 'touch');

    $c->stash->{ case_v0 }->_touch;

    $c->stash->{ zapi } = [ $c->stash->{ case_v0 }->TO_JSON_V0 ];
}

=head2 update

Update the case properties.

=head3 Updateable parameters

=over 4

=item result

Sets the C<case>'s result by its generic result type name. (e.g., C<verwerkt>
or C<akkoord>).

=item result_id

Sets the C<case>'s result by id.

=item status

Sets the C<case>'s status (e.g., C<open> or C<resolved>).

=item price

Sets the C<case>'s price property.

=item confidentiality

Sets the C<case>'s confidentiality state (e.g., C<internal> or C<public>).

=back

=cut

define_profile update => (
    optional => {
        result => 'Str',
        result_id => 'Int',
        status => 'Str',
        price => 'Str',
        confidentiality => 'Str'
    }
);

sub update : Chained('instance_base') : PathPart('update') : Args(0) {
    my ($self, $c) = @_;

    my $args = assert_profile($c->req->params)->valid;

    if (exists $args->{ result }) {
        $args->{ result } = $c->model('DB::ZaaktypeResultaten')->find_by_result_name(
            $args->{ result }
        );
    }

    if (exists $args->{ result_id }) {
        throw(
            'api/v0/case/update/result_id',
            'Multiple results provided in update request',
            { http_code => 400 }
        ) if exists $args->{ result };

        $args->{ result } = $c->model('DB::ZaaktypeResultaten')->find(
            delete $args->{ result_id }
        );

        throw(
            'api/v0/case/update/result_id/not_found',
            'Invalid result id specified',
            { http_code => 400 }
        ) if not defined $args->{result};
    }

    if (exists $args->{ price }) {
        # Automagically convert localized comma decimal seperator to dot
        # decimal notation, this should be safe as long as the caller adheres
        # to the 'no grouping seperator' agreement.
        $args->{ price } = 0 + ($args->{ price } =~ s/,/./r);
    }

    for my $item (keys %{ $args }) {
        $self->_assert_case_authorizations($c, $item);
    }

    my @events = $c->stash->{ case_v0 }->ddd_update($c->user->as_object, $args);

    ### Fake 'event listener' starts here, for ease of refactor or
    ### reimplementation in minty-python.
    my @status_events = grep {
        $_->event_type eq 'case/update/status'
    } @events;

    for my $event (@status_events) {
        $c->stash->{ case_v0 }->handle_status_triggers($event)
    }
    ### End 'event listener'

    $c->stash->{ case_v0 }->_touch;
    $c->stash->{ case_v0 }->discard_changes;

    $c->detach('get');
}

=head2 transition

Attempts to transition the case to the next phase.

=cut

sub transition : Chained('instance_base') : PathPart('transition') : Args(0) {
    my ($self, $c) = @_;

    $self->_assert_case_authorizations($c, 'phase', 'transition');

    my @queued_items = $c->stash->{ case_v0 }->ddd_transition($c->user, sub {
        return $c->model('Object')->validate_mutation(@_);
    });

    $c->model('Queue')->queue_item($_) for @queued_items;

    $c->stash->{ zapi } = [ { success => \1 } ];

    $c->detach('get');
}

=head2 attribute_save

Implements a non-object_data variant of v1 case attribute save call such that
data is updated 'in place', instead of as a full rewrite of the object_data
row and associated case-touch infrastructure.

=cut

sub attribute_save : Chained('attributes_base') : PathPart('save') : Args(0) {
    my ($self, $c) = @_;

    $c->assert_post;

    $self->_assert_case_authorizations($c, 'attributes');

    my @events = $c->stash->{case_v0}->update_fields(
        $c->req->json_parameters
    );

    ### Fake 'event listener' starts here, for ease of refactor or
    ### reimplementation in minty-python
    my @update_events = grep {
        $_->event_type eq 'case/attribute/update'
    } @events;

    for my $event (@update_events) {
        $c->stash->{ case_v0 }->handle_attribute_triggers(
            $c->model('Queue'),
            $event
        );
    }
    ### End 'event listener'

    # Current case_v0 has prefetched case_properties, we need to refresh the
    # state to reflect the updated rows.
    $c->stash->{ case_v0 } = $c->model('DB::Zaak')->find_v0(
        $c->stash->{ case_number }
    );

    my $queue = $c->model('Queue');
    my $uuid = $c->stash->{ case_v0 }->get_column('uuid');

    my $item = $queue->create_item({
        type => 'touch_case',
        label => 'Zaak synchronisatie (for attribute update)',
        disable_acl => 1,
        data => { case_object_id => $uuid }
    });

    $queue->queue_item($item) if defined $item;

    $c->detach('get');
}

=head2 peruse

Registers that the logged in user has viewed the case, such as when they open
the case file interface.

=head3 URI Path

C</api/v0/case/:number/peruse>

=cut

sub peruse : Chained('instance_base') : PathPart('peruse') : Args(0) {
    my ($self, $c) = @_;

    $c->assert_post;

    $c->stash->{ case_v0 }->log_view;

    $c->stash->{ zapi } = [];
}

=head2 checklists

Retrieves the checklist resources for a given case, per phase, as a ZAPI reponse.

    "results": [
        {
            "id": 123,
            "milestone": 2,
            "items": [
                {
                    "checked": true,
                    "id": 456,
                    "label": "I'm a checklist item",
                    "user_defined": false
                },
                ...
            ]
        },
        ...
    ]

=head3 URI Path

C</api/v0/case/:number/checklists>

=cut

sub checklists : Chained('instance_base') : PathPart('checklists') : Args(0) {
    my ($self, $c) = @_;

    my $checklists = $c->stash->{ case_v0 }->checklists->search(undef, {
        order_by => 'case_milestone',
        prefetch => 'ordered_checklist_items',
    });

    $c->stash->{ zapi } = [ $checklists->all ];
}

=head2 actions

Retrieves the action resources for a given case, per phase, as a ZAPI response.

    "results": [
        {
            "milestone_id": 123,
            "milestone": 1,
            "items": [
                {
					"automatic" : false,
					"data" : { ... }
					"description" : "Tooltip description of the action",
					"id" : 456,
					"label" : "Sort title",
					"tainted" : false,
					"type" : "my_type",
					"url" : "/zaak/789/action?id=456"
                },
                ...
            ]
        },
        ...
    ]

=head3 URI Path

C</api/v0/case/:number/actions>

=cut

sub actions : Chained('instance_base') : PathPart('actions') : Args(0) {
    my ($self, $c) = @_;

    my $node = $c->stash->{ case_v0 }->zaaktype_node_id;
    my $milestones = $node->zaaktype_statussen->search(undef, {
        order_by => 'status'
    });

    my $action_rs = $c->stash->{ case_v0 }->case_actions_cine;

    my @action_groups;

    while (my $milestone = $milestones->next) {
        push @action_groups, {
            milestone_id => $milestone->id,
            milestone => $milestone->status,
            items => [ $action_rs->milestone($milestone->status)->sorted ]
        };
    }

    $c->stash->{ zapi } = \@action_groups;
}

=head1 PRIVATE METHODS

=head2 _assert_case_authorizations

Helper for the L<Zaaksysteem::Zaken::Roles::Domain/ddd_is_editable_by>
permissions checks.

May throw C<api/v0/case/user_not_permitted>, otherwise returns.

=cut

sub _assert_case_authorizations {
    my $self = shift;
    my $c = shift;
    my $context = shift;
    my $action = shift || 'update';

    try {
        $c->stash->{ case_v0 }->ddd_assert_user_authorizations(
            $c->user,
            sub { return $c->check_any_given_zaak_permission(@_) },
            $context,
            $action
        )
    } catch {
        $c->log->info("$_");

        throw(
            sprint(
                'api/v0/case/%s/%s/user_not_permitted',
                $context,
                $action
            ),
            sprintf(
                'Current user is not permitted to %s case %s',
                $action,
                $context
            ),
            { http_code => 403 }
        );
    }

    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
