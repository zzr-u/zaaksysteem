package Zaaksysteem::Controller::API::Case::File;

use Moose;

use Archive::Tar::Stream;
use Archive::Zip qw(:CONSTANTS);
use BTTW::Tools;
use DateTime;
use Encode qw(encode_utf8);
use List::MoreUtils qw[first_index];
use List::Util qw[first];
use Zaaksysteem::Types qw(UUID);
use Zaaksysteem::Tie::CallingHandle;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

sub index : Chained('/api/case/base') : PathPart('file') : CaptureArgs(1) {
    my ($self, $c, $file_id) = @_;

    my %args;
    if ($file_id =~ /^[0-9]+$/) {
        $args{id} = $file_id;
    }
    elsif (UUID->check($file_id)) {
        $args{uuid} = $file_id;
    }
    else {
        throw('api/case/file/param', "No ID or UUID given");
    }

    $c->stash->{ file } = $c->stash->{zaak}->files->active->search_rs(\%args)->first;
    return if $c->stash->{file};

    throw('api/case/file/not_found', "file $file_id not available");
}

sub get : Chained('index') : PathPart("") : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ zapi } = [ $c->stash->{file} ];
}


=head2 directory_tree

Retrieve the full directory/file tree for a specific case

=head3 URL

C<< /api/case/<case_id>/directory/tree >>

=cut


sub directory_tree : Chained('/api/case/base') : PathPart('directory/tree') : ZAPI {
    my ($self, $c) = @_;

    # Sorting by length of array always guarantees we receive rows
    # in a breadth-first tree-walk
    my $rs = $c->stash->{ zaak }->directories->search(undef, {
        order_by => \'array_length(path, 1) ASC NULLS FIRST'
    });

    my @files = $c->stash->{ zaak }->files->document_list->all;
    $c->stash->{zapi_hide_mappings}
        = $c->stash->{zaak}->confidential_access ? 0 : 1;

    my $root = {
        _root => \1,
        children => [],
        files => [ grep { not $_->get_column('directory_id') } @files ],
    };

    for my $directory ($rs->all) {
        my $data = $directory->TO_JSON;
        my $parent = $root;

        $data->{ children } = [];
        $data->{ files } = [
            grep { $_->get_column('directory_id') eq $directory->id }
            grep { defined $_->get_column('directory_id') }
                 @files
        ];

        for my $parent_id (@{ $directory->path }) {
            $parent = first {
                $_->{ id } eq $parent_id
            } @{ $parent->{ children } };
        }

        push @{ $parent->{ children } }, $data;
    }

    $c->stash->{ zapi } = [ $root ];
}

=head2 move_directory

Move a subtree of directories into another parent.

=head3 URL

C<< /api/case/<case_id>/directory/move_directory >>

=head3 Parameters

=over 4

=item directory_id

Expected to be the primary key of the directory to be moved

=item parent_directory_id

Expected to be the primary key of the new parent directory

=back

=head3 Exceptions

=over 4

=item case/directory/move/self_referential_move

This exception is thrown when a call is made to move a directory into one
of it's own descendants.

=item case/directory/move/parent_not_found

Thrown when the target parent directory could not be found by ID.

=item case/directory/move/directory_not_found

Thrown when the directory to be moved could not be found by ID.

=back

=cut

define_profile move_directory => (
    required => {
        directory_id => 'Int',
    },
    optional => {
        parent_directory_id => 'Int'
    }
);

sub move_directory : Chained('/api/case/base') : PathPart('directory/move_directory') : ZAPI {
    my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    my @new_path;

    if (exists $opts->{ parent_directory_id }) {
        if ($opts->{ directory_id } eq $opts->{ parent_directory_id }) {
            throw(
                'case/directory/move/self_referential_move',
                'Refusing to move directory into itself'
            );
        }

        my $parent = $c->stash->{ zaak }->directories->find(
            $opts->{ parent_directory_id }
        );

        unless ($parent) {
            throw('case/directory/move/parent_not_fount', sprintf(
                'Unable to find parent directory "%s"',
                $opts->{ parent_directory_id }
            ));
        }

        if (grep { $opts->{ directory_id } eq $_ } @{ $parent->path }) {
            throw(
                'case/directory/move/self_referential_move',
                'Refusing to move parent into descendant'
            );
        }

        @new_path = (@{ $parent->path }, $parent->id);
    }

    my $directory = $c->stash->{ zaak }->directories->find($opts->{ directory_id });

    unless ($directory) {
        throw('case/directory/move/directory_not_found', sprintf(
            'Unable to find directory "%s"',
            $opts->{ directory_id }
        ));
    }

    # Update paths atomically, prevent inconsistant state
    $c->model('DB')->txn_do(sub {
        $directory->path(\@new_path);
        $directory->update;

        my @subdirs = $c->stash->{ zaak }->directories->search(
            { path => { '@>' => sprintf('{%d}', $directory->id) } },
        );

        for my $subdir (@subdirs) {
            my @path = @{ $subdir->path };

            my $index = first_index { $_ eq $directory->id } @path;

            splice @path, 0, $index, @new_path;

            $subdir->path(\@path);
            $subdir->update;
        }
    });

    $c->stash->{ zapi } = [ { success => 1 } ];
}

=head2 search

Display active files for a case. Used to supply the document tab.

=cut

sub search : Chained('/api/case/base') : PathPart('file/search') : ZAPI {
    my ($self, $c) = @_;

    $c->stash->{zapi_no_pager} = 1;
    $c->stash->{zapi} = $c->stash->{zaak}->active_files;
}

=head2 download

Download files related to the case via zip or tar archive. If no files are mentioned all files for the case are downloaded. If no format is given, zip is assumed.

=head3 SYNOPSIS

    GET /api/case/4/file/download/?file=1&file=2&format=tar HTTP/1.1

=cut

sub download : Chained('/api/case/base') : PathPart('file/download') : GET {
    my ($self, $c) = @_;

    my $format = $c->req->param('format') || 'zip';
    my @files  = $c->req->param('file');
    my $rs = $c->stash->{zaak}->search_active_files(\@files);
    if ($rs->count == 0) {
        $c->res->body('No files found!');
        return;
    }

    my $filename = sprintf("zs-%s-%s.%s",
        $c->stash->{zaak}->id,
        DateTime->now->iso8601,
        $format
    );

    $c->res->header('Content-Disposition',
        qq[attachment; filename="$filename"]);

    my $handle = Zaaksysteem::Tie::CallingHandle->create(
        write_cb => sub { $c->res->write($_[0]); }
    );

    # Temporary variable to keep get_path results until the ZIP is done,
    # because get_path can return File::Temp objects, which self-destruct
    # when they go out of scope.
    my @tempfiles;

    if ($format eq 'tar') {
        $c->res->content_type('application/x-tar');
        my $tar = Archive::Tar::Stream->new(outfh => $handle);

        while (my $file = $rs->next) {
            my $fh = $file->filestore->read_handle;
            my $rc = $tar->AddFile($file->filepath, -s $fh, $fh, mode => oct('0644'));
        }
        $tar->FinishTar();
    }
    else {
        $c->res->content_type('application/zip');

        my $zip = Archive::Zip->new();
        while (my $file = $rs->next) {
            my $fh = $file->filestore->get_path;
            push @tempfiles, $fh;

            # Stringify the file handle to get the file name (in case it's a
            # File::Temp object)
            $zip->addFile({
                filename         => "$fh",
                zipName          => encode_utf8($file->filename),
                compressionLevel => COMPRESSION_LEVEL_BEST_COMPRESSION,
            });
        }

        $zip->writeToFileHandle($handle);
    }

    # Streaming downloads don't mix well with views.
    $c->res->body('');
}

=head2 copy_to_case

Copy selected files to another case

=head3 URI

/api/case/:case_id/file/copy_to_case

=head3 JSON

    {
        // File ID's from the current case
        file_id : [ 1, 2, 4],

        // The target case ID
        case_id : 42,
    }

=cut

define_profile copy_to_case => (
    required => {
        file_id => 'Int',
        case_id => 'Int',
    },
);

sub copy_to_case : Chained('/api/case/base') : PathPart('file/copy_to_case') : ZAPI {
    my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    throw(
        "case/file/copy/target_case/current_case",
        "Unable to copy file to current case"
    ) if $opts->{case_id} == $c->stash->{zaak}->id ;

    my $target_case = $c->model('DB::Zaak')->find($opts->{case_id});

    throw(
        "case/file/copy/target_case/not_found",
        "Case $opts->{case_id} is not found"
    ) if !$target_case;

    # Force arrayref context
    if (ref $opts->{file_id} ne 'ARRAY') {
        $opts->{file_id} = [ $opts->{file_id} ];
    }

    my $rs = $c->stash->{zaak}->search_active_files($opts->{file_id});

    my @files;
    while (my $file = $rs->next) {
        push(
            @files,
            $file->copy_to_target_case(
                target_case => $target_case,
                subject     => $c->user,
            )
        );
    }

    $c->stash->{zapi} = \@files;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 index

TODO: Fix the POD

=cut

