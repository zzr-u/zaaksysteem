package Zaaksysteem::Controller::API::Case::Object;

use Moose;

use BTTW::Tools;
use Zaaksysteem::Types qw[UUID];

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::Case::Object

=head1 DESCRIPTION

This controller handles interactions on case objects in regard to related
objects and mutations on those objects.

=head1 ACTIONS

=head2 mutation_base

Base action for this controller. It sets up a C<mutation_rs> key in the
L<Catalyst/c-stash>, which is a L<Zaaksysteem::Schema::ObjectMutation>
resultset, joined on the case, and sorted by creation date.

=cut

sub mutation_base : Chained('/api/case/base') : PathPart('mutations') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{ mutation_rs } = $c->stash->{ case }->object_mutation_lock_object_uuids->search(undef, {
        order_by => 'date_created'
    });
}

=head2 mutation_list

This B<GET> action returns a ZAPI JSON response with all mutations locked
by the case based on the supplied C<object_type> parameter.

=head3 URL

C</api/case/[CASE_ID]/mutations>

=head3 Parameters

=over 4

=item object_type

This parameter is expected to be a string representation of the object type
for which the mutations must be returned, e.g., C<plantenbak>, C<contract>,
or C<type>.

=back

=cut

define_profile mutation_list => (
    optional => {
        object_type => 'Str'
    }
);

sub mutation_list : Chained('/api/v0/case/instance_base') : PathPart('mutations') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    my $args = assert_profile($c->req->params)->valid;

    my $mutations_model = $c->model('ObjectMutation', {
        zaak => $c->stash->{ case_v0 }
    });

    $c->stash->{ zapi } = [ $mutations_model->search($args) ];
}

=head2 mutation_add

This B<POST> action consumes a provided mutation object, and attempts to add
it to the case mutationslist. It may fail if another object (usually a case)
already has a lock on the object to be mutated.

=head3 URL

C</api/case/[CASE_ID]/mutations/add>

=head3 Parameters

The parameters for this action are expected to be encoded in a JSON object.

    {
        "object_type": "object type prefix here",

        "mutation": {
            "type"       : (create|update|delete),
            "id"         : UUID,
            "object_uuid": UUID,
            "values"     : {
               ...
            }
        }
    }

=head3 Response

This action internally forwards to L<Zaaksysteem::Controller::API::Case/get>,
and responds with the updated case JSON hydration.

=cut

define_profile mutation_add => (
    required => {
        mutation => 'HashRef',
        object_type => 'Str',
    }
);

sub mutation_add : Chained('mutation_base') : PathPart('add') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    my $params = assert_profile($c->req->params)->valid;

    $c->stash->{ case }->add_mutation(
        $c->user,
        $params->{ object_type },
        $params->{ mutation }
    );

    $c->detach('/api/case/get');
}

=head2 mutation_update

=head3 URL

C</api/case/[CASE_ID]/mutations/update>

=head3 Parameters

The parameters for this action are expected to be encoded in a JSON object.

    {
        "mutation": {
            "id": UUID,
            "object_uuid": UUID,
            "type": (create|update|delete),
            "values": {
                ...
            }
        }
    }

=cut

define_profile mutation_update => (
    required => { mutation => 'HashRef' }
);

sub mutation_update : Chained('mutation_base') : PathPart('update') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    my $params = assert_profile($c->req->params)->valid;

    $c->stash->{ case }->update_mutation(
        $c->user,
        $params->{ mutation }
    );

    $c->detach('/api/case/get');
}

=head2 mutation_delete

=head3 URL

C</api/case/[CASE_ID]/mutations/delete>

=head3 Parameters

The parameters for this action are expected to be encoded in a JSON object.

    {
        "id": UUID
    }

=over 4

=item id

This ID should reference a mutation that has previously been added to the
case.

=back

=head3 Response

This action internally forwards to L<Zaaksysteem::Controller::API::Case/get>,
and responds with the updated case JSON hydration.

=cut

define_profile mutation_delete => (
    required => { id => UUID }
);

sub mutation_delete : Chained('mutation_base') : PathPart('delete') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    my $id = assert_profile($c->req->params)->valid->{ id };

    $c->stash->{ case }->delete_mutation($id);

    $c->detach('/api/case/get');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
