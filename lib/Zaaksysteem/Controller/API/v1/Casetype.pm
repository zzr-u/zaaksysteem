package Zaaksysteem::Controller::API::v1::Casetype;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Casetype - APIv1 controller for casetype objects

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use Zaaksysteem::API::v1::ArraySet;
use Zaaksysteem::API::v1::CasetypeACL;
use Zaaksysteem::API::v1::ResultSet;
use Zaaksysteem::API::v1::Set;

sub BUILD {
    my $self = shift;

    $self->add_api_control_module_type('api', 'app');
    $self->add_api_context_permission('extern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/casetype> routing namespace.

=cut

sub base : Chained('/api/v1/base') : PathPart('casetype') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my $casetypes;
    if (my $zql = $c->req->params->{zql}) {
        my $zql_search = Zaaksysteem::Search::ZQL->new($zql);

        if ($zql_search->cmd->from->value ne 'casetype') {
            throw('api/v1/case/query_fault', sprintf(
                'Cannot parse ZQL, only objecttype "casetype" is allowed.'
            ));
        }

        $casetypes = $c->model('Object')->zql_search($zql);
    } else {
        $casetypes = $c->model('Object')->search('casetype');
    }

    my $set = try {
        return Zaaksysteem::API::v1::Set->new(iterator => $casetypes)->init_paging($c->request);
    } catch {
        $c->log->warn($_);

        throw(
            'api/v1/casetype/configuration_error',
            'API configuration error, unable to continue'
        );
    };

    $c->stash->{ set } = $set;
    $c->stash->{ casetypes } = $casetypes->rs;
}

=head2 instance_base

Reserves the L</api/v1/casetype/[CASETYPE_UUID]> routing namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    $c->stash->{ casetype } = try {
        return $c->model('Object')->inflate_from_row(
            $c->stash->{ casetypes }->find($uuid)
        );
    } catch {
        $c->log->debug($_);

        throw('api/v1/casetype/retrieval_fault', sprintf(
            'Casetype retrieval failed, unable to continue.'
        ));
    };

    my $version = $c->req->params->{ version };

    if (defined $version && $c->stash->{ casetype }->version ne $version) {
        $c->stash->{ casetype } = try {
            my $zaaktype = $c->model('DB::Zaaktype')->find(
                $c->stash->{ casetype }->casetype_id
            );

            return $zaaktype->build_casetype_object(version => $version);
        } catch {
            $c->log->debug($_);

            throw('api/v1/casetype/retrieval_fault', sprintf(
                'Casetype retrieval failed, unable to continue.'
            ));
        }
    }

    unless (defined $c->stash->{ casetype }) {
        throw('api/v1/casetype/not_found', sprintf(
            "The casetype object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }
}

=head2 list

=head3 URL Path

C</api/v1/casetype>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ set };
}

=head2 list_allowed_users

=head3 URL Path

C</api/v1/casetype/list_allowed_users>

=cut

sub list_allowed_users : Chained('base') : PathPart('list_allowed_users') : Args(0) : RO {
    my ($self, $c) = @_;

    my $acl = Zaaksysteem::API::v1::CasetypeACL->new(schema => $c->model('DB')->schema);
    $c->stash->{set} = Zaaksysteem::API::v1::ArraySet->new(content => $acl->get_casetype_users);
    $self->list_set($c);
}

=head2 get

=head3 URL Path

C</api/v1/casetype/[CASETYPE_UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ is_cacheable } = 1;

    my $max_age = 6 * 3600; # 6 hours in seconds

    $c->res->headers->last_modified($c->stash->{casetype}->date_modified->epoch);

    # The "create new case" slide-out doesn't request a specific version of the
    # case type.
    # This means it could be caching a stale (old) version.
    if (exists $c->req->params->{ version }) {
        $c->res->header('Cache-Control' => "private, max-age=$max_age");
    }

    $c->stash->{ result } = $c->stash->{ casetype };
}

=head2 list_allowed_users_ct

=head3 URL Path

C</api/v1/casetype/[CASETYPE_UUID]/list_allowed_users>

=cut

sub list_allowed_users_ct : Chained('instance_base') : PathPart('list_allowed_users') : Args(0) : RO {
    my ($self, $c) = @_;

    my $acl = Zaaksysteem::API::v1::CasetypeACL->new(schema => $c->model('DB')->schema);
    $c->stash->{result} = $acl->get_casetype_users({casetype_uuid => $c->stash->{casetype}->id})->[0];
}

sub list_authorisation_casetype_instance : Chained('instance_base') : PathPart('authorisation') : Args(0) : RO {
    my ($self, $c) = @_;

    my $auth_rs = $c->model("DB::ZaaktypeAuthorisation")->search_rs({
        zaaktype_id => $c->stash->{casetype}->casetype_id
    });

    my $set =  Zaaksysteem::API::v1::ResultSet->new(
        iterator => $auth_rs,
    );
    $set->init_paging($c->request);
    $c->stash->{result} = $set;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
