package Zaaksysteem::Controller::API::v1::Session;

use Moose;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Session - APIv1 controller for session information

=head1 DESCRIPTION

This is the controller API class for C<api/v1/session>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Session>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Session>

=cut

use BTTW::Tools;
use Zaaksysteem::API::v1::Object::Session;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('public_access');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/session> routing namespace.

=cut

sub base : Chained('/api/v1/base') : PathPart('session') : CaptureArgs(0) {}

=head2 current

Current session information.

=head3 URL Path

C</api/v1/session/current>

=cut

sub current : Chained('base') : PathPart('current') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{result}
        = Zaaksysteem::API::v1::Object::Session->new_from_catalyst($c);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
