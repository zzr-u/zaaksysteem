package Zaaksysteem::Controller::API::v1::Sysin::Interface;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

### WARNING: Every action in this controller is ACCESS: Public, no logged in user required. Handle (custom)
### authentication in subroutines.

=head1 NAME

Zaaksysteem::Controller::API::V1::Sysin::Interface - API calls for system integration module: Interface

=head1 DESCRIPTION

This is the controller API class for C<api/v1/sysin/interface>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Sysin::Interface>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Sysin::Interface>

=cut

use BTTW::Tools;
use List::MoreUtils qw(none);
use Zaaksysteem::API::v1::ArraySet;
use Zaaksysteem::Constants::Users qw[ADMIN REGULAR];
use Zaaksysteem::Types qw[UUID];

sub BUILD {
    my $self = shift;
    $self->add_api_context_permission('extern', 'public_access');
    $self->add_api_control_module_type('sysin/interface');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/sysin/interface> routing namespace.

=cut

sub base : Chained('/api/v1/sysin/base') : PathPart('interface') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my $base_rs;
    my @interfaces;
    if ($c->check_user_mask(REGULAR)) {
        $base_rs = $c->model('DB::Interface')->search_active;
    }
    else {
        $base_rs = $c->model('DB::Interface')->search_active_restricted;
    }
    $c->stash->{base_rs} = $base_rs;
}

=head2 instance_base

Reserves the C</api/v1/sysin/interface/[INTERFACE_UUID> routing namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $interface_id) = @_;

    unless (UUID->check($interface_id)) {
        throw('api/v1/sysin/interface/invalid_id', sprintf(
            'Provided interface id "%s" is not a valid UUID',
            $interface_id
        ));
    }

    $c->stash->{interface} = $c->stash->{base_rs}->search_rs({
        uuid => $interface_id
    })->first;

    throw(
        'api/v1/sysin/interface/unknown_id',
        "Unknown interface id '$interface_id'"
    ) if !$c->stash->{interface};
}

=head2 security_base

Chains from the L</base> action and asserts several
authentication/authorization predicates.

=cut

sub security_base : Chained('base') : PathPart('') : CaptureArgs(0) {
    my ($self, $c)  = @_;

    $c->assert_user;

    $c->stash->{v1_serializer_options}{is_admin} = $c->check_any_user_permission('admin');
    $c->stash->{v1_serializer_options}{user}     = $c->user;
}

=head2 get_all

Retrieve interface configuration for all supported (whitelisted) interfaces.

=head3 URL Path

C</api/v1/sysin/interface/get_all>

=cut

sub get_all : Chained('security_base') : PathPart('get_all') : RO {
    my ($self, $c) = @_;

    $self->_get_all($c);
}

sub list_all : Chained('security_base') : PathPart('') : RO {
    my ($self, $c) = @_;

    $self->_get_all($c);
}

sub _get_all {
    my ($self, $c) = @_;
    my $rs = $c->stash->{base_rs};

    my @allowed_modules = $rs->get_whitelisted_interfaces;

    my @interfaces;
    if (@allowed_modules) {
        @interfaces = $c->stash->{base_rs}->search_rs({
            module => { -in => \@allowed_modules }
        })->all;
    }

    $c->stash->{set} = Zaaksysteem::API::v1::ArraySet->new(
        content => \@interfaces
    );

    $self->list_set($c);
}

=head2 get

Retrieves an L<interface> object instance.

This action requires C<ADMIN> user permissions.

=head3 URL Path

C</api/v1/sysin/interface/[INTERFACE_UUID]>.

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    # Interfaces expose passwords, passphrases, and other sensitive and/or
    # secret data. Only trust admins here.
    $c->assert_user(ADMIN);

    $c->stash->{ result } = $c->stash->{ interface };
}

=head2 trigger

Triggers an interface process

=head3 URL Path

C</api/v1/sysin/interface/[INTERFACE_UUID]/trigger/[TRIGGER_NAME]>

=cut

sub trigger : Chained('instance_base') : PathPart('trigger') : Args(1) : RW {
    my ($self, $c, $trigger)  = @_;
    my $params      = {};

    my $def = $c->stash->{interface}->get_trigger_definition($trigger);

    if (! $def->{api}) {
        throw('api/v1/method_not_allowed', sprintf(
            'Trigger is not available through this API: "%s"',
            $trigger
        ));
    }

    if ($def->{api_allowed_users}) {
        $c->assert_user($def->{api_allowed_users});
    }

    if ($def->{api_is} && $def->{api_is} eq 'rw' && $c->req->method ne 'POST') {
        throw('api/v1/method_not_allowed', sprintf(
            'HTTP method "%s" is not allowed for trigger "%s": POST required',
            $c->req->method,
            $trigger,
        ));
    }

    ### Get necessary params:
    $params->{request_params}   = $c->req->params;

    ### Hash with uploads as key value pair, e.g. { form_upload => bless({ ... }, 'Catalyst::Request::Upload') }
    $params->{uploads}          = $c->req->uploads;

    ### HTTP::Headers
    $params->{headers}          = $c->req->headers;

    ### Body
    $params->{body}             = $c->req->body;

    $c->log->debug(sprintf(
        'api/v1/interface/trigger - Content-Type: "%s"',
        $c->req->content_type // ''
    ));

    ### Method
    $params->{method}           = lc($c->req->method);

    ### The Object model
    if ($c->user_exists) {
        $params->{object_model} = $c->model('Object');
    }

    ### Resolve all references in the request parameters
    if (exists $params->{request_params}{references}) {
        $self->_resolve_references($c, $params->{request_params}{references});
    }

    ### API trigger
    my $result                  = $c->stash->{interface}->process_api_trigger($trigger, $params);

    my $serializer = Zaaksysteem::API::v1::Serializer->new;

    if (blessed($result)) {
        if ($result->isa('DBIx::Class::ResultSet')) {
            $c->stash->{ result } = {
                type => 'set',
                instance => {
                    rows => [ map { $serializer->read($_) } $result->all ]
                }
            }
        } elsif ($result->isa('DBIx::Class::Row')) {
            ### A downlaod?
            if (
                $c->req->params->{result_as} eq 'download' &&
                $result->isa('Zaaksysteem::Model::DB::File')
            ) {
                $c->stash->{document} = $result;
                $c->detach('/api/v1/case/document/download');
            } else {
                $c->stash->{ result } = $result;
            }
        } else {
            $c->stash->{ result } = $result;
        }
    } elsif ( $self->_is_valid_serialized_object($result)) {
        $c->stash->{ result } = $result;
    }
}

=head2 get_by_module_name

Retrieve a specific interface configuration by interface name.

=head3 URL Path

C</api/v1/sysin/interface/get_by_module_name/[MODULE_NAME]>

=cut

sub get_by_module_name : Chained('security_base') : PathPart('get_by_module_name') : Args(1) : RO {
    my ($self, $c, $module_name) = @_;

    my $name = lc($module_name);

    unless (eval { $c->config->{ Sysin }{ $name }{ api_whitelisted } }) {
        throw(
            "api/v1/sysin/interface/get_module",
            "Unable to retrieve module information"
        );
    }

    my @interfaces= $c->stash->{base_rs}->search_rs({
        module => $name,
    })->all;

    $c->stash->{set} = Zaaksysteem::API::v1::ArraySet->new(
        content => \@interfaces
    );
    $self->list_set($c);
}

sub _resolve_references {
    my ($self, $c, $references) = @_;

    for my $k (keys %$references) {
        if ($references->{$k}{type} eq 'subject') {
            my $subject = $c->model('BR::Subject')->find($references->{$k}{reference});

            throw(
                'api/v1/sysin/interface/trigger/reference_not_found',
                sprintf(
                    'Unknown subject reference "%s"',
                    $references->{$k}{reference}
                )
            ) unless defined $subject;

            $references->{$k} = $subject;
        }
        else {
            throw('api/v1/sysin/interface/trigger/unknown_reference', sprintf(
                'Unsupported reference type: "%s"',
                $references->{$k}{type}
            ));
        }
    }

    return;
}

sub _is_valid_serialized_object {
    my $self   = shift;
    my $object = shift;

    return 1 if (ref $object eq 'HASH' && $object->{type} && $object->{instance});
    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
