package Zaaksysteem::Controller::API::Object;

use Moose;
use namespace::autoclean;

use Archive::Tar::Stream;
use BTTW::Tools;
use Digest::SHA qw(sha1_hex);
use JSON qw[encode_json decode_json];
use Moose::Util qw[ensure_all_roles];
use Moose::Util::TypeConstraints qw[union enum];

use Zaaksysteem::Attributes;
use Zaaksysteem::Constants qw[OBJECT_ACTIONS];
use Zaaksysteem::Object::Iterator;
use Zaaksysteem::Object::SecurityIdentity;
use Zaaksysteem::Tie::CallingHandle;
use Zaaksysteem::Types qw(UUID Boolean);
use Zaaksysteem::ZAPI::Form::Action;
use Zaaksysteem::ZAPI::Form::Builder;


BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::Object - Zaaksysteem Object API

=head1 SYNOPSIS


=head1 DESCRIPTION

A generic API for querying Zaaksysteem Objects.

=head1 API

For a basic knowledge about our JSON API, please take a look at
L<Zaaksysteem::Manual::API>

=head1 ACTIONS

=head2 object_base

This is a base action that validates that a given object id is a UUID and that
the object it refers to exists. It stores the object in the stash for chaining
controllers to use.

=cut

define_profile object_base => (
    required => {
        uuid => UUID,
    },
);

sub object_base : Chained('/api/base') : PathPart('object') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    $uuid = assert_profile({ uuid => $uuid })->valid->{ uuid };

    my $object = $c->model('Object')->retrieve(uuid => $uuid);

    unless(blessed($object)) {
        throw('api/object', sprintf(
            'No object could be found for the given UUID (%s)',
            $uuid
        ));
    }

    ### TODO dekludge this code via event messaging infra
    # {{{
    my $subject = $c->betrokkene_session;

    if($subject && ($object->type eq 'product' || $object->type eq 'vraag')) {
        $c->model('DB::Logging')->trigger('object/view', {
            created_for => $subject->betrokkene_identifier,
            object_uuid => $object->id,
            data => {
                object_label => $object->TO_STRING,
                subject_label => $subject->naam
            }
        });
    }
    # }}}

    $c->stash->{ object } = $object;
}

=head2 search_base

Base controller for search

=cut

define_profile search_base => (
    required => {
        zql => 'Str',
    }
);

sub search_base : Chained('/api/base') : PathPart('object/search') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my $query = assert_profile($c->req->params)->valid->{ zql };

    my $zql = Zaaksysteem::Search::ZQL->new($query);

    $c->stash->{ zql }        = $zql;
    $c->stash->{ zql_string } = $query;
    $c->stash->{ search_rs }  = $zql->apply_to_resultset($c->model('Object')->acl_rs);
}

=head2 search

L<http://10.44.0.11/api/object/search>

    {
       "next" : "http://10.44.0.11/api/object/search?zql=select+%7B%7D+from+case&zapi_page=2",
       "status_code" : "200",
       "prev" : null,
       "num_rows" : "2",
       "rows" : 2,
       "comment" : null,
       "at" : null,
       "result" : [
          {
             "object_type" : "case",
             "values" : {
                "zaaktype.titel" : "Testzaaktype",
                "test_02" : "25-01-2013",
                "test_03" : [
                   "Optie A"
                ],
                "test_04" : "09",
                "test_01" : [
                   "Optie 3"
                ],
                "case.registratiedatum" : "2013-01-24T14:09:56",
                "omschrijving" : "testest",
                "test_09" : [
                   "Nee"
                ],
                "test_08" : "testuleer",
                "case.id" : 2,
                "case.parent_case": [     #### XXX FIXME TODO (Relationships)
                  {
                     "object_type" : "case",
                     "values" : {
                        "zaaktype.titel" : "Testzaaktype",
                        "test_02" : "25-01-2013",
                        "test_03" : [
                           "Optie A"
                        ],
                        "test_04" : "09",
                        "test_01" : [
                           "Optie 3"
                        ],
                        "case.registratiedatum" : "2013-01-24T14:09:56",
                        "omschrijving" : "testest",
                        "test_09" : [
                           "Nee"
                        ],
                        "test_08" : "testuleer",
                        "case.id" : 7,
                        "case.parent_case": null,
                        "case.afhandeldatum" : null,
                        "test_07" : [
                           "Nee"
                        ],
                        "toelichting" : null
                     },
                     "object_id" : 7
                  },
                ],
                "case.afhandeldatum" : null,
                "test_07" : [
                   "Nee"
                ],
                "toelichting" : null
             },
             "object_id" : 2
          },
          {
             "object_type" : "case",
             "values" : {
                "zaaktype.titel" : "Testzaaktype",
                "test_02" : "25-01-2013",
                "test_03" : [
                   "Optie A"
                ],
                "test_04" : "09",
                "test_01" : [
                   "Optie 3"
                ],
                "case.registratiedatum" : "2013-01-24T14:09:56",
                "omschrijving" : "testest",
                "test_09" : [
                   "Nee"
                ],
                "test_08" : "testuleer",
                "case.id" : 3,
                "case.afhandeldatum" : null,
                "test_07" : [
                   "Nee"
                ],
                "toelichting" : null
             },
             "object_id" : 3
          }
       ]
    }

Returns a resultset of objects for the given type. Now, only the object type
case is here to use.

=cut

sub search : Chained('search_base') : PathPart('') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    my $zql = $c->stash->{ zql };

    if ($zql->cmd->isa('Zaaksysteem::Search::ZQL::Command::Count')) {
        $c->forward('search_count');
    } elsif ($zql->cmd->isa('Zaaksysteem::Search::ZQL::Command::Select')) {
        $c->forward('search_select');
    } else {
        throw('api/object/search', sprintf(
            'Unsupported ZQL command: "%s"',
            $c->stash->{ zql }->cmd
        ));
    }
}

=head2 get

Retrieves an L<Object|Zaaksysteem::Object> from the database, given its UUID.

=head3 URL

GET C</api/object/[UUID]>

=cut

sub get : Chained('object_base') : PathPart('') : Args(0) : GET : ZAPI {
    my ($self, $c) = @_;

    $c->stash->{ zapi } = [ $c->stash->{ object } ];
}

=head2 delete

Deletes an L<Object|Zaaksysteem::Object> from the database.

=head3 URL

POST C</api/object/[UUID]/delete>

=cut

sub delete : Chained('object_base') : PathPart('delete') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;

    $c->model('Object')->delete(object => $c->stash->{ object });

    $c->stash->{ zapi } = [];
}

=head2 file

Provides access to files referenced by an object.

The response will have a B<attachment> C<Content-Disposition> (user agents
must download it, not view it).

=head3 URL

GET C</api/object/[UUID]/file/[FILE_UUID]>

=cut

sub file : Chained('object_base') : PathPart('file') : Args(1) : GET {
    my ($self, $c, $file_id) = @_;

    my $file = $c->model('DB::Filestore')->search({ uuid => $file_id })->first;

    unless($file) {
        throw('object/files/file_not_found');
    }

    $c->forward('serve_file', [$file]);
}

=head2 serve_file

=over 4

=item Arguments: $ROW_FILESTORE

=back

    $c->forward('serve_file', [$c->model('DB::Filestore')->first]);

Serves the file via the browser.

=cut

sub serve_file : Private {
    my ($self, $c, $file) = @_;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $files = $file->files;

    while (my $file = $files->next) {
        if ($file->confidential && !$file->case_id->confidential_access) {
            $c->serve_file($file);
            return;
        }
    }

    $c->serve_filestore($file);

    $c->res->header('Cache-Control', 'must-revalidate');
    $c->res->header('Pragma', 'private');
    $c->res->header('Content-Disposition' => sprintf('attachment; filename="%s"', $file->original_name));

    Zaaksysteem::StatsD->statsd->end('serve_file.apiv1_object_serve_file.time', $t0);


}

=head2 relate

Relates the provided related object to the current main object.

=head3 URL

POST C</api/object/[UUID]/relate>

=head3 Parameters

This action expects at least a C<related_object_id>, and optionally a C<name>.

=cut

define_profile relate => (
    required => {
       related_object_id => UUID,
    },
    optional => {
        name => 'Str',
        blocks_deletion => Boolean
    }
);

sub relate : Chained('object_base') : PathPart('relate') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;

    unless ($c->stash->{ object }->does('Zaaksysteem::Object::Roles::Relation')) {
        throw('api/object/relate', sprintf(
            '"%s" does not implement relationships.',
            $c->stash->{ object }
        ));
    }

    my $opts = assert_profile($c->req->params)->valid;

    my $related_object = $c->model('Object')->retrieve(
        uuid => $opts->{ related_object_id }
    );

    unless (defined $related_object) {
        throw('api/object/relate', sprintf(
            'Unable to find object to relate by id (%s).',
            $opts->{ related_object_id }
        ));
    }

    my $relation = $c->stash->{ object }->relate($related_object);

    if (exists $opts->{ name }) {
        $relation->relationship_name_a($opts->{ name });
    }

    if (exists $opts->{ blocks_deletion }) {
        $relation->blocks_deletion($opts->{ blocks_deletion } ? 1 : 0);
    }

    $c->stash->{ zapi } = [
        $c->model('Object')->save(object => $c->stash->{ object })
    ];
}

=head2 unrelate

Unrelates the provided related object to the current main object.

=head3 URL

POST C</api/object/[UUID]/unrelate>

=head3 Parameters

This action expects a valid UUID in C<related_object_id>.

=cut

define_profile unrelate => (
    required => {
        related_object_id => UUID
    }
);

sub unrelate : Chained('object_base') : PathPart('unrelate') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;

    unless ($c->stash->{ object }->does('Zaaksysteem::Object::Roles::Relation')) {
        throw('api/object/relate', sprintf(
            '"%s" does not implement relationships.',
            $c->stash->{ object }
        ));
    }

    my $related_object_id = assert_profile($c->req->params)->valid->{ related_object_id };

    my $related_object = $c->model('Object')->retrieve(uuid => $related_object_id);

    unless (defined $related_object) {
        throw('api/object/unrelate', sprintf(
            'Provided related_object_id (%s) could not be found in the database',
            $related_object_id
        ));
    }

    $c->stash->{ object }->unrelate($related_object);

    $c->stash->{ zapi } = [
        $c->model('Object')->save(object => $c->stash->{ object })
    ];
}

=head2 ratify

Ratify an authorization for an object. This can mean to permit or proscribe a
certain action in the context of the object.

=head3 URL

POST C</api/object/[UUID]/ratify>

=head3 Parameters

=over 4

=item entity_id

This required argument is expected to uniquely identify a remote entity.

=item entity_type

This required argument is expected to be the type/namespace of the identifier
supplied in C<entity_id>.

=item capability

This argument references the 'action' on the object for which a verdict is
being ratified.

=item groupname

This optional argument is expected to be a string name for the ACL group the
ACL entry should be created for.

=back

=cut

define_profile ratify => (
    required => {
        entity_id => 'Str',
        entity_type => 'Str',
        capability => 'Str',
    },
    optional => {
        groupname => 'Str'
    }
);

sub ratify : Chained('object_base') : PathPart('ratify') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;

    unless ($c->stash->{ object }->does('Zaaksysteem::Object::Roles::Security')) {
        throw(
            'object/security',
            'Unable to ratify on an instance of an object type that does not implement a security scheme'
        );
    }

    my $opts = assert_profile($c->req->params)->valid;

    # XXX Kludge starts here. We need a proper thing that accepts an entity
    # hash and returns the correct entity object (and is testable).
    ### Well, we sort-of fixed part of it, kinda. Objects are now resolved via
    ### their respective models, but it's still not really clean or neat.
    my $entity;

    if ($opts->{entity_type} eq 'user') {
        $entity = $c->model('DB::Subject')->find({ username => $opts->{ entity_id } });
    } elsif ($opts->{entity_type} eq 'position') {
        $entity = $c->model('Object')->find_position($opts->{ entity_id });
    } else {
        throw(
            'object/unknown_entity_type',
            "Unable to ratify for an entity of unknown type (known types: user, position)"
        );
    }

    unless($entity) {
        throw('object/unknown_security_identity', sprintf(
            'Could not resolve entity id "%s" for type "%s"',
            $opts->{ entity_id },
            $opts->{ entity_type }
        ));
    }

    $c->stash->{object}->ratify(
        entity     => $entity,
        capability => $opts->{ capability },
        groupname  => $opts->{ groupname }
    );

    $c->stash->{ zapi } = [
        $c->model('Object')->save(object => $c->stash->{ object })
    ];
}

=head2 revoke

Revoke a specific authorization that has been put on the referenced object.

Although the colloquial meaning of 'revoking' usually means the security
identity can no longer act on the capability, in this context it means that
either a permission or a proscription is removed from the ACL entries. The
verdict made earlier is being overturned as it where, whether that was to
permit or proscribe.

=head3 URL

POST C</api/object/[UUID]/revoke>

=head3 Parameters

=over 4

=item entity

This argument encodes a soft reference to the entity for which the
authorization is being made. This can be a subject, a position, or something
else entirely.

=item capability

This argument references the 'action' on the object for which a verdict is
being ratified. It may be unspecified to indicate the 'wildcard' capability.

=item verdict

This argument encodes the verdict of the ratification, which can be either
C<permit> or C<proscribe>. A permission is an explicit rule that allows the
referenced security identity to perform the capability. A proscription is an
explicit rule that denies the referenced security identity the ability to
peform the capability.

Proscriptions always overrule permissions. That means that as long as an
entity has a proscription rule that matches the capability, it cannot act on
that capability, even if a permission for it exists.

This argument may be unspecified to indicate a 'wildcard' verdict.

=back

=cut

define_profile revoke => (
    required => {
        entity_id => 'Str',
        entity_type => 'Str'
    },
    optional => {
        capability => 'Str',
        groupname => 'Str'
    }
);

sub revoke : Chained('object_base') : PathPart('revoke') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;

    unless ($c->stash->{ object }->does('Zaaksysteem::Object::Roles::Security')) {
        throw(
            'object/security',
            'Unable to revoke from an instance of an object type that does not implement a security scheme'
        );
    }

    my $opts = assert_profile($c->req->params)->valid;

    # Fixup frontend quirk, role/group/position are indistinguishable by type
    if ($opts->{ entity_type } eq 'position') {
        my ($ou_id, $role_id) = split m[\|], $opts->{ entity_id };

        unless ($ou_id) {
            $opts->{ entity } = Zaaksysteem::Object::SecurityIdentity::Role->new(
                role_id => $role_id
            );
        }

        unless ($role_id) {
            $opts->{ entity } = Zaaksysteem::Object::SecurityIdentity::Group->new(
                ou_id => $ou_id
            );
        }
    }

    unless ($opts->{ entity }) {
        # Instantiate a data-wrapper style security identity, deleting non-existing
        # rules is a NOP, which we are fine with.
        $opts->{ entity } = Zaaksysteem::Object::SecurityIdentity->new(
            entity_id => delete $opts->{ entity_id },
            entity_type => delete $opts->{ entity_type }
        );
    }

    $c->stash->{ object }->revoke(%{ $opts });

    $c->stash->{ zapi } = [
        $c->model('Object')->save(object => $c->stash->{ object })
    ];
}

=head2 action

Execute some capability on an object. This controller exists as a pass-thru
for objects that extend the basic API with their own actions.

Because this action exists as a generic pass-thru, at this point we don't
know how to use the object-type specific interfaces. Said documentation will
be available in the POD for the type being accessed.

=head3 URL

C</api/object/[UUID]/action/[action_name]>

=head3 Exceptions

=over 4

=item C<object/action>

This exception is thrown when the object referenced claims not to support
the specified action.

=back

=cut

sub action : Chained('object_base') : PathPart('action') : Args(1) : ZAPI {
    my ($self, $c, $action) = @_;

    unless(grep { $_ eq $action } $c->stash->{ object }->capabilities) {
        throw('object/action', sprintf(
            'Object "%s" does not appear to have the "%s" capability',
            $c->stash->{ object },
            $action
        ));
    }

    $c->stash->{ zapi } = $c->stash->{ object }->$action($c);
}

=head2 save

Save an L<Object|Zaaksysteem::Object> in Zaaksysteem. This will create a new
database object if none exists, or update the existing one if it does.

=head3 URL

POST C</api/object/save>

=cut

define_profile save => (
    required => {
        # Object input can be HashRef or scalar, depending if called from
        # the frontend or from testsuite respectively.
        object => union([qw[Str HashRef]]),
    }
);

sub save : Chained('/api/base') : PathPart('object/save') : Args(0) : POST : ZAPI {
    my ($self, $c) = @_;

    # This looks like an unused variable, but *this* line makes sure
    # assert_profile does not somehow flatten bools to stringified ints in
    # nested objects... fml.
    my $gobject = $c->req->params->{ object };

    my $args           = assert_profile($c->req->params)->valid;
    my $object         = $args->{object};

    if(ref $object eq 'HASH') {
        $object = encode_json($object);
    }

    $c->stash->{zapi} = [
        $c->model('Object')->save(json => $object)
    ];
}

=head2 export_single

Export a single object in an export file.

=cut

sub export_single : Chained('object_base') : PathPart('export') : Args(0) : POST {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('view_sensitive_data');

    my $filename = sprintf(
        "zs-%s-%s-%s.tar",
        $c->stash->{object}->type,
        $c->stash->{object}->id,
        $c->stash->{object}->date_modified->iso8601,
    );

    $c->res->content_type('application/x-tar');
    $c->res->header('Content-Disposition', qq[attachment; filename="$filename"]);

    my $handle = Zaaksysteem::Tie::CallingHandle->create(
        write_cb => sub { $c->res->write($_[0]); },
    );

    my $tar = Archive::Tar::Stream->new(outfh => $handle);

    $c->model('Object')->export_single(
        tar_handle => $tar,
        object     => $c->stash->{object},
        metadata   => {
            zs_version  => $c->config->{ZS_VERSION},
            environment => $c->config->{gemeente_id},
            user        => $c->user->username,
        },
    );

    $tar->FinishTar();

    # Streaming downloads don't mix well with views.
    $c->res->body('');
}

=head2 export_multiple

Export a resultset of objects in an export file.

=cut

sub export_multiple : Chained('search_base') : PathPart('export') : Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('view_sensitive_data');

    my $filename = sprintf(
        "zs-%s-%s.tar",
        DateTime->now()->iso8601,
        substr(sha1_hex($c->stash->{zql}), 0, 6),
    );

    $c->res->content_type('application/x-tar');
    $c->res->header('Content-Disposition', qq[attachment; filename="$filename"]);

    my $handle = Zaaksysteem::Tie::CallingHandle->create(
        write_cb => sub { $c->res->write($_[0]); },
    );

    my $tar = Archive::Tar::Stream->new(outfh => $handle);

    $c->model('Object')->export_multiple(
        tar_handle => $tar,
        resultset  => $c->stash->{search_rs},
        metadata   => {
            zs_version  => $c->config->{ZS_VERSION},
            environment => $c->config->{gemeente_id},
            user        => $c->user->username,
            query       => $c->stash->{zql_string},
        },
    );

    $tar->FinishTar();

    # Streaming downloads don't mix well with views.
    $c->res->body('');
}

=head1 PRIVATE ACTIONS

=cut

sub _search_intake {
    my ($self, $c, $zql, $resultset) = @_;

    return $resultset unless ($zql->cmd->can('is_intake') && $zql->cmd->is_intake);

    throw('api/object/keyword_intake_not_allowed', sprintf(
        '"intake" keyword only allowed for object_type: "case"',
    )) if $zql->cmd->from->value ne 'case';

    my @seeers = ();
    my $mine = { 'me.status' => [ 'new' ] };
    $mine->{'me.behandelaar_gm_id'} = $c->user->uidnumber;
    push @seeers, $mine;

    my $ou_id = $c->user->primary_group->id;

    my @role_ids = map { $_->id } @{ $c->user->roles };

    push @seeers, {
        '-and' => [
            { '-and' => [
                    { '-or'       => { route_role => \@role_ids } },
                    { '-or'       => [
                            { 'route_ou'  => $ou_id },
                            { 'route_ou'  => undef },
                        ],
                    }
                ],
            },
            { 'behandelaar' => undef },
        ],
    };

    # See if we got a divver, special, [s]he can see all zaken without a complete role.
    if ($c->user->has_legacy_permission('zaak_route_default')) {
        push @seeers, { 'me.route_role' => undef };
    };

    my $where = {'-or' => \@seeers };
    $where->{'me.deleted'} = undef;

    # Don't show cases that were registered with a registration date in the future
    $where->{'me.registratiedatum'} = { '<' => \'NOW()' };

    # XXX Omdat zaak_intake alleen voor status new zaken geld
    $where->{'me.status'} = 'new';

    return $resultset->search_rs(
        {
            'me.object_class' => 'case',
            'me.object_id' => { -in => $c->model('DB::Zaak')->search($where)->get_column('id')->as_query }
        }
    );
}

=head2 search_select

This action handles ZQL queries that C<SELECT> data. It is invoked by
L</search>, and should not be detached to on a whim.

=cut

sub search_select : Private {
    my ($self, $c) = @_;

    my $zql = $c->stash->{ zql };

    ### Make sure intake is special
    $c->stash->{ search_rs } = $self->_search_intake($c, $zql, $c->stash->{ search_rs });

    my $rs = $zql->apply_to_resultset($c->stash->{ search_rs });

    if($zql->cmd->from->value eq 'case') {
        my $schema = $c->model('DB')->schema;
        my $rs     = $schema->resultset('BibliotheekKenmerken');
        my $search = "@> '{\"sensitive_field\":\"on\"}'::jsonb";
        $rs = $rs->search_rs({ deleted => undef, 'properties::jsonb' => \"$search" }, { select => [qw(id magic_string)]});
        my %hide_mapping;

        my $prefix = 'attribute.';
        while (my $found = $rs->next) {
            $hide_mapping{$prefix . $found->magic_string} = $found;
        }

        # Also hide keys found in zs::attributes
        my @attrs = grep { $_->is_sensitive } Zaaksysteem::Attributes->predefined_case_attributes();
        foreach (@attrs) {
            $hide_mapping{$_->name} = $_;
        }
        $c->stash->{zapi_hide_mappings} = \%hide_mapping;
    }
    else {
        # Deref model so the closure doesn't capture $c
        my $model = $c->model('Object');

        $c->stash->{ zapi } = Zaaksysteem::Object::Iterator->new(
            rs => $rs,
            inflator => sub { $model->inflate_from_row(shift) }
        );

        return;
    }

    if($zql->cmd->distinct) {
        my $opts = $zql->cmd->dbixify_opts;

        my @results;

        my $tr = sub { my $val = shift; $val =~ s/\$/./g; return $val };

        for my $object ($rs->all) {
            push @results, {
                count => int($object->get_column('count')),
                map { $tr->($_) => $object->get_column($_) } @{ $opts->{ as } }
            };
        }

        $rs = \@results;
    }

    $c->stash->{ zapi } = $rs;

    if (grep { exists $_->{order_by} } @{ $zql->cmd->opts }) {
        $c->req->mangle_params({
            zapi_order_by           => undef,
            zapi_order_by_direction => undef,
        });
    }
}

=head2 search_count

This action handles ZQL queries that C<COUNT> data. It is invoked by
L</search>, and should not be detached to on a whim.

=cut

sub search_count : Private {
    my ($self, $c) = @_;

    my $zql = $c->stash->{ zql };

    $c->stash->{ zapi } = [{
        count => $c->stash->{ search_rs }->count
    }];
}

=head2 begin

Disable the object API completely if the environment variable
C<DISABLE_OBJECT_API> is set.

=cut

sub begin : Private {
    my ($self, $c) = @_;

    $c->forward('/page/begin');

    if ($ENV{DISABLE_OBJECT_API}) {
        $c->error('Object API is disabled');

        return 0;
    }

    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
