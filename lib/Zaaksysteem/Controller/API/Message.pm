package Zaaksysteem::Controller::API::Message;

use Moose;
use namespace::autoclean;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::Message - Frontend library for Zaaksysteem messages.

=cut

=head2 create

Create a new file.

=head3 Arguments

=over

=item case_id [required]

=item message [required]

=back

=head3 Location

POST: /message/create

=head3 Returns

A JSON hash of the created object.

=cut

define_profile create => (
    required => [qw[message case_id]],
    optional => [qw[feedback_email]]
);

sub create
    : Chained('/')
    : PathPart('api/message/create')
{

    my ($self, $c) = @_;

    my $params = assert_profile($c->req->params)->valid;
    my ($case) = $c->model('DB::Zaak')->find($params->{case_id});

    my $betrokkene_identifier;
    if ($case->behandelaar) {
        $betrokkene_identifier = $case->behandelaar->betrokkene_identifier;
    }

    my $message = $c->model('DB::Message')->message_create({
        message     => $params->{message},
        case_id     => $params->{case_id},
        event_type  => 'case/pip/feedback',
        subject_id  => $betrokkene_identifier,
    });

    # Send a copy to the aanvrager if requested.
    if ($params->{feedback_email}) {
        my ($template) = $c->model('DB::BibliotheekNotificaties')->search({
            id => $c->model('DB::Config')->get('feedback_email_template_id'),
        });

        # Make sure magic string conversion knows which case is involved.
        $c->stash->{zaak} = $case;
        my $magic_strings = {
            pip_feedback_mail => $params->{message},
        };

        my $recipient = $case->notification_recipient({recipient_type => 'aanvrager'});

        if ($template && $recipient) {

            $c->log->debug(sprintf(
                "Sending email from %s->%s",
                __PACKAGE__,
                'create',
            ));

            $case->mailer->send_from_case({
                recipient      => $recipient,
                subject        => $template->subject,
                body           => $template->message,
                additional_ztt_context => $magic_strings,
                request_id     => $c->stash->{request_id},
            });
        }
    };

    $c->stash->{json} = $message;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 get_for_user

Gets all messages for this user

=head3 Arguments

=over

=item page [optional]

=item rows [optional]

=back

=head3 Location

POST: /message/get_for_user?page=1&rows=5

=head3 Returns

JSON list of message object hashes.

=cut

sub get_for_user
    : Chained('/')
    : PathPart('api/message/get_for_user')
    : JSON
{
    my ($self, $c) = @_;

    my $include_archived = $c->req->param('include_archived') ? 1 : 0;

    my $rs = $c->model('DB::Message')->search(
        {
            $include_archived ? () : (is_archived => 0),
            subject_id => $c->user->betrokkene_identifier,
        },
        {
            page => $c->req->param('page') || 1,
            rows => $c->req->param('rows') || 10,
            order_by   => { -desc => 'logging.created' },
        },
    )->with_related_json;

    my $pager = $rs->pager;
    if($pager->next_page) {
        $c->stash->{next_url} = $c->req->uri_with({
            before_id => undef,
            since_id => undef,
            page => $pager->next_page
        });
    }

    if($pager->previous_page) {
        $c->stash->{prev_url} = $c->req->uri_with({
            before_id => undef,
            since_id => undef,
            page => $pager->previous_page
        });
    }

    $c->stash->{json} = $rs;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 _get_messages

Parse the "messages" parameter, for use by several controllers below.

=cut

define_profile _get_messages => (
    required => {
        messages => 'Int',
    },
);

sub _get_messages {
    my $c = shift;

    my $params = assert_profile($c->req->params)->valid;

    return $params->{messages};
}

=head2 mark_read

Mark the message as being read for the current user.
If the ID is not assigned to the user, an error is thrown.

=head3 URL

    POST /api/message/mark_read

=cut

sub mark_read : Chained('/') : PathPart('api/message/mark_read') : Args(0) : Method('POST') : JSON {
    my ($self, $c) = @_;

    my $msg = _get_messages($c);

    my $rs = $c->model('DB::Message')->search(
        {
            id         => $msg,
            subject_id => $c->user->betrokkene_identifier,
        },
    );

    $rs->update({ is_read => 1 });
    $c->stash->{json} = $rs;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 mark_all_read

Archive all notifications for the current user.

=head3 URL

    POST /api/message/mark_read/all

=cut

sub mark_all_read : Chained('/') : PathPart('api/message/mark_read/all') : Args(0) : Method('POST') : JSON {
    my ($self, $c) = @_;

    my $rs = $c->model('DB::Message')->search(
        {
            subject_id => $c->user->betrokkene_identifier,
            is_read    => 0,
        },
    );

    my $rv = $rs->update({ is_read => 1 });

    $c->stash->{json} = {
        success         => 1,
        messages_marked => int($rv),
    };

    $c->forward('Zaaksysteem::View::JSON');
}

=head2 archive

Archive a specific notification for the current user.

=head3 URL

    POST /api/message/archive

=cut

sub archive : Chained('/') : PathPart('api/message/archive') : Args(0) : Method('POST') : JSON {
    my ($self, $c) = @_;

    my $msg = _get_messages($c);

    my $rs = $c->model('DB::Message')->search(
        {
            id         => $msg,
            subject_id => $c->user->betrokkene_identifier,
        },
    );

    $rs->update({ is_archived => 1 });
    $c->stash->{json} = $rs;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 unarchive

Un-archive a specific notification for the current user.

=head3 URL

    POST /api/message/unarchive

=cut

sub unarchive : Chained('/') : PathPart('api/message/unarchive') : Args(0) : Method('POST') : JSON {
    my ($self, $c) = @_;

    my $msg = _get_messages($c);

    my $rs = $c->model('DB::Message')->search(
        {
            id         => $msg,
            subject_id => $c->user->betrokkene_identifier,
        },
    );

    $rs->update({ is_archived => 0 });
    $c->stash->{json} = $rs;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 archive_all

Archive all notifications for the current user.

=head3 URL

    POST /api/message/archive/all

=cut

sub archive_all : Chained('/') : PathPart('api/message/archive/all') : Args(0) : Method('POST') : JSON {
    my ($self, $c) = @_;

    my $rs = $c->model('DB::Message')->search(
        {
            subject_id  => $c->user->betrokkene_identifier,
            is_archived => 0,
        },
    );

    my $rv = $rs->update({ is_archived => 1 });

    $c->stash->{json} = {
        success         => 1,
        messages_marked => int($rv),
    };

    $c->forward('Zaaksysteem::View::JSON');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
