package Zaaksysteem::Controller::Schedule;

use Moose;

use NetAddr::IP;

use BTTW::Tools;
use JSON;
use DateTime;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Schedule

=head1 SYNOPSIS

=head1 DESCRIPTION

Zaaksysteem controller for handling scheduled processes.

=head1 INSTRUCTIONS

=head1 METHODS

=head2 /schedule/run

Runs all jobs that are available on this domain/instance.

=cut

sub run
    : Chained('/')
    : PathPart('schedule/run')
    : ZAPI
    : DisableACL
{
    my ($self, $c) = @_;

    $c->assert_platform_access();

    $self->_execute_scheduled_run(
        "Transacties",
        sub {
            $c->model('DB::Transaction')->process_pending();
        },
    );

    $self->_execute_scheduled_run(
        "Onafgeronde zaken",
        sub {
            $self->remove_onafgeronde_zaken($c);
        },
    );

    $self->_execute_scheduled_run(
        "Opgeschorte zaken",
        sub {
            $c->model('Zaken')->resume_temporarily_stalled_cases;
        },
    );

    $self->_execute_scheduled_run(
        "DefaultScheduledJobs",
        sub {
            $c->model('Scheduler')->install_default_jobs;
        }
    );

    $self->_execute_scheduled_run(
        "Scheduler",
        sub {
            $c->model('Scheduler')->run_pending();
        },
    );

    $self->_execute_scheduled_run(
        "Buitenbeter",
        sub {
            $self->check_buitenbeter_meldingen($c);
        },
    );

    $self->_execute_scheduled_run(
        "Incoming email",
        sub {
            my @interfaces = $c->model('DB::Interface')->search_active(
                { module => 'pop3client' }
            )->all;

            for my $interface (@interfaces) {
                my $model = $interface->model;

                $model->login();
                my $messages = $interface->model->messages();

                while (my $message = $messages->()) {
                    $c->stash->{message} = $message->retrieve()
                        or next;

                    my $success = $c->forward('/api/mail/intake');

                    $message->delete() if $success;
                }

                $model->close();
            }
        }
    );

    $self->_execute_scheduled_run('Queue - Clean up successfully processed items' => sub {
        my $queue_gc_days = $c->config->{Queue}{maximum_age_days} // 120;
        my $max_rows = $c->config->{Queue}{maximum_rows};

        $c->model('Queue', DisableObjectModel => 1)->gc(
            DateTime->now()->subtract(days => $queue_gc_days),
            $max_rows
        );
    });

    $self->_execute_scheduled_run(SessionInvitations => sub {
        $c->model('Session::Invitation')->cleanup_expired_invitations;
    });

    $self->_execute_scheduled_run('Cleanup locked files', sub {
        $c->model('DB::File')->cleanup_file_locks(DateTime->now);
    });

    $self->_execute_scheduled_run('Repush pending virus scans', sub {
        $c->model('DB::Filestore')->repush_pending_file_scan_queue_items();
    });

    $self->_execute_scheduled_run('Cleanup old transactions', sub {
        $c->model('Transaction')->cleanup_old_transactions();
    });

    $c->stash->{zapi} = [{
        result => 'success',
    }];

}

=head2 remove_onafgeronde_zaken

Cleanup for the L<Zaaksysteem::Schema::ZaakOnafgerond> table.

=cut

sub remove_onafgeronde_zaken {
    my ($self, $c) = @_;

    my $time = DateTime->now->subtract(days => 30)->epoch;

    my $rs = $c->model('DB::ZaakOnafgerond')
        ->search({ create_unixtime => { '<=' => $time } });

    my @ids;
    while (my $concept = $rs->next) {
        my $properties = JSON->new->decode($concept->json_string);
        my $upload = $properties->{uploads} // {};
        foreach my $u (values %$upload) {
            my $ref = ref $u;
            if ($ref eq 'ARRAY') {
                foreach (@$u) {
                    push(@ids, $_->{filestore_id}) if $_->{filestore_id};
                }
            }
            elsif ($ref eq 'HASH') {
                push(@ids, $u->{filestore_id}) if $u->{filestore_id};
            }
            elsif ($ref) {
                throw(
                    "scheduled_run/concept_case/upload_hash/invalid_object",
                    "Invalid object ($ref) for uploads in concept cases"
                );
            }
            else {
                $self->log->debug("No uploads found");
            }
        }
    }

    if (@ids) {
        $self->log->debug(
            sprintf(
                "Deleting filestore objects %s from outdated concept cases",
                dump_terse(\@ids)));
        $c->model('DB::Filestore')->search({ id => \@ids })->delete;
    }

    $rs->delete;
}

=head2 check_buitenbeter_meldingen

Attempts to check if the C<buitenbeter> sysin interface has new messages
pending.

=cut

sub check_buitenbeter_meldingen {
    my ($self, $c) = @_;

    my $interface = $c->model('DB::Interface')->search_active({
        module => 'buitenbeter'
    })->first;

    if (!$interface) {
        $self->log->trace("Buitenbeter koppeling is not active, skipping run");
        return;
    }

    my $cases = $interface->process_trigger('GetNewMeldingen', {}, {
        zaak => $c->model('Zaak')
    });
}

sub _execute_scheduled_run {
    my ($self, $job, $sub) = @_;

    try {
        $self->log->debug("Running job $job");
        $sub->();
    } catch {
        $self->log->error("Unable to process Scheduled run $job: $_");
    };

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
