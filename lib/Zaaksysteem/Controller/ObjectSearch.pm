package Zaaksysteem::Controller::ObjectSearch;
use Moose;
use utf8;

BEGIN { extends 'Zaaksysteem::Controller' }

use HTML::TreeBuilder;
use Moose::Util::TypeConstraints qw[union];

use Zaaksysteem::Backend::Subject::Naamgebruik qw/naamgebruik/;
use Zaaksysteem::Constants qw/
    OBJECTSEARCH_TABLENAMES
    OBJECTSEARCH_TABLE_ORDER
    ZAAKSYSTEEM_OPTIONS
    STATUS_LABELS
/;
use Zaaksysteem::Object::Constants qw[
    SEARCH_OBJECT_TYPE_BLACKLIST
];

use BTTW::Tools;
use Zaaksysteem::Types qw[UUID];

=head1 NAME

Zaaksysteem::Controller::ObjectSearch

=head1 SUMMARY

This controller defines several actions that can be used to search for specific
objects within Zaaksysteem. What you should imagine is meant by that is things
like employees, addresses, contacts etc.

These actions are mainly used by the autocomplete fields you find throughout
the Zaaksysteem frontend (Spot-enlighter anyone?)

=head1 ACTIONS

=cut

define_profile objectsearch_base => (
    required => [],
    require_some => {
        query_or_all => [ 1, qw[query all] ],
    },
    typed => {
        query => 'Str'
    }
);

=head2 objectsearch_base

This action implements a base for all objectsearch actions. It does nought but
verify a C<query> parameter is present/valid and defines the main URI path
for this domain (C<objectsearch>).

=head3 Parameters

=over 4

=item query

This is the main interface to ObjectSearch queries, every action based on this
expects a string query to be present. It is stored in the C<< $c->stash >> hash.

=item all

This parameter overrides the default check that query is filled with data,
allowing subcontrollers to return all items in a collection as a search result.

=back

=cut

sub objectsearch_base : Chained('/') : PathPart('objectsearch') : CaptureArgs(0) {
    my ($self, $c) = @_;

    # return unless($c->req->is_xhr);

    my $params = assert_profile($c->req->params)->valid;

    $c->stash->{ query } = $params->{ query };
    $c->stash->{ show_all } = exists $params->{ all };
}

=head2 objectsearch_contact_base

This actions implements a base for all contact-related object search actions.
It merely defines the URI path to be that of the C<objectsearch_base> base
action with C<contact> slapped after it.

=cut

sub objectsearch_contact_base : Chained('objectsearch_base') : PathPart('contact') : CaptureArgs(0) {
    my ($self, $c) = @_;
}

=head2 objectsearch

=head3 URL

C</objectsearch>

=head3 Parameters

=over 4

=item object_type

=item rows

=item trigger

=item betrokkene_type

=back

=cut

define_profile objectsearch => (
    optional => [qw[object_type rows trigger betrokkene_type active zapi_num_rows]],
    typed => {
        object_type => 'Str',
        rows => 'Int',
        zapi_num_rows => 'Int',
        trigger => 'Str',
        betrokkene_type => 'Str',
        active => 'Bool'
    }
);

sub objectsearch : Chained('objectsearch_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    my $object_type = $opts->{ object_type };

    my $results;

    if($object_type) {
        my $extra_params = {};

        if($object_type eq 'zaaktype') {
            $extra_params->{ betrokkene_type } = [];

            if (exists $opts->{trigger}) {
                $extra_params->{ trigger } = $opts->{ trigger };

                if ($opts->{ trigger } =~ /intern/) {
                    push @{ $extra_params->{ betrokkene_type } }, 'medewerker';
                }
            }

            if(exists($opts->{ betrokkene_type })) {
                if($opts->{ betrokkene_type } ne 'natuurlijk_persoon') {
                    push @{ $extra_params->{ betrokkene_type } }, 'niet_natuurlijk_persoon';
                } else {
                    push @{ $extra_params->{ betrokkene_type } }, 'natuurlijk_persoon';
                }
            }
        }

        if($opts->{ rows }) {
            $extra_params->{ rows } = $opts->{ rows };
        } elsif($opts->{ zapi_num_rows }) {
            $extra_params->{ rows } = $opts->{ zapi_num_rows };
        }

        if ($opts->{active}) {
            $extra_params->{active} = $opts->{active};
        }

        $results = $self->_search_searchable_object_type({
            c            => $c,
            query        => $c->stash->{ query },
            object_type  => $object_type,
            extra_params => $extra_params,
        });
    } else {
        $results = $self->_search_searchable({
            c       => $c,
            query   => $c->stash->{ query },
        });
    }

    my @entries;

    if($results) {
        foreach my $result (@$results) {
            if($result->{ is_object_type_instance }) {
                push @entries, $result;
                next;
            }

            next unless $result->{ searchable_id };

            my $object = $self->_retrieve_object({
                c               => $c,
                object_type     => $result->{ object_type },
                searchable_id   => $result->{ searchable_id }
            });

            next unless $object;

            push @entries, {
                id          => $result->{ searchable_id },
                object_type => $result->{ object_type },
                label       => $result->{ search_term },
                object      => $object,
            };
        }
    }

    if ($object_type) {
        my $elapsed = $c->stats->elapsed*1000 + ($c->config->{performance_per_action_correction} || 0);
        Zaaksysteem::StatsD->statsd->timing(
            sprintf("performance_objectsearch_objectsearch_%s", $object_type),
            $elapsed
        );
    }

    $c->stash->{json} = { entries => \@entries };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 case_results

=head3 URL

C</objectsearch/case/results>

=cut

sub case_results : Chained('objectsearch_base') : PathPart('case/results') {
    my ($self, $c) = @_;

    my $types = ZAAKSYSTEEM_OPTIONS->{ RESULTAATTYPEN };

    my @entries;
    my $query = $c->stash->{ query };

    for my $type (grep { $_ =~ m[$query]i } @{ $types }) {
        warn $type;

        push @entries, {
            id => $type,
            label => ucfirst($type),
            object_type => 'case-result',
            object => {}
        };
    }

    $c->stash->{ json } = { entries => \@entries };
    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 attributes

This action implements an autocomplete backend for system/db/whatever
attributes. At this point it will freeform query the
L<Zaaksysteem::Schema::BibliotheekKenmerken> table, followed by an
iteration over all attributes defined in L<Zaaksysteem::Attributes>.

=head3 URL

C</objectsearch/attributes>

=head3 Parameters

=over 4

=item exclude_system_attributes

This parameter, when set to a true-ish value, will exclude the list of
'systeemkenmerken' from being searched.

=back

=head3 Output

The returned objects look something like this:

    {
        "id" : 463,
        "label" : "Groot tekstveld voorinvulling test",
        "object_type" : "attribute",
        "object" : {
            "value_type" : "textarea",
            "source" : "db-attr",
            "column_name" : "groot_tekstveld_voorinvulling_test"
        }
    },
    .
    .
    .

=cut

sub attributes : Chained('objectsearch_base') : PathPart('attributes') {
    my ($self, $c) = @_;

    my @entries = $c->model('Attributes')->search($c->stash->{ query }, %{ $c->req->params });

    $c->stash->{ json } = {
        entries => [ sort { uc($a->{ label }) cmp uc($b->{ label }) } @entries ]
    };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 objecttype_attributes

This action implements an autocomplete backend for attributes associated with
a specific L<Zaaksysteem::Object::Types::Type> instance.

=head3 URL

C</objectsearch/objecttype_attributes>

=head3 Paramters

=over 4

=item uuid

The UUID for the objecttype B<instance> for which you want to autocomplete
attributes

=back

=head3 Output

The returned objects look something like this:

    {
        "id" : "attribute.groot_tekstveld_voorinvulling_test",
        "label" : "Groot tekstveld voorinvulling test",
        "object_type" : "attribute",
        "object" : {
            "value_type" : "textarea",
            "source" : "db-attr",
            "column_name" : "attribute.groot_tekstveld_voorinvulling_test"
        }
    },
    .
    .
    .

=cut

sub objecttype_attributes : Chained('objectsearch_base') : PathPart('objecttype_attributes') {
    my ($self, $c) = @_;

    my @entries;
    my $query = quotemeta $c->stash->{ query };

    if ($c->req->params->{ uuid }) {
        try {
            my $type = $c->model('Object')->retrieve(
                uuid => $c->req->params->{ uuid }
            );

            return unless $type->isa('Zaaksysteem::Object::Types::Type');

            for my $attribute ($type->instance_attributes) {
                next unless $attribute->{ attribute_label } =~ m[$query]i;

                push @entries, {
                    id => $attribute->{ name },
                    label => $attribute->{ attribute_label },
                    object_type => 'attribute',
                    object => {
                        source => 'db-attr',
                        searchable_object_label => $attribute->{ attribute_label },
                        value_type => $attribute->{ attribute_type },
                        column_name => $attribute->{ name }
                    }
                }
            }
        } catch {
            $c->log->warn($_);
        };
    }

    $c->stash->{ json } = { entries => \@entries };
    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 objects

This action implements an autocomplete backend for objects. The provided query
will be applied as a ZQL text-vector search, optionally with a defined
object-type.

Because of the expensiveness of hydrating full objects in this fashion, no
more than B<20> results will be returned.

=head3 URL

C</objectsearch/objects>

=head3 Parameters

This action supports free-form queries on all object types, or specific types.

=over 4

=item object_type

If set, this action will only hydrate objects of the supplied type.

=back

=head3 Output

The returned objects look something like this:

    {
        "id": "bcabc1df-f9e0-4a8b-9d75-11e7d8fbf5cd",
        "label": "type(...fb5cd)",
        "object_type": "object",
        "object": {
            ...
        }
    },
    .
    .
    .

=cut

sub objects : Chained('objectsearch_base') : PathPart('objects') {
    my ($self, $c) = @_;

    my $rs = $c->model('Object')->rs->search_text_vector($c->stash->{ query } || '')->search(undef, { rows => 20 });

    if($c->req->param('object_type')) {
        $rs = $rs->search({ object_class => $c->req->param('object_type') });
    }

    $c->stash->{ json } = {
        entries => [
            map { {
                id => $_->id,
                label => "$_",
                object_type => 'object',
                object => $_
            } } $c->model('Object')->inflate_from_rs($rs)
        ]
    };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 bag_search (addresses)

=head3 URL

C</objectsearch/bag>

=cut

sub bag_search : Chained('objectsearch_base') : PathPart('bag') {
    my($self, $c) = @_;

    my $query = $c->stash->{ query };

    my $result = $c->model('BAG')->search(
        type  => 'nummeraanduiding',
        query => $query,
    );

    $c->stash->{json} = {
        entries => $result,
    };
    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 bag_search_street (streets)

=head3 URL

C</objectsearch/bag-street>

=cut

sub bag_search_street : Chained('objectsearch_base') : PathPart('bag-street') {
    my ($self, $c) = @_;

    my $query = $c->stash->{ query };

    my $result = $c->model('BAG')->search(
        type  => 'openbareruimte',
        query => $query,
    );

    $c->stash->{json} = {
        entries => $result,
    };
    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 bag_search_mixed (addresses + streets)

=head3 URL

C</objectsearch/bag-mixed>

=cut

sub bag_search_mixed : Chained('objectsearch_base') : PathPart('bag-mixed') {
    my ($self, $c) = @_;

    my $query = $c->stash->{ query };

    my $result = $c->model('BAG')->search(
        type  => ['nummeraanduiding', 'openbareruimte'],
        query => $query,
    );

    $c->stash->{json} = {
        entries => $result,
    };
    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 casetype_document

=head3 URL

C</objectsearch/case_type_document>

=cut

sub casetype_document : Chained('objectsearch_base') : PathPart('case_type_document') {
    my ($self, $c) = @_;

    my $entries = $self->_search_casetype_document({
        c       => $c,
        query   => $c->stash->{ query }
    });

    $c->stash->{json} = {
        entries => $entries,
    };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 template

Search for templates ("sjablonen")

=head3 URL

C</objectsearch/template>

=cut

sub template : Chained('objectsearch_base') : PathPart('template') {
    my ($self, $c) = @_;

    my @entries = map { $_->{label} = $_->{naam}; $_ } @{
        $self->_search_searchable_object_type(
            {
                c => $c,
                query => $c->stash->{ query },
                object_type => 'bibliotheek_sjablonen',
            }
        )
    };

    $c->stash->{json} = { entries => \@entries };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 notification

Search for notifications ("notificaties", "email-templates")

=head3 URL

C</objectsearch/notification>

=cut

sub notification : Chained('objectsearch_base') : PathPart('notification') {
    my ($self, $c) = @_;

    my $entries = $self->_search_searchable_object_type(
        {
            c           => $c,
            query       => $c->stash->{query},
            object_type => 'bibliotheek_notificaties',
        }
    );

    $c->stash->{json} = { entries => $entries };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 natuurlijk_persoon

=head3 URL

C</objectsearch/contact/natuurlijk_persoon>

=cut

sub natuurlijk_persoon : Chained('objectsearch_contact_base') : PathPart('natuurlijk_persoon') {
    my ($self, $c) = @_;

    $c->stash->{ json } = {
        entries => $self->_search_natuurlijk_persoon({
            c => $c,
            query => $c->stash->{ query }
        })
    };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 bedrijf

=head3 URL

C</objectsearch/contact/bedrijf>

=cut

sub bedrijf : Chained('objectsearch_contact_base') : PathPart('bedrijf') {
    my ($self, $c) = @_;

    my $entries = $self->_search_bedrijf({
        c       => $c,
        query   => $c->stash->{ query }
    });

    $c->stash->{json} = {
        entries    => $entries,
    };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 medewerker

=head3 URL

C</objectsearch/contact/medewerker>

=cut

sub medewerker : Chained('objectsearch_contact_base') : PathPart('medewerker') {
    my ($self, $c) = @_;

    my $search_args = {
        intern => 0,
        type => 'medewerker',
        rows_per_page => '',
    };

    my $inactive = $c->req->param('inactive') ? 1 : 0;
    my $active   = $c->req->param('active') ? 1 : 0;
    my @exclude  = $c->req->param('include_admin') ? () : 'admin';

    my $entries = $c->model('Betrokkene')->search($search_args, {
        freetext => $c->stash->{ query },
        inactive_search => $inactive,
        active_search   => $active,
    });

    my @hashresults;

    my %exclude = map { $_ => 1 } @exclude;

    if($entries) {
        my ($behandelaar) = grep
            { $_->system_role && $_->name eq 'Behandelaar' }
            @{ $c->model('DB::Roles')->get_all_cached($c->stash) };

        while(my $entry = $entries->next()) {

            # Skip entries when they are excluded or when they are still
            # in the "Inbox" at gebruikersbeheer.
            if ($exclude{$entry->username}) {
                next;
            }
            elsif (!$entry->org_eenheid) {
                next;
            }

            push @hashresults, {
                id          => $entry->ex_id,
                object_type => 'medewerker',
                label       => $entry->naam,
                object      => {
                    object_type => 'medewerker',
                    email       => $entry->email,
                    id          => $entry->ex_id,
                    naam        => $entry->naam,
                    route       => $entry->org_eenheid->id,
                    username    => $entry->username,
                    uuid        => $entry->uuid,

                    # Always return the "behandelaar" role -- this is used to
                    # set a case to the correct route+role when "Also set
                    # route" is selected, until we use api/v1 calls to search
                    # for medewerkers.
                    role        => $behandelaar->id,
                }
            };
        }
    }

    $c->stash->{json} = { entries => \@hashresults };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

sub objecttypes : Chained('objectsearch_base') : PathPart('objecttypes') {
    my ($self, $c) = @_;

    my @results;
    my @entries = (
        {
            id          => 1,
            label       => 'Zaak',
            object_type => 'case',
        },
        {
            id          => 2,
            label       => 'Geplande taak',
            object_type => 'scheduled_job'
        },
        {
            id          => 3,
            label       => 'Zaaktype',
            object_type => 'casetype'
        }
    );

    for my $type ($c->model('Object')->search('type', {})) {
        push @entries, {
            label => $type->name,
            object_type => $type->prefix,
            id => $type->id
        }
    }

    my $query = $c->req->param('query');

    if ($query) {
        my $regex = qr/$query/i;

        @results = grep {
            $_->{ object_type } =~ $regex || $_->{ label } =~ $regex
        } @entries;
    } else {
        @results = @entries;
    }

    $c->stash->{json} = { entries => \@results };
    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head2 objecttype

Search for user-defined objecttype instances

=head3 URL

C</objectsearch/objecttype>

=head3 Response

    {
        "json": {
            "entries": [
                {
                    "id": <UUID>,
                    "label": "My label"
                },
                .
                .
                .
            ]
        }
    }

=cut

sub objecttype : Chained('objectsearch_base') : PathPart('objecttype') {
    my ($self, $c) = @_;

    my @results;

    my $model = $c->model('Object');

    my $types = $model->rs->search({ object_class => 'type' });
    my $rs = $types->search_text_vector($c->req->params->{ query });

    for my $result (map { $model->inflate_from_row($_) } $rs->all ) {
        push @results, {
            label => $result->TO_STRING,
            id => $result->id
        };
    }

    $c->stash->{ json } = { entries => \@results };
    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head1 PRIVATE ACTIONS

=head2 _bag_postal

=cut

sub _bag_postal : Private {
    my ($self, $c, $postal_code, $rest) = @_;

    $postal_code =~ s/\s//;
    $postal_code = uc($postal_code);

    my $entries;

    if(defined $rest) {
        $entries = $c->model('DB::BagNummeraanduiding')->search(
            { 'me.postcode' => $postal_code },
            {
                order_by => [
                    qw[woonplaats.naam openbareruimte.naam postcode huisnummer],
                    \'huisletter NULLS FIRST',
                    \'huisnummertoevoeging NULLS FIRST'
                ],
                join => { openbareruimte => 'woonplaats' },
                rows => 20
            }
        );

        if($rest =~ m[^\d]) {
            my ($number, $letter, $suffix) = _parse_street_number($rest);

            if($number) { $entries = $entries->search({ 'me.huisnummer' => $number }); }
            if($letter) { $entries = $entries->search({ 'me.huisletter' => $letter }); }
            if($suffix) { $entries = $entries->search({ 'me.huisnummertoevoeging' => $suffix }); }
        }
    } else {
        $entries = $c->model('DB::BagOpenbareruimte')->search(
            { 'hoofdadressen.postcode' => $postal_code },
            {
                join => [qw[hoofdadressen woonplaats]],
                order_by => [
                    qw[woonplaats.naam me.naam hoofdadressen.postcode hoofdadressen.huisnummer],
                    \'hoofdadressen.huisletter NULLS FIRST',
                    \'hoofdadressen.huisnummertoevoeging NULLS FIRST'
                ],
                group_by => [qw[me.identificatie me.begindatum me.einddatum me.naam me.officieel me.woonplaats me.type me.inonderzoek me.documentdatum me.documentnummer me.status me.correctie]],
                rows => 20
            }
        );
    }

    $c->stash->{ json } = { entries => $self->_build_entry_set([$entries->all]) };

    $c->detach('Zaaksysteem::View::JSONlegacy');
}

=head1 METHODS

=head2 _search_natuurlijk_persoon

=cut

define_profile _search_natuurlijk_persoon => (
    required => [qw[c query]],
    typed => {
        c => 'Zaaksysteem',
        query => 'Str'
    }
);

sub _search_natuurlijk_persoon {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $c = $opts->{ c };
    my $query = $opts->{ query };

    my @entries;

    my $resultset;

    my $search_options = {
        rows     => 5,
        page     => 1,
        order_by => [
            { -desc => 'me.active' },
            { -asc  => 'me.search_order' },
        ],
        prefetch => 'adres_id',
    };


    if(length $query > 1 && length $query <= 3) {
        $resultset = $c->model("DB::NatuurlijkPersoon")->search(
            {
                'lower(naamgebruik)' => {
                    like => lc($query) . '%',
                }
            },
            $search_options,
        );
    } elsif(length $query > 3) {

        my $query_parts = _parse_query({
            query => $query
        });

        my @whereClause;

        foreach my $query_part (@$query_parts) {
            push @whereClause, { 'lower(search_term)' => {
                    like => sprintf('%%%s%%', lc($query_part))
            } };
        }

        $resultset = $c->model("DB::NatuurlijkPersoon")
            ->search({ -and => \@whereClause }, $search_options,);
    }

    if($resultset) {
        $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');

        while (my $result = $resultset->next) {

            my $object = $self->_retrieve_object({
                c               => $c,
                object_type     => 'natuurlijk_persoon',
                searchable_id   => $result->{searchable_id}
            });

            next unless $object;

            push @entries, {
                id          => $result->{searchable_id},
                label       => $object->{label} // $result->{search_term},
                object_type => 'natuurlijk_persoon',
                object      => $object,
            };
        }
    }

    return \@entries;
}

=head2 _search_casetype_document

=cut

define_profile _search_casetype_document => (
    required => [qw[c query]],
    typed => {
        c => 'Zaaksysteem',
        query => 'Str'
    }
);

sub _search_casetype_document {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $c = $opts->{ c };
    my $query = $opts->{ query };

    my @entries;

    my $resultset;

    if(length $query >= 1) {
        $resultset = $c->model('DB::BibliotheekKenmerken')->search({
            value_type => 'file',
            naam       => { '~*' => $query },
        });
    }

    if($resultset) {
        $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');

        while (my $result = $resultset->next) {

            my $object = $self->_retrieve_object({
                c               => $c,
                object_type     => 'bibliotheek_kenmerken',
                searchable_id   => $result->{ searchable_id }
            });

            next unless $object;

            push @entries, {
                id          => $result->{ searchable_id },
                label       => $result->{ search_term },
                object_type => 'bibliotheek_kenmerken',
                object      => $object,
            };
        }
    }

    return \@entries;
}

=head2 _search_searchable_object_type

=cut

define_profile _search_searchable_object_type => (
    required => [qw[c query object_type]],
    optional => [qw[extra_params]],
    typed => {
        c => 'Zaaksysteem',
        query => 'Str',
        object_type => 'Str',
        extra_params => 'HashRef'
    }
);

sub _search_searchable_object_type {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $c = $opts->{ c };
    my $query = $opts->{ query };
    my $object_type = $opts->{ object_type };
    my $extra_params = $opts->{ extra_params };

    my $max_items = $extra_params ? $extra_params->{ rows } : 50;

    return undef if(length $query < 1);

    my @results = $self->_build_resultset({
        object_type     => $object_type,
        query           => $query,
        c               => $c,
        extra_params    => $extra_params,
        max_items       => $max_items,
    });

    return \@results;
}

=head2 _search_searchable

=cut

define_profile _search_searchable => (
    required => [qw[c query]],
    typed => {
        c => 'Zaaksysteem',
        query => 'Str'
    }
);

sub _search_searchable {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $c = $opts->{ c };
    my $query = $opts->{ query };

    return undef if(length $query < 1);

    $self->log->trace("Got query $query");

    # we want the output ordered. order comes from the constant OBJECTSEARCH_TABLE_ORDER
    # a little perl poetry to accomplish this.
    my @grouped_results = map {
        $self->_build_resultset({
            object_type => $_,
            query => $query,
            c => $c,
        });
    } OBJECTSEARCH_TABLE_ORDER;

    push @grouped_results, $self->_build_object_resultset({
        query => $query,
        model => $c->model('Object')
    });

    return \@grouped_results;
}

=head2 _search_contacts

=cut

define_profile _search_contacts => (
    required => [qw[whereClause c limit]],
    typed => {
        c => 'Zaaksysteem',
        limit => 'Int'
    }
);

sub _search_contacts {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $whereClause = $opts->{ whereClause };
    my $c = $opts->{ c };
    my $limit = $opts->{ limit };

    my $resultset = $c->model('DB::Searchable')->search({
        -and        =>    $whereClause,
        object_type => { -in => [ qw/natuurlijk_persoon bedrijf/] },
    },
    {
        rows        => $limit,
        order_by    => 'me.search_order',
    });

    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');

    return $resultset->all;
}

=head2 _search_parts

=cut

define_profile _search_parts => (
    required => [qw[query]],
    typed => {
        query => 'Str'
    }
);

sub _search_parts {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $query = $opts->{ query };

    my $query_parts = _parse_query({
        query => $query
    });

    my @whereClause;

    foreach my $query_part (@$query_parts) {
        push @whereClause, { 'lower(me.search_term)' => {
            like => sprintf('%%%s%%', lc($query_part))
        } };
    }

    return { -and => \@whereClause };
}

=head2 _search_bedrijf

=cut

define_profile _search_bedrijf => (
    required => [qw[c query]],
    typed => {
        c => 'Zaaksysteem',
        query => 'Str'
    }
);

sub _search_bedrijf {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $c = $opts->{ c };
    my $query = $opts->{ query };

    my @entries;

    my $resultset;

    if(length $query > 1 && length $query <= 3) {
        $resultset = $c->model('DB::Bedrijf')->search(
            {
                'lower(handelsnaam)' => {
                    like => lc($query) . '%',
                },
                deleted_on => undef,
            },
            {
                rows => 5,
                page => 1,
                order_by => { -asc => 'handelsnaam' },
            }
        );

    } elsif(length $query > 3) {
        my $query_parts = _parse_query({
            query => $query
        });

        my @whereClause;

        foreach my $query_part (@$query_parts) {
            push @whereClause, {
                'lower(search_term)' => {
                    like => sprintf('%%%s%%', lc($query_part))
                },
            };
        }

        $resultset = $c->model('DB::Bedrijf')->search({ -and => \@whereClause }, {
            rows => 5,
            order_by => 'me.handelsnaam',
            page => 1,
        });
    }

    if($resultset) {
        $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');

        while (my $result = $resultset->next) {
            my $object = $self->_retrieve_object({
                c               => $c,
                object_type     => 'bedrijf',
                searchable_id   => $result->{searchable_id}
            });

            next unless $object;

            push @entries, {
                id          => $result->{searchable_id},
                label       => $result->{search_term},
                object_type => 'bedrijf',
                object      => $object,
            };
        }
    }

    return \@entries;
}

=head1 HELPER METHODS

=head2 _build_resultset

=cut

define_profile _build_resultset => (
    required => [qw[object_type query c]],
    optional => [qw[extra_params max_items]],
    typed => {
        object_type => 'Str',
        query => 'Str',
        c => 'Zaaksysteem',
        extra_params => 'HashRef'
    }
);

sub _build_resultset {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $object_type  = $params->{ object_type };
    my $query        = $params->{ query };
    my $c            = $params->{ c };
    my $extra_params = $params->{ extra_params };
    my $max_items    = $params->{ max_items } || 4;

    my $whereClause = $self->_search_parts({ query => $query });

    if($object_type eq 'contact') {
        return $self->_search_contacts({
            whereClause => $whereClause,
            c => $c,
            limit => $max_items,
        });
    }

    my $model = OBJECTSEARCH_TABLENAMES->{ $object_type }{ tablename };
    my $resultset;

    unless($model) {
        return $self->_build_object_resultset({
            model => $c->model('Object'),
            object_type => $object_type,
            query => $query,
            max_items => $max_items
        });
    }

    my $where = { -and => $whereClause };
    my $options = {
        rows        => $max_items,
        order_by    => 'me.search_order',
    };

    # don't include documents that belong to a deleted zaak
    if($object_type eq 'file') {
        $options->{join} = 'case_id';

        delete $whereClause->{ 'case_id.deleted' };
        delete $whereClause->{ 'date_deleted' };
        $where->{date_deleted} = undef;

        $options->{order_by} = { -desc => 'me.id'};

        my $querytext = BTTW::Tools::Text->new(source => $query, language => 'nl');

        my @tsquery_parts = map { sprintf('%s:*', $_) }
                                $querytext->stemmed_words;

        $where = [ $where, {
            'me.search_index' => { '@@' => join(' & ', @tsquery_parts) },
            date_deleted      => undef,
            active_version    => 1,
        } ];
    }

    if($object_type eq 'zaaktype') {
        $resultset = $c->model('DB::Zaaktype')->search_with_options({
            term            => $query,

            (scalar @{ $extra_params->{ betrokkene_type } }
                ? (betrokkene_type => $extra_params->{ betrokkene_type })
                : ()
            ),
            (exists $extra_params->{ trigger }
                ? (trigger => $extra_params->{ trigger })
                : ()
            ),
        });

        $resultset = $resultset->search({ 'me.active' => 1 }, {
            join        => 'zaaktype_node_id',
            order_by    => 'zaaktype_node_id.titel',
            rows        => 10,
        });
    }

    if($model eq 'Zaak') {
        my $object_model = $c->model('Object');

        my %args = ();
        $where = [ $where ];

        if ($params->{query} =~ /^[0-9]+$/) {
            # This does very nifty matching:
            # order by id and make the exact match the first item as this
            # respects the limit you set on a query.
            my $order = \[
                "(case when me.id = ? then 1 else 2 end), me.id desc",
                int($params->{query})
            ];
            $options->{order_by} = $order;

            push @$where, { 'me.id' => int($params->{query}) };
        }
        else {
            $options->{order_by} = { -desc => 'me.id'};
        }

        if ($extra_params->{active}) {
            $args{'me.status'} = [qw/new open stalled/];
        }

        my $rs = $object_model->search_rs('case');
        $args{'-and'} = [
            { 'me.deleted' => undef },
            { 'me.id' => [{ -in => $rs->search({}, {group_by => 'object_id'})->get_column('object_id')->as_query }] },
            { -or => $where },
        ];

        $resultset = $c->model('DB::Zaak')->search_rs(
            \%args,
            $options,
        );
    }
    elsif($model eq 'ObjectBibliotheekEntry') {
        $resultset = $c->model(sprintf('DB::%s', $model))->search(
            {
                -and => [
                    $where,
                    object_type => $object_type,
                ],
            },
            $options
        );
    }
    elsif($object_type ne 'zaaktype') {
        $resultset = $c->model(sprintf('DB::%s', $model))->search($where, $options);
    }

    if ($object_type eq 'zaak') {
        if ($extra_params->{active}) {
            $resultset = $resultset->search({
                'me.status' => [qw/new open stalled/]
            });
        }
    }
    elsif ($object_type eq 'natuurlijk_persoon') {
        $resultset = $resultset->search({'me.active' => $extra_params->{active} // 1});
    }

    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');

    my @results = $resultset->all;

    if($object_type eq 'file') {
        return _filter_permitted_files($c, @results);
    }

    ### Last minute safety checks
    if ($object_type eq 'documents') {
        for (my $i = 0; $i < scalar(@results); $i++) {
            my $value = $results[$i];

            $c->stash->{zaak} = $c->model('DB::Zaak')->find($value->{zaak_id});

            unless ($c->check_any_zaak_permission(qw[zaak_read zaak_beheer zaak_edit])) {
                $c->log->debug(sprintf(
                    'No permission for case: %s',
                    $c->stash->{ zaak }->id
                ));

                delete($results[$i]);
            }
        }
    }

    return @results;
}

=head2 _build_object_resultset

Methodname is a misnomer, but in line with the rest of the code. This private
method returns a list of hashrefs that represent the search results for the
provided query.

    my @results = $self->_build_object_resultset({
        query => 'plantenbak',
        model => $c->model('Object')
    });

=head3 Parameters

Only one argument is expected in the method call, which should be a hashref.
For this hashref, the following keys can/must be set:

=over 4

=item query

Validated to be a C<Str>.

=item model

Validated to be an instance of L<Zaaksysteem::Object::Model>.

=back

=cut

define_profile _build_object_resultset => (
    required => {
        query => 'Str',
        model => 'Zaaksysteem::Object::Model'
    },
    optional => {
        max_items => 'Num',
        object_type => 'Str'
    },
    defaults => {
        max_items => 3
    }
);

sub _build_object_resultset {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $object_class_constraint = { -not_in => SEARCH_OBJECT_TYPE_BLACKLIST };

    if($params->{ object_type }) {
        $object_class_constraint = $params->{ object_type };
    }

    my $in_rs = $params->{ model }->rs('coolkidsquery', 0)->search_text_vector($params->{ query })->search(
        { object_class => $object_class_constraint },
        {
            order_by => [
                { -asc => 'object_class' },
                { -desc => 'date_modified' }
            ],
            '+select' => \'row_number() OVER (PARTITION BY object_class) AS rnum',
            '+as' => 'rnum',
            alias => 'coolkidsquery'
        }
    );

    my ($in_sql, @in_bind) = @{ ${ $in_rs->as_query } };

    my $rs = $params->{ model }->new_resultset->search(
        { rnum => { '<=' => $params->{ max_items } } },
        { from => \[ sprintf('%s me', $in_sql), @in_bind ] }
    );

    return map {
        {
            id => $_->id,
            searchable_id => $_->id,
            object_type => $_->type,
            is_object_type_instance => 1,
            label => $_->TO_STRING,
            object => $_
        }
    } $params->{ model }->inflate_from_rs($rs);
}

=head2 _retrieve_object

=cut

define_profile _retrieve_object => (
    required => [qw[c object_type searchable_id]],
    typed => {
        c => 'Zaaksysteem',
        object_type => 'Str',
        searchable_id => union([ 'Int', UUID ])
    }
);

sub _retrieve_object {
    my ($self, $params) = @_;

    my $opts = assert_profile($params)->valid;

    my $c = $opts->{ c };
    my $object_type = $opts->{ object_type };
    my $searchable_id = $opts->{ searchable_id };

    my $tablename = OBJECTSEARCH_TABLENAMES->{ $object_type }{ tablename };

    my $options = {};

    if($object_type eq 'natuurlijk_persoon') {
        $options->{ prefetch } = [ 'adres_id' ];
    } elsif($object_type eq 'zaaktype') {
        $options->{ prefetch } = [
            { zaaktype_node_id => 'zaaktype_definitie_id' }
        ];
    }

    $options->{result_class} = 'DBIx::Class::ResultClass::HashRefInflator';

    my $resultset = $c->model("DB::" . $tablename)->search(
        { searchable_id => $searchable_id },
        $options
    );

    # searchable_id is unique, so it's safe to return first. there ain't
    # gonna be more rows.
    my $object = $resultset->single;

    ### Return on no result
    return unless $object;

    if ($tablename eq 'ObjectBibliotheekEntry') {
        $object = $c->model('Object')->retrieve(uuid => $object->{object_uuid});
    }

    ### Deleted of store, ignore
    if($object_type eq 'natuurlijk_persoon' || $object_type eq 'bedrijf') {
        return if $object->{deleted_on};
    }
    if($object_type eq 'file') {
        return if !$object->{active_version};
        return if $object->{date_deleted};
    }

    return $self->_preprocess_object($c, $object, $object_type);
}

=head2 _preprocess_object

=cut

sub _preprocess_object {
    my ($self, $c, $object, $object_type) = @_;

    if($object_type eq 'natuurlijk_persoon') {
        if($object->{voornamen}) {
            $object->{voorletters} = _get_voorletters({
                voornamen => $object->{voornamen}
            });
        }

        $object->{ decorated_name } = $object->{voorletters} . ' ' . naamgebruik(
            {
                aanduiding => $object->{aanduiding_naamgebruik} || '',
                partner_voorvoegsel => $object->{partner_voorvoegsel},
                partner_geslachtsnaam => $object->{partner_geslachtsnaam},
                voorvoegsel => $object->{voorvoegsel},
                geslachtsnaam => $object->{geslachtsnaam},
            }
        );
        $object->{label} = $object->{ decorated_name };


        if($object->{geboortedatum}) {
            my $dob = DateTime::Format::Pg->parse_datetime($object->{geboortedatum});
            # Database date of birth is UTC, but is parsed as "floating"
            $dob->set_time_zone('UTC');
            $dob->set_time_zone('Europe/Amsterdam');

            $object->{geboortedatum} = $dob->dmy;

            $object->{label} .= " ($object->{geboortedatum})"
        }

        if ($object->{datum_overlijden}) {
            $object->{label} = "† " . $object->{label};
        }
    } elsif($object_type eq 'bedrijf') {
        $object->{label} = $object->{handelsnaam};
    } elsif($object_type eq 'zaak') {
        my $status = $object->{status};

        $object->{status_formatted} = STATUS_LABELS->{$status};

        my $case = $c->model('DB::Zaak')->find($object->{id});

        if($case) {
            $object->{ description } = $case->onderwerp;
            $object->{ fase } = $case->volgende_fase ? $case->volgende_fase->fase : $case->huidige_fase->fase;
        }

        $object->{ case_id } = $c->model('DB::ObjectData')->search({
            object_class => 'case',
            object_id => $object->{ id }
        })->get_column('uuid')->first;

    } elsif($object_type eq 'zaaktype') {

        if(my $preset_client = $object->{zaaktype_node_id}->{zaaktype_definitie_id}->{preset_client}) {
            my ($betrokkene_type, $betrokkene_id) = $preset_client =~ m|betrokkene-(\w+)-(\d+)|;

            if($betrokkene_type && $betrokkene_id) {
                my $betrokkene = $c->model('Betrokkene')->get({ type=> $betrokkene_type}, $betrokkene_id);

                if($betrokkene) {
                    $object->{preset_client} = {
                        naam        => $betrokkene->display_name,
                        id          => $preset_client,
                    };
                }
            }
        }

        $object->{ casetype_id } = $c->model('DB::ObjectData')->search({
            object_class => 'casetype',
            object_id => $object->{ id }
        })->get_column('uuid')->first;

        $object->{ description } = $object->{ zaaktype_node_id }{ zaaktype_omschrijving };
    } elsif($object_type eq 'bag') {

    }

    return $object;
}

=head1 SUBROUTINES

=head2 _get_voorletters

=cut

define_profile _get_voorletters => (
    required => [qw[voornamen]],
    typed => {
        voornamen => 'Str'
    }
);

sub _get_voorletters {
    my $voornamen = assert_profile(shift)->valid->{ voornamen };

    my ($firstchar) = $voornamen =~ /^(\w{1})/;
    my @other_chars = $voornamen =~ / (\w{1})/g;

    return join(".", $firstchar, @other_chars) . (
        ($firstchar || @other_chars) ?
        '.' : ''
    );
}

=head2 _parse_query

=cut

define_profile _parse_query => (
    required => [qw[query]],
    typed => {
        query => 'Str'
    }
);

sub _parse_query {
    my $query = assert_profile(shift)->valid->{ query };

    my $seen = {};

    # Sort by length, the parts of the query not already seen,
    # by splitting the query on whitespace
    my @query_parts = sort { length $b <=> length $a }
        grep { !$seen->{ $_ }++ }
        split /\s/, $query;

    return \@query_parts;
}

=head2 _filter_permitted_files

Users may only find files they have permissions on - otherwise
they will denied anyway which is annoying. if documents have no
case_id, they are not useful for anything, so don't show them.

kinda of shame to create case objects here - but the performance
penalty doesn't seem to be extreme.

=cut

sub _filter_permitted_files {
    my ($c, @results) = @_;

    my $check_file = sub {
        my $zaak = $c->model('DB::Zaak')->find(shift);

        return if ($zaak->status eq 'deleted');

        return $c->check_any_given_zaak_permission($zaak, qw[
            zaak_read zaak_beheer zaak_edit
        ]);
    };

    return grep { $_->{ case_id } && $check_file->($_->{ case_id }) } @results;
}

=head2 _limit_text

=cut

sub _limit_text {
    my ($text, $maxlength) = @_;

    my @words = split /\b/, $text;

    my $limited = '';

    foreach my $word (@words) {
        if((length ($limited) + length ($word)) > $maxlength) {

            my $difference = $maxlength - length($limited);
            if($difference > 10) {
                $limited .= substr($word, 0, 8) . '...';
            }
            last;
        }
        $limited .= $word;
    }

    return $limited;
}

=head2 _parse_street_number

This helper method attempts to parse a street number string into it's
constituent components

Returns a list of components in the order C<number>, C<letter>, and C<suffix>

=head3 Example

    _parse_street_number('7-521');

=cut

sub _parse_street_number {
    return shift =~ m[(\d+)([a-zA-Z])?[\s\-]?(\w+)?];
}

=head2 _strip_html

This helper method strips the provided string of HTML content by building an
HTML tree, returning the flattened plain text content of the tree.

=head3 Example

    my $text = _strip_html($potentially_html_content);

=cut

sub _strip_html {
    my $tree = HTML::TreeBuilder->new;

    $tree->parse(shift);
    $tree->eof;

    return $tree->as_text;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 OBJECTSEARCH_TABLENAMES

TODO: Fix the POD

=cut

=head2 STATUS_LABELS

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_OPTIONS

TODO: Fix the POD

=cut

=head2 bag_search

TODO: Fix the POD

=cut

=head2 bag_search_street

TODO: Fix the POD

=cut

=head2 objecttypes

TODO: Fix the POD

=cut

