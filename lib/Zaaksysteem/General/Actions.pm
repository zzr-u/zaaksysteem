package Zaaksysteem::General::Actions;
use strict;
use warnings;

use BTTW::Tools;
use Crypt::OpenSSL::Random qw(random_bytes);
use DateTime;
use File::Spec::Functions qw(catfile catdir splitpath);
use Hash::Flatten qw(unflatten);
use Log::Log4perl::MDC;
use List::Util qw(first);
use MIME::Base64 qw(decode_base64);
use URI;
use URI::Escape qw(uri_unescape);
use Zaaksysteem::Environment;
use Zaaksysteem::Object::ConstantTables qw(ZAAKSYSTEEM_CONFIG_DEFINITIONS);
use Zaaksysteem::Types qw(NonEmptyStr);

=head1 NAME

Zaaksysteem::General::Actions - Different actions to call on $c

=head1 SYNOPSIS

  $can_upload = $c->can_upload;

=head1 DESCRIPTION

When you would like to create a method in the L<Zaaksysteem> namespace, please DO NOT place
them directly in L<Zaaksysteem>. It prevents you from testing the method entirely. Instead, place
it in one of the classes subclassed in L<Zaaksysteem::General>, or, much easyer: in this class.

This way they will be all found together.

=head1 METHODS

=head2 load_customer_d_configs

Load the customer configurations of zaaksysteem

=head3 ARGUMENTS

None

=head3 RETURNS

Returns true on success, undef in case of errors.

=cut

sub _check_digest {
    my $c = shift;

    my $digest = $c->config->{digest_customerd} // '';

    my $digest_cache = try {
        return $c->cache->get('digest_customerd');
    } catch {
        throw('customer_d/load/cache_retrieval_failure', sprintf(
            'Caught exception retrieving "digest_customerd" key from cache: %s',
            $_
        ));
    };

    $digest_cache //= 'No digest in cache';

    # If the digests match, return void
    return if $digest eq $digest_cache;

    # Otherwise, the new digest
    return $digest_cache;
}

sub load_customer_d_configs {
    my $c = shift;

    my $digest = $c->_check_digest;

    # If we have a $digest, the check failed.
    return unless defined $digest;

    $c->log->info(sprintf(
        'Reloading customer.d configurations, current digest is "%s"',
        $digest
    ));

    $c->model('InstanceConfig')->flush;

    $c->config->{ digest_customerd } = $digest;

    return;
}

=head2 show_woz

Show WOZ boolean

=cut

sub show_woz {
    my $self = shift;

    return $self->model('DB::Interface')->search_active({module => 'woz'})->first
        && $self->check_any_user_permission('woz_objects') ? 1 : 0;
}

=head2 show_show_alternative_authentication

Show Alternative authentication boolean for the contactdossier

=cut


sub show_alternative_authentication {
    my $self = shift;
    return $self->model('DB::Interface')->search_active({module => 'auth_twofactor'})->first;
}

=head2 set_zs_version

=cut

sub set_zs_version {
    my ($c, $version) = @_;
    $version =~ s/^v//;
    $version =~ s/^(\d+\.\d+\.\d+)\.(\d+)/$1rc$2/;
    $version =~ s|_|.|;
    $c->config->{ZS_VERSION} = $version;
}

=head2 set_log_context

Proxy for L<Log::Log4perl::MDC/put>.

=cut

sub set_log_context {
    Log::Log4perl::MDC->put(@_);
}


### Basic zaak authorisation
sub _can_change_messages {
    my ($c) = @_;

    return unless $c->user_exists;

    my $case = $c->stash->{ zaak };
    my $case_open_url = $c->uri_for(sprintf('/zaak/%d/open', $case->id));

    if ($case->is_afgehandeld) {
        $c->push_flash_message(
            'U kunt geen wijzigingen aanbrengen aan deze zaak. Deze zaak is afgehandeld'
        );

        return 1;
    }

    if ($case->status eq 'new') {
        unless($case->behandelaar) {
            $c->push_flash_message(sprintf(
                'U bent niet de behandelaar van deze zaak. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } elsif ($case->behandelaar->gegevens_magazijn_id eq $c->user->uidnumber) {
            $c->push_flash_message(sprintf(
                'U heeft deze zaak nog niet in behandeling genomen. Klik <a href="%s">hier</a> om in behandeling te nemen.',
                $case_open_url->as_string
            ));
        } else {
            $c->push_flash_message('U bent niet de behandelaar van deze zaak.');
        }
        return 1;
    }

    if ($case->status eq 'stalled') {
        my $event = $case->logging->search_rs({event_type => "case/suspend"}, { order_by => { -desc => 'id' }, rows => 1})->first;
        $c->push_flash_message($event->onderwerp);
    }

    # I don't want the case to be reopened without hitting "hervatten" in the
    # GUI. Because "hervatten" has some logic which open does not have.
    # Only open cases can directly assign a case to self.
    if ($case->status eq 'open' && !$case->behandelaar) {
        $c->push_flash_message(sprintf(
            'U bent niet de behandelaar van deze zaak. Klik <a href="%s">hier</a> om in behandeling te nemen.',
            $case_open_url->as_string
        ));
    } elsif (!$case->behandelaar || $c->user_exists && $case->behandelaar->gegevens_magazijn_id ne $c->user->uidnumber) {
        $c->push_flash_message('U bent niet de behandelaar van deze zaak.');
    }

}



=head2 $c->can_change

Arguments: none

Return value: true, when a person is allowed to change zaak

Determines wether the current user is allowed to make changes
on the current case.

=cut

sub can_change {
    my $c = shift;

    # This boolflag is basically a bad idea, but I don't want to break
    # the existing callers of this method. Either this thing should assert
    # a consistant state and throw some meaningful exception on failure, *or*
    # it should silently do it's job, without side effects like setting a
    # flash message. TODO
    my $silent = shift;

    my $zaak = $c->stash->{zaak};
    return unless $zaak;

    # only once per request
    unless($c->req->param('_can_change_messages')) {
        $c->req->param('_can_change_messages', 1);
        $c->_can_change_messages;
    }

    return if $zaak->is_afgehandeld;

    ### Zaak beheerders mogen wijzigingen aanbrengen ondanks dat ze geen
    ### behandelaar.
    return 1 if $c->check_any_zaak_permission('zaak_beheer');

    ### Override when we have the correct permissions, and we have a
    ### coordinator and behandelaar
    return 1 if (
        $zaak->behandelaar &&
        $zaak->coordinator &&
        $c->check_any_zaak_permission('zaak_beheer','zaak_edit')
    );

    if (
        !$zaak->behandelaar ||
        !$c->user_exists ||
        !$c->user->uidnumber ||
        (
            $zaak->behandelaar &&
            $zaak->behandelaar->gegevens_magazijn_id ne $c->user->uidnumber &&
            (
                !$zaak->coordinator ||
                $zaak->coordinator->gegevens_magazijn_id ne $c->user->uidnumber
            )
        )
    ){
        $c->push_flash_message(
            'U kunt geen wijzigingen aanbrengen aan deze zaak, u bent geen behandelaar / coordinator.'
        ) unless $silent;

        return;
    }

    return 1;
}

=head2 $c->check_queue_coworker_changes

Arguments: TODO

Return value: TODO

Implements case action 'queue coworker changes'. This makes exclusive editing
rights possible for case behandelaars, all other users will be forced to submit
to their approval. (except admins)

Returns a true value when the case is using that action, and the current user
is not the behandelaar (owner).

=cut

sub check_queue_coworker_changes {
    my $c    = shift;
    my $zaak = $c->stash->{zaak};

    # admins always get what they want, this doesn't need to be queued
    return if $c->check_any_user_permission('admin');

    return unless $c->user;

    return $zaak->check_queue_coworker_changes($c->user->uidnumber);
}

=head2 has_client_type

Returns true if the current request has the C<X-Client-Type> header.

=cut

sub has_client_type {
    my $c = shift;
    if(my $type = $c->req->header('X-Client-Type')) {
        $c->log->debug("Client reported X-Client-Type of '$type'");
        return 1;
    }
    return 0;

}

# See:
# http://search.cpan.org/~bobtfish/Catalyst-Plugin-Session-0.35/lib/Catalyst/Plugin/Session.pm
#

=head2 push_flash_message

Arguments: \%MSG | @MSGS

Return value: $flash_message

TODO: needs documenting

Pushes a flash message on the stack and returns the given message.

=cut

sub push_flash_message {
    my $c       = shift;
    my $message;

    if ($c->has_client_type) {
        return;
    }

    if (UNIVERSAL::isa($_[0], 'HASH')) {
        $message    = shift;
    } else {
        $message    = sprintf shift, @_;
    }

    my $result = $c->flash->{result} || [];

    # As long as flash->{result} is used without this sub, we need to accomodate
    unless(ref $result && ref $result eq 'ARRAY') {
        $result = [$result];
    } elsif (!ref($result)) {
        $result = {
            message         => $result,
            type            => 'info'
        };
    }

    if (!UNIVERSAL::isa($message, 'HASH')) {
        $message    = {
            message     => $message,
            type        => 'info',
            ## Debugging information
            date        => time(),
        };
    }

    # Since same code runs multiple times sometimes, check for unicity
    my $lookup = { map { $_->{message} =>  1} grep { UNIVERSAL::isa($_, 'HASH') } @$result };
    unless($lookup->{$message->{message}}) {
        push @$result, $message;
    }

    $c->flash->{result} = $result;
    return $result;
}

sub _get_customer_from_instance_config {
    my ($c, $hostname) = @_;

    my $customer;
    try {
        $customer = $c->model('InstanceConfig')->get_instance_config($hostname);
        $c->config->{hostname} = $hostname;
    } catch {
        $c->log->warn(sprintf(
            'Exception occurred while loading instance configuration for host "%s": %s',
            $hostname,
            $_
        ));
    };
    return $customer;
}

sub _get_customer {
    my $c = shift;
    my $hostname = shift;

    my $customer = $c->_get_customer_from_instance_config($hostname);
    return $customer if $customer;

    if (defined $c->config->{default_customer}) {
        $customer = $c->_get_customer_from_instance_config($c->config->{default_customer});
        return $customer if $customer;
    }

    my $msg = sprintf(
        'Could not find configuration for hostname "%s"',
        $hostname
    );

    $c->log->error($msg);
    throw('zaaksysteem/hostname/not_found', $msg);
}


=head2 customer_instance

Get the customer instance

=cut

sub customer_instance {
    my $c = shift;

    $c->load_customer_d_configs;

    my $customer = $c->_get_customer($c->req->uri->host);

    if (my $template = $customer->{customer_id}) {
        $c->config->{gemeente_id} = $template;
    }

    my $hostname = $c->config->{hostname};
    if (   exists $customer->{VirtualHosts}
        && exists $customer->{VirtualHosts}{$hostname}
        && exists $customer->{VirtualHosts}{$hostname}{customer_id})
    {
        $c->config->{gemeente_id} = $customer->{VirtualHosts}{$hostname}{customer_id};
    }

    $c->config->{services_base} = $customer->{services_base}
        // 'https://unconfigured.services.zaaksysteem.nl';

    $c->config->{instance_uuid} = $customer->{instance_uuid};
    $c->config->{instance_hostname} = $customer->{instance_hostname};
    $c->config->{logging_id} =
           $customer->{logging_id}
        // $customer->{customer_id}
        // 'unknown';


    $c->_additional_static(
        [
            $c->config->{root},
            join('/',
                $c->config->{root},
                'tpl',
                'zaak_v1',
                $c->config->{'View::TT'}{locale}),
        ]
    );

    $c->config->{static}{include_path} = $c->_additional_static;

    my $basedir = '/var/tmp/zs';
    if ($customer->{start_config}{basedir}) {
         $basedir = $customer->{start_config}{basedir};
    }

    $c->config->{filestore_base_path} = catdir($basedir, 'storage');

    # TODO: Remove c->config->gemeente calls
    if ($customer->{customer_info}) {
        $c->config->{gemeente} = $customer->{customer_info};
    }

    # Return a copy of the hash so our original (possibly cached) data cannot
    # be corrupted via side effects on global state
    return \%{ $customer };
}

=head2 get_customer_info

Get the customer information from the database.

=cut

sub get_customer_info {
    my $self = shift;

    my $config = $self->model('DB')->schema->resultset('Config')->get_customer_config;

    # TODO: Remove c->config->gemeente calls
    $self->config->{gemeente} = $config;

    return $config;
}

=head2 $c->can_upload

Arguments: none

Return value: true, when a person is allowed to upload in zaak

Will return C<true> when a user is able to upload in the current dossier. It differs from
C<can_change> in the way that people are able to add documents to the document queue when the
dossier is not yet resolved. Even when they are not allowed to alter the dossier.

In short: it will return C<true> when the case is open/stalled or new.

=cut

sub can_upload {
    my ($c) = @_;

    return unless $c->stash->{zaak};

    return 1 if $c->stash->{zaak}->status =~ /^open|stalled|new$/;

    return;
}

=head2 get_client_ip

Get the client IP from the request, we look for the header C<X-Real-IP>
or C<X-Forwarded-For> or fall back to the actual IP.

=cut

sub get_client_ip {
    my $self = shift;

    my $real_ip = $self->req->header('X-Real-IP');

    if (!$real_ip) {
        if ($real_ip = $self->req->header('X-Forwarded-For')) {
            # Strip everything after the first comma.
            $real_ip =~ s/,.*//;
        } else {
            $real_ip = $self->req->address;
        }
    }

    return $real_ip;
}


=head2 assert_allowed_ip

Assert that the source IP is allowed, by checking it against the provided
network list.  Aborts the request (and redirects to the configured "portal"
page) if it's not allowed.

=cut

sub assert_allowed_ip {
    my ($self, $allowed_ip_list) = @_;

    my $allowed_nets = $allowed_ip_list;

    my $real_ip   = $self->get_client_ip;
    my $client_ip = NetAddr::IP->new($real_ip);

    for (split m[\s*,\s*], $allowed_ip_list) {
        my $net = NetAddr::IP->new($_);

        unless($net) {
            $self->log->warn("Unable to parse range '$_'");
            next;
        }

        return 1 if $net->contains($client_ip);
    }

    $self->log->warn(sprintf(
        'Client %s not whitelisted, redirecting.',
        $real_ip
    ));

    $self->detach('/forbidden');
}

=head2 assert_platform_access

Will check if a platform call, like a cron "curl" job, is allowed to access. This will be
checked against the ip addresses listed in C<platform_allowed_from>. The other possibility
is a header called C<Platform-API-Key>, when it matches with config item C<platform_api_key>,
access will be allowed even when the IP address does not match.

Will also check for the new-style platform key in a C<ZS-Platform-Key> header. This only works
if L<Zaaksysteem::Controller::Page/check_platform_key> has been called
beforehand to authenticate the user.

=cut

sub assert_platform_access {
    my ($self) = @_;

    # New-style platform key usage (ZS-Platform-Key)
    return 1 if $self->stash->{platform_access};

    if ($self->req->header('platform-api-key') && ($self->req->header('platform-api-key') || '') eq ($self->config->{platform_api_key} || '')) {
        return 1;
    }

    $self->assert_allowed_ip($self->config->{platform_allowed_from} || '127.0.0.1');
}

=head2 get_saml_session_timeout

Get the ammount of milliseconds left before the end of the SAML session.

=head3 RETURNS

Any number of milliseconds left before the session is expired.
0 if there is no SAML session or if it is expired.

=cut

sub get_saml_session_timeout {
    my $self = shift;

    return 0 unless $self->check_saml_session;
    return $self->_get_session_diff($self->session->{__expire_keys}{_saml});
}

=head2 extend_saml_session_timeout

Extend the SAML session with 15 minutes.

=head3 RETURNS

1 if the session is extended, 0 if not.

=cut

sub extend_saml_session_timeout {
    my $self = shift;

    if ($self->get_saml_session_timeout) {
        $self->session_expire_key('_saml' => '900');
        return 1;
    }
    return 0;

}

sub _get_session_diff {
    my ($self, $expires) = @_;

    my $diff = DateTime->from_epoch(epoch => $expires) - DateTime->now();

    return 0 if !$diff->is_positive;
    return ( ( ( ( $diff->hours * 60 ) + $diff->minutes ) * 60 ) + $diff->seconds ) * 1000;

}

=head2 get_session_timeout

Get the ammount of milliseconds left before the end of the session.

=head3 RETURNS

Any number of milliseconds left before the session is expired.
0 if there is no session or if it is expired.

=cut

sub get_session_timeout {
    my $self = shift;
    return $self->_get_session_diff($self->session_expires);
}

=head2 extend_session_timeout

Extend the session with two hours

=head3 RETURNS

1 if the session is extended, 0 if not.

=cut

sub extend_session_timeout {
    my $self = shift;

    return 0 unless $self->get_session_timeout;

    # Lift the plugin config, or default to 2 hours
    my $timeout = eval { $self->config->{ 'Plugin::Session' }{ expires } } || 7200;

    $self->log->debug(sprintf(
        'Resetting user session timeout, %d seconds, expires at %s',
        $timeout,
        DateTime->now->add(seconds => $timeout)->iso8601
    ));

    $self->extend_session_expires($timeout);

    return 1;
}

=head2 check_saml_session

Soft fail for saml sessions. If we don't have a expire key for saml

=cut

sub check_saml_session {
    my $self = shift;

    if ($self->user_exists || !exists $self->session->{__expire_keys}{_saml}) {
        return 0;
    }
    return 1;
}

=head2 return_error

Set error status 500 and return the specified error as formatted by format_error().

=cut

sub return_error {
    my $c = shift;
    $c->res->status(500);
    return $c->format_error(@_)
}

=head2 process_error

Takes an exception and returns a formatted variant, while logging the message
and setting the status of the response to C<500>.

=cut

sub process_error {
    my $c = shift;
    my $error = shift;

    $c->log->warn($error);
    $c->res->code(500);

    return $c->format_error($error);
}

=head2 format_error

Takes an exception object and formats it for a JSON response.

=cut

sub format_error {
    my $c = shift;
    my $error = shift;
    my $error_ref = ref $error;

    # Pretty Exception::Class errors
    if (eval { $error->isa('Exception::Class::Base') }) {
        my @trace_args;

        # Stracktraces can be -huge-, only add them in developer mode.
        if ($c->debug) {
            @trace_args = map {
                {
                    package    => $_->package,
                    line       => $_->line,
                    filename   => $_->filename,
                    subroutine => $_->subroutine,
                    args_passed_to_sub => [$_->args],
                }
            } $error->trace->frames;
        }

        $c->log->error(sprintf('Global exception caught: %s', $error->as_string));

        return {
            stacktrace      => \@trace_args,
            debug           => $c->debug,
            body_parameters => $c->req->body_parameters,
            error_code      => $error->code || 'unhandled',
            category        => $error_ref,
            messages        => [$error->as_string],
        };
    } elsif (eval { $error->isa('Zaaksysteem::Exception::Base') }) {
        my $formatter = sub {
            my $frame = shift;

            return {
                package => $frame->package,
                filename => $frame->filename,
                line => $frame->line,
                subroutine => $frame->subroutine,
                args_passwd_to_sub => []
            };
        };

        $c->log->error(sprintf('Global exception caught: %s', $error->TO_STRING));

        my @trace_args;

        if($c->debug) {
            @trace_args = map { $formatter->($_) } $error->stack_trace->frames;
            $c->log->debug($error->stack_trace->as_string);
        }

        return {
            stacktrace      => \@trace_args,
            debug           => $c->debug,
            body_parameters => $c->req->body_parameters,
            error_code      => 500,
            category        => ref $error,
            messages        => [ $error->message ]
        };
    }
    # Non-Exception::Class errors - i.e. regular die's
    else {
        $c->log->error(sprintf('Global exception caught: %s', burp($error)));

        return {
            debug           => $c->debug,
            body_parameters => [$c->req->body_parameters],
            error_code      => 'unhandled',
            category        => sprintf("%s", $c->action),
            messages        => burp($error),
            stacktrace      => 'Not (yet) available for non-exception errors',
        };
    }
}

=head2 init_log4perl

Initializes the L<Log::Log4perl::Catalyst> instance for the current request.

Will attempt to load C</etc/zaaksysteem/log4perl.conf>,
C</vagrant/etc/log4perl.custom.conf>, C</vagrant/etc/log4perl.conf>.

This behavior can be overridden via the C<Log4perl> config item in the main
configuration file.

=cut

sub init_log4perl {
    my $self = shift;

    if (!$self->config->{Log4perl}) {
        foreach (qw(/etc/zaaksysteem/log4perl.conf /vagrant/etc/log4perl.custom.conf /vagrant/etc/log4perl.conf)) {
            next if (!-e $_);
            $self->log(Log::Log4perl::Catalyst->new($_));
            $self->log->warn("Loaded $_. Please set Log4perl in your configuration file");
            return;
        }
        return;
    }

    $self->log(
        Log::Log4perl::Catalyst->new(
            $self->config->{Log4perl}{file},
            %{$self->config->{Log4perl}{options}}
        )
    );
    $self->log->info(sprintf("Loaded %s from configuration file", $self->config->{Log4perl}{file}));
}

=head2 load_xml_compile_classes

Initializes L</xml_compile> with known classes, unless the instance is
configured to forgo early initialization via the
C<disable_xml_compile_preload> configuration key, or the
C<ZS_DISABLE_STUF_PRELOAD> environment variable.

=cut

sub load_xml_compile_classes {
    my $self = shift;

    if ($self->config->{disable_xml_compile_preload} || $ENV{ZS_DISABLE_STUF_PRELOAD}) {
        $self->log->debug("Skipping preloading of XML compile classes");
        return 0;
    }

    $self->xml_compile->add_class([qw/
        Zaaksysteem::StUF::0204::Instance
        Zaaksysteem::StUF::0301::Instance
        Zaaksysteem::StUF::0312::Instance

        Zaaksysteem::XML::Pink::Instance
        Zaaksysteem::XML::MijnOverheid::Instance
        Zaaksysteem::XML::Xential::Instance
        Zaaksysteem::XML::Zaaksysteem::Instance

        Zaaksysteem::XML::Generator::StUF0310
    /]);

    return 1;
}

=head2 set_referer

Set the referer in a safe way so we don't go outside our own domain. If the
referer starts with a C</> we treat this as within our own domain. All the
query parameters are used to redirect if the hostname equals to our own host,
otherwise you will be redirected to C</>.

=cut

sub set_referer {
    my ($self, $uri) = @_;

    return unless $uri;

    my $req_host = lc($self->req->uri->host);

    if ($uri =~ /^\//) {
        $uri = URI->new_abs($uri, "https://$req_host");
    }
    else {
        $uri = URI->new($uri);
    }

    if (($uri->scheme //'') ne 'https') {
        $self->log->warn("URI scheme is not 'https'");
        return $self->uri_for('/');
    }

    my $host = lc($uri->host);
    if ($req_host eq $host) {
        return $uri->as_string;
    }
    else {
        $self->log->warn("Referer host '$host' does not match '$req_host', redirecting to /");
        return $self->uri_for('/');
    }
}

=head2 get_zs_session_id

Returns the session id

=cut

sub get_zs_session_id {
    my $self = shift;
    return $Zaaksysteem::Environment::REQUEST_ID;
}

=head2 serve_file

Serve a L<Zaaksysteem::Schema::File> in a specific format, using Nginx
acceleration if possible.

=cut

=head2 @DOWNLOAD_DISABLED_PDF

Path to the "Unable to convert/download instead" for the "old style" (document tab) PDF viewer.

=head2 @DOWNLOAD_DISABLED_PDF_ZS_PDF_VIEWER

Path to the "Unable to convert/download instead" for the "new style" ("zsCaseView") PDF viewer.

=head2 @DOWNLOAD_WAITING_PDF

Filepath to the waiting_preview_pdf file

=cut

my @DOWNLOAD_DISABLED_PDF               = (qw(share pdf unable-to-convert.pdf));
my @DOWNLOAD_DISABLED_PDF_ZS_PDF_VIEWER = (qw(share pdf unable-to-convert-zs-pdf-viewer.pdf));
my @DOWNLOAD_WAITING_PDF                = (qw(share pdf converting.pdf));
my @DOWNLOAD_SCANNING_PDF               = (qw(share pdf virus-scanning.pdf));
my @DOWNLOAD_RESTRICTED_PDF             = (qw(share pdf insufficient-rights.pdf));


sub _serve_file {
    my $self = shift;

    my $file = catfile($self->config->{home}, @_);
    $self->log->debug("Serving local file: '$file'");
    $self->serve_static_file($file);

    my $filename = (splitpath($file))[2];
    return ($filename, 'application/pdf');
}

sub serve_file {
    my $self = shift;
    my ($file, $format) = @_;

    my %download_info = $file->get_download_info($format);

    if  ($download_info{restricted}) {
        return $self->_serve_file(@DOWNLOAD_RESTRICTED_PDF);
    }

    if ($download_info{download_disabled}) {
        return $self->_serve_file(
              $self->req->header('ZS-PDF-Viewer')
            ? @DOWNLOAD_DISABLED_PDF_ZS_PDF_VIEWER
            : @DOWNLOAD_DISABLED_PDF
        );
    }
    if ($download_info{download_scanning}) {
        return $self->_serve_file(@DOWNLOAD_SCANNING_PDF);
    }
    if ($download_info{download_waiting}) {
        return $self->_serve_file(@DOWNLOAD_WAITING_PDF);
    }
    else {
        $self->serve_filestore($download_info{filestore});
        return ($download_info{filename}, $download_info{filestore}->mimetype);
    }
}

=head2 serve_filestore

Serve a specific L<Zaaksysteem::Schema::Filestore> entry, using C<X-Accel-Redirect> acceleration.

=cut

sub serve_filestore {
    my $self = shift;
    my ($filestore) = @_;

    my $download_url = $filestore->download_url;
    my $mimetype = $filestore->mimetype;

    # Content-Length, etc. are handled by Nginx/the server it proxies to.
    $self->res->headers->header('X-Accel-Redirect', $download_url);
    $self->res->headers->content_type($mimetype);
    $self->res->body('');

    $self->log->debug("Serving '" . $filestore->original_name . "'; X-Accel-Redirect = '$download_url' as '$mimetype'");

    return;
}

=head2 load_session_casetype

Load the specified casetype into the session.

=cut

sub load_session_casetype {
    my ($self, $casetype_id) = @_;

    if (! $self->session->{zaaktypen}{$casetype_id}) {
        $self->session->{zaaktypen}{$casetype_id} = $self->model('Zaaktypen')->retrieve(
            id         => $casetype_id,
            as_session => 1,
        ) or die "unable to load case type into session";
    }

    return $self->session->{zaaktypen}{$casetype_id};
}

=head2 log_request_header

Log specific request headers: C<cookie> and C<x-xsrf-token>

=cut

sub log_request_header {
    my $self = shift;
    $self->_log_headers($self->req, 'request', qw(cookie x-xsrf-token));
}

=head2 log_response_header

Log specific response headers: C<set-cookie>

=cut

sub log_response_header {
    my $self = shift;
    $self->_log_headers($self->res, 'response', qw(set-cookie));
}

sub _log_headers {
    my ($self, $r, $type, @header_info) = @_;

    my $headers = $r->headers;
    my @warn;
    for my $h (@header_info) {
        if (my $found = $headers->header($h)) {
            $self->log->info(
                sprintf(
                    '%s found in %s %s',
                    lc($h), $type, dump_terse($found)
                )
            );
        }
        else {
            push(@warn, $h);
        }
    }
    if (@warn) {
        $self->log->warn(
            sprintf(
                "did not find %s in %s %s",
                join (", ", @warn), $type, dump_terse($headers)
            )
        );
    }
}

=head2 log_detach

Log an error and detach to an error page.

    $self->log_detach(
        error => 'logline for log4perl',

        # defaults to 'Helaas, deze zaak kan niet worden aangevraagd',
        title => 'Title for the error page',

        human_readable =>
            'A descriptive message for the user that action is not possible',
    );

=cut

define_profile log_detach => (
    required => { error => 'Str' },
    optional => { title => 'Str' },
    defaults => { title => 'Helaas, deze zaak kan niet worden aangevraagd' },
);

sub log_detach {
    my ($self, %options) = @_;

    $self->log->error($options{error});

    $self->stash->{template}   = 'general_error.tt';
    $self->stash->{request_id} = $self->get_zs_session_id;

    $self->stash->{error} = {
        titel => $options{title},
        bericht => $options{human_readable},
    };

    $self->detach;
}

=head2 get_client_certs

Get client certificates from c->engine->env or c->req->header.

=cut

sub get_client_certs {
    my $self = shift;

    my $env = $self->engine->env;

    my $client_cert;
    if (my $base64_crt = $env->{SSL_CLIENT_CERT} || $self->req->header('x-client-ssl-cert')) {
        $client_cert = decode_base64($base64_crt);
    } elsif (my $uri_crt = $self->req->header('ssl-client-cert')) {
        $client_cert = uri_unescape($uri_crt);
    }

    if ($client_cert) {
        my $cert = Crypt::OpenSSL::X509->new_from_string($client_cert);
        my $dn = $cert->subject();

        $self->log->debug("Got client certificates from caller $dn:$client_cert");
        return { dn => $dn, cert => $client_cert };

    }

    $self->log->debug("No client certificates provided by caller");
    return;
}

=head2 assert_soap_action

Asserts if a request has a SOAP Action.

The presence and content of the SOAPAction header field can be used by
servers such as firewalls to appropriately filter SOAP request messages
in HTTP. The header field value of empty string ("") means that the
intent of the SOAP message is provided by the HTTP Request-URI. No value
means that there is no indication of the intent of the message.

See also: L<https://www.w3.org/TR/2000/NOTE-SOAP-20000508/#_Toc478383528>.

=cut

sub assert_soap_action {
    my $self = shift;

    my $action = $self->req->header('soapaction');

    throw(
        "header/soap-action/missing",
        "No SOAPAction defined in header"
    ) unless defined $action;

    return $action if $action eq '';
    return $self->req->uri->as_string if $action eq '""';

    if ($action =~ /^\s*"(?<soapaction>.*)"\s*$/) {
        $action = $+{soapaction};

        return $action if NonEmptyStr->check($action);
    }

    throw(
        "header/soap-action/invalid_syntax",
        "No valid SOAPAction defined in header: '$action'"
    );
}

=head2 assert_post

Assert a POST action

=cut

sub assert_post {
    my $self = shift;

    my $m = uc($self->req->method);

    return 1 if $m eq 'POST';

    throw('request_method/not_allowed', sprintf(
        'HTTP request method %s is not allowed: POST only',
        $m
    ));

}

=head2 create_zs_login_token

Sets the "zs_login_token" key in the stash, so templates can use it, and sets
the "ZS-Login-Token" cookie to the same value.

=cut

sub create_zs_login_token {
    my $self = shift;

    my $random = unpack('H*', random_bytes(16));

    $self->res->cookies->{"ZS-Login-Token"} = {
        value    => $random,
        secure   => ($self->req->uri->scheme eq 'https' ? 1 : 0), # HAAAAACK
        expires  => '+24h',
        httponly => 1,
    };

    $self->stash->{zs_login_token} = $random;

    return;
}

=head2 assert_zs_login_token

Asserts that the "zs_login_token" specified in the request parameters is the same as
the one in the "ZS-Login-Token" request cookie.

This prevents a lot of CSRF attacks from working (as they can't send the cookie).

=cut

sub assert_zs_login_token {
    my $self = shift;

    my $login_token = $self->req->params->{zs_login_token} // '';

    my $cookie = $self->req->cookies->{"ZS-Login-Token"};
    my $expected_token = $cookie
        ? $cookie->value
        : '';

    throw(
        "auth/csrf_token_mismatch",
        "Login-token does not match expected value"
    ) unless(length($expected_token) && $login_token eq $expected_token);

    return;
}

=head2 show_error

Show an error template with the specified message.

Detaches.

=cut

sub show_error {
    my ($self, $message) = @_;

    $self->stash->{error_message} = $message;
    $self->stash->{template} = 'error.tt';
    $self->detach();
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
