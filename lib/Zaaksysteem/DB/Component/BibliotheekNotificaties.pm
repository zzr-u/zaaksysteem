package Zaaksysteem::DB::Component::BibliotheekNotificaties;

use Moose;

extends 'DBIx::Class::Row';
with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::DB::Component::BibliotheekNotificaties - E-mail template
behaviors

=head1 DESCRIPTION

This class overrides the default L<DBIx::Class::Row> behaviors such that the
rows automatically receive search terms for the system-wide search indexes.

=cut

use BTTW::Tools;

use Mail::Track;
use Zaaksysteem::ZTT;

=head1 METHODS

=head2 insert

Overrides the L<DBIx::Class::Row/insert> method and sets the search terms for
the new row.

This also removes conflicting data (mostly a guard for casetype imports)
before executing the insert. (C<searchable_id> and C<uuid> fields are
stripped).

=cut

before insert => sub {
    my ($self) = @_;

    # we need the database to generate the new searchable_id param.
    # if we don't supply it, it will take its chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    my $old_search_id = delete $self->{ _column_data }{ searchable_id };
    my $old_uuid = delete $self->{ _column_data }{ uuid };

    $self->log->info(sprintf(
        'Discarded value ("%s") for "searchable_id" from insert to prevent index conflicts',
        $old_search_id
    )) if defined $old_search_id;

    $self->log->info(sprintf(
        'Discarded value ("%s") for "uuid" from insert to prevent duplicated content in constrainted column',
        $old_uuid
    ));

    $self->_set_search_terms;

    return;
};

=head2 update

Overrides the L<DBIx::Class::Row/update> method and sets the search terms for
the row before updating.

=cut

before update => sub { shift->_set_search_terms(@_) };

=head2 TO_JSON

JSON serialization helper for L<JSON>.

=cut

sub TO_JSON {
    my $self = shift;


    my @attachments = $self->bibliotheek_notificatie_kenmerks->all;

    my @files = map {
        my $result = {
            naam  => $_->bibliotheek_kenmerken_id->naam,
            bibliotheek_kenmerk_id => $_->get_column('bibliotheek_kenmerken_id')
        };
        $result;
    } @attachments;

    return {
        attachments => \@files,
        label       => $self->label,
        message     => $self->message,
        subject     => $self->subject,
        sender      => $self->sender,
        sender_address => $self->sender_address,
        %{ $self->next::method() },
    }
}

=head2 send_mail

Arguments: \%PARAMS

Return value: $TRUE_ON_SUCCES

    $self->send_mail(
        {
            to  => 'michiel@example.com',
            from => 'info@example.com',
            ztt_context => {
                magicstring1 => 'value_magistring1',
                surname      => 'Jansen',
            },
            request_id => 'X',
        }
    )

Sends a mail containing the current template. With C<ztt_context> filled, it will replace
the magic strings with the given values.

B<Params>

=over 4

=item to [required]

The rcpt of this message

=item ztt_context [required]

A key-value HASHREF containing magic strings and their values.

=item from [optional]

The return-address of this message.

=back

=cut

define_profile 'send_mail' => (
    required    => [qw/to ztt_context request_id/],
    optional    => [qw/from/],
);

sub send_mail {
    my $self = shift;
    my $params = assert_profile(shift || {})->valid;

    my $obj = Mail::Track->new(
        identifier_regex     => qr/ZS (\d{4})/,
        identifier           => '1234',
    );

    my $ztt = Zaaksysteem::ZTT->new;

    $ztt->add_context($params->{ztt_context});

    $params->{subject} = $ztt->process_template($self->subject)->string;
    $params->{extra_headers} = { 'X-ZS-Request' => $params->{request_id} // 'unknown' };

    my $body           = $ztt->process_template($self->message)->string;

    if (!$params->{from}) {
        $params->{from} = $self->sender_address
            ? $self->sender_address
            : $self->result_source->schema->resultset('Config')->get_customer_config->{zaak_email};
    }

    my $msg = $obj->prepare($params);   
    $msg->add_body(
        {
            content      => $body,
            content_type => 'text/plain',
        }
    );
    
    return $msg->send;
}

sub _set_search_terms {
    my $self = shift;
    my $values = shift || {};

    my @fields = qw[label subject message uuid];

    my @terms = grep {
        defined && length
    } map {
        $values->{ $_ } || $self->get_column($_)
    } @fields;

    # Ensure empty content results in undefined columns.
    my $search_term = scalar @terms ? join(' ', @terms) : undef;

    $self->search_term($search_term);
    $self->search_order($search_term);

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
