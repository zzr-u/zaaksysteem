package Zaaksysteem::DB::Component::Logging::Case::Attribute::Create;

use Moose::Role;

use HTML::Strip;
use JSON;

has attribute => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('BibliotheekKenmerken')->find($self->data->{ attribute_id });
});

sub onderwerp {
    my $self = shift;

    unless($self->data->{ attribute_id }) {
        return $self->get_column('onderwerp');
    }

    sprintf(
        'Kenmerk "%s" aangemaakt',
        $self->attribute ? $self->attribute->naam : '&lt;geen&gt;',
    );
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    my $stripper = HTML::Strip->new;

    $data->{ content } = $stripper->parse($self->data->{ attribute_value });
    $data->{ expanded } = JSON::false;

    return $data;
};

sub event_category { 'case-mutation'; }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 event_category

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

