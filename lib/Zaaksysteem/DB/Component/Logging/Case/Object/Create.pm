package Zaaksysteem::DB::Component::Logging::Case::Object::Create;

use Moose::Role;

=head1 NAME

Zaaksysteem::DB::Component::Logging::Case::Object::Create - Event subject
for object creation mutations.

=head1 METHODS

=head2 onderwerp

Overrides L<Zaaksysteem::Schema::Logging/onderwerp> and provides a
contextualized summary of the event.

=cut

sub onderwerp {
    my $self = shift;

    return sprintf(
        '%s "%s" aangemaakt',
        $self->data->{ object_type_name },
        $self->object_url
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
