package Zaaksysteem::DB::Component::Logging::User::Apply;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Gebruiker "%s" aangemeld voor zaaksysteem voor gewenste afdeling: %s',
        $self->data->{ user_displayname },
        $self->data->{ group_name }
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

