package Zaaksysteem::DB::ResultSet::ZaaktypeRegel;
use Moose;

#
# The purpose of this module is to execute user defined rules in the input flow. E.g. when subsidy is
# applied for, the general range of subsidy will prompt different questions to be asked. The rules can
# hide questions, show others, pre-fill values, or stop the process altogether if a dead end has been
# reached.
#

extends 'DBIx::Class::ResultSet', 'Zaaksysteem::Zaaktypen::BaseResultSet';
with 'MooseX::Log::Log4perl';

use BTTW::Tools;
use Zaaksysteem::Backend::Rules::Serializer;

has serializer => (
    is      => 'ro',
    isa     => 'Zaaksysteem::Backend::Rules::Serializer',
    default => sub {
        my $self = shift;
        return Zaaksysteem::Backend::Rules::Serializer->new(
            schema => $self->result_source->schema,
        );
    },
    lazy => 1,
);

use constant    PROFILE => {
    optional        => [qw/
        naam
        settings
    /],
};

sub _validate_session {
    my $self            = shift;
    my $profile         = PROFILE;
    my $rv              = {};

    $self->__validate_session(@_, $profile);
}


sub _commit_session {
    my ($self, $node, $element_session_data) = @_;

    foreach my $regel_id (keys %$element_session_data) {

        my $regel = $element_session_data->{$regel_id};

        delete $regel->{ created };
        delete $regel->{ last_modified };

        # Force booleans values
        $regel->{is_group} //=0;

        ### Make sure regel is active when active rule did not exist in old import file
        $regel->{active} = 1 unless exists $regel->{active};

        delete $regel->{settings}; # settings is a json representation of the whole hash.
        $regel->{settings}= $self->serializer->encode($regel);
    }

    $self->next::method( $node, $element_session_data );
}

sub _retrieve_as_session {
    my $self            = shift;

    my $rv              = $self->next::method();

    return $rv unless UNIVERSAL::isa($rv, 'HASH');

    foreach my $index (keys %$rv) {
        my $regel = $rv->{$index};

        try {
            my $d = $self->serializer->decode($regel->{settings}) // {};
            foreach (keys %$d) {
                $regel->{$_} = $d->{$_};
            }
        }
        catch {
            $self->log->warn(sprintf("Could not deserialize regel '%s' : %s", $regel->{settings}, $_));
        };
    }

    return $rv;
}


sub _retrieve {
    my $self            = shift;

    my $rv              = $self->next::method();

    return $rv unless UNIVERSAL::isa($rv, 'HASH');
#    warn "retrived lekker";
    return $rv;
}

define_profile execute => (
    required => [qw(
        kenmerken
        aanvrager
        contactchannel
        casetype
    )],
    optional =>  [qw(
        payment_status
        case_result
        confidentiality
        payment_status
    )],
    missing_optional_valid => 1,
);

define_profile execute => (
    required => [qw(
        kenmerken
        aanvrager
        contactchannel
        casetype
    )],
    optional =>  [qw(
        payment_status
        case_result
        confidentiality
        payment_status
    )],
    missing_optional_valid => 1,
);

#
# execute the set of rules. traverse and execute one by one, gather results.
#
sub execute {
    my ($self, $opts) = @_;
    $opts = assert_profile($opts)->valid;
    my $result = {};

    while(my $rule = $self->next) {
        next unless $rule->active;

        $rule->execute({
            kenmerken        => $opts->{kenmerken},
            aanvrager        => $opts->{aanvrager},
            contactchannel   => $opts->{contactchannel},
            payment_status   => $opts->{payment_status},
            casetype         => $opts->{casetype},
            result           => $result,
            confidentiality  => $opts->{confidentiality},
            case_result      => $opts->{case_result},
        });
    }

    return $result;
}





1;





__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 execute

TODO: Fix the POD

=cut

