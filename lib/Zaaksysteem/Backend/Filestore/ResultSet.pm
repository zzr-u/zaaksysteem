package Zaaksysteem::Backend::Filestore::ResultSet;

use Moose;

use DateTime;
use File::ArchivableFormats;
use Params::Profile;
use Path::Tiny;
use Try::Tiny;
use UUID::Tiny;

use BTTW::Tools;
use BTTW::Tools::File qw(get_file_extension);

use Zaaksysteem::Constants
    qw|MAX_REPUSH_PENDING_ITEMS
       MAX_REPUSH_PENDING_STREAM
       MIN_REPUSH_PENDING_PERIOD
    |;


our $CHUNK_SIZE =                                64 *        1024; # 64k chuncks

our $MAX_ITEMS  = MAX_REPUSH_PENDING_ITEMS   // 100;               # more will wait
our $MAX_STREAM = MAX_REPUSH_PENDING_STREAM  // 0.5 * 1024 * 1024; # 0.5 MB/sec
our $MIN_PERIOD = MIN_REPUSH_PENDING_PERIOD  // 900;               # seconds in 15 mins

extends 'DBIx::Class::ResultSet';

with 'Zaaksysteem::Roles::FilestoreModel', 'MooseX::Log::Log4perl';

use Exception::Class (
    'Zaaksysteem::Backend::Filestore::ResultSet::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Filestore::ResultSet::Exception::FileType' => {
        isa         => 'Zaaksysteem::Backend::Filestore::ResultSet::Exception',
        description => 'Filetype exception',
        alias       => 'throw_filetype_exception',
    },
);

=head1 METHODS

=head2 filestore_create

Creates a Filestore entry.

=head3 Arguments

=over

=item file_path [required]

The path to the file that needs to be added to the file storage.

=item original_name [required]

The name the file originally has or had. This is always required to prevent
issues where a file storage (uuid-based) file name gets used.

=item ignore_extension [optional]

Does not execute the allowed extension check. Use with care.

=item force_mimetype [optional]

Force the given mimetype. Please use with care, the L<File::ArchivableFormats> module is smart enough to
find the mimetype for you.

=back

=head3 Returns

The newly created Filestore object.

=cut

define_profile 'filestore_create' => (
    required => [qw/
        original_name
        file_path
    /],
    optional => [qw/
        ignore_extension
        id
        force_mimetype
    /]
);

sub filestore_create {
    my $self = shift;
    my $opts = $_[0];

    my $args = assert_profile($opts)->valid;

    my $uuid = UUID::Tiny::create_uuid_as_string(UUID::Tiny::UUID_V4);

    my $db_params = {};

    my $t0 = Zaaksysteem::StatsD->statsd->start;
    # only if it exists
    my $file_path = Path::Tiny->new($args->{file_path})
        ->assert( sub {
            $_->exists or throw(
                'filestore/no_readable_file',
                "File '$_' is does not exist or isn't readable"
            )
        }
    );
    # size check
    $db_params->{size}              = $file_path->stat->size or
        throw("filestore/create/empty", "Will not add an empty file");

    # archivable format mime-type
    my $af_info = _archivable_format( $file_path, $args->{force_mimetype} );
    $db_params->{is_archivable}     = $af_info->{DANS}{archivable};
    $db_params->{mimetype}          = $af_info->{mime_type};

    # filestore location
    my $filestore_engine = $self->filestore_model->get_default_engine();
    $db_params->{storage_location}  = [$filestore_engine->name];

    # others
    $db_params->{original_name}     = $args->{original_name};
    $db_params->{md5}               = $file_path->digest( { chunk_size => $CHUNK_SIZE }, "MD5" );
    $db_params->{uuid}              = $uuid;
    $db_params->{id}                = $args->{id} if $args->{id}; # XXX Create with duplicat ID's break
    $db_params->{virus_scan_status} = 'pending'; # let's be explicit, it's the default value too
    Zaaksysteem::StatsD->statsd->end('filestore.io.stat.time', $t0);

    $t0 = Zaaksysteem::StatsD->statsd->start;
    my $filestore_row = $self->create($db_params);
    Zaaksysteem::StatsD->statsd->end('filestore.db.create.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestore.create', 1);

    $t0 = Zaaksysteem::StatsD->statsd->start;
    try {
        $filestore_engine->write(
            $uuid, $file_path->openr(), $file_path->realpath
        );
    }
    catch {
        $filestore_row->delete;
        die $_;
    };
    Zaaksysteem::StatsD->statsd->end('filestore.io.add.time', $t0);

    # add tasks to the queue
    $self->_create_and_push_file_scan_queue_item($filestore_row);

    return $filestore_row->discard_changes;
}

=head1 INTERNAL METHODS

=pod TODO

moved from File::Scan::ClamAV to VSS and went for minimal code changes and kept
most of it original behaviour:
- throw errors when something is wrong wioth the virus scanner
- throw errors when we detected errors
- return; otherwise, that is, no viruses detected

a better solution would be to return a list of found viruses (or empty), both 
are valid responses from the VSS - or throw an error.

our caller should then handle it correctly
- $filestore_row->update_from_viruses( @viruses )
- throw an error when 'found' - as that is the behavior on their level
- retry through async VSS when we had an exception/problem with VSS

=cut

sub _create_and_push_file_scan_queue_item {
	my $self = shift;
    my $filestore_row = shift;

    my $qrs = $self->result_source->schema->resultset('Queue');

    my $item = $qrs->create_item('scan_file', {
        label => 'Virus en malware scan',
        metadata => {
            target => 'virus_scanner',
        },
        data => {
            status => 'waiting',
            ignore_existing => 1,
            parameters => {
                file_id => $filestore_row->uuid
            }
        }
    });

    $qrs->queue_item($item);
    $item->delete;

    return;
}

=pod TODO Zaaksysteem::Service::Client::VirusScanner

has vss_client (
    builder => 1
);

sub _build_vss {
    return Zaaksysteem::Service::Client::VirusScanner->new(
        host => $instance_host,
        filestore_template => 'http://{host}/api/v1/file/{identifier}/download', # default from config
    );
}

my @viruses = $self->vss_client->scan_for_viruses( {identifier => $identifier} ) or die "oops!";

etc...

=cut

use URI::Template;
use JSON::XS;
use BTTW::Tools::UA;
#
# until we have a client ZS::Service::Client::VSS

sub _virus_scan_service_client_viruses {
    my $self = shift;
    my $identifier = shift;

    my $ua = $self->_virus_scan_service_useragent();
    my $request  = $self->_virus_scan_service_request($identifier);
    $self->log->debug( $request->as_string );
    my $response = $ua->request($request);
    $self->log->debug( $response->as_string );

    throw(
        "filestore/virus_scan_service_client_viruses/request_error",
        "Service responded with: " . $response->status_line
    ) unless $response->is_success;

    return $self->_virus_scan_service_response($response)->{ viruses };
}

sub _instance_hostname {
    my $self = shift;
    my $config = $self->result_source->schema->default_resultset_attributes->{ config };
    my $value = $config->{ instance_hostname };
    return $value;
}

sub _virus_scan_service_host {
    my $self = shift;
    my $config = $self->result_source->schema->default_resultset_attributes->{ config };
    my $value = $config->{ virus_scan_service_host }
        || $ENV{ VIRUS_SCAN_SERVICE_HOST };
    return $value;
}

sub _virus_scan_service_request {
    my $self = shift;
    my $identifier = shift;
    my $uri = $self->_virus_scan_service_request_uri();
    my $instance_hostname = $self->_instance_hostname();

    return HTTP::Request->new( GET => $uri->as_string,
        [
            'Content-Type' => 'application/json',
            'Accept'       => 'application/json',
        ],
        encode_json(
            {
                file_id           => $identifier,
                instance_hostname => $instance_hostname,
                mode              => 'sync',
            }
        )
    )

}

sub _virus_scan_service_request_uri {
    my $self = shift;
    my $host     = $self->_virus_scan_service_host();
    my $template = $self->_virus_scan_service_template();
    my $uri_tmpl = URI::Template->new($template);
    return $uri_tmpl->process( virus_scan_service_host => $host )
}

sub _virus_scan_service_response {
    my $self = shift;
    my $response = shift;
    my $virus_scan_response = decode_json($response->content);
    return $virus_scan_response->{ instance };

}

sub _virus_scan_service_template {
    my $self = shift;
    my $config = $self->result_source->schema->default_resultset_attributes->{ config };
    my $value = $config->{ virus_scan_service_template }
        || $ENV{ VIRUS_SCAN_SERVICE_TEMPLATE }
        || 'http://{+virus_scan_service_host}/filestore';
    return $value;
}

# this is easy and safe to mock! ... if only we could Test::MockModule :-(
sub _virus_scan_service_useragent {
    return BTTW::Tools::UA::new_user_agent( protocols_allowed => [ 'http', 'https' ] );
}

=head2 find_by_uuid

$result_source->find_by_uuid($uuid)

returns the firts result_row that search can find with given UUID

=cut

sub find_by_uuid {
    shift->search({ uuid => shift })->first;
}

=head2 _archivable_format($file_path, $force_mimetype)

Returns a L<File::ArchivableFormat> info object()

Unless C<$force_mimetype> is provided, it will try to take it from C<$file_path>

=cut

sub _archivable_format {
    my ($file_path, $force_mimetype) = @_;

    my $file_af = File::ArchivableFormats->new();
    my $af_info = $force_mimetype # XXX We no longer use $force_mimetype
        ? $file_af->identify_from_mimetype($force_mimetype)
        : $file_af->identify_from_path("$file_path");

    return $af_info;
}
#
# TODO: fix $force_mimetype here and all the way up and maybe tombstone it first

=head1 DBIX::Class::ResultSet METHODS

=head2 search_rs

restrict search and find methods commonly lnown in DBIx::Class to only return
results that have been scanned and found to be 'ok'.

limit results of any search or find to only return those ResultRows that have
been check and infected. Before the refactor of filestore_create() and Virus
Scan Service, only those files being uploaded that were not contaminated were
being inserted in the table. To be sure that we are not going to bubble up un-
wanted results, search_rs will only pass those that have been marked 'ok'

DBIx::Class internal search and/or find all call search_rs internally. Because
of the way DBIx::Class uses Class::C3, we can call next::method to actually
get the normal behaviour.

=cut

sub search_rs{
    my $self = shift;
    return $self
        ->next::method( @_ )
        ->next::method( { virus_scan_status => { -not_like => 'found%' } } )
}
#
# TODO: write search_any that does not have this build in filter.

=head2 repush_pending_file_scan_queue_items

this will be called by Zaaksysteem::Controller::Schedule and call our internal
_create_and_push_file_scan_queue_item to push the pending virusscan jobs again.

It has built in throtteling, no more queue items will be added when either the
maximum number has exceeded or the maximum total number of bytes for this period

=head3 args

=over

=item max_items

maximum number of items to add back to the queue

=item max_stream

maximum througput for rescanning, this should not be to high, so it will not
suffocate the virus-scan-service for normal jobs

=item min_period

the time in seconds between each run

=back

=cut



define_profile 'repush_pending_file_scan_queue_items' => (
    optional => {
        max_items  => 'Int',
        max_stream => 'Num',
        min_period => 'Num',
    },
    defaults => {
        max_items  => $MAX_ITEMS,
        max_stream => $MAX_STREAM,
        min_period => $MIN_PERIOD,
    }
);

sub repush_pending_file_scan_queue_items{
    my $self = shift;
    my $opts = shift // {} ;
    my $args = assert_profile($opts)->valid;
    my $max_items = $args->{max_items};
    my $max_total = $args->{max_stream} * $args->{min_period};

    my $pending_rows = $self->search(
        {
            virus_scan_status => 'pending',
            date_created => {
                '>=' => $self->result_source->schema->format_datetime_object(
                    DateTime->now->subtract(days => 1)
                )
            }
        },
        { order_by => 'date_created'}
    );
    return unless $pending_rows->count;

    my $items = 0;
    my $total = 0;

FILESTORE_ROW:
    while ( my $filestore_row = $pending_rows->next ) {
        $self->_create_and_push_file_scan_queue_item($filestore_row);

        $items += 1;
        $total += $filestore_row->size;

        last FILESTORE_ROW if $items >= $max_items;
        last FILESTORE_ROW if $total >= $max_total;
    }

    $self->log->info(
        sprintf(
            "Repush pending file_scan queue_items %d/%d total size: %d",
            $items,
            $args->{max_items},
            $total,
        )
    );

    return
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
