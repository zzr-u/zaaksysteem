package Zaaksysteem::Backend::Roles::ResultSet;

use Moose;

use BTTW::Tools;

extends 'Zaaksysteem::Backend::ResultSet';

=head1 NAME

Zaaksysteem::Backend::Roles::ResultSet - Convenient functions for Roles

=head1 SYNOPSIS

    # See USAGE tests:
    # TEST_METHOD="roles_usage.*" ./zs_prove -v t/testclass/100-general.t


=head1 DESCRIPTION

Roles within zaaksysteem functions

=head1 REQUIRED ATTRIBUTES

=head1 METHODS

=head2 create_role

Arguments: \%options

Return value: $COMPONENT_ROLE (Component of Group)

    my $role = $self->schema->resultset('Roles')->create_role(
        {
            name            => "Behandelaar",
            description     => "Standaard behandelaar",
        }
    );

    my $role = $self->schema->resultset('Roles')->create_role(
        {
            name            => "Afdelingshoofd",
            description     => "Standaard afdelingshoofd",
        }
    );




Create a role in our database.

B<Options>

=over 4

=item name [required]

isa: Str

Short name of the role, e.g. C<Behandelaar>

=item description

isa: Str

Description of the role, e.g. C<Standaard zaaksysteem user>

=item parent_group_id

isa: Number

Id of the parent_group to place this role in.

=item system_role

isa: Boolean

Defines whether this is a system role, Like the "Administrator"
role which needs to be present for correct functioning of zaaksysteem.nl

=back

=cut

define_profile create_role     => (
    required            => [qw/name/],
    optional            => [qw/description parent_group_id system_role override_id/],
    constraint_methods  => {
        parent_group_id     => qr/^\d+$/,
        system_role         => qr/^[01]$/,
        override_id         => qr/^\d+$/,
    }
);

sub create_role {
    my $self            = shift;
    my $params          = assert_profile(shift || {})->valid;
    my $schema          = $self->result_source->schema;

    my @cols            = qw/description name parent_group_id system_role/;

    ### TODO
    my @parent_ids;
    if ($params->{parent_group_id}) {
        my $parent = $schema->resultset('Groups')->find($params->{parent_group_id});

        throw(
            'roles/create_role/invalid_parent_group_id',
            'Cannot find group row by given id: ' . $params->{parent_group_id}
        ) unless $parent;
    }

    my $idstring    = $params->{override_id} if $params->{override_id};

    my $role        = $self->create(
        {
            $params->{override_id} ? (id => \$idstring) : (),
            name            => $params->{name},
            description     => $params->{description},
            system_role     => ($params->{system_role} ? 1 : 0),
            $params->{parent_group_id} ? (parent_group_id => $params->{parent_group_id}) : ()
        }
    );


    if ($role) {
        if ($params->{override_id}) {
            ### Make sure any counters get reset to correct value
            $self->result_source->schema->storage->dbh_do(
                sub {
                    my ($storage, $dbh) = @_;
                    $dbh->do("SELECT setval('roles_id_seq', (SELECT MAX(id) FROM roles));");
                }
            );
        }

        return $role->discard_changes();
    } else {
        return;
    }
}

=head2 set_roles_on_subject

Arguments: $COMPONENT_SUBJECT, \@ARRAYREF_OF_ROLE_IDS

Return value: $SUBJECT_COMPONENT

    $groups->set_roles_on_subject($subject, [$schema->resultset('Roles')->first->id]);

Sets the roles for this user.

=cut

sub set_roles_on_subject {
    my $self            = shift;
    my $subject         = shift;
    my @roles           = @{ shift() };

    my $schema          = $self->result_source->schema;


    throw(
        'roles/set_roles_on_subject/not_a_number',
        'Only role IDs allowed'
    ) for grep { $_ !~ /^\d+$/ } @roles;

    my $count           = $schema->resultset('Roles')->search(
        {
            id  => \@roles
        }
    )->count;

    if ($count != scalar @roles) {
        throw(
            'roles/set_roles_on_subject/roles_not_found',
            'One or more given role ids not found in database'
        );
    }

    $subject->role_ids(\@roles);
    $subject->update;
    return $subject->discard_changes;
}

=head2 roles_for_subject

Arguments: $COMPONENT_SUBJECT

Return value: \%RETURN VALUE

    my $roles = $groups->roles_for_subject($subject);

    # Prints
    [
        REF:Zaaksysteem::Schema::Roles,
        REF:Zaaksysteem::Schema::Roles,
        [...]
    ];

Returns an arrayref containing the roles for this user

=cut

sub roles_for_subject {
    my $self            = shift;
    my $subject         = shift;

    return [] unless $subject->role_ids && @{ $subject->role_ids };

    my @roles           = $self->search(
        { id  => $subject->role_ids },
        { order_by => 'name' }
    )->all;

    return \@roles;
}


=head2 get_all_cached

Arguments: [ $STASH ]

    my $stash   = $c->stash

    my $groups  = $schema->resultset('Rikes')->get_all_cached($stash);

Will retrieve _all_ roles, when stash is given, it will make sure it will load
only once, and set it on the stash key C<__cache_roles_all>. When it is already
set, it will return the value without a call to the database

TODO TESTS

=cut

sub get_all_cached {
    my $self        = shift;
    my $hash        = shift;

    return $hash->{__cache_roles_all} if $hash->{__cache_roles_all};

    $hash->{__cache_roles_all} = [
        $self->search({},
            { order_by => { '-asc' => 'me.name' } })
            ->all
        ];
    return $hash->{__cache_roles_all}
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
