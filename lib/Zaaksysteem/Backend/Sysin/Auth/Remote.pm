package Zaaksysteem::Backend::Sysin::Auth::Remote;
use Moose;
use namespace::autoclean;

use BTTW::Tools;
use BTTW::Tools::UA;
use Zaaksysteem::Constants qw(LOGGING_COMPONENT_USER);
use Zaaksysteem::Types qw(FQDN NonEmptyStr);
use PerlX::Maybe::XS qw(provided);

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Backend::Sysin::Auth::Remote - Model for requesting authentication from a remote Zaaksysteem

=head1 DESCRIPTION

This model handles calling a remote Zaaksysteem to retrieve a login link for an
optionally specified user using the platform key.

=head1 SYNOPSIS

    use Zaaksysteem::Backend::Sysin::Auth::Remote;

    my $model = Zaaksysteem::Backend::Sysin::Auth::Remote->new(
        logging_rs => $schema->resultset('Logging')
    );

    $model->request_login_url($instance_object, $subject_object)

=head1 ATTRIBUTES

=head2 hostname

The hostname of the current instance. Will be used to create a "remote
username" for logging in the target system.

=cut

has hostname => (
    is       => 'ro',
    isa      => FQDN,
    required => 1,
);

=head2 server_ca

Path to a file containing the CA certificate(s) for the "services" hosts used
to request login tokens.

=cut

has server_ca => (
    is        => 'ro',
    isa       => NonEmptyStr,
    required  => 0,
    predicate => 'has_server_ca',
);

=head2 platform_keys

Hash reference containing the platform keys, one per platform.

=cut

has platform_keys => (
    is       => 'ro',
    isa      => 'HashRef',
    required => 0,
    default  => sub { {} },
);

=head2 logging_rs

A resultset that will be used to log the request of the authentication URL.

=cut

has logging_rs => (
    is       => 'ro',
    isa      => 'Zaaksysteem::DB::ResultSet::Logging',
    required => 1,
);

=head2 ua

A HTTP useragent. Defaults to a new useragent object created by L<BTTW::Tools::UA>.

=cut

has ua => (
    is       => 'ro',
    required => 0,
    lazy     => 1,
    builder  => '_build_ua',
);

=head1 METHODS

=head2 request_login_url

=over

=item * $instance

A L<Zaaksysteem::Object::Types::Instance> instance for the instance the user wants to log in on.

=item * $subject

A L<Zaaksysteem::Backend::Subject::Component> instance, representing the user trying to get access to
C<$instance>.

=back

=cut

define_profile request_login_url => (
    required => {
        instance => 'Zaaksysteem::Object::Types::Instance',
        user     => 'Zaaksysteem::Backend::Subject::Component',
    },
    optional => { host => 'Zaaksysteem::Object::Types::Host', },
);


sub request_login_url {
    my $self = shift;
    my $args = assert_profile({@_})->valid;

    my $instance = $args->{instance};
    throw(
        'auth/remote/platform_key/not_found',
        sprintf("No platform key found for platform '%s'", $instance->customer_type),
    ) unless exists $self->platform_keys->{ $instance->customer_type };

    throw(
        'auth/remote/instance/inactive',
        "Instance is inactive, unable to login",
        { http_code => '409' },
    ) if $instance->disabled;

    my $subject  = $args->{user};
    my $host     = $args->{host};

    my $token_generation_url = _build_token_generation_url($host ? $host : $instance);
    my $remote_user          = _build_remote_user($subject, $self->hostname);

    $self->log->debug("Requesting login token from '$token_generation_url' for '$remote_user'");

    my $res = $self->ua->post(
        $token_generation_url,
        'ZS-Platform-Key' => $self->platform_keys->{ $instance->customer_type },
        'Content' => {
            remote_user => $remote_user,
        },
    );

    throw(
        'auth/remote/request_error',
        sprintf(
            "Error requesting authentication token: %s; request-id: '%s'",
            $res->status_line,
            $res->header('zs-req-id') // '<unknown>',
        )
    ) unless $res->is_success;

    $self->logging_rs->trigger(
        'auth/login/token_request',
        {
            'component' => LOGGING_COMPONENT_USER,
            'data' => {
                subject_id       => $subject->id,
                subject_username => $subject->username,
                instance_id      => $instance->id,
                instance_fqdn    => $instance->fqdn,

                defined $host ? (
                    host_id   => $host->id,
                    host_fqdn => $host->fqdn
                ) : ()
            },
        },
    );

    my $response = JSON::XS->new->utf8->decode($res->decoded_content);
    my $uri = URI->new($response->{login_url});
    $uri->host($host ? $host->fqdn : $instance->fqdn);
    return $uri;
}

sub _build_token_generation_url {
    my $instance = shift;

    return sprintf(
        "https://%s/auth/token/generate",
        $instance->fqdn,
    );
}

sub _build_remote_user {
    my $subject = shift;
    my $hostname = shift;

    return sprintf(
        "%s (%s@%s)",
        $subject->display_name,
        $subject->username,
        $hostname,
    );
}

sub _build_ua {
    my $self = shift;

    return new_user_agent(
        provided $self->has_server_ca, ca_cert => $self->server_ca,
    );
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
