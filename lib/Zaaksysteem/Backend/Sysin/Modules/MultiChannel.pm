package Zaaksysteem::Backend::Sysin::Modules::MultiChannel;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::MultiChannel - Integration module for
DataB's MultiChannel platform

=head1 DESCRIPTION

System integration for the DataB MultiChannel 'request session for some user'
functionality. This allows Zaaksysteem code to setup a remote session for the
user, and invite the user to open the MultiChannel application while avoiding
a secondary login.

=cut

use BTTW::Tools;
use Zaaksysteem::Constants qw[
    BETROKKENE_TYPE_NATUURLIJK_PERSOON
    BETROKKENE_TYPE_BEDRIJF
];
use Zaaksysteem::External::DataB;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

=head1 CONSTANTS

=head2 INTERFACE_ID

Static string with the module's name (C<multichannel)>.

=cut

use constant INTERFACE_ID => 'multichannel';

=head2 INTERFACE_DESCRIPTION

End-user description of the integration.

=cut

use constant INTERFACE_DESCRIPTION => q{
<p>
    Deze koppeling integreert DataB's MultiChannel oplossing voor
    documentdistributie. De werking is beperkt tot MultiChannel's sessie
    deling, waarmee een betrokkene op de PIP automatisch kan inloggen op de
    MultiChannel applicatie, en daar documenten kan terugvinden.

    DataB heeft MultiChannel zo ingericht dat op meerdere identificerende
    kenmerken van een betrokkene dit verzoek ingeschoten kan worden. Voordat
    deze koppeling ingericht kan worden moet nagevraagd worden op welk kenmerk
    DataB de koppeling ingericht heeft (gebruikelijk op basis van
    burgerservicenummer of vestigingsnummer).
</p>
};

=head2 INTERFACE_CONFIG_FIELDS

C<ArrayRef[Zaaksysteem::ZAPI::Form::Field]>

=cut

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint',
        type => 'text',
        label => 'URL MultiChannel Documentserver',
        required => 1,
        description => 'Configureer de endpoint URL voor de documentserver'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_cid',
        type => 'text',
        label => 'DataB CID',
        required => 1,
        description => 'Klantcode voor het gebruik van de DataB API'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_secret',
        type => 'text',
        label => 'DataB sleutel',
        required => 1,
        description => 'De geheime sleutel om veilig met DataB te communiceren'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_subject_type',
        type => 'select',
        label => 'Betrokkene type',
        required => 1,
        description => 'Selecteer voor welk type aanvrager deze koppeling bedoeld is',
        default => BETROKKENE_TYPE_NATUURLIJK_PERSOON,
        data => {
            options => [
                { value => BETROKKENE_TYPE_NATUURLIJK_PERSOON,  label => 'Natuurlijke personen' },
                { value => BETROKKENE_TYPE_BEDRIJF,             label => 'Niet-natuurlijk personen' },
            ],
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_subject_magic_string',
        type => 'text',
        label => 'Betrokkene-identificatie (magicstring)',
        required => 1,
        description => 'DataB accepteerd meerdere soorten identificerende kenmerken, afhankelijk van de inrichting buiten Zaaksysteem. Geef in dit veld op welke magicstring van de betrokkene gebruikt moet worden om met DataB te communiceren',
        data => {
            placeholder => 'Bijvoorbeeld [[ burgerservicenummer ]], [[ vestigingsnummer ]], etc'
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_explanation',
        type => 'richtext',
        label => 'WOZ tekst en uitleg',
        required => 0,
        description => 'Vul hier de tekst in die burgers krijgen te zien op de WOZ pagina'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_testmode',
        type => 'checkbox',
        label => 'Spoofmodus',
        required => 0,
        description => 'Indien actief, stelt de gebruiker in staat de inrichting van de koppeling te controleren zonder gebruik van DataB\' servers.'
    ),
];

=head2 MODULE_SETTINGS

Static hash of configuration for this module.

=cut

use constant MODULE_SETTINGS => {
    name                            => INTERFACE_ID,
    label                           => 'DataB MultiChannel DMS',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'outgoing',
    manual_type                     => [],
    is_multiple                     => 0,
    is_manual                       => 0,
    description                     => INTERFACE_DESCRIPTION,
    retry_on_error                  => 0,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    trigger_definition  => {
        get_session_url => { method => 'get_session_url', update => 1 }
    },
    test_interface                  => 1,
    test_definition                 => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u de verbinding
            van uw profiel.
        },

        tests => [
            {
                id => 1,
                label => 'Test verbinding',
                name => 'connection_test',
                method => 'test_connection',
                description => 'Test verbinding naar profiel URL'
            }
        ],
    },
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 TRIGGERS

=head2 get_session_url

The main trigger for this module, requests a new session URL from the
document server and returns it.

=head3 Usage

    my $interface = ...;    # The 'multichannel' interface component
    my $subject = ...;      # Betrokkene object
    my $useragent = ...;    # DataB does some sanity checks using the useragent string of the client

    my $url = $interface->process_trigger('get_session_url', {
        subject => $subject,
        useragent => $useragent_string,
    });

=head3 Exceptions

=over 4

=item sysin/multichannel/request_failed

Failure to connect to the MultiChannel Documentserver

=item sysin/multichannel/request_unsuccessful

MultiChannel Documentserver responded with an unsuccessful status

=back

=cut

sub get_session_url {
    my ($self, $params, $interface) = @_;

    return $interface->model->get_session_url($params);
}

=head2 test_connection

Hook for the Sysin function testing interface. This method will attempt to
connect to the given endpoint. Checks are do to ensure the endpoint is SSL
encryted.

=cut

sub test_connection {
    my ($self, $interface) = @_;

    return $self->test_host_port_ssl($interface->jpath('$.endpoint'));
}

sub _get_model {
    my ($self, $opts) = @_;

    my $interface = $opts->{interface};
    my $config    = $interface->get_interface_config;

    return Zaaksysteem::External::DataB->new(
        cid => $config->{ cid },
        secret => $config->{ secret },
        endpoint => $config->{ endpoint },
        subject_magic_string => $config->{ subject_magic_string },
        spoof_mode => $config->{ testmode }
    );
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
