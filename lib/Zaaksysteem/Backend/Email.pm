package Zaaksysteem::Backend::Email;
use Moose;

use Email::Valid;
use Email::Address;
use Encode qw(encode_utf8);
use IO::All;
use List::MoreUtils qw/all/;
use Email::Sender::Transport::SMTP;
use Mail::DKIM::Signer;
use Mail::Track;
use BTTW::Tools;
use Zaaksysteem::Object::Model;

with 'MooseX::Log::Log4perl';

=head2 case

The case from which the email is sent, used for retrieval of magic string
properties.

=cut

has case  => (
    is       => 'ro',
    required => 1,
    isa      => 'Zaaksysteem::Schema::Zaak',
);

has schema => (
    is => 'ro',
    isa => 'Zaaksysteem::Schema',
    lazy => 1,
    default => sub {
        my $self = shift;
        return $self->case->result_source->schema;
    }
);

=head2 additional_ztt_context

Used to provide an extra context for magic string retrieval. ZTT will
also source magic strings from this context if present.

=cut

has additional_ztt_context => (
    is => 'rw',
);

=head2 send_case_notification

Extract emails parameters from notifications and defer to send_from_case

=cut

define_profile send_case_notification => (
    required => [qw/notification recipient/],
    optional => [qw/attachments cc bcc additional_ztt_context/],
    typed => {
        recipient => 'Str',
        notification => 'Zaaksysteem::Model::DB::BibliotheekNotificaties'
    }
);

sub send_case_notification {
    my $self = shift;

    my $params = assert_profile(shift)->valid;

    my $notification = $params->{notification};

    return $self->send_from_case({
        recipient      => $params->{recipient},
        subject        => $notification->subject,
        body           => $notification->message,
        attachments    => $params->{attachments},
        sender_address => $notification->sender_address,
        sender         => $notification->sender,
        cc             => $params->{cc},
        bcc            => $params->{bcc},
        log_error      => 1,
        $params->{additional_ztt_context} ? (
            additional_ztt_context => $params->{additional_ztt_context},
        ) : (),
    });
}

=head2 sender

Determine the sender, varying from

=head3 Arguments

=over

=item sender [optional]

The name of the email sender, e.g. 'Internal Affairs Department'

=item from [optional]

The email-address of the sender, e.g. internal.affairs@shield.org

If empty, the default configured from address will be used.

=back

=cut

define_profile sender => (
    optional => [qw/sender_address sender/]
);

sub sender {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $from = defined $params->{sender_address} ? $self->replace_magic_strings($params->{sender_address}) : undef;
    $from ||= $self->schema->resultset('Config')->get_customer_config->{zaak_email};

    my $sender = $params->{sender} && $self->replace_magic_strings($params->{sender});
    my $email = Email::Address->new($sender, $from);

    return $email->format if $self->schema->is_allowed_mailhost($email->host);
    throw(
        "zaaksysteem/email/not_allowed",
        $email->host . " is not allowed to send e-mail"
    );

}

=head2 send_from_case

Send email based on a slew of case settings.

=cut

define_profile send_from_case => (
    required => [qw/body subject recipient/],
    optional => [
        qw/
            additional_ztt_context
            attachments
            bcc
            cc
            contactmoment
            file_attachments
            sender
            sender_address
            log_error
            sender_subject
            request_id
            /
    ],
    typed => {
        body    => 'Str',
        subject => 'Str',
        sender_subject => 'Zaaksysteem::Schema::Subject',
    },
    defaults           => { contactmoment => 'balie', log_error => 1 },
    constraint_methods => {
        recipient => sub {
            my ($dfv, $value) = @_;

            my @emails = split /[;,]/, $value;
            return all { Email::Valid->address($_) } @emails;
            }
    },
);

sub send_from_case {
    my $self = shift;
    my $params  = assert_profile(shift)->valid;

    my $case         = $self->case;
    my $schema       = $self->schema;

    $self->additional_ztt_context($params->{additional_ztt_context});

    my $body         = $self->replace_magic_strings($params->{body});
    my $subject      = $self->replace_magic_strings($params->{subject});

    my $interface = $schema->resultset('Interface')->search_active({module => 'emailconfiguration'})->first;

    # For now be backward compatible
    my $mt;
    my $transport;
    my ($dkim, $dkim_key_file);
    my $max_size = 10;
    if ($interface) {
        my $config = $interface->get_interface_config;
        my $object_data = $case->object_data();
        my $id = join("-", $case->id, substr($object_data->uuid, -6));
        my $regexp = qr/(\d+-[a-z0-9]{6})/;

        $max_size = $config->{max_size} || 10;
        if ($config->{use_smarthost}) {
            $transport = $self->_create_smtp_transport($config);
        }

        if ($config->{use_dkim}) {
            my $dkim_selector;
            if ($config->{dkim_key}[0]{id}) {
                $self->log->trace("Creating DKIM signing object from filestore");

                my $keyfile = $schema->resultset('Filestore')->find($config->{dkim_key}[0]{id});
                $dkim_key_file = $keyfile->get_path;
                $dkim_selector = $config->{dkim_selector};
            } else {
                $self->log->trace("Creating DKIM signing object from environment");

                $dkim_key_file = $ENV{DKIM_KEY_FILE};
                $dkim_selector = $ENV{DKIM_CURRENT_SELECTOR};
            }

            unless ($dkim_key_file && $dkim_selector) {
                my $error = "DKIM enabled, but no DKIM key or selector configured.";
                $self->log->error($error);

                throw(
                    "case/mail/dkim_settings",
                    $error,
                );
            }

            try {
                $dkim = Mail::DKIM::Signer->new(
                    Algorithm => $config->{dkim_algorithm} // 'rsa-sha256',
                    Method    => $config->{dkim_method} // 'relaxed',
                    Domain    => $config->{dkim_domain},
                    Selector  => $dkim_selector,
                    KeyFile   => $dkim_key_file,
                );
            }
            catch {
                $self->log->error("Error while sending email: $_");

                throw(
                    "case/email/dkim_signer",
                    'Interne fout bij het versturen van email (DKIM signer)',
                );
            };
        }

        $mt = Mail::Track->new(
            subject_prefix_name  => $config->{subject},
            identifier           => $id,
            identifier_regex     => $regexp,
        );

        if (!defined $params->{sender_address}) {
            $params->{sender_address} = $config->{api_user};
        }
        if (!defined $params->{sender} && $config->{sender_name}) {
            $params->{sender} = $config->{sender_name};
        }
    }
    else {
        $mt = Mail::Track->new();
    }

    my $from = $self->sender($params);

    $self->log->debug(sprintf(
        "Sending email. Sender: '%s'; Recipient: '%s'",
        $from,
        $params->{recipient},
    ));

    my $msg = $mt->prepare({
        from    => $from,
        to      => $params->{recipient},
        subject => $subject,
        cc      => $params->{cc},
        bcc     => $params->{bcc},

        extra_headers => {
            'X-ZS-Request' => $params->{request_id} // 'unknown',
        },

        # Pass in a Mail::DKIM::Signer
        ($dkim)
            ? (dkim => $dkim)
            : (),

        # Override SMTP settings, if requested in interface configuration
        (defined $transport)
            ? (transport => $transport)
            : (),
    });

    $msg->add_body({content => encode_utf8($body)});

    # Case type documents
    my @attached_files = $params->{attachments} ? $self->_get_files($params->{attachments}) : ();
    my %has_files;

    $self->log->debug("Number of attached files: " . @attached_files);
    foreach (@attached_files) {
        $has_files{ $_->filestore->md5 } = $_->filestore->get_path;

        $msg->add_attachment(
            filename     => $_->filename,
            content_type => $_->filestore->mimetype,
            path         => $has_files{ $_->filestore->md5 },
        );

        $self->log->debug("Added attachment through template: " . $_->filename);
    }

    # Individual files (selected on the document tab etc.)
    if ($params->{file_attachments}) {
        $self->log->debug("Number of specific file attachments: " . @{ $params->{file_attachments} });

        my $files = $schema->resultset('File')->search({id => $params->{file_attachments}});
        while (my $f = $files->next) {
            # Prevent dupes
            next if exists $has_files{ $f->filestore->md5 };

            $has_files{ $f->filestore->md5 } = $f->filestore->get_path;

            $msg->add_attachment(
                filename     => $f->filename,
                content_type => $f->filestore->mimetype,
                path         => $has_files{ $f->filestore->md5 },
            );

            push(@attached_files, $f);

            $self->log->debug("Added specific file attachment: " . $f->filename);
        }
    } else {
        $self->log->debug("No specific file attachments specified.");
    }

    my $size = $msg->size;

    if (!$subject) {
        $self->_log_or_throw(
            {
                log_error => $params->{log_error},
                type      => 'subject',
                message   => "Er is geen onderwerp gevonden",
                subject   => '<geen ingevuld onderwerp>',
            }
        );
        return 0;
    }
    elsif (!$body) {
        $self->_log_or_throw(
            {
                log_error => $params->{log_error},
                type      => 'body',
                message   => "Er is geen mailinhoud gevonden",
                subject   => $subject,
            }
        );
        return 0;
    }
    elsif ($size >= ($max_size * (1000 * 1000))) {
        $self->_log_or_throw(
            {
                log_error => $params->{log_error},
                type      => 'size',
                subject   => $subject,
                message   => sprintf(
                    'De maximale grootte van de mail is overschreden (%.2fMB / Max: %dMB)',
                    ($size / (1000 * 1000)), $max_size
                ),
            }
        );
        return 0;
    }

    # You shouldn't mess with the internals..
    my $msg_subject = $msg->_build_subject;

    my $ok = try {
        $msg->send;
        return 1;
    }
    catch {
         $self->_log_or_throw(
            {
                log_error => 1,
                type      => 'send',
                message   => $_,
                subject   => $msg_subject,
            }
         );
         return 0;
    };
    if ($ok) {
        my $betrokkene_id = $self->get_betrokkene_id($params->{sender_subject});
        $self->schema->resultset('Contactmoment')->contactmoment_create(
            {
                type       => 'email',
                subject_id => $betrokkene_id,
                subject    => $params->{sender_subject},
                created_by => $betrokkene_id,
                medium     => $params->{contactmoment},
                case_id    => $self->case->id,
                email      => {
                    body      => $body,
                    subject   => $msg_subject,
                    recipient => $params->{recipient},
                    cc        => $params->{cc},
                    bcc       => $params->{bcc},
                    from      => $from,
                    attachments =>
                        [$self->_log_attachments(@attached_files)],
                }
        });
    }
}

sub _create_smtp_transport {
    my $self = shift;
    my ($config) = @_;

    $self->log->debug(sprintf(
        "Creating SMTP transport for host:port '%s:%d', username '%s' (%s)",
        $config->{smarthost_hostname},
        $config->{smarthost_port},
        ($config->{smarthost_username} ? $config->{smarthost_username} : 'without username'),
        ($config->{smarthost_password} ? "with password" : "without password")
    ));

    my %smtp_options = (
        ssl => 'starttls',
        ssl_options => {
            # Use system-default SSL
            SSL_ca_path => '/etc/ssl/certs'
        },
        host => $config->{smarthost_hostname},
        port => $config->{smarthost_port},
    );

    if ($config->{smarthost_username} && $config->{smarthost_password}) {
        $smtp_options{sasl_username} = $config->{smarthost_username};
        $smtp_options{sasl_password} = $config->{smarthost_password};
    }

    return Email::Sender::Transport::SMTP->new(%smtp_options);
}

sub _log_or_throw {
    my $self = shift;
    my $params = shift;

    $self->log->error("Error while sending mail: $params->{message}");
    if ($params->{log_error}) {
        my $logging = $self->schema->resultset('Logging');
        $logging->trigger(
            'case/email', {
            component    => 'case',
            component_id => $self->case->id,
            zaak_id      => $self->case->id,
            data         => {
                destination => "mail",
                subject => $params->{subject},
                message => $params->{message},
                error   => 1,
            }
        });
        return;
    }
    throw("case/email/$params->{type}",  $params->{message});
}

=head2 get_betrokkene_id

When the email is sent by a logged in user, return that. Otherwise return
the case requestor.

=cut

sub get_betrokkene_id {
    my ($self, $current_user) = @_;

    $current_user //= $self->case->result_source->resultset->{attrs}{current_user};

    return $current_user ?
        'betrokkene-medewerker-' . $current_user->uidnumber :
        $self->case->aanvrager_object->rt_setup_identifier;
}

=head2 replace_magic_strings

Interpolate magic strings like [[this_is_a_magic_string]]
in the given string, using information from the case.

=cut

sub replace_magic_strings {
    my ($self, $body) = @_;

    throw 'email/replace_magic_strings/empty_body', 'Geen bericht aangeboden',
        unless defined $body;

    my $ztt = Zaaksysteem::ZTT->new;

    $ztt->add_context($self->case);

    $ztt->add_context($self->additional_ztt_context)
        if $self->additional_ztt_context;

    return $ztt->process_template($body)->string;
}


=head2 _process_mail

DEPRECATED! Will be removed ASAP.

Generate an Email::MIME message and pass to the supplied email-sender.

Either supply a body or parts array, the latter is to accomodate for
attachments. This requires a multi-part format.

    $self->_process_mail({
        from => 'servicedesk@mintlab.nl',
        to => 'burger@gemeente.nl',
        subject => 'Melding over uw zaak',
        body => 'Uw zaak loopt nog.'
    });

    $self->_process_mail({
        from => 'servicedesk@mintlab.nl',
        to => 'burger@gemeente.nl',
        subject => 'Melding over uw zaak',
        parts => [
            Email::Mime->new(%body_params),
            Email::Mime->new(%attachment1_params),
            Email::Mime->new(%attachment2_params)
        ],
    });

=cut

define_profile _process_mail => (
    required => [qw/from to subject/],
    optional => [qw/parts body cc bcc/]
);

sub _process_mail {
    my $self = shift;

    my @caller = caller(1);
    printf STDERR ("Deprecated: %s called %s->_process_mail. Please use other methods\n", $caller[3], __PACKAGE__);

    my $email = assert_profile(shift)->valid;

    my %mime = (
        header => [
            To => $email->{to},
            Cc => $email->{cc},
            From => $email->{from},
            Subject => $email->{subject},
        ],
        attributes => {
            charset => 'utf-8',
            content_type => 'text/plain'
        }
    );

    if ($email->{parts}) {
        $mime{parts} = $email->{parts};
    }
    elsif ($email->{body}) {
        $mime{body} = $email->{body};
    } else {
        throw('mail/missing_body_or_parts',
            "Can't send email without parts or body, check stash");
    }

    my $message = Email::MIME->create(%mime)
        or throw("mail/error", "Unable to create message");

    my $transport = Email::Sender::Transport::SMTP->new;

    $self->email_sender->send($message, {
        transport => $transport,
    }) or throw('mail/error', "E-mail kon niet worden verstuurd");

    # send a copy to bcc addresses.
    # emails are sent to the address on the envelope, not to anything that's
    # in the headers. so we swap out the envelop address and resend.
    # this way the bcc recipient will see the original headers.
    if ($email->{bcc}) {
        my $bcc = ref $email->{bcc} eq 'ARRAY' ? $email->{bcc} : [$email->{bcc}];

        $self->email_sender->send($message, {
            to => $email->{bcc},
            transport => $transport,
        }) or throw('mail/error', "E-mail kon niet worden verstuurd");
    }
}

=head2 retrieve_attachments

Given a list of file objects, retrieve the actual files and create Email::MIME
objects.

=cut

sub _retrieve_attachments {
    my ($self, @attached_files) = @_;

    return map { Email::MIME->create(
            attributes => {
                filename     => $_->filename,
                content_type => $_->filestore->mimetype,
                encoding     => 'base64',
                disposition  => 'attachment',
                name         => $_->filename,
            },
            body => io($_->filestore->get_path),
        )
    } @attached_files;
}

=head2 _log_attachments

Transform a list of attached files into a format that will fit into
what contactmoment_create expects.

=cut

sub _log_attachments {
    my ($self, @attached_files) = @_;

    return map {
        {
            filename       => $_->filename,
            file_id        => $_->id,
            email_filename => $_->filename,
        }
    } @attached_files;
}

=head2 _get_files

Retrieve file objects given a list of zaaktype_kenmerken_ids.

=cut

sub _get_files {
    my ($self, $zaaktype_kenmerken_ids) = @_;

    throw('email/get_files', 'Systeemfout: lijst met zaaktype kenmerk ids verwacht')
        unless ref $zaaktype_kenmerken_ids eq 'ARRAY';

    # Hack around ZS-5543
    # This is a join on self:
    #   SELECT
    #       DISTINCT(ztk.id)
    #   FROM
    #       zaaktype_kenmerken ztk
    #   JOIN
    #       zaaktype_kenmerken ztk2 ON ztk.bibliotheek_kenmerken_id = ztk2.bibliotheek_kenmerken_id
    #   WHERE
    #       ztk2.id IN $zaaktype_kenmerken_ids
    #   ;
    my $schema = $self->schema;
    my $rs = $schema->resultset('ZaaktypeKenmerken')->search_rs(
        {
            'id' => $zaaktype_kenmerken_ids,
        },
        {
            columns  => ['bibliotheek_kenmerken_id'],
            distinct => 1.
        },
    );
    $rs = $schema->resultset('ZaaktypeKenmerken')->search_rs(
        {
            'bibliotheek_kenmerken_id.id' => { -in => $rs->as_query },
        },
        {
            join => 'bibliotheek_kenmerken_id',
            columns  => ['me.id'],
            distinct => 1.
        },
    );
    # /Hack around ZS-5543

    my $rv = $schema->resultset('File')->search(
        {
            'case_documents.case_document_id' => { -in => $rs->as_query },
            case_id                           => $self->case->id,
            date_deleted                      => undef,
            accepted                          => 1,
            active_version                    => 1,
        },
        { join => { case_documents => 'file_id' }, }
    );

    if ($self->log->is_debug) {
        my $case_id = $self->case->id;
        my @found;
        while (my $file = $rv->next) {
            push(@found, $file);
            $self->log->debug(
                sprintf("Filename %s with ID %d found for mail attachement in case %d", $file->filename, $file->id, $case_id)
            );
        }
        return @found;
    }
    return $rv->all;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 load_mailer

TODO: Fix the POD

=cut

