package Zaaksysteem::Backend::Rules::Casetype;

use Moose::Role;
use BTTW::Tools;
use Scalar::Util qw/blessed/;
use Zaaksysteem::Backend::Rules::CasetypeRule;
use Zaaksysteem::Backend::Rules::Integrity;
use List::Util qw[first any none];

use Zaaksysteem::Constants qw/
    ZAAK_CONFIDENTIALITY
    CASE_PAYMENT_STATUS_FAILED
    CASE_PAYMENT_STATUS_SUCCESS
    CASE_PAYMENT_STATUS_PENDING
    ZAAKSYSTEEM_CONSTANTS
/;
use Zaaksysteem::Attributes;

use constant DEFINE_CASETYPE => 'casetype';
use constant DEFINE_DB_NODE  => 'Zaaksysteem::Model::DB::ZaaktypeNode';

with qw/
    Zaaksysteem::Backend::Rules::Casetype::Integrity
    Zaaksysteem::Backend::Rules::Casetype::Validation
/;

=head1 NAME

Zaaksysteem::Backend::Rule::Case - Case specific rules

=head1 SYNOPSIS

=head1 DESCRIPTION

Zaaksysteem Rules engine, in charge of generating decision trees, and manipulating
contexts (like a case) according to anwers on these decissions.

For now, this object has support for generating a decision tree which can be used
with our mintjs frontend framework.

=head1 ATTRIBUTES

=head2 casetype_node

isa: Zaaksysteem::Model::DB::ZaaktypeNode

Contains the zaaktype_node where we get our rules from

=cut

has 'casetype_node'  => (
    is      => 'rw',
    isa     => DEFINE_DB_NODE
);

=head2 old_case_attributes

isa: ArrayRef of Zaaksysteem::Model::DB::ZaaktypeKenmerken

Contains a list of all zaaktype_kenmerken belonging to casetype_node

=cut

has 'old_case_attributes'  => (
    is      => 'rw',
    isa     => 'ArrayRef',
    lazy    => 1,
    default => sub {
        my $self = shift;

        return [$self->casetype_node->zaaktype_kenmerken->search({},{prefetch => ['bibliotheek_kenmerken_id', 'zaak_status_id'], order_by => 'me.id'})->all];
    }
);

=head2 has_parent_case

Boolean value, indicates whether the case has a parent case (used to
determine whether rules referring to referential attributes should be
skipped).

=cut

has has_parent_case => (
    is       => 'ro',
    isa      => 'Bool',
    required => 0,
    default  => 0,
);

=head2 source_type

Returns "casetype"

=cut

around '_build_source_type'    => sub {
    my $method      = shift;
    my $self        = shift;

    if (UNIVERSAL::isa($self->source, DEFINE_DB_NODE)) {
        $self->casetype_node($self->source);
        return DEFINE_CASETYPE;
    }

    return $self->$method(@_);
};

=head1 INTERNAL ATTRIBUTES

=head2 _attribute_mapping

isa: HashRef

    {
        44 => 'kenteken',
        23 => 'omschrijving',
        [...]
    }

Hash of bibliotheek_kenmerken_id => magic_string

=cut

has '_attribute_mapping'     => (
    is      => 'rw',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub {
        my $self        = shift;

        # Build id => magic_string mapping for bibliotheek_kenmerken
        my %mapping = map {
            $_->id => $_->magic_string
        } grep {
            defined
        } map {
            $_->bibliotheek_kenmerken_id
        } @{ $self->old_case_attributes };

        return \%mapping;
    }
);

has '_attribute_mapping_for_case_status'     => (
    is      => 'rw',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub {
        my $self        = shift;

        return $self->_attribute_mapping_for_number_status($self->source_params->{'case.number_status'});
    }
);

sub _attribute_mapping_for_number_status {
    my $self            = shift;
    my $number_status   = shift;

    my %mapping;
    for my $ztk (@{ $self->old_case_attributes }) {
        my $bibliotheek_kenmerk = $ztk->bibliotheek_kenmerken_id;

        next unless $bibliotheek_kenmerk && $bibliotheek_kenmerk->magic_string;

        next if ($number_status && $number_status != $ztk->zaak_status_id->status);

        $mapping{$bibliotheek_kenmerk->id} = $bibliotheek_kenmerk->magic_string;
    }

    return \%mapping;
}

sub _text_blocks_for_number_status {
    my $self            = shift;
    my $number_status   = shift;

    my @blocks;
    for my $ztk (@{ $self->old_case_attributes }) {
        next unless defined $ztk->type && $ztk->type eq 'text_block';
        next if ($number_status && $number_status != $ztk->zaak_status_id->status);

        push @blocks, $ztk->id;
    }

    return \@blocks;
}

=head2 _attribute_mapping_for_case

Get a mapping of all attributes

=cut

has '_attribute_mapping_for_case'     => (
    is      => 'rw',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub {
        my $self        = shift;

        return $self->_attribute_mapping_for_number_status();
    }
);

=head1 METHODS

=head2 new_from_case

Arguments: $case, \%PARAMS

    $rule_engine     = Zaaksysteem::Backend::Rules->new_from_case(
        $schema->resultset('Zaak')->search->first,
        {
            'case.number_status'   => 1,
        }
    );

Returns a rule object deriven from the given case (and the belonging casetype) for
the given status.

B<Options>

=over 4

=item case.number_status

The status number to generate the rules for. The first milestone is 1, the second 2,
and so forth.

=back

=cut

define_profile 'new_from_case'  => (
    optional    => [qw/case.number_status/],
);

sub new_from_case {
    my $class           = shift;
    my $case            = shift;
    my $rawparams       = shift || {};
    my $params          = assert_profile($rawparams)->valid;

    throw(
        'api/rules/invalid_case',
        'Cannot find case to base rules on'
    ) unless $case && blessed($case);

    my $attr_params     = {
        map ({ $_ => $rawparams->{ $_ }} grep ({ $_ =~ /^attribute\./ } keys %$rawparams))
    };

    my $clean_params                  = {
        source_params   => {
            $params->{'case.number_status'}    ? ('case.number_status' => $params->{'case.number_status'}) : (),
        },
        rules_params    => {
            'case.channel_of_contact'   => $case->contactkanaal,
            'case.payment_status'       => $case->payment_status,
            'case.confidentiality'      => $case->confidentiality,
            'case.price'                => $case->payment_amount,

            %{ $class->_get_requestorparams_from_object($case->aanvrager_object) },
            %{ $attr_params }
        },
        has_parent_case => (defined $case->pid ? 1 : 0),
        source => $case->zaaktype_node_id
    };

    return $class->new(%$clean_params);
}

=head2 new_from_casetype

Arguments: $casetype_node, \%PARAMS

    $rule_engine    = Zaaksysteem::Backend::Rules->new_from_casetype(
        $schema->resultset('Zaak')->search->first->zaaktype_node_id,
        {
            'case.number_status'            => $c->req->params->{milestone},
            'case.requestor.subject_type'   => 'natuurlijk_persoon',
            'case.payment_status'           => 'pending',
            'case.requestor.zipcode'        => '1234AA',
            'case.requestor.house_number'   => 22,
            'case.channel_of_contact'       => 'behandelaar',
        }
    );

Returns a rule object deriven from the given casetype_node for the given status and requestor
parameters

B<Options>

See profile in code below

=cut

define_profile 'new_from_casetype' => (
    required            => [qw/
        case.channel_of_contact
        case.requestor.subject_type
    /],
    optional            => [qw/
        case.number_status

        case.payment_status
        case.confidentiality

        case.requestor.zipcode
        case.requestor.house_number
    /],
    defaults            => {
        'case.payment_status'   => CASE_PAYMENT_STATUS_PENDING,
        'case.number_status'    => 1,
    },
    constraint_methods  => {
        'case.casetype.node.id'         => qr/^\d+$/,
        'case.requestor.subject_type'   => qr/^\w+$/,
        'case.requestor.zipcode'        => qr/^\d{4} ?[a-zA-Z]{2}$/,
        'case.requestor.house_number'   => qr/^\d+$/,
        'case.payment_status'           => sub {
            my $val = pop;

            return (
                (grep { $val eq $_ } (CASE_PAYMENT_STATUS_FAILED,CASE_PAYMENT_STATUS_SUCCESS,CASE_PAYMENT_STATUS_PENDING))
                    ? 1
                    : undef
            );
        },
        'case.confidentiality'      => sub {
            return ( ZAAK_CONFIDENTIALITY->(pop) ? 1 : undef);
        },
        'case.channel_of_contact'   => sub {
            my $val = pop;

            return (
                (grep { $val eq $_ } @{ ZAAKSYSTEEM_CONSTANTS->{contactkanalen} })
                    ? 1
                    : undef
            );
        }
    },
    field_filters   => {
        'case.requestor.zipcode' => sub {
            my $field   = shift;

            $field =~ s/\s//;

            return $field;
        },
        'case.requestor.house_number'   => sub {
            my $field   = shift;

            $field =~ s/^(\d+).*/$1/;

            ### Strip invalid data
            return $field;
        }
    }
);

sub new_from_casetype {
    my $class                   = shift;
    my $casetype                = shift;
    my $rawparams               = shift || {};
    my $params                  = assert_profile($rawparams)->valid;

    my @rparams = qw/
        case.channel_of_contact
        case.payment_status
        case.confidentiality

        case.requestor.subject_type
        case.requestor.zipcode
        case.requestor.house_number
    /;

    my $attr_params     = {
        map ({ $_ => ($rawparams->{ $_ } || undef)} grep ({ $_ =~ /^attribute\./ } keys %$rawparams))
    };

    my $clean_params            = {
        source_params   => {
            'case.number_status' => $params->{'case.number_status'},
        },
        rules_params    => {
            (map { $_ => ($params->{$_} || undef)} @rparams),
            %{ $attr_params }
        },
        source  => $casetype
    };

    return $class->new(%$clean_params);
}

=head2 generate_object_params

Arguments: \%PARAMS, \%OPTIONS

Returns: { rules => \%rule_params, engine => L<Zaaksysteem::Backend::Rules> };

    $return_object = Zaaksysteem::Backend::Rules->generate_object_params(
        {
            'case.casetype.node'        => $schema->resultset('Zaak')->search->first->zaaktype_node_id,
            'case.channel_of_contact'   => 'behandelaar'
            'case.number_status'        => 1,
            'case.requestor'            => $schema->resultset('Zaak')->search->first->aanvrager_object,
        },
        {
            engine              => 1,
            include_kenmerken   => {
                44  => 'Ja',
                29  => 'Dit is de inhoud van richtext'
            }
        }
    );

Returns a list of params which can be used with L<new_from_casetype>. It also generates the rule
engine with these params, so you can use the engine directly when "engine" is given.

B<OPTIONS>

=over 4

=item engine

isa: boolean

Includes engine in output, a L<Zaaksysteem::Backend::Rules> object

=item include_kenmerken

Include the translated kenmerken in the rule_params output, e.g.:

    {
        44  => 'Ja',
        29  => 'Dit is de inhoud van richtext'
    }

    ### Becomes:

    {
        'attribute.lust_je_bier'    => 'Ja',
        'attribute.licht_toe'       => 'Dit is de inhoud van richtext'
    };

=back

=cut

define_profile 'generate_object_params' => (
    optional    => [qw/
        case.payment_status
        case.confidentiality
        case.number_status
    /],
    constraint_methods  => {
        'case.requestor'        => sub { blessed(pop()) },
        'case'                  => sub { blessed(pop()) },
        'case.casetype.node'    => sub { blessed(pop()) },
    },
    require_some    => {
        'case_or_casetype_node'  => [1, qw/case.casetype.node case/],
        'case_or_requestor'      => [1, qw/case case.requestor/],
        'case_or_channel_of_contact' => [1, qw/case case.channel_of_contact/],
    }
);

use constant GENERATED_KEYS => [qw/
    case.casetype.node.id
    case.channel_of_contact
    case.requestor.subject_type
    case.requestor.zipcode
    case.requestor.house_number
    case.number_status
    case.confidentiality
    case.payment_status
/];

sub generate_object_params {
    my $class               = shift;
    my $params              = assert_profile(shift || {})->valid;
    my $options             = shift || {};

    my $rv = $params->{case}
        ? $class->_generate_params_from_case($params->{case})
        : $class->_get_requestorparams_from_object($params->{'case.requestor'});

    my $casetype_node       = $params->{case} ? $params->{case}->zaaktype_node_id : $params->{'case.casetype.node'};

    ### Load casetype properties
    $rv->{'case.casetype.node.id'}      = $casetype_node->id;

    $rv                                 = $class->_add_casetype_properties($casetype_node, $rv);

    $rv     = {
        %{ $rv },
        map ({ $_ => ($params->{ $_ } || undef) } grep ({ !exists $rv->{ $_ } } @{ GENERATED_KEYS() })),
    };

    my $engine              = $class->_load_engine($params, $rv, $options);

    $rv                     = $class->_load_additional_attributes($engine, $rv, $options);

    my $validation          = $class->_load_validation($engine, $rv, $options);

    my $extra_params        = {};
    if ($params->{case}) {
        my $extra_params    = $engine->get_revalidate_params($params->{case});
    }

    $rv     = {
        %{ $rv },
        %{ $extra_params }
    };

    # Make sure the rule engine has the latest parameters
    $engine->rules_params($rv);

    return {
        rule_params => $rv,
        $engine ? (rules      => $engine) : (),
        $validation ? (validation => $validation) : (),
    };
}

=head2 _load_validation

=cut

sub _load_validation {
    my $self                    = shift;
    my ($engine, $rv, $options) = @_;

    return unless $options->{validation};

    ### Get active attributes
    my $validation  = $engine->validate(
        {
            %{ $engine->rules_params },
            %{ $options->{include_kenmerken} },
            'case.number_status' => ($rv->{'case.number_status'} || 1),
        }
    );

    return $validation;
}


=head2 get_revalidate_params

Arguments: $case

    my $rv = $class->get_revalidate_params($params, $rv, { engine => 1});

Will read field_values from case, and append this to $rv

=cut

sub get_revalidate_params {
    my $self                = shift;
    my $case                = shift;

    my $field_values        = $case->field_values;

    my $revalidate_params   = {};
    for my $rule (@{ $self->rules }) {
        next unless $rule->conditions;
        for my $condition (@{ $rule->conditions }) {
            if ($condition->validation_type eq 'revalidate') {
                $revalidate_params->{$condition->attribute} = $field_values->{ $condition->_old_kenmerk_id };
            }
        }
    }

    return $revalidate_params;

    # print Data::Dumper::Dumper($self->rules);
}


=head1 INTERNAL METHODS

=head2 _get_list_of_attributes

Returns a list of active attributes, according to kenmerken

=cut

has '_get_list_of_attributes' => (
    is      => 'rw',
    isa     => 'ArrayRef',
    lazy    => 1,
    default => sub {
        my $self            = shift;

        my @rv;
        for my $attribute (values %{ $self->_attribute_mapping_for_case_status }) {
            push(@rv, 'attribute.' . $attribute);
        }

        return \@rv;
    }

);

has '_get_list_of_visible_attributes' => (
    is      => 'rw',
    isa     => 'ArrayRef',
    lazy    => 1,
    default => sub {
        my $self            = shift;

        my @rv;
        for my $attribute_key (keys %{ $self->_attribute_mapping_for_case }) {
            next unless length($attribute_key);
            # Visibility of an attribute is derived from it's 'systeemkenmerk'
            # status. This condition holds until you consider a casetype may
            # have the same bibliotheek_kenmerk multiple times, either as
            # systeemkenmerk or not.
            #
            # If there's a systeemkenmerk, and no other reference to the
            # bibliotheek_kenmerk we should make the field.

            # Get all zaaktype_kenmerken associated with our attribute key
            my @zaaktype_kenmerken = grep {
                ($_->get_column('bibliotheek_kenmerken_id') //'') eq $attribute_key
            } @{ $self->old_case_attributes };

            # If any of the remaining attributes is a systeemkenmerk, we should mask
            # the attribute (and skip over it)
            my $mask_kenmerk = any { $_->is_systeemkenmerk } @zaaktype_kenmerken;

            # ...unless any of the remaining attributes *is not* a systeemkenmerk
            next if $mask_kenmerk && none { not $_->is_systeemkenmerk } @zaaktype_kenmerken;

            # Finally we push the 'objectdata-ish' attribute name.
            push @rv, sprintf(
                'attribute.%s',
                $self->_attribute_mapping_for_case->{ $attribute_key }
            );
        }

        return \@rv;
    }
);

has '_get_list_of_visible_text_blocks' => (
    is      => 'rw',
    isa     => 'ArrayRef',
    lazy    => 1,
    default => sub {
        my $self = shift;

        return $self->_text_blocks_for_number_status();
    },
);

has '_get_list_of_visible_text_blocks_for_number_status_cache' => (
    is          => 'rw',
    lazy        => 1,
    default     => sub { {}; }
);

sub _get_list_of_visible_text_blocks_for_number_status {
    my ($self, $status_number)  = @_;

    return $self->_get_list_of_visible_text_blocks_for_number_status_cache->{ $status_number }
        if $self->_get_list_of_visible_text_blocks_for_number_status_cache->{ $status_number };

    my $text_block_list = $self->_text_blocks_for_number_status($status_number);

    return ($self->_get_list_of_visible_text_blocks_for_number_status_cache->{ $status_number } = $text_block_list);
}

has '_get_list_of_visible_attributes_for_number_status_cache' => (
    is          => 'rw',
    lazy        => 1,
    default     => sub { {}; }
);

sub _get_list_of_visible_attributes_for_number_status {
    my ($self, $status_number)  = @_;

    # Memoized variant, if not cached for a given status number, fill the
    # cache accordingly.
    unless ($self->_get_list_of_visible_attributes_for_number_status_cache->{ $status_number }) {
        my $mapping = $self->_attribute_mapping_for_number_status($status_number);

        my @rv;

        for my $attribute_key (keys %{ $mapping }) {
            # We skip the attribute if it does not exist in our current status.
            next if none { $_->zaak_status_id->status == $status_number } grep {
                ($_->get_column('bibliotheek_kenmerken_id') // '') eq $attribute_key
            } @{ $self->old_case_attributes };

            push @rv, sprintf('attribute.%s', $mapping->{ $attribute_key });
        }

        $self->_get_list_of_visible_attributes_for_number_status_cache->{ $status_number } = \@rv;
    }

    return $self->_get_list_of_visible_attributes_for_number_status_cache->{ $status_number };
}


=head2 _get_requestorparams_from_object

Arguments: $REQUESTOR_OBJECT

    my $case    = $schema->resultset('Zaak')->search->first;

    my $params  = Zaaksysteem::Backend::Rules->_get_requestorparams_from_object($case->aanvrager_object);

    # Returns:
    # {
    #     case.requestor.zipcode          => '1234AA',
    #     case.requestor.house_number     => 22,
    #     case.requestor.subject_type     => 'natuurlijk_persoon',
    # }


Given a L<Zaaksysteem::Betrokkene::Object>, it returns the above parameters.

=cut

sub _get_requestorparams_from_object {
    my $class                    = shift;
    my $requestor               = shift;

    my %params;
    if ($requestor->can('postcode') && $requestor->postcode) {
        $params{'case.requestor.zipcode'} = $requestor->postcode;
    }

    if ($requestor->can('huisnummer') && $requestor->huisnummer) {
        $params{'case.requestor.house_number'}  = $requestor->huisnummer;
    }

    $params{'case.requestor.subject_type'}   = $requestor->btype,

    return \%params;
}

=head2 _build_tree

Extends L<Zaaksysteem::Backend::Rules> with specific case tree building. Will build the tree when source_type
equals 'casetype'

=cut

around '_build_tree'    => sub {
    my $method      = shift;
    my $self        = shift;

    if ($self->source_type && $self->source_type eq DEFINE_CASETYPE) {
        return $self->_build_tree_casetype;
    }

    return $self->$method(@_);
};

=head2 _build_tree_casetype

Extends L<Zaaksysteem::Backend::Rules> with specific case tree building

=cut


sub _build_tree_casetype {
    my $self = shift;

    my $status = $self->source_params->{'case.number_status'};

    my $db_rules = $self->casetype_node->zaaktype_regels->search_rs(
        {
            $status ? ('zaak_status_id.status' => $status) : (),
            is_group => 0,
            active   => 1,
            'me.naam' => { '!=' => undef },
        },
        {
            order_by => 'me.id',
            prefetch => 'zaak_status_id'
        }
    );

    my @rules;
    while (my $db_rule = $db_rules->next) {

        my $rule = Zaaksysteem::Backend::Rules::CasetypeRule->new(
            label               => $db_rule->naam,
            zt_definition       => $db_rule->as_hashref,
            attribute_map       => $self->_attribute_mapping,
            casetype_node       => $self->casetype_node,
            rules_params        => $self->rules_params,
            old_case_attributes => $self->old_case_attributes,
            has_parent_case     => $self->has_parent_case,
            status              => $db_rule->zaak_status_id->status,
        );

        if (!$rule->integrity_verified) {
            $self->log->warn(
                sprintf(
                    'Integrity checks failed for rule "%s" (%d)',
                    $db_rule->naam, $db_rule->id
                )
            );
            next;
        }

        push(@rules, $rule);

    }

    return \@rules;
}


=head2 _load_additional_attributes

Arguments: $ENGINE, \%PARAMS, \%OPTIONS

    my $rv = $class->_load_additional_attributes($engine, $params, $rv, { include_kenmerken => \%old_style_kenmerken});

When C<include_kenmerken> is set, it will add the parameters given in this hash to C<\%PARAMS>.

=cut

sub _load_additional_attributes {
    my ($class, $engine, $rv, $options) = @_;

    return $rv unless $engine && $options->{include_kenmerken};

    for my $kenmerk_id (keys %{ $options->{include_kenmerken} }) {
        my $attr = $engine->_attribute_mapping->{ $kenmerk_id };
        next unless $attr;

        my $zaaktype_kenmerk = first {
            # bibliotheek_kenmerken_id is prefetched
            ($_->get_column('bibliotheek_kenmerken_id') // '') eq $kenmerk_id
        } @{ $engine->old_case_attributes };

        my $value_type = $zaaktype_kenmerk->bibliotheek_kenmerken_id
            ? $zaaktype_kenmerk->bibliotheek_kenmerken_id->value_type
            : 'unknown';

        my $veldoptie = ZAAKSYSTEEM_CONSTANTS->{veld_opties}->{ $value_type };

        if ($veldoptie->{object_search_filter}) {
            $rv->{ 'attribute.' . $attr } = $veldoptie->{object_search_filter}->(
                $zaaktype_kenmerk->result_source->schema,
                $options->{include_kenmerken}->{ $kenmerk_id }
            );
        }
        else {
            $rv->{ 'attribute.' . $attr } = $options->{include_kenmerken}->{ $kenmerk_id };
        }
    }

    return $rv;
}

=head2 _add_casetype_properties

Arguments: $CASETYPE_NODE, \%PARAMS

    my $rv = $class->_add_casetype_properties($casetype_node, $rv);

Will add the properties defined in the casetype to the return hash.

=cut

sub _add_casetype_properties {
    my ($class, $casetype_node, $rv)    = @_;

    my $available_properties            = ZAAKSYSTEEM_CONSTANTS->{CASETYPE_RULE_PROPERTIES};

    for my $prop_key (@{ $available_properties }) {
        my ($attr)  = grep {
            ($_->{bwcompat_name} && $_->{bwcompat_name} eq $prop_key) ||
            ($_->{internal_name} && $_->{internal_name} eq $prop_key)
        } ZAAKSYSTEEM_SYSTEM_ATTRIBUTES();

        next unless $attr;

        $rv->{ $attr->{name} } = $casetype_node->properties->{ $prop_key };
    }

    ### Edge case for objection_and_appeal. Some jerk made a booboo in zaaktype_attributes
    $rv->{'case.casetype.objection_and_appeal'} = $casetype_node->properties->{beroep_mogelijk};

    return $rv;
}

=head2 _generate_params_from_case

Arguments: $CASE, \%PARAMS

    my $rv = $class->_generate_params_from_case($case, $rv);

    ### $rv is extended with the key=>values:
    {
        'case.channel_of_contact'       => 'behandelaar'
        'case.confidentiality'          => 'intern'
        'case.requestor.subject_type'   => 'natuurlijk_persoon',
        'case.payment_status'           => 'pending',
        'case.requestor.zipcode'        => '1234AA',
        'case.requestor.house_number'   => 22,
    }

Will add the properties defined this case to the return hash

=cut

sub _generate_params_from_case {
    my ($class, $case) = @_;

    my $rv = {
        'case.channel_of_contact'       => $case->contactkanaal || undef,
        'case.payment_status'           => $case->payment_status || undef,
        'case.confidentiality'          => $case->confidentiality || undef,
        'case.price'                    => $case->payment_amount // undef,
        %{ $class->_get_requestorparams_from_object($case->aanvrager_object) },
    };

    return $rv;
}

=head2 _load_engine

Arguments: \%PARAMS, $RV, \%OPTIONS

    my $engine = $class->_load_engine($params, $rv, { engine => 1});

Loads and returns new_from_case or new_from_casetype depending on the given params.

=cut

sub _load_engine {
    my ($class, $params, $rv, $options) = @_;

    if ($params->{ case }) {
        my $engine = $class->new_from_case($params->{ case }, $rv);

        return $engine if defined $engine;
    }

    if ($options->{ engine }) {
        return $class->new_from_casetype($params->{ 'case.casetype.node' }, $rv);
    }
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CASE_PAYMENT_STATUS_FAILED

TODO: Fix the POD

=cut

=head2 CASE_PAYMENT_STATUS_PENDING

TODO: Fix the POD

=cut

=head2 CASE_PAYMENT_STATUS_SUCCESS

TODO: Fix the POD

=cut

=head2 DEFINE_CASETYPE

TODO: Fix the POD

=cut

=head2 DEFINE_DB_NODE

TODO: Fix the POD

=cut

=head2 GENERATED_KEYS

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 ZAAK_CONFIDENTIALITY

TODO: Fix the POD

=cut

