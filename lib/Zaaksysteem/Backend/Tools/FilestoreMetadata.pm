package Zaaksysteem::Backend::Tools::FilestoreMetadata;

use warnings;
use strict;

use Exporter;

use base qw[Exporter];

our @EXPORT = qw[get_document_categories];

=head2 get_document_categories

Document categories.

=cut

sub get_document_categories {
    return (
        'Aangifte',
        'Aanmaning',
        'Aanmelding',
        'Aanvraag',
        'Advies',
        'Afbeelding',
        'Afmelding',
        'Afspraak',
        'Agenda',
        'Akte',
        'Bankgarantie',
        'Begroting',
        'Bekendmaking',
        'Beleidsdocument',
        'Benoeming',
        'Berekening',
        'Beroepschrift',
        'Beschikking',
        'Besluit',
        'Besluitenlijst',
        'Bestek',
        'Bestemmingsplan',
        'Betaalafspraak',
        'Betalingsherinnering',
        'Bevestiging',
        'Bezwaarschrift',
        'Brochure',
        'Catalogus',
        'Checklist',
        'Circulaire',
        'Declaratie',
        'Dwangbevel',
        'Factuur',
        'Film',
        'Foto',
        'Garantiebewijs',
        'Geluidsfragment',
        'Gespreksverslag',
        'Gids',
        'Grafiek',
        'Herinnering',
        'Identificatiebewijs',
        'Kaart',
        'Kennisgeving',
        'Klacht',
        'Lastgeving',
        'Mededeling',
        'Melding',
        'Norm',
        'Nota',
        'Notitie',
        'Offerte',
        'Ontvangstbevestiging',
        'Ontwerp',
        'Opdracht',
        'Overeenkomst',
        'Pakket Van Eisen',
        'Persbericht',
        'Plan',
        'Plan Van Aanpak',
        'Polis',
        'Procesbeschrijving',
        'Proces-verbaal',
        'Rapport',
        'Regeling',
        'Register',
        'Rooster',
        'Ruimtelijk plan',
        'Sollicitatiebrief',
        'Statistische opgave',
        'Taxatierapport',
        'Technische tekening',
        'Tekening',
        'Uitnodiging',
        'Uitspraak',
        'Uittreksel',
        'Vergaderverslag',
        'Vergunning',
        'Verklaring',
        'Verordening',
        'Verslag',
        'Verslag van bevindingen',
        'Verspreidingslijst',
        'Verweerschrift',
        'Verzoek',
        'Verzoekschrift',
        'Voordracht',
        'Voorschrift',
        'Voorstel',
        'Wet',
        'Zienswijze',
    );
}




__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

