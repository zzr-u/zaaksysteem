=head1 NAME

Zaaksysteem::Manual::API::V1::Sysin::Interface - Interface manipulation on Sysin interfaces

=head1 Description

This API-document describes the usage of our JSON Sysin=>Inteface API. Via the Interface API it is possible
to trigger various actions on public available interfaces (called "Koppelingen" in zaaksysteem.nl)

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/sysin/interface

Make sure you use the HTTP Method C<GET> for retrieving, and C<POST> for change requests.

=head2 get_module_by_name

   /api/v1/sysin/interface/get_module_by_name/NAME

Get interface information about an interface module. Currently only C<kcc> is supported as a name.

=head2 trigger

   /api/v1/sysin/interface/UUID/trigger/TRIGGERNAME

Triggers a public API action on a interface. It works as a bridge between our API v1 interface and
various triggers on different "Koppelingen"

You can imagine, there will be a lot of possible triggers. Below a list.

=head3 Xential: api_post_file

A call for posting back files to zaaksysteem, after being generated in Xential.

B<Example call>

 https://localhost/api/v1/sysin/interface/8a9518d9-df04-41ce-8648-d0c3c72f84b5/trigger/api_post_file?transaction_uuid=dc4f5860-5200-4fa4-a774-3a2c395b02f7&case_uuid=051e88a1-1180-4210-b04e-a07347e096fb

By posting a file to the above link in multipart/form-data, Xential is able to post back files to zaaksysteem. By
defining a few GET parameters, we make sure the files return to the right case.

By showing a bit of HTML, we can explain how it works.

B<HTML>

=begin html

&#x3C;form method=&#x22;post&#x22; action=&#x22;https://localhost/api/v1/sysin/interface/8a9518d9-df04-41ce-8648-d0c3c72f84b5/trigger/api_post_file&#x22; encoding=&#x22;multipart/form-data&#x22;&#x3E;
    &#x3C;input type=&#x22;file&#x22; name=&#x22;upload&#x22; /&#x3E;
    &#x3C;input type=&#x22;hidden&#x22; name=&#x22;transaction_uuid&#x22; value=&#x22;dc4f5860-5200-4fa4-a774-3a2c395b02f7&#x22; /&#x3E;
    &#x3C;input type=&#x22;hidden&#x22; name=&#x22;case_uuid&#x22; value=&#x22;051e88a1-1180-4210-b04e-a07347e096fb&#x22; /&#x3E;
&#x3C;/form&#x3E;

=end html

B<Hint>: You can upload multiple files this way, just create multiple "input"-types of the type "file".

After posting, a JSON response is generated, showing you what we've done.

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-061178-b2be73",
   "development" : false,
   "result" : {
      "instance" : {
         "rows" : [
            {
               "instance" : {
                  "checksum" : "md5:b771ff6f06666e4eb88aab9f2131a68f",
                  "created" : "2015-09-01T12:54:11Z",
                  "id" : "8a9518d9-df04-41ce-8648-d0c3c72f84b5",
                  "last_modified" : "2015-09-01T12:54:14Z",
                  "metadata" : {},
                  "mimetype" : "text/plain",
                  "name" : "xentialfile.doc",
                  "size" : 2387,
                  "title" : "xentialfile"
               },
               "reference" : "8a9518d9-df04-41ce-8648-d0c3c72f84b5",
               "type" : "file"
            }
         ]
      },
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

B<Properties>

=over 4

=item transaction_uuid [required]

B<TYPE>: UUID

The transaction UUID of our side. It will be send to Xential inside the
storageOptions. See L<Zaaksysteem::Xential> for more information.

=item case_uuid [required]

B<TYPE>: UUID

The UUID of the case to add the file to. It will check if it's valid, and is here for authentication
purposes.

=back

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::Dashboard>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
