package Zaaksysteem::BR::Subject::ResultSet::Person;

use Moose::Role;
use BTTW::Tools;

with qw/Zaaksysteem::BR::Subject::Utils/;

=head1 NAME

Zaaksysteem::BR::Subject::ResultSet::Person - This is a specific bridge role for L<DBIx::Class::ResultSet> classes.

=head1 DESCRIPTION

Applies logic for the L<Zaaksysteem::BR::Subject> bridge within L<DBIx::Class::ResultSet> classes

=head1 METHODS

=head2 search_from_bridge

=cut

sub search_from_bridge {
    my ($self, $params, $options) = @_;

    $options //= {};

    my $search_params   = $self->map_search_params($params, 'person')->{subject};

    ### First search into addresses
    my $primary_params = {
        map {
            $_ => $self->_bridge_format_for_dbix(
                'person', "subject.$_",
                $search_params->{$_}
            )
        } grep({ !ref $search_params->{$_} } keys %$search_params)
    };

    if ($primary_params->{burgerservicenummer}) {
        my $bsn = delete $primary_params->{burgerservicenummer};
        $primary_params->{'NULLIF(natuurlijk_persoon.burgerservicenummer,\'\')::integer'} = int($bsn);
    }

    my @address_queries;
    for my $address_key (qw/address_correspondence address_residence/) {
        next unless $search_params->{$address_key};

        my $address_params = { map { $_ => $self->_bridge_format_for_dbix('person', "subject.$address_key" . '.' . $_, $search_params->{$address_key}->{ $_ }) } keys %{ $search_params->{$address_key} } };

        if ($search_params->{$address_key}) {
            push(
                @address_queries,
                { 'natuurlijk_persoon.id' => { 'in' => $self->result_source->schema->resultset('Adres')->search($address_params)->get_column('natuurlijk_persoon_id')->as_query } }
            );
        }
    }

    $primary_params->{'natuurlijk_persoon.deleted_on'} = undef;
    $primary_params->{'natuurlijk_persoon.active'} = 1;

    $options->{prefetch}    = 'adres_id';
    $options->{alias}       = 'natuurlijk_persoon';

    return $self->search({
            '-and' => [
                $primary_params,
                @address_queries
            ]
        },
        $options
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
