package Zaaksysteem::View::API::v1;

use Moose;

use BTTW::Tools;
use HTTP::Status qw( :constants );
use Zaaksysteem::API::v1::Serializer;

BEGIN { extends 'Catalyst::View' }

=head1 NAME

Zaaksysteem::View::API::v1 - Seperate view class for API v1 infrastructure

=head1 SYNOPSIS

    sub my_action : Private {
        my ($self, $c) = @_;

        $c->stash->{ result } = ...;

        $c->detach($c->view('API::v1'));
    }

=head1 DESCRIPTION

This view handles generating a view for API v1 actions.

It uses the L<Zaaksysteem::API::v1::Serializer> class to serialize the
contents of the C<results> in L<Catalyst/c-stash>.

=head1 METHODS

=head2 process

Implements the interface required by L<Catalyst> for processing a view.

=cut

sub _is_valid_serialized_object {
    my $self   = shift;
    my $object = shift;

    return 1 if (ref $object eq 'HASH' && $object->{type} && $object->{instance});
    return;
}

sub process {
    my $self = shift;
    my $c = shift;

    my $serializer = Zaaksysteem::API::v1::Serializer->new(
        'has_full_access'       => ($c->stash->{has_full_access} || 0),
        'v1_serializer_options' => ($c->stash->{v1_serializer_options} || {}),
    );

    if ($c->stash->{file_download}) {
        return ;
    }

    if (not($c->config->{experimental_caching_disabled})
        && $c->stash->{is_cacheable}
        && not($c->not_cached)
    ) {
        # Cached response - no need to serialize
        return;
    }

    my $data;
    # In-the-wild configurations have 'development' as well as 'dev' values
    my $development = ($c->config->{ otap } =~ m[^dev]) ? 1 : 0;

    if (not exists $c->stash->{ result }) {
        $c->res->status(500);
        $data = {
            type => 'exception',
            instance => {
                type => 'api/v1/serializer/result_not_set',
                message => 'Serialization error; no result was set for the request'
            }
        };
    }
    elsif (not defined $c->stash->{ result }) {
        $c->res->status(500);
        $data = {
            type => 'exception',
            instance => {
                type => 'api/v1/serializer/result_not_defined',
                message => 'Serialization error; result was undefined'
            }
        };
    }
    elsif ($self->_is_valid_serialized_object($c->stash->{ result })) {
        $data = $c->stash->{ result };
    }
    else {
        $data = try {
            return $serializer->read(
                $c->stash->{ result },
                $c->stash->{ serializer_opts }
            );
        } catch {
            $c->log->info(sprintf("Error '%s' while reading", $_));

            $c->log->debug(dump_terse($c->stash->{result}))
                if ($c->log->is_debug);

            $c->res->status(500);

            # Handcrafted exception, for all we know, it's an exception
            # object that's causing the fault.
            return {
                type     => 'exception',
                instance => {
                    type    => 'api/v1/serializer/read_error',
                    message => 'Serialization error.',
                    $development
                        ? (original_error => "$_")
                        : (),
                }
            };
        };
    }

    my $request_id = $c->stash->{ request_id };


    my $response = {
        request_id => $request_id,
        development => $development ? \1 : \0,
        api_version => 1,
        status_code => $c->res->status,
        result => $data
    };

    my $result = try { $serializer->encode($response) } catch {
        $c->log->error($_);
        $c->res->status(500);

        my $dev = $development ? 'true' : 'false';

        # Handcrafted exception JSON serialization, for all we know, it's the
        # actual encoder that's causing the fault.
        return <<"RESULT";
{
    "request_id": "$request_id",
    "development": $dev,
    "api_version": 1,
    "status_code": 500,
    "result": {
        "type": "exception",
        "instance": {
            "type": "api/v1/serializer/encode_error",
            "message": "Serialization error."
        }
    }
}
RESULT
    };

    $c->res->content_type('application/json; charset=utf-8');
    $c->res->body($result);

    if (my $transaction = $c->stash->{request_event}) {
        # we previously exlusivly recorded this for external, inbound API calls

        # in Sysin::Modules::API :: log_mutations we did set it to default error
        # so, here we'll update it to the correct status

        my $is_success   = HTTP::Status::is_success($c->res->status);

        my $record = $transaction->transaction_records->search->first;

        my $updates = {
            transaction => {
                error_fatal    => $is_success ? 0 : 1,
            }, 
            record      => {
                output         => $result, # the original json output
                is_error       => $is_success ? 0 : 1,
            },
        };
        $updates->{transaction}{error_count}   = 1 unless $is_success;
        $updates->{transaction}{success_count} = 0 unless $is_success;
        if ( $data->{type} ) {
            my $preview_string = $data->{type};
            $preview_string .= ': ' . $data->{reference} if $data->{reference};
            $updates->{record}{preview_string} = $preview_string;
        }

        $transaction->update($updates->{transaction});
        $record->update($updates->{record});
    }

    return
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
