package Zaaksysteem::Session::Invitation::Model;

use Moose;
use namespace::autoclean;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Session::Invitation::Model - Session invitation abstractsions

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use Crypt::OpenSSL::Random qw[random_pseudo_bytes];
use Moose::Util::TypeConstraints qw[role_type];

use Zaaksysteem::Types qw[Store];
use Zaaksysteem::Object::Syntax;
use Zaaksysteem::Object::Types::Session::Invitation;

=head1 ATTRIBUTES

=head2 store

Reference to an object that implements L<Zaaksysteem::Interface::Store> and
contains the invitations proper.

=cut

has store => (
    is => 'rw',
    isa => Store,
    required => 1,
    handles => {
        retrieve_invitation => 'retrieve'
    }
);

=head1 METHODS

=head2 create

Creates a new invitation and returns the new
L<instance|Zaaksysteem::Object::Types::Session::Invitation>.

    my $invitation = $model->create({
        subject => $my_user_object,
        date_expires => DateTime->now->add(days => 5),
        # optionally
        action_path => '/my/path/to/catalyst/action',
        object => $my_reference_object
    });

    notify_user(token => $invitation->token);

=cut

define_profile create => (
    required => {
        subject => 'Zaaksysteem::Object::Types::Subject',
        date_expires => 'DateTime'
    },
    optional => {
        action_path => 'Str',
        object => role_type('Zaaksysteem::Object::Reference')
    }
);

sig create => 'HashRef';

sub create {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $token = unpack 'H*', random_pseudo_bytes(8);

    my %args = %{ $params };

    $args{ token } = $token;
    $args{ subject } = $args{ subject }->_ref;

    $args{ object } = $args{ object }->_ref if exists $args{ object };

    my $invitation = Zaaksysteem::Object::Types::Session::Invitation->new(%args);

    my $id = $self->store->create($invitation);

    $invitation->id($id);

    return $invitation;
}

=head2 validate

Validate a given token. If an invitation for the token is found, it will be
removed during this validation step. If a token was valid, the
L<invitation|Zaaksysteem::Object::Types::Session::Invitation> will be
returned.

    my $invitation = $model->validate('abc123xyz');

    die "token invalid" unless defined $invitation;

=cut

sig validate => 'Str';

sub validate {
    my $self = shift;
    my $token = shift;

    my $query = qb('session_invitation', {
        cond => qb_and(
            qb_eq('token', $token),
            qb_gt('date_expires', DateTime->now)
        )
    });

    my ($invitation) = $self->store->search($query);

    return unless defined $invitation;

    $self->log->debug(sprintf(
        'Removing session invitation for token "%s"',
        $token
    ));

    my $id = $self->store->delete($query);

    return $invitation;
}

=head2 cleanup_expired_invitations

Deletes all session invitations where C<date_expires> lies in the past.

    $model->cleanup_expired_invitations;

=cut

sub cleanup_expired_invitations {
    my $self = shift;

    my $query = qb('session_invitation', {
        cond => qb_lt('date_expires', DateTime->now)
    });

    my @ids = $self->store->delete($query);

    $self->log->info(sprintf(
        'Deleted %d expired session invitations',
        scalar(@ids)
    )) if scalar @ids;

    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
