package Zaaksysteem::API::v1::Serializer::Reader::JSONBoolean;
use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::JSONBoolean - Read Types::Serialiser::BooleanBase instances

=head1 DESCRIPTION

Parsed JSON data can contain L<JSON::PP::Boolean> instances.

=head1 METHODS

=head2 class

Implements interface required by
L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Types::Serialiser::BooleanBase' }

=head2 read

Implements interface required by
L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub read {
    my ($class, $serializer, $data) = @_;

    return $data;
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
