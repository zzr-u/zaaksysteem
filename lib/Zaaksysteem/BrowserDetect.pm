package Zaaksysteem::BrowserDetect;
use Moose;

use BTTW::Tools;
use HTTP::BrowserDetect;
use Perl::Version;

has useragent => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
);

has denied => (
    is => 'ro',
    isa => 'ArrayRef',
    default => sub {
        return [qw(
            ie::<11
        )];
    },
);

with 'MooseX::Log::Log4perl';

=head2 assert

Assert if the browser is allowed by Zaaksysteem

=cut

sub assert {
    my $self = shift;

    my $bd = HTTP::BrowserDetect->new($self->useragent);
    my $useragent_string = $self->useragent;
    my $browserlist = $self->denied;

    my $bversion = Perl::Version->new($bd->browser_version);

    $self->log->trace(sprintf("Useragent '%s' is version '%s'", $self->useragent, $bversion));

    for my $browserentry (@{ $browserlist }) {
        my ($browser, $version) = split(/::/, $browserentry);

        unless ($bd->can($browser) && $bd->$browser) {
            $self->log->trace("Skipping browser $browser, not supported by HTTP::BrowserDetect");
            next;
        }

        if (!$version) {
            throw("unsupported/browser/all_versions", "Browser is not supported");
        }
        elsif ($version =~ /^</) {
            $version =~ s/^<//;
            if ($bversion < Perl::Version->new($version)) {
                throw("unsupported/browser/version_too_low", "Browser is not supported, version too low: $bversion");
            }

        }
        elsif ($version =~ /^>/) {
            $version =~ s/^>//;
            if ($bversion > Perl::Version->new($version)) {
                throw("unsupported/browser/version_too_high", "Browser is not supported, version too high: $bversion");
            }
        }
        elsif ($bversion == Perl::Version->new($version)) {
            throw("unsupported/browser/exact_version", "Browser is not supported, version match: $bversion");
        }
    }
    return 1;
}

=head2 check

Check if the browser is allowed by Zaaksysteem.
Similar to C<assert> but does not die in case of an error.

=cut

sub check {
    my $self = shift;
    return try {
        $self->assert(@_);
    } catch {
        return 0;
    };
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::BrowserDetect - Do a check to see if the browser is allowed by Zaaksysteem

=head1 DESCRIPTION

Check if the browser is allowed by Zaaksysteem. We do not support old browsers.

=head1 SYNOPSIS

    use Zaaksysteem::BrowserDetect;

    my $b = Zaaksysteem::BrowserDetect->new(useragent => "Some agent");
    $b->assert;
    $b->check;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
