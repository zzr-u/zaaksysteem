package Zaaksysteem::StUF::ADR;

use Moose::Role;

use constant ADDRESS_MAP            => {
    'woonplaats_identificatie'                      => 'extra.authentiekeIdentificatieWoonplaats',
    'woonplaats_naam'                               => ['extra.authentiekeWoonplaatsnaam','woonplaatsnaam'],

    'openbareruimte_identificatie'                  => 'extra.authentiekeIdentificatieOpenbareRuimte',
    'openbareruimte_naam'                           => 'extra.officieleStraatnaam',

    'nummeraanduiding_identificatie'                => 'extra.identificatieNummerAanduiding',
    'nummeraanduiding_begindatum'                   => 'extra.ingangsdatum',
    'nummeraanduiding_huisnummer'                   => 'huisnummer',
    'nummeraanduiding_huisletter'                   => 'huisletter',
    'nummeraanduiding_huisnummertoevoeging'         => 'huisnummertoevoeging',
    'nummeraanduiding_postcode'                     => 'postcode',
    'nummeraanduiding_inonderzoek'                  => 'extra.aanduidingGegevensInOnderzoek',
    'nummeraanduiding_status'                       => 'extra.status',

    'verblijfsobject_identificatie'                 => 'extra.identificatiecodeVerblijfplaats'
};

=head2 METHODS

=head2 get_params_for_natuurlijk_persoon

Gets a set of params for manipulating natuurlijk_persoon

=cut

sub get_params_for_adr {
    my $self            = shift;

    my $params          = {};
    my $object_params   = $self->as_params->{ADR};

    for my $key (keys %{ ADDRESS_MAP() }) {
        my @object_keys;

        if (UNIVERSAL::isa(ADDRESS_MAP()->{$key}, 'ARRAY')) {
            @object_keys = @{ ADDRESS_MAP->{$key} };
        } else {
            @object_keys = (ADDRESS_MAP->{$key});
        }

        my $object_value;
        for my $object_key (@object_keys) {
            ### When we found a value, we are satisfied
            next if $object_value;

            if ($object_key =~ /^extra\./) {
                $object_key =~ s/^extra\.//;

                next unless exists($object_params->{ 'extraElementen' }-> { $object_key });
                $object_value = $object_params->{ 'extraElementen' }-> { $object_key };
            } else {
                next unless exists($object_params->{ $object_key });

                $object_value = $object_params->{ $object_key };
            }

            $params->{$key} = $object_value;
        }
    }

    return $params;
}


1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 get_params_for_adr

TODO: Fix the POD

=cut

