package Zaaksysteem::Constants;
use strict;
use warnings;

use DateTime;
use DateTime::Format::Strptime qw(strptime);
use Scalar::Util qw/blessed looks_like_number/;
use List::Util qw(any uniq);

use Zaaksysteem::Object::ConstantTables qw/MUNICIPALITY_TABLE COUNTRY_TABLE LEGAL_ENTITY_TYPE_TABLE/;

use utf8;

use Exporter qw(import);
our @EXPORT     = qw/
    ZAAKSYSTEEM_CONSTANTS
    ZAAKSYSTEEM_NAMING
    ZAAKSYSTEEM_OPTIONS
    ZAAKTYPE_DB_MAP

    ZAAKTYPE_PREFIX_SPEC_KENMERK
    ZAAKTYPE_KENMERKEN_ZTC_DEFINITIE
    ZAAKTYPE_KENMERKEN_DYN_DEFINITIE

    GEGEVENSMAGAZIJN_GBA_PROFILE
    GEGEVENSMAGAZIJN_GBA_ADRES_EXCLUDES

    GEGEVENSMAGAZIJN_KVK_PROFILE
    GEGEVENSMAGAZIJN_KVK_RECHTSVORMCODES

    ZAAKSYSTEEM_AUTHORIZATIONS
    ZAAKSYSTEEM_AUTHORIZATION_ROLES
    ZAAKSYSTEEM_AUTHORIZATION_PERMISSIONS

    SJABLONEN_EXPORT_FORMATS

    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_EIDAS
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_GBA
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_KVK
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_TWOFACTOR

    ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_HIGH
    ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_MEDIUM
    ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_LATE

    ZAAKSYSTEEM_BETROKKENE_KENMERK
    ZAAKSYSTEEM_BETROKKENE_SUB

    LOGGING_COMPONENT_ZAAK
    LOGGING_COMPONENT_USER
    LOGGING_COMPONENT_ZAAKTYPE
    LOGGING_COMPONENT_CATEGORIE
    LOGGING_COMPONENT_NOTITIE
    LOGGING_COMPONENT_BETROKKENE
    LOGGING_COMPONENT_KENMERK
    LOGGING_COMPONENT_SJABLOON
    LOGGING_COMPONENT_NOTIFICATIE
    LOGGING_COMPONENT_DOCUMENT

    STUF_VERSIONS
    STUF_XSD_PATH
    STUF_XML_URL

    DEFAULT_KENMERKEN_GROUP_DATA

    ZAKEN_STATUSSEN
    ZAKEN_STATUSSEN_DEFAULT

    SEARCH_QUERY_SESSION_VAR
    SEARCH_QUERY_TABLE_NAME

    ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM
    ZAAKSYSTEEM_CONTACTKANAAL_BALIE
    ZAAKSYSTEEM_CASE_ACTION_AUTHORIZATION_MATRIX

    VALIDATION_CONTACT_DATA
    VALIDATION_EXTERNAL_CONTACT_DATA

    ZAAK_CREATE_PROFILE
    ZAAKTYPE_DEPENDENCY_IDS
    ZAAKTYPE_DEPENDENCIES

    BASE_RELATION_ROLES

    BETROKKENE_RELATEREN_PROFILE
    BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION

    VERNIETIGINGS_REDENEN
    ZAAK_WIJZIG_VERNIETIGINGSDATUM_PROFILE
    ZAAK_WIJZIG_STREEFAFHANDELDATUM_PROFILE
    ZAAK_WIJZIG_REGISTRATIEDATUM_PROFILE

    PARAMS_PROFILE_MESSAGES_SUB

    ZAAKSYSTEEM_NAAM
    ZAAKSYSTEEM_OMSCHRIJVING
    ZAAKSYSTEEM_LEVERANCIER
    ZAAKSYSTEEM_STARTDATUM
    ZAAKSYSTEEM_LICENSE
    PARAMS_PROFILE_DEFAULT_MSGS

    DOCUMENTS_STORE_TYPE_NOTITIE
    DOCUMENTS_STORE_TYPE_FILE
    DOCUMENTS_STORE_TYPE_MAIL
    DOCUMENTS_STORE_TYPE_JOB

    FILE_STORE_LOCATION

    BETROKKENE_TYPE_BEDRIJF
    BETROKKENE_TYPE_NATUURLIJK_PERSOON
    BETROKKENE_TYPE_MEDEWERKER

    OBJECTSEARCH_TABLENAMES
    OBJECTSEARCH_TABLE_ORDER

    OBJECT_ACTIONS

    EDE_PUBLICATION_STRINGS

    STATUS_LABELS
    MIMETYPES_ALLOWED

    ALLOW_NO_HTML
    ALLOW_ONLY_TRUSTED_HTML

    JOBS_INFORMATION_MAP

    STRONG_RELATED_COLUMNS
    ZAAK_EMPTY_COLUMNS

    EVENT_TYPE_GROUPS

    FRIENDLY_BETROKKENE_MESSAGES

    CASE_PAYMENT_STATUS_FAILED
    CASE_PAYMENT_STATUS_SUCCESS
    CASE_PAYMENT_STATUS_PENDING
    CASE_PAYMENT_STATUS_OFFLINE

    SUBJECT_TYPE_EMPLOYEE
    SUBJECT_TYPE_PERSON
    SUBJECT_TYPE_COMPANY

    CHILD_CASETYPE_OPTIONS
/;

our @EXPORT_OK = qw(
    MAX_CONVERSION_FILE_SIZE
    BAG_TYPES

    MAX_REPUSH_PENDING_ITEMS
    MAX_REPUSH_PENDING_STREAM
    MIN_REPUSH_PENDING_PERIOD

    SERVICE_NORM_TYPES
    SERVICE_NORM_TYPES_OK

    ZAAKTYPE_ATTRIBUTEN

    ZAAKSYSTEEM_BOFH

    ZAAKTYPE_TRIGGER
    ZAAK_CREATE_PROFILE_BETROKKENE
    ZAAKSYSTEEM_CASE_ACTION_AUTHORIZATION_MATRIX

    VALID_FQDN
    ZS_PROFILE_CREATE_DOMAIN
    OBJECT_CLASS_DOMAIN

    ZAAK_CONFIDENTIALITY
    CASE_PAYMENT_STATUS_FAILED
    CASE_PAYMENT_STATUS_SUCCESS
    CASE_PAYMENT_STATUS_PENDING
    CASE_PAYMENT_STATUS_OFFLINE
    ZAAKSYSTEEM_CONSTANTS

    RGBZ_LANDCODES
    RGBZ_GEMEENTECODES
    VALIDATION_RELATEREN_PROFILE
    BETROKKENE_RELATEREN_PROFILE

    STUF_SUBSCRIPTION_VIEWS

    PUBLIC_CASE_FINISH_TITLE
    PUBLIC_CASE_FINISH_MESSAGE
    ZAAKSYSTEEM_CSS_TEMPLATES
    ZAAKSYSTEEM_CSS_TEMPLATES_OLD

    SUBJECT_TYPES

    SAML_TYPE_LOGIUS
    SAML_TYPE_KPN_LO
    SAML_TYPE_EIDAS
    SAML_TYPE_ADFS
    SAML_TYPE_SPOOF
    SAML_TYPE_MINIMAL

    CONTROLPANEL_CUSTOMER_TYPE_POST_DOMAIN
);

our %EXPORT_TAGS = (
    SAML_TYPES => [qw(
        SAML_TYPE_LOGIUS
        SAML_TYPE_KPN_LO
        SAML_TYPE_EIDAS
        SAML_TYPE_ADFS
        SAML_TYPE_SPOOF
        SAML_TYPE_MINIMAL
    )],
    SUBJECT_TYPES => [qw(
        SUBJECT_TYPE_EMPLOYEE
        SUBJECT_TYPE_PERSON
        SUBJECT_TYPE_COMPANY
    )],
);

# Don't convert/thumbnail files if they're >200MiB
use constant MAX_CONVERSION_FILE_SIZE => $ENV{ZS_MAX_CONVERSION_FILE_SIZE} || 200 * 1024 * 1024;

# used for re-pushing missed virus scans
use constant MAX_REPUSH_PENDING_ITEMS  => $ENV{ZS_MAX_REPUSH_PENDING_ITEMS}  // 100; # more files will wait for next run
use constant MAX_REPUSH_PENDING_STREAM => $ENV{ZS_MAX_REPUSH_PENDING_STREAM} // 0.5 * 1024 * 1024; # 0.5 MB/sec
use constant MIN_REPUSH_PENDING_PERIOD => $ENV{ZS_MIN_REPUSH_PENDING_PERIOD} // 900; # seconds in 15 mins



### DO NOT FREAKING TOUCH ;)
### {
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID       => 'digid';
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_EIDAS       => 'eidas';
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID   => 'bedrijfid';
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_GBA         => 'gba';
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_KVK         => 'kvk';
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR => 'behandelaar';
use constant ZAAKSYSTEEM_GM_AUTHENTICATEDBY_TWOFACTOR   => 'twofactor';

use constant ZAAKTYPE_PREFIX_SPEC_KENMERK   => 'spec';

use constant ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_MEDIUM      => 0.2;
use constant ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_HIGH        => 0.1;
use constant ZAAKSYSTEEM_ZAAK_URGENTIE_PERC_LATE        => 1;

use constant LOGGING_COMPONENT_USER         => 'user';
use constant LOGGING_COMPONENT_ZAAK         => 'zaak';
use constant LOGGING_COMPONENT_ZAAKTYPE     => 'zaaktype';
use constant LOGGING_COMPONENT_CATEGORIE    => 'bibliotheek_categorie';
use constant LOGGING_COMPONENT_NOTITIE      => 'notitie';

use constant LOGGING_COMPONENT_BETROKKENE   => 'betrokkene';
use constant LOGGING_COMPONENT_KENMERK      => 'kenmerk';
use constant LOGGING_COMPONENT_SJABLOON     => 'sjabloon';
use constant LOGGING_COMPONENT_NOTIFICATIE  => 'notificatie';
use constant LOGGING_COMPONENT_DOCUMENT     => 'document';

use constant ZAKEN_STATUSSEN                => [ qw/new open stalled resolved deleted/ ];
use constant ZAKEN_STATUSSEN_DEFAULT        => 'new';

use constant ZAAKSYSTEEM_NAAM               => 'zaaksysteem.nl';
use constant ZAAKSYSTEEM_OMSCHRIJVING       => 'Het zaaksysteem.nl is een '
                                                .'complete oplossing '
                                                .'(all-in-one) voor '
                                                .'gemeenten om de '
                                                .'dienstverlening te '
                                                .'verbeteren.';
use constant ZAAKSYSTEEM_LEVERANCIER        => 'Mintlab B.V.';
use constant ZAAKSYSTEEM_STARTDATUM         => '01-10-2009';
use constant ZAAKSYSTEEM_LICENSE            => 'EUPL';

### } END DO NOT FREAKING TOUCH

use constant ZAAKSYSTEEM_NAMING     => {
    TRIGGER_EXTERN                              => 'extern',
    TRIGGER_INTERN                              => 'intern',
    AANVRAGER_TYPE_NATUURLIJK_PERSOON           => 'natuurlijk_persoon',
    AANVRAGER_TYPE_NATUURLIJK_PERSOON_NA        => 'natuurlijk_persoon_na',
    AANVRAGER_TYPE_NIET_NATUURLIJK_PERSOON      => 'niet_natuurlijk_persoon',
    AANVRAGER_TYPE_MEDEWERKER                   => 'medewerker',
    AANVRAGER_ADRES_TYPE_ADRES                  => 'aanvrager_adres',
    AANVRAGER_ADRES_TYPE_ANDERS                 => 'anders',
    WEBFORM_TOEGANG                             => 'webform_toegang',
    WEBFORM_AUTHENTICATIE_AAN                   => 'authenticatie',
    WEBFORM_AUTHENTICATIE_OPTIONEEL             => 'optie',
    HANDELINGSINITIATOR_AANGAAN                 => 'aangaan',
    HANDELINGSINITIATOR_AANGEVEN                => 'aangeven',
    HANDELINGSINITIATOR_AANMELDEN               => 'aanmelden',
    HANDELINGSINITIATOR_AANVRAGEN               => 'aanvragen',
    HANDELINGSINITIATOR_AFKOPEN                 => 'afkopen',
    HANDELINGSINITIATOR_AFMELDEN                => 'afmelden',
    HANDELINGSINITIATOR_INDIENEN                => 'indienen',
    HANDELINGSINITIATOR_INSCHRIJVEN             => 'inschrijven',
    HANDELINGSINITIATOR_MELDEN                  => 'melden',
    HANDELINGSINITIATOR_OPZEGGEN                => 'opzeggen',
    HANDELINGSINITIATOR_REGISTREREN             => 'registreren',
    HANDELINGSINITIATOR_RESERVEREN              => 'reserveren',
    HANDELINGSINITIATOR_STELLEN                 => 'stellen',
    HANDELINGSINITIATOR_VOORDRAGEN              => 'voordragen',
    HANDELINGSINITIATOR_VRAGEN                  => 'vragen',
    HANDELINGSINITIATOR_ONTVANGEN               => 'ontvangen',
    HANDELINGSINITIATOR_AANSCHRIJVEN            => 'aanschrijven',
    HANDELINGSINITIATOR_VASTSTELLEN             => 'vaststellen',
    HANDELINGSINITIATOR_VERSTUREN               => 'versturen',
    HANDELINGSINITIATOR_UITVOEREN               => 'uitvoeren',
    HANDELINGSINITIATOR_OPSTELLEN               => 'opstellen',
    HANDELINGSINITIATOR_STARTEN                 => 'starten',
    OPENBAARHEID_OPENBAAR                       => 'openbaar',
    OPENBAARHEID_GESLOTEN                       => 'gesloten',
    TERMS_TYPE_KALENDERDAGEN                    => 'kalenderdagen',
    TERMS_TYPE_WEKEN                            => 'weken',
    TERMS_TYPE_WERKDAGEN                        => 'werkdagen',
    TERMS_TYPE_EINDDATUM                        => 'einddatum',
    TERMS_TYPE_MINUTES                          => 'minuten', # minutes from now, for testing agendering
    TERMS_TYPE_MONTHS                           => 'maanden',
    RESULTAATINGANG_VERVALLEN                   => 'vervallen',
    RESULTAATINGANG_ONHERROEPELIJK              => 'onherroepelijk',
    RESULTAATINGANG_AFHANDELING                 => 'afhandeling',
    RESULTAATINGANG_VERWERKING                  => 'verwerking',
    RESULTAATINGANG_GEWEIGERD                   => 'geweigerd',
    RESULTAATINGANG_VERLEEND                    => 'verleend',
    RESULTAATINGANG_GEBOORTE                    => 'geboorte',
    RESULTAATINGANG_EINDE_DIENSTVERBAND         => 'einde-dienstverband',
    DOSSIERTYPE_DIGITAAL                        => 'digitaal',
    DOSSIERTYPE_FYSIEK                          => 'fysiek',
};

use constant PUBLIC_CASE_FINISH_TITLE   => 'Uw zaak is geregistreerd';
use constant PUBLIC_CASE_FINISH_MESSAGE => 'Bedankt voor het [[case.casetype.initiator_type]] van een <strong>[[case.casetype.name]]</strong>. Uw registratie is bij ons bekend onder <strong>zaaknummer [[case.number]]</strong>. Wij verzoeken u om bij verdere communicatie dit zaaknummer te gebruiken. De behandeling van deze zaak zal spoedig plaatsvinden.';

use constant ZAAKSYSTEEM_OPTIONS    => {
    'PUBLIC_CASE_FINISH_TITLE'   => PUBLIC_CASE_FINISH_TITLE,
    'PUBLIC_CASE_FINISH_MESSAGE' => PUBLIC_CASE_FINISH_MESSAGE,
    'RESULTAATINGANGEN'   => [
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_VERVALLEN},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_ONHERROEPELIJK},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_AFHANDELING},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_VERWERKING},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_GEWEIGERD},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_VERLEEND},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_GEBOORTE},
        ZAAKSYSTEEM_NAMING->{RESULTAATINGANG_EINDE_DIENSTVERBAND},
    ],
    'DOSSIERTYPE'       => [
        ZAAKSYSTEEM_NAMING->{DOSSIERTYPE_DIGITAAL},
        ZAAKSYSTEEM_NAMING->{DOSSIERTYPE_FYSIEK}
    ],
    'PROCESTYPE_GENERIEK' => [
        '',
        'Generiek',
        'Specifiek',
    ],
    'HERKOMST' => [
        '',
        'Specifiek benoemde wet- en regelgeving',
        'Systeemanalyse',
        'Trendanalyse',
        'Risicoanalyse',
    ],
    'PROCESTERMIJN' => [
        '',
        'A',
        'B',
        'C',
        'D',
        'E',
    ],
    'BEWAARTERMIJN'     => {
        28      => '4 weken',
        93      => '3 maanden',
        186     => '6 maanden',
        365     => '1 jaar',
        548     => '1,5 jaar',
        730     => '2 jaar',
        1095    => '3 jaar',
        1460    => '4 jaar',
        1825    => '5 jaar',
        2190    => '6 jaar',
        2555    => '7 jaar',
        2920    => '8 jaar',
        3285    => '9 jaar',
        3650    => '10 jaar',
        4015    => '11 jaar',
        4380    => '12 jaar',
        4745    => '13 jaar',
        5110    => '14 jaar',
        5475    => '15 jaar',
        5840    => '16 jaar',
        6935    => '19 jaar',
        7300    => '20 jaar',
        7665    => '21 jaar',
        10950   => '30 jaar',
        14600   => '40 jaar',
        18250   => '50 jaar',
        40150   => '110 jaar',
        99999   => 'Bewaren',
    },
    WEBFORM_AUTHENTICATIE   => [
        ZAAKSYSTEEM_NAMING->{WEBFORM_AUTHENTICATIE_AAN},
        ZAAKSYSTEEM_NAMING->{WEBFORM_AUTHENTICATIE_OPTIONEEL},
    ],
    TRIGGERS                => [
        ZAAKSYSTEEM_NAMING->{TRIGGER_EXTERN},
        ZAAKSYSTEEM_NAMING->{TRIGGER_INTERN},
    ],
    AANVRAGERS_INTERN       => [
        ZAAKSYSTEEM_NAMING->{AANVRAGER_TYPE_NATUURLIJK_PERSOON},
        ZAAKSYSTEEM_NAMING->{AANVRAGER_TYPE_NATUURLIJK_PERSOON_NA},
        ZAAKSYSTEEM_NAMING->{AANVRAGER_TYPE_NIET_NATUURLIJK_PERSOON},
    ],
    AANVRAGERS_EXTERN       => [
        ZAAKSYSTEEM_NAMING->{AANVRAGER_TYPE_MEDEWERKER},
    ],
    AANVRAGER_ADRES_TYPEN   => [
        ZAAKSYSTEEM_NAMING->{AANVRAGER_ADRES_TYPE_ADRES},
        ZAAKSYSTEEM_NAMING->{AANVRAGER_ADRES_TYPE_ANDERS},
    ],
    HANDELINGSINITIATORS    => [
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AANGEVEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AANMELDEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AANVRAGEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AFKOPEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AFMELDEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_INDIENEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_INSCHRIJVEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_MELDEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_RESERVEREN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_STELLEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_VOORDRAGEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_VRAGEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_ONTVANGEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AANSCHRIJVEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_VASTSTELLEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_UITVOEREN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_OPSTELLEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_STARTEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_OPZEGGEN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_AANGAAN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_REGISTREREN},
        ZAAKSYSTEEM_NAMING->{HANDELINGSINITIATOR_VERSTUREN},
        ],
    OPENBAARHEDEN           => [
        ZAAKSYSTEEM_NAMING->{OPENBAARHEID_OPENBAAR},
        ZAAKSYSTEEM_NAMING->{OPENBAARHEID_GESLOTEN},
    ],
    ARCHIEFNOMINATIE_OPTIONS => {
        bewaren => 'Bewaren (B)',
        vernietigen => 'Vernietigen (V)',
        overbrengen => 'Overbrengen (O)'
    },
    RESULTAATTYPEN => [
        'aangegaan',
        'aangehouden',
        'aangekocht',
        'aangesteld',
        'aanvaard',
        'afgeboekt',
        'afgebroken',
        'afgehandeld',
        'afgesloten',
        'afgewezen',
        'akkoord',
        'akkoord met wijzigingen',
        'beëindigd',
        'behaald',
        'betaald',
        'buiten behandeling gesteld',
        'definitief toegekend',
        'geannuleerd',
        'gedeeltelijk gegrond',
        'gedeeltelijk verleend',
        'gedoogd',
        'gegrond',
        'gegund',
        'geïnd',
        'geleverd',
        'geweigerd',
        'gewijzigd',
        'handhaving uitgevoerd',
        'ingericht',
        'ingeschreven',
        'ingesteld',
        'ingetrokken',
        'ingewilligd',
        'niet aangekocht',
        'niet aangesteld',
        'niet akkoord',
        'niet behaald',
        'niet betaald',
        'niet doorgegaan',
        'niet gegund',
        'niet geïnd',
        'niet geleverd',
        'niet gewijzigd',
        'niet ingesteld',
        'niet ingetrokken',
        'niet nodig',
        'niet ontvankelijk',
        'niet opgelegd',
        'niet opgeleverd',
        'niet toegekend',
        'niet uitgevoerd',
        'niet vastgesteld',
        'niet verkregen',
        'niet verleend',
        'niet verstrekt',
        'niet verwerkt',
        'ongegrond',
        'ontvankelijk',
        'opgeheven',
        'opgelegd',
        'opgeleverd',
        'opgelost',
        'opgezegd',
        'toegekend',
        'toezicht uitgevoerd',
        'uitgevoerd',
        'vastgesteld',
        'verhuurd',
        'verkocht',
        'verkregen',
        'verleend',
        'vernietigd',
        'verstrekt',
        'verwerkt',
        'voorlopig toegekend',
        'voorlopig verleend',
    ],
};


use constant ZAAKTYPE_DB_MAP    => {
    'kenmerken'                     => {
        'id'            => 'id',
        'naam'          => 'key',
        'label'         => 'label',
        'type'          => 'value_type',
        'omschrijving'  => 'description',
        'help'          => 'help',
        #'value'         => 'value'             # Value of kenmerken_values
                                                # FOR: ztc
#        'magicstring'   => 'magicstring',
    },
    'kenmerken_values'              => {
        'value'         => 'value',
    },
};

use constant ZAAKTYPE_KENMERKEN_ZTC_DEFINITIE   => [
    {
        'naam'          => 'zaaktype_id',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'zaaktype_nid',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'zaaktype_naam',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'zaaktype_code',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'categorie_naam',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'categorie_id',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'mogelijke_aanvragers',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'trigger',
        'in_node'       => 1,
    },
    {
        'naam'          => 'webform_authenticatie',
        'in_node'       => 1,
    },
    {
        'naam'          => 'adres_relatie',
        'in_node'       => 1,
    },
    {
        'naam'          => 'handelingsinitiator'
    },
    {
        'naam'          => 'grondslag',
    },
    {
        'naam'          => 'selectielijst',
    },
    {
        'naam'          => 'afhandeltermijn',
    },
    {
        'naam'          => 'afhandeltermijn_type',
    },
    {
        'naam'          => 'servicenorm',
    },
    {
        'naam'          => 'servicenorm_type',
    },
    {
        'naam'          => 'openbaarheid',
    },
    {
        'naam'          => 'procesbeschrijving',
    },
];

use constant ZAAKTYPE_KENMERKEN_DYN_DEFINITIE   => [
    {
        'naam'          => 'status',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'bag_items',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'streefafhandeldatum',
    },
    {
        'naam'          => 'contactkanaal',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'behandelaar',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'zaakeigenaar',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'org_eenheid',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_verificatie',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_geslachtsnaam',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_naam',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_telefoon',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_mobiel',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'aanvrager_email',
        'in_rt_only'    => 1,
    },
    { naam => 'gebruiker_naam', in_rt_only => 1 },
    {
        'naam'          => 'registratiedatum',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'afhandeldatum',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'vernietigingsdatum',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'besluit',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'bezwaar',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'locatie',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'depend_info',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'vroegtijdig_info',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'registratiedatum',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'urgentiedatum_high',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'urgentiedatum_medium',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'route_ou_role',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'opgeschort_info',
        'in_rt_only'    => 1,
    },
    {
        'naam'          => 'resultaat',
        'in_rt_only'    => 1,
    },
];

use constant 'GEGEVENSMAGAZIJN_GBA_ADRES_EXCLUDES'     => [qw/
    straatnaam
    huisnummer
    postcode
    huisnummertoevoeging
    huisletter
    woonplaats
    functie_adres
    gemeente_code
    landcode
    adres_buitenland1
    adres_buitenland2
    adres_buitenland3
/];

use constant PARAMS_PROFILE_MESSAGES_SUB                => sub {
    my $dfv     = shift;
    my $rv      = {};

    for my $missing ($dfv->missing) {
        $rv->{$missing}  = 'Veld is verplicht.';
    }
    for my $missing ($dfv->invalid) {
        $rv->{$missing}  = 'Veld is niet correct ingevuld.';
    }

    return $rv;
};

use constant 'GEGEVENSMAGAZIJN_GBA_PROFILE'     => {
    'missing_optional_valid'  => 1,
    'required'      => [qw/
        burgerservicenummer
        geslachtsnaam
        geboortedatum
    /],
    'optional'      => [qw/
        straatnaam
        huisnummer
        postcode
        woonplaats

        voornamen
        huisnummertoevoeging
        huisletter
        geboorteplaats
        geboorteland
        geslachtsaanduiding
        a_nummer

        voorletters
        voorvoegsel
        adellijke_titel
        nationaliteitscode1
        nationaliteitscode2
        nationaliteitscode3
        aanhef_aanschrijving
        voorletters_aanschrijving
        voornamen_aanschrijving
        naam_aanschrijving
        voorvoegsel_aanschrijving
        burgerlijke_staat
        indicatie_geheim
        adres_buitenland1
        adres_buitenland2
        adres_buitenland3
        landcode
        import_datum
        adres_id
        authenticated
        authenticatedby

        aanduiding_naamgebruik
        functie_adres
        onderzoek_persoon
        onderzoek_huwelijk
        onderzoek_overlijden
        onderzoek_verblijfplaats

        datum_overlijden

        partner_a_nummer
        partner_voorvoegsel
        partner_geslachtsnaam
        partner_burgerservicenummer
        datum_huwelijk
        datum_huwelijk_ontbinding

        gemeente_code
        in_gemeente

        correspondentie_straatnaam
        correspondentie_huisnummer
        correspondentie_postcode
        correspondentie_woonplaats
        correspondentie_huisnummertoevoeging
        correspondentie_huisletter
        correspondentie_gemeente_code
        correspondentie_adres_buitenland1
        correspondentie_adres_buitenland2
        correspondentie_adres_buitenland3
        correspondentie_landcode
    /],
    defaults    => {
        landcode => 6030,
    },
    constraint_methods => {
        'geslachtsaanduiding'   => qr/^[MV]?$/,
        'in_gemeente'           => qr/^[01]$/,
    },
    dependencies => {
        landcode => sub {
            my $dfv     = shift;
            my $code    = shift;

            if ($code eq '6030') {
                if ($dfv->get_filtered_data->{correspondentie_straatnaam}) {
                    return ['correspondentie_postcode','correspondentie_huisnummer','correspondentie_straatnaam','correspondentie_woonplaats'];
                } else {
                    return ['postcode','huisnummer','straatnaam','woonplaats'];
                }
            } else {
                return ['adres_buitenland1'];
            }
        }
    },
    field_filters     => {
        'burgerservicenummer'   => sub {
            my ($field) = @_;

            return $field if length($field) == 9;

            return sprintf("%09d", $field);
        },
        'postcode'    => sub {
            my ($field) = @_;

            $field = uc($field);
            $field =~ s/\s*//g;

            return $field;
        },
        'huisnummer'    => sub {
            my ($field) = @_;

            return undef unless $field =~ /^\d+$/;

            return $field;
        },
        'correspondentie_postcode'    => sub {
            my ($field) = @_;

            $field = uc($field);
            $field =~ s/\s*//g;

            return $field;
        },
        'huisnummertoevoeging'    => sub {
            my ($field) = @_;

            return undef unless $field =~ /^[\w\d\s-]+$/;

            return $field;
        },
        'correspondentie_huisnummer'    => sub {
            my ($field) = @_;

            return undef unless $field =~ /^\d+$/;

            return $field;
        },
        'correspondentie_huisnummertoevoeging'    => sub {
            my ($field) = @_;

            return undef unless $field =~ /^[\w\d\s-]+$/;

            return $field;
        },
        'geboortedatum'             => sub {
            my ($field) = @_;

            return undef unless $field =~ /^[\d-]+$/;

            if ($field =~ /^\d{8}$/) {
                my ($year, $month, $day) = $field =~ /^(\d{4})(\d{2})(\d{2})$/;

                $month  = 1 if $month    < 1;
                $day    = 1 if $day      < 1;

                my $dt;

                eval {
                    $dt      = DateTime->new(
                        'year'          => $year,
                        'month'         => $month,
                        'day'           => $day,
                        #'time_zone'     => 'Europe/Amsterdam',
                    );
                };

                if ($@) {
                    $dt = undef;
                }

                return $dt;
            } elsif ($field =~ /^(\d{2})-(\d{2})-(\d{4})$/) {
                my ($day, $month, $year) = $field =~
                    /^(\d{2})-(\d{2})-(\d{4})$/;

                $month  = 1 if $month    < 1;
                $day    = 1 if $day      < 1;

                my $dt;

                eval {
                    $dt      = DateTime->new(
                        'year'          => $year,
                        'month'         => $month,
                        'day'           => $day,
                        #'time_zone'     => 'Europe/Amsterdam',
                    );
                };

                if ($@) {
                    $dt = undef;
                }

                return $dt;
            }

            return undef;
        },
        'geslachtsaanduiding'       => sub {
            my ($field) = @_;

            return $field unless $field =~ /^[mMvV]?$/;

            return uc($field);
        },
        'partner_burgerservicenummer' => sub {
            my ($field) = @_;

            return '' if $field =~ /^[0 ]+$/;

            return $field;
        },
    },
    msgs                => sub {
        my $dfv     = shift;
        my $rv      = {};

        for my $missing ($dfv->missing) {
            $rv->{$missing}  = 'Veld is verplicht.';
        }
        for my $missing ($dfv->invalid) {
            $rv->{$missing}  = 'Veld is niet correct ingevuld.';
        }

        return $rv;
    },
};

use constant GEGEVENSMAGAZIJN_KVK_PROFILE   => {
    missing_optional_valid  => 1,
    required => [ qw/
        handelsnaam

        vestiging_adres
    /],
    optional => [ qw/
        dossiernummer
        subdossiernummer
        hoofdvestiging_dossiernummer
        hoofdvestiging_subdossiernummer

        fulldossiernummer
        vorig_dossiernummer
        vorig_subdossiernummer

        vestigingsnummer

        vestiging_postcodewoonplaats
        vestiging_woonplaats
        vestiging_straatnaam
        vestiging_huisnummer
        vestiging_postcode
        vestiging_huisletter
        vestiging_huisnummertoevoeging
        vestiging_adres_buitenland1
        vestiging_adres_buitenland2
        vestiging_adres_buitenland3
        vestiging_landcode

        rechtsvorm
        telefoonnummer
        surseance
        kamernummer

        correspondentie_adres
        correspondentie_straatnaam
        correspondentie_huisnummer
        correspondentie_huisletter
        correspondentie_huisnummertoevoeging
        correspondentie_postcodewoonplaats
        correspondentie_postcode
        correspondentie_woonplaats
        correspondentie_adres_buitenland1
        correspondentie_adres_buitenland2
        correspondentie_adres_buitenland3
        correspondentie_landcode

        hoofdactiviteitencode
        nevenactiviteitencode1
        nevenactiviteitencode2

        werkzamepersonen

        contact_naam
        contact_aanspreektitel
        contact_voorletters
        contact_geslachtsnaam
        contact_voorvoegsel
        contact_geslachtsaanduiding

        email
    /],
    dependencies => {
        vestiging_landcode => sub {
            my $dfv     = shift;
            my $code    = shift;

            if ($code eq '6030') {
                return [
                    'vestiging_postcode',
                    'vestiging_straatnaam',
                    'vestiging_huisnummer',
                    'vestiging_postcodewoonplaats',
                    'vestiging_woonplaats',
                    'dossiernummer',
                    'hoofdvestiging_dossiernummer',
                ];
            } else {
                return ['vestiging_adres_buitenland1'];
            }
        },
        correspondentie_landcode => sub {
            my $dfv     = shift;
            my $code    = shift;

            if ($code eq '6030') {
                return ['correspondentie_postcode','correspondentie_straatnaam','correspondentie_huisnummer','correspondentie_postcodewoonplaats','correspondentie_woonplaats'];
            } else {
                return ['correspondentie_adres_buitenland1'];
            }
        },
    },
    constraint_methods => {
        'dossiernummer'                     => qr/^\d{6,8}$/,
        'subdossiernummer'                  => qr/^\d{4}$/,
        'vestigingsnummer'                  => qr/^\d{1,12}$/,
        'hoofdvestiging_dossiernummer'      => qr/^\d*$/,
        'hoofdvestiging_subdossiernummer'   => qr/^\d*$/,

        'handelsnaam'                       => qr/^.{0,150}$/,
        'rechtsvorm'                        => qr/^\d{0,3}$/,

        'hoofdactiviteitencode'             => qr/^\d{0,6}$/,
        'nevenactiviteitencode1'            => qr/^\d{0,6}$/,
        'nevenactiviteitencode2'            => qr/^\d{0,6}$/,

        'vestiging_adres'                   => qr/^.{0,512}$/,
        'vestiging_straatnaam'              => qr/^.{0,255}$/,
        'vestiging_huisnummer'              => qr/^\d{0,6}$/,
        'vestiging_huisletter'              => qr/^[a-zA-Z]+$/,
        'vestiging_huisnummertoevoeging'    => qr/^.{0,12}$/,
        'vestiging_postcodewoonplaats'      => qr/^.{0,512}$/,
        'vestiging_postcode'                => qr/^\d{4}[A-Z]{2}$/i,
        'vestiging_woonplaats'              => qr/^.{0,512}$/,

        'correspondentie_adres'                 => qr/^.{0,512}$/,
        'correspondentie_straatnaam'            => qr/^.{0,255}$/,
        'correspondentie_huisnummer'            => qr/^.{0,6}$/,
        'correspondentie_huisletter'            => qr/^[a-zA-Z]+$/,
        'correspondentie_huisnummertoevoeging'  => qr/^.{0,12}$/,
        'correspondentie_postcodewoonplaats'    => qr/^.{0,512}$/,
        'correspondentie_postcode'              => qr/^\d{4}[A-Z]{2}$/i,
        'correspondentie_woonplaats'            => qr/^.{0,512}$/,
    },
    defaults => {
        vestiging_adres => sub {
            my ($dfv) = @_;

            if ($dfv->get_filtered_data->{'vestiging_straatnaam'}) {
                return
                    $dfv->get_filtered_data->{'vestiging_straatnaam'} . ' ' .
                    $dfv->get_filtered_data->{'vestiging_huisnummer'} .
                    ($dfv->get_filtered_data->{'vestiging_huisnummertoevoeging'}
                        ?  ' ' .  $dfv->get_filtered_data->{'vestiging_huisnummertoevoeging'}
                        : ''
                    );
            } elsif ($dfv->get_filtered_data->{'adres_buitenland1'}) {
                my @address      = grep(
                    { $dfv->get_filtered_data->{$_} }
                    qw/
                        vestiging_adres_buitenland1
                        vestiging_adres_buitenland2
                        vestiging_adres_buitenland3
                    /
                );

                return join(' ', @address);
            }

            return undef;
        },
        vestiging_postcodewoonplaats => sub {
            my ($dfv) = @_;

            return unless $dfv->get_filtered_data->{'vestiging_postcode'};

            return
                $dfv->get_filtered_data->{'vestiging_postcode'} . ' ' .
                $dfv->get_filtered_data->{'vestiging_woonplaats'};
        },
        correspondentie_adres => sub {
            my ($dfv) = @_;

            if ($dfv->get_filtered_data->{'correspondentie_straatnaam'}) {
                return
                    $dfv->get_filtered_data->{'correspondentie_straatnaam'} . ' ' .
                    $dfv->get_filtered_data->{'correspondentie_huisnummer'} .
                    ($dfv->get_filtered_data->{'correspondentie_huisnummertoevoeging'}
                        ?  ' ' .  $dfv->get_filtered_data->{'correspondentie_huisnummertoevoeging'}
                        : ''
                    );
            } elsif ($dfv->get_filtered_data->{'adres_buitenland1'}) {
                my @address      = grep(
                    { $dfv->get_filtered_data->{$_} }
                    qw/
                        correspondentie_adres_buitenland1
                        correspondentie_adres_buitenland2
                        correspondentie_adres_buitenland3
                    /
                );

                return join(' ', @address);
            }

            return undef;
        },
#        telefoonnummer => sub {
#            my ($dfv) = @_;
#
#            return
#                ($dfv->get_input_data->{'telefoonnummer_netnummer'} || '') . '-' .
#                ($dfv->get_input_data->{'telefoonnummer_nummer'} || '');
#        },
        fulldossiernummer => sub {
            my ($dfv) = @_;

            return unless $dfv->get_filtered_data->{'dossiernummer'};

            return
                ($dfv->get_filtered_data->{'dossiernummer'} || '') .
                ($dfv->get_filtered_data->{'subdossiernummer'} || '');
        },
        correspondentie_postcodewoonplaats => sub {
            my ($dfv) = @_;

            return unless (
                $dfv->get_filtered_data->{'correspondentie_postcode'} &&
                $dfv->get_filtered_data->{'correspondentie_woonplaats'}
            );

            return
                $dfv->get_filtered_data->{'correspondentie_postcode'} . ' ' .
                $dfv->get_filtered_data->{'correspondentie_woonplaats'};
        },
        hoofdvestiging_dossiernummer => sub {
            return unless $_[0]->get_filtered_data->{dossiernummer};

            return $_[0]->get_filtered_data->{dossiernummer};
        },
    },
    field_filters     => {
        'werkzamepersonen'  => sub {
            my ($field) = @_;

            $field =~ s/^0*//;

            return $field;
        },
        'kamernummer'  => sub {
            my ($field) = @_;

            $field =~ s/^0*//;

            return $field;
        },
        'vestigingsnummer'  => sub {
            my ($field) = @_;

            $field =~ s/^0*//;

            return $field;
        },
        'rechtsvorm'  => sub {
            my ($field) = @_;

            $field =~ s/^0*//;

            return $field;
        },
        'vestiging_huisnummer'  => sub {
            my ($field) = @_;

            $field =~ s/^0*//;

            return $field;
        },
        'surseance'  => sub {
            my ($field) = @_;

            if (lc($field) eq 'y') {
                return 1;
            } else {
                return 0;
            }

            return $field;
        },
        'telefoonnummer'  => sub {
            my ($field) = @_;

            $field =~ s/\-//;
            $field =~ s/ //;

            return substr($field, 0, 10);
        },
        'vestiging_postcode'    => sub {
            my ($field) = @_;

            $field = uc($field);
            $field =~ s/\s*//g;

            return $field;
        },
        'correspondentie_postcode'    => sub {
            my ($field) = @_;

            $field = uc($field);
            $field =~ s/\s*//g;

            return $field;
        },
    },
    msgs    => PARAMS_PROFILE_MESSAGES_SUB,
};

use constant ZAAK_SCOPE_ADMIN => [qw[zaak_beheer]];
use constant ZAAK_SCOPE_EDIT  => [qw[zaak_beheer zaak_edit]];
use constant ZAAK_SCOPE_READ  => [qw[zaak_beheer zaak_edit zaak_read]];

use constant ZAAKSYSTEEM_CASE_ACTION_AUTHORIZATION_MATRIX => {
    # In every context, for every action, the case cannot be deleted
    _               => {
        _           => [
            { scope => undef,               conditions => [qw[is_extant]] },
        ],

        read        => [
            { scope => ZAAK_SCOPE_ADMIN,    conditions => [] },
            { scope => ZAAK_SCOPE_EDIT,     conditions => [] },
            { scope => ZAAK_SCOPE_READ,     conditions => [] },
            { scope => undef,               conditions => [qw[user_is_assignee]] },
            { scope => undef,               conditions => [qw[user_is_coordinator]] },
            { scope => undef,               conditions => [qw[user_is_requestor]] },
        ],
    },

    attributes      => {
        update      => [
            { scope => ZAAK_SCOPE_ADMIN,    conditions => [qw[is_editable]] },
            { scope => ZAAK_SCOPE_EDIT,     conditions => [qw[is_editable has_assignee has_coordinator]] },
            { scope => undef,               conditions => [qw[is_editable user_is_assignee]] },
            { scope => undef,               conditions => [qw[is_editable has_assignee user_is_coordinator]] },
        ],
    },

    price           => {
        update      => [
            { scope => ZAAK_SCOPE_ADMIN,    conditions => [qw[is_editable]] },
        ],
    },

    confidentiality => {
        update      => [
            { scope => ZAAK_SCOPE_ADMIN,    conditions => [] },
            { scope => ZAAK_SCOPE_EDIT,     conditions => [qw[is_editable]] },
        ],
    },

    result          => {
        update      => [
            { scope => ZAAK_SCOPE_ADMIN,    conditions => [qw[is_editable]] },
            { scope => ZAAK_SCOPE_EDIT,     conditions => [qw[is_editable is_unresolved]] },
        ],
    },

    phase           => {
        transition  => [
            { scope => ZAAK_SCOPE_EDIT,     conditions => [qw[is_editable]] },
        ],
    },

    status          => {
        update      => [
            { scope => ZAAK_SCOPE_ADMIN,    conditions => [] },
        ],
    },

    meta            => {
        touch       => [
            { scope => ZAAK_SCOPE_ADMIN,    conditions => [] },
        ],
    },
};

use constant GEGEVENSMAGAZIJN_KVK_RECHTSVORMCODES   => {
    map { $_->{code} => $_->{label} } @{ LEGAL_ENTITY_TYPE_TABLE() }
};

### Contactkanalen
use constant ZAAKSYSTEEM_CONTACTKANAAL_BALIE        => 'balie';
use constant ZAAKSYSTEEM_CONTACTKANAAL_TELEFOON     => 'telefoon';
use constant ZAAKSYSTEEM_CONTACTKANAAL_POST         => 'post';
use constant ZAAKSYSTEEM_CONTACTKANAAL_EMAIL        => 'email';
use constant ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM      => 'webformulier';
use constant ZAAKSYSTEEM_CONTACTKANAAL_BEHANDELAAR  => 'behandelaar';
use constant ZAAKSYSTEEM_CONTACTKANAAL_SOCIALMEDIA  => 'sociale media';
use constant ZAAKSYSTEEM_CONTACTKANAAL_EXTERNAL_APP => 'externe applicatie';

### Hoofd en deelzaken
use constant ZAAKSYSTEEM_SUBZAKEN_DEELZAAK          => 'deelzaak';
use constant ZAAKSYSTEEM_SUBZAKEN_GERELATEERD       => 'gerelateerd';
use constant ZAAKSYSTEEM_SUBZAKEN_VERVOLGZAAK       => 'vervolgzaak';

use constant CASE_PAYMENT_STATUS_FAILED             => 'failed';
use constant CASE_PAYMENT_STATUS_SUCCESS            => 'success';
use constant CASE_PAYMENT_STATUS_PENDING            => 'pending';
use constant CASE_PAYMENT_STATUS_OFFLINE            => 'offline';

use constant STATUS_LABELS => {
    new         => 'Nieuw',
    open        => 'In behandeling',
    stalled     => 'Opgeschort',
    deleted     => 'Vernietigd',
    resolved    => 'Afgehandeld',
    overdragen  => 'Te bewaren of overdragen',
    vernietigen => 'Te vernietigen',
};

use constant ZAAKSYSTEEM_CONSTANTS  => {
    'zaaksysteem_about' => [ qw/
        applicatie
        omschrijving
        leverancier
        versie
        licentie
        startdatum
    /],
    'mimetypes'         => {
        'default'                                   => 'icon-txt-32.gif',
        'dir'                                       => 'icon-folder-32.gif',
        'application/msword'                        => 'icon-doc-32.gif',
        'application/pdf'                           => 'icon-pdf-32.gif',
        'application/msexcel'                       => 'icon-xls-32.gif',
        'application/vnd.ms-excel'                  => 'icon-xls-32.gif',
        'application/vnd.ms-powerpoint'             => 'icon-ppt-32.gif',
        'text/email'                                => 'icon-email-32.gif',
        'image/jpeg'                                => 'icon-jpg-32.gif',
        'application/vnd.oasis.opendocument.text'   => 'icon-odt-32.gif',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'icon-doc-32.gif',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'icon-ppt-32.gif',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'icon-xls-32.gif',
    },
    'zaken_statussen'   => ZAKEN_STATUSSEN,
    'status_labels'     => STATUS_LABELS,
    'contactkanalen'    => [
        ZAAKSYSTEEM_CONTACTKANAAL_BEHANDELAAR,
        ZAAKSYSTEEM_CONTACTKANAAL_BALIE,
        ZAAKSYSTEEM_CONTACTKANAAL_TELEFOON,
        ZAAKSYSTEEM_CONTACTKANAAL_POST,
        ZAAKSYSTEEM_CONTACTKANAAL_EMAIL,
        ZAAKSYSTEEM_CONTACTKANAAL_WEBFORM,
        ZAAKSYSTEEM_CONTACTKANAAL_SOCIALMEDIA,
        ZAAKSYSTEEM_CONTACTKANAAL_EXTERNAL_APP,
    ],
    'payment_statuses'  => {
        CASE_PAYMENT_STATUS_FAILED() => 'Niet geslaagd',
        CASE_PAYMENT_STATUS_SUCCESS() => 'Geslaagd',
        CASE_PAYMENT_STATUS_PENDING() => 'Wachten op bevestiging',
        CASE_PAYMENT_STATUS_OFFLINE() => 'Later betalen',
    },
    'subzaken_deelzaak'         => ZAAKSYSTEEM_SUBZAKEN_DEELZAAK,
    'subzaken_gerelateerd'      => ZAAKSYSTEEM_SUBZAKEN_GERELATEERD,
    'subzaken_vervolgzaak'      => ZAAKSYSTEEM_SUBZAKEN_VERVOLGZAAK,
    'authenticatedby'   => {
        'digid'         => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID,
        'bedrijfid'     => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID,
        'behandelaar'   => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR,
        'gba'           => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_GBA,
        'kvk'           => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_KVK,
        'twofactor'     => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_TWOFACTOR,
    },
    'mail_rcpt' => [
        {
            name => 'aanvrager',
            label => 'Aanvrager'
        },
        {
            name => 'zaak_behandelaar',
            label => 'Behandelaar',
        },
        {
            name => 'coordinator',
            label => 'Co&ouml;rdinator'
        },
        {
            name => 'behandelaar',
            label => 'Collega',
        },
        {
            name => 'gemachtigde',
            label => 'Gemachtigde',
        },
        {
            name => 'betrokkene',
            label => 'Betrokkene',
        },
        {
            name => 'overig',
            label => 'Overig',
        }
    ],
    'zaaktype'  => {
        'zt_trigger'    => {
            extern  => 'Extern',
            intern  => 'Intern',
        },
        'betrokkenen'   => {
            'niet_natuurlijk_persoon'       => 'Niet natuurlijk persoon',
            'natuurlijk_persoon'            => 'Natuurlijk persoon',
            'natuurlijk_persoon_na'         => 'Natuurlijk persoon (Ongeauthoriseerd)',
            'medewerker'                    => 'Behandelaar',
            'org_eenheid'                   => 'Organisatorische eenheid',
        },

        deelvervolg_eigenaar => {
            behandelaar => { label => 'Behandelaar van de huidige zaak' },
            aanvrager   => { label => 'Aanvrager van de huidige zaak' },
            ontvanger   => { label => 'Ontvanger van de huidige zaak' },
            betrokkene  => { label => 'Betrokkene van de huidige zaak' },
            anders      => { label => 'Andere aanvrager' }
        }
    },

    ### Explanation of attributes for veld_opties:
    ###
    ### label                   : Human description
    ### object_search_type      : object type of this attribute
    ### allow_default_value     : toestaan voorinvulling
    ### allow_multiple_instances: toon optie "Meerdere waarden toegestaan" in kenmerkbeheer
    ### is_multiple_value       : is altijd een "meerdere waarden toestaan" veld, *DEPRECATED*, op dit
    ###                           moment enkel de bag_adresSEN, bag_openbareruimteS en bag_straat_adresSEN

    'veld_opties' => {
        'bankaccount' => {
            'label'              => 'Rekeningnummer',
            'object_search_type' => 'text',
        },
        'email' => {
            'label'              => 'E-mail',
            'object_search_type' => 'text',
        },
        'url' => {
            'label'               => 'Webadres',
            'allow_default_value' => 1,
            'object_search_type'  => 'text',
        },
        'text' => {
            'label'                    => 'Tekstveld',
            'allow_multiple_instances' => 1,
            'allow_default_value'      => 1,
            'object_search_type'  => 'text',
        },
        'richtext' => {
            'label'                    => 'Rich text',
            'allow_multiple_instances' => 0,
            'object_search_type'  => 'text',
        },
        'image_from_url' => {
            'label'                    => 'Afbeelding (URL)',
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'  => 'text',
        },
        'text_uc' => {
            'label'                    => 'Tekstveld (HOOFDLETTERS)',
            'allow_multiple_instances' => 1,
            'allow_default_value'      => 1,
            'object_search_type'       => 'text',
        },
        'numeric' => {
            'label'                    => 'Numeriek',
            'constraint'               => qr/^\d*$/,
            'allow_multiple_instances' => 1,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'valuta' => {
            'label'                    => 'Valuta',
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'valutain' => {
            'label'                    => 'Valuta (inclusief BTW)',
            'type'                     => 'valuta',
            'options'                  => { 'btwin' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'valutaex' => {
            'label'                    => 'Valuta (exclusief BTW)',
            'type'                     => 'valuta',
            'options'                  => { 'btwex' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'valutain6' => {
            'label'                    => 'Valuta (inclusief BTW (6))',
            'type'                     => 'valuta',
            'options'                  => { 'btwin' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'valutaex6' => {
            'label'                    => 'Valuta (exclusief BTW (6))',
            'type'                     => 'valuta',
            'options'                  => { 'btwex' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'valutain21' => {
            'label'                    => 'Valuta (inclusief BTW (21))',
            'type'                     => 'valuta',
            'options'                  => { 'btwin' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'valutaex21' => {
            'label'                    => 'Valuta (exclusief BTW (21))',
            'type'                     => 'valuta',
            'options'                  => { 'btwex' => 1, },
            'constraint'               => qr/^[\d,.]*$/,
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'number',
            'object_search_filter'     => \&_numeric_fix_filter,
        },
        'date' => {
            'label'                    => 'Datum',
            'type'                     => 'datetime',
            'allow_multiple_instances' => 0,
            'allow_default_value'      => 1,
            'object_search_type'       => 'timestamp',
            'object_search_filter'     => sub {
                my ($schema, $value) = @_;

                return if not $value;

                my $rv;
                my @formats = ("%d-%m-%Y", "%Y-%m-%d", "%Y%m%d");
                while(@formats && !$rv) {
                    $rv = eval { strptime(pop @formats, $value) };
                }

                return $rv;
            },
        },
        'googlemaps' => {
            'label'               => 'Adres (Google Maps)',
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'object_search_type'  => 'text',
        },
        'textarea' => {
            'label'                    => 'Groot tekstveld',
            'allow_multiple_instances' => 1,
            'allow_default_value'      => 1,
            'object_search_type'  => 'text',
        },
        'option' => {
            'multiple'            => 1,
            'label'               => 'Enkelvoudige keuze',
            'allow_default_value' => 1,
            'object_search_type'  => 'text',
        },
        'select' => {
            'multiple'                 => 1,
            'label'                    => 'Keuzelijst',
            'allow_multiple_instances' => 1,
            'allow_default_value'      => 1,
            'object_search_type'       => 'text',
        },
        'checkbox' => {
            'multiple'            => 1,
            'label'               => 'Meervoudige keuze',
            'multiple_values_per_value' => 1,
            'allow_default_value' => 0,
            'object_search_type'  => 'text',
        },
        'file' => {
            'label'               => 'Document',
            'allow_default_value' => 0,
            'object_search_type'  => 'text',
        },
        'subject' => {
            label => 'Betrokkene',
            object_search_type => 'text',
            allow_default_value => 0
        },
        'appointment' => {
            'label'               => 'Kalenderafspraak',
            'object_search_type'  => 'text',
            'allow_default_value' => 0,
        },
        'calendar' => {
            'label'               => 'Kalender afspraak (QMatic)',
            'allow_default_value' => 0,
            'object_search_type'  => 'text',
        },
        'calendar_supersaas' => {
            'label'               => 'Kalender afspraak (SuperSaaS)',
            'allow_default_value' => 0,
            'object_search_type'  => 'text',
        },
        'bag_straat_adres' => {
            'label'               => 'Adres (dmv straatnaam) (BAG)',
            'object_search_type'  => 'text',
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'bag_straat_adressen' => {
            'label'               => 'Adressen (dmv straatnaam) (BAG)',
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'is_multiple_value'   => 1,
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'bag_adres' => {
            'label'               => 'Adres (dmv postcode) (BAG)',
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'bag_adressen' => {
            'label'               => 'Adressen (dmv postcode) (BAG)',
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'is_multiple_value'   => 1,
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'bag_openbareruimte' => {
            'label'               => 'Straat (BAG)',
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'bag_openbareruimtes' => {
            'label'               => 'Straten (BAG)',
            'can_zaakadres'       => 1,
            'allow_default_value' => 0,
            'is_multiple_value'   => 1,
            'object_search_type'  => 'bag',
            'object_search_filter' => \&_bag_filter,
        },
        'geolatlon' => {
            'label'                 => 'Locatie met kaart',
            'allow_default_value'   => 0,
            'multiple'              => 0,
        },
    },
    'document'      => {
        'categories'        => [qw/
            Advies
            Afbeelding
            Audio
            Begroting
            Behandelvoorstel
            Beleidsnota
            Besluit
            Bewijsstuk
            Brief
            Contract
            Document
            E-mail
            Envelop
            Factuur
            Formulier
            Foto
            Legitimatie
            Memo
            Offerte
            Presentatie
            Procesverbaal
            Product
            Projectplan
            Rapport
            Tekening
            Uittreksel
            Vergaderdocument
            Verslag
            Video
            Anders
        /],
        'types'             => {
            file        => {},
            mail        => {},
            dir         => {},
            sjabloon    => {},
        },
        'sjabloon'  => {
            'export_types'  => {
                'odt'   => {
                    mimetype    => 'application/vnd.oasis.opendocument.text',
                    label       => 'OpenDocument',
                },
                'pdf'   => {
                    mimetype    => 'application/pdf',
                    label       => 'PDF',
                },
                'doc'   => {
                    mimetype    => 'application/msword',
                    label       => 'MS Word',
                }
            },
        },
        'sjabloon_xential'  => {
            'export_types'  => {
                'odt'   => {
                    mimetype    => 'application/vnd.oasis.opendocument.text',
                    label       => 'OpenDocument',
                },
                'pdf'   => {
                    mimetype    => 'application/pdf',
                    label       => 'PDF',
                },
                'doc'   => {
                    mimetype    => 'application/msword',
                    label       => 'MS Word',
                }
            },
        },
    },
    'kvk_rechtsvormen'          => GEGEVENSMAGAZIJN_KVK_RECHTSVORMCODES,
    'kvk_rechtsvormen_enabled'  => [qw/
        1
        7
        11
        21
        41
        51
        55
        70
        73
        74
        88
        201
        202
    /],
    # make a hash because checkbox widget expect that
    vertrouwelijkheidsaanduiding_options => {map { $_ => $_ } (
        '-',
        'Openbaar',
        'Beperkt openbaar',
        'Intern',
        'Zaakvertrouwelijk',
        'Vertrouwelijk',
        'Confidentieel',
        'Geheim',
        'Zeer geheim'
    )},
    # seemingly convoluted but useful for making sure the same options exist
    # in the rules and casetype admin screens
    casetype_boolean_property_options => {map {$_ => $_} ('Nee', 'Ja') },
    CASETYPE_RULE_PROPERTIES => [qw/
        vertrouwelijkheidsaanduiding
        beroep_mogelijk
        publicatie
        bag
        lex_silencio_positivo
        opschorten_mogelijk
        verlenging_mogelijk
        wet_dwangsom
        wkpb
    /],
    confidentiality => {
        public => 'Openbaar',
        internal => 'Intern',
        confidential => 'Vertrouwelijk'
    },
    CHILD_CASETYPE_OPTIONS => [
        {
            "value" => "betrokkenen",
            "label" => "Relaties"
        },
        {
            "value" => "actions",
            "label" => "Acties"
        },
        {
            "value" => "allocation",
            "label" => "Toewijzing (Fase Registreren)"
        },
        {
            "value" => "first_phase",
            "label" => "Fase Registreren"
        },
        {
            "value" => "last_phase",
            "label" => "Fase Afhandelen"
        },
        {
            "value" => "resultaten",
            "label" => "Resultaten"
        },
        {
            "value" => "authorisaties",
            "label" => "Rechten"
        },
    ]
};

use constant ZAAKSYSTEEM_AUTHORIZATION_PERMISSIONS => {
    'admin'                     => {
        'label'             => 'Administrator',
        'is_systeem_recht'  => 0,
    },
    'gebruiker'                 => {
        'label'             => 'Gebruiker',
        'is_systeem_recht'  => 0,
    },
    'dashboard'                 => {},
    'zaak_intake'               => {},
    'zaak_eigen'                => {},
    'zaak_afdeling'             => {},
    'search'                    => {},
    'plugin_mgmt'               => {},
    'contact_nieuw'             => {},
    'contact_search'            => {},
    'beheer_gegevens_admin'     => {},
    'beheer_basisregistratie'   => {},
    'beheer_plugin_admin'       => {},
    'vernietigingslijst'        => {},
    'view_sensitive_data' => {
        label            => 'Mag (privacy) gevoelige data inzien',
        is_systeem_recht => 0,
    },
    'zaak_add'                  => {
        'label'             => 'Mag zaak aanmaken',
        'deprecated'        => 1,
    },
    'zaak_edit'                  => {
        'label'             => 'Mag zaken behandelen',
        'is_systeem_recht'  => 1,
    },
    'zaak_read'                  => {
        'label'             => 'Mag zaken raadplegen',
        'is_systeem_recht'  => 1,
    },
    'zaak_beheer'                  => {
        'label'             => 'Mag zaken beheren',
        'is_systeem_recht'  => 1,
    },
    'view_sensitive_contact_data' => {
        'label'                     => 'Mag gevoelige persoonsgegevens zien',
        'is_systeem_recht'          => 1,
    }
};

use constant ZAAKSYSTEEM_BETROKKENE_KENMERK => {
    id                  => {
        'bedrijf'               => 'id',
        'medewerker'            => 'id',
        'natuurlijk_persoon'    => 'id'
    },
    btype               => {
        'bedrijf'               => 'btype',
        'medewerker'            => 'btype',
        'natuurlijk_persoon'    => 'btype'
    },
    naam                => {
        'bedrijf'               => 'handelsnaam',
        'medewerker'            => 'naam',
        'natuurlijk_persoon'    => 'naam'
    },
    handelsnaam         => {
        'bedrijf'               => 'handelsnaam',
    },
    display_name        => {
        'bedrijf'               => 'display_name',
        'medewerker'            => 'display_name',
        'natuurlijk_persoon'    => 'display_name'
    },
    kvknummer           => {
        'bedrijf'               => 'dossiernummer',
    },
    burgerservicenummer => {
        'natuurlijk_persoon'    => 'bsn',
    },
    a_nummer => {
        'natuurlijk_persoon'    => 'a_nummer',
    },
    login     => {
        'bedrijf'               => 'login',
    },
    password  => {
        'bedrijf'               => 'password',
    },
    'achternaam' => {
        'medewerker'            => 'geslachtsnaam',
        'natuurlijk_persoon'    => 'achternaam'
    },
    'volledigenaam' => {
        'medewerker'            => 'display_name',
        'natuurlijk_persoon'    => 'volledige_naam'
    },
    'geslachtsnaam' => {
        'medewerker'            => 'geslachtsnaam',
        'natuurlijk_persoon'    => 'geslachtsnaam'
    },
    'voorvoegsel' => {
        'natuurlijk_persoon'    => 'voorvoegsel'
    },
    'voornamen'   => {
        'natuurlijk_persoon'    => 'voornamen',
        'medewerker'            => 'voornamen',
    },
    'noble_title' => {
        'natuurlijk_persoon'    => 'adellijke_titel'
    },
    'geslacht'    => {
        'natuurlijk_persoon'    => 'geslacht'
    },
    'aanhef'      => {
        'natuurlijk_persoon'    => 'aanhef'
    },
    'aanhef1'     => {
        'natuurlijk_persoon'    => 'aanhef1'
    },
    'aanhef2'     => {
        'natuurlijk_persoon'    => 'aanhef2'
    },
    'straat'      => {
        'natuurlijk_persoon'    => 'straatnaam',
        'bedrijf'               => 'straatnaam',
        'medewerker'            => 'straatnaam'
    },
    'huisnummer'  => {
        'natuurlijk_persoon'    => 'volledig_huisnummer',
        'bedrijf'               => 'volledig_huisnummer',
        'medewerker'            => 'huisnummer'
    },
    'postcode'    => {
        'natuurlijk_persoon'    => 'postcode',
        'bedrijf'               => 'postcode',
        'medewerker'            => 'postcode'
    },
    'woonplaats'  => {
        'natuurlijk_persoon'    => 'woonplaats',
        'bedrijf'               => 'woonplaats',
        'medewerker'            => 'woonplaats'
    },
    'verblijf_straat'      => {
        'natuurlijk_persoon'    => 'verblijf_straatnaam',
        'bedrijf'               => 'vestiging_straatnaam',
        'medewerker'            => 'straatnaam'
    },
    'verblijf_huisnummer'  => {
        'natuurlijk_persoon'    => 'verblijf_volledig_huisnummer',
        'bedrijf'               => 'vestiging_volledig_huisnummer',
        'medewerker'            => 'huisnummer'
    },
    'verblijf_postcode'    => {
        'natuurlijk_persoon'    => 'verblijf_postcode',
        'bedrijf'               => 'vestiging_postcode',
        'medewerker'            => 'postcode'
    },
    'verblijf_woonplaats'  => {
        'natuurlijk_persoon'    => 'verblijf_woonplaats',
        'bedrijf'               => 'vestiging_woonplaats',
        'medewerker'            => 'woonplaats'
    },
    'correspondentie_straat'      => {
        'natuurlijk_persoon'    => 'correspondentie_straatnaam',
        'bedrijf'               => 'correspondentie_straatnaam',
        'medewerker'            => 'straatnaam'
    },
    'correspondentie_huisnummer'  => { #TODO
        'natuurlijk_persoon'    => 'correspondentie_volledig_huisnummer',
        'bedrijf'               => 'correspondentie_volledig_huisnummer',
        'medewerker'            => 'huisnummer'
    },
    'correspondentie_postcode'    => {
        'natuurlijk_persoon'    => 'correspondentie_postcode',
        'bedrijf'               => 'correspondentie_postcode',
        'medewerker'            => 'postcode'
    },
    'correspondentie_woonplaats'  => {
        'natuurlijk_persoon'    => 'correspondentie_woonplaats',
        'bedrijf'               => 'correspondentie_woonplaats',
        'medewerker'            => 'woonplaats'
    },
    'tel'           => {
        'natuurlijk_persoon'    => 'telefoonnummer',
        'bedrijf'               => 'telefoonnummer',
        'medewerker'            => 'telefoonnummer'
    },
    'mobiel'       => {
        'natuurlijk_persoon'    => 'mobiel',
        'bedrijf'               => 'mobiel',
    },
    'email'       => {
        'natuurlijk_persoon'    => 'email',
        'bedrijf'               => 'email',
        'medewerker'            => 'email'
    },
    'geboortedatum'             => {
        'natuurlijk_persoon'    => 'geboortedatum',
    },
    'geboorteplaats'             => {
        'natuurlijk_persoon'    => 'geboorteplaats',
    },
    'geboorteland'             => {
        'natuurlijk_persoon'    => 'geboorteland',
    },
    'datum_huwelijk'            => {
        'natuurlijk_persoon'    => 'datum_huwelijk',
    },
    'datum_overlijden'            => {
        'natuurlijk_persoon'    => 'datum_overlijden',
    },
    'is_verhuisd' => {
        'natuurlijk_persoon'    => 'is_verhuisd'
    },
    'is_briefadres' => {
        'natuurlijk_persoon'    => 'is_briefadres',
        'bedrijf'               => 'is_briefadres'
    },
    'indicatie_geheim' => {
        'natuurlijk_persoon'    => 'indicatie_geheim'
    },
    'afdeling'              => {
        'medewerker'            => 'afdeling'
    },
    'type'        => {
        'natuurlijk_persoon'    => 'human_type',
        'bedrijf'               => 'human_type',
        'medewerker'            => 'human_type'
    },
    naamgebruik => {
        natuurlijk_persoon      => 'achternaam',
        medewerker              => 'geslachtsnaam',
    },
    in_onderzoek => {
        natuurlijk_persoon => 'in_onderzoek'
    },
    datum_huwelijk_ontbinding => {
        natuurlijk_persoon => 'datum_huwelijk_ontbinding'
    },
    rechtsvorm => {
        bedrijf => 'rechtsvorm',
    },
    vestigingsnummer => {
        bedrijf => 'vestigingsnummer'
    },

    verblijf_buitenland => {
        bedrijf            => 'adres_buitenland1',
        natuurlijk_persoon => 'adres_buitenland1',
    },
    verblijf_buitenland1 => {
        bedrijf            => 'adres_buitenland1',
        natuurlijk_persoon => 'adres_buitenland1',
    },
    verblijf_buitenland2 => {
        bedrijf            => 'adres_buitenland2',
        natuurlijk_persoon => 'adres_buitenland2',
    },
    verblijf_buitenland3 => {
        bedrijf            => 'adres_buitenland3',
        natuurlijk_persoon => 'adres_buitenland3',
    },
    verblijf_land => {
        bedrijf            => 'vestiging_landcode',
        natuurlijk_persoon => 'landcode',
    },
    vestiging_land => {
        bedrijf            => 'vestiging_landcode',
        natuurlijk_persoon => 'landcode',
    },
    'initialen' => {
        natuurlijk_persoon      => 'voorletters',
        medewerker              => 'voorletters',
    },
};

use constant ZAAKSYSTEEM_BETROKKENE_SUB     => sub {
    my ($betrokkene, $attr_name, $show_historic) = @_;

    return if (!$betrokkene || !$attr_name);

    my $attribute   = ZAAKSYSTEEM_BETROKKENE_KENMERK->{$attr_name} or return;
    my $method_name = $attribute->{ $betrokkene->btype }           or return;

    my $copy = $show_historic ? $betrokkene->historic : $betrokkene->current;

    my $code_ref = $copy->can($method_name);
    return unless $code_ref;

    my $value = $code_ref->($copy);
    return unless defined $value;
    return $value unless $attr_name =~ /_land$/;

    return RGBZ_LANDCODES()->{$value} // "Onbekende landcode: $value";

};

use constant ZAAKSYSTEEM_AUTHORIZATION_ROLES => {
    'admin'             => {
        'ldapname'          => 'Administrator',
        'rechten'       => {
            'global'        => {
                'admin'                     => 1,
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'zaak_beheer'               => 1,
                'search'                    => 1,
                'plugin_mgmt'               => 1,
                'contact_nieuw'             => 1,
                'contact_search'            => 1,
                'contact_search_extern'     => 1,
                'beheer'                    => 1,
                'beheer_gegevens_admin'     => 1,
                'beheer_zaaktype_admin'     => 1,
                'beheer_plugin_admin'       => 1,
                'vernietigingslijst'        => 1,
                'owner_signatures'          => 1,
                ### The permissions below are since our new case design. We try to make a permission
                ### per action, and not a permission for a bunch of actions. Please follow this new idea,
                ### so we can cleanup the above permissions.
                'case_allocation'           => 1,
                'view_sensitive_data'       => 1,
                'zaak_create_skip_required' => 1,
                'useradmin'                 => 1,
            }
        },
    },
    'beheerder'         => {
        'ldapname'          => 'Zaaksysteembeheerder',
        'rechten'       => {
            'global'        => {
                'admin'                     => 1,
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'zaak_beheer'               => 1,
                'search'                    => 1,
                'plugin_mgmt'               => 1,
                'contact_nieuw'             => 1,
                'contact_search'            => 1,
                'contact_search_extern'     => 1,
                'beheer'                    => 1,
                'beheer_gegevens_admin'     => 1,
                'beheer_zaaktype_admin'     => 1,
                'beheer_plugin_admin'       => 1,
                'vernietigingslijst'        => 1,
                'owner_signatures'           => 1,
                ### The permissions below are since our new case design. We try to make a permission
                ### per action, and not a permission for a bunch of actions. Please follow this new idea,
                ### so we can cleanup the above permissions.
                'case_allocation'           => 1,
                'view_sensitive_data'       => 1,
                'zaak_create_skip_required' => 1,
                'useradmin'                 => 1,
            }
        },
    },
    'gebruikersbeheerder'       => {
        'ldapname'          => 'Gebruikersbeheerder',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'documenten_intake_subject' => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'zaak_create_skip_required' => 1,
                'useradmin'                 => 1,
                # This permission is only here because the wiki says so, and
                # because frontend directs admins to the catalog first.
                'beheer_zaaktype_admin'     => 1,
            }
        },
    },
    'zaaktypebeheerder' => {
        'ldapname'          => 'Zaaktypebeheerder',
        'rechten'       => {
            'global'        => {
                'dashboard'                 => 1,
                'gebruiker'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'zaak_beheer'               => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'beheer_zaaktype_admin'     => 1,
                'objecttype_admin'          => 1,
                ### The permissions below are since our new case design. We try to make a permission
                ### per action, and not a permission for a bunch of actions. Please follow this new idea,
                ### so we can cleanup the above permissions.
                'case_allocation'           => 1,
                'view_sensitive_data'       => 1,
                'zaak_create_skip_required' => 1,
            }
        },
    },
    'zaakbeheerder' => {
        'ldapname'          => 'Zaakbeheerder',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'zaak_beheer'               => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                ### The permissions below are since our new case design. We try to make a permission
                ### per action, and not a permission for a bunch of actions. Please follow this new idea,
                ### so we can cleanup the above permissions.
                'case_allocation'           => 1,
                'view_sensitive_data'       => 1,
                'zaak_create_skip_required' => 1,
            }
        },
    },
    'contactbeheerder' => {
        'ldapname'          => 'Contactbeheerder',
        'rechten'       => {
            'global'        => {
                'contact_nieuw'             => 1,
            }
        },
    },
    'basisregistratiebeheerder' => {
        'ldapname'          => 'Basisregistratiebeheerder',
        'rechten'       => {
            'global'        => {
                'woz_objects'              => 1,
                'beheer_gegevens_admin'    => 1,
            }
        },
    },
    'wethouder'    => {
        'ldapname'          => 'Wethouder',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'directielid'    => {
        'ldapname'          => 'Directielid',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'afdelingshoofd'    => {
        'ldapname'          => 'Afdelingshoofd',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'div-medewerker'    => {
        'ldapname'          => 'Zaakbeheerder',
        'rechten'       => {
            'global'        => {
                'documenten_intake_all'     => 1,
                'documenten_intake_subject' => 1,
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_beheer'               => 1,
                'zaak_afdeling'             => 1,
                'zaak_route_default'        => 1, # Implies 'div' medewerker
                'search'                    => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
                ### The permissions below are since our new case design. We try to make a permission
                ### per action, and not a permission for a bunch of actions. Please follow this new idea,
                ### so we can cleanup the above permissions.
                'case_allocation'           => 1,
            }
        },
    },
    'kcc-medewerker'    => {
        'ldapname'          => 'Kcc-medewerker',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_intake'               => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
            }
        },
    },
    'zaakverdeler'      => {
        'ldapname'          => 'Zaakverdeler',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_intake'               => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'plugin_mgmt'               => 1,
                ### The permissions below are since our new case design. We try to make a permission
                ### per action, and not a permission for a bunch of actions. Please follow this new idea,
                ### so we can cleanup the above permissions.
                'case_allocation'           => 1,
            }
        },
    },
    'behandelaar'       => {
        'ldapname'          => 'Behandelaar',
        'rechten'       => {
            'global'        => {
                'gebruiker'                 => 1,
                'documenten_intake_subject' => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'zaak_afdeling'             => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                'contact_edit_subset'       => 1
            }
        },
    },
    'brp_externe_bevrager'    => {
        'ldapname'            => 'BRP externe bevrager',
        'rechten'       => {
            'global'        => {
                'contact_search'            => 1,
                'contact_search_extern'     => 1,
            }
        },
    },
    'app_gebruiker' => {
        ldapname => 'App gebruiker',
        rechten  => {
            global => {
                gebruiker     => 1,
            },
        },
    },
    'documentintaker'    => {
        'ldapname'          => 'Documentintaker',
        'rechten'       => {
            'global'        => {
                'documenten_intake_all'     => 1,
                'documenten_intake_subject' => 1,
                'gebruiker'                 => 1,
                'dashboard'                 => 1,
                'zaak_eigen'                => 1,
                'search'                    => 1,
                'contact_search'            => 1,
                # Skip required fields on case creation
                zaak_create_skip_required => 1,
            }
        },
    },
    'persoonsverwerker' => {
        'ldapname'          => 'Persoonsverwerker',
        'rechten'           => {
            'global'            => {
                'view_sensitive_contact_data' => 1,
            }
        }
    },
};

use constant ZAAKSYSTEEM_AUTHORIZATIONS => [
    uniq map {
        keys %{ $_->{rechten}{global} }
    } values %{ ZAAKSYSTEEM_AUTHORIZATION_ROLES() }
];

use constant PARAMS_PROFILE_DEFAULT_MSGS => PARAMS_PROFILE_MESSAGES_SUB;

use constant DEFAULT_KENMERKEN_GROUP_DATA => {
    help        => 'Vul de benodigde velden in voor uw zaak',
    label       => 'Benodigde gegevens',
};

use constant SEARCH_QUERY_SESSION_VAR => 'SearchQuery_search_query_id';
use constant SEARCH_QUERY_TABLE_NAME  => 'DB::SearchQuery';

use constant VALIDATION_CONTACT_DATA    => {
    optional    => [qw/
        npc-telefoonnummer
        npc-email
        npc-mobiel
    /],
    constraint_methods  => {
        'npc-email'                 => qr/^.+?\@.+\.[a-z0-9]{2,}$/,
        'npc-telefoonnummer'        => qr/^\+?\d{10,15}$/,
        'npc-mobiel'                => qr/^\+?\d{10,15}$/,
    },
    msgs                => {
        'format'    => '%s',
        'missing'   => 'Veld is verplicht.',
        'invalid'   => 'Veld is niet correct ingevuld.',
        'constraints' => {
            '(?-xism:^\d{4}[A-Z]{2}$)' => 'Postcode zonder spatie (1000AA)',
            '(?-xism:^[\d\+]{6,15}$)' => 'Nummer zonder spatie (e.g: +312012345678)',
        }
    }, };

use constant ZAAKTYPE_TRIGGER => sub {
    my $trigger = pop;
    return grep { $trigger eq $_ } @{ZAAKSYSTEEM_OPTIONS->{TRIGGERS}};
};

use constant ZAAK_CONFIDENTIALITY => sub {
    my $c = pop;
    return grep { $c eq $_ } keys %{ZAAKSYSTEEM_CONSTANTS->{confidentiality}}
};

# TODO: Rename to ASSERT_BETROKKENE

use constant VALIDATION_EXTERNAL_CONTACT_DATA => sub {
    my $zaaktype_node = shift;
    my $profile = {
        required           => [],
        optional           => [],
        constraint_methods => VALIDATION_CONTACT_DATA->{constraint_methods},
        msgs               => VALIDATION_CONTACT_DATA->{msgs},
    };

    my %mapping = (
        contact_info_email_required        => 'npc-email',
        contact_info_phone_required        => 'npc-telefoonnummer',
        contact_info_mobile_phone_required => 'npc-mobiel',
    );

    for my $contact_type (keys %mapping) {
        if ($zaaktype_node->$contact_type) {
            push @{ $profile->{required} }, $mapping{$contact_type};
        }
        else {
            push @{ $profile->{optional} }, $mapping{$contact_type};
        }
    }

    return $profile;
};

use constant ZAAK_CREATE_PROFILE_BETROKKENE => sub {
    my $val = pop;

    # TODO: Figure this out
    my $BETROKKENE_DEFAULT_HASH = {
        'betrokkene_type'   =>
            qr/^natuurlijk_persoon|medewerker|bedrijf|org_eenheid$/,
        'betrokkene_id'     => qr/^\d+$/,
        'betrokkene'        => qr/^[\w\d-]+$/,
        'verificatie'       => qr/^(digid|medewerker|stufzkn)$/,
    };

    my @betrokkenen;
    push(@betrokkenen, $val) if UNIVERSAL::isa($val, 'HASH');
    push(@betrokkenen, $val) if blessed($val);
    push(@betrokkenen, @{ $val }) if UNIVERSAL::isa($val, 'ARRAY');

    return 0 if !@betrokkenen;

    for my $betrokkene (@betrokkenen) {
        # Object? Assume betrokkene object
        # TODO: Be more strict in checking
        next if blessed($betrokkene);

        if (!$betrokkene->{betrokkene} && !$betrokkene->{create} and
            !$betrokkene->{betrokkene_id} || !$betrokkene->{betrokkene_type}) {
                return 0;
        }

        if ($betrokkene->{create} && !$betrokkene->{betrokkene_type}) {
            return 0;
        }

        if (!$betrokkene->{verificatie}) {
            return 0;
        }
    }
    return 1;
};

use constant ZAAK_CREATE_PROFILE        => {
    required        => [ qw/
        aanvraag_trigger

        aanvragers

        registratiedatum
        contactkanaal
    /],
    'optional'      => [ qw/
        status
        milestone

        onderwerp
        resultaat
        besluit

        route_ou
        route_role

        ou_id
        role_id

        betrokkene_id
        assignee_id

        streefafhandeldatum
        afhandeldatum
        vernietigingsdatum

        coordinators
        behandelaars

        kenmerken

        created
        last_modified
        deleted

        id
        override_zaak_id

        locatie_zaak
        locatie_correspondentie

        relatie
        zaak_id

        actie_kopieren_kenmerken
        streefafhandeldatum_data

        ontvanger
        betrokkene_relaties
        bestemming

        duplicate_prevention_token
        confidentiality

        related_object
        skip_required_attributes

        subject_role
        copy_subject_role
    /],
    'require_some'  => {
        'zaaktype_id_or_zaaktype_node_id'    => [
            1,
            'zaaktype_id',
            'zaaktype_node_id'
        ],
    },
    'constraint_methods'            => {
        'status'            => sub {
            my $val     = pop;

            return 1 unless $val;

            my $statussen = ZAKEN_STATUSSEN;

            return 1 if grep { $_ eq $val } @{ $statussen };
            return;
        },
        milestone         => qr/^\d+$/,
        contactkanaal     => qr/^[\w\s]{1,128}$/,
        aanvragers        => ZAAK_CREATE_PROFILE_BETROKKENE,
        coordinators      => ZAAK_CREATE_PROFILE_BETROKKENE,
        behandelaars      => ZAAK_CREATE_PROFILE_BETROKKENE,
        aanvraag_trigger  => ZAAKTYPE_TRIGGER,
        confidentiality   => ZAAK_CONFIDENTIALITY,
        assignee_id         => sub {
            my $val = pop;

            return 1 if $val =~ /-/;
            return;
        },
        ontvanger         => sub {
            my $val = pop;

            return 1 if $val =~ /-/;
            return;
        },
        'betrokkene_relaties' => sub {
            my $val = pop;

            return 1 if UNIVERSAL::isa($val, 'HASH');
            return;
        },

        related_object => qr/^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$/i,
    },
    dependencies => {
        aanvraag_trigger => sub {
            my ($dfv, $val) = @_;

            if (   lc($val) eq 'intern'
                && lc($dfv->get_filtered_data->{'bestemming'}) eq 'extern') {
                return ['ontvanger'];
            }

            return [];
        },
    },
    dependency_groups => {
        zaak_and_relatie => ['relatie', 'zaak_id'],
    },
    defaults => {
        status                   => ZAKEN_STATUSSEN_DEFAULT,
        milestone                => 1,
        confidentiality          => 'public',
        skip_required_attributes => 0,
    },
    msgs                => sub {
        my $dfv     = shift;
        my $rv      = {};

        for my $missing ($dfv->missing) {
            $rv->{$missing}  = 'Veld is verplicht.';
        }
        for my $missing ($dfv->invalid) {
            $rv->{$missing}  = 'Veld is niet correct ingevuld.';
        }

        return $rv;
    }
};

#
# this is the configuration for importing zaaktypes from one system into another. the challenge
# is that configurations will differ, thus not every dependency is present on every system. at
# the same time, it is not helpful to just re-import any dependency that is missing, this will
# cause duplicate items. e.g. when importing a zaaktype it looks to re-link to all it's needed
# kenmerken. if one is not found, it will ask the user if the kenmerk must be imported, or that
# another kenmerk must be selected in it's place.
#
#
# when exporting, all dependencies are exported with the zaaktype.
#
# the 'match' subroutine tries to match these exported dependencies with items that are in the
# local database. it returns a filter that is used in a query.
#
#
use constant ZAAKTYPE_DEPENDENCY_IDS => {
    'zaaktype_id$'                      => 'Zaaktype',
    '_kenmerk$'                         => 'BibliotheekKenmerken',
    '^bibliotheek_kenmerken_id$'        => 'BibliotheekKenmerken',
    '^bibliotheek_sjablonen_id$'        => 'BibliotheekSjablonen',
    '^bibliotheek_categorie_id$'        => 'BibliotheekCategorie',
    '^bibliotheek_notificaties_id$'     => 'BibliotheekNotificaties',
    '^role_id$'                         => 'LdapRole',
    '^ou_id$'                           => 'LdapOu',
    '^object_id$'                       => 'ObjectData',
    '^filestore_id$'                    => 'Filestore',
    '^checklist_vraag_status_id$'       => 'ChecklistVraagStatus',
    '^actie_\d+_datum_bibliotheek_kenmerken_id$' => 'BibliotheekKenmerken',
    'standaard_betrokkenen'             => 'ZaaktypeStandaardBetrokkenen',
};

use constant ZAAKTYPE_DEPENDENCIES => {
    ChecklistVraagStatus => {
        match => ['naam'],
        name  => 'naam',
        label => 'checklistvraag',
        title => 'Checklistvraag',
        letter_e => '',
    },
    Filestore => {
        match => ['md5'],
        name  => 'original_name',
        label => 'bestand',
        title => 'Bestand',
        letter_e => '',
    },
    Zaaktype => {
        match => [],
        name  => 'zaaktype_titel',
        label => 'zaaktype',
        title => 'Zaaktype',
        letter_e => '',
        has_category => 1,
    },
    BibliotheekKenmerken => {
        match => [qw/naam deleted value_type value_mandatory value_length besluit magic_string/],
        name  => 'naam',
        label => 'kenmerk',
        title => 'Kenmerken',
        letter_e => '',
        has_category => 1,
    },
    BibliotheekNotificaties => {
        match => [qw/label subject message/],
        name  => 'label',
        label => 'bericht',
        title => 'Berichten',
        letter_e => '',
        has_category => 1,
    },
    BibliotheekSjablonen         => {
        match => [qw/naam/],
        name  => 'naam',
        label => 'sjabloon',
        letter_e => 'e',
        has_category => 1,
    },
    LdapRole => {
        match => [qw/short_name/],
        name  => 'short_name',
        label => 'rol',
        title => 'Rol',
        letter_e => 'e',
    },
    LdapOu => {
        match => [qw/ou/],
        name  => 'ou',
        label => 'organisatorische eenheid',
        letter_e => 'e',
    },
    BibliotheekCategorie        => {
        match => [qw/naam/],
        name  => 'naam',
        label => 'categorie',
        letter_e => 'e',
    },
    ObjectData => {
        match => [qw/uuid/],
        name => 'naam',
        label => 'object',
        letter_e => '',
    },
    ZaaktypeStandaardBetrokkenen => {
        match => [qw/naam/],
        name => 'naam',
        label => 'betrokkene rol',
        letter_e => 'e',
    },
};

use constant BASE_RELATION_ROLES => [
    "Advocaat",
    "Auditor",
    "Aannemer",
    "Bewindvoerder",
    "Familielid",
    "Gemachtigde",
    "Mantelzorger",
    "Ouder",
    "Verzorger",
    "Ontvanger"
];

use constant BETROKKENE_RELATEREN_PROFILE => {
    required    => [qw/
        betrokkene_identifier
        magic_string_prefix
        rol
    /],
    optional => [qw[
        pip_authorized
        send_auth_confirmation
    ]],
    msgs                => sub {
        my $dfv     = shift;
        my $rv      = {};

        for my $missing ($dfv->missing) {
            $rv->{$missing}  = 'Veld is verplicht.';
        }
        for my $missing ($dfv->invalid) {
            $rv->{$missing}  = 'Veld is niet correct ingevuld.';
        }

        return $rv;
    }
};

use constant BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION => sub {
    my (@used_columns)              = @{ shift(@_) };
    my ($prefix, $rol) = @_;

    ### make a suggestion or give back given string
    my $suggestion  = lc($prefix || $rol);
    $suggestion =~ s/[^a-z0-9]//g;

    my $counter     = '';

    while (
        grep {
            $suggestion . $counter .'_naam' eq $_ ||
            $suggestion .  $counter eq $_
        } @used_columns, Zaaksysteem::Attributes::ZAAKSYSTEEM_MAGIC_STRINGS()
             # Attributes.pm already uses Constants.pm
    ) {
        $counter = 0 if !$counter;
        $counter++;
    }

    $suggestion     .= $counter;

    return $suggestion;

};

use constant VERNIETIGINGS_REDENEN  => [
    'In belang van de aanvrager',
    'Uniek of bijzonder karakter voor de organisatie',
    'Bijzondere tijdsomstandigheid of gebeurtenis',
    'Beeldbepalend karakter',
    'Samenvatting van gegevens',
    'Betrokkene(n) is van bijzondere betekenis geweest',
    'Vervanging van stukken bij calamiteit',
    'Aanleiding van algemene regelgeving',
    'Verstoring van logische samenhang',
];



use constant ZAAK_WIJZIG_VERNIETIGINGSDATUM_PROFILE         => {
    required            => [qw/
    /],
    optional            => [qw/
        reden
        vernietigingsdatum_type
        vernietigingsdatum_recalculate
        vernietigingsdatum
    /],
    constraint_methods  => {
        vernietigingsdatum_type => sub {
            my ($dfv, $val) = @_;

            if($val eq 'termijn') {
                my $vernietigingsdatum = $dfv->{'__INPUT_DATA'}->{vernietigingsdatum};

                if(
                    !UNIVERSAL::isa($vernietigingsdatum, 'DateTime') &&
                    $vernietigingsdatum !~ /^\d{2}\-\d{2}\-\d{4}$/
                ) {
                    return;
                }
            }

            return 1;
        },
        vernietigingsdatum  => sub {
            my ($dfv, $val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return 1;
            }

            if ($val =~ /^\d{2}\-\d{2}\-\d{4}$/) {
                return 1;
            }

            return;
        },
        reden               => sub {
            my ($dfv, $value) = @_;

            my $redenen = VERNIETIGINGS_REDENEN;

            if (grep { $_ eq $value } @{ $redenen }) {
                return 1;
            }

            return;
        },
    },
    field_filters       => {
        vernietigingsdatum  => sub {
            my ($val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return $val;
            }

            if (
                (my ($day, $month, $year) = $val =~
                    /^(\d{2})\-(\d{2})\-(\d{4})$/)
            ) {
                return DateTime->new(
                    year        => $year,
                    day         => $day,
                    month       => $month
                );
            }

            return $val;
        }
    },
    msgs                => PARAMS_PROFILE_MESSAGES_SUB,
};



use constant ZAAK_WIJZIG_STREEFAFHANDELDATUM_PROFILE         => {
    required            => [qw/
        streefafhandeldatum
    /],
    constraint_methods  => {
        streefafhandeldatum  => sub {
            my ($dfv, $val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return 1;
            }

            if ($val =~ /^\d{2}\-\d{2}\-\d{4}$/) {
                return 1;
            }

            return;
        },
    },
    field_filters       => {
        streefafhandeldatum  => sub {
            my ($val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return $val;
            }

            if (
                (my ($day, $month, $year) = $val =~
                    /^(\d{2})\-(\d{2})\-(\d{4})$/)
            ) {
                return DateTime->new(
                    year        => $year,
                    day         => $day,
                    month       => $month,
                    time_zone   => 'Europe/Amsterdam',
                );
            }

            return $val;
        }
    },
    msgs                => PARAMS_PROFILE_MESSAGES_SUB,
};

use constant ZAAK_WIJZIG_REGISTRATIEDATUM_PROFILE         => {
    required            => [qw/
        registratiedatum
    /],
    constraint_methods  => {
        registratiedatum  => sub {
            my ($dfv, $val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return 1;
            }

            if ($val =~ /^\d{2}\-\d{2}\-\d{4}$/) {
                return 1;
            }

            return;
        },
    },
    field_filters       => {
        registratiedatum  => sub {
            my ($val) = @_;

            if (UNIVERSAL::isa($val, 'DateTime')) {
                return $val;
            }

            if (
                (my ($day, $month, $year) = $val =~
                    /^(\d{2})\-(\d{2})\-(\d{4})$/)
            ) {
                return DateTime->new(
                    year        => $year,
                    day         => $day,
                    month       => $month
                );
            }

            return $val;
        }
    },
    msgs => PARAMS_PROFILE_MESSAGES_SUB,
};

use constant DOCUMENTS_STORE_TYPE_NOTITIE       => 'notitie';
use constant DOCUMENTS_STORE_TYPE_FILE          => 'file';
use constant DOCUMENTS_STORE_TYPE_MAIL          => 'mail';
use constant DOCUMENTS_STORE_TYPE_JOB           => 'job';

use constant BETROKKENE_TYPE_BEDRIJF            => 'bedrijf';
use constant BETROKKENE_TYPE_NATUURLIJK_PERSOON => 'natuurlijk_persoon';
use constant BETROKKENE_TYPE_MEDEWERKER         => 'medewerker';

use constant SUBJECT_TYPES                      => [
    BETROKKENE_TYPE_BEDRIJF,
    BETROKKENE_TYPE_NATUURLIJK_PERSOON,
    BETROKKENE_TYPE_MEDEWERKER
];

use constant OBJECTSEARCH_TABLE_ORDER => qw[contact zaak file];

use constant OBJECTSEARCH_TABLENAMES => {
    bedrijf                  => { tablename => 'Bedrijf' },
    natuurlijk_persoon       => { tablename => 'NatuurlijkPersoon', },
    zaak                     => { tablename => 'Zaak' },
    zaaktype                 => { tablename => 'Zaaktype' },
    file                     => { tablename => 'File' },
    bibliotheek_kenmerken    => { tablename => 'BibliotheekKenmerken' },
    bibliotheek_sjablonen    => { tablename => 'BibliotheekSjablonen' },
    bibliotheek_categorie    => { tablename => 'BibliotheekCategorie' },
    bibliotheek_notificaties => { tablename => 'BibliotheekNotificaties' }
};

use constant OBJECT_ACTIONS => {
    case => {
        allocate => {
            label => 'Toewijzing wijzigen',
            path => '/bulk/update/allocation'
        },
        acquire => {
            label => 'In behandeling nemen',
            path => '/bulk/update/owner'
        },
        suspend => {
            label => 'Opschorten',
            path => '/bulk/update/opschorten'
        },
        resume => {
            label => 'Hervatten',
            path => '/bulk/update/resume'
        },
        prolong => {
            label => 'Verlengen',
            path => '/bulk/update/verlengen'
        },
        relate => {
            label => 'Relateren',
            path => '/bulk/update/relatie'
        },
        manage => {
            label => 'Beheeracties',
            path => '/bulk/update/set_settings'
        },
        destroy => {
            label => 'Vernietigen',
            path => '/bulk/update/destroy'
        },
        publish => {
            label => 'Publiceren',
            path => '/bulk/publish'
        },
        export => {
            label => 'Exporteren',
            path => undef # implies current zql query
        }
    }
};

use constant ALLOW_ONLY_TRUSTED_HTML    => sub {
    my $val     = shift;

    use HTML::TagFilter;

    my $tf      = new HTML::TagFilter;

    return $tf->filter($val);
};

use constant ALLOW_NO_HTML    => sub {
    my $val     = shift;

    use HTML::TagFilter;

    my $tf      = HTML::TagFilter->new(allow => {});

    return $tf->filter($val);
};

use constant MIMETYPES_ALLOWED => {
    '.doc' => {
        mimetype   => 'application/msword',
        copy2doc   => 1,
    },
    '.dot' => {
        mimetype   => 'application/msword',
        copy2doc   => 1,
    },
    '.docx' => {
        mimetype =>
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.dotx' => {
        mimetype =>
            'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
        alternate_mimetypes => ['application/x-zip'],
        copy2doc   => 1,
    },
    '.docm' => {
        mimetype => 'application/vnd.ms-word.document.macroEnabled.12',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.dotm' => {
        mimetype => 'application/vnd.ms-word.template.macroEnabled.12',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.xls' => {
        mimetype            => 'application/vnd.ms-excel',
        alternate_mimetypes => ['application/msword'],
        preview             => 0,
        copy2pdf            => 0,
    },
    '.xlt' => {
        mimetype            => 'application/vnd.ms-excel',
        alternate_mimetypes => ['application/msword'],
        copy2pdf            => 0,
        preview             => 0,
    },
    '.xlsx' => {
        mimetype =>
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        alternate_mimetypes => ['application/x-zip'],
        copy2pdf            => 0,
        preview             => 0,
    },
    '.xltx' => {
        mimetype =>
            'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
        alternate_mimetypes => ['application/x-zip'],
        copy2pdf            => 0,
        preview             => 0,
    },
    '.xlsm' => {
        mimetype   => 'application/vnd.ms-excel.sheet.macroEnabled.12',
        alternate_mimetypes => ['application/x-zip'],
        copy2pdf            => 0,
        preview             => 0,
    },
    '.xltm' => {
        mimetype => 'application/vnd.ms-excel.template.macroEnabled.12',
        alternate_mimetypes => ['application/x-zip'],
        copy2pdf            => 0,
        preview             => 0,
    },
    '.xlsb' => {
        mimetype =>
            'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
        alternate_mimetypes => ['application/x-zip'],
        copy2pdf            => 0,
        preview             => 0,
    },
    '.ppt' => {
        mimetype            => 'application/vnd.ms-powerpoint',
        alternate_mimetypes => [
            'application/msword',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        ],
    },
    '.pot' => {
        mimetype            => 'application/vnd.ms-powerpoint',
        alternate_mimetypes => [
            'application/msword',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        ],
    },
    '.pps' => {
        mimetype            => 'application/vnd.ms-powerpoint',
        alternate_mimetypes => [
            'application/msword',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        ],
    },
    '.ppa' => {
        mimetype            => 'application/vnd.ms-powerpoint',
        alternate_mimetypes => [
            'application/msword',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        ],
    },
    '.pptx' => {
        mimetype =>
            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.potx' => {
        mimetype =>
            'application/vnd.openxmlformats-officedocument.presentationml.template',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.ppsx' => {
        mimetype =>
            'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.pptm' => {
        mimetype =>
            'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.potm' => {
        mimetype =>
            'application/vnd.ms-powerpoint.template.macroEnabled.12',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.ppsm' => {
        mimetype =>
            'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.sldx' => {
        mimetype =>
            'application/vnd.openxmlformats-officedocument.presentationml.slide',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.sldm' => {
        mimetype =>
            'application/vnd.ms-powerpoint.slide.macroEnabled.12',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.odt' => {
        mimetype   => 'application/vnd.oasis.opendocument.text',
        alternate_mimetypes => ['application/x-zip'],
        copy2doc   => 1,
    },
    '.ods' => {
        mimetype   => 'application/vnd.oasis.opendocument.spreadsheet',
        alternate_mimetypes => ['application/x-zip'],
        copy2pdf            => 0,
        preview             => 0,
    },
    '.odf' => {
        mimetype   => 'application/vnd.oasis.opendocument.text',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.odp' => {
        mimetype   => 'application/vnd.oasis.opendocument.presentation',
        alternate_mimetypes => [
            'application/x-zip',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        ],
    },
    '.pdf' => {
        mimetype   => 'application/pdf',
    },
    '.bmp' => {
        mimetype   => 'image/bmp',
    },
    '.gif' => {
        mimetype   => 'image/gif',
    },
    '.jpe' => {
        mimetype => 'image/jpeg',
    },
    '.jpeg' => {
        mimetype   => 'image/jpeg',
    },
    '.jpg' => {
        mimetype   => 'image/jpeg',
    },
    '.png' => {
        mimetype   => 'image/png',
    },
    '.tiff' => {
        mimetype   => 'image/tiff',
        copy2pdf   => 0,
        preview    => 1,
    },
    '.tif' => {
        mimetype   => 'image/tiff',
        copy2pdf   => 0,
        preview    => 1,
    },
    '.htm' => {
        mimetype   => 'text/html',
    },
    '.html' => {
        mimetype   => 'text/html',
    },
    '.xml' => {
        mimetype   => 'text/xml',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.rtf' => {
        mimetype            => 'text/rtf',
        alternate_mimetypes => ['application/rtf'],
    },
    '.svg' => {
        mimetype   => 'image/svg+xml',
    },
    '.csv' => {
        mimetype   => 'text/csv',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.xps' => {
        mimetype            => 'application/vnd.ms-xpsdocument',
        alternate_mimetypes => ['application/x-zip'],
    },
    '.asc' => {
        mimetype   => 'text/plain',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.txt' => {
        mimetype   => 'text/plain',
    },
    '.gml' => {
        mimetype   => 'application/xml',
        alternate_mimetypes =>
            ['application/gml', 'application/gml+xml'],
        copy2pdf => 0,
        preview  => 0,
    },
    '.shp' => {
        mimetype   => 'application/octet-stream',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.crt' => {
        mimetype            => 'text/plain',
        alternate_mimetypes => ['application/x-pem-file'],
        copy2pdf            => 0,
        preview             => 0,
    },
    '.pem' => {
        mimetype            => 'text/plain',
        alternate_mimetypes => ['application/x-pem-file'],
        copy2pdf            => 0,
        preview             => 0,
    },
    '.key' => {
        mimetype            => 'text/plain',
        alternate_mimetypes => ['application/x-pem-file'],
        copy2pdf            => 0,
        preview             => 0,
    },
    '.msg' => {
        mimetype => 'application/vnd.ms-outlook',
        copy2pdf => 0,
        preview  => 0,
    },
    '.eml' => {
        mimetype => 'message/rfc822',
        copy2pdf => 0,
        preview  => 0,
    },
    '.sql' => {
        mimetype   => 'text/plain',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.mdb' => {
        mimetype   => 'application/octet-stream',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.accdb' => {
        mimetype   => 'application/msaccess',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.dbf' => {
        mimetype   => 'application/octet-stream',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.por' => {
        mimetype   => 'application/octet-stream',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.sas' => {
        mimetype   => 'text/plain',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.dta' => {
        mimetype   => 'application/octet-stream',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.ai' => {
        mimetype   => 'application/postscript',
    },
    '.eps' => {
        mimetype   => 'application/postscript',
    },
    '.mpg' => {
        mimetype   => 'video/mpeg',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.mpeg' => {
        mimetype   => 'video/mpeg',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.mp4' => {
        mimetype   => 'video/mp4',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.avi' => {
        mimetype   => 'application/octet-stream',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.mov' => {
        mimetype   => 'video/quicktime',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.wav' => {
        mimetype   => 'audio/wav',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.dxf' => {
        mimetype   => 'text/plain',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.dwg' => {
        mimetype   => 'application/octet-stream',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.mif' => {
        mimetype   => 'text/plain',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.mid' => {
        mimetype   => 'audio/mid',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.shp' => {
        mimetype   => 'application/octet-stream',
        conversion => '',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.tab' => {
        mimetype   => 'application/octet-stream',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.ogg' => {
        mimetype   => 'audio/ogg',
        copy2pdf   => 0,
        preview    => 0,
    },
    '.mp3' => {
        mimetype   => 'audio/mpeg',
        copy2pdf   => 0,
        preview    => 0,
    },
};

use constant JOBS_INFORMATION_MAP   => {
    'zaak::update_kenmerk'    => {
        change_type => 'wijziging',
        label       => 'Wijzigen van kenmerk',
    },
};

use constant STRONG_RELATED_COLUMNS => [qw/
    aanvrager
    behandelaar
    coordinator
    locatie_correspondentie
    locatie_zaak
/];

use constant ZAAK_EMPTY_COLUMNS => [qw/
    aanvrager
    behandelaar
    coordinator
    locatie_correspondentie
    locatie_zaak
/];

=head2 Event type groupings

Ability to group events into categories for easier consumption.

=cut

use constant EVENT_TYPE_GROUPS => {
    document        => [qw[document case/document]],
    case            => [qw[case]],
    contactmoment   => [qw[subject/contact_moment]],
    note            => [qw[case/note subject/note]]
};

use constant FILE_STORE_LOCATION =>
    '/home/zaaksysteem/dev/Zaaksysteem_Documentairly/files/storage';

use constant FRIENDLY_BETROKKENE_MESSAGES => {
    deceased    => 'Betrokkene is overleden',
    secret      => 'Betrokkene heeft een indicatie "Geheim"',
    moved       => 'Betrokkene is verhuisd',
    onderzoek   => 'Betrokkene staat in onderzoek',
    briefadres  => 'Betrokkene heeft een briefadres'
};

use constant EDE_PUBLICATION_STRINGS => {
    '_empty' => [],
    'Collegevergadering' => [],
    'Raadscommissie AZ&M' => [],
    'Raadscommissie RO' => [],
    'Raadscommissie MZ' => [],
    'Raadscommissie BFO' => [],
    'Raadsvergadering' => [],
    'College van B en W' => [],
    'Besluitvormende vergadering' => ['Opening en mededelingen van de voorzitter', 'Vaststelling agenda', 'Vragenuurtje'],
    'Oordeelvormende vergadering' => ['Opening en mededelingen van de voorzitter'],
    'Beeldvormende/informatieve ronde' => ['Opening en mededelingen van de voorzitter']
};

use constant SUBJECT_TYPE_EMPLOYEE => 'employee';
use constant SUBJECT_TYPE_PERSON   => 'person';
use constant SUBJECT_TYPE_COMPANY  => 'company';

use constant ZAAKTYPE_ATTRIBUTEN => {
    ztc_grondslag             => 'text',
    ztc_handelingsinitiator   => 'text',
    ztc_selectielijst         => 'text',
    ztc_afhandeltermijn       => 'text',
    ztc_afhandeltermijn_type  => 'text',
    ztc_servicenorm           => 'text',
    ztc_servicenorm_type      => 'text',
    ztc_escalatiegeel         => 'text',
    ztc_escalatieoranje       => 'text',
    ztc_escalatierood         => 'text',
    ztc_openbaarheid          => 'text',
    ztc_webform_toegang       => 'text',
    ztc_webform_authenticatie => 'text',
    pdc_meenemen              => 'text',
    pdc_description           => 'text',
    pdc_voorwaarden           => 'text',
    pdc_tarief                => 'text',
    ztc_procesbeschrijving    => 'text',
};


use constant STUF_VERSIONS => qr/^(?:0204|0301)$/;
use constant STUF_XSD_PATH => 'share/wsdl/stuf';
use constant STUF_XML_URL  => 'http://www.egem.nl/StUF';

=head2 BAG_TYPES

The types of Basis Administratie Gemeentes supported by Zaaksysteem

=cut

use constant BAG_TYPES => [qw/ligplaats nummeraanduiding openbareruimte pand standplaats verblijfsobject woonplaats/];

use constant SERVICE_NORM_TYPES => [
      ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WERKDAGEN},
      ZAAKSYSTEEM_NAMING->{TERMS_TYPE_KALENDERDAGEN},
      ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WEKEN},
      ZAAKSYSTEEM_NAMING->{TERMS_TYPE_EINDDATUM},
];

use constant SERVICE_NORM_TYPES_OK => sub {
    my $type = shift;
    if ($type) {
        $type = lc($type);
        if (grep { $type eq lc($_) } @{SERVICE_NORM_TYPES()}) {
            return $type;
        }
    }
    return undef;
};

use constant ZAAKSYSTEEM_BOFH => 'betrokkene-medewerker-20000';

{
    #
    # The code in the _domain function is stolen from
    # http://cpansearch.perl.org/src/CREIN/Regexp-Common-dns-0.00_01/lib/Regexp/Common/dns.pm
    #
    # Original author: Chris Reinhardt cpan at triv.org
    # Copyright (c) 2003 Chris Reinhardt.
    #
    # All rights reserved.  This program is free software; you may redistribute
    # it and/or modify it under the same terms as Perl itself.
    #
    # Wesley Schwengle (2014)
    # I've added support for RFC 1123.
    # The license is compatible with the license we use at Mintlab:
    # https://joinup.ec.europa.eu/software/page/eupl/eupl-compatible-open-source-licences
    #
    sub _domain {
        my %flags = @_;

        my $sep         = '\.';
        my $letter      = '[a-zA-Z]';
        my $let_dig     = '[a-zA-Z0-9]';
        my $let_dig_hyp = '[-a-zA-Z0-9]';

        my %labels = (
            1035   => "(?:$letter(?:$let_dig|$let_dig_hyp\{1,61}$let_dig)?)",
            1123   => "(?:(?:$let_dig|$let_dig$let_dig_hyp*$let_dig)$sep)*(?:$let_dig|$let_dig$let_dig_hyp*$let_dig)",
            2181   => '[^.]{1,63}',
            hybrid => '[a-zA-Z0-9_-]{1,63}'
        );

        $flags{'-rfc'} ||= 1035;

        my $label = $labels{$flags{'-rfc'}} || die("Unknown DNS RFC: $flags{'-rfc'}");

        if ($flags{'-rfc'} ne 2181 && exists $flags{'-wildcard'} && not defined $flags{'-wildcard'}) {
            $label = "(?:\\*|$label)";
        }

        my $quant = '*';
        if ($flags{'-minlabels'}) {
            $quant = '{' . ($flags{'-minlabels'} - 1) . ',}';
        }

        return qr/^(?:$label$sep)$quant$label$sep?$/;
    }

    my $fqdn_regexp = _domain(-rfc => 1123, -minlabels => 2);

    use constant VALID_FQDN => sub {
        my $fqdn = pop;
        if ($fqdn && $fqdn =~ m/$fqdn_regexp/) {
            $fqdn =~ s/\.$//;
            return 0 if (length($fqdn) > 255);
            return 0 if (grep { length($_) > 63 } split(/\./, $fqdn));
            return 1;
        }
        return 0;
    };
}

use constant OBJECT_CLASS_DOMAIN  => 'zs_domain';
use constant OBJECT_CLASS_CASE    => 'case';
use constant OBJECT_CLASS_DEFAULT => 'object';

use constant ZS_PROFILE_CREATE_DOMAIN => {
    required => {
        fqdn  => 'Str',
        label => 'Str',
    },
    optional => {
        owner       => 'Any',    # TODO: Type this correctly
        certificate => 'Any',    # TODO: This should create a file
    },
    # This breaks, for some vague reason
    #constraint_methods => { fqdn  => VALID_FQDN },
    defaults           => { owner => '' },
};

sub _bag_filter {
    # XXX Almost cloned from lib/Zaaksysteem/Attributes.pm
    my ($schema, $bag_id) = @_;

    return unless $bag_id;

    my $bag_model = $schema->bag_model;

    if (ref($bag_id) eq 'ARRAY') {
        my @rv;

        for my $bag_identifier (@$bag_id) {
            my ($type, $id) = split /-/, $bag_identifier;
            my $bag_object;

            if ($type && $id) {
                $bag_object = $bag_model->get($type => $id);
            }

            if ($bag_object) {
                push @rv, $bag_object->to_attribute_value($type);
            } else {
                push @rv, __fake_bag_attribute_value($bag_identifier);
            }
        }

        return \@rv;
    }

    my ($type, $id) = split /-/, $bag_id;
    my $bag_object;

    if ($type && $id) {
        $bag_object = $bag_model->get($type => $id);
    }

    my $rv;
    if ($bag_object) {
        $rv = $bag_object->to_attribute_value($type);
    } else {
        $rv = __fake_bag_attribute_value($bag_id);
    }

    return $rv;
}

sub __fake_bag_attribute_value {
    my $id = shift;

    return {
        bag_id           => $id,
        human_identifier => "Onbekend BAG-object: '$id'",
        address_data     => {}
    };
}

sub _numeric_fix_filter {
    my ($schema, $value) = @_;

    return if not length($value);

    # Some inputs have non breaking spaces in front of them, clear those
    $value =~ s/\p{Space}+//g;

    if (looks_like_number($value)) {
        return $value;
    }
    return;
}

=head1 SAML2 Constants

=over 4

=item SAML_TYPE_LOGIUS

This the main (only?) provider for DigiD services.

=item SAML_TYPE_KPN_LO

This is our currently supported supplier for eHerkenning services.

=item SAML_TYPE_EIDAS

SAML dialect for eIDAS authentication.

=item SAML_TYPE_ADFS

The identifier for Microsoft's AD Federation Services

=item SAML_TYPE_MINIMAL

Minimal SAML instance, only given attribute is the login name as login identifier

=back

=cut

use constant SAML_TYPE_LOGIUS   => 'digid';
use constant SAML_TYPE_KPN_LO   => 'eherkenning';
use constant SAML_TYPE_EIDAS    => 'eidas';
use constant SAML_TYPE_ADFS     => 'adfs';
use constant SAML_TYPE_SPOOF    => 'spoof';
use constant SAML_TYPE_MINIMAL  => 'minimal';

use constant RGBZ_LANDCODES => {
    map { $_->{dutch_code} => $_->{label} } @{ COUNTRY_TABLE() }
};

use constant RGBZ_GEMEENTECODES => { map { $_->{dutch_code} => $_->{label} }
        grep { !defined $_->{historical} || ${ $_->{historical} } == 0 }
        @{ MUNICIPALITY_TABLE() } };

use constant STUF_SUBSCRIPTION_VIEWS => {
    subscriptions_without_cases             => 'Personen zonder zaken',
    subscriptions_without_cases_since_1d    => 'Personen zonder zaken (persoon minimaal 1 dag geleden geimporteerd)',
    subscriptions_with_cases                => 'Personen met zaken',

    subscriptions_outside_municipality_without_cases    => 'Buitengemeentelijke personen zonder zaken',
    subscriptions_outside_municipality_with_cases       => 'Buitengemeentelijke personen met zaken',

    subscriptions_desceased       => 'Overleden personen met zaken',
    subscriptions_inactive        => 'Inactieve personen met zaken',

    subscriptions_desceased_without_cases => 'Overleden personen zonder zaken',
    subscriptions_inactive_without_cases  => 'Inactieve personen zonder zaken',
};

use constant ZAAKSYSTEEM_CSS_TEMPLATES => [qw/
    155
    alkmaar
    baarn
    bbz
    brabantsedelta
    bommelerwaard
    beuningen
    bwb
    debilt
    dinkelland
    ede
    epe
    epe-oud
    etten-leur
    groningenseaports
    halderberge
    hardenberg
    halte-werk
    gouda
    gemeenteberkelland
    escaperoom
    geschillencommissie
    hellendoorn
    heumen
    heusden
    huis-voor-klokkenluiders
    huurcomissie
    hofvantwente
    imk
    lansingerland
    leerdam
    lingewaard
    krimpenerwaard
    lingewaard-oud
    maassluis
    maasdriel
    mintlab
    moerdijk
    munitax
    noaberkracht
    geschillencommissie-kinderopvang
    noordenveld
    noordwijk
    geschillencommissie-klachtenloket-zorg
    ommen
    geschillencommissie-consumentenzaken
    oude-ijsselstreek
    ommen-hardenberg
    roosendaal
    raadvanstate
    schouwen-duiveland
    simpelveld
    smallingerland
    soest
    solo
    solo-mijngemeentedichtbij
    solo-kerkrade
    solo-berg-en-dal
    solo-isdbrabantsewal
    solo-breda
    solo-weert
    solo-baanbrekers
    solo-buren
    solo-emmen
    solo-geertruidenberg
    solo-hulst
    solo-katwijk
    solo-meierijstad
    solo-montferland
    solo-ooststellingwerf
    solo-optimisd
    solo-veenendaal
    solo-scmer
    steenbergen
    stroomopwaarts
    sudwest-fryslan
    tholen
    tubbergen
    vianen
    vijfheerenlanden
    voerendaal
    waalre
    geschillencommissie-zorg
    werkendam
    werkplein
    wijkbijduurstede
    zaakstad
    zeeland
    zoetermeer
    zaaksysteem
    zaltbommel
    zederik
/];

use constant ZAAKSYSTEEM_CSS_TEMPLATES_OLD => [qw/
    brandweer
    bussum
    hillegom
    hlt
    lisse
    niwo
    teylingen
/];


use constant CONTROLPANEL_CUSTOMER_TYPE_POST_DOMAIN => {
    'commercial'    => 'zaaksysteem.net',           # P
    'government'    => 'zaaksysteem.nl',            # P
    'lab'           => 'lab.zaaksysteem.app',
    'development'   => 'dev.zaaksysteem.app',       # O
    'staging'       => 'stg.zaaksysteem.app',       # T
    'acceptance'    => 'acc.zaaksysteem.app'        # A
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CHILD_CASETYPE_OPTIONS

TODO: Fix the POD

=cut

=head2 SJABLONEN_EXPORT_FORMATS

TODO: Fix the POD

=cut

=head2 VALIDATION_RELATEREN_PROFILE

TODO: Fix the POD

=cut

=head2 MIMETYPES_ALLOWED

TODO: Fix the POD

=cut
