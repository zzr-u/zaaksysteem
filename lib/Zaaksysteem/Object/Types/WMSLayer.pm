package Zaaksysteem::Object::Types::WMSLayer;
use Moose;

extends 'Zaaksysteem::Object';

use MooseX::Types::Moose qw(Str ArrayRef);
use URI;
use Zaaksysteem::Types qw(JSONBoolean);

=head1 NAME

Zaaksysteem::Object::Types::WMSLayer - Object representing a WMS map layer

=head1 DESCRIPTIVE

A class describing a WMS map layer

=head1 ATTRIBUTES

=head2 layer_name

Name of the WMS layer as used in WMS. This can generally be extracted from the
WMS "GetCapabilities" XML.

=cut

has layer_name => (
    is       => 'ro',
    isa      => Str,
    required => 1,
    label    => 'WMS layer name',
    traits   => [qw(OA)],
);

=head2 label

A human-readable name for this map layer.

=cut

has label => (
    is       => 'ro',
    isa      => Str,
    required => 1,
    label    => 'Descriptive name',
    traits   => [qw(OA)],
);

=head2 uri

Internal attribute, used to store the URI object pointing to the WMS service.

=cut

has uri => (
    is       => 'ro',
    isa      => 'URI::https',
    required => 1,
);

=head2 url

String version of L</uri>

=cut

has url => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        shift->uri->as_string;
    },
    label    => 'URL',
    traits   => [qw(OA)],
);

=head2 active

Boolean, indicating whether this layer is configured to be active.

=cut

has active => (
    is       => 'ro',
    isa      => JSONBoolean,
    required => 1,
    coerce   => 1,
    label    => 'Active',
    traits   => [qw(OA)],
);

=head2 feature_info_xpath

XPath expression that can be used to extract the feature name from the XML
document returned by the WMS service "feature info" request.

=cut

has feature_info_xpath => (
    is  => 'ro',
    isa => Str,
    label    => 'XPath for feature info',
    traits   => [qw(OA)],
);

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
