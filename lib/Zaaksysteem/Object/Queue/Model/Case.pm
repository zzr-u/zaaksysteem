package Zaaksysteem::Object::Queue::Model::Case;

use Moose::Role;

use Zaaksysteem::Constants qw(
    LOGGING_COMPONENT_ZAAK
);

use BTTW::Tools;
use Zaaksysteem::Types qw[UUID CaseNumber];

use Zaaksysteem::Object::Types::Address;
use Zaaksysteem::Object::Types::Location;
use Zaaksysteem::Zaken::Model;

=head1 NAME

Zaaksysteem::Object::Queue::Model::Case - Case queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 create_case_subcase

=cut

sig create_case_subcase => 'Zaaksysteem::Backend::Object::Queue::Component';

sub create_case_subcase {
    my $self = shift;
    my $item = shift;

    my $case_model;
    if($item->subject) {
        $case_model = $self->case_model($item->subject->id);
    } else {
        $case_model = $self->case_model();
    }

    my %start_subcase_args = (
        action_data      => $item->data,
        object_model     => $self->object_model,
        betrokkene_model => $self->betrokkene_model,
        case_model       => $case_model,
    );
    $start_subcase_args{current_user} = $item->subject if $item->subject;

    $item->object_data->get_source_object->start_subcase(%start_subcase_args);

    return;
}

=head2 create_case_document

=cut

sig create_case_document => 'Zaaksysteem::Backend::Object::Queue::Component';

sub create_case_document {
    my $self = shift;
    my $item = shift;

    my %template_action_args = (
        action_data => $item->data
    );

    if ($item->subject) {
        $template_action_args{ current_user } = $item->subject;
    }

    my $file = $item->object_data->get_source_object->template_action(
        %template_action_args
    );

    ### Detected an external template, act accordingly
    if (ref $file eq 'HASH' && exists $file->{type}) {
        $item->data($file);
    } else {
        my $data = $item->data;

        if ($data->{interface_id}) {
            # In case of external templates we have no clue what the file id
            # is or will be, posted on a later time to ZS
            $data->{result} = {
                file_id         => undef,
                file_store_uuid => undef,
            };

        }
        elsif (blessed $file) {
            $data->{result} = {
                file_id        => $file->id,
                filestore_uuid => $file->filestore_id->uuid,
            };
        }
        else {
            throw('qitem/create_case_document/state/unknown',
                "Not an external template and not a file object, unknown failure condition"
            );
        }

        $item->data($data);
    }

    return;
}

=head2 update_case_location

Handler for C<update_case_location> queue item instances.

=cut

sig update_case_location => 'Zaaksysteem::Backend::Object::Queue::Component';

sub update_case_location {
    my $self = shift;
    my $item = shift;

    my $zaak = $item->object_data->get_source_object;
    my $location = Zaaksysteem::Object::Types::Location->new($item->data);
    $zaak->update_location($location);
    $zaak->touch;

    return;
}

=head2 allocate_case

=cut

sig allocate_case => 'Zaaksysteem::Backend::Object::Queue::Component';

sub allocate_case {
    my $self = shift;
    my $item = shift;

    $item->object_data->get_source_object->allocation_action($item->data);

    return;
}

=head2 transition_case_phase

=cut

sub transition_case_phase {
    my $self = shift;
    my $item = shift;

    my $subject_id = $item->subject_id;

    unless ($subject_id) {
        throw('queue/create/case/subject', sprintf(
            'Subject is missing, unable to process queue item "%s"',
            $item->stringify
        ));
    }

    my $model = $self->case_model($subject_id);
    my $zaak = $model->rs_zaak->find($item->data->{ case_number });

    $model->execute_phase_actions($zaak, $item->data->{ transition_args });

    $zaak->touch;

    return;
}

=head2 add_case_subject

Add a new subject to a case, in a specified role.

If the (role, subject) combination already exists for this case, this queue
item will do nothing.

=cut

sub add_case_subject {
    my $self = shift;
    my $item = shift;

    my $case = $item->object_data->get_source_object;
    my $schema = $case->result_source->schema;

    my $subject = $schema->betrokkene_model->get_by_string($item->data->{betrokkene_identifier});

    if (
        !$subject &&
        (
            $subject->can('gm_extern_np') &&
            $subject->gm_extern_np &&
            $subject->gm_extern_np->can('deleted_on') &&
            $subject->gm_extern_np->deleted_on
        )
    ) {
        $self->log->warn(sprintf("Subject with identifier %s not found", $item->data->{betrokkene_identifier}));
        return;
    }

    my $rv = $case->betrokkene_relateren({
        betrokkene_identifier  => $item->data->{betrokkene_identifier},
        magic_string_prefix    => $item->data->{magic_string_prefix},
        rol                    => $item->data->{rol},
        pip_authorized         => $item->data->{gemachtigd},
        send_auth_confirmation => $item->data->{notify},
    });

    if ($rv && $item->data->{notify} && $item->data->{gemachtigd}) {
        try {
            my $template_id = $schema->resultset('Config')->get(
                'subject_pip_authorization_confirmation_template_id'
            );

            unless($template_id) {
                my $message = 'Kon geen e-mail notificatie template vinden, geen notificatie verstuurd naar ingestelde betrokkene';
                $self->log->error($message);

                return;
            }

            my $template = $schema->resultset('BibliotheekNotificaties')->find($template_id);

            unless($template) {
                my $message = 'Notificatie template kon niet gevonden worden, geen e-mail notificatie verstuurd';
                $self->log->warn($message);
                return;
            }

            unless($subject->email) {
                my $message = sprintf(
                    'Geen e-mail adres gevonden voor "%s", geen notificatie verstuurd',
                    $subject->display_name
                );
                $self->log->warn($message);
                return;
            }

            $case->mailer->send_case_notification({
                notification => $template,
                recipient => $subject->email
            });


        } catch {
            $self->log->warn('Sending e-mail to authorized subject for subcase failed: ' . $_);
        };
    }

    if (!$rv) {
        $case->trigger_logging(
            'case/subject/exists',
            {
                component => LOGGING_COMPONENT_ZAAK,
                data => {
                    case_id      => $case->id,
                    subject_name => $item->data->{naam},
                    role         => $item->data->{rol},
                },
            },
        );
    }

    return 1;
}

=head2 touch_case

Handler for asynchronous case touches.

=cut

define_profile touch_case => (
    optional => {
        case_object_id => UUID,
        case_number => CaseNumber
    },
    require_some => {
        case_identifier => [ 1, qw[case_object_id case_number] ]
    }
);

sub touch_case {
    my $self = shift;
    my $item = shift;

    my $args = assert_profile($item->data)->valid;

    my $zaak;

    if (exists $args->{ case_object_id }) {
        my $case = $self->object_model->new_resultset->find(
            $args->{ case_object_id }
        );

        $zaak = $case->get_source_object;
    } else {
        $zaak = $self->_rs_zaak->find($args->{ case_number });
    }

    unless (defined $zaak) {
        throw('queue/case/touch/not_found', sprintf(
            'Could not find case by identifier "%s"',
            $args->{ case_object_id } || $args->{ case_number }
        ));
    }

    $zaak->_touch;

    return;
}

=head2 create_case

Create a case via the queue model.

=cut

sig create_case => 'Zaaksysteem::Backend::Object::Queue::Component';

sub create_case {
    my ($self, $item) = @_;

    my $subject_id = $item->subject_id;

    unless ($subject_id) {
        throw('queue/create/case/subject', sprintf(
            'Subject is missing, unable to process queue item "%s"',
            $item->stringify
        ));
    }

    my $model = $self->case_model($subject_id);

    my $create_arguments = $model->prepare_case_arguments(
        $item->data->{ create_args }
    );

    $create_arguments->{ case_id } = $item->data->{ case_id };

    my $case = $model->create_case($create_arguments);

    $case->_touch;

    return;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
