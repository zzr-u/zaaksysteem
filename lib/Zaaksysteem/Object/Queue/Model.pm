package Zaaksysteem::Object::Queue::Model;

use Moose;

with qw[
    MooseX::Log::Log4perl

    Zaaksysteem::Object::Queue::Model::Attribute
    Zaaksysteem::Object::Queue::Model::Case
    Zaaksysteem::Object::Queue::Model::Casetype
    Zaaksysteem::Object::Queue::Model::Email
    Zaaksysteem::Object::Queue::Model::File
    Zaaksysteem::Object::Queue::Model::Object
    Zaaksysteem::Object::Queue::Model::ObjectMutation
    Zaaksysteem::Object::Queue::Model::ObjectSubscription
    Zaaksysteem::Object::Queue::Model::Meta
    Zaaksysteem::Object::Queue::Model::Subject
    Zaaksysteem::Object::Queue::Model::NatuurlijkPersoon
];

=head1 NAME

Zaaksysteem::Object::Queue::Model - Interface for processing queue items

=head1 DESCRIPTION

=cut

use BTTW::Tools::UA;
use BTTW::Tools;
use DateTime;
use HTTP::Status qw(:constants);
use IO::Socket::SSL::Utils;
use JSON::XS;
use List::Util qw(uniq);
use Moose::Util::TypeConstraints qw[enum];
use Net::AMQP::RabbitMQ;
use Zaaksysteem::Types qw(
    NonEmptyStr
    UUID
);

=head1 ATTRIBUTES

=head2 table

Required L<DBIx::Class::ResultSet> instance which handles data storage.

=cut

has table => (
    is => 'rw',
    isa => 'DBIx::Class::ResultSet',
    required => 1,
    handles => {
        queue_item => 'queue_item',
        queued_items => 'queued_items',
        reset_queue => 'reset_queue',
        create_items => 'create_items'
    }
);

=head2 instance_hostname

Required hostname of the instance.

=cut

has instance_hostname => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head2 base_uri

Base URI of the environment of the current request.

=cut

has base_uri => (
    is => 'rw',
    isa => 'URI',
    required => 1
);

=head2 object_model

L<Zaaksysteem::Object::Model> instance for handling C<mutate_object> queue
items.

=cut

has object_model => (
    is => 'rw',
    isa => 'Zaaksysteem::Object::Model',
    predicate => 'has_object_model',
    accessor => '_object_model'
);

=head2 subject_model

L<Zaaksysteem::BR::Subject> instance for handling calls to the subject model.

=cut

has subject_model => (
    is => 'rw',
    isa => 'Zaaksysteem::BR::Subject',
    predicate => 'has_subject_model',
    accessor => '_subject_model',
);

=head2 document_model

L<Zaaksysteem::Document::Model> instance for handling document model calls

=cut

has document_model => (
    is => 'rw',
    isa => 'Zaaksysteem::Document::Model',
    predicate => 'has_document_model',
    accessor => '_document_model',
);

=head2 betrokkene_model

L<Zaaksysteem::Betrokkene> instance for handling C<create_case_subcase> queue
items.

=cut

has betrokkene_model => (
    is => 'rw',
    isa => 'Zaaksysteem::Betrokkene',
    predicate => 'has_betrokkene_model',
    accessor => '_betrokkene_model',
);

=head2 rs_zaak

L<Zaaksysteem::Zaken::Resultset> instance for handling C<create_case_subcase> queue
items.

=cut

has rs_zaak => (
    is        => 'rw',
    isa       => 'Zaaksysteem::Zaken::ResultSetZaak',
    predicate => 'has_rs_zaak',
    accessor  => '_rs_zaak',
);

=head2 attribute_index_model

L<Zaaksysteem::Search::Elasticsearch::MappingModel> instance for handling
C<update_attribute_index_schema> queue items.

=cut

has attribute_index_model => (
    is => 'rw',
    isa => 'Zaaksysteem::Search::Elasticsearch::MappingModel',
    predicate => 'has_attribute_index_model',
    accessor => '_attribute_index_model'
);

=head2 rs_subject

L<DBIx::Class::ResultSet> instance for handling C<create_case_subcase> queue
items.

=cut

has rs_subject => (
    is => 'rw',
    isa => 'DBIx::Class::ResultSet',
    predicate => 'has_rs_subject',
    accessor => '_rs_subject',
    handles => {
        find_subject => 'find'
    }
);

=head2 geocoder

L<Zaaksysteem::Geo::Model> instance for queue items that require geocoding
services.

=cut

has geocoder => (
    is => 'rw',
    isa => 'Zaaksysteem::Geo::Model',
    predicate => 'has_geocoder',
);

=head2 statsd

Holds a reference to a L<Zaaksysteem::StatsD::Backend> instance for queue item
handling statistics.

=cut

has statsd => (
    is => 'rw',
    isa => 'Zaaksysteem::StatsD::Backend',
    required => 1
);

=head2 target_resolvers

Holds a map of target names to closures that produce URI endpoints for the
queued item.

=cut

has target_resolvers => (
    is => 'rw',
    isa => 'HashRef[CodeRef]',
    traits => [qw[Hash]],
    default => sub { return {} },
    handles => {
        has_target_resolver => 'exists',
        get_target_resolver => 'get'
    }
);

=head2 json

Custom L<JSON::XS> instance for serializing queue item data. Data is encoded
canonically, so we can search for duplicates of items-to-be-created and
prevent duplication.

=cut

has json => (
    is => 'rw',
    isa => 'JSON::XS',
    default => sub { JSON::XS->new->canonical(1) }
);

=head2 message_queue

Instance of L<Net::AMQP::RabbitMQ> (or compatible module), connected to the
configured RabbitMQ server.

=cut

has message_queue => (
    is => 'rw',
    isa => 'Net::AMQP::RabbitMQ',
    lazy => 1,
    builder => '_build_message_queue',
);

=head2 message_queue_factory

A code reference that returns a connected 'message_queue' instance. Used by
that attribute's builder.

The code ref will be called without arguments.

=cut

has message_queue_factory => (
    is       => 'ro',
    isa      => 'CodeRef',
    required => 1,
    traits   => [qw(Code)],
    handles  => { '_build_message_queue' => 'execute' }
);

=head2 message_queue_channel

Channel on the message bus to send queue item broadcasts on.

=cut

has message_queue_channel => (
    is => 'ro',
    isa => NonEmptyStr,
    default => 'amq.topic',
);

=head2 message_queue_exchange

Exchange to send the message on. Required.

=cut

has message_queue_exchange => (
    is       => 'ro',
    isa      => NonEmptyStr,
    required => 1,
);

=head2 message_queue_route_prefix

RMQ routing key prefix.

Defaults to C<zs.v0>.

=cut

has message_queue_route_prefix => (
    is => 'ro',
    isa => NonEmptyStr,
    default => 'zs.v0'
);

=head2 http_ua

HTTP UserAgent instance, used to retrieve external resources.

Created lazily when needed using L<BTTW::Tools::UA>

=cut

has http_ua => (
    is => 'ro',
    isa => 'LWP::UserAgent',
    required => 0,
    lazy => 1,
    default => sub {
        my $self = shift;
        my $ua = new_user_agent(timeout => 600);

        my $dev_cert_file = '/etc/ssl/certs/zaaksysteem.crt';
        if (-e $dev_cert_file) {
            $self->log->debug("Adding certificate: $dev_cert_file");

            my $devcert = PEM_file2cert($dev_cert_file);
            $ua->ssl_opts(SSL_ca => [$devcert]);
        }

        return $ua;
    },
);

=head1 METHODS

=head2 fetch_item

Retrieves an item from the database. The returned row will be selected with
an update lock, so no other processes should interfere.

=cut

sig fetch_item => 'Str';

sub fetch_item {
    my $self = shift;
    my $id = shift;

    return $self->table->find($id, { for => 'update' });
}

=head2 run_item

Convenience wrapper around L</run>. Retrieves a queue item by its C<id> and
locks further updates on the row within the current transaction.

=cut

sig run_item => 'Str';

sub run_item {
    my $self = shift;
    my $id = shift;

    return $self->run($self->fetch_item($id));
}

=head2 run_item_externally

Runs the specified queue-item "externally", by calling the API.

=cut

sig run_item_externally => 'Zaaksysteem::Backend::Object::Queue::Component';

sub run_item_externally {
    my $self = shift;
    my $item = shift;

    my $url = $self->_get_target_url($item);
    my $res = $self->http_ua->get($url);

    unless ($res->is_success) {
        my $message = sprintf(
            'Externally running queue item "%s" at "%s" failed: "%s"',
            $item->stringify,
            $url,
            $res->message
        );

        $self->log->error($message);
        $self->log->trace(sprintf(
            "Full response:\n%s",
            $res->as_string
        ));

        throw(
            'object/queue/model/run_item_external_failed',
            $message,
        );
    }

    # Force re-retrieval; the external process probably changed bits
    return $item->discard_changes;
}

=head2 create_item

Creates a queue item in the database.

=cut

define_profile create_item => (
    required => {
        label => 'Str',
        type  => 'Str',
    },
    optional => {
        status                => enum([qw[pending waiting]]),
        object_id             => UUID,
        ignore_existing       => 'Bool',
        data                  => 'HashRef',
        target                => 'Str',
        subject               => 'Zaaksysteem::Backend::Subject::Component',
        requires_object_model => 'Bool',
        disable_acl           => 'Bool',
    },
    defaults => {
        status                => 'pending',
        ignore_existing       => 0,
        target                => 'backend',
        disable_acl           => 0,
        requires_object_model => 1,
    }
);

sig create_item => 'HashRef';

sub create_item {
    my $self = shift;
    my $options = assert_profile(shift)->valid;

    my $label     = $options->{label};
    my $type      = $options->{type};
    my $data      = $options->{data} || {};
    my $status    = $options->{status};
    my $object_id = $options->{object_id};

    $data->{ target } = $options->{ target };

    if ($self->log->is_trace) {
        $self->log->trace(sprintf(
            'Creating %s queue item with label: %s, data: %s',
            $type,
            $label,
            dump_terse($data)
        ));
    }

    # This uses the same ordering of overriding hash keys as the create_item
    # call below ends up with.
    my $queue_data = try {
        return $self->json->encode($data);
    } catch {
        throw(
            'queue/create_item/item_data_serialization_failed',
            sprintf('Failed to encode queue item data: %s', $_),
            $data
        );
    };

    my $metadata = {
        disable_acl  => $options->{disable_acl},
        object_model => $options->{requires_object_model},
        target       => $options->{target},
        $options->{subject} ? ( subject_id   => $options->{subject}->id ) : (),
    };

    unless ($options->{ ignore_existing }) {
        my $existing = $self->table->search({
            status => $status,
            type   => $type,
            data   => $queue_data,
        })->first;

        if (defined $existing) {
            $self->log->info(sprintf(
                'Not re-creating %s queue item "%s", already exists (data: %s)',
                $type,
                $label,
                dump_terse($queue_data)
            ));

            return;
        }
    }

    my $item = try {
        return $self->table->create_item(
            $type,
            {
                label    => $label,
                data     => $data,
                status   => $status,
                metadata => $metadata,
                $object_id ? (object_id => $object_id) : (),
            }
        );
    }
    catch {
        throw('queue/create_item/create_failed',
            sprintf('Failed to create %s queue item: %s', $_), dump_terse($data));
    };

    return $item;
}

=head2 create_ordered_item_set

Creates a C<run_ordered_item_set> queue item for the given items and updates
the embedded items accordingly (status to waiting, reference meta item id).

=cut

sig create_ordered_item_set => join ', ', qw[
    Maybe[Zaaksysteem::Backend::Subject::Component]
    @Zaaksysteem::Backend::Object::Queue::Component
];

sub create_ordered_item_set {
    my ($self, $subject, @items) = @_;

    my @item_ids = uniq map { $_->id } grep { defined $_ } @items;
    return unless @item_ids;

    my $item_args = {
        type => 'run_ordered_item_set',
        label => 'Ordered item set',
        data => {
            item_ids => \@item_ids
        }
    };

    if ($subject) {
        $item_args->{ subject } = $subject;
    }

    my $item = $self->create_item($item_args);
    return unless $item;

    $self->table->search({ id => \@item_ids })->update({
        status => 'waiting',
        parent_id => $item->id
    });

    return $item;
}

=head2 broadcast_queued_items

Takes the L<list|/queued_items> of queued items and broadcasts each item to
the queue runner.

=cut

sub broadcast_queued_items {
    my $self = shift;

    my @items = $self->queued_items;
    my $item_count = scalar @items;

    if (!$item_count) {
        $self->log->debug("No queue-items to broadcast");
        return;
    }

    $self->reset_queue;

    my @items_prioritized = (
        (grep { $_->type eq 'create_case' } @items),
        (grep { $_->type eq 'run_ordered_item_set' } @items),
        (grep { $_->type eq 'touch_case' } @items),
        (grep {
               $_->type ne 'create_case'
            && $_->type ne 'run_ordered_item_set'
            && $_->type ne 'touch_case'
        } @items),
    );

    $self->log->info(sprintf(
        "Broadcasting %d queue items",
        $item_count,
    ));

    for my $item (@items_prioritized) {
        try {
            $self->broadcast_item($item)
        } catch {
            $self->log->warn(sprintf(
                'Caught exception while broadcasting queue item: %s',
                $_
            ));
        };
    }

    return;
}

=head2 broadcast_item

Broadcast a single queued item.

=cut

sig broadcast_item => 'Zaaksysteem::Backend::Object::Queue::Component';

sub broadcast_item {
    my $self = shift;
    my $item = shift;

    $self->log->debug(sprintf(
        'Broadcasting queue item %s',
        $item->stringify
    ));

    if ($item->is_changed) {
        throw('queue/broadcast/item_has_changes', sprintf(
            'Queue item %s has unsaved changes, refusing to broadcast',
            $item->stringify
        ));
    }

    my $data = $item->data || {};
    my $params = $data->{ parameters } || {};

    $params->{ url } = $self->_get_target_url($item);
    $params->{ instance_hostname } //= $self->instance_hostname;

    my $routing_key = join '.', (
        $self->message_queue_route_prefix,
        $item->type
    );

    $self->log->trace(sprintf(
        "Broadcasting item with routing key = '%s', data: %s",
        $routing_key,
        dump_terse($params)
    )) if $self->log->is_trace;

    $self->message_queue->publish(
        $self->message_queue_channel,
        $routing_key,
        $self->json->encode($params),
        { exchange => $self->message_queue_exchange },
    );

    $self->statsd->increment('broadcast_queued_items');

    return;
}

=head2 mark_item

Convenience method that can mark specific queued items with the specified
status, in a locking fashion to prevent races.

Optionally returns a fresh copy of the item (depends on context of the call).

    # Safely update the status of the specified item and re-fetches the row
    # from db.
    my $item = $model->mark_item($uuid, 'waiting');

    # Also safely updates the status, but won't refetch
    $model->mark_item($uuid, 'failed');

=cut

sig mark_item => 'Str, Str';

sub mark_item {
    my $self = shift;
    my $id = shift;
    my $status = shift;

    my $item = $self->fetch_item($id);

    $item->update({ status => $status });

    # Only re-fetch when required.
    return $item->discard_changes if defined wantarray;
}

=head2 run

=cut

sig run => 'Zaaksysteem::Backend::Object::Queue::Component';

sub run {
    my $self = shift;
    my $item = shift;

    unless ($item->is_ready) {
        throw('queue/run/item_not_ready', sprintf(
            'Unable to run queue item "%s", unexpected status "%s"',
            $item->label,
            $item->status
        ));
    }

    # void context signals we don't care about new state
    $item->update({
        status => 'running',
        date_started => \"statement_timestamp() at time zone 'UTC'" # easy millisec precision
    });

    my $ret;

    if ($self->log->is_trace) {
        $self->log->trace(sprintf(
            'Starting dispatch for queue item "%s"',
            $item->stringify
        ));
    }

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    try {
        my $handler = $self->can($item->type);

        unless ($handler) {
            throw('queue/run/item_type_not_supported', sprintf(
                'Unable to handle item "%s"',
                $item->stringify
            ));
        }

        $handler->($self, $item);

        $item->status('finished');
    } catch {
        my $error = $_;

        $self->log->warn(sprintf(
            'Exception caught in queue item handler for item "%s": %s',
            $item->stringify,
            $error
        ));

        if ($item->get_column('object_id')) {
            $item->object_id->trigger_logging('object/queue/item_failed', {
                data => {
                    item => $item->TO_JSON,
                    item_label => $item->stringify,
                    exception => "$error"
                }
            });
        }

        $item->status('failed');
    } finally {
        $ret = $item->update({
            date_finished => \"statement_timestamp() at time zone 'UTC'"
        })->discard_changes;
    };

    my $silo = $ENV{ZS_SILO_NAME} // 'no_silo';

    my $end = Zaaksysteem::StatsD->statsd->end("queue.run.$silo." . $item->type . '.time', $t0);

    Zaaksysteem::StatsD->statsd->increment("queue.run.$silo." . $item->type, 1);
    Zaaksysteem::StatsD->statsd->increment("queue.run.$silo." . $item->type . "." . $item->status, 1);

    $self->_log_item_timing($silo, $ret->date_created, $ret->date_finished, $ret->status, $item->type);

    $self->log->info(sprintf("Dispatch duration for %s: %d ms", $item->stringify, $end));

    return $ret;
}

sub _log_item_timing {
    my ($self, $silo, $created, $finish, $status, $type) = @_;

    my $time_diff = $finish - $created;
    my $time_diff_ms = int($time_diff->in_units('nanoseconds') / 1_000_000);

    Zaaksysteem::StatsD->statsd->timing("queue_item.$silo.$type.$status.age", $time_diff_ms);

    return;
}

=head2 gc

Garbage collect the queue item table.

Cleans up all succesfully executed rows older than the required L<DateTime>
argument.

=cut

sig gc => 'DateTime,?Int';

sub gc {
    my $self = shift;
    my $since = shift;
    my $rows = shift || 50;

    return unless $ENV{ZS_FEATURE_FLAG_QUEUE_CLEANUP};

    my $rs = $self->table->search(
        {
            status => 'finished',
            date_finished =>
                { '<=' => $self->schema->format_datetime_object($since) }
        },
        {
            order_by => { '-asc' => 'date_finished' },
            columns  => 'uuid',
            rows     => $rows,
        }
    );
    $self->log->debug(
        sprintf("Deleting %d finished queue entries", $rs->count));
    $rs->delete;

    return;
}

=head2 cleanup_old_queue_items

Queue item cleanup.

Cleans up all queue-items older than the required L<DateTime> argument that
do not have status "pending".

=cut

sig cleanup_old_queue_items => 'DateTime';

sub cleanup_old_queue_items {
    my $self = shift;
    my $since = shift;

    $self->table->search({
        # Pending queue-items will be retried.
        status       => { '!=' => 'pending' },
        date_created => { '<' => $self->schema->format_datetime_object($since) },
    })->delete;

    return;
}

=head2 object_model

Read accessor for the C<object_model> attribute.

=head3 Exceptions

=over 4

=item queue/run/object_model_required

Thrown when the object model is not set.

=back

=cut

sub object_model {
    my $self = shift;

    unless ($self->has_object_model) {
        throw(
            'queue/run/object_model_required',
            'A request for an object model was made, but it was not set'
        );
    }

    return $self->_object_model;
}

=head2 subject_model

Read accessor for the C<subject_model> attribute.

=head3 Exceptions

=over 4

=item queue/run/subject_model_required

Thrown when the subject model is not set.

=back

=cut

sub subject_model {
    my $self = shift;

    unless ($self->has_subject_model) {
        throw(
            'queue/run/subject_model_required',
            'A request for a subject model was made, but it was not set'
        );
    }

    return $self->_subject_model;
}

=head2 betrokkene_model

Read accessor for the C<betrokkene_model> attribute.

=head3 Exceptions

=over 4

=item queue/run/betrokkene_model_required

Thrown when the betrokkene model is not set.

=back

=cut

sub betrokkene_model {
    my $self = shift;

    unless ($self->has_betrokkene_model) {
        throw(
            'queue/run/betrokkene_model_required',
            'A request for a betrokkene model was made, but it was not set'
        );
    }

    return $self->_betrokkene_model;
}

=head2 attribute_index_model

Read accessor for the C<attribute_index_model> attribute.

=head3 Exceptions

=over 4

=item C<queue/run/attribute_index_model/required>

Thrown when the attribute index model is accessed, but not set.

=back

=cut

sub attribute_index_model {
    my $self = shift;

    unless ($self->has_attribute_index_model) {
        throw(
            'queue/run/attribute_index_model_required',
            'A request for the attribute index model was made, but it was not set'
        );
    }

    return $self->_attribute_index_model;
}

=head2 rs_subject

Read accessor for the C<rs_subject> attribute.

=head3 Exceptions

=over 4

=item queue/run/rs_subject_required

Thrown when the subject table is not set.

=back

=cut

sub rs_subject {
    my $self = shift;

    unless ($self->has_rs_subject) {
        throw(
            'queue/run/rs_subject_required',
            'A request for a rs_subject was made, but it was not set'
        );
    }

    return $self->_rs_subject;
}

=head2 schema

Returns a L<Zaaksysteem::Schema> instance (derived from L</table>).

=cut

sub schema {
    return shift->table->result_source->schema;
}

=head2 case_model

Builds a fresh L<Zaaksysteem::Zaken::Model> instances, derived from our own
dependencies.

Expects a C<integer> C<subject_id> parameter for the
L<Zaaksysteem::Zaken::Model/user> parameter.

    my $case_model = $queue_model->case_model(123);

=cut

sig case_model => '?Int';

sub case_model {
    my $self = shift;
    my $subject_id = shift;

    my %new_args = (
        schema         => $self->schema,
        base_uri       => $self->base_uri,
        object_model   => $self->object_model,
        subject_model  => $self->subject_model,
        queue_model    => $self,
        document_model => $self->_document_model,
        rs_zaak        => $self->_rs_zaak,
    );
    $new_args{user} = $self->find_subject($subject_id) if $subject_id;

    return Zaaksysteem::Zaken::Model->new(%new_args);
}

=head2 _get_target_url

Use the target resolver to build the URL to run the specified queue item.

Throws an exception if no resolver exists for the item's target.

=cut

sub _get_target_url {
    my $self = shift;
    my $item = shift;

    my $target = $item->target;

    unless ($self->has_target_resolver($target)) {
        throw('queue/broadcast/unknown_target', sprintf(
            'Queue item "%s" has an unknown target "%s"',
            $item->stringify,
            $target,
        ));
    }

    return '' . $self->get_target_resolver($target)->($item);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
