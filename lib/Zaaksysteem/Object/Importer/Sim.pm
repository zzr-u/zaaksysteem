package Zaaksysteem::Object::Importer::Sim;

use Moose::Role;

use XML::Twig;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Object::Importer::Sim - Import products in Kluwer XML export format

=head1 DESCRIPTION

=head1 CONSTANTS

=head2 FILESTORE_MODEL

=cut

use constant FILESTORE_MODEL => 'Filestore';

=head1 ATTRIBUTES

=head1 METHODS

=head2 handles

=cut

sub handles {
    my $self = shift;
    my $object_type = shift;

    return $object_type =~ m/product|qa/;
}

=head2 execute_import_state

=cut

sub execute_import_state {
    my $self = shift;
    my $filestore = $self->schema->resultset(FILESTORE_MODEL);

    my $fileref = $filestore->find_by_uuid($self->state->import_fileref);

    my $doc = XML::Twig->new(
        twig_handlers => { 'feed/entry' => sub {
            $self->import_entry($_->simplify);
        }}
    );

    $doc->parsefile($fileref->get_path);
}

=head2 import_entry

=cut

sub import_entry {
    my $self = shift;
    my $entry = shift;

    return unless $entry->{ id };

    my $option = $self->state->get_by('id', $entry->{ id });

    # If not in the state, or the option is to ignore, this item should not be processed
    return unless $option;
    return if $option->selected_option eq 'ignore';

    if($entry->{ type } eq 'simloket') {
        return $self->import_product_entry($entry, $option);
    }

    if($entry->{ type } eq 'simfaq') {
        return $self->import_faq_entry($entry, $option);
    }

    warn "Encountered unknown entry type: " . $entry->type;

    return;
}

sub import_product_entry {
    my $self = shift;
    my $entry = shift;
    my $option = shift;

    my $product;
    my $meta = $self->model->find_type_meta('product');

    if($option->selected_option eq 'create') {
        my $new_product = $meta->new_object(
            {
                pdc_name        => "",
                pdc_description => "",
            }
        );

        $self->copy_product_entry_fields($entry, $new_product);

        if($option->config->{ product_name }) {
            $new_product->pdc_name($option->config->{ product_name });
        }

        $product = $self->model->save(object => $new_product);
    }

    if($option->selected_option eq 'existing') {
        unless($option->config->{ product_id }) {
            throw('object/import/kluwer_pdc', 'Item configured for linking with an existing product, but no product_id supplied');
        }

        my $existing_product = $self->model->retrieve(uuid => $option->config->{ product_id });

        unless($existing_product) {
            throw('object/import/kluwer_pdc', sprintf(
                'Item %s configured for updating, but original product could not be found',
                $option->config->{ product_id }
            ));
        }

        $self->copy_product_entry_fields($entry, $existing_product);

        $product = $self->model->save(object => $existing_product);
    }

    if($option->selected_option eq 'update') {
        my ($existing_product) = $self->model->search('product', {
            pdc_external_id => $entry->{ id }
        });

        unless($existing_product) {
            throw('object/import/kluwer_pdc', sprintf(
                'Item %s configured for updating, but original product could not be found',
                $entry->{ id }
            ));
        }

        $self->copy_product_entry_fields($entry, $existing_product);

        $product = $self->model->save(object => $existing_product);
    }

    for my $relation (@{ $option->relations }) {
        next unless $relation->{ local_id };

        my $related = $self->model->retrieve(uuid => $relation->{ local_id });

        $product->relate($related);
    }

    $self->model->save(object => $product);

    $option->source_uri(sprintf('/object/%s', $product->id));
}

sub import_faq_entry {
    my $self = shift;
    my $entry = shift;
    my $option = shift;

    my $faq;
    my $meta = $self->model->find_type_meta('vraag');

    if($option->selected_option eq 'create') {
        my $new_faq = $meta->new_object(
            {
                faq_question => "",
                faq_answer   => "",
            }
        );

        $self->copy_faq_entry_fields($entry, $new_faq);

        if($option->config->{ product_name }) {
            $new_faq->naam($option->config->{ product_name });
        }

        $faq = $self->model->save(object => $new_faq);
    }

    if($option->selected_option eq 'existing') {
        unless($option->config->{ product_id }) {
            throw('object/import/kluwer_faq', 'Item configured for linking with an existing product, but no product_id supplied');
        }

        my $existing_faq = $self->model->retrieve(uuid => $option->config->{ product_id });

        unless($existing_faq) {
            throw('object/import/kluwer_faq', sprintf(
                'Item %s configured for updating, but original product could not be found',
                $option->config->{ product_id }
            ));
        }

        $self->copy_faq_entry_fields($entry, $existing_faq);

        $faq = $self->model->save(object => $existing_faq);
    }

    if($option->selected_option eq 'update') {
        my ($existing_faq) = $self->model->search('vraag', {
            pdc_external_id => $entry->{ id }
        });

        unless($existing_faq) {
            throw('object/import/kluwer_faq', sprintf(
                'Item %s configured for updating, but original product could not be found',
                $entry->{ id }
            ));
        }

        $self->copy_faq_entry_fields($entry, $existing_faq);

        $faq = $self->model->save(object => $existing_faq);
    }

    for my $relation (@{ $option->relations }) {
        next unless $relation->{ local_id };

        my $related = $self->model->retrieve(uuid => $relation->{ local_id });

        warn "relating $related";

        $faq->relate($related);
    }

    $self->model->save(object => $faq);

    $option->source_uri(sprintf('/object/%s', $faq->id));
}

=head2 copy_product_entry_fields

Copy the fields from the imported XML file to the product object.

=cut

sub copy_product_entry_fields {
    my $self = shift;
    my $item = shift;
    my $product = shift;

    $product->pdc_name($item->{ title });
    $product->pdc_price($item->{ content }{ kosten });
    $product->pdc_external_id($item->{ id });

    $product->pdc_description($self->extract_text(
        $item->{ content }{ samenvatting }{ tekst }
    ));

    $product->pdc_approach($self->extract_text(
        $item->{ content }{ gangvanzaken }{ tekst }
    ));

    $product->pdc_terms($self->extract_text(
        $item->{ content }{ voorwaarden }{ tekst }
    ));

    if (defined $item->{ content }{ scdoelgroepen }) {
        my $doelgroep = $item->{ content }{ scdoelgroepen }{ scdoelgroep };
        my @doelgroepen = ref $doelgroep eq 'ARRAY' ? @{ $doelgroep } : $doelgroep;

        $product->pdc_audience(join(", ", @doelgroepen));
    }

    if (exists $item->{ content }{ werkinstructie }{ tekst }) {
        $product->pdc_work_instruction($self->extract_text(
            $item->{ content }{ werkinstructie }{ tekst }
        ));
    }

    if (exists $item->{ content }{ internecontactpersonen }{ tekst }) {
        $product->pdc_internal_contact_persons($self->extract_text(
            $item->{ content }{ internecontactpersonen }{ tekst }
        ));
    }

    return $product;
}

=head2 copy_faq_entry_fields

Copy the fields from the imported XML file to the "vraag" object.

=cut

sub copy_faq_entry_fields {
    my $self = shift;
    my $item = shift;
    my $faq = shift;

    $faq->faq_title($item->{ title });
    $faq->faq_question($item->{ title });

    $faq->faq_answer($self->extract_text(
        $item->{ content }{ antwoord }{ communicatiekanaal }{ tekst }
    ));

    $faq->pdc_external_id($item->{ id });

    return $faq;
}

=head2 extract_text

=cut

sub extract_text {
    my $self = shift;
    my $subtree = shift;

    my @texts;

    if(ref $subtree eq 'ARRAY') {
        push(@texts, @{ $subtree });
    } else {
        push(@texts, $subtree);
    }

    return join "\n\n", map { $_->{ content } } grep { defined } @texts;
}

=head2 hydrate_import_state

=cut

sub hydrate_import_state {
    my $self = shift;
    my $fileref = shift;

    unless($self->validate($fileref)) {
       die('Importbestand kon niet geinterpreteerd worden als geldige Kluwer XML');
    }

    $self->state->import_fileref($fileref->uuid);
    $self->state->format($self->format);
    $self->state->object_type($self->object_type);

    my @items_found;
    my $doc = XML::Twig->new(
        twig_handlers => { 'feed/entry' => sub {
            my $twig = shift;
            my $entry = shift;

            my @relations;

            for my $faqRelation ($entry->get_xpath('content/verwijzingen_andere_vragen/koppelbox/entry')) {
                push @relations, {
                    external_id => $faqRelation->children_text('uuid') || undef,
                    label => $faqRelation->children_text('title') || undef,
                    type => 'vraag'
                };
            }

            for my $productRelation ($entry->get_xpath('content/producten/entry')) {
                push @relations, {
                    external_id => $productRelation->children_text('uuid') || undef,
                    label => $productRelation->children_text('title') || undef,
                    type => 'product'
                };
            }

            my $simplified = $_->simplify;

            push @items_found, $simplified->{id} if $simplified->{id};

            return $self->parse($simplified, \@relations);
        }}
    );

    $doc->parsefile($fileref->get_path);

    my $class = $self->object_type;
    $class = 'vraag' if $class eq 'qa';

    my $items = $self->_find_items_to_delete($class, \@items_found);
    $self->state->items_to_delete($items);
}

sub _find_items_to_delete {
    my $self = shift;
    my ($class, $items) = @_;

    my $results = $self->model->rs->search(
        {
            "me.object_class" => $class,
            "index_hstore->'pdc_external_id'" => { -not_in => $items }
        }
    );

    return [
        map {
            my $obj = $self->model->inflate_from_row($_);

            my $title;
            if ($obj->can('pdc_name')) {
                $title = $obj->pdc_name;
            }
            else {
                $title = $obj->faq_question;
            }

            +{
                title => $title,
                id    => $obj->id,
            };
        } $results->all
    ];
}

=head2 parse

=cut

use constant STRINGS => {
    simloket => {
        create => 'Importeer als nieuw product',
        update => 'Update bestaand product',
        ignore => 'Negeer product'
    },
    simfaq => {
        create => 'Importeer als nieuwe vraag',
        update => 'Update bestaande vraag',
        ignore => 'Negeer vraag'
    }
};

sub parse {
    my $self = shift;
    my $item = shift;
    my $relations = shift;

    return unless $item->{ id };

    my $row;
    my $tmp_object;

    my $product_meta = $self->model->find_type_meta('product');
    my $vraag_meta = $self->model->find_type_meta('vraag');

    if($item->{ type } eq 'simloket') {
        ($row) = $self->model->search('product', {
            pdc_external_id => $item->{ id }
        });

        my $new_product = $product_meta->new_object('product', {
            pdc_name        => ''
        });

        $tmp_object = $self->copy_product_entry_fields(
            $item,
            $new_product
        );
    }

    if($item->{ type } eq 'simfaq') {
        ($row) = $self->model->search('vraag', {
            pdc_external_id => $item->{ id }
        });

        my $new_faq = $vraag_meta->new_object('vraag', {
            faq_title => ''
        });

        $tmp_object = $self->copy_faq_entry_fields(
            $item,
            $new_faq,
        );
    }

    for my $relation (@{ $relations }) {
        my ($related) = $self->model->search($relation->{ type }, {
            pdc_external_id => $relation->{ external_id }
        });

        next unless $related;

        $relation->{ local_id } = $related->id;
    }

    my $state_item = $self->state->new_item({
        id => $item->{ id },
        label => $item->{ title },
        properties => $tmp_object->TO_JSON->{ values },
        relations => $relations
    });

    my $strings = STRINGS()->{ $item->{ type } };

    # New import item. full control over it
    unless($row) {
        $state_item->add_default_option('create', $strings->{ create });
        $state_item->add_option('ignore', $strings->{ ignore });

        return;
    }

    $state_item->existing($row->TO_JSON->{ values });

    $state_item->add_default_option('update', $strings->{ update });
    $state_item->add_option('ignore', $strings->{ ignore });
}

=head2 validate

=cut

sub validate {
    my $self = shift;
    my $fileref = shift;

    return unless($fileref->mimetype eq 'application/xml');

    my $xml = XML::XPath->new(filename => $fileref->get_path);

    return unless $xml;

    my @nodes = $xml->find('/feed/entry/type')->get_nodelist;

    return unless scalar @nodes;

    my $check = 'simfaq|simloket';

    if ($self->object_type && $self->object_type eq 'qa') {
        $check = 'simfaq';
    }

    if ($self->object_type && $self->object_type eq 'product') {
        $check = 'simloket';
    }

    return if grep { $_->string_value !~ m[^($check)$] } @nodes;

    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 import_faq_entry

TODO: Fix the POD

=cut

=head2 import_product_entry

TODO: Fix the POD

=cut

