use utf8;
package Zaaksysteem::Schema::CaseAction;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseAction

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<case_action>

=cut

__PACKAGE__->table("case_action");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'case_action_id_seq'

=head2 case_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 casetype_status_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 type

  data_type: 'varchar'
  is_nullable: 1
  size: 64

=head2 label

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 automatic

  data_type: 'boolean'
  is_nullable: 1

=head2 data

  data_type: 'text'
  is_nullable: 1

=head2 state_tainted

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 data_tainted

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "case_action_id_seq",
  },
  "case_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "casetype_status_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "type",
  { data_type => "varchar", is_nullable => 1, size => 64 },
  "label",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "automatic",
  { data_type => "boolean", is_nullable => 1 },
  "data",
  { data_type => "text", is_nullable => 1 },
  "state_tainted",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "data_tainted",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 case_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("case_id", "Zaaksysteem::Schema::Zaak", { id => "case_id" });

=head2 casetype_status_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeStatus>

=cut

__PACKAGE__->belongs_to(
  "casetype_status_id",
  "Zaaksysteem::Schema::ZaaktypeStatus",
  { id => "casetype_status_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-06-15 14:24:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:JycKONKmnUNQP2iYli947g

use JSON::XS qw();

__PACKAGE__->load_components(
    '+Zaaksysteem::Backend::Case::Action::Component',
    __PACKAGE__->load_components()
);

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Case::Action::ResultSet');

__PACKAGE__->inflate_column('data', {
    inflate => sub { JSON::XS->new->decode(shift // '{}') },
    deflate => sub { JSON::XS->new->encode(shift // {}) }
});

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

