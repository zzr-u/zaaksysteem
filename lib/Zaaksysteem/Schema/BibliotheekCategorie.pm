use utf8;
package Zaaksysteem::Schema::BibliotheekCategorie;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::BibliotheekCategorie

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<bibliotheek_categorie>

=cut

__PACKAGE__->table("bibliotheek_categorie");

=head1 ACCESSORS

=head2 search_index

  data_type: 'tsvector'
  is_nullable: 1

=head2 search_term

  data_type: 'text'
  is_nullable: 1

=head2 object_type

  data_type: 'text'
  default_value: 'bibliotheek_categorie'
  is_nullable: 1

=head2 searchable_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'searchable_searchable_id_seq'

=head2 search_order

  data_type: 'text'
  is_nullable: 1

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bibliotheek_categorie_id_seq'

=head2 naam

  data_type: 'varchar'
  is_nullable: 1
  size: 256

=head2 label

  data_type: 'text'
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 help

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 system

  data_type: 'integer'
  is_nullable: 1

=head2 pid

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=cut

__PACKAGE__->add_columns(
  "search_index",
  { data_type => "tsvector", is_nullable => 1 },
  "search_term",
  { data_type => "text", is_nullable => 1 },
  "object_type",
  {
    data_type     => "text",
    default_value => "bibliotheek_categorie",
    is_nullable   => 1,
  },
  "searchable_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "searchable_searchable_id_seq",
  },
  "search_order",
  { data_type => "text", is_nullable => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bibliotheek_categorie_id_seq",
  },
  "naam",
  { data_type => "varchar", is_nullable => 1, size => 256 },
  "label",
  { data_type => "text", is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "help",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "system",
  { data_type => "integer", is_nullable => 1 },
  "pid",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<bibliotheek_categorie_uuid_idx>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("bibliotheek_categorie_uuid_idx", ["uuid"]);

=head1 RELATIONS

=head2 bibliotheek_categories

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekCategorie>

=cut

__PACKAGE__->has_many(
  "bibliotheek_categories",
  "Zaaksysteem::Schema::BibliotheekCategorie",
  { "foreign.pid" => "self.id" },
  undef,
);

=head2 bibliotheek_kenmerkens

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekKenmerken>

=cut

__PACKAGE__->has_many(
  "bibliotheek_kenmerkens",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { "foreign.bibliotheek_categorie_id" => "self.id" },
  undef,
);

=head2 bibliotheek_notificaties

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekNotificaties>

=cut

__PACKAGE__->has_many(
  "bibliotheek_notificaties",
  "Zaaksysteem::Schema::BibliotheekNotificaties",
  { "foreign.bibliotheek_categorie_id" => "self.id" },
  undef,
);

=head2 bibliotheek_sjablonens

Type: has_many

Related object: L<Zaaksysteem::Schema::BibliotheekSjablonen>

=cut

__PACKAGE__->has_many(
  "bibliotheek_sjablonens",
  "Zaaksysteem::Schema::BibliotheekSjablonen",
  { "foreign.bibliotheek_categorie_id" => "self.id" },
  undef,
);

=head2 object_bibliotheek_entries

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectBibliotheekEntry>

=cut

__PACKAGE__->has_many(
  "object_bibliotheek_entries",
  "Zaaksysteem::Schema::ObjectBibliotheekEntry",
  { "foreign.bibliotheek_categorie_id" => "self.id" },
  undef,
);

=head2 pid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekCategorie>

=cut

__PACKAGE__->belongs_to(
  "pid",
  "Zaaksysteem::Schema::BibliotheekCategorie",
  { id => "pid" },
);

=head2 zaaktypes

Type: has_many

Related object: L<Zaaksysteem::Schema::Zaaktype>

=cut

__PACKAGE__->has_many(
  "zaaktypes",
  "Zaaksysteem::Schema::Zaaktype",
  { "foreign.bibliotheek_categorie_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-01-29 14:29:47
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:5nvoShoMBVDDjGiPYeV9OA

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::BibliotheekCategorie');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::BibliotheekCategorie",
    "+Zaaksysteem::Helper::ToJSON",
    __PACKAGE__->load_components()
);


__PACKAGE__->has_many(
  "categorien",
  "Zaaksysteem::Schema::BibliotheekCategorie",
  { "foreign.pid" => "self.id" },
);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

