package Zaaksysteem::Schema::ObjectGrafiekAfgehandeld;

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->table_class('DBIx::Class::ResultSource::View');


__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "Core");
__PACKAGE__->table("object_grafiek_afgehandeld");
__PACKAGE__->add_columns(
  "periode",
  {
    data_type => "DATETIME",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "zaken",
  {
    data_type => "integer",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
);

__PACKAGE__->result_source_instance->mk_classdata('view_definition_template');

__PACKAGE__->result_source_instance->view_definition_template(
    "
    select period.date periode, count(object_data.object_id) as zaken from
        (
            select generate_series(?::timestamp, ?::timestamp, INTERVAL)
                as date
        ) as period left outer join object_data on hstore_to_timestamp((index_hstore -> 'case.date_of_completion')) between period.date
        AND (
            period.date + interval INTERVAL
        )
        AND object_data.object_id IN INNERQUERY
        group by period.date order by period.date
    "
);

__PACKAGE__->result_source_instance->view_definition(
    __PACKAGE__->result_source_instance->view_definition_template
);

__PACKAGE__->result_source_instance->is_virtual(1);


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

