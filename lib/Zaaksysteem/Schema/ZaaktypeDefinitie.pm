use utf8;
package Zaaksysteem::Schema::ZaaktypeDefinitie;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ZaaktypeDefinitie

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<zaaktype_definitie>

=cut

__PACKAGE__->table("zaaktype_definitie");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaaktype_definitie_id_seq'

=head2 openbaarheid

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 handelingsinitiator

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 grondslag

  data_type: 'text'
  is_nullable: 1

=head2 procesbeschrijving

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 afhandeltermijn

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 afhandeltermijn_type

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 selectielijst

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 servicenorm

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 servicenorm_type

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 pdc_voorwaarden

  data_type: 'text'
  is_nullable: 1

=head2 pdc_description

  data_type: 'text'
  is_nullable: 1

=head2 pdc_meenemen

  data_type: 'text'
  is_nullable: 1

=head2 pdc_tarief

  data_type: 'text'
  is_nullable: 1

=head2 omschrijving_upl

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 aard

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 extra_informatie

  data_type: 'text'
  is_nullable: 1

=head2 preset_client

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 extra_informatie_extern

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaaktype_definitie_id_seq",
  },
  "openbaarheid",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "handelingsinitiator",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "grondslag",
  { data_type => "text", is_nullable => 1 },
  "procesbeschrijving",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "afhandeltermijn",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "afhandeltermijn_type",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "selectielijst",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "servicenorm",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "servicenorm_type",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "pdc_voorwaarden",
  { data_type => "text", is_nullable => 1 },
  "pdc_description",
  { data_type => "text", is_nullable => 1 },
  "pdc_meenemen",
  { data_type => "text", is_nullable => 1 },
  "pdc_tarief",
  { data_type => "text", is_nullable => 1 },
  "omschrijving_upl",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "aard",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "extra_informatie",
  { data_type => "text", is_nullable => 1 },
  "preset_client",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "extra_informatie_extern",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 zaaktype_nodes

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->has_many(
  "zaaktype_nodes",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { "foreign.zaaktype_definitie_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-06-15 14:24:45
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:VtDZOM8vq6l4vAAGH0j35w

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ZaaktypeDefinitie');

__PACKAGE__->load_components(
    '+Zaaksysteem::DB::Component::ZaaktypeDefinitie',
    __PACKAGE__->load_components()
);



# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

