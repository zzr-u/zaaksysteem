use utf8;
package Zaaksysteem::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_classes;


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2017-08-08 09:35:56
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:QpH2NoNR5kCk+SNcBJ03nA

use Moose;

__PACKAGE__->storage_type('Zaaksysteem::DB::Storage');

use BTTW::Tools;
use File::Spec::Functions qw(catdir);
use JSON::XS;
use List::Util qw(uniq);
use Log::Log4perl;
use Module::Find;
use Search::Elasticsearch;
use Zaaksysteem::Betrokkene;
use Zaaksysteem::Cache;
use Zaaksysteem::Geo::BAG::Model;

use Zaaksysteem::Store::Redis;

=head1 NAME

Zaaksysteem::Schema - Schema definition for Zaaksysteem datamodel

=head1 DESCRIPTION

=head1 CONSTANTS

=head1 ATTRIBUTES

=head2 current_user_ou_ancestry

Stores a list of groups the L</current_user> is (implicitly via inheritance)
member of.

=cut

has 'current_user_ou_ancestry'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self                = shift;

        return [ map {
            {
                ou_id => $_->id,
                name  => $_->name,
            }
        } @{ $self->current_user->inherited_groups } ];
    },
    'clearer' => 'clear_current_user_ou_ancestry',
);

=head2 users

=head2 current_user

Stores a weak reference to the currently logged in user.

=cut

has current_user => (
    is       => 'rw',
    clearer  => 'clear_current_user',
    predicate => 'has_current_user',
);

=head2 customer_config

Stores a weak reference to the cutomer config for the current request.

=cut

has customer_config => (
    is       => 'rw',
    lazy     => 1,
    isa      => 'Maybe[HashRef]',
    default  => sub {
        my $self = shift;
        if ($self->customer_instance) {
            return $self->customer_instance;
        }
        return {};
    },
    clearer => 'clear_customer_config',
);


=head2 cache

=cut

has cache => (
    'is'      => 'rw',
    'clearer' => 'clear_cache',
);

=head2 betrokkene_pager

=head2 catalyst_config

=cut

for my $attr (qw/betrokkene_pager catalyst_config customer_instance/) {
    has $attr => (
        'is'        => 'rw',
        'lazy'      => 1,
        'default'   => sub { return {} },
        'clearer'   => "clear_$attr",
    );
}

=head2 redis

An accessor to Redis

=cut

has redis => (
    is      => 'ro',
    isa     => 'Zaaksysteem::Store::Redis',
    lazy    => 1,
    builder => '_build_redis',
    clearer => 'clear_redis',
);

sub _build_redis {
    my $self = shift;

    return Zaaksysteem::Store::Redis->new(
        %{$self->catalyst_config->{'Store::Redis'}},
        prepend_key => $self->customer_config->{instance_uuid},
    );

}

=head2 bag_model

An instance of L<Zaaksysteem::Geo::BAG::Model>. Should be configured for the
current request instance.

=cut

has bag_model => (
    is       => 'rw',
    isa      => 'Zaaksysteem::Geo::BAG::Model',
    lazy     => 1,
    clearer  => 'clear_bag_model',
    default  => sub { return $_[0]->bag_model_builder->(); },
    predicate => 'has_bag_model',
);

=head2 bag_model_builder

BAG model factory.

Should be a code ref that returns the BAG model for the current instance.

=cut

has bag_model_builder => (
    is => 'rw',
    isa => 'CodeRef',
    clearer => 'clear_bag_model_builder',
    predicate => 'has_bag_model_builder',
);

=head2 customer_instance

Information about the customer instance

=cut

has customer_instance => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { {} },
    lazy    => 1,
);

=head2 active_modules

Cached array of active sysin modules

=cut

has active_modules => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    lazy => 1,
    traits => [qw[Array]],
    builder => '_build_active_modules',
    handles => {
        _find_active_module => 'first',
    }
);

=head1 METHODS

=head2 betrokkene_model

Factory method that instantiates fresh L<Zaaksysteem::Betrokkene> instances.

=cut

sub betrokkene_model {
    my $self            = shift;

    my $dbic            = $self->clone;

    my $stash           = {};

    if ($self->betrokkene_pager) {
        for my $key (keys %{ $self->betrokkene_pager }) {
            $stash->{ $key } = $self->betrokkene_pager->{ $key };
        }
    }

    $dbic->cache($self->cache);

    return Zaaksysteem::Betrokkene->new(
        dbic            => $dbic,
        stash           => $stash,
        config          => $self->catalyst_config,
        customer        => $self->customer_instance,
    );
}

=head2 set_current_user

Installs the provided subject as current_user

=cut

sub set_current_user {
    my $self = shift;
    my $subject = shift;

    $self->default_resultset_attributes->{ current_user } = $subject;
    $self->current_user($subject);

    return $self;
}

=head2 lock

    $schema->lock($integer);

Set a blocking lock on the database.

This uses C<pg_advisory_lock>.
For more information see L<https://www.postgresql.org/docs/9.1/static/functions-admin.html>

=cut

sub lock {
    my ($self, $id) = @_;
    $self->storage->dbh_do(sub {
        my ($storage, $dbh) = @_;
        $dbh->do('SELECT pg_advisory_lock(?)', undef, $id);
    });
}

=head2 unlock

    $schema->unlock($integer);

Unlock a lock on the database

This uses C<pg_advisory_unlock>.
For more information see L<https://www.postgresql.org/docs/9.1/static/functions-admin.html>

=cut

sub unlock {
    my ($self, $id) = @_;
    $self->storage->dbh_do(sub {
        my ($storage, $dbh) = @_;
        $dbh->do('SELECT pg_advisory_unlock(?)', undef, $id);
    });
}

=head2 try_lock

    $schema->try_lock($integer);

Try setting a lock on the database, when a lock is aquired returns true otherwise false.

This uses C<pg_try_advisory_lock>.
For more information see L<https://www.postgresql.org/docs/9.1/static/functions-admin.html>

=cut

sub try_lock {
    my ($self, $id) = @_;
    my @lock = $self->storage->dbh_do(sub {
        my ($storage, $dbh) = @_;
        $dbh->selectrow_array('SELECT pg_try_advisory_lock(?)', undef, $id);
    });
    return $lock[0];
}

=head2 format_datetime_object

    $schema->format_datetime_object(DateTime->now());

Formats the DateTime object as recommended by
L<DBIx::Class::Manual::Cookbook/Formatting DateTime objects in queries>

=cut

sub format_datetime_object {
    my $self = shift;
    my $dt   = shift;
    return $self->storage->datetime_parser->format_datetime($dt);
}

=head2 has_active_module

Predicate helper that checks if a given module exists in the list of active
modules.

    if ($schema->has_active_module('authldap')) {
        ...
    }

=cut

sub has_active_module {
    my ($self, $module_name) = @_;

    return defined $self->_find_active_module(sub { $_ eq $module_name });
}

=head2 is_multi_tenant

Boolean value indicating that this instance is running in multi tenant mode

=cut

sub is_multi_tenant {
    my $self = shift;
    if (exists $self->customer_instance->{VirtualHosts}) {
        return keys %{ $self->customer_instance->{VirtualHosts} } > 1 ? 1 : 0;
    }
    return 0;
}

=head2 customer_hostnames

Returns an array ref with the hostnames configured for this instance.
When multi tenant is not configured this will return undef.

=cut

sub customer_hostnames {
    my $self = shift;

    return if !$self->is_multi_tenant;

    my @hosts = keys %{ $self->customer_instance->{VirtualHosts} };
    push(@hosts, $self->customer_instance->{instance_hostname});
    @hosts = sort { $a cmp $b } (uniq @hosts);
    return \@hosts;
}

sub mail_hosts {
    my $self = shift;

    my @hosts = keys %{ $self->customer_instance->{MailHosts} };
    return unless @hosts;

    push(@hosts, $self->customer_instance->{instance_hostname});
    @hosts = sort { $a cmp $b } (uniq @hosts);
    return \@hosts;
}

sub is_allowed_mailhost {
    my ($self, $host) = @_;
    my $hosts = $self->mail_hosts;
    return 1 unless $hosts;
    # DBIx::Class also has first, let's be safe
    return List::Util::first { $host eq $_ } @{$hosts};
}

sig encode_jsonb => 'HashRef';

sub encode_jsonb {
    my ($self, $data) = @_;

    return $self->storage->dbh_do(sub {
        my ($storage, $dbh) = @_;
        return $dbh->quote(encode_json($data)) . "::jsonb";
    });
}

=head2 clear

Clear the DB object

=cut

sub clear {
    my $self = shift;
    for my $attr ($self->meta->get_all_attributes) {
        $attr->clear_value($self) if $attr->has_value($self);
    }
    $self->default_resultset_attributes({});
}

=head2 disconnect

Disconnect from the DB server

=cut

sub disconnect {
    my $self = shift;

    $self->storage->disconnect;
}

=head1 PRIVATE METHODS

=head2 _ensure_all_db_classes_are_loaded

Ensures that every resultset class related to the schema files are loaded

=cut

sub _ensure_all_db_classes_are_loaded {
    for my $module (Module::Find::findallmod(__PACKAGE__)) {

        ## Do not bother with the default resultset...it is already loaded
        next if $module->resultset_class eq 'DBIx::Class::ResultSet';

        __PACKAGE__->ensure_class_loaded($module->resultset_class);
    }
}

=head2 _build_active_modules

Returns a list of module names for which an interface has been configured and
activated.

=cut

sub _build_active_modules {
    my $self = shift;

    my $rs = $self->resultset('Interface')->search_active_restricted;

    return [ uniq $rs->get_column('module')->all ];
}

=head2 _setup_schema

Setup the schema for a specific InstanceConfig

=cut

sub _setup_schema {
    my $self = shift;
    my $config = shift;

    my $cache    = Zaaksysteem::Cache->new(storage => { });
    my $logger   = Log::Log4perl->get_logger(ref $self);

    my $betrokkene_model = Zaaksysteem::Betrokkene->new(
        dbic     => $self,
        stash    => {},
        config   => $self->catalyst_config,
        customer => $config
    );

    $self->customer_config($config);
    $self->cache($cache);

    $self->bag_model_builder(sub {
        my $spoof_mode         = $self->resultset('Config')->get("bag_spoof_mode");
        my $priority_gemeentes = $self->resultset('Config')->get("bag_priority_gemeentes") || [];
        my $local_only         = $self->resultset('Config')->get("bag_local_only");

        $priority_gemeentes = [
            grep { defined($_) && length($_) } @$priority_gemeentes
        ];

        my $es = Search::Elasticsearch->new(
            %{ $config->{"Search::Elasticsearch"}{BAG} }
        );

        my $model = Zaaksysteem::Geo::BAG::Model->new_from_config(
            bag_spoof_mode         => $spoof_mode,
            bag_priority_gemeentes => $priority_gemeentes,
            bag_local_only         => $local_only,
            home                   => $config->{home},
            es_connection          => $es,
        );

        return $model;
    });

    my %attributes = (
        cache  => $cache,
        log    => $logger,
        config => $self->catalyst_config,
        betrokkene_model => $betrokkene_model
    );

    for my $key (keys %attributes) {
        $self->default_resultset_attributes->{ $key } = $attributes{ $key };
    }

    return $self;
}

# Install our own exception action, so when DBIx throws one it's an
# Zaaksysteem::Exceptio::Base instance
__PACKAGE__->exception_action(sub {
    my $err = shift;

    if(blessed $err) {
        $err->throw if $err->can('throw');
        throw('db', $err->as_string, $err) if $err->can('as_string');
        throw('db', '' . $err, $err);
    }
    throw('db', $err);
});


## Run on catalyst setup time
_ensure_all_db_classes_are_loaded();

sub DEMOLISH {
    my $self = shift;
    $self->clear;
    $self->disconnect;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
