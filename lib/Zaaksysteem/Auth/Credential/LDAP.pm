package Zaaksysteem::Auth::Credential::LDAP;

use Moose;

use BTTW::Tools;
use Zaaksysteem::Constants;

use Authen::Passphrase;


has _config => ( is => 'ro' );
has app => ( is => 'ro' );
has realm => ( is => 'ro' );
has interface => ( is => 'rw', isa => 'Zaaksysteem::Model::DB::Interface' );

around BUILDARGS => sub {
    my $orig    = shift;
    my $class   = shift;
    my $config  = shift;


    $config->{'password_field'}     ||= 'password';


    $class->$orig(
        _config => $config,
        app => shift,
        realm => shift
    );
};


sub authenticate {
    my ($self, $c, $realm, $authinfo) = @_;

    my $user = $realm->find_user($c, {
        username => $authinfo->{ username },
        source => $self->interface->id
    });

    if (ref($user)) {
        if ($self->check_password($user, $authinfo)) {
            return $user;
        }
    } else {
        $c->log->debug(
            'Unable to locate user matching user info provided in realm: '
            . $realm->name
            ) if $c->debug;
        return;
    }
}

=head2 check_password

Arguments: $USER, $AUTHINFO

Return value: $TRUE_OR_FALSE

    print $self->check_password($user, { password => 'jaddaj@dda' })

    # Prints '1' on success

Validates the password according to RFC2307. In other words, it will look at the
first few characters in the database, to find out the hashing algorythm:

    {SSHA}DjSkeWeacd8a9dc8a9ASDf8dadscfasdaa9dSddsdf8D

    or

    {SHA}DjSkeWeacd8a9dc8a9ASDf8dadscfasdaa9dSddsdf8D

    etc.

=cut

sub check_password {
    my ( $self, $user, $authinfo ) = @_;

    return unless ($authinfo->{$self->_config->{'password_field'}} && $user->login_entity->password);

    my $ppr = Authen::Passphrase->from_rfc2307($user->login_entity->password);

    return $ppr->match($authinfo->{$self->_config->{'password_field'}});
}

1;




__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 authenticate

TODO: Fix the POD

=cut

=head2 config

TODO: Fix the POD

=cut

=head2 search_user_dn

TODO: Fix the POD

=cut

