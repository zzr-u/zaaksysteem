package Zaaksysteem::UserSettings::Favourites;

use Moose;
use BTTW::Tools;

use Zaaksysteem::Types qw/UUID/;
use Moose::Util::TypeConstraints;

with qw/
    Zaaksysteem::UserSettings::Roles::CRUDArray
    Zaaksysteem::DynClass

    Zaaksysteem::UserSettings::Favourites::Casetype
/;

=head1 NAME

Zaaksysteem::UserSettings::FavouriteSearches - Favourite searches implementation in user settings

=head1 SYNOPSIS

    my $favourites = $usersettings->favourites;

    $favourites->search();
    $favourites->create(data => {});
    $favourites->update(id => uuid, data => {});
    $favourites->delete(id => uuid);

=head1 DESCRIPTION

Before we know it, we got a lot of settings in anonymous JSON hashes in our L<Zaaksysteem::DB::Subject> table.

This namespace makes sure favourites get a nice API

=head1 ATTRIBUTES

=head2 schema

DBIx::Class schema

=cut

has 'schema'    => (
    is          => 'rw',
    isa         => 'Zaaksysteem::Schema',
    required    => 1,
);

=head2 _allowed_reference_types

A list of reference types, which can be used in favourites. Like casetype, or subject, or or or.

=cut

has '_allowed_reference_types'  => (
    is          => 'rw',
    isa         => 'ArrayRef',
    default     => sub { return []; }
);


=head2 _store

The place where we store our settings internally

=cut

has '_store'     => (
    is          => 'rw',
    default     => sub { [] },
    isa         => 'ArrayRef',
);

=head2 _row_class

The class to inflate array objects to

=cut

has '_crud_settings' => (
    is          => 'ro',
    default     => sub { 
        {
            primary_key     => 'id',
            private_columns => [qw/id/],
            row_class       => 'Zaaksysteem::UserSettings::Favourites::Row',
            order_column    => 'order',
            order_dependency => 'reference_type'
        }
    },
);

=head2 _dynamic_classes

    [
        {
            class       => 'My::Package::Row',
            attributes  => {
                id              => {
                    is          => 'ro',
                    isa         => UUID,
                    writer      => '_set_id',
                },
                label           => {
                    is          => 'ro',
                    writer      => '_set_label',
                },
            },
            methods     => {
                deflate_to_settings     => sub {
                    my $self        = shift;

                    return {
                        id                  => $self->id,
                        order               => $self->order,
                        reference_id        => $self->reference_id,
                        reference_type      => $self->reference_type,
                    };
                },
            }
        }
    ]

Attribute containing a list of classes which you would like to generate dynamically.

=cut

has '+_dynamic_classes'    => (
    lazy => 1,
    default     => sub {
        my $self        = shift;

        my $reftypes    = join('|', @{ $self->_allowed_reference_types });

        return [
            {
                class       => __PACKAGE__ . '::Row',
                attributes  => {
                    order           => {
                        is          => 'ro',
                        writer      => '_set_order',
                        isa         => 'Int',
                    },
                    reference_id    => {
                        is          => 'ro',
                        writer      => '_set_reference_id',
                        required    => 1,
                    },
                    id              => {
                        is          => 'ro',
                        isa         => UUID,
                        writer      => '_set_id',
                    },
                    reference_type  => {
                        is          => 'ro',
                        isa         => subtype('Str' => where { $_ =~ m/^(?:case|$reftypes)$/ }),
                        writer      => '_set_reference_type',
                        required    => 1,
                    },
                    label           => {
                        is          => 'ro',
                        writer      => '_set_label',
                    },
                },
                methods     => {
                    deflate_to_settings     => sub {
                        my $self        = shift;

                        return {
                            id                  => $self->id,
                            order               => $self->order,
                            reference_id        => $self->reference_id,
                            reference_type      => $self->reference_type,
                        };
                    },
                },
                moose       => 1,
            }
        ]
    }
);

=head2 BUILD

Call _create_classes, to build the dynamic classes

=cut

sub BUILD {
    my $self        = shift;

    $self->_load_allowed_reference_types;
    $self->_create_dynamic_classes;
}

sub _load_allowed_reference_types {}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
