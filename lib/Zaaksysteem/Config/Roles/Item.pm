package Zaaksysteem::Config::Roles::Item;
use Moose::Role;

use BTTW::Tools;
use JSON ();
use URI;

use Zaaksysteem::API::v1::ArraySet;
use Zaaksysteem::Types qw(UUID);

=head1 NAME

Zaaksysteem::Config::Roles::Item - A C<config/item> role

=head1 DESCRIPTION

This role implements logic that the model can/should use when it works with
config-items and -defintions.

=head1 SYNOPSIS

    package Foo;
    use Moose;
    use namespace::autoclean;

    # You must implement
    # bibliotheek_notificaties_rs

    with 'Zaaksysteem::Config::Roles::Item';

=head1 REQUIRES

Modules implementing this role must provide the following methods:

=over

=item * bibliotheek_notificaties_rs

=back

=cut

requires qw(
    bibliotheek_notificaties_rs
    groups_rs
    log
    roles_rs
    subject_model
);

=head1 ATTRIBUTES

=head2 json

Our default JSON de- and encoder.

=cut

has json => (
    is => 'ro',
    isa => 'JSON',
    default => sub {
        return JSON->new->utf8(0);
    }
);

=head1 METHODS

=head2 get_value_type

Get the value type from the definition

=cut

sub get_value_type {
    my ($self, $definition) = @_;
    my $value_type = $definition->has_value_type_name
           ? $definition->value_type_name
           : $definition->value_type->{parent_type_name};

    if (!defined $value_type) {
        $self->log->warn(
            sprintf(
                "Empty value_type for '%s' with id '%s' ",
                $definition->config_item_name,
                $definition->id
            )
        );
        $value_type = '' ;
    }
    return $value_type;
}

=head2 get_type_options

Get the value type options from the definition

=cut

sub get_type_options {
    my ($self, $definition) = @_;

    return unless $definition->has_value_type;
    return $definition->value_type->{options} // {};
}

=head2 inflate

Inflate the values from the database

=cut

sub inflate {
    my ($self, $definition, $value) = @_;

    if ($definition->mvp) {
        try {
            $value = ($self->json->decode($value));
        }
        catch {
            $self->log->info("Unable to decode JSON: $value");
        };
    }

    $value = $self->_flat_to_values($definition, $value);
    my $value_type = $self->get_value_type($definition);

    if ($definition->mvp && $value_type eq 'object_ref') {
        $value = Zaaksysteem::API::v1::ArraySet->new(
            is_paged => 0,
            content  => $value,
        );
    }
    return $value;
}

sub _flat_to_values {
    my ($self, $definition, $value) = @_;

    my $value_type   = $self->get_value_type($definition);
    my $type_options = $self->get_type_options($definition);

    if ($value_type eq 'uri') {
        return URI->new($value)->as_string;
    }
    elsif ($value_type eq 'boolean') {
        return $value ? \1 : \0;
    }
    elsif ($value_type eq 'object_ref') {

        my $object_type = $type_options->{object_type_name};

        if ($object_type eq 'email_template') {
            return $self->_inflate_email_template($value, $type_options);
        }
        elsif ($object_type eq 'subject') {
            return $self->_get_subject_from_value($value, $type_options);
        }
        elsif ($object_type eq 'group') {
            return $self->_get_group_from_value($value, $type_options);
        }
        elsif ($object_type eq 'role') {
            return $self->_get_role_from_value($value, $type_options);
        }
        elsif ($object_type eq 'municipality_code') {
            return $self->_get_municipality_from_value($value, $type_options);
        }

        throw("config/role/items/inflate/object",
            "Unable to inflate object with type '$object_type'");
    }
    elsif ($value_type eq 'number') {
        $value = $value + 0;

        if ($type_options && exists $type_options->{type}) {
            if ($type_options->{type} ne 'real') {
                return int($value);
            }
        }
        return $value;
    }
    return $value;
}

=head2 deflate

Deflate the values from an item

=cut

sub deflate {
    my ($self, $definition, $item) = @_;

    my $value = $item->value;

    $value = $self->_flat_to_values($definition, $value);

    my $value_type   = $self->get_value_type($definition);
    my $type_options = $self->get_type_options($definition);

    if (defined $value && $value_type eq 'object_ref') {

        if ($definition->mvp) {
            $value = [
                map $self->_object_to_value($_, $type_options), @$value
            ];
        }
        else {
            $value = $self->_object_to_value($value, $type_options);
        }
    }

    if ($definition->mvp) {
        try {
            $value = $self->json->encode($value);
        }
        catch {
            $self->log->info(
                sprintf("Unable to encode '%s': %s", dump_terse($value), $_));
            die $_;
        };
    }

    return $value;
}

sub _object_to_value {
    my ($self, $value, $type_options) = @_;

    my $mapping = $type_options->{ id_map_field_name };

    return $value->id unless defined $mapping;

    if (my $attribute = $value->meta->find_attribute_by_name($mapping)) {
        return $attribute->get_value($value);
    }

    if (my $attribute = $value->can($mapping)) {
        return $attribute->($value);
    }

    throw('config/item/value/object_attribute_not_found', sprintf(
        'Could not find attribute "%s" on value object',
        $mapping
    ));
}

sub _get_municipality_from_value {
    my ($self, $value, $options) = @_;

    # Don't even try to inflate undefined values
    return unless $value;

    if ($options->{ constraints }) {
        throw(
            'config/item/value/constraints_not_supported_for_municipalities',
            sprintf(
                'Constraints defined for municipality value type are unsupported (got "%s")',
                dump_terse($options)
            )
        );
    }

    my @result;
    foreach (@$value) {
        if ($_ =~ /^[0-9]+$/) {
            push(
                @result,
                Zaaksysteem::Object::Types::MunicipalityCode
                    ->new_from_dutch_code(
                    $_)
            );
        }
        elsif (UUID->check($_)) {
            push(
                @result,
                Zaaksysteem::Object::Types::MunicipalityCode->new_from_uuid(
                    $_)
            );
        }
        else {
            push(
                @result,
                Zaaksysteem::Object::Types::MunicipalityCode->new_from_name(
                    $_)
            );
        }
    }
    return \@result;

}

sub _get_group_from_value {
    my ($self, $value, $options) = @_;

    # Don't even try to inflate undefined values
    return unless $value;

    my $rs = $self->groups_rs->search($options->{ constraints });

    if ($value =~ /^[0-9]+$/) {
        return $rs->find($value)->as_object;
    }
    elsif (UUID->check($value)) {
        return $rs->find({uuid => $value})->as_object;
    }
    else {
        return $rs->find({name => $value})->as_object;
    }
}

sub _get_role_from_value {
    my ($self, $value, $options) = @_;

    # Don't even try to inflate undefined values
    return unless $value;

    my $rs = $self->roles_rs->search_rs($options->{ constraints });

    if ($value =~ /^[0-9]+$/) {
        return $rs->find($value)->as_object;
    }
    elsif (UUID->check($value)) {
        return $rs->find({uuid => $value})->as_object;
    }
    else {
        return $rs->find({"LOWER(me.name)" => lc($value)})->as_object;
    }
}

sub _get_subject_from_value {
    my ($self, $value, $options) = @_;

    if ($options->{ constraints }) {
        throw(
            'config/item/value/constraints_not_supported_for_subjects',
            sprintf(
                'Constraints defined for subject value type are unsupported (got "%s")',
                dump_terse($options)
            )
        );
    }

    # Don't even try to inflate undefined values
    return unless $value;
    if ($value =~ /^[0-9]+$/) {
        return $self->subject_model->find($value);
    }
    elsif (UUID->check($value)) {
        return $self->subject_model->find({uuid => $value});
    }
    else {
        throw("subject/syntax/error", "Subject ID syntax is invalid!");
    }
}

sub _inflate_email_template {
    my ($self, $value, $options) = @_;

    # Don't even try to inflate undefined values
    return unless $value;

    my $rs = $self->bibliotheek_notificaties_rs->search_rs(
        $options->{ constraints } # undef constraint is safe as search param
    );

    if ($value =~ /^[0-9]+$/) {
        return $rs->find($value);
    }
    elsif (UUID->check($value)) {
        return $rs->find({uuid => $value});
    }
    else {
        throw("email_template_id/syntax/error", "Email ID syntax is invalid!");
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
