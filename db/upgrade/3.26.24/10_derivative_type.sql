BEGIN;

    ALTER TABLE file_derivative DROP CONSTRAINT IF EXISTS file_thumbnail_type_check;
    ALTER TABLE file_derivative ADD CONSTRAINT file_derivative_type CHECK("type" IN ('pdf', 'thumbnail', 'doc', 'docx'));

COMMIT;
