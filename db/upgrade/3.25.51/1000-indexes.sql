BEGIN;

CREATE INDEX queue_finished_status_idx ON queue(date_finished, status);

COMMIT;
