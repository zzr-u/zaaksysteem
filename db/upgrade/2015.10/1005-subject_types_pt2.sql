BEGIN;
    ALTER TABLE subject DROP CONSTRAINT subject_subject_type_check;

    UPDATE subject SET subject_type = 'person' WHERE subject_type = 'natuurlijk_persoon';
    UPDATE subject SET subject_type = 'company' WHERE subject_type = 'bedrijf';

    ALTER TABLE subject ADD  CONSTRAINT subject_subject_type_check
        CHECK (subject_type IN ('person', 'company', 'employee'));
COMMIT;
