BEGIN;

    ALTER TABLE file_thumbnail ADD COLUMN type TEXT CHECK (type IN ('thumbnail', 'pdf', 'doc'));
    UPDATE file_thumbnail SET type = 'thumbnail';
    ALTER TABLE file_thumbnail ALTER COLUMN type SET NOT NULL;
    ALTER TABLE file_thumbnail RENAME TO file_derivative;

COMMIT;
