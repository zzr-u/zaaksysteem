BEGIN;

ALTER TABLE object_relationships ADD type1 TEXT NOT NULL;
ALTER TABLE object_relationships ADD type2 TEXT NOT NULL;

ALTER TABLE object_relationships
    ADD FOREIGN KEY (object1_uuid) REFERENCES object_data(uuid);
ALTER TABLE object_relationships
    ADD FOREIGN KEY (object2_uuid) REFERENCES object_data(uuid);

COMMIT;
