--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.15
-- Dumped by pg_dump version 9.6.15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: confidentiality; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.confidentiality AS ENUM (
    'public',
    'internal',
    'confidential'
);


--
-- Name: contactmoment_medium; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.contactmoment_medium AS ENUM (
    'behandelaar',
    'balie',
    'telefoon',
    'post',
    'email',
    'webformulier'
);


--
-- Name: contactmoment_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.contactmoment_type AS ENUM (
    'email',
    'note'
);


--
-- Name: documentstatus; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.documentstatus AS ENUM (
    'original',
    'copy',
    'replaced',
    'converted'
);


--
-- Name: zaaksysteem_bag_types; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.zaaksysteem_bag_types AS ENUM (
    'nummeraanduiding',
    'verblijfsobject',
    'pand',
    'openbareruimte'
);


--
-- Name: zaaksysteem_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.zaaksysteem_status AS ENUM (
    'new',
    'open',
    'resolved',
    'stalled',
    'deleted',
    'overdragen'
);


--
-- Name: zaaksysteem_trigger; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.zaaksysteem_trigger AS ENUM (
    'extern',
    'intern'
);


--
-- Name: hstore_to_timestamp(character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.hstore_to_timestamp(date_field character varying) RETURNS timestamp without time zone
    LANGUAGE sql IMMUTABLE
    AS $_$







  SELECT to_timestamp($1, 'YYYY-MM-DD"T"HH24:MI:SS')::TIMESTAMP;







$_$;


--
-- Name: insert_file_timestamps(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.insert_file_timestamps() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
            NEW.date_created = NOW() AT TIME ZONE 'UTC';
            NEW.date_modified = NOW() AT TIME ZONE 'UTC';
            RETURN NEW;
        END;
    $$;


--
-- Name: insert_timestamps(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.insert_timestamps() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
            NEW.created = NOW();
            NEW.last_modified = NOW();
            RETURN NEW;
        END;
    $$;


--
-- Name: update_file_timestamps(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_file_timestamps() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
            NEW.date_modified = NOW() AT TIME ZONE 'UTC';
            RETURN NEW;
        END;
    $$;


--
-- Name: update_timestamps(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_timestamps() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
            NEW.last_modified = NOW();
            RETURN NEW;
        END;
    $$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: adres; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.adres (
    id integer NOT NULL,
    straatnaam text,
    huisnummer bigint,
    huisletter character(1),
    huisnummertoevoeging text,
    nadere_aanduiding character varying(35),
    postcode character varying(6),
    woonplaats text,
    gemeentedeel text,
    functie_adres character(1) NOT NULL,
    datum_aanvang_bewoning date,
    woonplaats_id character varying(32),
    gemeente_code smallint,
    hash character varying(32),
    import_datum timestamp(6) without time zone,
    deleted_on timestamp(6) without time zone,
    adres_buitenland1 text,
    adres_buitenland2 text,
    adres_buitenland3 text,
    landcode integer DEFAULT 6030,
    natuurlijk_persoon_id integer
);


--
-- Name: adres_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.adres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.adres_id_seq OWNED BY public.adres.id;


--
-- Name: alternative_authentication_activation_link; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.alternative_authentication_activation_link (
    token text NOT NULL,
    subject_id uuid NOT NULL,
    expires timestamp without time zone NOT NULL
);


--
-- Name: bag_ligplaats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_ligplaats (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    status character varying(80) NOT NULL,
    hoofdadres character varying(16) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_ligplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_ligplaats IS '55 : een ligplaats is een formeel door de gemeenteraad als zodanig aangewezen plaats in het water, al dan niet aangevuld met een op de oever aanwezig terrein of een gedeelte daarvan, dat bestemd is voor het permanent afmeren van een voor woon-, bedrijfsmatige- of recreatieve doeleinden geschikt vaartuig.';


--
-- Name: COLUMN bag_ligplaats.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.identificatie IS '58.01 : de unieke aanduiding van een ligplaats.';


--
-- Name: COLUMN bag_ligplaats.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.begindatum IS '58.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een ligplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_ligplaats.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.einddatum IS '58.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een ligplaats.';


--
-- Name: COLUMN bag_ligplaats.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.officieel IS '58.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_ligplaats.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.status IS '58.03 : de fase van de levenscyclus van een ligplaats, waarin de betreffende ligplaats zich bevindt.';


--
-- Name: COLUMN bag_ligplaats.hoofdadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.hoofdadres IS '58:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een ligplaats, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_ligplaats.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.inonderzoek IS '58.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_ligplaats.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.documentdatum IS '58.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een in de historie plaatsen van gegevens ten aanzien van een ligplaats heeft plaatsgevonden.';


--
-- Name: COLUMN bag_ligplaats.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.documentnummer IS '58.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een ligplaats heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_ligplaats.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_ligplaats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_ligplaats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_ligplaats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_ligplaats_id_seq OWNED BY public.bag_ligplaats.id;


--
-- Name: bag_ligplaats_nevenadres; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_ligplaats_nevenadres (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    nevenadres character varying(16) NOT NULL,
    correctie character varying(1) NOT NULL
);


--
-- Name: TABLE bag_ligplaats_nevenadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_ligplaats_nevenadres IS 'koppeltabel voor nevenadressen bij ligplaats';


--
-- Name: COLUMN bag_ligplaats_nevenadres.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats_nevenadres.identificatie IS '58.01 : de unieke aanduiding van een ligplaats.';


--
-- Name: COLUMN bag_ligplaats_nevenadres.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats_nevenadres.begindatum IS '58.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een ligplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_ligplaats_nevenadres.nevenadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats_nevenadres.nevenadres IS '58.11 : de identificatiecodes nummeraanduiding waaronder nevenadressen van een ligplaats, die in het kader van de basis gebouwen registratie als zodanig zijn aangemerkt, zijn opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_ligplaats_nevenadres.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_ligplaats_nevenadres.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_nummeraanduiding; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_nummeraanduiding (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    huisnummer bigint NOT NULL,
    officieel character varying(1),
    huisletter character varying(1),
    huisnummertoevoeging text,
    postcode character varying(6),
    woonplaats character varying(4),
    inonderzoek character varying(1) NOT NULL,
    openbareruimte character varying(16) NOT NULL,
    type character varying(20) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    status character varying(80) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL,
    gps_lat_lon point
);


--
-- Name: TABLE bag_nummeraanduiding; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_nummeraanduiding IS '11.2 : een nummeraanduiding is een door de gemeenteraad als zodanig toegekende aanduiding van een adresseerbaar object.';


--
-- Name: COLUMN bag_nummeraanduiding.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.identificatie IS '11.02 : de unieke aanduiding van een nummeraanduiding.';


--
-- Name: COLUMN bag_nummeraanduiding.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.begindatum IS '11.62 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een nummeraanduiding een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_nummeraanduiding.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.einddatum IS '11.63 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een nummeraanduiding.';


--
-- Name: COLUMN bag_nummeraanduiding.huisnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.huisnummer IS '11.20 : een door of namens het gemeentebestuur ten aanzien van een adresseerbaar object toegekende nummering.';


--
-- Name: COLUMN bag_nummeraanduiding.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.officieel IS '11.21 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_nummeraanduiding.huisletter; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.huisletter IS '11.30 : een door of namens het gemeentebestuur ten aanzien van  een adresseerbaar object toegekende toevoeging aan een huisnummer in de vorm van een alfanumeriek teken.';


--
-- Name: COLUMN bag_nummeraanduiding.huisnummertoevoeging; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.huisnummertoevoeging IS '11.40 : een door of namens het gemeentebestuur ten aanzien van  een adresseerbaar object toegekende nadere toevoeging aan een huisnummer of een combinatie van huisnummer en huisletter.';


--
-- Name: COLUMN bag_nummeraanduiding.postcode; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.postcode IS '11.60 : de door tnt post vastgestelde code behorende bij een bepaalde combinatie van een straatnaam en een huisnummer.';


--
-- Name: COLUMN bag_nummeraanduiding.woonplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.woonplaats IS '11.61 : unieke aanduiding van de woonplaats waarbinnen het object waaraan de nummeraanduiding is toegekend is gelegen.';


--
-- Name: COLUMN bag_nummeraanduiding.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.inonderzoek IS '11.64 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_nummeraanduiding.openbareruimte; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.openbareruimte IS '11.65 : de unieke aanduiding van een openbare ruimte waaraan een adresseerbaar object is gelegen.';


--
-- Name: COLUMN bag_nummeraanduiding.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.type IS '11.66 : de aard van een als zodanig benoemde nummeraanduiding.';


--
-- Name: COLUMN bag_nummeraanduiding.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.documentdatum IS '11.67 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een nummeraanduiding heeft plaatsgevonden.';


--
-- Name: COLUMN bag_nummeraanduiding.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.documentnummer IS '11.68 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een nummeraanduiding heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_nummeraanduiding.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.status IS '11.69 : de fase van de levenscyclus van een nummeraanduiding, waarin de betreffende nummeraanduiding zich bevindt.';


--
-- Name: COLUMN bag_nummeraanduiding.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_nummeraanduiding.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_nummeraanduiding_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_nummeraanduiding_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_nummeraanduiding_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_nummeraanduiding_id_seq OWNED BY public.bag_nummeraanduiding.id;


--
-- Name: bag_openbareruimte; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_openbareruimte (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    naam character varying(80) NOT NULL,
    officieel character varying(1),
    woonplaats character varying(4),
    type character varying(40) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    status character varying(80) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL,
    gps_lat_lon point
);


--
-- Name: TABLE bag_openbareruimte; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_openbareruimte IS '11.1 : een openbare ruimte is een door de gemeenteraad als zodanig aangewezen benaming van een binnen een woonplaats gelegen buitenruimte.';


--
-- Name: COLUMN bag_openbareruimte.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.identificatie IS '11.01 : de unieke aanduiding van een openbare ruimte.';


--
-- Name: COLUMN bag_openbareruimte.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.begindatum IS '11.12 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een openbare ruimte een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_openbareruimte.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.einddatum IS '11.13 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een openbare ruimte.';


--
-- Name: COLUMN bag_openbareruimte.naam; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.naam IS '11.10 : een naam die aan een openbare ruimte is toegekend in een daartoe strekkend formeel gemeentelijk besluit.';


--
-- Name: COLUMN bag_openbareruimte.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.officieel IS '11.11 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_openbareruimte.woonplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.woonplaats IS '11.15 : unieke aanduiding van de woonplaats waarbinnen een openbare ruimte is gelegen.';


--
-- Name: COLUMN bag_openbareruimte.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.type IS '11.16 : de aard van de als zodanig benoemde openbare ruimte.';


--
-- Name: COLUMN bag_openbareruimte.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.inonderzoek IS '11.14 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_openbareruimte.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.documentdatum IS '11.17 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een openbare ruimte heeft plaatsgevonden.';


--
-- Name: COLUMN bag_openbareruimte.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.documentnummer IS '11.18 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een openbare ruimte heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_openbareruimte.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.status IS '11.19 : de fase van de levenscyclus van een openbare ruimte, waarin de betreffende openbare ruimte zich bevindt.';


--
-- Name: COLUMN bag_openbareruimte.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_openbareruimte.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_openbareruimte_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_openbareruimte_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_openbareruimte_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_openbareruimte_id_seq OWNED BY public.bag_openbareruimte.id;


--
-- Name: bag_pand; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_pand (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    bouwjaar integer,
    status character varying(80) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_pand; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_pand IS '55 : een pand is de kleinste, bij de totstandkoming functioneel en bouwkundig constructief zelfstandige eenheid, die direct en duurzaam met de aarde is verbonden.';


--
-- Name: COLUMN bag_pand.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.identificatie IS '55.01 : de unieke aanduiding van een pand';


--
-- Name: COLUMN bag_pand.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.begindatum IS '55.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een pand een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_pand.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.einddatum IS '55.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een pand.';


--
-- Name: COLUMN bag_pand.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.officieel IS '55.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname';


--
-- Name: COLUMN bag_pand.bouwjaar; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.bouwjaar IS '55.30 : de aanduiding van het jaar waarin een pand oorspronkelijk als bouwkundig gereed is opgeleverd.';


--
-- Name: COLUMN bag_pand.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.status IS '55.31 : de fase van de levenscyclus van een pand, waarin het betreffende pand zich bevindt.';


--
-- Name: COLUMN bag_pand.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.inonderzoek IS '55.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_pand.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.documentdatum IS '55.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een pand heeft plaatsgevonden.';


--
-- Name: COLUMN bag_pand.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.documentnummer IS '55.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een pand heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_pand.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_pand.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_pand_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_pand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_pand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_pand_id_seq OWNED BY public.bag_pand.id;


--
-- Name: bag_standplaats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_standplaats (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    status character varying(80) NOT NULL,
    hoofdadres character varying(16) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_standplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_standplaats IS '57 : een standplaats is een formeel door de gemeenteraad als zodanig aangewezen terrein of een gedeelte daarvan, dat bestemd is voor het permanent plaatsen van een niet direct en duurzaam met de aarde verbonden en voor woon -, bedrijfsmatige - of recreatieve doeleinden geschikte ruimte.';


--
-- Name: COLUMN bag_standplaats.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.identificatie IS '57.01 : de unieke aanduiding van een standplaats.';


--
-- Name: COLUMN bag_standplaats.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.begindatum IS '57.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een standplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_standplaats.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.einddatum IS '57.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een standplaats.';


--
-- Name: COLUMN bag_standplaats.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.officieel IS '57.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_standplaats.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.status IS '57.03 : de fase van de levenscyclus van een standplaats, waarin de betreffende standplaats zich bevindt.';


--
-- Name: COLUMN bag_standplaats.hoofdadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.hoofdadres IS '57:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een standplaats, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_standplaats.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.inonderzoek IS '57.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_standplaats.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.documentdatum IS '57.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een in de historie plaatsen van gegevens ten aanzien van een standplaats heeft plaatsgevonden.';


--
-- Name: COLUMN bag_standplaats.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.documentnummer IS '57.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een standplaats heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_standplaats.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_standplaats.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_standplaats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_standplaats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_standplaats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_standplaats_id_seq OWNED BY public.bag_standplaats.id;


--
-- Name: bag_verblijfsobject; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_verblijfsobject (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    hoofdadres character varying(16) NOT NULL,
    oppervlakte integer,
    status character varying(80) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_verblijfsobject; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_verblijfsobject IS '56 : een verblijfsobject is de kleinste binnen een of meerdere panden gelegen en voor woon -, bedrijfsmatige - of recreatieve doeleinden geschikte eenheid van gebruik, die ontsloten wordt via een eigen toegang vanaf de openbare weg, een erf of een gedeelde verkeersruimte en die onderwerp kan zijn van rechtshandelingen.';


--
-- Name: COLUMN bag_verblijfsobject.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.identificatie IS '56.01 : de unieke aanduiding van een verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.begindatum IS '56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_verblijfsobject.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.einddatum IS '56.92 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een verblijfsobject.';


--
-- Name: COLUMN bag_verblijfsobject.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.officieel IS '56.02 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname';


--
-- Name: COLUMN bag_verblijfsobject.hoofdadres; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.hoofdadres IS '56:10 : de identificatiecode nummeraanduiding waaronder het hoofdadres van een verblijfsobject, dat in het kader van de basis gebouwen registratie als zodanig is aangemerkt, is opgenomen in de basis registratie adressen.';


--
-- Name: COLUMN bag_verblijfsobject.oppervlakte; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.oppervlakte IS '56.31 : de gebruiksoppervlakte van een verblijfsobject in gehele vierkante meters.';


--
-- Name: COLUMN bag_verblijfsobject.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.status IS '56.32 : de fase van de levenscyclus van een verblijfsobject, waarin het betreffende verblijfsobject zich bevindt.';


--
-- Name: COLUMN bag_verblijfsobject.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.inonderzoek IS '56.93 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_verblijfsobject.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.documentdatum IS '56.97 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een verblijfsobject heeft plaatsgevonden.';


--
-- Name: COLUMN bag_verblijfsobject.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.documentnummer IS '56.98 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een verblijfsobject heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_verblijfsobject.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_verblijfsobject_gebruiksdoel; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_verblijfsobject_gebruiksdoel (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14),
    gebruiksdoel character varying(80) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_verblijfsobject_gebruiksdoel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_verblijfsobject_gebruiksdoel IS 'koppeltabel voor gebruiksdoelen bij verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_gebruiksdoel.identificatie IS '56.01 : de unieke aanduiding van een verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_gebruiksdoel.begindatum IS '56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.gebruiksdoel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_gebruiksdoel.gebruiksdoel IS '56.30 : een categorisering van de gebruiksdoelen van het betreffende verblijfsobject, zoals dit  formeel door de overheid als zodanig is toegestaan.';


--
-- Name: COLUMN bag_verblijfsobject_gebruiksdoel.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_gebruiksdoel.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_verblijfsobject_gebruiksdoel_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_verblijfsobject_gebruiksdoel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_verblijfsobject_gebruiksdoel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_verblijfsobject_gebruiksdoel_id_seq OWNED BY public.bag_verblijfsobject_gebruiksdoel.id;


--
-- Name: bag_verblijfsobject_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_verblijfsobject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_verblijfsobject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_verblijfsobject_id_seq OWNED BY public.bag_verblijfsobject.id;


--
-- Name: bag_verblijfsobject_pand; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_verblijfsobject_pand (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14),
    pand character varying(16) NOT NULL,
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_verblijfsobject_pand; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_verblijfsobject_pand IS 'koppeltabel voor panden bij verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_pand.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_pand.identificatie IS '56.01 : de unieke aanduiding van een verblijfsobject';


--
-- Name: COLUMN bag_verblijfsobject_pand.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_pand.begindatum IS '56.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een verblijfsobject een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_verblijfsobject_pand.pand; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_pand.pand IS '56.90 : de unieke aanduidingen van de panden waarvan het verblijfsobject onderdeel uitmaakt.';


--
-- Name: COLUMN bag_verblijfsobject_pand.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_verblijfsobject_pand.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_verblijfsobject_pand_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_verblijfsobject_pand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_verblijfsobject_pand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_verblijfsobject_pand_id_seq OWNED BY public.bag_verblijfsobject_pand.id;


--
-- Name: bag_woonplaats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bag_woonplaats (
    identificatie character varying(16) NOT NULL,
    begindatum character varying(14) NOT NULL,
    einddatum character varying(14),
    officieel character varying(1),
    naam character varying(80) NOT NULL,
    status character varying(80) NOT NULL,
    inonderzoek character varying(1) NOT NULL,
    documentdatum character varying(14),
    documentnummer character varying(20),
    correctie character varying(1),
    id integer NOT NULL
);


--
-- Name: TABLE bag_woonplaats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bag_woonplaats IS '11.7 : een woonplaats is een door de gemeenteraad als zodanig aangewezen gedeelte van het gemeentelijk grondgebied.';


--
-- Name: COLUMN bag_woonplaats.identificatie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.identificatie IS '11.03 : de landelijk unieke aanduiding van een woonplaats, zoals vastgesteld door de beheerder van de landelijke tabel voor woonplaatsen.';


--
-- Name: COLUMN bag_woonplaats.begindatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.begindatum IS '11.73 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een woonplaats een wijziging hebben ondergaan.';


--
-- Name: COLUMN bag_woonplaats.einddatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.einddatum IS '11.74 : de einddatum van een periode waarin er geen wijzigingen hebben plaatsgevonden in de gegevens die worden bijgehouden over een woonplaats.';


--
-- Name: COLUMN bag_woonplaats.officieel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.officieel IS '11.72 : een aanduiding waarmee kan worden aangegeven dat een object in de registratie is opgenomen als gevolg van een feitelijke constatering, zonder dat er op het moment van opname sprake is van een formele grondslag voor deze opname.';


--
-- Name: COLUMN bag_woonplaats.naam; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.naam IS '11.70 : de benaming van een door het gemeentebestuur aangewezen woonplaats.';


--
-- Name: COLUMN bag_woonplaats.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.status IS '11.79 : de fase van de levenscyclus van een woonplaats, waarin de betreffende woonplaats zich bevindt.';


--
-- Name: COLUMN bag_woonplaats.inonderzoek; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.inonderzoek IS '11.75 : een aanduiding waarmee wordt aangegeven dat een onderzoek wordt uitgevoerd naar de juistheid van een of meerdere gegevens van het betreffende object.';


--
-- Name: COLUMN bag_woonplaats.documentdatum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.documentdatum IS '11.77 : de datum waarop het brondocument is vastgesteld, op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een woonplaats heeft plaatsgevonden.';


--
-- Name: COLUMN bag_woonplaats.documentnummer; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.documentnummer IS '11.78 : de unieke aanduiding van het brondocument op basis waarvan een opname, mutatie of een verwijdering van gegevens ten aanzien van een woonplaats heeft plaatsgevonden, binnen een gemeente.';


--
-- Name: COLUMN bag_woonplaats.correctie; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bag_woonplaats.correctie IS 'het gegeven is gecorrigeerd.';


--
-- Name: bag_woonplaats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bag_woonplaats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bag_woonplaats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bag_woonplaats_id_seq OWNED BY public.bag_woonplaats.id;


--
-- Name: searchable; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.searchable (
    search_index tsvector,
    search_term text,
    object_type text,
    searchable_id integer NOT NULL,
    search_order text
);


--
-- Name: bedrijf; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bedrijf (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bedrijf'::character varying,
    id integer NOT NULL,
    dossiernummer character varying(8),
    subdossiernummer character varying(4),
    hoofdvestiging_dossiernummer character varying(8),
    hoofdvestiging_subdossiernummer character varying(4),
    vorig_dossiernummer character varying(8),
    vorig_subdossiernummer character varying(4),
    handelsnaam text,
    rechtsvorm smallint,
    kamernummer smallint,
    faillisement smallint,
    surseance smallint,
    telefoonnummer character varying(10),
    email character varying(128),
    vestiging_adres text,
    vestiging_straatnaam text,
    vestiging_huisnummer bigint,
    vestiging_huisnummertoevoeging text,
    vestiging_postcodewoonplaats text,
    vestiging_postcode character varying(6),
    vestiging_woonplaats text,
    correspondentie_adres text,
    correspondentie_straatnaam text,
    correspondentie_huisnummer bigint,
    correspondentie_huisnummertoevoeging text,
    correspondentie_postcodewoonplaats text,
    correspondentie_postcode character varying(6),
    correspondentie_woonplaats text,
    hoofdactiviteitencode integer,
    nevenactiviteitencode1 integer,
    nevenactiviteitencode2 integer,
    werkzamepersonen integer,
    contact_naam character varying(64),
    contact_aanspreektitel character varying(45),
    contact_voorletters character varying(19),
    contact_voorvoegsel character varying(8),
    contact_geslachtsnaam character varying(95),
    contact_geslachtsaanduiding character varying(1),
    authenticated smallint,
    authenticatedby text,
    fulldossiernummer text,
    import_datum timestamp(6) without time zone,
    deleted_on timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    system_of_record character varying(32),
    system_of_record_id bigint,
    vestigingsnummer bigint,
    vestiging_huisletter text,
    correspondentie_huisletter text,
    vestiging_adres_buitenland1 text,
    vestiging_adres_buitenland2 text,
    vestiging_adres_buitenland3 text,
    vestiging_landcode integer DEFAULT 6030,
    correspondentie_adres_buitenland1 text,
    correspondentie_adres_buitenland2 text,
    correspondentie_adres_buitenland3 text,
    correspondentie_landcode integer DEFAULT 6030,
    uuid uuid DEFAULT public.uuid_generate_v4()
)
INHERITS (public.searchable);


--
-- Name: bedrijf_authenticatie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bedrijf_authenticatie (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    login integer,
    password character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: bedrijf_authenticatie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bedrijf_authenticatie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bedrijf_authenticatie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bedrijf_authenticatie_id_seq OWNED BY public.bedrijf_authenticatie.id;


--
-- Name: bedrijf_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bedrijf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bedrijf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bedrijf_id_seq OWNED BY public.bedrijf.id;


--
-- Name: beheer_import; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.beheer_import (
    id integer NOT NULL,
    importtype character varying(256),
    succesvol integer,
    finished timestamp without time zone,
    import_create integer,
    import_update integer,
    error integer,
    error_message text,
    entries integer,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: beheer_import_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.beheer_import_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: beheer_import_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.beheer_import_id_seq OWNED BY public.beheer_import.id;


--
-- Name: beheer_import_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.beheer_import_log (
    id integer NOT NULL,
    import_id integer,
    old_data text,
    new_data text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    kolom text,
    identifier text,
    action character varying(255)
);


--
-- Name: beheer_import_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.beheer_import_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: beheer_import_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.beheer_import_log_id_seq OWNED BY public.beheer_import_log.id;


--
-- Name: betrokkene_notes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.betrokkene_notes (
    id integer NOT NULL,
    betrokkene_exid integer,
    betrokkene_type text,
    betrokkene_from text,
    ntype text,
    subject text,
    message text,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: betrokkene_notes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.betrokkene_notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: betrokkene_notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.betrokkene_notes_id_seq OWNED BY public.betrokkene_notes.id;


--
-- Name: betrokkenen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.betrokkenen (
    id integer NOT NULL,
    btype integer,
    gm_natuurlijk_persoon_id integer,
    naam text
);


--
-- Name: betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.betrokkenen_id_seq OWNED BY public.betrokkenen.id;


--
-- Name: bibliotheek_categorie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_categorie (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_categorie'::character varying,
    id integer NOT NULL,
    naam character varying(256),
    label text,
    description text,
    help text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    system integer,
    pid integer,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
)
INHERITS (public.searchable);


--
-- Name: bibliotheek_categorie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_categorie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_categorie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_categorie_id_seq OWNED BY public.bibliotheek_categorie.id;


--
-- Name: bibliotheek_kenmerken; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_kenmerken (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_kenmerken'::character varying,
    id integer NOT NULL,
    naam character varying(256),
    value_type text,
    value_default text DEFAULT ''::text NOT NULL,
    label text,
    description text,
    help text DEFAULT ''::text NOT NULL,
    magic_string text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    bibliotheek_categorie_id integer,
    document_categorie text,
    system integer,
    deleted timestamp without time zone,
    file_metadata_id integer,
    version integer DEFAULT 1 NOT NULL,
    properties text DEFAULT '{}'::text,
    naam_public text DEFAULT ''::text NOT NULL,
    type_multiple boolean DEFAULT false NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    CONSTRAINT bibliotheek_kenmerken_value_type_check CHECK ((value_type = ANY (ARRAY['text_uc'::text, 'checkbox'::text, 'richtext'::text, 'date'::text, 'file'::text, 'bag_straat_adres'::text, 'email'::text, 'valutaex'::text, 'bag_openbareruimte'::text, 'text'::text, 'bag_openbareruimtes'::text, 'url'::text, 'valuta'::text, 'option'::text, 'bag_adres'::text, 'select'::text, 'valutain6'::text, 'valutaex6'::text, 'valutaex21'::text, 'image_from_url'::text, 'bag_adressen'::text, 'valutain'::text, 'calendar'::text, 'calendar_supersaas'::text, 'bag_straat_adressen'::text, 'googlemaps'::text, 'numeric'::text, 'valutain21'::text, 'textarea'::text, 'bankaccount'::text, 'subject'::text, 'geolatlon'::text, 'appointment'::text])))
)
INHERITS (public.searchable);


--
-- Name: bibliotheek_kenmerken_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_kenmerken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_kenmerken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_kenmerken_id_seq OWNED BY public.bibliotheek_kenmerken.id;


--
-- Name: bibliotheek_kenmerken_values; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_kenmerken_values (
    id integer NOT NULL,
    bibliotheek_kenmerken_id integer,
    value text,
    active boolean DEFAULT true NOT NULL,
    sort_order integer NOT NULL
);


--
-- Name: bibliotheek_kenmerken_values_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_kenmerken_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_kenmerken_values_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_kenmerken_values_id_seq OWNED BY public.bibliotheek_kenmerken_values.id;


--
-- Name: bibliotheek_kenmerken_values_sort_order_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_kenmerken_values_sort_order_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_kenmerken_values_sort_order_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_kenmerken_values_sort_order_seq OWNED BY public.bibliotheek_kenmerken_values.sort_order;


--
-- Name: bibliotheek_notificatie_kenmerk; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_notificatie_kenmerk (
    id integer NOT NULL,
    bibliotheek_notificatie_id integer NOT NULL,
    bibliotheek_kenmerken_id integer NOT NULL
);


--
-- Name: bibliotheek_notificatie_kenmerk_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_notificatie_kenmerk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_notificatie_kenmerk_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_notificatie_kenmerk_id_seq OWNED BY public.bibliotheek_notificatie_kenmerk.id;


--
-- Name: bibliotheek_notificaties; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_notificaties (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_notificaties'::character varying,
    id integer NOT NULL,
    bibliotheek_categorie_id integer,
    label text,
    subject text,
    message text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    sender text,
    sender_address text,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
)
INHERITS (public.searchable);


--
-- Name: bibliotheek_notificaties_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_notificaties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_notificaties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_notificaties_id_seq OWNED BY public.bibliotheek_notificaties.id;


--
-- Name: bibliotheek_sjablonen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_sjablonen (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'bibliotheek_sjablonen'::character varying,
    id integer NOT NULL,
    bibliotheek_categorie_id integer,
    naam character varying(256),
    label text,
    description text,
    help text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    filestore_id integer,
    deleted timestamp without time zone,
    interface_id integer,
    template_external_name text,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    CONSTRAINT filestore_or_interface CHECK (((filestore_id IS NOT NULL) OR (interface_id IS NOT NULL)))
)
INHERITS (public.searchable);


--
-- Name: bibliotheek_sjablonen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_sjablonen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_sjablonen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_sjablonen_id_seq OWNED BY public.bibliotheek_sjablonen.id;


--
-- Name: bibliotheek_sjablonen_magic_string; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bibliotheek_sjablonen_magic_string (
    id integer NOT NULL,
    bibliotheek_sjablonen_id integer,
    value text
);


--
-- Name: bibliotheek_sjablonen_magic_string_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bibliotheek_sjablonen_magic_string_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bibliotheek_sjablonen_magic_string_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bibliotheek_sjablonen_magic_string_id_seq OWNED BY public.bibliotheek_sjablonen_magic_string.id;


--
-- Name: case_action; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.case_action (
    id integer NOT NULL,
    case_id integer NOT NULL,
    casetype_status_id integer,
    type character varying(64),
    label character varying(255),
    automatic boolean,
    data text,
    state_tainted boolean DEFAULT false,
    data_tainted boolean DEFAULT false
);


--
-- Name: case_action_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.case_action_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: case_action_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.case_action_id_seq OWNED BY public.case_action.id;


--
-- Name: case_property; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.case_property (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    type text NOT NULL,
    namespace text NOT NULL,
    value jsonb,
    value_v0 jsonb,
    case_id integer NOT NULL,
    object_id uuid NOT NULL,
    CONSTRAINT case_property_min_value_count CHECK (((value IS NOT NULL) OR (value_v0 IS NOT NULL)))
);


--
-- Name: COLUMN case_property.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.case_property.name IS 'unified magic_string/v1 attribute/v0 property';


--
-- Name: COLUMN case_property.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.case_property.type IS 'de-normalized type of the /value/ of the property in the context of the referent object';


--
-- Name: COLUMN case_property.value; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.case_property.value IS 'syzygy-style value blob; {"value_type_name":"text","value":"myval","meta":"data"}';


--
-- Name: COLUMN case_property.value_v0; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.case_property.value_v0 IS 'object_data-style value blob; {"human_label":"Aanvrager KvK-nummer","human_value":"123456789","value":":123456789","name":"case.requestor.coc","attribute_type":"text"}';


--
-- Name: case_relation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.case_relation (
    id integer NOT NULL,
    case_id_a integer,
    case_id_b integer,
    order_seq_a integer,
    order_seq_b integer,
    type_a character varying(64),
    type_b character varying(64),
    uuid uuid DEFAULT public.uuid_generate_v4()
);


--
-- Name: case_relation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.case_relation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: case_relation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.case_relation_id_seq OWNED BY public.case_relation.id;


--
-- Name: checklist; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.checklist (
    id integer NOT NULL,
    case_id integer NOT NULL,
    case_milestone integer
);


--
-- Name: checklist_antwoord_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.checklist_antwoord_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: checklist_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.checklist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: checklist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.checklist_id_seq OWNED BY public.checklist.id;


--
-- Name: checklist_item; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.checklist_item (
    id integer NOT NULL,
    checklist_id integer NOT NULL,
    label text,
    state boolean DEFAULT false NOT NULL,
    sequence integer,
    user_defined boolean DEFAULT true NOT NULL,
    deprecated_answer character varying(8)
);


--
-- Name: checklist_item_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.checklist_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: checklist_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.checklist_item_id_seq OWNED BY public.checklist_item.id;


--
-- Name: config; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.config (
    id integer NOT NULL,
    parameter character varying(128),
    value text,
    advanced boolean DEFAULT true NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    definition_id uuid NOT NULL
);


--
-- Name: config_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.config_id_seq OWNED BY public.config.id;


--
-- Name: contact_data; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contact_data (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    betrokkene_type integer,
    mobiel character varying(255),
    telefoonnummer character varying(255),
    email character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone,
    note text
);


--
-- Name: contact_data_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contact_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contact_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contact_data_id_seq OWNED BY public.contact_data.id;


--
-- Name: contactmoment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contactmoment (
    id integer NOT NULL,
    subject_id character varying(100),
    case_id integer,
    type public.contactmoment_type NOT NULL,
    medium public.contactmoment_medium NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    created_by text NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


--
-- Name: contactmoment_email; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contactmoment_email (
    id integer NOT NULL,
    filestore_id integer NOT NULL,
    contactmoment_id integer NOT NULL,
    body text NOT NULL,
    subject text NOT NULL,
    recipient text NOT NULL,
    cc text,
    bcc text
);


--
-- Name: contactmoment_email_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contactmoment_email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contactmoment_email_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contactmoment_email_id_seq OWNED BY public.contactmoment_email.id;


--
-- Name: contactmoment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contactmoment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contactmoment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contactmoment_id_seq OWNED BY public.contactmoment.id;


--
-- Name: contactmoment_note; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contactmoment_note (
    id integer NOT NULL,
    message character varying,
    contactmoment_id integer NOT NULL
);


--
-- Name: contactmoment_note_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contactmoment_note_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contactmoment_note_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contactmoment_note_id_seq OWNED BY public.contactmoment_note.id;


--
-- Name: directory; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.directory (
    id integer NOT NULL,
    name text NOT NULL,
    case_id integer NOT NULL,
    original_name text NOT NULL,
    path integer[] DEFAULT ARRAY[]::integer[] NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


--
-- Name: directory_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.directory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: directory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.directory_id_seq OWNED BY public.directory.id;


--
-- Name: searchable_searchable_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.searchable_searchable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: searchable_searchable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.searchable_searchable_id_seq OWNED BY public.searchable.searchable_id;


--
-- Name: file; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'file'::character varying,
    searchable_id integer DEFAULT nextval('public.searchable_searchable_id_seq'::regclass),
    id integer NOT NULL,
    filestore_id integer NOT NULL,
    name text NOT NULL,
    extension character varying(10) NOT NULL,
    root_file_id integer,
    version integer DEFAULT 1,
    case_id integer,
    metadata_id integer,
    subject_id character varying(100),
    directory_id integer,
    creation_reason text NOT NULL,
    accepted boolean DEFAULT false NOT NULL,
    rejection_reason text,
    reject_to_queue boolean DEFAULT false,
    is_duplicate_name boolean DEFAULT false NOT NULL,
    publish_pip boolean DEFAULT false NOT NULL,
    publish_website boolean DEFAULT false NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    created_by character varying(100) NOT NULL,
    date_modified timestamp without time zone,
    modified_by character varying(100),
    date_deleted timestamp without time zone,
    deleted_by character varying(100),
    destroyed boolean DEFAULT false,
    scheduled_jobs_id integer,
    intake_owner character varying,
    active_version boolean DEFAULT false NOT NULL,
    is_duplicate_of integer,
    queue boolean DEFAULT true NOT NULL,
    document_status public.documentstatus DEFAULT 'original'::public.documentstatus NOT NULL,
    generator text,
    lock_timestamp timestamp without time zone,
    lock_subject_id uuid,
    lock_subject_name text,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    confidential boolean DEFAULT false NOT NULL,
    CONSTRAINT lock_fields_check CHECK (((lock_timestamp IS NULL) OR ((lock_subject_id IS NOT NULL) AND (lock_subject_name IS NOT NULL))))
)
INHERITS (public.searchable);


--
-- Name: file_annotation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file_annotation (
    id uuid NOT NULL,
    file_id integer NOT NULL,
    subject character varying(255) NOT NULL,
    properties text DEFAULT '{}'::text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone
);


--
-- Name: file_case_document; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file_case_document (
    id integer NOT NULL,
    file_id integer NOT NULL,
    case_document_id integer NOT NULL
);


--
-- Name: file_case_document_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.file_case_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: file_case_document_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.file_case_document_id_seq OWNED BY public.file_case_document.id;


--
-- Name: file_derivative; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file_derivative (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    file_id integer NOT NULL,
    filestore_id integer NOT NULL,
    max_width integer NOT NULL,
    max_height integer NOT NULL,
    date_generated timestamp without time zone DEFAULT now() NOT NULL,
    type text NOT NULL,
    CONSTRAINT file_derivative_type CHECK ((type = ANY (ARRAY['pdf'::text, 'thumbnail'::text, 'doc'::text, 'docx'::text])))
);


--
-- Name: file_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.file_id_seq OWNED BY public.file.id;


--
-- Name: file_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file_metadata (
    id integer NOT NULL,
    description text,
    trust_level text DEFAULT 'Zaakvertrouwelijk'::character varying NOT NULL,
    origin text,
    document_category text,
    origin_date date,
    pronom_format text,
    appearance text,
    structure text,
    creation_date date,
    CONSTRAINT file_metadata_origin_check CHECK ((origin ~ '(Inkomend|Uitgaand|Intern)'::text)),
    CONSTRAINT file_metadata_trust_level_check CHECK ((trust_level ~ '(Openbaar|Beperkt openbaar|Intern|Zaakvertrouwelijk|Vertrouwelijk|onfidentieel|Geheim|Zeer geheim)'::text))
);


--
-- Name: file_metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.file_metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: file_metadata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.file_metadata_id_seq OWNED BY public.file_metadata.id;


--
-- Name: filestore; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.filestore (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    thumbnail_uuid uuid,
    original_name text NOT NULL,
    size integer NOT NULL,
    mimetype character varying(160) NOT NULL,
    md5 character varying(100) NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    storage_location text[],
    is_archivable boolean DEFAULT false NOT NULL,
    virus_scan_status text DEFAULT 'pending'::text NOT NULL,
    CONSTRAINT filestore_virus_scan_status_check CHECK ((virus_scan_status ~ '^(pending|ok|found(:.*)?)$'::text))
);


--
-- Name: filestore_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.filestore_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: filestore_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.filestore_id_seq OWNED BY public.filestore.id;


--
-- Name: gegevensmagazijn_subjecten; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gegevensmagazijn_subjecten (
    subject_uuid uuid NOT NULL,
    nnp_uuid uuid NOT NULL
);


--
-- Name: gm_adres; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gm_adres (
    id integer NOT NULL,
    straatnaam character varying(80),
    huisnummer bigint,
    huisletter character(1),
    huisnummertoevoeging text,
    nadere_aanduiding character varying(35),
    postcode character varying(6),
    woonplaats character varying(75),
    gemeentedeel character varying(75),
    functie_adres character(1) NOT NULL,
    datum_aanvang_bewoning date,
    woonplaats_id character varying(32),
    gemeente_code smallint,
    hash character varying(32),
    import_datum timestamp(6) without time zone,
    adres_buitenland1 text,
    adres_buitenland2 text,
    adres_buitenland3 text,
    landcode integer DEFAULT 6030,
    natuurlijk_persoon_id integer,
    deleted_on timestamp(6) without time zone
);


--
-- Name: gm_adres_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.gm_adres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gm_adres_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.gm_adres_id_seq OWNED BY public.gm_adres.id;


--
-- Name: gm_bedrijf; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gm_bedrijf (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    dossiernummer character varying(8),
    subdossiernummer character varying(4),
    hoofdvestiging_dossiernummer character varying(8),
    hoofdvestiging_subdossiernummer character varying(4),
    vorig_dossiernummer character varying(8),
    vorig_subdossiernummer character varying(4),
    handelsnaam text,
    rechtsvorm smallint,
    kamernummer smallint,
    faillisement smallint,
    surseance smallint,
    telefoonnummer character varying(15),
    email character varying(128),
    vestiging_adres text,
    vestiging_straatnaam text,
    vestiging_huisnummer bigint,
    vestiging_huisnummertoevoeging text,
    vestiging_postcodewoonplaats text,
    vestiging_postcode character varying(6),
    vestiging_woonplaats text,
    correspondentie_adres text,
    correspondentie_straatnaam text,
    correspondentie_huisnummer bigint,
    correspondentie_huisnummertoevoeging text,
    correspondentie_postcodewoonplaats text,
    correspondentie_postcode character varying(6),
    correspondentie_woonplaats text,
    hoofdactiviteitencode integer,
    nevenactiviteitencode1 integer,
    nevenactiviteitencode2 integer,
    werkzamepersonen integer,
    contact_naam character varying(64),
    contact_aanspreektitel character varying(45),
    contact_voorletters character varying(19),
    contact_voorvoegsel character varying(8),
    contact_geslachtsnaam character varying(95),
    contact_geslachtsaanduiding character varying(1),
    authenticated smallint,
    authenticatedby text,
    import_datum timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    vestigingsnummer bigint,
    vestiging_huisletter text,
    correspondentie_huisletter text,
    vestiging_adres_buitenland1 text,
    vestiging_adres_buitenland2 text,
    vestiging_adres_buitenland3 text,
    vestiging_landcode integer DEFAULT 6030,
    correspondentie_adres_buitenland1 text,
    correspondentie_adres_buitenland2 text,
    correspondentie_adres_buitenland3 text,
    correspondentie_landcode integer DEFAULT 6030
);


--
-- Name: gm_bedrijf_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.gm_bedrijf_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gm_bedrijf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.gm_bedrijf_id_seq OWNED BY public.gm_bedrijf.id;


--
-- Name: gm_natuurlijk_persoon; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gm_natuurlijk_persoon (
    id integer NOT NULL,
    gegevens_magazijn_id integer,
    betrokkene_type integer,
    burgerservicenummer character varying(9),
    a_nummer character varying(10),
    voorletters character varying(50),
    voornamen character varying(200),
    geslachtsnaam character varying(200),
    voorvoegsel character varying(50),
    geslachtsaanduiding character varying(3),
    nationaliteitscode1 smallint,
    nationaliteitscode2 smallint,
    nationaliteitscode3 smallint,
    geboorteplaats character varying(75),
    geboorteland character varying(75),
    geboortedatum timestamp without time zone,
    aanhef_aanschrijving character varying(10),
    voorletters_aanschrijving character varying(20),
    voornamen_aanschrijving character varying(200),
    naam_aanschrijving character varying(200),
    voorvoegsel_aanschrijving character varying(50),
    burgerlijke_staat character(1),
    indicatie_geheim character(1),
    import_datum timestamp(6) without time zone,
    adres_id integer,
    authenticatedby text,
    authenticated smallint,
    datum_overlijden timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    aanduiding_naamgebruik character varying(1),
    onderzoek_persoon boolean,
    onderzoek_huwelijk boolean,
    onderzoek_overlijden boolean,
    onderzoek_verblijfplaats boolean,
    partner_a_nummer character varying(50),
    partner_burgerservicenummer character varying(50),
    partner_voorvoegsel character varying(50),
    partner_geslachtsnaam character varying(50),
    datum_huwelijk timestamp without time zone,
    datum_huwelijk_ontbinding timestamp without time zone,
    landcode integer DEFAULT 6030 NOT NULL,
    naamgebruik text,
    adellijke_titel text
);


--
-- Name: gm_natuurlijk_persoon_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.gm_natuurlijk_persoon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gm_natuurlijk_persoon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.gm_natuurlijk_persoon_id_seq OWNED BY public.gm_natuurlijk_persoon.id;


--
-- Name: groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.groups (
    id integer NOT NULL,
    path integer[] NOT NULL,
    name text,
    description text,
    date_created timestamp without time zone,
    date_modified timestamp without time zone,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.groups_id_seq OWNED BY public.groups.id;


--
-- Name: interface; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.interface (
    id integer NOT NULL,
    name text NOT NULL,
    active boolean NOT NULL,
    case_type_id integer,
    max_retries integer NOT NULL,
    interface_config text NOT NULL,
    multiple boolean DEFAULT false NOT NULL,
    module text NOT NULL,
    date_deleted timestamp without time zone,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    objecttype_id uuid
);


--
-- Name: interface_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.interface_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: interface_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.interface_id_seq OWNED BY public.interface.id;


--
-- Name: logging; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.logging (
    id integer NOT NULL,
    zaak_id integer,
    betrokkene_id character varying(128),
    aanvrager_id character varying(128),
    is_bericht integer,
    component character varying(64),
    component_id integer,
    seen integer,
    onderwerp text,
    bericht text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted_on timestamp without time zone,
    event_type text,
    event_data text,
    created_by text,
    modified_by text,
    deleted_by text,
    created_for text,
    created_by_name_cache character varying,
    object_uuid uuid,
    restricted boolean DEFAULT false NOT NULL
);


--
-- Name: logging_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.logging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: logging_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.logging_id_seq OWNED BY public.logging.id;


--
-- Name: message; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.message (
    id integer NOT NULL,
    message text NOT NULL,
    subject_id character varying,
    logging_id integer NOT NULL,
    is_read boolean DEFAULT false,
    is_archived boolean DEFAULT false NOT NULL
);


--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.message_id_seq OWNED BY public.message.id;


--
-- Name: natuurlijk_persoon; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.natuurlijk_persoon (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'natuurlijk_persoon'::character varying,
    id integer NOT NULL,
    burgerservicenummer character varying(9),
    a_nummer character varying(10),
    voorletters character varying(50),
    voornamen text,
    geslachtsnaam text,
    voorvoegsel character varying(50),
    geslachtsaanduiding character varying(3),
    nationaliteitscode1 smallint,
    nationaliteitscode2 smallint,
    nationaliteitscode3 smallint,
    geboorteplaats text,
    geboorteland text,
    geboortedatum timestamp without time zone,
    aanhef_aanschrijving character varying(10),
    voorletters_aanschrijving character varying(20),
    voornamen_aanschrijving character varying(200),
    naam_aanschrijving text,
    voorvoegsel_aanschrijving character varying(50),
    burgerlijke_staat character(1),
    indicatie_geheim character(1),
    land_waarnaar_vertrokken smallint,
    import_datum timestamp(6) without time zone,
    adres_id integer,
    authenticated boolean DEFAULT false NOT NULL,
    authenticatedby text,
    deleted_on timestamp(6) without time zone,
    verblijfsobject_id character varying(16),
    datum_overlijden timestamp without time zone,
    aanduiding_naamgebruik character varying(1),
    onderzoek_persoon boolean,
    onderzoek_huwelijk boolean,
    onderzoek_overlijden boolean,
    onderzoek_verblijfplaats boolean,
    partner_a_nummer character varying(50),
    partner_burgerservicenummer character varying(50),
    partner_voorvoegsel character varying(50),
    partner_geslachtsnaam character varying(50),
    datum_huwelijk timestamp without time zone,
    datum_huwelijk_ontbinding timestamp without time zone,
    in_gemeente boolean,
    landcode integer DEFAULT 6030 NOT NULL,
    naamgebruik text,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    active boolean DEFAULT true NOT NULL,
    adellijke_titel text
)
INHERITS (public.searchable);


--
-- Name: natuurlijk_persoon_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.natuurlijk_persoon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: natuurlijk_persoon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.natuurlijk_persoon_id_seq OWNED BY public.natuurlijk_persoon.id;


--
-- Name: object_acl_entry; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_acl_entry (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_uuid uuid NOT NULL,
    entity_type text NOT NULL,
    entity_id text NOT NULL,
    capability text NOT NULL,
    scope text DEFAULT 'instance'::text NOT NULL,
    groupname text,
    CONSTRAINT object_acl_entry_groupname_check CHECK (((groupname IS NULL) OR (character_length(groupname) > 0))),
    CONSTRAINT object_acl_entry_scope_check CHECK ((scope = ANY (ARRAY['instance'::text, 'type'::text])))
);


--
-- Name: object_bibliotheek_entry; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_bibliotheek_entry (
    search_index tsvector,
    search_term text NOT NULL,
    object_type text NOT NULL,
    searchable_id integer DEFAULT nextval('public.searchable_searchable_id_seq'::regclass),
    id integer NOT NULL,
    bibliotheek_categorie_id integer,
    object_uuid uuid NOT NULL,
    name text NOT NULL
)
INHERITS (public.searchable);


--
-- Name: object_bibliotheek_entry_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.object_bibliotheek_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: object_bibliotheek_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.object_bibliotheek_entry_id_seq OWNED BY public.object_bibliotheek_entry.id;


--
-- Name: object_data; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_data (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id integer,
    object_class text NOT NULL,
    properties text DEFAULT '{}'::text NOT NULL,
    index_hstore public.hstore,
    date_created timestamp without time zone,
    date_modified timestamp without time zone,
    text_vector tsvector,
    class_uuid uuid,
    acl_groupname text,
    invalid boolean DEFAULT false,
    CONSTRAINT object_data_acl_groupname_check CHECK (((acl_groupname IS NULL) OR (character_length(acl_groupname) > 0)))
);


--
-- Name: object_mutation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_mutation (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_uuid uuid,
    object_type text DEFAULT 'object'::text,
    lock_object_uuid uuid,
    type text NOT NULL,
    "values" text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT now(),
    subject_id integer NOT NULL,
    executed boolean DEFAULT false NOT NULL,
    CONSTRAINT object_mutation_type_check CHECK ((type = ANY (ARRAY['create'::text, 'update'::text, 'delete'::text, 'relate'::text])))
);


--
-- Name: object_relation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_relation (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    object_type text NOT NULL,
    object_uuid uuid,
    object_embedding text,
    object_id uuid,
    object_preview text,
    CONSTRAINT object_relation_ref_xor_embed CHECK (((object_uuid IS NULL) <> (object_embedding IS NULL)))
);


--
-- Name: object_relationships; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_relationships (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object1_uuid uuid NOT NULL,
    object2_uuid uuid NOT NULL,
    type1 text NOT NULL,
    type2 text NOT NULL,
    object1_type text NOT NULL,
    object2_type text NOT NULL,
    blocks_deletion boolean DEFAULT false NOT NULL,
    title1 text,
    title2 text,
    owner_object_uuid uuid
);


--
-- Name: object_subscription; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.object_subscription (
    id integer NOT NULL,
    interface_id integer NOT NULL,
    external_id character varying(255) NOT NULL,
    local_table character varying(100) NOT NULL,
    local_id character varying(255) NOT NULL,
    date_created timestamp without time zone DEFAULT now(),
    date_deleted timestamp without time zone,
    object_preview text,
    config_interface_id integer
);


--
-- Name: object_subscription_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.object_subscription_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: object_subscription_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.object_subscription_id_seq OWNED BY public.object_subscription.id;


--
-- Name: parkeergebied; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.parkeergebied (
    id integer NOT NULL,
    bag_hoofdadres bigint,
    postcode character varying(6) NOT NULL,
    straatnaam character varying(255) NOT NULL,
    huisnummer integer,
    huisletter character varying(8),
    huisnummertoevoeging character varying(4),
    parkeergebied_id integer,
    parkeergebied character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone,
    woonplaats character varying(255)
);


--
-- Name: parkeergebied_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.parkeergebied_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: parkeergebied_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.parkeergebied_id_seq OWNED BY public.parkeergebied.id;


--
-- Name: queue; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.queue (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    object_id uuid,
    status text DEFAULT 'pending'::text NOT NULL,
    type text NOT NULL,
    label text NOT NULL,
    data text DEFAULT '{}'::text NOT NULL,
    date_created timestamp without time zone DEFAULT statement_timestamp() NOT NULL,
    date_started timestamp without time zone,
    date_finished timestamp without time zone,
    parent_id uuid,
    priority integer DEFAULT 1000 NOT NULL,
    metadata text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT queue_status_check CHECK ((status = ANY (ARRAY['pending'::text, 'running'::text, 'finished'::text, 'failed'::text, 'waiting'::text])))
);


--
-- Name: remote_api_keys; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.remote_api_keys (
    id integer NOT NULL,
    key character varying(60) NOT NULL,
    permissions text NOT NULL
);


--
-- Name: remote_api_keys_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.remote_api_keys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: remote_api_keys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.remote_api_keys_id_seq OWNED BY public.remote_api_keys.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    parent_group_id integer,
    name text,
    description text,
    system_role boolean,
    date_created timestamp without time zone,
    date_modified timestamp without time zone,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: sbus_logging; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sbus_logging (
    id integer NOT NULL,
    sbus_traffic_id integer,
    pid integer,
    mutatie_type text,
    object text,
    params text,
    kerngegeven text,
    label text,
    changes text,
    error boolean,
    error_message text,
    created timestamp without time zone,
    modified timestamp without time zone
);


--
-- Name: sbus_logging_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sbus_logging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sbus_logging_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sbus_logging_id_seq OWNED BY public.sbus_logging.id;


--
-- Name: sbus_traffic; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sbus_traffic (
    id integer NOT NULL,
    sbus_type text,
    object text,
    operation text,
    input text,
    input_raw text,
    output text,
    output_raw text,
    error boolean,
    error_message text,
    created timestamp without time zone,
    modified timestamp without time zone
);


--
-- Name: sbus_traffic_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sbus_traffic_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sbus_traffic_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sbus_traffic_id_seq OWNED BY public.sbus_traffic.id;


--
-- Name: scheduled_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.scheduled_jobs (
    id integer NOT NULL,
    task character varying(255) NOT NULL,
    scheduled_for timestamp without time zone,
    parameters text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    schedule_type character varying(16),
    case_id integer,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    CONSTRAINT scheduled_jobs_schedule_type_check CHECK (((schedule_type)::text ~ '(manual|time)'::text))
);


--
-- Name: scheduled_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.scheduled_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: scheduled_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.scheduled_jobs_id_seq OWNED BY public.scheduled_jobs.id;


--
-- Name: search_query; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.search_query (
    id integer NOT NULL,
    settings text,
    ldap_id integer,
    name character varying(256),
    sort_index integer
);


--
-- Name: search_query_delen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.search_query_delen (
    id integer NOT NULL,
    search_query_id integer,
    ou_id integer,
    role_id integer
);


--
-- Name: search_query_delen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.search_query_delen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: search_query_delen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.search_query_delen_id_seq OWNED BY public.search_query_delen.id;


--
-- Name: search_query_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.search_query_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: search_query_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.search_query_id_seq OWNED BY public.search_query.id;


--
-- Name: session_invitation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.session_invitation (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    subject_id uuid NOT NULL,
    object_id uuid,
    object_type text,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_expires timestamp without time zone NOT NULL,
    token text NOT NULL,
    action_path text,
    CONSTRAINT object_reference CHECK ((((object_id IS NOT NULL) AND (object_type IS NOT NULL)) OR ((object_id IS NULL) AND (object_type IS NULL))))
);


--
-- Name: settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.settings (
    id integer NOT NULL,
    key character varying(255),
    value text
);


--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.settings_id_seq OWNED BY public.settings.id;


--
-- Name: subject; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subject (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    subject_type text NOT NULL,
    properties text DEFAULT '{}'::text NOT NULL,
    settings text DEFAULT '{}'::text NOT NULL,
    username character varying NOT NULL,
    last_modified timestamp without time zone DEFAULT now(),
    role_ids integer[],
    group_ids integer[],
    nobody boolean DEFAULT false NOT NULL,
    system boolean DEFAULT false NOT NULL,
    CONSTRAINT subject_subject_type_check CHECK ((subject_type = ANY (ARRAY['person'::text, 'company'::text, 'employee'::text])))
);


--
-- Name: subject_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.subject_id_seq OWNED BY public.subject.id;


--
-- Name: transaction; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transaction (
    id integer NOT NULL,
    interface_id integer NOT NULL,
    external_transaction_id character varying(250),
    input_data text,
    input_file integer,
    automated_retry_count integer,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    date_last_retry timestamp without time zone,
    date_next_retry timestamp without time zone,
    processed boolean DEFAULT false,
    date_deleted timestamp without time zone,
    error_count integer DEFAULT 0,
    direction character varying(255) DEFAULT 'incoming'::character varying NOT NULL,
    success_count integer DEFAULT 0,
    total_count integer DEFAULT 0,
    processor_params text,
    error_fatal boolean,
    preview_data text DEFAULT '{}'::text NOT NULL,
    error_message text,
    text_vector tsvector,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    CONSTRAINT input_data_or_input_file CHECK (((input_data IS NOT NULL) OR (input_file IS NOT NULL))),
    CONSTRAINT transaction_direction_check CHECK ((((direction)::text = 'incoming'::text) OR ((direction)::text = 'outgoing'::text)))
);


--
-- Name: transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.transaction_id_seq OWNED BY public.transaction.id;


--
-- Name: transaction_record; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transaction_record (
    id integer NOT NULL,
    transaction_id integer,
    input text NOT NULL,
    output text NOT NULL,
    is_error boolean DEFAULT false NOT NULL,
    date_executed timestamp without time zone DEFAULT now() NOT NULL,
    date_deleted timestamp without time zone,
    preview_string character varying(200),
    last_error text,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


--
-- Name: transaction_record_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.transaction_record_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transaction_record_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.transaction_record_id_seq OWNED BY public.transaction_record.id;


--
-- Name: transaction_record_to_object; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transaction_record_to_object (
    id integer NOT NULL,
    transaction_record_id integer,
    local_table character varying(100),
    local_id character varying(255),
    mutations text,
    date_deleted timestamp without time zone,
    mutation_type character varying(100)
);


--
-- Name: transaction_record_to_object_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.transaction_record_to_object_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transaction_record_to_object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.transaction_record_to_object_id_seq OWNED BY public.transaction_record_to_object.id;


--
-- Name: user_app_lock; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_app_lock (
    type character(40) NOT NULL,
    type_id character(20) NOT NULL,
    create_unixtime integer NOT NULL,
    session_id character(40) NOT NULL,
    uidnumber integer NOT NULL
);


--
-- Name: user_entity; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_entity (
    id integer NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    source_interface_id integer,
    source_identifier text NOT NULL,
    subject_id integer,
    date_created timestamp without time zone,
    date_deleted timestamp without time zone,
    properties text DEFAULT '{}'::text,
    password character varying(255),
    active boolean DEFAULT true NOT NULL
);


--
-- Name: user_entity_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_entity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_entity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_entity_id_seq OWNED BY public.user_entity.id;


--
-- Name: woz_objects; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.woz_objects (
    id integer NOT NULL,
    object_data text,
    owner character varying(255) NOT NULL,
    object_id character varying(32) NOT NULL
);


--
-- Name: woz_objects_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.woz_objects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: woz_objects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.woz_objects_id_seq OWNED BY public.woz_objects.id;


--
-- Name: zaak; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'zaak'::character varying,
    id integer NOT NULL,
    pid integer,
    relates_to integer,
    zaaktype_id integer NOT NULL,
    zaaktype_node_id integer NOT NULL,
    milestone integer NOT NULL,
    contactkanaal character varying(128) NOT NULL,
    aanvraag_trigger public.zaaksysteem_trigger NOT NULL,
    onderwerp text,
    resultaat text,
    besluit text,
    coordinator integer,
    behandelaar integer,
    aanvrager integer NOT NULL,
    route_ou integer,
    route_role integer,
    locatie_zaak integer,
    locatie_correspondentie integer,
    streefafhandeldatum timestamp(6) without time zone,
    registratiedatum timestamp(6) without time zone NOT NULL,
    afhandeldatum timestamp(6) without time zone,
    vernietigingsdatum timestamp(6) without time zone,
    created timestamp(6) without time zone NOT NULL,
    last_modified timestamp(6) without time zone NOT NULL,
    deleted timestamp(6) without time zone,
    vervolg_van integer,
    aanvrager_gm_id integer,
    behandelaar_gm_id integer,
    coordinator_gm_id integer,
    uuid uuid,
    payment_status text,
    payment_amount numeric(100,2),
    hstore_properties public.hstore,
    confidentiality public.confidentiality DEFAULT 'public'::public.confidentiality NOT NULL,
    stalled_until timestamp without time zone,
    onderwerp_extern text,
    archival_state text,
    status text NOT NULL,
    duplicate_prevention_token uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    resultaat_id integer,
    urgency text,
    CONSTRAINT archival_state_value CHECK ((archival_state = ANY (ARRAY['overdragen'::text, 'vernietigen'::text]))),
    CONSTRAINT status_value CHECK ((status = ANY (ARRAY['new'::text, 'open'::text, 'stalled'::text, 'resolved'::text, 'deleted'::text])))
)
INHERITS (public.searchable);


--
-- Name: zaak_authorisation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_authorisation (
    id integer NOT NULL,
    zaak_id integer NOT NULL,
    capability text NOT NULL,
    entity_id text NOT NULL,
    entity_type text NOT NULL,
    scope text NOT NULL
);


--
-- Name: zaak_authorisation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_authorisation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_authorisation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_authorisation_id_seq OWNED BY public.zaak_authorisation.id;


--
-- Name: zaak_bag; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_bag (
    id integer NOT NULL,
    pid integer,
    zaak_id integer,
    bag_type public.zaaksysteem_bag_types,
    bag_id character varying(255),
    bag_verblijfsobject_id character varying(255),
    bag_openbareruimte_id character varying(255),
    bag_nummeraanduiding_id character varying(255),
    bag_pand_id character varying(255),
    bag_standplaats_id character varying(255),
    bag_ligplaats_id character varying(255),
    bag_coordinates_wsg point
);


--
-- Name: zaak_bag_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_bag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_bag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_bag_id_seq OWNED BY public.zaak_bag.id;


--
-- Name: zaak_betrokkenen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_betrokkenen (
    id integer NOT NULL,
    zaak_id integer,
    betrokkene_type character varying(128),
    betrokkene_id integer,
    gegevens_magazijn_id integer,
    verificatie character varying(128),
    naam character varying(255),
    rol text,
    magic_string_prefix text,
    deleted timestamp without time zone,
    uuid uuid DEFAULT public.uuid_generate_v4(),
    pip_authorized boolean DEFAULT false NOT NULL,
    subject_id uuid
);


--
-- Name: zaak_betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_betrokkenen_id_seq OWNED BY public.zaak_betrokkenen.id;


--
-- Name: zaak_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_id_seq OWNED BY public.zaak.id;


--
-- Name: zaak_kenmerk; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_kenmerk (
    zaak_id integer NOT NULL,
    bibliotheek_kenmerken_id integer NOT NULL,
    id integer NOT NULL,
    value text[] NOT NULL
);


--
-- Name: zaak_kenmerk_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_kenmerk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_kenmerk_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_kenmerk_id_seq OWNED BY public.zaak_kenmerk.id;


--
-- Name: zaak_meta; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_meta (
    id integer NOT NULL,
    zaak_id integer,
    verlenging character varying(255),
    opschorten character varying(255),
    deel character varying(255),
    gerelateerd character varying(255),
    vervolg character varying(255),
    afhandeling character varying(255),
    stalled_since timestamp without time zone
);


--
-- Name: zaak_meta_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_meta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_meta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_meta_id_seq OWNED BY public.zaak_meta.id;


--
-- Name: zaak_onafgerond; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_onafgerond (
    zaaktype_id integer NOT NULL,
    betrokkene character(50) NOT NULL,
    json_string text NOT NULL,
    afronden boolean,
    create_unixtime integer
);


--
-- Name: zaak_subcase; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaak_subcase (
    id integer NOT NULL,
    zaak_id integer NOT NULL,
    relation_zaak_id integer,
    required integer
);


--
-- Name: zaak_subcase_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaak_subcase_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaak_subcase_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaak_subcase_id_seq OWNED BY public.zaak_subcase.id;


--
-- Name: zaaktype; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype (
    search_index tsvector,
    search_term text,
    object_type text DEFAULT 'zaaktype'::character varying,
    id integer NOT NULL,
    zaaktype_node_id integer,
    version integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    bibliotheek_categorie_id integer,
    active boolean DEFAULT true NOT NULL
)
INHERITS (public.searchable);


--
-- Name: zaaktype_authorisation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_authorisation (
    id integer NOT NULL,
    zaaktype_node_id integer,
    recht text NOT NULL,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    role_id integer,
    ou_id integer,
    zaaktype_id integer,
    confidential boolean DEFAULT false NOT NULL,
    CONSTRAINT zaaktype_authorisation_recht CHECK ((recht = ANY (ARRAY['zaak_beheer'::text, 'zaak_edit'::text, 'zaak_read'::text, 'zaak_search'::text])))
);


--
-- Name: zaaktype_authorisation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_authorisation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_authorisation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_authorisation_id_seq OWNED BY public.zaaktype_authorisation.id;


--
-- Name: zaaktype_betrokkenen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_betrokkenen (
    id integer NOT NULL,
    zaaktype_node_id integer,
    betrokkene_type text,
    created timestamp without time zone,
    last_modified timestamp without time zone
);


--
-- Name: zaaktype_betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_betrokkenen_id_seq OWNED BY public.zaaktype_betrokkenen.id;


--
-- Name: zaaktype_definitie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_definitie (
    id integer NOT NULL,
    openbaarheid character varying(255),
    handelingsinitiator character varying(255),
    grondslag text,
    procesbeschrijving character varying(255),
    afhandeltermijn character varying(255),
    afhandeltermijn_type character varying(255),
    selectielijst character varying(255),
    servicenorm character varying(255),
    servicenorm_type character varying(255),
    pdc_voorwaarden text,
    pdc_description text,
    pdc_meenemen text,
    pdc_tarief text,
    omschrijving_upl character varying(255),
    aard character varying(255),
    extra_informatie text,
    preset_client character varying(255),
    extra_informatie_extern text
);


--
-- Name: zaaktype_definitie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_definitie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_definitie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_definitie_id_seq OWNED BY public.zaaktype_definitie.id;


--
-- Name: zaaktype_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_id_seq OWNED BY public.zaaktype.id;


--
-- Name: zaaktype_kenmerken; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_kenmerken (
    id integer NOT NULL,
    bibliotheek_kenmerken_id integer,
    value_mandatory integer,
    label text,
    help text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    zaaktype_node_id integer,
    zaak_status_id integer,
    pip integer,
    zaakinformatie_view integer DEFAULT 1,
    bag_zaakadres integer,
    value_default text,
    pip_can_change boolean,
    publish_public integer,
    referential character varying(12),
    is_systeemkenmerk boolean DEFAULT false,
    required_permissions text,
    version integer,
    help_extern text,
    object_id uuid,
    object_metadata text DEFAULT '{}'::text NOT NULL,
    label_multiple text,
    properties text DEFAULT '{}'::text,
    is_group boolean DEFAULT false NOT NULL
);


--
-- Name: zaaktype_kenmerken_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_kenmerken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_kenmerken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_kenmerken_id_seq OWNED BY public.zaaktype_kenmerken.id;


--
-- Name: zaaktype_node; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_node (
    id integer NOT NULL,
    zaaktype_id integer,
    zaaktype_rt_queue text,
    code text,
    trigger text,
    titel character varying(128),
    version integer,
    active integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    webform_toegang integer,
    webform_authenticatie text,
    adres_relatie text,
    aanvrager_hergebruik integer,
    automatisch_aanvragen integer,
    automatisch_behandelen integer,
    toewijzing_zaakintake integer,
    toelichting character varying(128),
    online_betaling integer,
    zaaktype_definitie_id integer,
    adres_andere_locatie integer,
    adres_aanvrager integer,
    bedrijfid_wijzigen integer,
    zaaktype_vertrouwelijk integer,
    zaaktype_trefwoorden text,
    zaaktype_omschrijving text,
    extra_relaties_in_aanvraag boolean,
    properties text DEFAULT '{}'::text NOT NULL,
    contact_info_intake boolean DEFAULT true,
    is_public boolean DEFAULT false,
    prevent_pip boolean DEFAULT false,
    contact_info_email_required boolean DEFAULT false,
    contact_info_phone_required boolean DEFAULT false,
    contact_info_mobile_phone_required boolean DEFAULT false,
    moeder_zaaktype_id integer,
    logging_id integer,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


--
-- Name: zaaktype_node_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_node_id_seq OWNED BY public.zaaktype_node.id;


--
-- Name: zaaktype_notificatie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_notificatie (
    id integer NOT NULL,
    zaaktype_node_id integer,
    zaak_status_id integer,
    label text,
    rcpt text,
    onderwerp text,
    bericht text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    intern_block integer,
    email text,
    bibliotheek_notificaties_id integer,
    behandelaar character varying(255),
    automatic integer,
    cc text,
    bcc text,
    betrokkene_role text
);


--
-- Name: zaaktype_notificatie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_notificatie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_notificatie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_notificatie_id_seq OWNED BY public.zaaktype_notificatie.id;


--
-- Name: zaaktype_regel; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_regel (
    id integer NOT NULL,
    zaaktype_node_id integer,
    zaak_status_id integer,
    naam text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    settings text,
    active boolean DEFAULT true,
    is_group boolean DEFAULT false NOT NULL
);


--
-- Name: zaaktype_regel_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_regel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_regel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_regel_id_seq OWNED BY public.zaaktype_regel.id;


--
-- Name: zaaktype_relatie; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_relatie (
    id integer NOT NULL,
    zaaktype_node_id integer,
    relatie_zaaktype_id integer,
    zaaktype_status_id integer,
    relatie_type text,
    eigenaar_type text DEFAULT 'aanvrager'::text NOT NULL,
    start_delay character varying(255),
    created timestamp without time zone,
    last_modified timestamp without time zone,
    status integer,
    kopieren_kenmerken integer,
    ou_id integer,
    role_id integer,
    automatisch_behandelen boolean,
    required character varying(12),
    betrokkene_authorized boolean,
    betrokkene_notify boolean,
    betrokkene_id text,
    betrokkene_role text,
    betrokkene_role_set text,
    betrokkene_prefix text,
    eigenaar_id text,
    eigenaar_role text,
    show_in_pip boolean DEFAULT false NOT NULL,
    pip_label text,
    subject_role text[],
    copy_subject_role boolean DEFAULT false
);


--
-- Name: zaaktype_relatie_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_relatie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_relatie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_relatie_id_seq OWNED BY public.zaaktype_relatie.id;


--
-- Name: zaaktype_resultaten; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_resultaten (
    id integer NOT NULL,
    zaaktype_node_id integer,
    zaaktype_status_id integer,
    resultaat text,
    ingang text,
    bewaartermijn integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    dossiertype character varying(50),
    label text,
    selectielijst text,
    archiefnominatie text,
    comments text,
    external_reference text,
    trigger_archival boolean DEFAULT true NOT NULL,
    selectielijst_brondatum date,
    selectielijst_einddatum date,
    properties text DEFAULT '{}'::text NOT NULL,
    standaard_keuze boolean DEFAULT false NOT NULL
);


--
-- Name: zaaktype_resultaten_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_resultaten_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_resultaten_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_resultaten_id_seq OWNED BY public.zaaktype_resultaten.id;


--
-- Name: zaaktype_sjablonen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_sjablonen (
    id integer NOT NULL,
    zaaktype_node_id integer,
    bibliotheek_sjablonen_id integer,
    help text,
    zaak_status_id integer,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    automatisch_genereren integer,
    bibliotheek_kenmerken_id integer,
    target_format character varying(5)
);


--
-- Name: zaaktype_sjablonen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_sjablonen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_sjablonen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_sjablonen_id_seq OWNED BY public.zaaktype_sjablonen.id;


--
-- Name: zaaktype_standaard_betrokkenen; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_standaard_betrokkenen (
    id integer NOT NULL,
    zaaktype_node_id integer NOT NULL,
    zaak_status_id integer NOT NULL,
    betrokkene_type text NOT NULL,
    betrokkene_identifier text NOT NULL,
    naam text NOT NULL,
    rol text NOT NULL,
    magic_string_prefix text,
    gemachtigd boolean DEFAULT false NOT NULL,
    notify boolean DEFAULT false NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4()
);


--
-- Name: zaaktype_standaard_betrokkenen_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_standaard_betrokkenen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_standaard_betrokkenen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_standaard_betrokkenen_id_seq OWNED BY public.zaaktype_standaard_betrokkenen.id;


--
-- Name: zaaktype_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_status (
    id integer NOT NULL,
    zaaktype_node_id integer,
    status integer,
    status_type text,
    naam text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    ou_id integer,
    role_id integer,
    checklist integer,
    fase character varying(255),
    role_set integer
);


--
-- Name: zaaktype_status_checklist_item; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zaaktype_status_checklist_item (
    id integer NOT NULL,
    casetype_status_id integer NOT NULL,
    label text,
    external_reference text
);


--
-- Name: zaaktype_status_checklist_item_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_status_checklist_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_status_checklist_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_status_checklist_item_id_seq OWNED BY public.zaaktype_status_checklist_item.id;


--
-- Name: zaaktype_status_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zaaktype_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zaaktype_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zaaktype_status_id_seq OWNED BY public.zaaktype_status.id;


--
-- Name: zorginstituut_identificatie_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zorginstituut_identificatie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adres id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.adres ALTER COLUMN id SET DEFAULT nextval('public.adres_id_seq'::regclass);


--
-- Name: bag_ligplaats id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_ligplaats ALTER COLUMN id SET DEFAULT nextval('public.bag_ligplaats_id_seq'::regclass);


--
-- Name: bag_nummeraanduiding id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_nummeraanduiding ALTER COLUMN id SET DEFAULT nextval('public.bag_nummeraanduiding_id_seq'::regclass);


--
-- Name: bag_openbareruimte id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_openbareruimte ALTER COLUMN id SET DEFAULT nextval('public.bag_openbareruimte_id_seq'::regclass);


--
-- Name: bag_pand id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_pand ALTER COLUMN id SET DEFAULT nextval('public.bag_pand_id_seq'::regclass);


--
-- Name: bag_standplaats id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_standplaats ALTER COLUMN id SET DEFAULT nextval('public.bag_standplaats_id_seq'::regclass);


--
-- Name: bag_verblijfsobject id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject ALTER COLUMN id SET DEFAULT nextval('public.bag_verblijfsobject_id_seq'::regclass);


--
-- Name: bag_verblijfsobject_gebruiksdoel id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject_gebruiksdoel ALTER COLUMN id SET DEFAULT nextval('public.bag_verblijfsobject_gebruiksdoel_id_seq'::regclass);


--
-- Name: bag_verblijfsobject_pand id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject_pand ALTER COLUMN id SET DEFAULT nextval('public.bag_verblijfsobject_pand_id_seq'::regclass);


--
-- Name: bag_woonplaats id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_woonplaats ALTER COLUMN id SET DEFAULT nextval('public.bag_woonplaats_id_seq'::regclass);


--
-- Name: bedrijf searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: bedrijf id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf ALTER COLUMN id SET DEFAULT nextval('public.bedrijf_id_seq'::regclass);


--
-- Name: bedrijf_authenticatie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf_authenticatie ALTER COLUMN id SET DEFAULT nextval('public.bedrijf_authenticatie_id_seq'::regclass);


--
-- Name: beheer_import id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.beheer_import ALTER COLUMN id SET DEFAULT nextval('public.beheer_import_id_seq'::regclass);


--
-- Name: beheer_import_log id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.beheer_import_log ALTER COLUMN id SET DEFAULT nextval('public.beheer_import_log_id_seq'::regclass);


--
-- Name: betrokkene_notes id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.betrokkene_notes ALTER COLUMN id SET DEFAULT nextval('public.betrokkene_notes_id_seq'::regclass);


--
-- Name: betrokkenen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.betrokkenen ALTER COLUMN id SET DEFAULT nextval('public.betrokkenen_id_seq'::regclass);


--
-- Name: bibliotheek_categorie searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_categorie ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: bibliotheek_categorie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_categorie ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_categorie_id_seq'::regclass);


--
-- Name: bibliotheek_kenmerken searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: bibliotheek_kenmerken id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_kenmerken_id_seq'::regclass);


--
-- Name: bibliotheek_kenmerken_values id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken_values ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_kenmerken_values_id_seq'::regclass);


--
-- Name: bibliotheek_kenmerken_values sort_order; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken_values ALTER COLUMN sort_order SET DEFAULT nextval('public.bibliotheek_kenmerken_values_sort_order_seq'::regclass);


--
-- Name: bibliotheek_notificatie_kenmerk id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificatie_kenmerk ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_notificatie_kenmerk_id_seq'::regclass);


--
-- Name: bibliotheek_notificaties searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificaties ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: bibliotheek_notificaties id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificaties ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_notificaties_id_seq'::regclass);


--
-- Name: bibliotheek_sjablonen searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: bibliotheek_sjablonen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_sjablonen_id_seq'::regclass);


--
-- Name: bibliotheek_sjablonen_magic_string id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen_magic_string ALTER COLUMN id SET DEFAULT nextval('public.bibliotheek_sjablonen_magic_string_id_seq'::regclass);


--
-- Name: case_action id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_action ALTER COLUMN id SET DEFAULT nextval('public.case_action_id_seq'::regclass);


--
-- Name: case_relation id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_relation ALTER COLUMN id SET DEFAULT nextval('public.case_relation_id_seq'::regclass);


--
-- Name: checklist id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist ALTER COLUMN id SET DEFAULT nextval('public.checklist_id_seq'::regclass);


--
-- Name: checklist_item id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist_item ALTER COLUMN id SET DEFAULT nextval('public.checklist_item_id_seq'::regclass);


--
-- Name: config id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config ALTER COLUMN id SET DEFAULT nextval('public.config_id_seq'::regclass);


--
-- Name: contact_data id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contact_data ALTER COLUMN id SET DEFAULT nextval('public.contact_data_id_seq'::regclass);


--
-- Name: contactmoment id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment ALTER COLUMN id SET DEFAULT nextval('public.contactmoment_id_seq'::regclass);


--
-- Name: contactmoment_email id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_email ALTER COLUMN id SET DEFAULT nextval('public.contactmoment_email_id_seq'::regclass);


--
-- Name: contactmoment_note id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_note ALTER COLUMN id SET DEFAULT nextval('public.contactmoment_note_id_seq'::regclass);


--
-- Name: directory id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.directory ALTER COLUMN id SET DEFAULT nextval('public.directory_id_seq'::regclass);


--
-- Name: file id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file ALTER COLUMN id SET DEFAULT nextval('public.file_id_seq'::regclass);


--
-- Name: file_case_document id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_case_document ALTER COLUMN id SET DEFAULT nextval('public.file_case_document_id_seq'::regclass);


--
-- Name: file_metadata id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_metadata ALTER COLUMN id SET DEFAULT nextval('public.file_metadata_id_seq'::regclass);


--
-- Name: filestore id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.filestore ALTER COLUMN id SET DEFAULT nextval('public.filestore_id_seq'::regclass);


--
-- Name: gm_adres id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_adres ALTER COLUMN id SET DEFAULT nextval('public.gm_adres_id_seq'::regclass);


--
-- Name: gm_bedrijf id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_bedrijf ALTER COLUMN id SET DEFAULT nextval('public.gm_bedrijf_id_seq'::regclass);


--
-- Name: gm_natuurlijk_persoon id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_natuurlijk_persoon ALTER COLUMN id SET DEFAULT nextval('public.gm_natuurlijk_persoon_id_seq'::regclass);


--
-- Name: interface id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.interface ALTER COLUMN id SET DEFAULT nextval('public.interface_id_seq'::regclass);


--
-- Name: logging id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.logging ALTER COLUMN id SET DEFAULT nextval('public.logging_id_seq'::regclass);


--
-- Name: message id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message ALTER COLUMN id SET DEFAULT nextval('public.message_id_seq'::regclass);


--
-- Name: natuurlijk_persoon searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: natuurlijk_persoon id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon ALTER COLUMN id SET DEFAULT nextval('public.natuurlijk_persoon_id_seq'::regclass);


--
-- Name: object_bibliotheek_entry id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_bibliotheek_entry ALTER COLUMN id SET DEFAULT nextval('public.object_bibliotheek_entry_id_seq'::regclass);


--
-- Name: object_subscription id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_subscription ALTER COLUMN id SET DEFAULT nextval('public.object_subscription_id_seq'::regclass);


--
-- Name: parkeergebied id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.parkeergebied ALTER COLUMN id SET DEFAULT nextval('public.parkeergebied_id_seq'::regclass);


--
-- Name: remote_api_keys id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.remote_api_keys ALTER COLUMN id SET DEFAULT nextval('public.remote_api_keys_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: sbus_logging id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_logging ALTER COLUMN id SET DEFAULT nextval('public.sbus_logging_id_seq'::regclass);


--
-- Name: sbus_traffic id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_traffic ALTER COLUMN id SET DEFAULT nextval('public.sbus_traffic_id_seq'::regclass);


--
-- Name: scheduled_jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_jobs ALTER COLUMN id SET DEFAULT nextval('public.scheduled_jobs_id_seq'::regclass);


--
-- Name: search_query id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.search_query ALTER COLUMN id SET DEFAULT nextval('public.search_query_id_seq'::regclass);


--
-- Name: search_query_delen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.search_query_delen ALTER COLUMN id SET DEFAULT nextval('public.search_query_delen_id_seq'::regclass);


--
-- Name: searchable searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.searchable ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: settings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.settings ALTER COLUMN id SET DEFAULT nextval('public.settings_id_seq'::regclass);


--
-- Name: subject id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject ALTER COLUMN id SET DEFAULT nextval('public.subject_id_seq'::regclass);


--
-- Name: transaction id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction ALTER COLUMN id SET DEFAULT nextval('public.transaction_id_seq'::regclass);


--
-- Name: transaction_record id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record ALTER COLUMN id SET DEFAULT nextval('public.transaction_record_id_seq'::regclass);


--
-- Name: transaction_record_to_object id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record_to_object ALTER COLUMN id SET DEFAULT nextval('public.transaction_record_to_object_id_seq'::regclass);


--
-- Name: user_entity id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_entity ALTER COLUMN id SET DEFAULT nextval('public.user_entity_id_seq'::regclass);


--
-- Name: woz_objects id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.woz_objects ALTER COLUMN id SET DEFAULT nextval('public.woz_objects_id_seq'::regclass);


--
-- Name: zaak searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: zaak id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak ALTER COLUMN id SET DEFAULT nextval('public.zaak_id_seq'::regclass);


--
-- Name: zaak_authorisation id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_authorisation ALTER COLUMN id SET DEFAULT nextval('public.zaak_authorisation_id_seq'::regclass);


--
-- Name: zaak_bag id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_bag ALTER COLUMN id SET DEFAULT nextval('public.zaak_bag_id_seq'::regclass);


--
-- Name: zaak_betrokkenen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_betrokkenen ALTER COLUMN id SET DEFAULT nextval('public.zaak_betrokkenen_id_seq'::regclass);


--
-- Name: zaak_kenmerk id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_kenmerk ALTER COLUMN id SET DEFAULT nextval('public.zaak_kenmerk_id_seq'::regclass);


--
-- Name: zaak_meta id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_meta ALTER COLUMN id SET DEFAULT nextval('public.zaak_meta_id_seq'::regclass);


--
-- Name: zaak_subcase id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_subcase ALTER COLUMN id SET DEFAULT nextval('public.zaak_subcase_id_seq'::regclass);


--
-- Name: zaaktype searchable_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype ALTER COLUMN searchable_id SET DEFAULT nextval('public.searchable_searchable_id_seq'::regclass);


--
-- Name: zaaktype id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_id_seq'::regclass);


--
-- Name: zaaktype_authorisation id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_authorisation ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_authorisation_id_seq'::regclass);


--
-- Name: zaaktype_betrokkenen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_betrokkenen ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_betrokkenen_id_seq'::regclass);


--
-- Name: zaaktype_definitie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_definitie ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_definitie_id_seq'::regclass);


--
-- Name: zaaktype_kenmerken id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_kenmerken_id_seq'::regclass);


--
-- Name: zaaktype_node id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_node_id_seq'::regclass);


--
-- Name: zaaktype_notificatie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_notificatie ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_notificatie_id_seq'::regclass);


--
-- Name: zaaktype_regel id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_regel ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_regel_id_seq'::regclass);


--
-- Name: zaaktype_relatie id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_relatie ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_relatie_id_seq'::regclass);


--
-- Name: zaaktype_resultaten id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_resultaten ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_resultaten_id_seq'::regclass);


--
-- Name: zaaktype_sjablonen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_sjablonen_id_seq'::regclass);


--
-- Name: zaaktype_standaard_betrokkenen id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_standaard_betrokkenen ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_standaard_betrokkenen_id_seq'::regclass);


--
-- Name: zaaktype_status id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_status_id_seq'::regclass);


--
-- Name: zaaktype_status_checklist_item id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status_checklist_item ALTER COLUMN id SET DEFAULT nextval('public.zaaktype_status_checklist_item_id_seq'::regclass);


--
-- Data for Name: adres; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.adres (id, straatnaam, huisnummer, huisletter, huisnummertoevoeging, nadere_aanduiding, postcode, woonplaats, gemeentedeel, functie_adres, datum_aanvang_bewoning, woonplaats_id, gemeente_code, hash, import_datum, deleted_on, adres_buitenland1, adres_buitenland2, adres_buitenland3, landcode, natuurlijk_persoon_id) FROM stdin;
1	Jimlaan	42	X	\N	\N	1234AB	Hilversum	\N	W	\N	\N	\N	\N	2013-01-04 10:33:38	\N	\N	\N	\N	6030	\N
\.


--
-- Name: adres_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.adres_id_seq', 1, true);


--
-- Data for Name: alternative_authentication_activation_link; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.alternative_authentication_activation_link (token, subject_id, expires) FROM stdin;
\.


--
-- Data for Name: bag_ligplaats; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_ligplaats (identificatie, begindatum, einddatum, officieel, status, hoofdadres, inonderzoek, documentdatum, documentnummer, correctie, id) FROM stdin;
\.


--
-- Name: bag_ligplaats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_ligplaats_id_seq', 1, false);


--
-- Data for Name: bag_ligplaats_nevenadres; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_ligplaats_nevenadres (identificatie, begindatum, nevenadres, correctie) FROM stdin;
\.


--
-- Data for Name: bag_nummeraanduiding; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_nummeraanduiding (identificatie, begindatum, einddatum, huisnummer, officieel, huisletter, huisnummertoevoeging, postcode, woonplaats, inonderzoek, openbareruimte, type, documentdatum, documentnummer, status, correctie, id, gps_lat_lon) FROM stdin;
\.


--
-- Name: bag_nummeraanduiding_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_nummeraanduiding_id_seq', 1, false);


--
-- Data for Name: bag_openbareruimte; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_openbareruimte (identificatie, begindatum, einddatum, naam, officieel, woonplaats, type, inonderzoek, documentdatum, documentnummer, status, correctie, id, gps_lat_lon) FROM stdin;
\.


--
-- Name: bag_openbareruimte_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_openbareruimte_id_seq', 1, false);


--
-- Data for Name: bag_pand; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_pand (identificatie, begindatum, einddatum, officieel, bouwjaar, status, inonderzoek, documentdatum, documentnummer, correctie, id) FROM stdin;
\.


--
-- Name: bag_pand_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_pand_id_seq', 1, false);


--
-- Data for Name: bag_standplaats; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_standplaats (identificatie, begindatum, einddatum, officieel, status, hoofdadres, inonderzoek, documentdatum, documentnummer, correctie, id) FROM stdin;
\.


--
-- Name: bag_standplaats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_standplaats_id_seq', 1, false);


--
-- Data for Name: bag_verblijfsobject; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_verblijfsobject (identificatie, begindatum, einddatum, officieel, hoofdadres, oppervlakte, status, inonderzoek, documentdatum, documentnummer, correctie, id) FROM stdin;
\.


--
-- Data for Name: bag_verblijfsobject_gebruiksdoel; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_verblijfsobject_gebruiksdoel (identificatie, begindatum, gebruiksdoel, correctie, id) FROM stdin;
\.


--
-- Name: bag_verblijfsobject_gebruiksdoel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_verblijfsobject_gebruiksdoel_id_seq', 1, false);


--
-- Name: bag_verblijfsobject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_verblijfsobject_id_seq', 1, false);


--
-- Data for Name: bag_verblijfsobject_pand; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_verblijfsobject_pand (identificatie, begindatum, pand, correctie, id) FROM stdin;
\.


--
-- Name: bag_verblijfsobject_pand_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_verblijfsobject_pand_id_seq', 1, false);


--
-- Data for Name: bag_woonplaats; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bag_woonplaats (identificatie, begindatum, einddatum, officieel, naam, status, inonderzoek, documentdatum, documentnummer, correctie, id) FROM stdin;
\.


--
-- Name: bag_woonplaats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bag_woonplaats_id_seq', 1, false);


--
-- Data for Name: bedrijf; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bedrijf (search_index, search_term, object_type, searchable_id, search_order, id, dossiernummer, subdossiernummer, hoofdvestiging_dossiernummer, hoofdvestiging_subdossiernummer, vorig_dossiernummer, vorig_subdossiernummer, handelsnaam, rechtsvorm, kamernummer, faillisement, surseance, telefoonnummer, email, vestiging_adres, vestiging_straatnaam, vestiging_huisnummer, vestiging_huisnummertoevoeging, vestiging_postcodewoonplaats, vestiging_postcode, vestiging_woonplaats, correspondentie_adres, correspondentie_straatnaam, correspondentie_huisnummer, correspondentie_huisnummertoevoeging, correspondentie_postcodewoonplaats, correspondentie_postcode, correspondentie_woonplaats, hoofdactiviteitencode, nevenactiviteitencode1, nevenactiviteitencode2, werkzamepersonen, contact_naam, contact_aanspreektitel, contact_voorletters, contact_voorvoegsel, contact_geslachtsnaam, contact_geslachtsaanduiding, authenticated, authenticatedby, fulldossiernummer, import_datum, deleted_on, verblijfsobject_id, system_of_record, system_of_record_id, vestigingsnummer, vestiging_huisletter, correspondentie_huisletter, vestiging_adres_buitenland1, vestiging_adres_buitenland2, vestiging_adres_buitenland3, vestiging_landcode, correspondentie_adres_buitenland1, correspondentie_adres_buitenland2, correspondentie_adres_buitenland3, correspondentie_landcode, uuid) FROM stdin;
\.


--
-- Data for Name: bedrijf_authenticatie; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bedrijf_authenticatie (id, gegevens_magazijn_id, login, password, created, last_modified) FROM stdin;
\.


--
-- Name: bedrijf_authenticatie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bedrijf_authenticatie_id_seq', 1, false);


--
-- Name: bedrijf_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bedrijf_id_seq', 1, false);


--
-- Data for Name: beheer_import; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.beheer_import (id, importtype, succesvol, finished, import_create, import_update, error, error_message, entries, created, last_modified) FROM stdin;
\.


--
-- Name: beheer_import_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.beheer_import_id_seq', 1, false);


--
-- Data for Name: beheer_import_log; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.beheer_import_log (id, import_id, old_data, new_data, created, last_modified, kolom, identifier, action) FROM stdin;
\.


--
-- Name: beheer_import_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.beheer_import_log_id_seq', 1, false);


--
-- Data for Name: betrokkene_notes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.betrokkene_notes (id, betrokkene_exid, betrokkene_type, betrokkene_from, ntype, subject, message, created, last_modified) FROM stdin;
\.


--
-- Name: betrokkene_notes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.betrokkene_notes_id_seq', 1, false);


--
-- Data for Name: betrokkenen; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.betrokkenen (id, btype, gm_natuurlijk_persoon_id, naam) FROM stdin;
\.


--
-- Name: betrokkenen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.betrokkenen_id_seq', 1, false);


--
-- Data for Name: bibliotheek_categorie; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bibliotheek_categorie (search_index, search_term, object_type, searchable_id, search_order, id, naam, label, description, help, created, last_modified, system, pid, uuid) FROM stdin;
\N	Testing	bibliotheek_categorie	11	\N	2	Testing	\N	\N	\N	\N	\N	\N	\N	4e03e59d-42e7-4904-823d-4adb9628f95c
\.


--
-- Name: bibliotheek_categorie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_categorie_id_seq', 2, true);


--
-- Data for Name: bibliotheek_kenmerken; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bibliotheek_kenmerken (search_index, search_term, object_type, searchable_id, search_order, id, naam, value_type, value_default, label, description, help, magic_string, created, last_modified, bibliotheek_categorie_id, document_categorie, system, deleted, file_metadata_id, version, properties, naam_public, type_multiple, uuid) FROM stdin;
\N	Test 01	bibliotheek_kenmerken	12	\N	1	Test 01	checkbox		\N	\N		test_01	\N	2019-05-27 08:18:41.077287	2	Advies	\N	\N	\N	1	{}		f	64863a69-beb8-43bf-a393-2ffb7f79314b
\N	Test 02	bibliotheek_kenmerken	13	\N	2	Test 02	date		\N	\N		test_02	\N	2019-05-27 08:18:41.077287	2	Advies	\N	\N	\N	1	{}		f	f64beb98-28d0-405f-bd9d-83e3faf6d027
\N	Test 03	bibliotheek_kenmerken	14	\N	3	Test 03	option		\N	\N		test_03	\N	2019-05-27 08:18:41.077287	2	Advies	\N	\N	\N	1	{}		f	291149c2-c578-4a09-ac8a-751437340175
\N	Omschrijving	bibliotheek_kenmerken	15	\N	4	Omschrijving	textarea		\N	\N		omschrijving	\N	2019-05-27 08:18:41.077287	2	Advies	\N	\N	\N	1	{}		f	37f916aa-6518-47b6-9847-15d6b6aef5cf
\N	Test 04	bibliotheek_kenmerken	16	\N	5	Test 04	numeric		\N	\N		test_04	\N	2019-05-27 08:18:41.077287	2	Advies	\N	\N	\N	1	{}		f	37af65a9-f72a-4fd6-8423-17d2efdae16b
\N	Test 05	bibliotheek_kenmerken	17	\N	6	Test 05	file		\N	\N		test_05	\N	2019-05-27 08:18:41.077287	2	Bewijsstuk	\N	\N	\N	1	{}		f	06d2e410-7df0-4c30-a169-31ce6c47b515
\N	Test 06	bibliotheek_kenmerken	18	\N	7	Test 06	file		\N	\N		test_06	\N	2019-05-27 08:18:41.077287	2	Bewijsstuk	\N	\N	\N	1	{}		f	42530286-3247-47d5-85d0-f2754be74ba1
\N	Test 07	bibliotheek_kenmerken	19	\N	8	Test 07	option		\N	\N		test_07	\N	2019-05-27 08:18:41.077287	2	Advies	\N	\N	\N	1	{}		f	41ddaf40-4b9f-4087-9897-482e57fa29e5
\N	Test 08	bibliotheek_kenmerken	23	\N	9	Test 08	text		\N	\N		test_08	\N	2019-05-27 08:18:41.077287	2	Advies	\N	\N	\N	1	{}		f	fe8cbbf4-64f8-4a6b-8052-f6d6a4e7d552
\N	Test 09	bibliotheek_kenmerken	25	\N	10	Test 09	option		\N	\N		test_09	\N	2019-05-27 08:18:41.077287	2	Advies	\N	\N	\N	1	{}		f	dcf3b26f-e83f-41ca-916f-2e27098ab391
\N	Toelichting	bibliotheek_kenmerken	26	\N	11	Toelichting	textarea		\N	\N		toelichting	\N	2019-05-27 08:18:41.077287	2	Advies	\N	\N	\N	1	{}		f	13ed46dc-951a-40fe-ab11-c46a9c33f4a2
\N	Test A	bibliotheek_kenmerken	29	\N	12	Test A	file		\N	\N		test_a	\N	2019-05-27 08:18:41.077287	2	Bewijsstuk	\N	\N	\N	1	{}		f	75cd304c-206c-468a-aef3-864a7beebca4
\.


--
-- Name: bibliotheek_kenmerken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_kenmerken_id_seq', 12, true);


--
-- Data for Name: bibliotheek_kenmerken_values; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bibliotheek_kenmerken_values (id, bibliotheek_kenmerken_id, value, active, sort_order) FROM stdin;
1	1	Optie 1	t	1
2	1	Optie 2	t	2
3	1	Optie 3	t	3
4	3	Optie A	t	4
5	3	Optie B	t	5
6	8	Ja	t	6
7	8	Nee	t	7
8	10	Ja	t	8
9	10	Nee	t	9
\.


--
-- Name: bibliotheek_kenmerken_values_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_kenmerken_values_id_seq', 9, true);


--
-- Name: bibliotheek_kenmerken_values_sort_order_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_kenmerken_values_sort_order_seq', 9, true);


--
-- Data for Name: bibliotheek_notificatie_kenmerk; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bibliotheek_notificatie_kenmerk (id, bibliotheek_notificatie_id, bibliotheek_kenmerken_id) FROM stdin;
\.


--
-- Name: bibliotheek_notificatie_kenmerk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_notificatie_kenmerk_id_seq', 1, false);


--
-- Data for Name: bibliotheek_notificaties; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bibliotheek_notificaties (search_index, search_term, object_type, searchable_id, search_order, id, bibliotheek_categorie_id, label, subject, message, created, last_modified, deleted, sender, sender_address, uuid) FROM stdin;
\N	OntvangstbevestigingOntvangstbevestigingGeachte meneer/mevrouw [[aanvrager_achternaam]],\r\n\r\nOp [[registratiedatum]] heeft u een aanvraag ontvangen voor een [[zaaktype]] gedaan. Bij deze bevestigen wij de ontvangst van uw aanvraag, welke bij ons geregistreerd staat onder zaaknummer [[zaaknummer]]. In geval van correspondentie verzoeken wij u dit zaaknummer te gebruiken.\r\n\r\nWij streven er naar om uw aanvraag uiterlijk [[streefafhandeldatum]] te hebben afgehandeld. Tijdens de behandeling van uw aanvraag houden wij u op de hoogte van de voortgang.\r\n\r\nMet vriendelijke groet,\r\n\r\nHet testteam	bibliotheek_notificaties	21	\N	3	2	Ontvangstbevestiging	Ontvangstbevestiging	Geachte meneer/mevrouw [[aanvrager_achternaam]],\r\n\r\nOp [[registratiedatum]] heeft u een aanvraag ontvangen voor een [[zaaktype]] gedaan. Bij deze bevestigen wij de ontvangst van uw aanvraag, welke bij ons geregistreerd staat onder zaaknummer [[zaaknummer]]. In geval van correspondentie verzoeken wij u dit zaaknummer te gebruiken.\r\n\r\nWij streven er naar om uw aanvraag uiterlijk [[streefafhandeldatum]] te hebben afgehandeld. Tijdens de behandeling van uw aanvraag houden wij u op de hoogte van de voortgang.\r\n\r\nMet vriendelijke groet,\r\n\r\nHet testteam	\N	\N	\N	\N	\N	fd6cdbb3-463b-4be8-a5ef-565d76742ba8
\N	Beoordeling negatiefBeoordeling negatief zaaknummer [[zaaknummer]]Aan: behandelaar van zaaknummer [[zaaknummer]]\r\n\r\nIk heb deze zaak beoordeeld en besloten de zaak niet te accorderen. Neem contact op met de aanvrager.\r\n\r\nMet vriendelijke groet,\r\n\r\n[[beoordelaar_naam]]	bibliotheek_notificaties	22	\N	4	2	Beoordeling negatief	Beoordeling negatief zaaknummer [[zaaknummer]]	Aan: behandelaar van zaaknummer [[zaaknummer]]\r\n\r\nIk heb deze zaak beoordeeld en besloten de zaak niet te accorderen. Neem contact op met de aanvrager.\r\n\r\nMet vriendelijke groet,\r\n\r\n[[beoordelaar_naam]]	\N	\N	\N	\N	\N	56449126-de60-4c79-a631-7980a552c127
\N	Beoordeling positiefBeoordeling positief zaaknummer [[zaaknummer]]Aan: behandelaar van zaaknummer [[zaaknummer]]\r\n\r\nIk heb deze zaak beoordeeld en besloten de zaak te accorderen. Deze zaak mag worden afgehandeld.\r\n\r\nMet vriendelijke groet,\r\n\r\n[[beoordelaar_naam]]	bibliotheek_notificaties	24	\N	5	2	Beoordeling positief	Beoordeling positief zaaknummer [[zaaknummer]]	Aan: behandelaar van zaaknummer [[zaaknummer]]\r\n\r\nIk heb deze zaak beoordeeld en besloten de zaak te accorderen. Deze zaak mag worden afgehandeld.\r\n\r\nMet vriendelijke groet,\r\n\r\n[[beoordelaar_naam]]	\N	\N	\N	\N	\N	be0caefa-000c-46ac-b737-40c7b90ceda9
\.


--
-- Name: bibliotheek_notificaties_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_notificaties_id_seq', 5, true);


--
-- Data for Name: bibliotheek_sjablonen; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bibliotheek_sjablonen (search_index, search_term, object_type, searchable_id, search_order, id, bibliotheek_categorie_id, naam, label, description, help, created, last_modified, filestore_id, deleted, interface_id, template_external_name, uuid) FROM stdin;
\.


--
-- Name: bibliotheek_sjablonen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_sjablonen_id_seq', 2, true);


--
-- Data for Name: bibliotheek_sjablonen_magic_string; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bibliotheek_sjablonen_magic_string (id, bibliotheek_sjablonen_id, value) FROM stdin;
\.


--
-- Name: bibliotheek_sjablonen_magic_string_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bibliotheek_sjablonen_magic_string_id_seq', 4, true);


--
-- Data for Name: case_action; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.case_action (id, case_id, casetype_status_id, type, label, automatic, data, state_tainted, data_tainted) FROM stdin;
\.


--
-- Name: case_action_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.case_action_id_seq', 1, false);


--
-- Data for Name: case_property; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.case_property (id, name, type, namespace, value, value_v0, case_id, object_id) FROM stdin;
\.


--
-- Data for Name: case_relation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.case_relation (id, case_id_a, case_id_b, order_seq_a, order_seq_b, type_a, type_b, uuid) FROM stdin;
\.


--
-- Name: case_relation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.case_relation_id_seq', 1, false);


--
-- Data for Name: checklist; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.checklist (id, case_id, case_milestone) FROM stdin;
1	2	3
2	2	2
3	2	4
\.


--
-- Name: checklist_antwoord_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.checklist_antwoord_id_seq', 1, true);


--
-- Name: checklist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.checklist_id_seq', 3, true);


--
-- Data for Name: checklist_item; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.checklist_item (id, checklist_id, label, state, sequence, user_defined, deprecated_answer) FROM stdin;
1	2	Is alle info juist ingevuld?	t	1	f	ja
\.


--
-- Name: checklist_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.checklist_item_id_seq', 1, true);


--
-- Data for Name: config; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.config (id, parameter, value, advanced, uuid, definition_id) FROM stdin;
2	allocation_notification_template_id		f	fea28e71-95ed-4e35-bc4b-7c1fae51d338	8c3f8850-435d-4b64-8489-4285cdeedf7e
5	file_username_seperator	-	t	c11ca538-6184-4f94-a7d8-26af5cf9c095	98a664ba-e652-4ac0-8bd5-a6962389c6c6
6	feedback_email_template_id	\N	t	1e8e3428-6d4c-44f9-86d5-95fca5d6e848	1acf71e1-aab1-4951-8e7a-db7cdaaf6a2b
7	document_intake_user	intake	t	a77c4c15-b1e5-48c2-b8f9-1a2a3587f253	7561d210-b9be-4e29-9b65-c4f8a2de1fd4
8	users_can_change_password	off	t	554f1c49-f0ce-475d-8b60-1b281de74103	49999eee-96b4-46c4-babc-8fa6bb5978cb
10	enable_stufzkn_simulator	0	t	e5b1c74d-41ed-49f0-b9fd-b5000288c1f8	42f9b366-e324-4b79-abcc-a5d699290da6
11	disable_dashboard_customization	0	t	3c5a13df-3dd0-48cd-888b-083fd1de4046	1c7b45b7-f101-4b7e-8514-f412063cc0c8
13	custom_relation_roles	[]	t	33bdad65-e98f-4730-8e4c-ea1b713a7d14	e73874c6-e81a-49cb-9b87-05deb68e1118
14	app_enabled_wordapp	0	t	f521191a-8b88-4489-8840-c4264d3dbfbd	2c3ed588-6770-41df-8c95-2987b9bd6c61
15	bag_spoof_mode	0	t	df29e6b5-3946-4e5c-94d4-06c1e862a698	9b0956d2-c9a6-4e84-8c49-0f53d17551a1
16	bag_priority_gemeentes	[]	t	a4e2f453-a0dc-4823-ac26-0b136b079371	55482181-7e7d-4bc2-b653-356762a91ade
17	app_enabled_meeting	0	t	c028447e-9316-42f3-959f-f8cb6d291e7a	287fd068-0999-4c44-a504-a9538f19a763
18	case_distributor_group		t	91e94bd8-782c-42ce-a197-8a47016e43e8	82f270d2-498e-4ce9-adcd-cb23331daf69
19	case_distributor_role		t	59993c5c-6024-4e4b-a44e-3c90f025556d	7362679d-b8f6-493b-bcb1-b8d9350b443e
20	customer_info_adres		t	23099e44-844a-4998-a9f4-ede1098825bd	33e32331-e6d6-4031-85c7-59c7eb1163bc
21	customer_info_email		t	b7805a17-4328-4ace-b518-07eebe1b5843	239bbdee-0811-42c3-8cc2-10d20b8b1e30
22	customer_info_faxnummer		t	d14ce10f-b4a2-4760-9127-3cb37975d19f	3499c848-0903-47aa-9d86-f3bc4190e7e5
23	customer_info_gemeente_id_url		t	34f2d011-827a-4d93-b7a2-800769eb87cc	424ff3bc-bcf6-4772-872a-b9a93a2f3c34
24	customer_info_gemeente_portal		t	8c89a0ad-4658-498b-917e-5d5d01bad19c	11fa98a7-4fd4-4dbc-a929-c2e6ab25f49b
25	customer_info_huisnummer		t	2dfafb2c-a6a8-4161-8d07-b12a98164727	4bd449ae-d82d-43ed-aa84-72fe01b7462d
26	customer_info_latitude		t	02c8a7a3-e91b-4f44-a653-d0dca196b180	d81f8648-2a36-4ea0-814e-2af227a5da8a
27	customer_info_longitude		t	d30cc439-b314-4586-beee-ba3df15c1d2c	165a4192-8c28-4e74-904b-2317b00e2e23
28	customer_info_naam		t	83c86fd1-6687-4afc-a32c-290410e5ca0f	5f2fef2a-4833-4847-9b1a-236039700fb0
29	customer_info_naam_kort		t	6c1ad384-3d8b-4e7b-845c-9ae65abfc952	fadcf16c-4bbe-4157-9771-e156ca0ae205
30	customer_info_naam_lang		t	f3f7b2f3-09b4-4314-a975-3a87e258ff89	945bd84a-d3cc-412f-a801-e89787deaf29
31	customer_info_postbus		t	08dc5c0f-a2ff-40be-8a71-f227c6d568f3	eece146f-97e3-4315-a4dd-e6751360059b
32	customer_info_postbus_postcode		t	877fa0dd-3b62-4bed-b3ca-423f2313c556	02ec24a4-b511-4596-ad22-1b2a381f26ee
33	customer_info_postcode		t	d81239fa-9765-4ac1-abfb-0119a03f68e2	f3e9df2b-61cc-4d38-9feb-35dda8a5d22a
34	customer_info_straatnaam		t	6a5135a2-5e1d-406a-827a-3caab708c853	2e35ebfd-e6e0-4124-8e4f-7c28cfffeefd
35	customer_info_telefoonnummer		t	2b38ff4b-2644-4cb4-831d-2dd7a29f1045	4eea2794-28ac-4d24-a07e-e13d75528d1e
36	customer_info_website		t	020f395f-0c64-40f5-b56b-61e05f4369fc	e9cf0c02-f64f-44a2-b7d7-ac6004218084
37	customer_info_woonplaats		t	a30787cf-8e71-4cc1-b1ef-49c22bf3012a	086512fa-4b43-48c6-94da-7c9d1ccc895e
38	customer_info_zaak_email		t	fe662d68-339f-45ac-b035-ee6296581bc8	a21cbc94-b624-4a6a-8e10-3fa73c6148c8
39	enable_mintlab_id	0	t	cd356fbc-94c2-4483-9853-6085d47900b0	e3d6909a-134b-4317-b61f-1b7dca1569b5
40	files_locally_editable	0	t	f2300167-50ea-465b-92cf-a54a3405916f	92236ee6-eb05-44f3-bd0a-5bca376749b4
41	first_login_confirmation		t	ff503ecc-5071-4365-8dff-6b4d0f8e9dd3	f4ebe619-933c-4ead-b664-506a9c6e1ea0
42	first_login_intro		t	507e0a82-fd96-4993-9a9e-098eaf6524b3	a34e8a0c-6427-4e97-a14b-db0f12783a21
43	new_user_template		t	b7121a73-60a1-4a95-99c5-f73286fb9c11	6271cb81-e5fb-40fd-9385-d8544fee75f0
44	pdf_annotations_public	0	t	60425515-9f0d-40dd-90cf-7bee92f579c8	608f73b1-f15f-4985-8dc6-06ce2d9c257a
45	pip_login_intro		t	067e64b0-29c8-4502-ab3d-65a16676e293	2f9d7541-8e22-4dfc-9934-1374c55175b4
46	public_manpage	1	t	4f8595f1-7e72-4268-bf8a-ef4f90277ec0	2ea16761-23b7-4964-9548-2d54861cc78d
47	requestor_search_extension_active		t	8195a984-53a7-43d8-b9f8-627216e5b54e	348a00a6-4163-49f4-9279-eef1ccb19ae6
48	requestor_search_extension_href		t	9850db97-3100-45ae-a95a-032b4e2bcb8b	7fcfb2ea-8f46-475c-8f89-e09b2c28c828
49	requestor_search_extension_name		t	4428bd11-ba67-47d2-bff9-737f60dbf544	836c7e4b-a896-4c25-a7a4-6f138f75d41e
50	signature_upload_role		t	68410b89-a14c-430a-a23f-8b0959a462a4	09c5e460-2fd3-4153-84db-7f9159fe4f07
51	subject_pip_authorization_confirmation_template_id		t	0b9b08bc-f65f-4a85-8fa1-542775af9446	2cd00952-d6a0-4096-93c9-7515108c798d
52	bag_local_only	0	t	3a48b33a-9129-440c-beab-8ec938f3a409	399be534-a816-42b9-bae8-f2899405b7b8
12	allowed_templates	[]	t	bdd24eb7-1911-4919-ae01-070a17c666f1	33aeed61-95bb-4dd7-9022-f2ba5aa2b718
\.


--
-- Name: config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.config_id_seq', 52, true);


--
-- Data for Name: contact_data; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.contact_data (id, gegevens_magazijn_id, betrokkene_type, mobiel, telefoonnummer, email, created, last_modified, note) FROM stdin;
1	1	1	\N	\N	laura@mintlab.nl	\N	\N	\N
\.


--
-- Name: contact_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.contact_data_id_seq', 1, true);


--
-- Data for Name: contactmoment; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.contactmoment (id, subject_id, case_id, type, medium, date_created, created_by, uuid) FROM stdin;
5	\N	2	email	email	2013-01-24 14:10:41	betrokkene-medewerker-20000	de45d9d8-d2ba-4ed7-9595-89a93688e212
6	\N	2	email	email	2013-01-24 14:18:08	betrokkene-medewerker-20000	dfba4542-d802-4f08-ba56-34dafb727c1a
\.


--
-- Data for Name: contactmoment_email; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.contactmoment_email (id, filestore_id, contactmoment_id, body, subject, recipient, cc, bcc) FROM stdin;
3	3	5	Geachte meneer/mevrouw de Blauwe-Martin,\r\n\r\nOp 24-01-2013 heeft u een aanvraag ontvangen voor een Testzaaktype gedaan. Bij deze bevestigen wij de ontvangst van uw aanvraag, welke bij ons geregistreerd staat onder zaaknummer 2. In geval van correspondentie verzoeken wij u dit zaaknummer te gebruiken.\r\n\r\nWij streven er naar om uw aanvraag uiterlijk 29-01-2013 te hebben afgehandeld. Tijdens de behandeling van uw aanvraag houden wij u op de hoogte van de voortgang.\r\n\r\nMet vriendelijke groet,\r\n\r\nHet testteam	Ontvangstbevestiging	laura@mintlab.nl	\N	\N
4	4	6	Aan: behandelaar van zaaknummer 2\r\n\r\nIk heb deze zaak beoordeeld en besloten de zaak te accorderen. Deze zaak mag worden afgehandeld.\r\n\r\nMet vriendelijke groet,\r\n\r\n	Beoordeling positief zaaknummer 2	laura@mintlab.nl	\N	\N
\.


--
-- Name: contactmoment_email_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.contactmoment_email_id_seq', 4, true);


--
-- Name: contactmoment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.contactmoment_id_seq', 6, true);


--
-- Data for Name: contactmoment_note; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.contactmoment_note (id, message, contactmoment_id) FROM stdin;
\.


--
-- Name: contactmoment_note_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.contactmoment_note_id_seq', 1, false);


--
-- Data for Name: directory; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.directory (id, name, case_id, original_name, path, uuid) FROM stdin;
\.


--
-- Name: directory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.directory_id_seq', 1, false);


--
-- Data for Name: file; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.file (search_index, search_term, object_type, searchable_id, search_order, id, filestore_id, name, extension, root_file_id, version, case_id, metadata_id, subject_id, directory_id, creation_reason, accepted, rejection_reason, reject_to_queue, is_duplicate_name, publish_pip, publish_website, date_created, created_by, date_modified, modified_by, date_deleted, deleted_by, destroyed, scheduled_jobs_id, intake_owner, active_version, is_duplicate_of, queue, document_status, generator, lock_timestamp, lock_subject_id, lock_subject_name, uuid, confidential) FROM stdin;
\.


--
-- Data for Name: file_annotation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.file_annotation (id, file_id, subject, properties, created, modified) FROM stdin;
\.


--
-- Data for Name: file_case_document; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.file_case_document (id, file_id, case_document_id) FROM stdin;
\.


--
-- Name: file_case_document_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.file_case_document_id_seq', 1, false);


--
-- Data for Name: file_derivative; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.file_derivative (id, file_id, filestore_id, max_width, max_height, date_generated, type) FROM stdin;
\.


--
-- Name: file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.file_id_seq', 1, false);


--
-- Data for Name: file_metadata; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.file_metadata (id, description, trust_level, origin, document_category, origin_date, pronom_format, appearance, structure, creation_date) FROM stdin;
\.


--
-- Name: file_metadata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.file_metadata_id_seq', 1, false);


--
-- Data for Name: filestore; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.filestore (id, uuid, thumbnail_uuid, original_name, size, mimetype, md5, date_created, storage_location, is_archivable, virus_scan_status) FROM stdin;
3	61491ebf-0de4-4292-b27f-15699aca7625	\N	laura@mintlab.nl.email	644	text/plain	de7b2316c6de5e22c2fe681ddbf9fd12	2013-06-18 08:40:31.899684	\N	t	ok
4	6d851c69-d131-42ba-896d-62d8514884d0	\N	laura@mintlab.nl.email	313	text/plain	e496b14038351c41454cabf21ab713ed	2013-06-18 08:40:31.899684	\N	t	ok
\.


--
-- Name: filestore_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.filestore_id_seq', 4, true);


--
-- Data for Name: gegevensmagazijn_subjecten; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.gegevensmagazijn_subjecten (subject_uuid, nnp_uuid) FROM stdin;
\.


--
-- Data for Name: gm_adres; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.gm_adres (id, straatnaam, huisnummer, huisletter, huisnummertoevoeging, nadere_aanduiding, postcode, woonplaats, gemeentedeel, functie_adres, datum_aanvang_bewoning, woonplaats_id, gemeente_code, hash, import_datum, adres_buitenland1, adres_buitenland2, adres_buitenland3, landcode, natuurlijk_persoon_id, deleted_on) FROM stdin;
2	Jimlaan	42	X	\N	\N	1234AB	Hilversum	\N	W	\N	\N	\N	\N	\N	\N	\N	\N	6030	\N	\N
\.


--
-- Name: gm_adres_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.gm_adres_id_seq', 2, true);


--
-- Data for Name: gm_bedrijf; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.gm_bedrijf (id, gegevens_magazijn_id, dossiernummer, subdossiernummer, hoofdvestiging_dossiernummer, hoofdvestiging_subdossiernummer, vorig_dossiernummer, vorig_subdossiernummer, handelsnaam, rechtsvorm, kamernummer, faillisement, surseance, telefoonnummer, email, vestiging_adres, vestiging_straatnaam, vestiging_huisnummer, vestiging_huisnummertoevoeging, vestiging_postcodewoonplaats, vestiging_postcode, vestiging_woonplaats, correspondentie_adres, correspondentie_straatnaam, correspondentie_huisnummer, correspondentie_huisnummertoevoeging, correspondentie_postcodewoonplaats, correspondentie_postcode, correspondentie_woonplaats, hoofdactiviteitencode, nevenactiviteitencode1, nevenactiviteitencode2, werkzamepersonen, contact_naam, contact_aanspreektitel, contact_voorletters, contact_voorvoegsel, contact_geslachtsnaam, contact_geslachtsaanduiding, authenticated, authenticatedby, import_datum, verblijfsobject_id, vestigingsnummer, vestiging_huisletter, correspondentie_huisletter, vestiging_adres_buitenland1, vestiging_adres_buitenland2, vestiging_adres_buitenland3, vestiging_landcode, correspondentie_adres_buitenland1, correspondentie_adres_buitenland2, correspondentie_adres_buitenland3, correspondentie_landcode) FROM stdin;
\.


--
-- Name: gm_bedrijf_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.gm_bedrijf_id_seq', 1, false);


--
-- Data for Name: gm_natuurlijk_persoon; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.gm_natuurlijk_persoon (id, gegevens_magazijn_id, betrokkene_type, burgerservicenummer, a_nummer, voorletters, voornamen, geslachtsnaam, voorvoegsel, geslachtsaanduiding, nationaliteitscode1, nationaliteitscode2, nationaliteitscode3, geboorteplaats, geboorteland, geboortedatum, aanhef_aanschrijving, voorletters_aanschrijving, voornamen_aanschrijving, naam_aanschrijving, voorvoegsel_aanschrijving, burgerlijke_staat, indicatie_geheim, import_datum, adres_id, authenticatedby, authenticated, datum_overlijden, verblijfsobject_id, aanduiding_naamgebruik, onderzoek_persoon, onderzoek_huwelijk, onderzoek_overlijden, onderzoek_verblijfplaats, partner_a_nummer, partner_burgerservicenummer, partner_voorvoegsel, partner_geslachtsnaam, datum_huwelijk, datum_huwelijk_ontbinding, landcode, naamgebruik, adellijke_titel) FROM stdin;
\.


--
-- Name: gm_natuurlijk_persoon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.gm_natuurlijk_persoon_id_seq', 1, false);


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.groups (id, path, name, description, date_created, date_modified, uuid) FROM stdin;
\.


--
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.groups_id_seq', 1, false);


--
-- Data for Name: interface; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.interface (id, name, active, case_type_id, max_retries, interface_config, multiple, module, date_deleted, uuid, objecttype_id) FROM stdin;
1	LDAP Authenticatie	t	\N	10	{}	t	authldap	\N	5e9a6bf2-6f9a-43b8-9643-0144ef1df3a9	\N
\.


--
-- Name: interface_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.interface_id_seq', 1, true);


--
-- Data for Name: logging; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.logging (id, zaak_id, betrokkene_id, aanvrager_id, is_bericht, component, component_id, seen, onderwerp, bericht, created, last_modified, deleted_on, event_type, event_data, created_by, modified_by, deleted_by, created_for, created_by_name_cache, object_uuid, restricted) FROM stdin;
5	\N	betrokkene-medewerker-20000	\N	\N	kenmerk	1	\N	Kenmerk 1 (Test 01) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-22 15:34:28	2013-01-22 14:34:28	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
6	\N	betrokkene-medewerker-20000	\N	\N	kenmerk	2	\N	Kenmerk 2 (Test 02) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-22 15:34:46	2013-01-22 14:34:46	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
7	\N	betrokkene-medewerker-20000	\N	\N	kenmerk	3	\N	Kenmerk 3 (Test 03) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-22 15:35:36	2013-01-22 14:35:36	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
8	\N	betrokkene-medewerker-20000	\N	\N	kenmerk	4	\N	Kenmerk 4 (Omschrijving) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-22 15:37:05	2013-01-22 14:37:05	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
9	\N	betrokkene-medewerker-20000	\N	\N	kenmerk	5	\N	Kenmerk 5 (Test 04) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-22 15:37:41	2013-01-22 14:37:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
10	\N	betrokkene-medewerker-20000	\N	\N	kenmerk	6	\N	Kenmerk 6 (Test 05) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-22 15:37:59	2013-01-22 14:37:59	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
11	\N	betrokkene-medewerker-20000	\N	\N	kenmerk	7	\N	Kenmerk 7 (Test 06) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-22 15:38:50	2013-01-22 14:38:50	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
12	\N	betrokkene-medewerker-20000	\N	\N	kenmerk	8	\N	Kenmerk 8 (Test 07) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-22 15:39:41	2013-01-22 14:39:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
13	\N	betrokkene-medewerker-20000	\N	\N	zaaktype	6	\N	Zaaktype 6 (Testzaaktype) opgeslagen, Componenten gewijzigd: kenmerken	\N	2013-01-22 15:40:10	2013-01-22 14:40:10	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
14	\N	betrokkene-medewerker-20000	\N	\N	notificatie	3	\N	E-mailsjabloon 3 (Ontvangstbevestiging) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-22 15:43:36	2013-01-22 14:43:36	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
15	\N	betrokkene-medewerker-20000	\N	\N	notificatie	4	\N	E-mailsjabloon 4 (Beoordeling negatief) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-22 15:47:38	2013-01-22 14:47:38	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
16	\N	betrokkene-medewerker-20000	\N	\N	kenmerk	9	\N	Kenmerk 9 (Test 08) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-22 15:49:33	2013-01-22 14:49:33	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
17	\N	betrokkene-medewerker-20000	\N	\N	notificatie	5	\N	E-mailsjabloon 5 (Beoordeling positief zaaknummer [[zaaknummer]]) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-22 15:50:25	2013-01-22 14:50:25	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
18	\N	betrokkene-medewerker-20000	\N	\N	notificatie	5	\N	E-mailsjabloon 5 (Beoordeling positief) opgeslagen, opmerking: Wijziging 	\N	2013-01-22 15:50:48	2013-01-22 14:50:48	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
19	\N	betrokkene-medewerker-20000	\N	\N	notificatie	4	\N	E-mailsjabloon 4 (Beoordeling negatief) opgeslagen, opmerking: Wijziging 	\N	2013-01-22 15:51:19	2013-01-22 14:51:19	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
20	\N	betrokkene-medewerker-20000	\N	\N	notificatie	5	\N	E-mailsjabloon 5 (Beoordeling positief) opgeslagen, opmerking: Wijziging 	\N	2013-01-22 15:52:04	2013-01-22 14:52:04	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
21	\N	betrokkene-medewerker-20000	\N	\N	kenmerk	10	\N	Kenmerk 10 (Test 09) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-22 15:53:46	2013-01-22 14:53:46	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
22	\N	betrokkene-medewerker-20000	\N	\N	zaaktype	6	\N	Zaaktype 6 (Testzaaktype) opgeslagen, Componenten gewijzigd: kenmerken	\N	2013-01-22 15:55:29	2013-01-22 14:55:29	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
23	\N	betrokkene-medewerker-20000	\N	\N	kenmerk	11	\N	Kenmerk 11 (Toelichting) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-22 16:43:40	2013-01-22 15:43:40	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
24	\N	betrokkene-medewerker-20000	\N	\N	zaaktype	6	\N	Zaaktype 6 (Testzaaktype) opgeslagen, Componenten gewijzigd: kenmerken, autorisatie	\N	2013-01-22 16:45:00	2013-01-22 15:45:00	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
25	\N	betrokkene-medewerker-20000	\N	\N	zaaktype	6	\N	Zaaktype 6 (Testzaaktype) opgeslagen, Componenten gewijzigd: kenmerken	\N	2013-01-22 16:57:18	2013-01-22 15:57:18	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
26	\N	betrokkene-medewerker-20000	\N	\N	sjabloon	1	\N	Sjabloon 1 (Beoordeling positief) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-24 14:54:00	2013-01-24 13:54:00	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
27	\N	betrokkene-medewerker-20000	\N	\N	sjabloon	2	\N	Sjabloon 2 (Beoordeling negatief) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-24 14:54:26	2013-01-24 13:54:26	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
28	\N	betrokkene-medewerker-20000	\N	\N	zaaktype	6	\N	Zaaktype 6 (Testzaaktype) opgeslagen, Componenten gewijzigd: kenmerken	\N	2013-01-24 14:58:38	2013-01-24 13:58:38	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
29	\N	betrokkene-medewerker-20000	\N	\N	kenmerk	12	\N	Kenmerk 12 (Test A) aangemaakt, opmerking: Nieuw aangemaakt 	\N	2013-01-24 15:00:13	2013-01-24 14:00:13	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
30	\N	betrokkene-medewerker-20000	\N	\N	zaaktype	6	\N	Zaaktype 6 (Testzaaktype) opgeslagen, Componenten gewijzigd: kenmerken	\N	2013-01-24 15:06:13	2013-01-24 14:06:13	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
31	\N	betrokkene-medewerker-20000	\N	\N	zaaktype	6	\N	Zaaktype 6 (Testzaaktype) opgeslagen, Componenten gewijzigd: kenmerken	\N	2013-01-24 15:08:56	2013-01-24 14:08:56	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
32	2	betrokkene-medewerker-20000	\N	\N	betrokkene	\N	\N	Betrokkene "aanvrager" gewijzigd naar: "Jim Willemina Martin"	\N	2013-01-24 15:10:41	2013-01-24 14:10:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
33	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 01" gewijzigd naar: "Optie 3"	Optie 3	2013-01-24 15:10:41	2013-01-24 14:10:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
34	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 03" gewijzigd naar: "Optie A"	Optie A	2013-01-24 15:10:41	2013-01-24 14:10:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
35	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 02" gewijzigd naar: "25-01-2013"	25-01-2013	2013-01-24 15:10:41	2013-01-24 14:10:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
36	2	betrokkene-medewerker-20000	\N	\N	zaak	2	\N	Zaak (2) aangemaakt	\N	2013-01-24 15:10:41	2013-01-24 14:10:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
37	2	betrokkene-medewerker-20000	\N	\N	document	1	\N	Document "laura@mintlab.nl" [1] succesvol aangemaakt	\N	2013-01-24 15:10:41	2013-01-24 14:10:41	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
38	\N	betrokkene-medewerker-20000	\N	\N	zaaktype	6	\N	Zaaktype 6 (Testzaaktype) opgeslagen, Componenten gewijzigd: basisattributen	\N	2013-01-24 15:11:44	2013-01-24 14:11:44	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
39	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Omschrijving" gewijzigd naar: "testest"	testest	2013-01-24 15:12:52	2013-01-24 14:12:52	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
40	2	betrokkene-medewerker-20000	\N	\N	kenmerk	8	\N	Kenmerk "Test 07" verwijderd ivm verbergen door regels.	\N	2013-01-24 15:12:52	2013-01-24 14:12:52	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
41	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Omschrijving" gewijzigd naar: "testest"	testest	2013-01-24 15:12:57	2013-01-24 14:12:57	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
42	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 07" gewijzigd naar: "Ja"	Ja	2013-01-24 15:12:57	2013-01-24 14:12:57	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
43	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Omschrijving" gewijzigd naar: "testest"	testest	2013-01-24 15:13:07	2013-01-24 14:13:07	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
44	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 07" gewijzigd naar: "Nee"	Nee	2013-01-24 15:13:07	2013-01-24 14:13:07	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
45	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Omschrijving" gewijzigd naar: "testest"	testest	2013-01-24 15:13:54	2013-01-24 14:13:54	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
46	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 04" gewijzigd naar: "09"	09	2013-01-24 15:13:54	2013-01-24 14:13:54	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
47	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 07" gewijzigd naar: "Nee"	Nee	2013-01-24 15:13:54	2013-01-24 14:13:54	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
48	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Omschrijving" gewijzigd naar: "testest"	testest	2013-01-24 15:14:06	2013-01-24 14:14:06	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
49	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 04" gewijzigd naar: "09"	09	2013-01-24 15:14:07	2013-01-24 14:14:07	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
50	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 07" gewijzigd naar: "Nee"	Nee	2013-01-24 15:14:07	2013-01-24 14:14:07	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
51	2	betrokkene-medewerker-20000	\N	\N	zaak	\N	\N	Zaak geaccepteerd door "A. Admin"	\N	2013-01-24 15:14:28	2013-01-24 14:14:28	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
52	2	betrokkene-medewerker-20000	\N	\N	betrokkene	\N	\N	Betrokkene "behandelaar" gewijzigd naar: "A. Admin"	\N	2013-01-24 15:14:28	2013-01-24 14:14:28	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
53	2	betrokkene-medewerker-20000	\N	\N	betrokkene	\N	\N	Betrokkene "coordinator" gewijzigd naar: "A. Admin"	\N	2013-01-24 15:14:28	2013-01-24 14:14:28	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
54	2	betrokkene-medewerker-20000	\N	\N	checklist	34	\N	Antwoord voor vraag: "Is alle info juist ingevuld?" gewijzigd naar "ja"	\N	2013-01-24 15:14:47	2013-01-24 14:14:47	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
55	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 08" gewijzigd naar: "testuleer"	testuleer	2013-01-24 15:16:30	2013-01-24 14:16:30	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
56	2	betrokkene-medewerker-20000	\N	\N	kenmerk	10	\N	Kenmerk "Test 09" verwijderd ivm verbergen door regels.	\N	2013-01-24 15:16:30	2013-01-24 14:16:30	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
57	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 08" gewijzigd naar: "testuleer"	testuleer	2013-01-24 15:16:30	2013-01-24 14:16:30	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
58	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 09" gewijzigd naar: "Nee"	Nee	2013-01-24 15:16:30	2013-01-24 14:16:30	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
59	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 08" gewijzigd naar: "testuleer"	testuleer	2013-01-24 15:16:35	2013-01-24 14:16:35	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
60	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 09" gewijzigd naar: "Ja"	Ja	2013-01-24 15:16:35	2013-01-24 14:16:35	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
61	2	betrokkene-medewerker-20000	\N	\N	document	2	\N	Document "laura@mintlab.nl" [2] succesvol aangemaakt	\N	2013-01-24 15:18:08	2013-01-24 14:18:08	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
62	\N	betrokkene-medewerker-20000	\N	\N	zaaktype	6	\N	Zaaktype 6 (Testzaaktype) opgeslagen, Componenten gewijzigd: kenmerken	\N	2013-01-24 15:29:24	2013-01-24 14:29:24	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
63	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 08" gewijzigd naar: "testuleer"	testuleer	2013-01-24 15:31:12	2013-01-24 14:31:12	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
64	2	betrokkene-medewerker-20000	\N	\N	kenmerk	\N	\N	Kenmerk "Test 09" gewijzigd naar: "Nee"	Nee	2013-01-24 15:31:12	2013-01-24 14:31:12	\N	\N	\N	\N	\N	\N	\N	\N	\N	f
67	2	\N	\N	\N	email	\N	\N	E-mail "Ontvangstbevestiging" verstuurd naar "laura@mintlab.nl"	\N	2013-06-18 06:40:32	2013-06-18 06:40:32	\N	email/send	{"attachments":null,"case_id":2,"contactmoment_id":5,"content":"Geachte meneer/mevrouw de Blauwe-Martin,\\r\\n\\r\\nOp 24-01-2013 heeft u een aanvraag ontvangen voor een Testzaaktype gedaan. Bij deze bevestigen wij de ontvangst van uw aanvraag, welke bij ons geregistreerd staat onder zaaknummer 2. In geval van correspondentie verzoeken wij u dit zaaknummer te gebruiken.\\r\\n\\r\\nWij streven er naar om uw aanvraag uiterlijk 29-01-2013 te hebben afgehandeld. Tijdens de behandeling van uw aanvraag houden wij u op de hoogte van de voortgang.\\r\\n\\r\\nMet vriendelijke groet,\\r\\n\\r\\nHet testteam","recipient":"laura@mintlab.nl","subject":"Ontvangstbevestiging"}	\N	\N	\N	\N	\N	\N	f
68	2	\N	\N	\N	email	\N	\N	E-mail "Beoordeling positief zaaknummer 2" verstuurd naar "laura@mintlab.nl"	\N	2013-06-18 06:40:32	2013-06-18 06:40:32	\N	email/send	{"attachments":null,"case_id":2,"contactmoment_id":6,"content":"Aan: behandelaar van zaaknummer 2\\r\\n\\r\\nIk heb deze zaak beoordeeld en besloten de zaak te accorderen. Deze zaak mag worden afgehandeld.\\r\\n\\r\\nMet vriendelijke groet,\\r\\n\\r\\n","recipient":"laura@mintlab.nl","subject":"Beoordeling positief zaaknummer 2"}	\N	\N	\N	\N	\N	\N	f
\.


--
-- Name: logging_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.logging_id_seq', 68, true);


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.message (id, message, subject_id, logging_id, is_read, is_archived) FROM stdin;
\.


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.message_id_seq', 1, false);


--
-- Data for Name: natuurlijk_persoon; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.natuurlijk_persoon (search_index, search_term, object_type, searchable_id, search_order, id, burgerservicenummer, a_nummer, voorletters, voornamen, geslachtsnaam, voorvoegsel, geslachtsaanduiding, nationaliteitscode1, nationaliteitscode2, nationaliteitscode3, geboorteplaats, geboorteland, geboortedatum, aanhef_aanschrijving, voorletters_aanschrijving, voornamen_aanschrijving, naam_aanschrijving, voorvoegsel_aanschrijving, burgerlijke_staat, indicatie_geheim, land_waarnaar_vertrokken, import_datum, adres_id, authenticated, authenticatedby, deleted_on, verblijfsobject_id, datum_overlijden, aanduiding_naamgebruik, onderzoek_persoon, onderzoek_huwelijk, onderzoek_overlijden, onderzoek_verblijfplaats, partner_a_nummer, partner_burgerservicenummer, partner_voorvoegsel, partner_geslachtsnaam, datum_huwelijk, datum_huwelijk_ontbinding, in_gemeente, landcode, naamgebruik, uuid, active, adellijke_titel) FROM stdin;
\.


--
-- Name: natuurlijk_persoon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.natuurlijk_persoon_id_seq', 1, true);


--
-- Data for Name: object_acl_entry; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.object_acl_entry (uuid, object_uuid, entity_type, entity_id, capability, scope, groupname) FROM stdin;
\.


--
-- Data for Name: object_bibliotheek_entry; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.object_bibliotheek_entry (search_index, search_term, object_type, searchable_id, search_order, id, bibliotheek_categorie_id, object_uuid, name) FROM stdin;
\.


--
-- Name: object_bibliotheek_entry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.object_bibliotheek_entry_id_seq', 1, false);


--
-- Data for Name: object_data; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.object_data (uuid, object_id, object_class, properties, index_hstore, date_created, date_modified, text_vector, class_uuid, acl_groupname, invalid) FROM stdin;
\.


--
-- Data for Name: object_mutation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.object_mutation (id, object_uuid, object_type, lock_object_uuid, type, "values", date_created, subject_id, executed) FROM stdin;
\.


--
-- Data for Name: object_relation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.object_relation (id, name, object_type, object_uuid, object_embedding, object_id, object_preview) FROM stdin;
\.


--
-- Data for Name: object_relationships; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.object_relationships (uuid, object1_uuid, object2_uuid, type1, type2, object1_type, object2_type, blocks_deletion, title1, title2, owner_object_uuid) FROM stdin;
\.


--
-- Data for Name: object_subscription; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.object_subscription (id, interface_id, external_id, local_table, local_id, date_created, date_deleted, object_preview, config_interface_id) FROM stdin;
\.


--
-- Name: object_subscription_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.object_subscription_id_seq', 1, false);


--
-- Data for Name: parkeergebied; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.parkeergebied (id, bag_hoofdadres, postcode, straatnaam, huisnummer, huisletter, huisnummertoevoeging, parkeergebied_id, parkeergebied, created, last_modified, woonplaats) FROM stdin;
\.


--
-- Name: parkeergebied_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.parkeergebied_id_seq', 1, false);


--
-- Data for Name: queue; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.queue (id, object_id, status, type, label, data, date_created, date_started, date_finished, parent_id, priority, metadata) FROM stdin;
\.


--
-- Data for Name: remote_api_keys; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.remote_api_keys (id, key, permissions) FROM stdin;
\.


--
-- Name: remote_api_keys_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.remote_api_keys_id_seq', 1, false);


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.roles (id, parent_group_id, name, description, system_role, date_created, date_modified, uuid) FROM stdin;
\.


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.roles_id_seq', 1, false);


--
-- Data for Name: sbus_logging; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.sbus_logging (id, sbus_traffic_id, pid, mutatie_type, object, params, kerngegeven, label, changes, error, error_message, created, modified) FROM stdin;
\.


--
-- Name: sbus_logging_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sbus_logging_id_seq', 1, false);


--
-- Data for Name: sbus_traffic; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.sbus_traffic (id, sbus_type, object, operation, input, input_raw, output, output_raw, error, error_message, created, modified) FROM stdin;
\.


--
-- Name: sbus_traffic_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sbus_traffic_id_seq', 1, false);


--
-- Data for Name: scheduled_jobs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.scheduled_jobs (id, task, scheduled_for, parameters, created, last_modified, deleted, schedule_type, case_id, uuid) FROM stdin;
\.


--
-- Name: scheduled_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.scheduled_jobs_id_seq', 1, false);


--
-- Data for Name: search_query; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.search_query (id, settings, ldap_id, name, sort_index) FROM stdin;
\.


--
-- Data for Name: search_query_delen; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.search_query_delen (id, search_query_id, ou_id, role_id) FROM stdin;
\.


--
-- Name: search_query_delen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.search_query_delen_id_seq', 1, false);


--
-- Name: search_query_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.search_query_id_seq', 1, false);


--
-- Data for Name: searchable; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.searchable (search_index, search_term, object_type, searchable_id, search_order) FROM stdin;
\.


--
-- Name: searchable_searchable_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.searchable_searchable_id_seq', 35, true);


--
-- Data for Name: session_invitation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.session_invitation (id, subject_id, object_id, object_type, date_created, date_expires, token, action_path) FROM stdin;
\.


--
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.settings (id, key, value) FROM stdin;
\.


--
-- Name: settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.settings_id_seq', 1, false);


--
-- Data for Name: subject; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.subject (id, uuid, subject_type, properties, settings, username, last_modified, role_ids, group_ids, nobody, system) FROM stdin;
\.


--
-- Name: subject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.subject_id_seq', 1, false);


--
-- Data for Name: transaction; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.transaction (id, interface_id, external_transaction_id, input_data, input_file, automated_retry_count, date_created, date_last_retry, date_next_retry, processed, date_deleted, error_count, direction, success_count, total_count, processor_params, error_fatal, preview_data, error_message, text_vector, uuid) FROM stdin;
\.


--
-- Name: transaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.transaction_id_seq', 1, false);


--
-- Data for Name: transaction_record; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.transaction_record (id, transaction_id, input, output, is_error, date_executed, date_deleted, preview_string, last_error, uuid) FROM stdin;
\.


--
-- Name: transaction_record_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.transaction_record_id_seq', 1, false);


--
-- Data for Name: transaction_record_to_object; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.transaction_record_to_object (id, transaction_record_id, local_table, local_id, mutations, date_deleted, mutation_type) FROM stdin;
\.


--
-- Name: transaction_record_to_object_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.transaction_record_to_object_id_seq', 1, false);


--
-- Data for Name: user_app_lock; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.user_app_lock (type, type_id, create_unixtime, session_id, uidnumber) FROM stdin;
\.


--
-- Data for Name: user_entity; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.user_entity (id, uuid, source_interface_id, source_identifier, subject_id, date_created, date_deleted, properties, password, active) FROM stdin;
\.


--
-- Name: user_entity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.user_entity_id_seq', 1, false);


--
-- Data for Name: woz_objects; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.woz_objects (id, object_data, owner, object_id) FROM stdin;
\.


--
-- Name: woz_objects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.woz_objects_id_seq', 1, false);


--
-- Data for Name: zaak; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak (search_index, search_term, object_type, searchable_id, search_order, id, pid, relates_to, zaaktype_id, zaaktype_node_id, milestone, contactkanaal, aanvraag_trigger, onderwerp, resultaat, besluit, coordinator, behandelaar, aanvrager, route_ou, route_role, locatie_zaak, locatie_correspondentie, streefafhandeldatum, registratiedatum, afhandeldatum, vernietigingsdatum, created, last_modified, deleted, vervolg_van, aanvrager_gm_id, behandelaar_gm_id, coordinator_gm_id, uuid, payment_status, payment_amount, hstore_properties, confidentiality, stalled_until, onderwerp_extern, archival_state, status, duplicate_prevention_token, resultaat_id, urgency) FROM stdin;
\N	Testzaaktype 2 Jim Willemina Martin A. Admin 2013-01-24T14:09:56 testing scenario	zaak	30	\N	2	\N	\N	6	12	2	behandelaar	extern	\N	afgewezen	\N	7	6	5	10011	20007	\N	\N	2013-01-29 14:09:56	2013-01-24 14:09:56	\N	\N	2013-01-24 14:09:56	2013-01-25 08:10:15	\N	\N	1	20000	20000	\N	\N	\N	\N	public	\N	\N	\N	open	a6ef05a1-fb9f-4b97-bc91-f4c71acd1767	\N	\N
\.


--
-- Data for Name: zaak_authorisation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak_authorisation (id, zaak_id, capability, entity_id, entity_type, scope) FROM stdin;
\.


--
-- Name: zaak_authorisation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaak_authorisation_id_seq', 1, false);


--
-- Data for Name: zaak_bag; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak_bag (id, pid, zaak_id, bag_type, bag_id, bag_verblijfsobject_id, bag_openbareruimte_id, bag_nummeraanduiding_id, bag_pand_id, bag_standplaats_id, bag_ligplaats_id, bag_coordinates_wsg) FROM stdin;
\.


--
-- Name: zaak_bag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaak_bag_id_seq', 1, false);


--
-- Data for Name: zaak_betrokkenen; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak_betrokkenen (id, zaak_id, betrokkene_type, betrokkene_id, gegevens_magazijn_id, verificatie, naam, rol, magic_string_prefix, deleted, uuid, pip_authorized, subject_id) FROM stdin;
5	2	natuurlijk_persoon	2	1	medewerker	Jim Willemina Martin	\N	\N	\N	c09a0ace-4663-4610-b135-53dd8add9d8f	f	\N
4	\N	medewerker	20000	20000	\N	A. Admin	\N	\N	\N	3ede9e9f-9ab0-4dc7-a513-9a44f11e6a13	f	\N
6	\N	medewerker	20000	20000	\N	A. Admin	\N	\N	\N	b59cf2f2-636d-48fb-a0d0-89c843b7eebe	f	\N
7	\N	medewerker	20000	20000	\N	A. Admin	\N	\N	\N	4901a581-f534-4bbe-b397-72fb18620b53	f	\N
\.


--
-- Name: zaak_betrokkenen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaak_betrokkenen_id_seq', 7, true);


--
-- Name: zaak_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaak_id_seq', 2, true);


--
-- Data for Name: zaak_kenmerk; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak_kenmerk (zaak_id, bibliotheek_kenmerken_id, id, value) FROM stdin;
2	1	22	{"Optie 3"}
2	2	23	{25-01-2013}
2	3	24	{"Optie A"}
2	4	25	{testest}
2	5	26	{09}
2	8	27	{Nee}
2	9	28	{testuleer}
2	10	29	{Nee}
\.


--
-- Name: zaak_kenmerk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaak_kenmerk_id_seq', 29, true);


--
-- Data for Name: zaak_meta; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak_meta (id, zaak_id, verlenging, opschorten, deel, gerelateerd, vervolg, afhandeling, stalled_since) FROM stdin;
\.


--
-- Name: zaak_meta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaak_meta_id_seq', 1, false);


--
-- Data for Name: zaak_onafgerond; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak_onafgerond (zaaktype_id, betrokkene, json_string, afronden, create_unixtime) FROM stdin;
\.


--
-- Data for Name: zaak_subcase; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaak_subcase (id, zaak_id, relation_zaak_id, required) FROM stdin;
\.


--
-- Name: zaak_subcase_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaak_subcase_id_seq', 1, false);


--
-- Data for Name: zaaktype; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype (search_index, search_term, object_type, searchable_id, search_order, id, zaaktype_node_id, version, created, last_modified, deleted, bibliotheek_categorie_id, active) FROM stdin;
\N	6 Testzaaktype testing scenario Alomvattend testzaaktype	zaaktype	20	\N	6	14	\N	2013-01-22 14:40:10	2013-01-24 14:29:24	\N	2	t
\.


--
-- Data for Name: zaaktype_authorisation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_authorisation (id, zaaktype_node_id, recht, created, last_modified, deleted, role_id, ou_id, zaaktype_id, confidential) FROM stdin;
37	14	zaak_edit	\N	\N	\N	20002	10020	6	f
38	14	zaak_edit	\N	\N	\N	20002	10011	6	f
39	14	zaak_read	\N	\N	\N	20002	10020	6	f
40	14	zaak_read	\N	\N	\N	20002	10011	6	f
\.


--
-- Name: zaaktype_authorisation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_authorisation_id_seq', 40, true);


--
-- Data for Name: zaaktype_betrokkenen; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_betrokkenen (id, zaaktype_node_id, betrokkene_type, created, last_modified) FROM stdin;
16	6	natuurlijk_persoon	2013-01-22 14:40:10	2013-01-22 14:40:10
17	6	natuurlijk_persoon_na	2013-01-22 14:40:10	2013-01-22 14:40:10
18	6	medewerker	2013-01-22 14:40:10	2013-01-22 14:40:10
19	6	medewerker	2013-01-22 14:40:10	2013-01-22 14:40:10
20	7	natuurlijk_persoon	2013-01-22 14:55:29	2013-01-22 14:55:29
21	7	natuurlijk_persoon_na	2013-01-22 14:55:29	2013-01-22 14:55:29
22	7	medewerker	2013-01-22 14:55:29	2013-01-22 14:55:29
23	7	medewerker	2013-01-22 14:55:29	2013-01-22 14:55:29
24	8	natuurlijk_persoon	2013-01-22 15:45:00	2013-01-22 15:45:00
25	8	natuurlijk_persoon_na	2013-01-22 15:45:00	2013-01-22 15:45:00
26	8	medewerker	2013-01-22 15:45:00	2013-01-22 15:45:00
27	8	medewerker	2013-01-22 15:45:00	2013-01-22 15:45:00
28	9	natuurlijk_persoon	2013-01-22 15:57:18	2013-01-22 15:57:18
29	9	natuurlijk_persoon_na	2013-01-22 15:57:18	2013-01-22 15:57:18
30	9	medewerker	2013-01-22 15:57:18	2013-01-22 15:57:18
31	9	medewerker	2013-01-22 15:57:18	2013-01-22 15:57:18
32	10	natuurlijk_persoon	2013-01-24 13:58:38	2013-01-24 13:58:38
33	10	natuurlijk_persoon_na	2013-01-24 13:58:38	2013-01-24 13:58:38
34	10	medewerker	2013-01-24 13:58:38	2013-01-24 13:58:38
35	10	medewerker	2013-01-24 13:58:38	2013-01-24 13:58:38
36	11	natuurlijk_persoon	2013-01-24 14:06:12	2013-01-24 14:06:12
37	11	natuurlijk_persoon_na	2013-01-24 14:06:12	2013-01-24 14:06:12
38	11	medewerker	2013-01-24 14:06:12	2013-01-24 14:06:12
39	11	medewerker	2013-01-24 14:06:12	2013-01-24 14:06:12
40	12	natuurlijk_persoon	2013-01-24 14:08:56	2013-01-24 14:08:56
41	12	natuurlijk_persoon_na	2013-01-24 14:08:56	2013-01-24 14:08:56
42	12	medewerker	2013-01-24 14:08:56	2013-01-24 14:08:56
43	12	medewerker	2013-01-24 14:08:56	2013-01-24 14:08:56
44	13	natuurlijk_persoon	2013-01-24 14:11:44	2013-01-24 14:11:44
45	13	natuurlijk_persoon_na	2013-01-24 14:11:44	2013-01-24 14:11:44
46	13	medewerker	2013-01-24 14:11:44	2013-01-24 14:11:44
47	13	medewerker	2013-01-24 14:11:44	2013-01-24 14:11:44
48	14	natuurlijk_persoon	2013-01-24 14:29:24	2013-01-24 14:29:24
49	14	natuurlijk_persoon_na	2013-01-24 14:29:24	2013-01-24 14:29:24
50	14	medewerker	2013-01-24 14:29:24	2013-01-24 14:29:24
51	14	medewerker	2013-01-24 14:29:24	2013-01-24 14:29:24
\.


--
-- Name: zaaktype_betrokkenen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_betrokkenen_id_seq', 51, true);


--
-- Data for Name: zaaktype_definitie; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_definitie (id, openbaarheid, handelingsinitiator, grondslag, procesbeschrijving, afhandeltermijn, afhandeltermijn_type, selectielijst, servicenorm, servicenorm_type, pdc_voorwaarden, pdc_description, pdc_meenemen, pdc_tarief, omschrijving_upl, aard, extra_informatie, preset_client, extra_informatie_extern) FROM stdin;
6	gesloten	aangaan	nvt		5	kalenderdagen	nvt	5	kalenderdagen	\N	\N	\N	\N				\N	\N
7	gesloten	aangaan	nvt		5	kalenderdagen	nvt	5	kalenderdagen	\N	\N	\N	\N				\N	\N
8	gesloten	aangaan	nvt		5	kalenderdagen	nvt	5	kalenderdagen	\N	\N	\N	\N				\N	\N
9	gesloten	aangaan	nvt		5	kalenderdagen	nvt	5	kalenderdagen	\N	\N	\N	\N				\N	\N
10	gesloten	aangaan	nvt		5	kalenderdagen	nvt	5	kalenderdagen	\N	\N	\N	\N				\N	\N
11	gesloten	aangaan	nvt		5	kalenderdagen	nvt	5	kalenderdagen	\N	\N	\N	\N				\N	\N
12	gesloten	aangaan	nvt		5	kalenderdagen	nvt	5	kalenderdagen	\N	\N	\N	\N				\N	\N
13	gesloten	aanvragen	nvt		5	kalenderdagen	nvt	5	kalenderdagen	\N	\N	\N	\N				\N	\N
14	gesloten	aanvragen	nvt		5	kalenderdagen	nvt	5	kalenderdagen	\N	\N	\N	\N				\N	\N
\.


--
-- Name: zaaktype_definitie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_definitie_id_seq', 14, true);


--
-- Name: zaaktype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_id_seq', 6, true);


--
-- Data for Name: zaaktype_kenmerken; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_kenmerken (id, bibliotheek_kenmerken_id, value_mandatory, label, help, created, last_modified, zaaktype_node_id, zaak_status_id, pip, zaakinformatie_view, bag_zaakadres, value_default, pip_can_change, publish_public, referential, is_systeemkenmerk, required_permissions, version, help_extern, object_id, object_metadata, label_multiple, properties, is_group) FROM stdin;
3	1	\N	\N	\N	\N	\N	6	8	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
4	2	\N	\N	\N	\N	\N	6	8	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
5	3	\N	\N	\N	\N	\N	6	8	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
6	4	\N	\N	\N	\N	\N	6	10	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
7	5	\N	\N	\N	\N	\N	6	10	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
8	8	\N	\N	\N	\N	\N	6	10	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
9	6	\N	\N	\N	\N	\N	6	10	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
10	7	\N	\N	\N	\N	\N	6	10	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
12	1	\N	\N	\N	\N	\N	7	11	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
13	2	\N	\N	\N	\N	\N	7	11	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
14	3	\N	\N	\N	\N	\N	7	11	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
15	9	1		Vul hier uw voor- en achternaam	\N	\N	7	13	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
16	10	\N	\N	\N	\N	\N	7	13	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
18	4	\N	\N	\N	\N	\N	7	14	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
19	5	\N	\N	\N	\N	\N	7	14	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
20	8	\N	\N	\N	\N	\N	7	14	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
21	6	\N	\N	\N	\N	\N	7	14	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
22	7	\N	\N	\N	\N	\N	7	14	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
23	11	\N	\N	\N	\N	\N	8	15	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
25	1	\N	\N	\N	\N	\N	8	16	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
26	2	\N	\N	\N	\N	\N	8	16	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
27	3	\N	\N	\N	\N	\N	8	16	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
29	9	1		Vul hier uw voor- en achternaam	\N	\N	8	17	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
30	10	\N	\N	\N	\N	\N	8	17	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
32	4	\N	\N	\N	\N	\N	8	18	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
33	5	\N	\N	\N	\N	\N	8	18	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
34	8	\N	\N	\N	\N	\N	8	18	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
35	6	\N	\N	\N	\N	\N	8	18	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
36	7	\N	\N	\N	\N	\N	8	18	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
38	3	\N	\N	\N	\N	\N	9	19	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
39	2	\N	\N	\N	\N	\N	9	19	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
40	1	\N	\N	\N	\N	\N	9	19	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
42	11	\N	\N	\N	\N	\N	9	20	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
44	9	1		Vul hier uw voor- en achternaam	\N	\N	9	21	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
45	10	\N	\N	\N	\N	\N	9	21	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
47	4	\N	\N	\N	\N	\N	9	22	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
48	5	\N	\N	\N	\N	\N	9	22	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
49	8	\N	\N	\N	\N	\N	9	22	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
50	6	\N	\N	\N	\N	\N	9	22	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
51	7	\N	\N	\N	\N	\N	9	22	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
53	11	\N	\N	\N	\N	\N	10	23	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
55	3	\N	\N	\N	\N	\N	10	24	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
56	2	\N	\N	\N	\N	\N	10	24	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
57	1	\N	\N	\N	\N	\N	10	24	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
59	9	1		Vul hier uw voor- en achternaam	\N	\N	10	25	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
60	10	\N	\N	\N	\N	\N	10	25	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
62	4	\N	\N	\N	\N	\N	10	26	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
63	5	\N	\N	\N	\N	\N	10	26	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
64	8	\N	\N	\N	\N	\N	10	26	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
65	6	\N	\N	\N	\N	\N	10	26	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
66	7	\N	\N	\N	\N	\N	10	26	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
68	3	\N	\N	\N	\N	\N	11	27	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
69	2	\N	\N	\N	\N	\N	11	27	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
70	1	\N	\N	\N	\N	\N	11	27	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
71	12	1			\N	\N	11	27	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
73	11	\N	\N	\N	\N	\N	11	28	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
75	9	1		Vul hier uw voor- en achternaam	\N	\N	11	29	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
76	10	\N	\N	\N	\N	\N	11	29	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
78	4	\N	\N	\N	\N	\N	11	30	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
79	5	\N	\N	\N	\N	\N	11	30	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
80	8	\N	\N	\N	\N	\N	11	30	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
81	6	1			\N	\N	11	30	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
82	7	\N	\N	\N	\N	\N	11	30	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
84	11	\N	\N	\N	\N	\N	12	31	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
86	3	\N	\N	\N	\N	\N	12	32	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
87	2	\N	\N	\N	\N	\N	12	32	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
88	1	\N	\N	\N	\N	\N	12	32	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
89	12	1			\N	\N	12	32	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
91	9	1		Vul hier uw voor- en achternaam	\N	\N	12	33	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
92	10	\N	\N	\N	\N	\N	12	33	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
94	4	\N	\N	\N	\N	\N	12	34	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
95	5	\N	\N	\N	\N	\N	12	34	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
96	8	\N	\N	\N	\N	\N	12	34	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
97	6	1			\N	\N	12	34	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
98	7	\N	\N	\N	\N	\N	12	34	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
118	3	\N	\N	\N	\N	\N	14	40	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
100	3	\N	\N	\N	\N	\N	13	35	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
101	2	\N	\N	\N	\N	\N	13	35	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
102	1	\N	\N	\N	\N	\N	13	35	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
103	12	1			\N	\N	13	35	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
105	11	\N	\N	\N	\N	\N	13	36	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
107	9	1		Vul hier uw voor- en achternaam	\N	\N	13	37	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
108	10	\N	\N	\N	\N	\N	13	37	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
110	4	\N	\N	\N	\N	\N	13	38	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
111	5	\N	\N	\N	\N	\N	13	38	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
112	8	\N	\N	\N	\N	\N	13	38	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
113	6	1			\N	\N	13	38	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
114	7	\N	\N	\N	\N	\N	13	38	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
116	11	\N	\N	\N	\N	\N	14	39	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
119	2	\N	\N	\N	\N	\N	14	40	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
120	1	\N	\N	\N	\N	\N	14	40	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
121	12	1			\N	\N	14	40	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
123	9	1		Vul hier uw voor- en achternaam	\N	\N	14	41	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
124	10	\N	\N	\N	\N	\N	14	41	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
126	4	\N	\N	\N	\N	\N	14	42	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
127	5	\N	\N	\N	\N	\N	14	42	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
128	8	\N	\N	\N	\N	\N	14	42	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
129	6	1			\N	\N	14	42	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
130	7	\N	\N	\N	\N	\N	14	42	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	f
11	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	7	11	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
17	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	7	14	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
24	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	8	16	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
28	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	8	17	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
31	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	8	18	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
37	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	9	19	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
41	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	9	20	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
43	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	9	21	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
46	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	9	22	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
52	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	10	23	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
54	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	10	24	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
58	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	10	25	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
61	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	10	26	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
67	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	11	27	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
72	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	11	28	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
74	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	11	29	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
77	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	11	30	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
83	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	12	31	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
85	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	12	32	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
90	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	12	33	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
93	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	12	34	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
99	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	13	35	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
104	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	13	36	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
106	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	13	37	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
109	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	13	38	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
115	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	14	39	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
117	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	14	40	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
122	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	14	41	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
125	\N	\N	Benodigde gegevens	Vul de benodigde velden in voor uw zaak	\N	\N	14	42	\N	1	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	{}	\N	{}	t
\.


--
-- Name: zaaktype_kenmerken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_kenmerken_id_seq', 130, true);


--
-- Data for Name: zaaktype_node; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_node (id, zaaktype_id, zaaktype_rt_queue, code, trigger, titel, version, active, created, last_modified, deleted, webform_toegang, webform_authenticatie, adres_relatie, aanvrager_hergebruik, automatisch_aanvragen, automatisch_behandelen, toewijzing_zaakintake, toelichting, online_betaling, zaaktype_definitie_id, adres_andere_locatie, adres_aanvrager, bedrijfid_wijzigen, zaaktype_vertrouwelijk, zaaktype_trefwoorden, zaaktype_omschrijving, extra_relaties_in_aanvraag, properties, contact_info_intake, is_public, prevent_pip, contact_info_email_required, contact_info_phone_required, contact_info_mobile_phone_required, moeder_zaaktype_id, logging_id, uuid) FROM stdin;
6	6	\N	1	internextern	Testzaaktype	1	\N	2013-01-22 14:40:10	2013-01-22 14:55:29	2013-01-22 14:55:29	\N	\N	\N	\N	\N	\N	\N	\N	\N	6	\N	\N	\N	\N	testing scenario	Alomvattend testzaaktype	t	{}	\N	f	f	t	f	f	\N	\N	6a54f9ed-427b-4d1a-80dd-b16e8442744f
7	6	\N	1	internextern	Testzaaktype	2	\N	2013-01-22 14:55:29	2013-01-22 15:45:00	2013-01-22 15:45:00	\N	\N	\N	\N	\N	\N	\N	\N	\N	7	\N	\N	\N	\N	testing scenario	Alomvattend testzaaktype	t	{}	\N	f	f	t	f	f	\N	\N	a9dcffbf-68ae-4851-9154-ff30c08e2618
8	6	\N	1	internextern	Testzaaktype	3	\N	2013-01-22 15:45:00	2013-01-22 15:57:18	2013-01-22 15:57:18	\N	\N	\N	\N	\N	\N	\N	\N	\N	8	\N	\N	\N	\N	testing scenario	Alomvattend testzaaktype	t	{}	\N	f	f	t	f	f	\N	\N	a0fbdf39-aed6-4b0d-aff6-24580ca054db
9	6	\N	1	internextern	Testzaaktype	4	\N	2013-01-22 15:57:17	2013-01-24 13:58:38	2013-01-24 13:58:38	\N	\N	\N	\N	\N	\N	\N	\N	\N	9	\N	\N	\N	\N	testing scenario	Alomvattend testzaaktype	t	{}	\N	f	f	t	f	f	\N	\N	fdee991e-bae6-4cc3-974f-8eb62f10486d
10	6	\N	1	internextern	Testzaaktype	5	\N	2013-01-24 13:58:38	2013-01-24 14:06:12	2013-01-24 14:06:12	\N	\N	\N	\N	\N	\N	\N	\N	\N	10	\N	\N	\N	\N	testing scenario	Alomvattend testzaaktype	t	{}	\N	f	f	t	f	f	\N	\N	c68cc18d-211b-44d3-95d6-613f99a2593c
11	6	\N	1	internextern	Testzaaktype	6	\N	2013-01-24 14:06:12	2013-01-24 14:08:56	2013-01-24 14:08:56	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	\N	\N	\N	\N	testing scenario	Alomvattend testzaaktype	t	{}	\N	f	f	t	f	f	\N	\N	9837ab10-1c5c-4464-a6ba-9584ec712bb3
12	6	\N	1	internextern	Testzaaktype	7	\N	2013-01-24 14:08:56	2013-01-24 14:11:44	2013-01-24 14:11:44	\N	\N	\N	\N	\N	\N	\N	\N	\N	12	\N	\N	\N	\N	testing scenario	Alomvattend testzaaktype	t	{}	\N	f	f	t	f	f	\N	\N	55f3a259-9b49-4a74-9ace-3ccfa9516ddb
13	6	\N	1	internextern	Testzaaktype	8	\N	2013-01-24 14:11:44	2013-01-24 14:29:24	2013-01-24 14:29:24	\N	\N	\N	\N	\N	\N	\N	\N	\N	13	\N	\N	\N	\N	testing scenario	Alomvattend testzaaktype	t	{}	\N	f	f	t	f	f	\N	\N	7fa5e33e-e2dc-42b6-afd9-05476a921acd
14	6	\N	1	internextern	Testzaaktype	9	\N	2013-01-24 14:29:24	2013-01-24 14:29:24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	14	\N	\N	\N	\N	testing scenario	Alomvattend testzaaktype	t	{}	\N	f	f	t	f	f	\N	\N	126cf82a-6581-47b0-90f5-776b8eb8bbeb
\.


--
-- Name: zaaktype_node_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_node_id_seq', 14, true);


--
-- Data for Name: zaaktype_notificatie; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_notificatie (id, zaaktype_node_id, zaak_status_id, label, rcpt, onderwerp, bericht, created, last_modified, intern_block, email, bibliotheek_notificaties_id, behandelaar, automatic, cc, bcc, betrokkene_role) FROM stdin;
2	7	13	\N	aanvrager	\N	\N	\N	\N	\N		5		\N	\N	\N	\N
3	7	13	\N	aanvrager	\N	\N	\N	\N	\N		4		\N	\N	\N	\N
4	8	17	\N	aanvrager	\N	\N	\N	\N	\N		5		\N	\N	\N	\N
5	8	17	\N	aanvrager	\N	\N	\N	\N	\N		4		\N	\N	\N	\N
7	9	21	\N	aanvrager	\N	\N	\N	\N	\N		5		\N	\N	\N	\N
8	9	21	\N	aanvrager	\N	\N	\N	\N	\N		4		\N	\N	\N	\N
10	10	25	\N	aanvrager	\N	\N	\N	\N	\N		5		\N	\N	\N	\N
11	10	25	\N	aanvrager	\N	\N	\N	\N	\N		4		\N	\N	\N	\N
13	11	29	\N	aanvrager	\N	\N	\N	\N	\N		5		\N	\N	\N	\N
14	11	29	\N	aanvrager	\N	\N	\N	\N	\N		4		\N	\N	\N	\N
16	12	33	\N	aanvrager	\N	\N	\N	\N	\N		5		\N	\N	\N	\N
17	12	33	\N	aanvrager	\N	\N	\N	\N	\N		4		\N	\N	\N	\N
19	13	37	\N	aanvrager	\N	\N	\N	\N	\N		5		\N	\N	\N	\N
20	13	37	\N	aanvrager	\N	\N	\N	\N	\N		4		\N	\N	\N	\N
22	14	41	\N	coordinator	\N	\N	\N	\N	\N		5		\N	\N	\N	\N
23	14	41	\N	coordinator	\N	\N	\N	\N	\N		4		\N	\N	\N	\N
6	9	19	\N	aanvrager	\N	\N	\N	\N	\N		3		1	\N	\N	\N
9	10	24	\N	aanvrager	\N	\N	\N	\N	\N		3		1	\N	\N	\N
12	11	27	\N	aanvrager	\N	\N	\N	\N	\N		3		1	\N	\N	\N
15	12	32	\N	aanvrager	\N	\N	\N	\N	\N		3		1	\N	\N	\N
18	13	35	\N	aanvrager	\N	\N	\N	\N	\N		3		1	\N	\N	\N
21	14	40	\N	aanvrager	\N	\N	\N	\N	\N		3		1	\N	\N	\N
\.


--
-- Name: zaaktype_notificatie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_notificatie_id_seq', 23, true);


--
-- Data for Name: zaaktype_regel; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_regel (id, zaaktype_node_id, zaak_status_id, naam, created, last_modified, settings, active, is_group) FROM stdin;
8	9	21	Als test 09 = ja dan 	2013-01-22 15:57:18	2013-01-22 15:57:18	^JSON|||hex|^7b22766f6f727761617264656e223a2231222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a223130222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224a61222c226d696a6c736f7274223a2231222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f31223a22736a61626c6f6f6e5f67656e65726572656e222c226e61616d223a22416c732074657374203039203d206a612064616e20227d	t	f
7	9	19	Als test 03 = optie A toon test 02	2013-01-22 15:57:18	2013-01-22 15:57:18	^JSON|||hex|^7b22766f6f727761617264656e223a2231222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2232222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2233222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224f707469652041222c226d696a6c736f7274223a2231222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2232222c226e61616d223a22416c732074657374203033203d206f70746965204120746f6f6e2074657374203032222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
9	9	22	Als test 07 = ja toon test 05	2013-01-22 15:57:18	2013-01-22 15:57:18	^JSON|||hex|^7b22766f6f727761617264656e223a2231222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2236222c22766f6f727761617264655f315f6b656e6d65726b223a2238222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224a61222c226d696a6c736f7274223a2231222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2236222c226e61616d223a22416c732074657374203037203d206a6120746f6f6e2074657374203035222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
10	9	22	Als test 07 = nee toon test 06	2013-01-22 15:57:18	2013-01-22 15:57:18	^JSON|||hex|^7b22766f6f727761617264656e223a2231222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2237222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2238222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224e6565222c226d696a6c736f7274223a2232222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2237222c226e61616d223a22416c732074657374203037203d206e656520746f6f6e2074657374203036222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
11	10	24	Als test 03 = optie A toon test 02	2013-01-24 13:58:38	2013-01-24 13:58:38	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a392c22766f6f727761617264656e223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2233222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224f707469652041222c227a61616b5f7374617475735f6964223a31392c226964223a372c226e61616d223a22416c732074657374203033203d206f70746965204120746f6f6e2074657374203032222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2232222c226d696a6c736f7274223a2231222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2232222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
12	10	25	Als test 09 = ja dan 	2013-01-24 13:58:38	2013-01-24 13:58:38	^JSON|||hex|^7b22766f6f727761617264656e223a2231222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a223130222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224a61222c226d696a6c736f7274223a2231222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f76616c7565223a22222c2261637469655f315f6b656e6d65726b223a2239222c226e61616d223a22416c732074657374203039203d206a612064616e20222c2261637469655f31223a22736a61626c6f6f6e5f67656e65726572656e227d	t	f
13	10	26	Als test 07 = ja toon test 05	2013-01-24 13:58:38	2013-01-24 13:58:38	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a392c22766f6f727761617264656e223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2238222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224a61222c227a61616b5f7374617475735f6964223a32322c226964223a392c226e61616d223a22416c732074657374203037203d206a6120746f6f6e2074657374203035222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2236222c226d696a6c736f7274223a2231222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2236222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
14	10	26	Als test 07 = nee toon test 06	2013-01-24 13:58:38	2013-01-24 13:58:38	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a392c22766f6f727761617264656e223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2238222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224e6565222c227a61616b5f7374617475735f6964223a32322c226964223a31302c226e61616d223a22416c732074657374203037203d206e656520746f6f6e2074657374203036222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2237222c226d696a6c736f7274223a2232222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2237222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
15	11	27	Als test 03 = optie A toon test 02	2013-01-24 14:06:12	2013-01-24 14:06:12	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a392c22766f6f727761617264656e223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2233222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224f707469652041222c227a61616b5f7374617475735f6964223a31392c226964223a372c226e61616d223a22416c732074657374203033203d206f70746965204120746f6f6e2074657374203032222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2232222c226d696a6c736f7274223a2231222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2232222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
16	11	27	Als test 01 = 1 en/of 2 toon kenmerk test A	2013-01-24 14:06:12	2013-01-24 14:06:12	^JSON|||hex|^7b22766f6f727761617264656e223a2231222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a5b224f707469652031222c224f707469652032225d2c226d696a6c736f7274223a2232222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a223132222c226e61616d223a22416c732074657374203031203d203120656e2f6f66203220746f6f6e206b656e6d65726b20746573742041222c2261637469655f31223a22746f6f6e5f6b656e6d65726b227d	t	f
17	11	29	Als test 09 = ja dan genereer sjabloon positief	2013-01-24 14:06:12	2013-01-24 14:06:12	^JSON|||hex|^7b22766f6f727761617264656e223a2231222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a223130222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224a61222c226d696a6c736f7274223a2231222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f736a61626c6f6f6e223a2231222c2261637469655f31223a22736a61626c6f6f6e5f67656e65726572656e222c226e61616d223a22416c732074657374203039203d206a612064616e2067656e657265657220736a61626c6f6f6e20706f736974696566227d	t	f
18	11	29	Als test 09 = nee dan genereer sjabloon negatief	2013-01-24 14:06:12	2013-01-24 14:06:12	^JSON|||hex|^7b22766f6f727761617264656e223a2231222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a223130222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224e6565222c226d696a6c736f7274223a2232222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f736a61626c6f6f6e223a2232222c226e61616d223a22416c732074657374203039203d206e65652064616e2067656e657265657220736a61626c6f6f6e206e65676174696566222c2261637469655f31223a22736a61626c6f6f6e5f67656e65726572656e227d	t	f
19	11	30	Als test 07 = ja toon test 05	2013-01-24 14:06:13	2013-01-24 14:06:13	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a392c22766f6f727761617264656e223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2238222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224a61222c227a61616b5f7374617475735f6964223a32322c226964223a392c226e61616d223a22416c732074657374203037203d206a6120746f6f6e2074657374203035222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2236222c226d696a6c736f7274223a2231222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2236222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
20	11	30	Als test 07 = nee toon test 06	2013-01-24 14:06:13	2013-01-24 14:06:13	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a392c22766f6f727761617264656e223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2238222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224e6565222c227a61616b5f7374617475735f6964223a32322c226964223a31302c226e61616d223a22416c732074657374203037203d206e656520746f6f6e2074657374203036222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2237222c226d696a6c736f7274223a2232222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2237222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
21	12	32	Als test 03 = optie A toon test 02	2013-01-24 14:08:56	2013-01-24 14:08:56	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a392c22766f6f727761617264656e223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2233222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224f707469652041222c227a61616b5f7374617475735f6964223a31392c226964223a372c226e61616d223a22416c732074657374203033203d206f70746965204120746f6f6e2074657374203032222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2232222c226d696a6c736f7274223a2231222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2232222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
22	12	32	Als test 01 = 1 en/of 2 toon kenmerk test A	2013-01-24 14:08:56	2013-01-24 14:08:56	^JSON|||hex|^7b22766f6f727761617264656e223a2231222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a223132222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a5b224f707469652031222c224f707469652032225d2c226d696a6c736f7274223a2232222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a223132222c226e61616d223a22416c732074657374203031203d203120656e2f6f66203220746f6f6e206b656e6d65726b20746573742041222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
23	12	33	Als test 09 = ja dan genereer sjabloon positief	2013-01-24 14:08:56	2013-01-24 14:08:56	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a31312c22766f6f727761617264656e223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a223130222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224a61222c226d696a6c736f7274223a2231222c227a61616b5f7374617475735f6964223a32392c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c226964223a31372c2261637469655f315f736a61626c6f6f6e223a2231222c226e61616d223a22416c732074657374203039203d206a612064616e2067656e657265657220736a61626c6f6f6e20706f736974696566222c2261637469655f31223a22736a61626c6f6f6e5f67656e65726572656e227d	t	f
24	12	33	Als test 09 = nee dan genereer sjabloon negatief	2013-01-24 14:08:56	2013-01-24 14:08:56	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a31312c22766f6f727761617264656e223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a223130222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224e6565222c226d696a6c736f7274223a2232222c227a61616b5f7374617475735f6964223a32392c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c226964223a31382c2261637469655f315f736a61626c6f6f6e223a2232222c226e61616d223a22416c732074657374203039203d206e65652064616e2067656e657265657220736a61626c6f6f6e206e65676174696566222c2261637469655f31223a22736a61626c6f6f6e5f67656e65726572656e227d	t	f
25	12	34	Als test 07 = ja toon test 05	2013-01-24 14:08:56	2013-01-24 14:08:56	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a392c22766f6f727761617264656e223a2231222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2238222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224a61222c227a61616b5f7374617475735f6964223a32322c226964223a392c226e61616d223a22416c732074657374203037203d206a6120746f6f6e2074657374203035222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2236222c226d696a6c736f7274223a2231222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2236222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
26	12	34	Als test 07 = nee toon test 06	2013-01-24 14:08:56	2013-01-24 14:08:56	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a392c22766f6f727761617264656e223a2231222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2238222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224e6565222c227a61616b5f7374617475735f6964223a32322c226964223a31302c226e61616d223a22416c732074657374203037203d206e656520746f6f6e2074657374203036222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2237222c226d696a6c736f7274223a2232222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2237222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
27	13	35	Als test 03 = optie A toon test 02	2013-01-24 14:11:44	2013-01-24 14:11:44	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a392c22766f6f727761617264656e223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2233222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224f707469652041222c227a61616b5f7374617475735f6964223a31392c226964223a372c226e61616d223a22416c732074657374203033203d206f70746965204120746f6f6e2074657374203032222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2232222c226d696a6c736f7274223a2231222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2232222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
28	13	35	Als test 01 = 1 en/of 2 toon kenmerk test A	2013-01-24 14:11:44	2013-01-24 14:11:44	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a31322c22766f6f727761617264656e223a2231222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a5b224f707469652031222c224f707469652032225d2c227a61616b5f7374617475735f6964223a33322c226964223a32322c226e61616d223a22416c732074657374203031203d203120656e2f6f66203220746f6f6e206b656e6d65726b20746573742041222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a223132222c226d696a6c736f7274223a2232222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a223132222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
29	13	37	Als test 09 = ja dan genereer sjabloon positief	2013-01-24 14:11:44	2013-01-24 14:11:44	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a31312c22766f6f727761617264656e223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a223130222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224a61222c226d696a6c736f7274223a2231222c227a61616b5f7374617475735f6964223a32392c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f736a61626c6f6f6e223a2231222c226964223a31372c2261637469655f31223a22736a61626c6f6f6e5f67656e65726572656e222c226e61616d223a22416c732074657374203039203d206a612064616e2067656e657265657220736a61626c6f6f6e20706f736974696566227d	t	f
30	13	37	Als test 09 = nee dan genereer sjabloon negatief	2013-01-24 14:11:44	2013-01-24 14:11:44	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a31312c22766f6f727761617264656e223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a223130222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224e6565222c226d696a6c736f7274223a2232222c227a61616b5f7374617475735f6964223a32392c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f736a61626c6f6f6e223a2232222c226964223a31382c2261637469655f31223a22736a61626c6f6f6e5f67656e65726572656e222c226e61616d223a22416c732074657374203039203d206e65652064616e2067656e657265657220736a61626c6f6f6e206e65676174696566227d	t	f
31	13	38	Als test 07 = ja toon test 05	2013-01-24 14:11:44	2013-01-24 14:11:44	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a392c22766f6f727761617264656e223a2231222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2238222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224a61222c227a61616b5f7374617475735f6964223a32322c226964223a392c226e61616d223a22416c732074657374203037203d206a6120746f6f6e2074657374203035222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2236222c226d696a6c736f7274223a2231222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2236222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
32	13	38	Als test 07 = nee toon test 06	2013-01-24 14:11:44	2013-01-24 14:11:44	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a392c22766f6f727761617264656e223a2231222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2238222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224e6565222c227a61616b5f7374617475735f6964223a32322c226964223a31302c226e61616d223a22416c732074657374203037203d206e656520746f6f6e2074657374203036222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2237222c226d696a6c736f7274223a2232222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2237222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
33	14	40	Als test 03 = optie A toon test 02	2013-01-24 14:29:24	2013-01-24 14:29:24	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a392c22766f6f727761617264656e223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2233222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224f707469652041222c227a61616b5f7374617475735f6964223a31392c226964223a372c226e61616d223a22416c732074657374203033203d206f70746965204120746f6f6e2074657374203032222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2232222c226d696a6c736f7274223a2231222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2232222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
34	14	40	Als test 01 = 1 en/of 2 toon kenmerk test A	2013-01-24 14:29:24	2013-01-24 14:29:24	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a31322c22766f6f727761617264656e223a2231222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a5b224f707469652031222c224f707469652032225d2c227a61616b5f7374617475735f6964223a33322c226964223a32322c226e61616d223a22416c732074657374203031203d203120656e2f6f66203220746f6f6e206b656e6d65726b20746573742041222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a223132222c226d696a6c736f7274223a2232222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a223132222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
35	14	41	Als test 09 = ja dan genereer sjabloon positief	2013-01-24 14:29:24	2013-01-24 14:29:24	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a31312c22766f6f727761617264656e223a2231222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a223130222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224a61222c226d696a6c736f7274223a2231222c227a61616b5f7374617475735f6964223a32392c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f736a61626c6f6f6e223a2231222c226964223a31372c2261637469655f31223a22736a61626c6f6f6e5f67656e65726572656e222c226e61616d223a22416c732074657374203039203d206a612064616e2067656e657265657220736a61626c6f6f6e20706f736974696566227d	t	f
36	14	41	Als test 09 = nee dan genereer sjabloon negatief	2013-01-24 14:29:24	2013-01-24 14:29:24	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a31312c22766f6f727761617264656e223a2231222c22616374696573223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a223130222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224e6565222c226d696a6c736f7274223a2232222c227a61616b5f7374617475735f6964223a32392c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f736a61626c6f6f6e223a2232222c226964223a31382c2261637469655f31223a22736a61626c6f6f6e5f67656e65726572656e222c226e61616d223a22416c732074657374203039203d206e65652064616e2067656e657265657220736a61626c6f6f6e206e65676174696566227d	t	f
37	14	42	Als test 07 = ja toon test 05	2013-01-24 14:29:24	2013-01-24 14:29:24	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a392c22766f6f727761617264656e223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2238222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224a61222c227a61616b5f7374617475735f6964223a32322c226964223a392c226e61616d223a22416c732074657374203037203d206a6120746f6f6e2074657374203035222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2236222c226d696a6c736f7274223a2231222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2236222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
38	14	42	Als test 07 = nee toon test 06	2013-01-24 14:29:24	2013-01-24 14:29:24	^JSON|||hex|^7b227a61616b747970655f6e6f64655f6964223a392c22766f6f727761617264656e223a2231222c22766f6f727761617264655f315f6b656e6d65726b223a2238222c22616374696573223a2231222c226269626c696f746865656b5f6b656e6d65726b656e5f6964223a6e756c6c2c22766f6f727761617264655f315f76616c7565223a224e6565222c227a61616b5f7374617475735f6964223a32322c226964223a31302c226e61616d223a22416c732074657374203037203d206e656520746f6f6e2074657374203036222c22616e6465725f32223a22766572626572675f6b656e6d65726b222c22616e6465725f325f6b656e6d65726b223a2237222c226d696a6c736f7274223a2232222c22766f6f727761617264655f315f76616c75655f636865636b626f78223a2231222c2261637469655f315f6b656e6d65726b223a2237222c2261637469655f31223a22746f6f6e5f6b656e6d65726b222c22616e64657273223a2232227d	t	f
\.


--
-- Name: zaaktype_regel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_regel_id_seq', 38, true);


--
-- Data for Name: zaaktype_relatie; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_relatie (id, zaaktype_node_id, relatie_zaaktype_id, zaaktype_status_id, relatie_type, eigenaar_type, start_delay, created, last_modified, status, kopieren_kenmerken, ou_id, role_id, automatisch_behandelen, required, betrokkene_authorized, betrokkene_notify, betrokkene_id, betrokkene_role, betrokkene_role_set, betrokkene_prefix, eigenaar_id, eigenaar_role, show_in_pip, pip_label, subject_role, copy_subject_role) FROM stdin;
\.


--
-- Name: zaaktype_relatie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_relatie_id_seq', 4, true);


--
-- Data for Name: zaaktype_resultaten; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_resultaten (id, zaaktype_node_id, zaaktype_status_id, resultaat, ingang, bewaartermijn, created, last_modified, dossiertype, label, selectielijst, archiefnominatie, comments, external_reference, trigger_archival, selectielijst_brondatum, selectielijst_einddatum, properties, standaard_keuze) FROM stdin;
1	7	12	verwerkt	vervallen	93	2013-01-22 14:55:29	2013-01-22 14:55:29	digitaal	\N	nvt	\N	\N	\N	t	\N	\N	{}	f
2	8	15	afgewezen	vervallen	93	2013-01-22 15:45:00	2013-01-22 15:45:00	digitaal	\N	nvt	\N	\N	\N	t	\N	\N	{}	f
3	8	15	toegekend	vervallen	93	2013-01-22 15:45:00	2013-01-22 15:45:00	digitaal	\N	nvt	\N	\N	\N	t	\N	\N	{}	f
4	9	20	afgewezen	vervallen	93	2013-01-22 15:57:18	2013-01-22 15:57:18	digitaal	\N	nvt	\N	\N	\N	t	\N	\N	{}	f
5	9	20	toegekend	vervallen	93	2013-01-22 15:57:18	2013-01-22 15:57:18	digitaal	\N	nvt	\N	\N	\N	t	\N	\N	{}	f
6	10	23	afgewezen	vervallen	93	2013-01-24 13:58:38	2013-01-24 13:58:38	digitaal	\N	nvt	\N	\N	\N	t	\N	\N	{}	f
7	10	23	toegekend	vervallen	93	2013-01-24 13:58:38	2013-01-24 13:58:38	digitaal	\N	nvt	\N	\N	\N	t	\N	\N	{}	f
8	11	28	afgewezen	vervallen	93	2013-01-24 14:06:12	2013-01-24 14:06:12	digitaal	\N	nvt	\N	\N	\N	t	\N	\N	{}	f
9	11	28	toegekend	vervallen	93	2013-01-24 14:06:12	2013-01-24 14:06:12	digitaal	\N	nvt	\N	\N	\N	t	\N	\N	{}	f
10	12	31	afgewezen	vervallen	93	2013-01-24 14:08:56	2013-01-24 14:08:56	digitaal	\N	nvt	\N	\N	\N	t	\N	\N	{}	f
11	12	31	toegekend	vervallen	93	2013-01-24 14:08:56	2013-01-24 14:08:56	digitaal	\N	nvt	\N	\N	\N	t	\N	\N	{}	f
12	13	36	afgewezen	vervallen	93	2013-01-24 14:11:44	2013-01-24 14:11:44	digitaal	\N	nvt	\N	\N	\N	t	\N	\N	{}	f
13	13	36	toegekend	vervallen	93	2013-01-24 14:11:44	2013-01-24 14:11:44	digitaal	\N	nvt	\N	\N	\N	t	\N	\N	{}	f
14	14	39	afgewezen	vervallen	93	2013-01-24 14:29:24	2013-01-24 14:29:24	digitaal	\N	nvt	\N	\N	\N	t	\N	\N	{}	f
15	14	39	toegekend	vervallen	93	2013-01-24 14:29:24	2013-01-24 14:29:24	digitaal	\N	nvt	\N	\N	\N	t	\N	\N	{}	f
\.


--
-- Name: zaaktype_resultaten_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_resultaten_id_seq', 15, true);


--
-- Data for Name: zaaktype_sjablonen; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_sjablonen (id, zaaktype_node_id, bibliotheek_sjablonen_id, help, zaak_status_id, created, last_modified, automatisch_genereren, bibliotheek_kenmerken_id, target_format) FROM stdin;
\.


--
-- Name: zaaktype_sjablonen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_sjablonen_id_seq', 10, true);


--
-- Data for Name: zaaktype_standaard_betrokkenen; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_standaard_betrokkenen (id, zaaktype_node_id, zaak_status_id, betrokkene_type, betrokkene_identifier, naam, rol, magic_string_prefix, gemachtigd, notify, uuid) FROM stdin;
\.


--
-- Name: zaaktype_standaard_betrokkenen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_standaard_betrokkenen_id_seq', 1, false);


--
-- Data for Name: zaaktype_status; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_status (id, zaaktype_node_id, status, status_type, naam, created, last_modified, ou_id, role_id, checklist, fase, role_set) FROM stdin;
9	6	3	\N	Beoordeeld	2013-01-22 14:40:10	2013-01-22 14:40:10	10011	20007	\N	Beoordeling	1
10	6	2	\N	Behandeld	2013-01-22 14:40:10	2013-01-22 14:40:10	10011	20007	\N	Behandeling	1
13	7	3	\N	Beoordeeld	2013-01-22 14:55:29	2013-01-22 14:55:29	10011	20007	\N	Beoordeling	1
14	7	2	\N	Behandeld	2013-01-22 14:55:29	2013-01-22 14:55:29	10011	20007	\N	Behandeling	1
17	8	3	\N	Beoordeeld	2013-01-22 15:45:00	2013-01-22 15:45:00	10011	20007	\N	Beoordeling	1
18	8	2	\N	Behandeld	2013-01-22 15:45:00	2013-01-22 15:45:00	10020	20002	\N	Behandeling	1
21	9	3	\N	Beoordeeld	2013-01-22 15:57:18	2013-01-22 15:57:18	10011	20007	\N	Beoordeling	1
22	9	2	\N	Behandeld	2013-01-22 15:57:18	2013-01-22 15:57:18	10020	20002	\N	Behandeling	1
25	10	3	\N	Beoordeeld	2013-01-24 13:58:38	2013-01-24 13:58:38	10011	20007	\N	Beoordeling	1
26	10	2	\N	Behandeld	2013-01-24 13:58:38	2013-01-24 13:58:38	10020	20002	\N	Behandeling	1
29	11	3	\N	Beoordeeld	2013-01-24 14:06:12	2013-01-24 14:06:12	10011	20007	\N	Beoordeling	1
30	11	2	\N	Behandeld	2013-01-24 14:06:13	2013-01-24 14:06:13	10020	20002	\N	Behandeling	1
33	12	3	\N	Beoordeeld	2013-01-24 14:08:56	2013-01-24 14:08:56	10011	20007	\N	Beoordeling	1
34	12	2	\N	Behandeld	2013-01-24 14:08:56	2013-01-24 14:08:56	10020	20002	\N	Behandeling	1
37	13	3	\N	Beoordeeld	2013-01-24 14:11:44	2013-01-24 14:11:44	10011	20007	\N	Beoordeling	1
38	13	2	\N	Behandeld	2013-01-24 14:11:44	2013-01-24 14:11:44	10020	20002	\N	Behandeling	1
41	14	3	\N	Beoordeeld	2013-01-24 14:29:24	2013-01-24 14:29:24	10011	20007	\N	Beoordeling	1
42	14	2	\N	Behandeld	2013-01-24 14:29:24	2013-01-24 14:29:24	10020	20002	\N	Behandeling	1
8	6	1	\N	Geregistreerd	2013-01-22 14:40:10	2013-01-22 14:40:10	10011	20007	\N	Registreren	\N
11	7	1	\N	Geregistreerd	2013-01-22 14:55:29	2013-01-22 14:55:29	10011	20007	\N	Registreren	\N
16	8	1	\N	Geregistreerd	2013-01-22 15:45:00	2013-01-22 15:45:00	10011	20007	\N	Registreren	\N
19	9	1	\N	Geregistreerd	2013-01-22 15:57:18	2013-01-22 15:57:18	10011	20007	\N	Registreren	\N
24	10	1	\N	Geregistreerd	2013-01-24 13:58:38	2013-01-24 13:58:38	10011	20007	\N	Registreren	\N
27	11	1	\N	Geregistreerd	2013-01-24 14:06:12	2013-01-24 14:06:12	10011	20007	\N	Registreren	\N
32	12	1	\N	Geregistreerd	2013-01-24 14:08:56	2013-01-24 14:08:56	10011	20007	\N	Registreren	\N
35	13	1	\N	Geregistreerd	2013-01-24 14:11:44	2013-01-24 14:11:44	10011	20007	\N	Registreren	\N
40	14	1	\N	Geregistreerd	2013-01-24 14:29:24	2013-01-24 14:29:24	10011	20007	\N	Registreren	\N
7	6	4	\N	Afgehandeld	2013-01-22 14:40:10	2013-01-22 14:40:10	10011	20007	\N	Afhandelen	\N
12	7	4	\N	Afgehandeld	2013-01-22 14:55:29	2013-01-22 14:55:29	10011	20007	\N	Afhandelen	\N
15	8	4	\N	Afgehandeld	2013-01-22 15:45:00	2013-01-22 15:45:00	10011	20007	\N	Afhandelen	\N
20	9	4	\N	Afgehandeld	2013-01-22 15:57:18	2013-01-22 15:57:18	10011	20007	\N	Afhandelen	\N
23	10	4	\N	Afgehandeld	2013-01-24 13:58:38	2013-01-24 13:58:38	10011	20007	\N	Afhandelen	\N
28	11	4	\N	Afgehandeld	2013-01-24 14:06:12	2013-01-24 14:06:12	10011	20007	\N	Afhandelen	\N
31	12	4	\N	Afgehandeld	2013-01-24 14:08:56	2013-01-24 14:08:56	10011	20007	\N	Afhandelen	\N
36	13	4	\N	Afgehandeld	2013-01-24 14:11:44	2013-01-24 14:11:44	10011	20007	\N	Afhandelen	\N
39	14	4	\N	Afgehandeld	2013-01-24 14:29:24	2013-01-24 14:29:24	10011	20007	\N	Afhandelen	\N
\.


--
-- Data for Name: zaaktype_status_checklist_item; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.zaaktype_status_checklist_item (id, casetype_status_id, label, external_reference) FROM stdin;
1	10	Is alle info juist ingevuld?	\N
2	14	Is alle info juist ingevuld?	\N
3	18	Is alle info juist ingevuld?	\N
4	22	Is alle info juist ingevuld?	\N
5	26	Is alle info juist ingevuld?	\N
6	30	Is alle info juist ingevuld?	\N
7	34	Is alle info juist ingevuld?	\N
8	38	Is alle info juist ingevuld?	\N
9	42	Is alle info juist ingevuld?	\N
\.


--
-- Name: zaaktype_status_checklist_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_status_checklist_item_id_seq', 9, true);


--
-- Name: zaaktype_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zaaktype_status_id_seq', 42, true);


--
-- Name: zorginstituut_identificatie_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.zorginstituut_identificatie_seq', 1, false);


--
-- Name: natuurlijk_persoon adres_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon
    ADD CONSTRAINT adres_id UNIQUE (id);


--
-- Name: adres adres_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.adres
    ADD CONSTRAINT adres_pkey PRIMARY KEY (id);


--
-- Name: alternative_authentication_activation_link alternative_authentication_activation_link_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.alternative_authentication_activation_link
    ADD CONSTRAINT alternative_authentication_activation_link_pkey PRIMARY KEY (token);


--
-- Name: bag_ligplaats bag_ligplaats_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_ligplaats
    ADD CONSTRAINT bag_ligplaats_pkey PRIMARY KEY (id);


--
-- Name: bag_nummeraanduiding bag_nummeraanduiding_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_nummeraanduiding
    ADD CONSTRAINT bag_nummeraanduiding_pkey PRIMARY KEY (id);


--
-- Name: bag_openbareruimte bag_openbareruimte_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_openbareruimte
    ADD CONSTRAINT bag_openbareruimte_pkey PRIMARY KEY (id);


--
-- Name: bag_pand bag_pand_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_pand
    ADD CONSTRAINT bag_pand_pkey PRIMARY KEY (id);


--
-- Name: bag_standplaats bag_standplaats_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_standplaats
    ADD CONSTRAINT bag_standplaats_pkey PRIMARY KEY (id);


--
-- Name: bag_verblijfsobject_gebruiksdoel bag_verblijfsobject_gebruiksdoel_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject_gebruiksdoel
    ADD CONSTRAINT bag_verblijfsobject_gebruiksdoel_pkey PRIMARY KEY (id);


--
-- Name: bag_verblijfsobject_pand bag_verblijfsobject_pand_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject_pand
    ADD CONSTRAINT bag_verblijfsobject_pand_pkey PRIMARY KEY (id);


--
-- Name: bag_verblijfsobject bag_verblijfsobject_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_verblijfsobject
    ADD CONSTRAINT bag_verblijfsobject_pkey PRIMARY KEY (id);


--
-- Name: bag_woonplaats bag_woonplaats_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_woonplaats
    ADD CONSTRAINT bag_woonplaats_pkey PRIMARY KEY (id);


--
-- Name: bedrijf_authenticatie bedrijf_authenticatie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf_authenticatie
    ADD CONSTRAINT bedrijf_authenticatie_pkey PRIMARY KEY (id);


--
-- Name: bedrijf bedrijf_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf
    ADD CONSTRAINT bedrijf_pkey PRIMARY KEY (id);


--
-- Name: bedrijf bedrijf_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrijf
    ADD CONSTRAINT bedrijf_uuid_key UNIQUE (uuid);


--
-- Name: beheer_import_log beheer_import_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.beheer_import_log
    ADD CONSTRAINT beheer_import_log_pkey PRIMARY KEY (id);


--
-- Name: beheer_import beheer_import_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.beheer_import
    ADD CONSTRAINT beheer_import_pkey PRIMARY KEY (id);


--
-- Name: betrokkene_notes betrokkene_notes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.betrokkene_notes
    ADD CONSTRAINT betrokkene_notes_pkey PRIMARY KEY (id);


--
-- Name: betrokkenen betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.betrokkenen
    ADD CONSTRAINT betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_categorie bibliotheek_categorie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_categorie
    ADD CONSTRAINT bibliotheek_categorie_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_kenmerken bibliotheek_kenmerken_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken
    ADD CONSTRAINT bibliotheek_kenmerken_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_kenmerken_values bibliotheek_kenmerken_values_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken_values
    ADD CONSTRAINT bibliotheek_kenmerken_values_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_notificatie_kenmerk bibliotheek_notificatie_kenmerk_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificatie_kenmerk
    ADD CONSTRAINT bibliotheek_notificatie_kenmerk_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_notificaties bibliotheek_notificaties_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificaties
    ADD CONSTRAINT bibliotheek_notificaties_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_sjablonen_magic_string bibliotheek_sjablonen_magic_string_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen_magic_string
    ADD CONSTRAINT bibliotheek_sjablonen_magic_string_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_pkey PRIMARY KEY (id);


--
-- Name: case_action case_actions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_action
    ADD CONSTRAINT case_actions_pkey PRIMARY KEY (id);


--
-- Name: case_property case_property_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_property
    ADD CONSTRAINT case_property_pkey PRIMARY KEY (id);


--
-- Name: case_relation case_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_relation
    ADD CONSTRAINT case_relation_pkey PRIMARY KEY (id);


--
-- Name: checklist_item checklist_item_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist_item
    ADD CONSTRAINT checklist_item_pkey PRIMARY KEY (id);


--
-- Name: checklist checklist_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist
    ADD CONSTRAINT checklist_pkey PRIMARY KEY (id);


--
-- Name: config config_definition_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config
    ADD CONSTRAINT config_definition_id_key UNIQUE (definition_id);


--
-- Name: config config_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config
    ADD CONSTRAINT config_pkey PRIMARY KEY (id);


--
-- Name: contact_data contact_data_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contact_data
    ADD CONSTRAINT contact_data_pkey PRIMARY KEY (id);


--
-- Name: contactmoment_email contactmoment_email_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_email
    ADD CONSTRAINT contactmoment_email_pkey PRIMARY KEY (id);


--
-- Name: contactmoment_note contactmoment_note_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_note
    ADD CONSTRAINT contactmoment_note_pkey PRIMARY KEY (id);


--
-- Name: contactmoment contactmoment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment
    ADD CONSTRAINT contactmoment_pkey PRIMARY KEY (id);


--
-- Name: directory directory_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.directory
    ADD CONSTRAINT directory_pkey PRIMARY KEY (id);


--
-- Name: directory directory_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.directory
    ADD CONSTRAINT directory_uuid_key UNIQUE (uuid);


--
-- Name: file_annotation file_annotation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_annotation
    ADD CONSTRAINT file_annotation_pkey PRIMARY KEY (id);


--
-- Name: file_case_document file_case_document_file_id_case_document_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_case_document
    ADD CONSTRAINT file_case_document_file_id_case_document_id_key UNIQUE (file_id, case_document_id);


--
-- Name: file_case_document file_case_document_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_case_document
    ADD CONSTRAINT file_case_document_pkey PRIMARY KEY (id);


--
-- Name: file_metadata file_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_metadata
    ADD CONSTRAINT file_metadata_pkey PRIMARY KEY (id);


--
-- Name: file file_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_pkey PRIMARY KEY (id);


--
-- Name: file_derivative file_thumbnail_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_derivative
    ADD CONSTRAINT file_thumbnail_pkey PRIMARY KEY (id);


--
-- Name: filestore filestore_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.filestore
    ADD CONSTRAINT filestore_pkey PRIMARY KEY (id);


--
-- Name: gegevensmagazijn_subjecten gegevensmagazijn_subjecten_subject_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gegevensmagazijn_subjecten
    ADD CONSTRAINT gegevensmagazijn_subjecten_subject_uuid_key UNIQUE (subject_uuid);


--
-- Name: gm_adres gm_adres_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_adres
    ADD CONSTRAINT gm_adres_pkey PRIMARY KEY (id);


--
-- Name: gm_bedrijf gm_bedrijf_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_bedrijf
    ADD CONSTRAINT gm_bedrijf_pkey PRIMARY KEY (id);


--
-- Name: gm_natuurlijk_persoon gm_natuurlijk_persoon_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_natuurlijk_persoon
    ADD CONSTRAINT gm_natuurlijk_persoon_pkey PRIMARY KEY (id);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: interface interface_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.interface
    ADD CONSTRAINT interface_pkey PRIMARY KEY (id);


--
-- Name: logging logging_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.logging
    ADD CONSTRAINT logging_pkey PRIMARY KEY (id);


--
-- Name: message message_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: directory name_case_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.directory
    ADD CONSTRAINT name_case_id UNIQUE (name, case_id);


--
-- Name: natuurlijk_persoon natuurlijk_persoon_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_pkey PRIMARY KEY (id);


--
-- Name: natuurlijk_persoon natuurlijk_persoon_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_uuid_key UNIQUE (uuid);


--
-- Name: object_acl_entry object_acl_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_acl_entry
    ADD CONSTRAINT object_acl_entry_pkey PRIMARY KEY (uuid);


--
-- Name: object_data object_data_object_class_object_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_data
    ADD CONSTRAINT object_data_object_class_object_id_key UNIQUE (object_class, object_id);


--
-- Name: object_data object_data_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_data
    ADD CONSTRAINT object_data_pkey PRIMARY KEY (uuid);


--
-- Name: object_mutation object_mutation_object_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_mutation
    ADD CONSTRAINT object_mutation_object_uuid_key UNIQUE (object_uuid);


--
-- Name: object_mutation object_mutation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_mutation
    ADD CONSTRAINT object_mutation_pkey PRIMARY KEY (id);


--
-- Name: object_relation object_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relation
    ADD CONSTRAINT object_relation_pkey PRIMARY KEY (id);


--
-- Name: object_relationships object_relationships_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relationships
    ADD CONSTRAINT object_relationships_pkey PRIMARY KEY (uuid);


--
-- Name: object_subscription object_subscription_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_subscription
    ADD CONSTRAINT object_subscription_pkey PRIMARY KEY (id);


--
-- Name: object_bibliotheek_entry object_type_bibliotheek_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_bibliotheek_entry
    ADD CONSTRAINT object_type_bibliotheek_entry_pkey PRIMARY KEY (id);


--
-- Name: config parameter_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config
    ADD CONSTRAINT parameter_unique UNIQUE (parameter);


--
-- Name: parkeergebied parkeergebied_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.parkeergebied
    ADD CONSTRAINT parkeergebied_pkey PRIMARY KEY (id);


--
-- Name: bag_ligplaats_nevenadres pk_ligplaats_nevenadres; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bag_ligplaats_nevenadres
    ADD CONSTRAINT pk_ligplaats_nevenadres PRIMARY KEY (identificatie, begindatum, correctie, nevenadres);


--
-- Name: queue queue_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.queue
    ADD CONSTRAINT queue_pkey PRIMARY KEY (id);


--
-- Name: remote_api_keys remote_api_keys_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.remote_api_keys
    ADD CONSTRAINT remote_api_keys_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: sbus_logging sbus_logging_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_logging
    ADD CONSTRAINT sbus_logging_pkey PRIMARY KEY (id);


--
-- Name: sbus_traffic sbus_traffic_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_traffic
    ADD CONSTRAINT sbus_traffic_pkey PRIMARY KEY (id);


--
-- Name: scheduled_jobs scheduled_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_jobs
    ADD CONSTRAINT scheduled_jobs_pkey PRIMARY KEY (id);


--
-- Name: scheduled_jobs scheduled_jobs_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_jobs
    ADD CONSTRAINT scheduled_jobs_uuid_key UNIQUE (uuid);


--
-- Name: search_query_delen search_query_delen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.search_query_delen
    ADD CONSTRAINT search_query_delen_pkey PRIMARY KEY (id);


--
-- Name: search_query search_query_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.search_query
    ADD CONSTRAINT search_query_pkey PRIMARY KEY (id);


--
-- Name: session_invitation session_invitation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.session_invitation
    ADD CONSTRAINT session_invitation_pkey PRIMARY KEY (id);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: bibliotheek_kenmerken_values sort_order_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken_values
    ADD CONSTRAINT sort_order_unique UNIQUE (sort_order, bibliotheek_kenmerken_id);


--
-- Name: subject subject_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject
    ADD CONSTRAINT subject_pkey PRIMARY KEY (id);


--
-- Name: subject subject_uuid_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subject
    ADD CONSTRAINT subject_uuid_key UNIQUE (uuid);


--
-- Name: transaction transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id);


--
-- Name: transaction_record transaction_record_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record
    ADD CONSTRAINT transaction_record_pkey PRIMARY KEY (id);


--
-- Name: transaction_record_to_object transaction_record_to_object_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record_to_object
    ADD CONSTRAINT transaction_record_to_object_pkey PRIMARY KEY (id);


--
-- Name: user_app_lock user_app_lock_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_app_lock
    ADD CONSTRAINT user_app_lock_pkey PRIMARY KEY (uidnumber, type, type_id);


--
-- Name: user_entity user_entity_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT user_entity_pkey PRIMARY KEY (id);


--
-- Name: woz_objects woz_objects_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.woz_objects
    ADD CONSTRAINT woz_objects_pkey PRIMARY KEY (id);


--
-- Name: zaak_authorisation zaak_authorisation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_authorisation
    ADD CONSTRAINT zaak_authorisation_pkey PRIMARY KEY (id);


--
-- Name: zaak_bag zaak_bag_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_bag
    ADD CONSTRAINT zaak_bag_pkey PRIMARY KEY (id);


--
-- Name: zaak_betrokkenen zaak_betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_betrokkenen
    ADD CONSTRAINT zaak_betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: zaak zaak_duplicate_prevention_token_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_duplicate_prevention_token_key UNIQUE (duplicate_prevention_token);


--
-- Name: zaak_kenmerk zaak_kenmerk_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_kenmerk
    ADD CONSTRAINT zaak_kenmerk_pkey PRIMARY KEY (id);


--
-- Name: zaak_meta zaak_meta_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_meta
    ADD CONSTRAINT zaak_meta_pkey PRIMARY KEY (id);


--
-- Name: zaak_onafgerond zaak_onafgerond_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_onafgerond
    ADD CONSTRAINT zaak_onafgerond_pkey PRIMARY KEY (zaaktype_id, betrokkene);


--
-- Name: zaak zaak_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_pkey PRIMARY KEY (id);


--
-- Name: zaak_subcase zaak_subcase_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_subcase
    ADD CONSTRAINT zaak_subcase_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_authorisation zaaktype_authorisation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_authorisation
    ADD CONSTRAINT zaaktype_authorisation_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_betrokkenen zaaktype_betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_betrokkenen
    ADD CONSTRAINT zaaktype_betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_status_checklist_item zaaktype_checklist_item_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status_checklist_item
    ADD CONSTRAINT zaaktype_checklist_item_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_definitie zaaktype_definitie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_definitie
    ADD CONSTRAINT zaaktype_definitie_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_node zaaktype_node_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node
    ADD CONSTRAINT zaaktype_node_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_notificatie zaaktype_notificatie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_pkey PRIMARY KEY (id);


--
-- Name: zaaktype zaaktype_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype
    ADD CONSTRAINT zaaktype_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_regel zaaktype_regel_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_regel
    ADD CONSTRAINT zaaktype_regel_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_relatie zaaktype_relatie_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_resultaten zaaktype_resultaten_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_resultaten
    ADD CONSTRAINT zaaktype_resultaten_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_standaard_betrokkenen zaaktype_standaard_betrokkenen_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_standaard_betrokkenen
    ADD CONSTRAINT zaaktype_standaard_betrokkenen_pkey PRIMARY KEY (id);


--
-- Name: zaaktype_status zaaktype_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status
    ADD CONSTRAINT zaaktype_status_pkey PRIMARY KEY (id);


--
-- Name: beheer_import_log_idx_import_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX beheer_import_log_idx_import_id ON public.beheer_import_log USING btree (import_id);


--
-- Name: bibliotheek_categorie_pid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_categorie_pid_idx ON public.bibliotheek_categorie USING btree (pid);


--
-- Name: bibliotheek_categorie_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_categorie_uuid_idx ON public.bibliotheek_categorie USING btree (uuid);


--
-- Name: bibliotheek_category_unique_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_category_unique_name ON public.bibliotheek_categorie USING btree (naam, (COALESCE(pid, '-1'::integer)));


--
-- Name: bibliotheek_kenmerken_bibliotheek_categorie_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_kenmerken_bibliotheek_categorie_id_idx ON public.bibliotheek_kenmerken USING btree (bibliotheek_categorie_id);


--
-- Name: bibliotheek_kenmerken_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_kenmerken_deleted_idx ON public.bibliotheek_kenmerken USING btree (deleted);


--
-- Name: bibliotheek_kenmerken_magic_string_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_kenmerken_magic_string_idx ON public.bibliotheek_kenmerken USING btree (magic_string);


--
-- Name: bibliotheek_kenmerken_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_kenmerken_uuid_idx ON public.bibliotheek_kenmerken USING btree (uuid);


--
-- Name: bibliotheek_kenmerken_values_idx_bibliotheek_kenmerken_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_kenmerken_values_idx_bibliotheek_kenmerken_id ON public.bibliotheek_kenmerken_values USING btree (bibliotheek_kenmerken_id);


--
-- Name: bibliotheek_notificaties_bibliotheek_categorie_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_notificaties_bibliotheek_categorie_id_idx ON public.bibliotheek_notificaties USING btree (bibliotheek_categorie_id);


--
-- Name: bibliotheek_notificaties_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_notificaties_deleted_idx ON public.bibliotheek_notificaties USING btree (deleted);


--
-- Name: bibliotheek_notificaties_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_notificaties_uuid_idx ON public.bibliotheek_notificaties USING btree (uuid);


--
-- Name: bibliotheek_sjablonen_bibliotheek_categorie_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_sjablonen_bibliotheek_categorie_id_idx ON public.bibliotheek_sjablonen USING btree (bibliotheek_categorie_id);


--
-- Name: bibliotheek_sjablonen_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_sjablonen_deleted_idx ON public.bibliotheek_sjablonen USING btree (deleted);


--
-- Name: bibliotheek_sjablonen_interface_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_sjablonen_interface_id_idx ON public.bibliotheek_sjablonen USING btree (interface_id);


--
-- Name: bibliotheek_sjablonen_template_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bibliotheek_sjablonen_template_uuid_idx ON public.bibliotheek_sjablonen USING btree (template_external_name);


--
-- Name: bibliotheek_sjablonen_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX bibliotheek_sjablonen_uuid_idx ON public.bibliotheek_sjablonen USING btree (uuid);


--
-- Name: burgerservicenummer; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX burgerservicenummer ON public.natuurlijk_persoon USING btree (burgerservicenummer, authenticated) WHERE (deleted_on IS NULL);


--
-- Name: case_property_case_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_property_case_id_idx ON public.case_property USING btree (case_id);


--
-- Name: case_property_name_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_property_name_idx ON public.case_property USING btree (name);


--
-- Name: case_property_name_ref_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX case_property_name_ref_idx ON public.case_property USING btree (name, namespace, object_id, case_id);


--
-- Name: INDEX case_property_name_ref_idx; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON INDEX public.case_property_name_ref_idx IS 'enforce uniqueness of param<->object rows, multiple values not allowed';


--
-- Name: case_property_object_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_property_object_id_idx ON public.case_property USING btree (object_id);


--
-- Name: case_property_type_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX case_property_type_idx ON public.case_property USING btree (type);


--
-- Name: contact_data_betrokkene_type_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contact_data_betrokkene_type_idx ON public.contact_data USING btree (betrokkene_type);


--
-- Name: contact_data_gegevens_magazijn_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contact_data_gegevens_magazijn_id_idx ON public.contact_data USING btree (gegevens_magazijn_id);


--
-- Name: contactmoment_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX contactmoment_uuid_idx ON public.contactmoment USING btree (uuid);


--
-- Name: filestore_storage_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX filestore_storage_idx ON public.filestore USING gin (storage_location);


--
-- Name: gm_natuurlijk_persoon_idx_adres_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX gm_natuurlijk_persoon_idx_adres_id ON public.gm_natuurlijk_persoon USING btree (adres_id);


--
-- Name: groups_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX groups_uuid_idx ON public.groups USING btree (uuid);


--
-- Name: idx_case_action_casetype_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_case_action_casetype_status_id ON public.case_action USING btree (casetype_status_id);


--
-- Name: idx_zaaktype_kenmerken_zaak_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_zaaktype_kenmerken_zaak_status_id ON public.zaaktype_kenmerken USING btree (zaak_status_id);


--
-- Name: idx_zaaktype_notificatie_zaak_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_zaaktype_notificatie_zaak_status_id ON public.zaaktype_notificatie USING btree (zaak_status_id);


--
-- Name: idx_zaaktype_regel_zaak_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_zaaktype_regel_zaak_status_id ON public.zaaktype_regel USING btree (zaak_status_id);


--
-- Name: idx_zaaktype_sjablonen_zaak_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_zaaktype_sjablonen_zaak_status_id ON public.zaaktype_sjablonen USING btree (zaak_status_id);


--
-- Name: interface_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX interface_uuid_idx ON public.interface USING btree (uuid);


--
-- Name: logging_component_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_component_id_idx ON public.logging USING btree (component_id) WHERE (component_id IS NOT NULL);


--
-- Name: logging_component_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_component_idx ON public.logging USING btree (component) WHERE (component IS NOT NULL);


--
-- Name: logging_restricted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_restricted_idx ON public.logging USING btree (restricted);


--
-- Name: logging_zaak_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX logging_zaak_id_idx ON public.logging USING btree (zaak_id);


--
-- Name: message_subject_archived_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX message_subject_archived_idx ON public.message USING btree (subject_id, is_archived);


--
-- Name: message_subject_read_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX message_subject_read_idx ON public.message USING btree (subject_id, is_read);


--
-- Name: natuurlijk_persoon_burgerservicenummer; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX natuurlijk_persoon_burgerservicenummer ON public.natuurlijk_persoon USING btree (burgerservicenummer);


--
-- Name: natuurlijk_persoon_idx_adres_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX natuurlijk_persoon_idx_adres_id ON public.natuurlijk_persoon USING btree (adres_id);


--
-- Name: object_acl_entity_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_acl_entity_idx ON public.object_acl_entry USING btree (entity_type, entity_id);


--
-- Name: object_acl_entry_groupname_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_acl_entry_groupname_idx ON public.object_acl_entry USING btree (groupname);


--
-- Name: object_acl_entry_permission_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_acl_entry_permission_idx ON public.object_acl_entry USING btree (capability);


--
-- Name: object_acl_entry_unique_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX object_acl_entry_unique_idx ON public.object_acl_entry USING btree (object_uuid, entity_type, entity_id, capability, scope, groupname);


--
-- Name: object_acl_object_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_acl_object_uuid_idx ON public.object_acl_entry USING btree (object_uuid);


--
-- Name: object_bibliotheek_entry_bibliotheek_categorie_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_bibliotheek_entry_bibliotheek_categorie_id_idx ON public.object_bibliotheek_entry USING btree (bibliotheek_categorie_id);


--
-- Name: object_data_assignee_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_assignee_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.assignee'::text)));


--
-- Name: object_data_case_casetype_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_casetype_id_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.casetype.id'::text)));


--
-- Name: object_data_case_number_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_number_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.number'::text)));


--
-- Name: object_data_case_phase_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_phase_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.phase'::text)));


--
-- Name: object_data_case_requestor_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_case_requestor_id_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.requestor.id'::text)));


--
-- Name: object_data_casetype_description_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_casetype_description_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'casetype.description'::text)));


--
-- Name: object_data_casetype_name_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_casetype_name_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'casetype.name'::text)));


--
-- Name: object_data_completion_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_completion_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'case.date_of_completion'::text)));


--
-- Name: object_data_hstore_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_hstore_idx ON public.object_data USING gin (index_hstore);


--
-- Name: object_data_recipient_name_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_recipient_name_idx ON public.object_data USING btree (((index_hstore OPERATOR(public.->) 'recipient.full_name'::text)));


--
-- Name: object_data_regdate_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_regdate_idx ON public.object_data USING btree (public.hstore_to_timestamp(((index_hstore OPERATOR(public.->) 'case.date_of_registration'::text))::character varying));


--
-- Name: object_data_target_date_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_data_target_date_idx ON public.object_data USING btree (public.hstore_to_timestamp(((index_hstore OPERATOR(public.->) 'case.date_target'::text))::character varying));


--
-- Name: object_text_vector_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX object_text_vector_idx ON public.object_data USING gin (text_vector);


--
-- Name: queue_date_created_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_date_created_idx ON public.queue USING btree (date_created);


--
-- Name: queue_date_finished_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_date_finished_idx ON public.queue USING btree (date_finished);


--
-- Name: queue_object_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_object_id_idx ON public.queue USING btree (object_id);


--
-- Name: queue_priority_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_priority_idx ON public.queue USING btree (priority);


--
-- Name: queue_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX queue_status_idx ON public.queue USING btree (status);


--
-- Name: roles_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX roles_uuid_idx ON public.roles USING btree (uuid);


--
-- Name: scheduled_jobs_case_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX scheduled_jobs_case_id_idx ON public.scheduled_jobs USING btree (case_id);


--
-- Name: scheduled_jobs_task_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX scheduled_jobs_task_idx ON public.scheduled_jobs USING btree (task);


--
-- Name: transaction_external_transaction_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_external_transaction_id_idx ON public.transaction USING btree (external_transaction_id);


--
-- Name: transaction_record_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_record_uuid_idx ON public.transaction_record USING btree (uuid);


--
-- Name: transaction_text_vector_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_text_vector_idx ON public.transaction USING gist (text_vector);


--
-- Name: transaction_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX transaction_uuid_idx ON public.transaction USING btree (uuid);


--
-- Name: user_entity_source_interface_id_source_identifier_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX user_entity_source_interface_id_source_identifier_idx ON public.user_entity USING btree (source_interface_id, source_identifier);


--
-- Name: zaak_betrokkenen_gegevens_magazijn_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaak_betrokkenen_gegevens_magazijn_index ON public.zaak_betrokkenen USING btree (gegevens_magazijn_id);


--
-- Name: zaak_kenmerk_bibliotheek_kenmerken_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX zaak_kenmerk_bibliotheek_kenmerken_id ON public.zaak_kenmerk USING btree (zaak_id, bibliotheek_kenmerken_id);


--
-- Name: zaaktype_authorisation_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_authorisation_idx_zaaktype_node_id ON public.zaaktype_authorisation USING btree (zaaktype_node_id);


--
-- Name: zaaktype_betrokkenen_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_betrokkenen_idx_zaaktype_node_id ON public.zaaktype_betrokkenen USING btree (zaaktype_node_id);


--
-- Name: zaaktype_bibliotheek_categorie_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_bibliotheek_categorie_id_idx ON public.zaaktype USING btree (bibliotheek_categorie_id);


--
-- Name: zaaktype_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_deleted_idx ON public.zaaktype USING btree (deleted);


--
-- Name: zaaktype_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_idx_zaaktype_node_id ON public.zaaktype USING btree (zaaktype_node_id);


--
-- Name: zaaktype_node_idx_zaaktype_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_node_idx_zaaktype_id ON public.zaaktype_node USING btree (zaaktype_id);


--
-- Name: zaaktype_node_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX zaaktype_node_uuid_idx ON public.zaaktype_node USING btree (uuid);


--
-- Name: zaaktype_node_zaaktype_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_node_zaaktype_id_idx ON public.zaaktype_node USING btree (zaaktype_id);


--
-- Name: zaaktype_relatie_idx_relatie_zaaktype_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_relatie_idx_relatie_zaaktype_id ON public.zaaktype_relatie USING btree (relatie_zaaktype_id);


--
-- Name: zaaktype_relatie_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_relatie_idx_zaaktype_node_id ON public.zaaktype_relatie USING btree (zaaktype_node_id);


--
-- Name: zaaktype_relatie_idx_zaaktype_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_relatie_idx_zaaktype_status_id ON public.zaaktype_relatie USING btree (zaaktype_status_id);


--
-- Name: zaaktype_resultaten_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_resultaten_idx_zaaktype_node_id ON public.zaaktype_resultaten USING btree (zaaktype_node_id);


--
-- Name: zaaktype_resultaten_idx_zaaktype_status_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_resultaten_idx_zaaktype_status_id ON public.zaaktype_resultaten USING btree (zaaktype_status_id);


--
-- Name: zaaktype_status_idx_zaaktype_node_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_status_idx_zaaktype_node_id ON public.zaaktype_status USING btree (zaaktype_node_id);


--
-- Name: zaaktype_status_zaaktype_node_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_status_zaaktype_node_status_idx ON public.zaaktype_status USING btree (zaaktype_node_id, status);


--
-- Name: zaaktype_zaaktype_node_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zaaktype_zaaktype_node_id_idx ON public.zaaktype USING btree (zaaktype_node_id);


--
-- Name: bedrijf_authenticatie bedrijf_authenticatie_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bedrijf_authenticatie_insert_trigger BEFORE INSERT ON public.bedrijf_authenticatie FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: bedrijf_authenticatie bedrijf_authenticatie_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bedrijf_authenticatie_update_trigger BEFORE UPDATE ON public.bedrijf_authenticatie FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: beheer_import beheer_import_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER beheer_import_insert_trigger BEFORE INSERT ON public.beheer_import FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: beheer_import_log beheer_import_log_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER beheer_import_log_insert_trigger BEFORE INSERT ON public.beheer_import_log FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: beheer_import_log beheer_import_log_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER beheer_import_log_update_trigger BEFORE UPDATE ON public.beheer_import_log FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: beheer_import beheer_import_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER beheer_import_update_trigger BEFORE UPDATE ON public.beheer_import FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: betrokkene_notes betrokkene_notes_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER betrokkene_notes_insert_trigger BEFORE INSERT ON public.betrokkene_notes FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: betrokkene_notes betrokkene_notes_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER betrokkene_notes_update_trigger BEFORE UPDATE ON public.betrokkene_notes FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: bibliotheek_categorie bibliotheek_categorie_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_categorie_insert_trigger BEFORE INSERT ON public.bibliotheek_categorie FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: bibliotheek_categorie bibliotheek_categorie_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_categorie_update_trigger BEFORE UPDATE ON public.bibliotheek_categorie FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: bibliotheek_kenmerken bibliotheek_kenmerken_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_kenmerken_insert_trigger BEFORE INSERT ON public.bibliotheek_kenmerken FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: bibliotheek_kenmerken bibliotheek_kenmerken_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_kenmerken_update_trigger BEFORE UPDATE ON public.bibliotheek_kenmerken FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: bibliotheek_notificaties bibliotheek_notificaties_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_notificaties_insert_trigger BEFORE INSERT ON public.bibliotheek_notificaties FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: bibliotheek_notificaties bibliotheek_notificaties_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_notificaties_update_trigger BEFORE UPDATE ON public.bibliotheek_notificaties FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_sjablonen_insert_trigger BEFORE INSERT ON public.bibliotheek_sjablonen FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER bibliotheek_sjablonen_update_trigger BEFORE UPDATE ON public.bibliotheek_sjablonen FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: contact_data contact_data_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER contact_data_insert_trigger BEFORE INSERT ON public.contact_data FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: contact_data contact_data_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER contact_data_update_trigger BEFORE UPDATE ON public.contact_data FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: file file_insert_timestamp_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER file_insert_timestamp_trigger BEFORE INSERT ON public.file FOR EACH ROW EXECUTE PROCEDURE public.insert_file_timestamps();


--
-- Name: bibliotheek_notificaties file_update_timestamp_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER file_update_timestamp_trigger BEFORE UPDATE ON public.bibliotheek_notificaties FOR EACH ROW EXECUTE PROCEDURE public.update_file_timestamps();


--
-- Name: logging logging_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER logging_insert_trigger BEFORE INSERT ON public.logging FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: logging logging_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER logging_update_trigger BEFORE UPDATE ON public.logging FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: parkeergebied parkeergebied_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER parkeergebied_insert_trigger BEFORE INSERT ON public.parkeergebied FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: parkeergebied parkeergebied_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER parkeergebied_update_trigger BEFORE UPDATE ON public.parkeergebied FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: scheduled_jobs scheduled_jobs_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER scheduled_jobs_insert_trigger BEFORE INSERT ON public.scheduled_jobs FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: scheduled_jobs scheduled_jobs_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER scheduled_jobs_update_trigger BEFORE UPDATE ON public.scheduled_jobs FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: zaaktype_authorisation zaaktype_authorisation_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_authorisation_insert_trigger BEFORE INSERT ON public.zaaktype_authorisation FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: zaaktype_authorisation zaaktype_authorisation_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_authorisation_update_trigger BEFORE UPDATE ON public.zaaktype_authorisation FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: zaaktype_betrokkenen zaaktype_betrokkenen_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_betrokkenen_insert_trigger BEFORE INSERT ON public.zaaktype_betrokkenen FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: zaaktype_betrokkenen zaaktype_betrokkenen_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_betrokkenen_update_trigger BEFORE UPDATE ON public.zaaktype_betrokkenen FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: zaaktype zaaktype_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_insert_trigger BEFORE INSERT ON public.zaaktype FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_kenmerken_insert_trigger BEFORE INSERT ON public.zaaktype_kenmerken FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_kenmerken_update_trigger BEFORE UPDATE ON public.zaaktype_kenmerken FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: zaaktype_node zaaktype_node_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_node_insert_trigger BEFORE INSERT ON public.zaaktype_node FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: zaaktype_node zaaktype_node_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_node_update_trigger BEFORE UPDATE ON public.zaaktype_node FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: zaaktype_notificatie zaaktype_notificatie_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_notificatie_insert_trigger BEFORE INSERT ON public.zaaktype_notificatie FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: zaaktype_notificatie zaaktype_notificatie_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_notificatie_update_trigger BEFORE UPDATE ON public.zaaktype_notificatie FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: zaaktype_regel zaaktype_regel_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_regel_insert_trigger BEFORE INSERT ON public.zaaktype_regel FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: zaaktype_regel zaaktype_regel_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_regel_update_trigger BEFORE UPDATE ON public.zaaktype_regel FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: zaaktype_relatie zaaktype_relatie_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_relatie_insert_trigger BEFORE INSERT ON public.zaaktype_relatie FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: zaaktype_relatie zaaktype_relatie_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_relatie_update_trigger BEFORE UPDATE ON public.zaaktype_relatie FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: zaaktype_resultaten zaaktype_resultaten_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_resultaten_insert_trigger BEFORE INSERT ON public.zaaktype_resultaten FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: zaaktype_resultaten zaaktype_resultaten_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_resultaten_update_trigger BEFORE UPDATE ON public.zaaktype_resultaten FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_sjablonen_insert_trigger BEFORE INSERT ON public.zaaktype_sjablonen FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_sjablonen_update_trigger BEFORE UPDATE ON public.zaaktype_sjablonen FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: zaaktype_status zaaktype_status_insert_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_status_insert_trigger BEFORE INSERT ON public.zaaktype_status FOR EACH ROW EXECUTE PROCEDURE public.insert_timestamps();


--
-- Name: zaaktype_status zaaktype_status_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_status_update_trigger BEFORE UPDATE ON public.zaaktype_status FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: zaaktype zaaktype_update_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER zaaktype_update_trigger BEFORE UPDATE ON public.zaaktype FOR EACH ROW EXECUTE PROCEDURE public.update_timestamps();


--
-- Name: adres adres_natuurlijk_persoon_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.adres
    ADD CONSTRAINT adres_natuurlijk_persoon_id_fkey FOREIGN KEY (natuurlijk_persoon_id) REFERENCES public.natuurlijk_persoon(id);


--
-- Name: alternative_authentication_activation_link alternative_authentication_activation_link_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.alternative_authentication_activation_link
    ADD CONSTRAINT alternative_authentication_activation_link_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(uuid);


--
-- Name: beheer_import_log beheer_import_log_import_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.beheer_import_log
    ADD CONSTRAINT beheer_import_log_import_id_fkey FOREIGN KEY (import_id) REFERENCES public.beheer_import(id);


--
-- Name: object_bibliotheek_entry bibliotheek_categorie_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_bibliotheek_entry
    ADD CONSTRAINT bibliotheek_categorie_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES public.bibliotheek_categorie(id) ON DELETE CASCADE;


--
-- Name: bibliotheek_categorie bibliotheek_categorie_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_categorie
    ADD CONSTRAINT bibliotheek_categorie_pid_fkey FOREIGN KEY (pid) REFERENCES public.bibliotheek_categorie(id);


--
-- Name: bibliotheek_kenmerken bibliotheek_kenmerken_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken
    ADD CONSTRAINT bibliotheek_kenmerken_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES public.bibliotheek_categorie(id);


--
-- Name: bibliotheek_kenmerken bibliotheek_kenmerken_file_metadata_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken
    ADD CONSTRAINT bibliotheek_kenmerken_file_metadata_id_fkey FOREIGN KEY (file_metadata_id) REFERENCES public.file_metadata(id);


--
-- Name: bibliotheek_kenmerken_values bibliotheek_kenmerken_values_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_kenmerken_values
    ADD CONSTRAINT bibliotheek_kenmerken_values_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: bibliotheek_notificatie_kenmerk bibliotheek_notificatie_kenmerk_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificatie_kenmerk
    ADD CONSTRAINT bibliotheek_notificatie_kenmerk_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id);


--
-- Name: bibliotheek_notificatie_kenmerk bibliotheek_notificatie_kenmerk_bibliotheek_notificatie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificatie_kenmerk
    ADD CONSTRAINT bibliotheek_notificatie_kenmerk_bibliotheek_notificatie_id_fkey FOREIGN KEY (bibliotheek_notificatie_id) REFERENCES public.bibliotheek_notificaties(id);


--
-- Name: bibliotheek_notificaties bibliotheek_notificaties_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_notificaties
    ADD CONSTRAINT bibliotheek_notificaties_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES public.bibliotheek_categorie(id);


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES public.bibliotheek_categorie(id);


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: bibliotheek_sjablonen bibliotheek_sjablonen_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen
    ADD CONSTRAINT bibliotheek_sjablonen_interface_id_fkey FOREIGN KEY (interface_id) REFERENCES public.interface(id);


--
-- Name: bibliotheek_sjablonen_magic_string bibliotheek_sjablonen_magic_strin_bibliotheek_sjablonen_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bibliotheek_sjablonen_magic_string
    ADD CONSTRAINT bibliotheek_sjablonen_magic_strin_bibliotheek_sjablonen_id_fkey FOREIGN KEY (bibliotheek_sjablonen_id) REFERENCES public.bibliotheek_sjablonen(id);


--
-- Name: case_action case_actions_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_action
    ADD CONSTRAINT case_actions_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: case_action case_actions_casetype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_action
    ADD CONSTRAINT case_actions_casetype_status_id_fkey FOREIGN KEY (casetype_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: case_property case_property_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_property
    ADD CONSTRAINT case_property_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id) ON DELETE CASCADE;


--
-- Name: case_property case_property_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_property
    ADD CONSTRAINT case_property_object_id_fkey FOREIGN KEY (object_id) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: case_relation case_relation_case_id_a_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_relation
    ADD CONSTRAINT case_relation_case_id_a_fkey FOREIGN KEY (case_id_a) REFERENCES public.zaak(id);


--
-- Name: case_relation case_relation_case_id_b_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.case_relation
    ADD CONSTRAINT case_relation_case_id_b_fkey FOREIGN KEY (case_id_b) REFERENCES public.zaak(id);


--
-- Name: zaaktype_node casetype_node_logging_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node
    ADD CONSTRAINT casetype_node_logging_id_fkey FOREIGN KEY (logging_id) REFERENCES public.logging(id);


--
-- Name: checklist checklist_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist
    ADD CONSTRAINT checklist_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: checklist_item checklist_item_checklist_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.checklist_item
    ADD CONSTRAINT checklist_item_checklist_id_fkey FOREIGN KEY (checklist_id) REFERENCES public.checklist(id);


--
-- Name: contactmoment contactmoment_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment
    ADD CONSTRAINT contactmoment_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: contactmoment_email contactmoment_email_contactmoment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_email
    ADD CONSTRAINT contactmoment_email_contactmoment_id_fkey FOREIGN KEY (contactmoment_id) REFERENCES public.contactmoment(id);


--
-- Name: contactmoment_email contactmoment_email_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_email
    ADD CONSTRAINT contactmoment_email_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: contactmoment_note contactmoment_note_contactmoment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contactmoment_note
    ADD CONSTRAINT contactmoment_note_contactmoment_id_fkey FOREIGN KEY (contactmoment_id) REFERENCES public.contactmoment(id);


--
-- Name: directory directory_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.directory
    ADD CONSTRAINT directory_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: file_annotation file_annotation_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_annotation
    ADD CONSTRAINT file_annotation_file_id_fkey FOREIGN KEY (file_id) REFERENCES public.file(id);


--
-- Name: file_case_document file_case_document_case_document_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_case_document
    ADD CONSTRAINT file_case_document_case_document_id_fkey FOREIGN KEY (case_document_id) REFERENCES public.zaaktype_kenmerken(id);


--
-- Name: file_case_document file_case_document_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_case_document
    ADD CONSTRAINT file_case_document_file_id_fkey FOREIGN KEY (file_id) REFERENCES public.file(id);


--
-- Name: file file_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: file file_directory_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_directory_id_fkey FOREIGN KEY (directory_id) REFERENCES public.directory(id);


--
-- Name: file file_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: file file_is_duplicate_of_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_is_duplicate_of_fkey FOREIGN KEY (is_duplicate_of) REFERENCES public.file(id) ON DELETE SET NULL;


--
-- Name: file file_metadata_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_metadata_id_fkey FOREIGN KEY (metadata_id) REFERENCES public.file_metadata(id);


--
-- Name: file file_root_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_root_file_id_fkey FOREIGN KEY (root_file_id) REFERENCES public.file(id);


--
-- Name: file file_scheduled_jobs_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file
    ADD CONSTRAINT file_scheduled_jobs_id_fkey FOREIGN KEY (scheduled_jobs_id) REFERENCES public.scheduled_jobs(id);


--
-- Name: file_derivative file_thumbnail_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_derivative
    ADD CONSTRAINT file_thumbnail_file_id_fkey FOREIGN KEY (file_id) REFERENCES public.file(id);


--
-- Name: file_derivative file_thumbnail_filestore_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_derivative
    ADD CONSTRAINT file_thumbnail_filestore_id_fkey FOREIGN KEY (filestore_id) REFERENCES public.filestore(id);


--
-- Name: gegevensmagazijn_subjecten gegevensmagazijn_subjecten_nnp_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gegevensmagazijn_subjecten
    ADD CONSTRAINT gegevensmagazijn_subjecten_nnp_uuid_fkey FOREIGN KEY (nnp_uuid) REFERENCES public.bedrijf(uuid);


--
-- Name: gegevensmagazijn_subjecten gegevensmagazijn_subjecten_subject_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gegevensmagazijn_subjecten
    ADD CONSTRAINT gegevensmagazijn_subjecten_subject_uuid_fkey FOREIGN KEY (subject_uuid) REFERENCES public.subject(uuid);


--
-- Name: gm_adres gm_adres_natuurlijk_persoon_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_adres
    ADD CONSTRAINT gm_adres_natuurlijk_persoon_id_fkey FOREIGN KEY (natuurlijk_persoon_id) REFERENCES public.gm_natuurlijk_persoon(id);


--
-- Name: gm_natuurlijk_persoon gm_natuurlijk_persoon_adres_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gm_natuurlijk_persoon
    ADD CONSTRAINT gm_natuurlijk_persoon_adres_id_fkey FOREIGN KEY (adres_id) REFERENCES public.gm_adres(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: interface interface_case_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.interface
    ADD CONSTRAINT interface_case_type_id_fkey FOREIGN KEY (case_type_id) REFERENCES public.zaaktype(id);


--
-- Name: interface interface_objecttype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.interface
    ADD CONSTRAINT interface_objecttype_id_fkey FOREIGN KEY (objecttype_id) REFERENCES public.object_data(uuid) ON DELETE SET NULL;


--
-- Name: logging logging_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.logging
    ADD CONSTRAINT logging_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES public.object_data(uuid) ON DELETE SET NULL;


--
-- Name: message message_logging_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_logging_id_fkey FOREIGN KEY (logging_id) REFERENCES public.logging(id);


--
-- Name: natuurlijk_persoon natuurlijk_persoon_adres_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_adres_id_fk FOREIGN KEY (adres_id) REFERENCES public.adres(id);


--
-- Name: object_bibliotheek_entry object_data; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_bibliotheek_entry
    ADD CONSTRAINT object_data FOREIGN KEY (object_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_data object_data_class_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_data
    ADD CONSTRAINT object_data_class_uuid_fkey FOREIGN KEY (class_uuid) REFERENCES public.object_data(uuid);


--
-- Name: object_acl_entry object_data_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_acl_entry
    ADD CONSTRAINT object_data_fkey FOREIGN KEY (object_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_mutation object_mutation_lock_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_mutation
    ADD CONSTRAINT object_mutation_lock_object_uuid_fkey FOREIGN KEY (lock_object_uuid) REFERENCES public.object_data(uuid);


--
-- Name: object_mutation object_mutation_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_mutation
    ADD CONSTRAINT object_mutation_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_mutation object_mutation_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_mutation
    ADD CONSTRAINT object_mutation_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(id);


--
-- Name: object_relation object_relation_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relation
    ADD CONSTRAINT object_relation_object_id_fkey FOREIGN KEY (object_id) REFERENCES public.object_data(uuid) ON DELETE CASCADE DEFERRABLE;


--
-- Name: object_relation object_relation_object_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relation
    ADD CONSTRAINT object_relation_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES public.object_data(uuid);


--
-- Name: object_relationships object_relationships_object1_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relationships
    ADD CONSTRAINT object_relationships_object1_uuid_fkey FOREIGN KEY (object1_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_relationships object_relationships_object2_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_relationships
    ADD CONSTRAINT object_relationships_object2_uuid_fkey FOREIGN KEY (object2_uuid) REFERENCES public.object_data(uuid) ON DELETE CASCADE;


--
-- Name: object_subscription object_subscription_config_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_subscription
    ADD CONSTRAINT object_subscription_config_interface_id_fkey FOREIGN KEY (config_interface_id) REFERENCES public.interface(id);


--
-- Name: object_subscription object_subscription_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.object_subscription
    ADD CONSTRAINT object_subscription_interface_id_fkey FOREIGN KEY (interface_id) REFERENCES public.interface(id);


--
-- Name: queue queue_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.queue
    ADD CONSTRAINT queue_object_id_fkey FOREIGN KEY (object_id) REFERENCES public.object_data(uuid) ON DELETE CASCADE DEFERRABLE;


--
-- Name: queue queue_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.queue
    ADD CONSTRAINT queue_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.queue(id) ON DELETE SET NULL;


--
-- Name: roles roles_parent_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_parent_group_id_fkey FOREIGN KEY (parent_group_id) REFERENCES public.groups(id);


--
-- Name: sbus_logging sbus_logging_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_logging
    ADD CONSTRAINT sbus_logging_pid_fkey FOREIGN KEY (pid) REFERENCES public.sbus_logging(id);


--
-- Name: sbus_logging sbus_logging_sbus_traffic_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbus_logging
    ADD CONSTRAINT sbus_logging_sbus_traffic_id_fkey FOREIGN KEY (sbus_traffic_id) REFERENCES public.sbus_traffic(id);


--
-- Name: scheduled_jobs scheduled_jobs_case_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_jobs
    ADD CONSTRAINT scheduled_jobs_case_id_fkey FOREIGN KEY (case_id) REFERENCES public.zaak(id);


--
-- Name: search_query_delen search_query_delen_search_query_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.search_query_delen
    ADD CONSTRAINT search_query_delen_search_query_id_fkey FOREIGN KEY (search_query_id) REFERENCES public.search_query(id);


--
-- Name: session_invitation session_invitation_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.session_invitation
    ADD CONSTRAINT session_invitation_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(uuid) ON DELETE CASCADE;


--
-- Name: transaction transaction_input_file_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT transaction_input_file_fkey FOREIGN KEY (input_file) REFERENCES public.filestore(id);


--
-- Name: transaction transaction_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT transaction_interface_id_fkey FOREIGN KEY (interface_id) REFERENCES public.interface(id);


--
-- Name: transaction_record_to_object transaction_record_to_object_transaction_record_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record_to_object
    ADD CONSTRAINT transaction_record_to_object_transaction_record_id_fkey FOREIGN KEY (transaction_record_id) REFERENCES public.transaction_record(id) ON DELETE CASCADE DEFERRABLE;


--
-- Name: transaction_record transaction_record_transaction_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transaction_record
    ADD CONSTRAINT transaction_record_transaction_id_fkey FOREIGN KEY (transaction_id) REFERENCES public.transaction(id) ON DELETE CASCADE DEFERRABLE;


--
-- Name: user_entity user_entity_source_interface_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT user_entity_source_interface_id_fkey FOREIGN KEY (source_interface_id) REFERENCES public.interface(id);


--
-- Name: user_entity user_entity_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT user_entity_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.subject(id);


--
-- Name: zaak zaak_aanvrager_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_aanvrager_fkey FOREIGN KEY (aanvrager) REFERENCES public.zaak_betrokkenen(id);


--
-- Name: zaak_authorisation zaak_authorisation_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_authorisation
    ADD CONSTRAINT zaak_authorisation_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak_bag zaak_bag_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_bag
    ADD CONSTRAINT zaak_bag_pid_fkey FOREIGN KEY (pid) REFERENCES public.zaak_bag(id);


--
-- Name: zaak_bag zaak_bag_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_bag
    ADD CONSTRAINT zaak_bag_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_behandelaar_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_behandelaar_fkey FOREIGN KEY (behandelaar) REFERENCES public.zaak_betrokkenen(id);


--
-- Name: zaak_betrokkenen zaak_betrokkenen_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_betrokkenen
    ADD CONSTRAINT zaak_betrokkenen_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_coordinator_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_coordinator_fkey FOREIGN KEY (coordinator) REFERENCES public.zaak_betrokkenen(id);


--
-- Name: logging zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.logging
    ADD CONSTRAINT zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak_kenmerk zaak_kenmerk_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_kenmerk
    ADD CONSTRAINT zaak_kenmerk_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id);


--
-- Name: zaak_kenmerk zaak_kenmerk_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_kenmerk
    ADD CONSTRAINT zaak_kenmerk_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id) ON DELETE CASCADE;


--
-- Name: zaak zaak_locatie_correspondentie_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_locatie_correspondentie_fkey FOREIGN KEY (locatie_correspondentie) REFERENCES public.zaak_bag(id);


--
-- Name: zaak zaak_locatie_zaak_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_locatie_zaak_fkey FOREIGN KEY (locatie_zaak) REFERENCES public.zaak_bag(id);


--
-- Name: zaak_meta zaak_meta_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_meta
    ADD CONSTRAINT zaak_meta_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_object_data_uuid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_object_data_uuid_fkey FOREIGN KEY (uuid) REFERENCES public.object_data(uuid) ON DELETE SET NULL;


--
-- Name: zaak zaak_pid_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_pid_fk FOREIGN KEY (pid) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_pid_fkey FOREIGN KEY (pid) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_relates_to_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_relates_to_fkey FOREIGN KEY (relates_to) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_resultaat_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_resultaat_id_fkey FOREIGN KEY (resultaat_id) REFERENCES public.zaaktype_resultaten(id);


--
-- Name: zaak_subcase zaak_subcase_relation_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_subcase
    ADD CONSTRAINT zaak_subcase_relation_zaak_id_fkey FOREIGN KEY (relation_zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak_subcase zaak_subcase_zaak_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_subcase
    ADD CONSTRAINT zaak_subcase_zaak_id_fkey FOREIGN KEY (zaak_id) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_vervolg_van_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_vervolg_van_fkey FOREIGN KEY (vervolg_van) REFERENCES public.zaak(id);


--
-- Name: zaak zaak_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES public.zaaktype(id);


--
-- Name: zaak_onafgerond zaak_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak_onafgerond
    ADD CONSTRAINT zaak_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES public.zaaktype(id);


--
-- Name: zaak zaak_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaak
    ADD CONSTRAINT zaak_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_authorisation zaaktype_authorisation_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_authorisation
    ADD CONSTRAINT zaaktype_authorisation_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES public.zaaktype(id);


--
-- Name: zaaktype_authorisation zaaktype_authorisation_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_authorisation
    ADD CONSTRAINT zaaktype_authorisation_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_betrokkenen zaaktype_betrokkenen_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_betrokkenen
    ADD CONSTRAINT zaaktype_betrokkenen_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype zaaktype_bibliotheek_categorie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype
    ADD CONSTRAINT zaaktype_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES public.bibliotheek_categorie(id) DEFERRABLE;


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id);


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_object_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_object_id_fkey FOREIGN KEY (object_id) REFERENCES public.object_data(uuid) ON DELETE RESTRICT;


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_kenmerken zaaktype_kenmerken_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_kenmerken
    ADD CONSTRAINT zaaktype_kenmerken_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_node zaaktype_node_moeder_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node
    ADD CONSTRAINT zaaktype_node_moeder_zaaktype_id_fkey FOREIGN KEY (moeder_zaaktype_id) REFERENCES public.zaaktype(id) ON DELETE SET NULL;


--
-- Name: zaaktype_node zaaktype_node_zaaktype_definitie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node
    ADD CONSTRAINT zaaktype_node_zaaktype_definitie_id_fkey FOREIGN KEY (zaaktype_definitie_id) REFERENCES public.zaaktype_definitie(id) DEFERRABLE;


--
-- Name: zaaktype_node zaaktype_node_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_node
    ADD CONSTRAINT zaaktype_node_zaaktype_id_fkey FOREIGN KEY (zaaktype_id) REFERENCES public.zaaktype(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_notificatie zaaktype_notificatie_bibliotheek_notificaties_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_bibliotheek_notificaties_id_fkey FOREIGN KEY (bibliotheek_notificaties_id) REFERENCES public.bibliotheek_notificaties(id);


--
-- Name: zaaktype_notificatie zaaktype_notificatie_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_notificatie zaaktype_notificatie_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_notificatie
    ADD CONSTRAINT zaaktype_notificatie_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_regel zaaktype_regel_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_regel
    ADD CONSTRAINT zaaktype_regel_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_regel zaaktype_regel_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_regel
    ADD CONSTRAINT zaaktype_regel_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_relatie zaaktype_relatie_relatie_zaaktype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_relatie_zaaktype_id_fkey FOREIGN KEY (relatie_zaaktype_id) REFERENCES public.zaaktype(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_relatie zaaktype_relatie_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_relatie zaaktype_relatie_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_relatie
    ADD CONSTRAINT zaaktype_relatie_zaaktype_status_id_fkey FOREIGN KEY (zaaktype_status_id) REFERENCES public.zaaktype_status(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_resultaten zaaktype_resultaten_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_resultaten
    ADD CONSTRAINT zaaktype_resultaten_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_resultaten zaaktype_resultaten_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_resultaten
    ADD CONSTRAINT zaaktype_resultaten_zaaktype_status_id_fkey FOREIGN KEY (zaaktype_status_id) REFERENCES public.zaaktype_status(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_bibliotheek_kenmerken_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_bibliotheek_kenmerken_id_fkey FOREIGN KEY (bibliotheek_kenmerken_id) REFERENCES public.bibliotheek_kenmerken(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_bibliotheek_sjablonen_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_bibliotheek_sjablonen_id_fkey FOREIGN KEY (bibliotheek_sjablonen_id) REFERENCES public.bibliotheek_sjablonen(id);


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_zaak_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_zaak_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_sjablonen zaaktype_sjablonen_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_sjablonen
    ADD CONSTRAINT zaaktype_sjablonen_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_standaard_betrokkenen zaaktype_standaard_betrokkenen_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_standaard_betrokkenen
    ADD CONSTRAINT zaaktype_standaard_betrokkenen_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id);


--
-- Name: zaaktype_standaard_betrokkenen zaaktype_standaard_betrokkenen_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_standaard_betrokkenen
    ADD CONSTRAINT zaaktype_standaard_betrokkenen_zaaktype_status_id_fkey FOREIGN KEY (zaak_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_status_checklist_item zaaktype_status_checklist_item_zaaktype_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status_checklist_item
    ADD CONSTRAINT zaaktype_status_checklist_item_zaaktype_status_id_fkey FOREIGN KEY (casetype_status_id) REFERENCES public.zaaktype_status(id);


--
-- Name: zaaktype_status zaaktype_status_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype_status
    ADD CONSTRAINT zaaktype_status_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- Name: zaaktype zaaktype_zaaktype_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zaaktype
    ADD CONSTRAINT zaaktype_zaaktype_node_id_fkey FOREIGN KEY (zaaktype_node_id) REFERENCES public.zaaktype_node(id) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;


--
-- PostgreSQL database dump complete
--

