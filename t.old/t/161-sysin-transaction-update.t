#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';

use TestSetup;
initialize_test_globals_ok;
### Test header end

$zs->zs_transaction_ok(sub {
    my $i = $zs->create_transaction_ok;

    ok $i->transaction_update({
        bla => 'bla',
    }), 'Updated Transaction';


}, 'transaction_update');

$zs->zs_transaction_ok(sub {
    throws_ok sub {
        $zs->create_transaction_ok->transaction_update({
            interface_id => 'BA NA NA',
        });
    }, qr/invalid: interface_id/, 'Ran update with invalid params';
}, 'transaction_update invalid params');

$zs->zs_transaction_ok(sub {
    ok $zs->create_transaction_ok->transaction_update({}), 'Ran update without params';
}, 'transaction_update no params');

zs_done_testing;
