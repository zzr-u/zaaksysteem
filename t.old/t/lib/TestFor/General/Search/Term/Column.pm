package TestFor::General::Search::Term::Column;
use base qw(Test::Class);

use TestSetup;

use Moose::Util qw(ensure_all_roles);
use Zaaksysteem::Search::Conditional;
use Zaaksysteem::Search::Term::Column;
use Zaaksysteem::Search::Term::Literal;
use Zaaksysteem::Search::Term::ArrayRole;

sub test_column_contains : Tests {
    my $mock_rs = Test::MockObject->new();
    $mock_rs->mock(
        'map_hstore_key',
        sub {
            my $self = shift;
            return "hstore[$_[0]]";
        }
    );

    my $lterm = Zaaksysteem::Search::Term::Column->new(value => 'col');
    my $rterm = Zaaksysteem::Search::Term::Literal->new(value => 'lit');
    my $cond = Zaaksysteem::Search::Conditional->new(
        lterm    => $lterm,
        rterm    => $rterm,
        operator => 'contains',
    );

    ok(
        !$rterm->does('Zaaksysteem::Search::Term::ArrayRole'),
        "ArrayRole not yet applied to rterm",
    );

    my $res = $lterm->evaluate($mock_rs, $cond);

    ok(
        $rterm->does('Zaaksysteem::Search::Term::ArrayRole'),
        "ArrayRole applied to rterm after evaluate",
    );
    is(
        $res,
        "string_to_array(hstore[col], '\x1E')",
        'Correct SQL snippet was returned for the column in a "contains" conditional',
    );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
