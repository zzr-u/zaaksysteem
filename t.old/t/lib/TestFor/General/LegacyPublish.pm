package TestFor::General::LegacyPublish;
use base qw(ZSTest);
use TestSetup;

use Zaaksysteem::LegacyPublish;
use POSIX qw(strftime);

sub zs_publish_unsupported {
    my %args = (
        username => 'gebruiker',
        password => 'wachtwoord',
        hostname => 'test.zs.nl',
        port     => '22',
        src_dir  => '/var/tmp/zs',
        protocol => 'meuk',
        csv_filename => 'foo.csv',
    );

    my $publisher = Zaaksysteem::LegacyPublish->new(%args);
    isa_ok($publisher, "Zaaksysteem::LegacyPublish");
    throws_ok(
        sub {
            $publisher->run();
        },
        qr#LegacyPublish/unsupported: Unsupported protocol meuk#,
        "Unsupported protocol",
    );
}

sub zs_publish_ftp : Tests {
    my %args = (
        username => 'gebruiker',
        password => 'wachtwoord',
        hostname => 'test.zs.nl',
        port     => '22',
        src_dir  => '/var/tmp/zs',
        protocol => 'ftp',
        csv_filename => 'foo.csv',
    );

    my $publisher = Zaaksysteem::LegacyPublish->new(%args);
    isa_ok($publisher, "Zaaksysteem::LegacyPublish");

    my ($net_ftp_opts, $net_ftp_login, $net_ftp_put);
    no warnings qw(redefine once);
    local *Net::FTP::new = sub {
        my $self = shift;
        $net_ftp_opts = {@_};
        return bless($net_ftp_opts, "Net::FTP");
    };

    local *Net::FTP::login = sub {
        my $self = shift;
        $net_ftp_login = [@_];
    };

    local *Net::FTP::put = sub {
        my $self = shift;
        $net_ftp_put = [@_];
    };

    local *Net::FTP::quit = sub { return 1; };

    local *HTTP::Response::code = sub {
        return 200;
    };

    local *LWP::UserAgent::request = sub {
        return bless({}, "HTTP::Response");
    };
    use warnings;

    my $ok = $publisher->run();
    ok($ok, "Files via FTP going fine");

    is_deeply(
        $net_ftp_opts,
        { Host => 'test.zs.nl', Port => 22, Debug => 0 },
        'Net::FTP->new called correctly'
    );
    is_deeply(
        $net_ftp_login,
        [ qw(gebruiker wachtwoord) ],
        'Net::FTP->login called correctly'
    );
    is_deeply(
        $net_ftp_put,
        [ '/var/tmp/zs/foo.csv' ],
        'Net::FTP->put called correctly'
    );

    $args{notify_filename} = 'foo.txt';
    $publisher = Zaaksysteem::LegacyPublish->new(%args);
    $publisher->run();
    is_deeply(
        $net_ftp_put,
        [ '/var/tmp/zs/foo.txt' ],
        'Net::FTP->put called correctly for notify'
    );

    with_stopped_clock {
        my $content = Zaaksysteem::LegacyPublish::_get_header_file_content();

        my $time = strftime "%H:%M:%S", gmtime;
        my $date = strftime "%d-%m-%Y", gmtime;

        my $expect = qq{


----- Dit bestand is gegenereerd met de Zaaksysteem.nl module -----
Datum       : $date
Tijd        : $time
Gebruiker   : Mintlab
Module      : Gevonden voorwerpen
};
        is($content, $expect, 'Notify file contents is correct');
    };

    # Error conditions;
    {
        no warnings qw(redefine once);
        local *Net::FTP::new = sub {
            die "Net::FTP->new died";
        };
        use warnings;

        throws_ok(
            sub {
                $publisher->run();
            },
            qr/Net::FTP->new died/,
            "Net::FTP->new died"
        );
    }

    {
        no warnings qw(redefine once);
        local *Net::FTP::login = sub {
            die "Net::FTP->login died";
        };
        use warnings;

        throws_ok(
            sub {
                $publisher->run();
            },
            qr/Net::FTP->login died/,
            "Net::FTP->login died"
        );
    }

    {
        no warnings qw(redefine once);
        local *Net::FTP::put = sub {
            die "Net::FTP->put died";
        };
        use warnings;

        throws_ok(
            sub {
                $publisher->run();
            },
            qr/Net::FTP->put died/,
            "Net::FTP->put died"
        );

    }

}

sub zs_publish_scp : Tests {

    my %args = (
        username => 'test',
        password => 'test',
        hostname => 'test.zs.nl',
        port     => '22',
        src_dir  => '/var/tmp/zs',
        dst_dir  => '/var/tmp/meuk',
        protocol => 'scp',
    );

    my $publisher = Zaaksysteem::LegacyPublish->new(%args);
    isa_ok($publisher, "Zaaksysteem::LegacyPublish");

    my ($net_scp_opts, $net_scp_dirs);

    no warnings qw(redefine once);
    local *Net::SCP::Expect::new = sub {
        my $self = shift;
        $net_scp_opts = {@_};
        return bless($net_scp_opts, "Net::SCP::Expect");
    };

    local *Net::SCP::Expect::scp = sub {
        my $self = shift;
        $net_scp_dirs = [@_];
    };

    local *HTTP::Response::code = sub {
        return 200;
    };

    local *LWP::UserAgent::request = sub {
        return bless({}, "HTTP::Response");
    };
    use warnings;

    my $ok = $publisher->run();
    ok($ok, "File transfer successful");

    is_deeply(
        $net_scp_opts,
        {
            compress  => 1,
            auto_yes  => 1,
            recursive => 1,
            port      => 22,
            host      => 'test.zs.nl',
            user      => 'test',
            password  => 'test',
        },
        "Net::SCP::Expect->new is called correctly",
    );

    is_deeply($net_scp_dirs,
        [
            '/var/tmp/zs/.',
            '/var/tmp/meuk',
        ],
        "Net::SCP::Expect->run is called correctly",
    );

    %args = (
        %args,
        notify_url  => 'https://localhost',
        notify_wait => 1,
    );

    $publisher = Zaaksysteem::LegacyPublish->new(%args);
    $ok = $publisher->run();
    ok($ok, "File transfer successful");

    %args = (
        %args,
        notify_wait => 0,
    );

    $publisher = Zaaksysteem::LegacyPublish->new(%args);

    { # Failure is mandatory
        no warnings qw(redefine once);
        local *Net::SCP::Expect::scp = sub {
            die 'Net::SCP::Expect just threw an error';
        };
        use warnings;

        throws_ok(
            sub {
                $publisher->run();
            },
            qr#ZS/LegacyPublish/scp: Error during SCP transfer: Net::SCP::Expect just threw an error#,
            "We died while scp'ing"
        );
    }
    { # Failure is mandatory
        no warnings qw(redefine once);
        local *HTTP::Response::code = sub {
            return 503;
        };
        use warnings;

        throws_ok(
            sub {
                $publisher->run();
            },
            qr#ZS/LegacyPublish/server/busy#,
            "Server is busy",
        );

        no warnings qw(redefine once);
        local *HTTP::Response::code = sub {
            return 666;
        };
        local *HTTP::Response::message = sub {
            return 'Horrible Error';
        };

        use warnings;
        throws_ok(
            sub {
                $publisher->run();
            },
            qr#ZS/LegacyPublish/server/failure: 666: Horrible Error#,
            "Unexpected server failure",
        );
    }
}

1;

__END__

=head1 NAME

TestFor::General::LegacyPublish - A test class for legacy publications

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
