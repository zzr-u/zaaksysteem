#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use_ok('Zaaksysteem::Auth::Bedrijfid');

### Test header end

use constant SUCCESS_ENDPOINT => 'http://www.mintlab.nl/';

$zs->zs_transaction_ok(sub {

    my $catalyst_session = {};

    my $rs = $schema->resultset('BedrijfAuthenticatie');

    my $auth = new Zaaksysteem::Auth::Bedrijfid(
        authdbic => $schema->resultset('BedrijfAuthenticatie'),
        log => $schema->log,
        config => $schema->default_resultset_attributes->{config},
        session => $catalyst_session,
    );

    isnt $auth->succes, "Doesn't say it's succesful before logging in";
    isnt $auth->login, "Doesn't say have a login before logging in";

    my $redir = $auth->authenticate(
        login               => '12345678',
        password            => 'password',
        success_endpoint    => SUCCESS_ENDPOINT
    );

    isnt $redir, "Does return falsy when no user is logged in";

    my $bedrijf = $rs->create({
        gegevens_magazijn_id => 25,
        login => '12345678',
        password => 'password',
    });

    isa_ok $bedrijf, "Zaaksysteem::Model::DB::BedrijfAuthenticatie";

    throws_ok(sub {
        $auth->authenticate(login => '12345678', password => 'password');
    }, qr/Validation of profile failed/, "Fails when not given a success endpoint");

    $redir = $auth->authenticate(
        login               => '12345678',
        password            => 'password',
        success_endpoint    => SUCCESS_ENDPOINT
    );

    is $redir, SUCCESS_ENDPOINT, "Success endpoint returned when logging in succesfully";

}, 'Bedrijf authentication');


zs_done_testing();
