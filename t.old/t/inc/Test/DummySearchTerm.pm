package Test::DummySearchTerm;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Search::Term';

has source => (
    is => 'ro',
    isa => 'ArrayRef',
);

override 'evaluate' => sub {
    my $self = shift;

    return @{ $self->{source} };
};

__PACKAGE__->meta->make_immutable();



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

