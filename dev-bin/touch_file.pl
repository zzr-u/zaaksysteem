#!/usr/bin/env perl

use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Getopt::Long;
use Zaaksysteem::Config;
use Pod::Usage;
use File::Spec::Functions;

use Time::HiRes qw(gettimeofday tv_interval);

my %opt = (
    help       => 0,
    config     => '/etc/zaaksysteem/zaaksysteem.conf',
    customer_d => '/etc/zaaksysteem/customer.d',
    n          => 0,
);

{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(\%opt, qw(
            help
            n
            config=s
            customer_d=s
            hostname=s
        ));
    };
    if (!$ok) {
        pod2usage(1) ;
    }
}

pod2usage(0) if ($opt{help});

foreach (qw(config customer_d hostname)) {
    if (!defined $opt{$_}) {
        warn "Missing option: $_";
        pod2usage(1) ;
    }
}

my $ZS = Zaaksysteem::Config->new(
    zs_customer_d => $opt{customer_d},
    zs_conf       => $opt{config},
);

my $schema = $ZS->get_customer_schema($opt{hostname}, 1);

initialize_hstore($schema);

sub initialize_hstore {
    my $dbic = shift;

    my $rs = $dbic->resultset('File')->search({ date_deleted => undef }, { order_by => { -desc => 'date_modified' } });

    my $count = $rs->count();
    if (!$count) {
        print "No files found\n";
        return;
    }

    my $starttime = [gettimeofday];
    my $files_done = 0;
    while(my $file = $rs->next) {
        $files_done++;
        do_transaction($dbic, sub {

            printf(
                "Updating file @{[$file->id]} '@{[$file->name]}' (%d of %d, %.3f/second)\n",
                $files_done, $count,
                $files_done / tv_interval($starttime, [gettimeofday]),
            );

            $file->update;
        });
    }
}

sub do_transaction {
    my ($dbic, $sub) = @_;

    $dbic->txn_do(sub {
        $sub->();

        if ($opt{n}) {
            $dbic->txn_rollback;
        }
    });
}

1;

__END__

=head1 NAME

touch_file.pl - A file toucher

=head1 SYNOPSIS

    $ cd /path/to/source;
    $ ./dev-bin/touch_file.pl OPTIONS

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to /etc/zaaksysteem/customer.d

=item * hostname

The hostname you want to touch files for

=item * n

Dry run, run it, but don't

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
