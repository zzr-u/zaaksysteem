#!/usr/bin/env bash

DIR=$(readlink -m $(dirname $0)/../);
cd $DIR/client
npm start &

cd $DIR/frontend
npm start &
