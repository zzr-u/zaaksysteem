/* ***** BEGIN LICENSE BLOCK ********************************************
 *
 * Version: EUPL 1.1
 *
 * The contents of this file are subject to the EUPL, Version 1.1 or
 * - as soon they will be approved by the European Commission -
 * subsequent versions of the EUPL (the "Licence");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://joinup.ec.europa.eu/software/page/eupl
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Zaaksysteem
 *
 * The Initial Developer of the Original Code is
 * Mintlab B.V. <info@mintlab.nl>
 *
 * Portions created by the Initial Developer are Copyright (C) 2009-2011
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 * Michiel Ootjers <michiel@mintlab.nl>
 * Jonas Paarlberg <jonas@mintlab.nl>
 * Jan-Willem Buitenhuis <jw@mintlab.nl>
 * Peter Moen <peter@mintlab.nl>
 *
 * ***** END LICENSE BLOCK ******************************************** */

/* eslint no-undef:0 */
// ZS-FIXME: this file introduces a ludicrous amount of global variables.

jQuery.migrateMute = true;

 // stub console for ie
 (function(w) {
    if (!w.console) {
        var c = w.console = {};
        c.log = c.error = c.info = c.debug = c.warn = c.trace = c.dir = c.dirxml = c.group = c.groupEnd = c.time = c.timeEnd = c.assert = c.profile = c.profileEnd = function() {};
    }
 })(window);

$(window).resize(function() {
    resizePanel();
});

$(document).ready(function ( ) {

    var ie = (function(){

        var undef,
            v = 3,
            div = document.createElement('div'),
            all = div.getElementsByTagName('i');

        while (
            div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
            all[0]
        );

        return v > 4 ? v : undef;

    }());

    if(ie === undefined) {
        // fallback for IE10, which dropped support for conditional comments
        ie = (function(){
            var undef,rv = -1; // Return value assumes failure.
            if (navigator.appName === 'Microsoft Internet Explorer')
            {
                var ua = navigator.userAgent;
                var re  = /MSIE ([0-9]{1,}[\.0-9]{0,})/;
                if (re.exec(ua) !== null) {
                    rv = parseFloat( RegExp.$1 );
                }
            }

            return ((rv > -1) ? rv : undef);
        }());
    }

    if(!ie && (!(window.ActiveXObject) && "ActiveXObject" in window)) {
        ie = 11;
        $('body').addClass('ie11');
    }

    if(ie <= 8) {
        $('body').addClass('ie8 oldie');
    } else if(ie === 9) {
        $('body').addClass('ie9');
    }


});

// polyfill toISOString
(function ( ) {

    if ( !Date.prototype.toISOString ) {

        (function() {

            function pad(number) {
                var r = String(number);
                if ( r.length === 1 ) {
                    r = '0' + r;
                }
                return r;
            }

            Date.prototype.toISOString = function() {
                return this.getUTCFullYear() +
                    '-' + pad( this.getUTCMonth() + 1 ) +
                    '-' + pad( this.getUTCDate() ) +
                    'T' + pad( this.getUTCHours() ) +
                    ':' + pad( this.getUTCMinutes() ) +
                    ':' + pad( this.getUTCSeconds() ) +
                    '.' + String( (this.getUTCMilliseconds()/1000).toFixed(3) ).slice( 2, 5 ) +
                    'Z';
            };

        }() );
    }
})();

    var outerLayout;

    function createLayouts () {

        outerLayout = $('.splitter').layout({
                name:                    "outer"
            ,   resizerCursor:          "col-resize"
            ,    resizeWhileDragging:    true
            ,    spacing_open:            3 // ALL panes
            ,    spacing_closed:            12 // ALL panes
            ,    center__paneSelector:    ".blockcontent-right"
            ,    west__paneSelector:        ".blockcontent-left"
            ,    west__size:                '30%'
            ,    west__closable:            false
            ,    minSize:                50
            ,   scrollToBookmarkOnLoad: false

        });

    };

    var outerLayoutContact;

    function createLayoutsContact () {

        outerLayoutContact = $('.splitter-dashboard').layout({
                name:                    "outer"
            ,   resizerCursor:          "col-resize"
            ,    resizeWhileDragging:    true
            ,    spacing_open:            3 // ALL panes
            ,    spacing_closed:            12 // ALL panes
            ,    center__paneSelector:    ".blockcontent-right"
            ,    west__paneSelector:        ".blockcontent-left"
            ,    west__size:                '65%'
            ,    west__closable:            false
            ,    minSize:                50
            ,   scrollToBookmarkOnLoad: false

        });

    };

    var outerLayoutDossier;

    function createLayoutsDossier () {

        outerLayoutContact = $('.splitter-dossier').layout({
                name:                    "outer"
            ,   resizerCursor:          "col-resize"
            ,    resizeWhileDragging:    true
            ,    spacing_open:            3 // ALL panes
            ,    spacing_closed:            12 // ALL panes
            ,    center__paneSelector:    ".block-inner-scroll-center"
            ,    east__paneSelector:        ".block-inner-scroll-east"
            ,    east__size:                '30%'
            ,    east__closable:            false
            ,    minSize:                50
            ,   scrollToBookmarkOnLoad: false
            ,   east__onresize:         $.layout.callbacks.resizePaneAccordions
        }).resizeAll();

    };

    // taken tab sidebar


function resizePanel() {
    var blockInnerHeight = $('.taken .taken-inner .ezra_fields:visible').height();
    //if($(window).width() >= 1024) {
    //    if ($.browser.msie  && parseInt($.browser.version, 10) === 8) {
    //      var bHeight = '234';
    //    } else {
    //      var bHeight = '230';
    //    }
    // $('.block-inner-scroll-east').height(blockInnerHeight);
}

function htmlEscape(s) {
    return s.replace(/[&"<>]/g, function (c) {
        return {
            '&': "&amp;",
            '"': "&quot;",
            '<': "&lt;",
            '>': "&gt;"
        }[c];
    });
}

$(document).ready(function(){

    $("a").filter(function() {
        // !!!HACK!!! Somehown the OpenLayers library breaks the hostname
        // property of the local this objects. As soon as we call
        // this.hostname when openlayers is loaded, IE will return an "invalid
        // argument" error. Dirty and quick fix.
        if ($('.ezra_map').length) {
            return false;
        }

        // return this.hostname && (this.hostname.replace("www.", "") != location.hostname.replace("www.", ""));
    // }).click(function() {
    //     window.open($(this).attr("href"));
    //     return false;
    });


    $('.ezra_checkbox_preset_client').each( function() {
        initializeCasetypePresetClient($(this));

        $(this).change(function() {
            initializeCasetypePresetClient($(this));
        });
    });


/*

    $(document).on('submit', 'form.ezra_timeline_note', function() {
        var obj = $(this);
        var serialized = obj.serialize();
        $.getJSON(
            obj.attr('action'),
            serialized,
            function(data) {
                var result = data.json.result;
                obj.find('textarea[name="message"]').val('');
                result.class = 'note';
                var event = render_timeline_item(result);
                var timeline_item_content = event.find('.timeline-item-inner');
                event.addClass('right');
                timeline_item_content.css('backgroundColor', '#f9fa9f');

                obj.closest('.ezra_timeline_event').after(event);
                timeline_item_content.animate({
                    backgroundColor: "#fff",
                }, 1000 );
            }
        );

        return false;
    });

*/


//    $(document).on('change', 'form.ezra_timeline_filter input[type=checkbox]', function() {
//        load_betrokkene_timeline();
//    });

//    $('.ezra_subject_timeline').click( function() {
//        load_betrokkene_timeline();
//    });


    $('.ezra_checkbox_preset_client').each( function() {
        initializeCasetypePresetClient($(this));

        $(this).change(function() {
            initializeCasetypePresetClient($(this));
        });
    });

    if($('.ezra_auto_start_subcase').length) {
        $('.ezra_auto_start_subcase').on('click', function() {
            var obj = $(this);

            if(obj.attr('checked')) {
                obj.parents('tr').find('.ezra_edit_subcase').show();
            } else {
                obj.parents('tr').find('.ezra_edit_subcase').hide();
            }
        });
    }


    if($('.ezra_ztb_make_td_link').length) {
        $('.ezra_ztb_make_td_link').on('click', function() {
            var obj = $(this);
            var inner_link = obj.find('a.ezra_zaaktype_goto');
            return ezra_zaaktype_goto(inner_link);
        });
    }

    if ($('.splitter').length) {
        createLayouts();
    }

    if ($('.splitter-dashboard').length) {
        createLayoutsContact();
    }











    $(document).on('click', '#next_status_subzaak .edit', function() {
        var obj     = $(this);
        var thisrow = obj.closest('tr');

        var questionname = thisrow.find('input[name^="status_zaaktype_id"]');
        obj.attr('rel',
            (
             obj.attr('rel') ?
             obj.attr('rel') + '; ' : ''
             ) + 'destination: ' + questionname.attr('name')
        );

        ezra_dialog({
            url         : obj.attr('href'),
            title       : obj.attr('title'),
            cgi_params  : getOptions(obj.attr('rel'))
        }, function() {
            load_subzaken_kenmerken();
        })
        return false;
    });


    $(document).on('click', '#next_status_subzaak .delete_row', function() {
        var parentrow = $(this).closest('tr');
        parentrow.remove();
        return false;
    });


    // Make 'add' button work
/*
    $(document).on('click', '#next_status_subzaak .status_definition_add_subzaak', function() {
        var obj = $(this);

        var zt_row_id = clone_row2('#next_status_subzaak .subzaken', 'next_status_subzaak_subzaken');

        // Launch our zaaktype selection window
        select_zaaktype('#' + zt_row_id + ' input[name^="status_zaaktype_id"],#' +
            zt_row_id + ' input[name^="status_zaaktype_open"]', '#' + zt_row_id + ' .description', '#' + zt_row_id);

        // Make sure this one gets checked
        $('#' + zt_row_id + ' input[name^="status_zaaktype_run"]').attr('checked', 'checked');

        // Make sure this option does not get an id appended
        $('#' + zt_row_id + ' input[name^="status_zaaktype_open"]').attr('name', 'status_zaaktype_open');

        return false;
    });
*/
    /* XXX Shouldn't be used anymore on /zaak/create/balie, see above refreshForm.
     * this function is here for backwards compatibility
     */
    $(document).on('click', '.ezra_zaaktype_keuze .ezra_kies_zaaktype', function() {
        var extra_options   = new Array();

        var formcontainer   = $(this).closest('form');

        var trigger         = formcontainer.find('input[name="ztc_trigger"]').val();
        var betrokkene_type = formcontainer.find('input[name="betrokkene_type"]:checked').val();

        extra_options['zt_trigger']         = trigger;
        extra_options['zt_betrokkene_type'] = betrokkene_type;


        /* Replace the above variables when we run this search through our new
         * zaak_aanmaken dialog
         */
        if (formcontainer.find('.zaak_aanmaken').length) {
            var zaak_aanmaken_classes = $('.zaak_aanmaken').attr('class').split(/\s+/);

            for (var i = 0; i < zaak_aanmaken_classes.length; i++) {
                if (zaak_aanmaken_classes[i].match(/refreshform-trigger/)) {
                    trigger = zaak_aanmaken_classes[i].match(/refreshform-trigger-(\w+)/);
                    trigger = trigger[1];
                    extra_options['zt_trigger']         = trigger;
                }

                if (zaak_aanmaken_classes[i].match(/refreshform-betrokkene_type/)) {
                    betrokkene_type = zaak_aanmaken_classes[i].match(/refreshform-betrokkene_type-(\w+)/);
                    betrokkene_type = betrokkene_type[1];
                    extra_options['zt_betrokkene_type'] = betrokkene_type;
                }
            }

            select_zaaktype('.zaak_aanmaken input[name="zaaktype_id"]', '.zaak_aanmaken .ezra_zaaktype_keuze_textbox', null, extra_options);
        } else {
            select_zaaktype('#start_' + trigger + ' .ezra_zaaktype_keuze input[name="zaaktype_id"]', '#start_' + trigger + ' .ezra_zaaktype_keuze_textbox', null, extra_options);
        }

        return false;
    });

    $(document).on('click', '.search_betrokkene2', function() {
        updateBetrokkeneSearch($(this));
    });


    $(document).on('click', '.wijzig_bestand', function() {
        var fileupload = $(this).closest(".fileUpload");
        $(this).hide();
 //       fileupload.find('.existing_file').hide();
//        fileupload.find('.new_upload').show();
        fileupload.find('.new_upload').css('visibility', 'visible');
//        fileupload.find('.new_upload input[type="file"]').click();
        return false;
    });

    // allow for error checking on a download.
//    $(document).on('click', '.ezra_download_as_pdf', function(e) {
//        var href = $(this).attr('href');

    // allow for error checking on a download.
    $(document).on('click', '.ezra_verify_md5_checksum', function(e) {
        var href = $(this).attr('href');
        $.get(href, function(data){
            var result = data.json.result;
            var message = result ? 'Document is in orde' : 'Document is niet in orde';
            ezra_dialog({
                url: '/page/alert',
                title: 'Integriteit document',
                cgi_params: {
                    message: message
                },
                layout:'small'
            });
        }).error(function() {
        });
        return false;
    });


    $(document).on('click', '.close_alert_dialog', function() {
        $('#dialog').dialog('close');
        return false;
    });

    $(document).on('click', '.ezra_publish_zaaktype', function() {

        var obj = $(this);
        var commit_message = obj.closest('form').find('textarea[name=commit_message]').val();
        var checkboxValues = [];
        obj.closest('form').find('input[name=components]:checked').each(function() {
            checkboxValues.push($(this).val());
        });
        var components = checkboxValues.join(',');

         if(!components) {
            $('.ezra_finish_zaaktype_commit_message_verplicht').show();
            $.ztWaitStop();
            return false;
        }
        return true;
    });

    $(document).on('submit', '.ezra_generic_confirmation_dialog', function() {
        var obj = $(this);
        if(obj.find('input[name=commit_message]').length) {
            var commit_message = obj.find('input[name=commit_message]').val();
            if(!commit_message.length) {
                obj.find('.ezra_commit_message_required').show();
                return false;
            }
        }
    });

    $(document).on('click', '#zaaktype_kenmerk_add', function() {
        clone_kenmerk();
        return false;
    });

    $(document).on('change', '#kenmerk_invoertype', function() {
        if ($(this).find(':selected').hasClass('has_options')) {
            $('#kenmerk_invoertype_options').show();
        } else {
            $('#kenmerk_invoertype_options').hide();
        }
    });

    $(document).on('change', '#start_intern input[name="betrokkene_type"]', function() {
        reload_interne_aanvrager();
    });


    $(document).on('click', "input[name='betrokkene_type']", function() {
        reload_aanvrager_wgts();
    });

    /* kenmerken groepen toggle */

    $(document).on('click', '.ezra_table_row_heading', function() {
      $('.groep1').toggle();
      $(this).toggleClass('close')
    });

    $(document).on('click', '.doSubmit', function() {
        container = $(this).closest('form');

        container.trigger('submit');
        return false;
    });

    var ua = navigator.userAgent,
    ipadClick = (ua.match(/iPad/i)) ? "touchstart" : "click";


    $(document).on(ipadClick, '.menu_heading', function() {
        var menuContentDiv = $(this).next();
        if (menuContentDiv.is(":hidden")) {
            menuContentDiv.slideDown("fast");
            //$(this).children(".menuImgClose").removeClass("menuIconOpen");
            $.cookie(menuContentDiv.attr('id'), 'expanded');
        }
        else {
            menuContentDiv.slideUp("fast");
            //$(this).children(".menuImgClose").addClass("menuIconOpen");
            $.cookie(menuContentDiv.attr('id'), 'collapsed');
        }
    });

    $(document).on('change', '.chk-swapper', function() {
        match = $(this).attr('id').match(/\d+/g);
        $('.depper').attr('style', 'display: none;');
        $('.dependend-' + match[0]).attr('style', false);
    });


     // C::Zaak::Documents
    $(document).on('click', '.add-dialog', function(){
        $('.option-documenttype').each(function() {
            if ($(this).attr('checked')) {
                documenttype = $(this).attr('value');
            }
        });
        documentdepth = $('.option-documentdepth').attr('value');

        $('#dialog .dialog-content').load('/zaak/'
            + $.zaaknr
            + '/documents/' + documentdepth + '/add/' + documenttype,
            null,
            function() {
                openDialog('Een bestand toevoegen aan het zaakdossier', 690, 500);

                ezra_document_functions();
            }
        );

        return false;
    });

    $(document).on('change', 'input[name="afdeling-eigenaar"]', function() {
        parent_elem = $(this).parent('div');
        if ($(this).attr('checked') && $(this).attr('value') == 'afdeling') {
            $('#' + parent_elem.attr('id') + ' input[name="zaakeigenaar"]').css('display', 'none');
            $('#' + parent_elem.attr('id') + ' select[name="ztc_org_eenheid_id"]').css('display', 'inline');
            $('#' + parent_elem.attr('id') + ' a').css('display', 'none');
        } else {
            $('#' + parent_elem.attr('id') + ' select[name="ztc_org_eenheid_id"]').css('display', 'none');
            $('#' + parent_elem.attr('id') + ' input[name="zaakeigenaar"]').css('display', 'inline');
            $('#' + parent_elem.attr('id') + ' a').css('display', 'inline');
        }

    });



/*
    yuck! :) obsolete

    $(document).on('click', '.dialog-help-button', function() {
        var obj = $(this);

        elements = $(this).parent('div .dialog-help');
        elements.find('.dialog-help-text').each(function() {
            $('#dialog .dialog-content').html($(this).html());
        });
        openDialog($(this).attr('title'), 790, 350);

        return false;
    });
*/

    // C::Zaak::Checklist
    // yes/no selections functioning as radio buttons
    $(document).on('change', "input[class^='yesno_']", function(){
        question        = $(this).attr('class');
        questionname    = $(this).attr('name');

        /* Now retrieve every other options, and turn them off when we
         * turned this option on */
        others = $("input[class='" + question + "']");

        /* Do nothing, we did not turn this selection on */
        if (!$(this).attr('checked')) { return; }

        others.each(function() {
            /* Make sure we do not uncheck ourself */
            if ($(this).attr('name') == questionname) { return; }

            /* Uncheck other options within this question/vraag */
            if ($(this).attr('checked')) {
                $(this).attr('checked', false);
            }
        });
    });


    $(document).on('click', "#spec_kenmerk .update_dialog", function() {
        if (!$(this).attr('id')) {
            return false;
        }

        kenmerk = $(this).attr('id');
        $('#dialog .dialog-content').load(
            '/zaak/' + $.zaaknr + '/update',
            {
                kenmerk: $(this).attr('id')
            },
            function() {
                title = 'Specifieke zaakinformatie bijwerken (' + kenmerk + ')';
                openDialog(title);
            }
        );
     });


    $(document).on('click', ".ztc_search_popup", function() {
    alert("load ztc searchpopup");
        var context     = $(this).closest('div').attr('id');
        if ($(this).attr('name') == 'ztc_aanvrager') {
            jstype      = $("#" + context + " input[name='betrokkene_type']:checked").attr('value');
            if (!jstype) {
                return;
            }
        } else {
            jstype      = 'medewerker';
        }

        $('#dialog .dialog-content').load(
            '/betrokkene/search',
            {
                jsfill: $(this).attr('name'),
                jscontext: context,
                jstype: jstype
            },
            function() {
                title = 'Zoek betrokkene (' + jstype + ')';
                openDialog(title);
            }
        );
    });




    $(document).on('click', '.ezra_actie_container a', function() {
        var obj = $(this);

        if(obj.hasClass('popup')) {
            var title = obj.attr('title');
            var url = obj.attr('href');
            var options = getOptions(obj.attr('rel'));
            var layout = options.layout ? options.layout : null;
            ezra_dialog({title: title, url:url, layout:layout});
            obj.closest('.dropdown').hide();
            return false;
        }

        obj.closest('.dropdown').hide();
        return true;
    });


    //    Actie container
    $(document).on('click', ".select_actie", function() {
        var obj = $(this);
        var container = $(this).closest('div.select_actie_container');
        var elem = $('select option:selected',container);

        if (!elem.val()) {
            return false;
        }

        // URL in dialog openen?
        if (elem.hasClass('popup')) {

            title   = 'Actie';

            if (elem.text()) {
                title = elem.text();
            }
            title = elem.attr('title') || title;
            var options = getOptions(obj.attr('rel'));
            url  = elem.val();
            ezra_dialog({title: title, url: url, layout: options['ezra_dialog_layout']});
            return false;
        }

        if(elem.hasClass('save')) {
            var changed = $('form.webform').data('changed');
            if(changed) {
                $('form.webform').append('<input type="hidden" name="redirect" value="' + elem.val() + '"/>');
                $('form.webform').submit();
                return false;
            }
        }

        if(elem.hasClass('ezra_nieuwe_zaak_tooltip-popup')) {
            $('#ezra_nieuwe_zaak_tooltip').trigger({
                type: 'nieuweZaakTooltip',
                show: 1,
                popup: 1,
                action: elem.val()
            });
            return false;
        }

        // Anders gewoon naar de url
        window.location = elem.val();

        return false;
    });



    /* New style betrokkene search */
    $(document).on('click', '.search_betrokkene', function() {
        searchBetrokkene($(this));

        return false;
    });


    /* ztb auth multiselectbox */

    function hide_all_auth_checkboxes () {
        $('div.auth-checkboxes-wrap .show').each( function() {
            var obj = $(this);
            obj.next().hide();
            obj.removeClass('ui-state-active').removeClass('ui-corner-all');
        });
    };

    $(document).on('click', '.auth-checkboxes-wrap .show', function() {

        hide_all_auth_checkboxes();

        $(this).next().show();
        $(this).toggleClass('ui-corner-all');
        $(this).toggleClass('ui-state-active');
        return false;
    });

    $(document).on('click', '.auth-hide-checkboxes', function() {
        $(this).closest('div').hide();
        hide_all_auth_checkboxes();

        return false;
    });

    $('.auth-checkboxes-wrap .show').hover(
        function () {
          $(this).addClass('ui-state-hover');
        },
        function () {
          $(this).removeClass('ui-state-hover');
        }
      );


    /* sortable visual feedback */

    $(document).on('mouseenter', '.ezra_table tr, .ezra_tree_table tr.initialized',
      function () {
        $('.drag',this).addClass("hover");
      }
    );

    $(document).on('mouseleave', '.ezra_table tr, .ezra_tree_table tr.initialized',
      function () {
        $('.drag',this).removeClass("hover");
      }
    );




   // DROPDOWNS


   // click dropdowns
   $(document).on('click', '.ezra_dropdown_init', function(event) {

        var obj                 = $(this);

        var ezra_dropdown       = obj.closest('.dropdown-wrap').find('.ezra_dropdown');
        var ezra_dropdown_init  = obj.closest('.ezra_dropdown_init');

        var is_disabled = ezra_dropdown_init.hasClass('disabled');
        if(is_disabled) {
            return false;
        }

        var is_active = ezra_dropdown_init.hasClass('is_active');
        // just hide every dropdown on the page
        $('.ezra_dropdown').each( function() {$(this).hide() });
        $('.ezra_dropdown_init').each( function() { $(this).removeClass('is_active'); });

        if(is_active) {

        } else {
            var dropdown_position = 'left top';
            if(obj.hasClass('ezra_dropdown_right')) {
                dropdown_position = 'right top';
            }

            ezra_dropdown.css('visibility','visible').show();
            ezra_dropdown.position({
                    of: obj,
                    my: dropdown_position,
                    at: "left bottom"
                });
            ezra_dropdown_init.addClass('is_active');
        }


        event.stopPropagation();

        return false;
    });


    // document click closes menu
    $(document).on('mousedown', function(event) {
        var target = $(event.target);

        var ezra_dropdown = target.closest('.ezra_dropdown');
        var ezra_dropdown_init = target.closest('.ezra_dropdown_init');

        // if the click didn't happen in a dropdown init
        if(!ezra_dropdown_init.length) {
            $('.ezra_dropdown_init').removeClass('is_active');

            // if the click didn't happen in a dropdown
            if(!ezra_dropdown.length) {
                $('.ezra_dropdown').hide();
            }
        }
    });


    $(document).on('click', '.ezra_dropdown_init_groepeer', function() {
        var obj = $(this);
        $('.ezra_dropdown_groepeer').css('visibility','visible').show();
        $('.ezra_dropdown_groepeer').position({
                    of: obj,
                    my: "left top",
                    at: "left bottom"
                });

        $(this).addClass('active');
    });

    // Nieuwe zaak popup

    $(document).on(ipadClick, '.ezra_nieuwezaak_extra', function() {
        var myleft = $('.nieuwezaak_wrap').offset().left;
        var mytop = $(this).offset().top + $(this).outerHeight();
        $('.ezra_nieuwe_zaak_extra_dropdown').show();
        $('.ezra_nieuwe_zaak_extra_dropdown').offset({ left:myleft, top: mytop});
        $(this).addClass('active');
    });







    $(document).on('mousedown', function(event) {
        var clicked = $(event.target);

        $('.ezra_nieuwezaak_extra').removeClass('active');

        var ua = navigator.userAgent;
        if (!ua.match(/iPad/i)){
            if (!clicked.parents().hasClass("ezra_nieuwe_zaak_extra_dropdown")) {
            $('.ezra_nieuwe_zaak_extra_dropdown').hide();
        }
        }



        $('.ezra_dropdown_init_groepeer').removeClass('active');
        if (!clicked.parents().hasClass("ezra_dropdown_groepeer")) {
            $('.ezra_dropdown_groepeer').hide();
        }

        $('.doc_preview').css('visibility', 'hidden');
        $('.doc-preview-init').removeClass('active');
        $('.doc_intake_row').removeClass('active');
        if (clicked.parents().hasClass(".doc-preview-init")) {
            clicked.parents().each(function() {
                $(this).find('.doc-preview-init').addClass('active')
                 .closest('.doc_intake_row').addClass('active');
            });
        }

        if ($('.auth-checkboxes').length && !clicked.closest('.auth-checkboxes-wrap').length) {
            $('.auth-hide-checkboxes').click();
        }

        if (!clicked.closest('.ezra_actie_button_handling-active').length) {
            $('.ezra_actie_button_handling').trigger({
                type: 'actieButton',
                hide: 1
            });
        }
    });


    // show & hide dropdown

    $(document).on('mouseover', '.ezra_show_dropdown', function() {
        //$(this).find('.ezra_dropdown_init').css('visibility','visible');
    });

     $(document).on('mouseout', '.ezra_show_dropdown', function() {
        //var dropdowninit = $(this).find('.dropdown-init-hide');
        //dropdowninit.css('visibility','hidden');
        $(this).find('.dropdown').hide();
        //dropdowninit.removeClass('active');
    });


    /* EINDE DROPDOWN */

    $(document).on('click', '.ezra_betrokkene_session-deactivate', function() {
        var current_this = this;

        $.getJSON(
            $(this).attr('href'),
            {
                disable_betrokkene_session: 1
            },
            function(data) {
                var result = data.json;

                if (result.succes) {
                    $(current_this).closest('.ezra_header_persistent_user').hide();
                }
            }
        );

        return false;
    });

    $(document).on('click', '.ezra_zaaktype_goto', function() {
        return ezra_zaaktype_goto($(this));
/*
        var form = $('form.ezra_zaaktypebeheer');
        var destination = $(this).attr('href');

        // finish is more of a view page, just let the link do it's job as Berners-Lee decreed it.
        if(form.hasClass('ezra_zaaktype_finish')) {
            return true;
        }

        form.find('input[name="destination"]').val(destination);
        zvalidate(form);
        return false;*/
    });


    /*Hoogte van de stappen container bepalen

        var stapheight = 0;
        $('.stappen-nav .stap').each(function(){
            if(stapheight < $(this).height())
                stapheight = $(this).height();
        });

        $(".stappen-nav .stap").css("height",(stapheight)+"px");

    einde */


    /* placeholder ie compatibility script */

    if ($.browser.msie) {         //this is for only ie condition ( microsoft internet explore )
        $('input[type="text"][placeholder], textarea[placeholder]').each(function () {
            var obj = $(this);

            if (obj.attr('placeholder') != '') {
                obj.addClass('IePlaceHolder');

                if ($.trim(obj.val()) == '' && obj.attr('type') != 'password') {
                    obj.val(obj.attr('placeholder'));
                }
            }
        });

        $('.IePlaceHolder').focus(function () {
            var obj = $(this);
            if (obj.val() == obj.attr('placeholder')) {
                obj.val('');
            }
        });

        $('.IePlaceHolder').blur(function () {
            var obj = $(this);
            if ($.trim(obj.val()) == '') {
                obj.val(obj.attr('placeholder'));
            }
        });
    }

    /* end placeholder script */

    $(".actions-nav-filter").click(function () {
        $(this).parent().toggleClass("actions-nav-show-input");
    });

    $('.ztb-finish-fasen-overzicht h3').click(function() {
        $(this).next().slideToggle('fast');
        $(this).toggleClass('expanded');
        return false;
    }).next().hide();


    $(document).on('click', '.ezra_clickable_tr td:not(.field_actions)', function(e) {
        var obj = $(this);
        var href = obj.parent().find("a").attr("href");
        if(href) {
            window.location = href;
        }
    });

    $('#fase-tl td:last-child').addClass('last');

    $('.show_deelzaken').click(function() {
        $('.deelzaken').slideToggle('fast', function() {
        // Animation complete.
        });
    });




    /* contactmomenten toggle javascript */

    $('.contactmomenten tr').click(function () {
      $(this).find('div.contactmoment-text').toggleClass("show-text");
      $(this).find('a.toggle').toggleClass("active");
    });

    $('.contactmomenten tr,.header-persistent-user-wrap').hover(
      function () {
        $(this).addClass('hover');
      },
      function () {
        $(this).removeClass('hover');
      }
    );




    $('.add-tooltip-left[title]').qtip({
       position: {
          my: 'bottom right',
          at: 'top left',
          viewport: $(window),
          adjust: {
                 x: 0,
                 y: -3
              }
        },
        show: {
            event: 'mouseenter',
            solo: true
         },
         hide: {
              event: 'mouseleave'
         }
    });


    $("input.ui-autocomplete-input,input.zaak_search_filter").on({
        focus: function(){
            $(this).parent().removeClass("blur").addClass("focus");
        },
        blur: function(){
            $(this).parent().removeClass("focus").addClass("blur");
            //$(this).parent().find('.icon-cancel-search').hide(); this messes up the functionality of the close thingy
        }
    });



    /* Veldoptie runnings */
    ezra_basic_zaaktype_functions();
    ezra_basic_beheer_functions();

    $("a > input[type=button].link").click(function() {
        window.location = $(this).closest("a").attr("href");
    });

    $("#digidknop").click(function() {
        window.location = $("a",this).attr("href");
    });

    $('.go_back').click(function() {
        var action = $(this).parents('form').attr('action');
        window.location = action + '?goback=1';
    });

    // hide user menu when clicked anywhere else.
    $(document).on('click', function(event) {

        $('.header-user').removeClass('dropped');
        $('.user-menu').hide();
    });

    $(document).on('click', '.ezra_dropdown_init_hover:not(.ezra_dropdown_init_hover_allow_click)', function ( element) {
        return false;
    });

    // show & hide dropdown init hover block
    $(document).on('mouseover focus', '.ezra_dropdown_init_hover', function () {
            var obj = $(this);
            var mydropdown = $(this).closest('.dropdown-wrap').find('.dropdown');

            mydropdown.removeClass('visuallyhidden');

            if(obj.hasClass('dropdown-init-block')) {
                mydropdown.position({
                    of: obj,
                    my: "right top",
                    at: "left top"
                });
            }
            else if(obj.hasClass('dropdown-init-bottom')) {
                mydropdown.position({
                    of: obj,
                    my: "right bottom",
                    at: "right top",
                    offset: "0 -15"
                });
            }
            else {
                mydropdown.position({
                    of: obj,
                    my: "right top",
                    at: "left top",
                    offset: "-10 -10"
                });

                var dropdown_content = mydropdown.find('.dropdown-content');
                var dropdown_pijl = mydropdown.find('.dropdown-pijl');
                var isFlipped = mydropdown.position().left > 0;
                // if flipped to the right, we want to put the arrow to the other side
                dropdown_content.toggleClass('dropdown-flipped', isFlipped);
                dropdown_pijl.toggleClass('dropdown-pijl-flipped', isFlipped).position({
                    of: obj,
                    my: "left top",
                    at: isFlipped ? "left top" : "right top",
                    offset: isFlipped ? "18 0" : "-28 0"
                });
            }
            $(this).addClass('is_active');
        }
    );

    $(document).on('mouseout blur', '.ezra_dropdown_init_hover', function () {
            var timeoutId = setTimeout(hideMenu, 150),
                target = $(this);

            $(this).bind('mouseover focus', onDropdownOver);

            function onDropdownOver ( ) {
                clearTimeout(timeoutId);
            }

            function hideMenu ( ) {
                var dropdown = target.hasClass('dropdown-wrap') ? target : target.find('.dropdown-wrap');
                if(!dropdown.length) {
                    dropdown = target.closest('.dropdown-wrap');
                }

                target.unbind('mouseover', onDropdownOver);
                target.unbind('focus', onDropdownOver);

                dropdown.find('.dropdown').addClass('visuallyhidden');
            }

        }
    );


}); // end dom ready


/*** ezra_beheer_functions
 *
 * functions which will be reloaded on beheer specific windows
 */

function ezra_spiffy_spinner(formelement) {
    formelement.submit(function() {
        ezra_spiffy_spinner_submit($(this));
    });

}

function ezra_spiffy_spinner_submit(formelement) {
    var action = formelement.attr('action');

    $.getJSON(
        action,
        {
            spiffy_spinner: 1
        },
        function(data) {
            var spinnerinfo = data.json.spinner;

            $('.milestoneSpinner .spinner_header').html(spinnerinfo.title);
            $('.milestoneSpinner .spinner_content').html('');

            var totaltime = 0;
            var totalheight = 96;
            $.each(spinnerinfo.checks, function(key, val) {
                var newcheck = $(
                    '<span class="element" id="' + val.naam + '">'
                    + val.label + '<span class="ajax checking spinner_anim_'
                    + val.naam + '"></span></span>'
                );

                $('.milestoneSpinner .spinner_content').append(newcheck);

                totaltime = (totaltime + val.timer);
                totalheight = (totalheight + 32);
                setTimeout(
                    function() {
                        $('.milestoneSpinner .spinner_content .spinner_anim_' + val.naam)
                           .removeClass('checking').addClass('valid');
                    },
                    totaltime
                );
            });

            $('.milestoneSpinner div.spinner_wrap').css('height', totalheight + 'px');

            $('.milestoneSpinner').show();

            formelement.unbind().submit();
        }
    );

    return false;
}

function ezra_basic_beheer_functions() {
    ezra_basic_beheer_kenmerk_naam();

    /* Multiple options */
    $('#kenmerk_definitie').each(function() {
        $('#kenmerk_invoertype').change(function() {
            if (is_ezra_class($(this), 'has_options')) {
                $('.multiple-options').show();
            } else {
                $('.multiple-options').hide();
            }

            if (is_ezra_class($(this), 'appointment')) {
                $('.appointment-settings').show();
            } else {
                $('.appointment-settings').hide();
            }

            if (is_ezra_class($(this), 'allow_default_value')) {
                $('.default-value').show();
            } else {
                $('.default-value').hide();
            }

            if (is_ezra_class($(this), 'file')) {
                $('.ezra_is_for_document').show();
                $('.ezra_is_not_for_document').hide();
            } else {
                $('.ezra_is_for_document').hide();
                $('.ezra_is_not_for_document').show();
            }

            if (is_ezra_class($(this), 'date')) {
                $('.ezra_is_for_date').show();
            } else {
                $('.ezra_is_for_date').hide();
            }

            if ($(this).find(':selected').hasClass('allow_multiple_instances')) {
                $('#kenmerk_definitie .edit_kenmerk_multiple').show();
            } else {
                $('#kenmerk_definitie .edit_kenmerk_multiple').hide();
                $('#kenmerk_definitie input[name=kenmerk_type_multiple]').removeAttr('checked');
            }

        });

        $('#kenmerk_invoertype').change();
    });

    /* Notificatie options */
    $('.ezra_notificatie_rcpt').change(function() {
        $('.ezra_notificatie_rcpt_behandelaar').hide();
        $('.ezra_notificatie_rcpt_betrokkene').hide();
        $('.ezra_notificatie_rcpt_overig').hide();

        if ($(this).val() == 'behandelaar') {
            $('.ezra_notificatie_rcpt_behandelaar').show();
        }
        if ($(this).val() == 'betrokkene') {
            $('.ezra_notificatie_rcpt_betrokkene').show();
        }
        if ($(this).val() == 'overig') {
            $('.ezra_notificatie_rcpt_overig').show();
        }
    });
    $('.ezra_notificatie_rcpt').change();
}

function is_ezra_class(foo, class_type) {
    if (($(foo).attr('type') == 'hidden' && $(foo).hasClass(class_type)) || $(foo).find(':selected').hasClass(class_type)) {
        return true;
    }
    return false;
}

function ezra_basic_beheer_kenmerk_naam() {
    $('.ezra_kenmerk_naam').blur(function() {
        if ( $('.ezra_kenmerk_magic_string').length &&
            !$('.ezra_kenmerk_magic_string').val()
        ) {
            $.ajax({
                url: '/beheer/bibliotheek/kenmerken/get_magic_string',
                data: {
                    naam: $('.ezra_kenmerk_naam').val()
                },
                success: function(data) {
                    $('.ezra_kenmerk_magic_string').val(data);
                }
            });
        }
    });

    $('.ezra_sjabloon_suggest').change(function() {
        var currentform   = $('.ezra_sjabloon_suggest').parents('form');
        var currentaction = currentform.attr('action').replace(/add/,'suggest');

        if (!$('.ezra_sjabloon_suggest').val()) { return false; }

        $.ajax({
            url: currentaction,
            data: {
                naam: $('.ezra_sjabloon_suggest').val()
            },
            success: function(data) {
                $('.ezra_sjabloon_naam').val(data.json.suggestie);
                if (data.json.toelichting) {
                    $('.ezra_sjabloon_toelichting').val(data.json.toelichting);
                }
            }
        });
    });
    $('.ezra_sjabloon_suggest').change();
}

/* JQuery extensions */

/*** CLASS: ztAjaxUpdate
 *
 * ztAjaxUpdate, use:
 * $('.tableclass').ztAjaxUpdate();
 *
 * class: ztAjaxUpdate_update on update button
 * class: ztAjaxUpdate_dest on to updated html
 *
 * place div: ztAjaxUpdate_loader after title or something,
 *            which will show a spinner
 */

jQuery.fn.ztAjaxUpdate = function() {
    return this.each(function() {
        jQuery.ztAjaxUpdate.load($(this));
    });
}

jQuery.ztWaitStart = function() {
    $('.custom_overlay').show();
    $('.custom_overlay_loader').show();
}

jQuery.ztWaitStop = function() {
    $('.custom_overlay').hide();
    $('.custom_overlay_loader').hide();
}

jQuery.ztAjaxUpdate = {
    load: function(elem) {
        // Make sure destination is a block
        elem.find('.ztAjaxUpdate_dest').css('display', null);
        elem.find('.ztAjaxUpdate_loader').hide();

        // Update button working
        if (elem.find('.ztAjaxUpdate_update').attr('action')) {
            elem.find('.ztAjaxUpdate_update').submit(function() {

                var container   = $(this).closest('.ztAjaxUpdate');
                var destination = container.find('.ztAjaxUpdate_dest');

                if (container.hasClass('ztAjaxUpdate_loading')) {
                    return false;
                }

                var serialized = $(this).serialize();

                /* Loading */
                container.addClass('ztAjaxUpdate_loading');
                //container.find('.ztAjaxUpdate_loader').show();
                $.ztWaitStart();

                destination.load(
                    $(this).attr('action'),
                    serialized,
                    function() {
                        //container.find('.ztAjaxUpdate_loader').hide();
                        $.ztWaitStop();
                        container.removeClass('ztAjaxUpdate_loading');
                        $(this).closest('.ztAjaxUpdate')
                            .find('.ztAjaxUpdate_update button, .ztAjaxUpdate_update input[type="submit"]')
                            .attr('disabled', null);

                        container.find('form.ztAjaxUpdate_update')[0].reset();

                        ezra_basic_functions();
                    }
                );

                return false;
            });
        } else {

            console.log(elem.find('.ztAjaxUpdate_update'));

            elem.find('.ztAjaxUpdate_update').click(function() {
                var options     = getOptions($(this).attr('rel'));
                var container   = $(this).closest('.ztAjaxUpdate');
                var destination = container.find('.ztAjaxUpdate_dest');

                console.log(options);

                if (container.hasClass('ztAjaxUpdate_loading')) {
                    return false;
                }

                var serialized = '';
                if (options.form) {
                    var serialized = container.find('form').serialize();
                }

                /* Loading */
                container.addClass('ztAjaxUpdate_loading');
                //container.find('.ztAjaxUpdate_loader').show();
                //$('#globalSpinner').show();
                //$('#maincontainer').animate({ opacity: 0.25}, 600)
                $.ztWaitStart();


                destination.load(
                    $(this).attr('href'),
                    serialized,
                    function() {
                        //container.find('.ztAjaxUpdate_loader').hide();
                        $.ztWaitStop();
                        container.removeClass('ztAjaxUpdate_loading');
                        $(this).closest('.ztAjaxUpdate')
                            .find('.ztAjaxUpdate_update button')
                            .attr('disabled', null);

                        ezra_basic_functions();
                    }
                );

                return false;
            });
        }

    }
}



/*** CLASS: ztAjaxTable
 * ztAjaxTable, use:
 * $('.tableclass').ztAjaxTable();
 *
 * class: ztAjaxTable_template on template <tr>
 * class: ztAjaxTable_row on 'real' rows <tr>
 * class: ztAjaxTable_del on delete button
 * class: ztAjaxTable_add on add button
 *
 * optional:
 * class: ztAjaxTable_ignore for rows to ignore in table
 */

jQuery.fn.ztAjaxTable = function() {
    return this.each(function() {
        jQuery.ztAjaxTable.load($(this));
        jQuery.ztAjaxTable.init($(this));
    });
};

jQuery.ztAjaxTable = {
    load: function(elem) {
        // { Load ignored rows in table
        elem.ztAjaxTable.config = {
            ignored_rows: [],
            num_rows: 0
        };

        elem.find('.ztAjaxTable_ignore').each(function() {
            if (!elem) { return; }

            elem.ztAjaxTable.config.ignored_rows.push($(this));
        });
        // }
    },
    init: function(elem) {

        /* Hide ignored rows */
        jQuery.each(
            elem.ztAjaxTable.config.ignored_rows,
            function(index, ignored_row) {
                ignored_row.hide();
            }
        );

        // {{{ TEMPLATE ROW

        // Hide
        elem.find('.ztAjaxTable_template').hide();

        elem.closest('div').find('.ztAjaxTable_del').click(function() {
            $(this).closest('tr.ztAjaxTable_row').remove();
            updateSearchFilters();
            return false;
        });

        // }}}

        /* Count visible rows */
        elem.ztAjaxTable.config.num_rows = elem.find('tr.ztAjaxTable_row').length;

        /* Search for row creation button within prev. handler */
        elem.closest('div').find('.ztAjaxTable_add').unbind().click(function() {
            var row = jQuery.ztAjaxTable.row_add(elem);
            /* Start callback after adding row */
            var options     = getOptions($(this).attr('rel'));

            /* Create callback */
            if (options.rowcallback) {
                /* Find unique hidden */
                var unique_hidden_value = null;
                if (row.find('.ztAjaxTable_uniquehidden').length) {
                    var unique_hidden_value = row.find('.ztAjaxTable_uniquehidden').attr('name');
                }

                var callbackfunction = options.rowcallback;
                var rownumber        = jQuery.ztAjaxTable.get_row_number(row);
                window[callbackfunction]($(this),row,unique_hidden_value,rownumber);
            }

            return false;
        });

        // Find callbackfunction
        elem.closest('div').find('.ztAjaxTable_add').each(function() {
            var options     = getOptions($(this).attr('rel'));
            if (options.initcallback) {
                var callbackfunction = options.initcallback;
                window[callbackfunction]($(this));
            }

        });

    },
    get_row_number: function(rowelem) {

        var trclass     = rowelem.attr('class');
        if (!trclass.match(/ztAjaxTable_rownumber/)) {
            return 0;
        }
        var trnumber    = trclass.replace(/.*ztAjaxTable_rownumber_(\d+).*/g, '$1');

        return trnumber;
    },
    get_new_rownumber: function(rowelem) {

        var highest = 0;
        var table   = rowelem.closest('table');

        table.find('.ztAjaxTable_row').each(function() {
            var rownumber = jQuery.ztAjaxTable.get_row_number($(this));

            if (parseInt(rownumber) > parseInt(highest)) {
                highest = parseInt(rownumber);
            }
        });

        if (highest < 1) {
            returnval = 1;
        } else {
            returnval = parseInt(highest) + parseInt(1);
        }

        return returnval;
    },
    row_add: function(elem) {
        var nextrow = (elem.ztAjaxTable.config.num_rows + 1);
        var clone   = elem.find('.ztAjaxTable_template').clone(true);
        var parent  = elem.find('.ztAjaxTable_template').parent();

        /* Change row basics */
        if (clone.attr('id')) {
            clone.attr('id', clone.attr('id') + '_' + nextrow);
        }

        clone.removeClass('ztAjaxTable_template').addClass('ztAjaxTable_row');


        /* show row */
        clone.show();
        parent.append(clone);

        var nextrownumber = jQuery.ztAjaxTable.get_new_rownumber(clone);
        clone.addClass('ztAjaxTable_rownumber_' + nextrownumber);

        /* Set config */
        elem.ztAjaxTable.config.num_rows = nextrow;

        return clone;
    }
};

/* NOTIFICATIES */

function ezra_zaaktype_add_notificatie(add_elem,add_row,uniquehidden, rownumber) {
    var fire_elem = add_elem.clone();

    ezra_zaaktype_notificatie_edit(add_elem);

    add_row.find('input').each(function() {
        var inputname = $(this).attr('name');
        inputname = inputname + '_' + rownumber;
        $(this).attr('name', inputname);
    });
}

function ezra_zaaktype_notificatie_edit(add_elem) {
    var container = add_elem.closest('div');

    container.find('.edit').unbind().click(function() {
        var obj = $(this);
        var container   = obj.closest('tr');
        var uniqueidr   = container.find('.ztAjaxTable_uniquehidden').attr('name');
        obj.attr('rel', 'callback: ezra_zaaktype_add_notificatie_definitie_dialog; uniqueidr: ' + uniqueidr);

        ezra_dialog({
            url: obj.attr('href'),
            title: obj.attr('title'),
            cgi_params : {
                uniqueidr: uniqueidr
            }
        }, function() {
            ezra_zaaktype_add_notificatie_definitie_dialog();
        });

        return false;
    });
}

function ezra_zaaktype_add_notificatie_definitie_dialog() {
    $('#notificatie_definitie').find('form').submit(function() {
        var postaction = '/zaaktype/status/notificatie_definitie';
        if ($(this).attr('action')) {
            postaction = $(this).attr('action');
        }

        // Do Ajax call
        var serialized = $(this).serialize();
        var container  = $(this).closest('#notificatie_definitie');

        $.post(
            postaction,
            serialized,
            function(data) {
                $('#dialog').dialog('close');
            }
        );

        return false;
    });
}

/* SJABLONEN
 *
 *
 *
 */


function ezra_zaaktype_sjabloon_edit(add_elem) {
    var container = add_elem.closest('div');

    container.find('.edit').unbind().click(function() {
        var container   = $(this).closest('tr');
        var uniqueidr   = container.find('.ztAjaxTable_uniquehidden').attr('name');
        $(this).attr('rel', 'callback: ezra_zaaktype_add_sjabloon_definitie_dialog; uniqueidr: ' + uniqueidr);

        fireDialog($(this));

        return false;
    });
}

function ezra_zaaktype_add_sjabloon_definitie_dialog() {
    $('#sjabloon_definitie').find('form').submit(function() {
        // Do Ajax call
        var serialized = $(this).serialize();
        var container  = $(this).closest('#sjabloon_definitie');

        $.post(
            '/zaaktype/status/sjablonen_definitie',
            serialized,
            function(data) {
                $('#dialog').dialog('close');
            }
        );

        return false;
    });
}

function ezra_zaaktype_add_sjabloon(add_elem,add_row,uniquehidden, rownumber) {
    var fire_elem = add_elem.clone();

    ezra_zaaktype_sjabloon_edit(add_elem);

    uniquehidden = uniquehidden + '_' + rownumber;
    add_row.find('.ztAjaxTable_uniquehidden').attr('name', uniquehidden);

    fire_elem.attr(
        'rel',
        'hidden_name: ' + uniquehidden + '; callback: ezra_zaaktype_add_sjabloon_create'
    );

    ezra_dialog({
        url     : fire_elem.attr('href'),
        title   : fire_elem.attr('title')
    }, function() {
        ezra_zaaktype_select_sjabloon_search();
    });

//    fireDialog(fire_elem);
    $('#dialog').bind( 'dialogclose', function (event,ui) {
            add_row.remove();
        }
    );
}



function ezra_zaaktype_add_sjabloon_create(jaja) {
    // Search accordion
    $('.ztAccordion').each(function() {
        $(this).accordion({
            heightStyle: 'content'
        });
    });

    $('#sjabloon_definitie').find('form').unbind().submit(function() {

        var serialized = $(this).serialize();
        var container  = $(this).closest('#sjabloon_definitie');

        $.getJSON(
            '/beheer/bibliotheek/sjablonen/skip/0/bewerken?' + serialized + '&json_response=1',
            function(data) {
                var sjabloon = data.json;

                var rownaam      = container.find('input[name="naam"]').val();

                var uniqueid = sjabloon.id;

                // { NEW ZTB STYLE
                container.find('input[name="row_id"]').each(function() {
                    // Shoot info to this row
                    var row_id      = $(this).val();

                    var ezra_table  = $('#' + $(this).val()).parents('div.ezra_table');

                    ezra_table.ezra_table('update_row', row_id, {
                        '.rownaam'  : rownaam,
                        '.rowid'    : uniqueid
                    });
                });
                // } ZTB
                $('#dialog').unbind( 'dialogclose');
                $('#dialog').dialog('close');
            }
        );

        return false;
    });
}


function ezra_zaaktype_select_sjabloon(add_elem,add_row,uniquehidden, rownumber) {
    var fire_elem = add_elem.clone();

    ezra_zaaktype_sjabloon_edit(add_elem);

    uniquehidden = uniquehidden + '_' + rownumber;
    add_row.find('.ztAjaxTable_uniquehidden').attr('name', uniquehidden);

    fire_elem.attr(
        'rel',
        'hidden_name: ' + uniquehidden + '; callback: ezra_zaaktype_select_sjabloon_search'
    );
    fireDialog(fire_elem);
    $('#dialog').bind( 'dialogclose', function (event,ui) {
            add_row.remove();
        }
    );
}


function ezra_zaaktype_select_objecttype_search(jaja) {

    $('#search_bibliotheek_objecttype').find('form').submit(function() {
        // Do Ajax call
        var serialized = $(this).serialize();
        var container  = $(this).closest('.ztAccordion');

        $.getJSON(
            '/beheer/bibliotheek/objecttype/search?' + serialized,
            function(data) {
                var objecttypes = data.json;

                if (!data.json.length) {
                    container.find('#resultaten').html('Geen resultaten gevonden');
                }

                // Create table
                var newtable = $('<table class="objecttype_resultaten"></table>');
                var newtable_header = $(
                    '<tr>'
                    + '<th class="td250">Naam</th>'
                    + '<th class="td250">Categorie</th>'
                    + '</tr>');

                newtable.append(newtable_header);

                // Create rows
                for (var i in objecttypes) {
                    var objecttype=objecttypes[i];

                    var newrow = $(
                        '<tr class="select-row" id="objecttype-id-' + objecttype.uuid + '">'
                        + '<td class="td250 objecttype_naam">' + objecttype.naam + '</td>'
                        + '<td class="td150">' + objecttype.categorie + '</td>'
                        + '</tr>'
                    );

                    newtable.append(newrow);
                }

                // Add some logic to the table, when clicked
                newtable.find('tr').click(function() {
                    var uniquehidden = container.find('input[name="uniquehidden"]').val();
                    var rownaam      = $(this).find('.objecttype_naam').html();

                    var uniqueid = $(this).attr('id');
                    uniqueid = uniqueid.replace('objecttype-id-', '');

                    // { NEW ZTB STYLE
                    container.find('input[name="row_id"]').each(function() {
                        // Shoot info to this row
                        var row_id      = $(this).val();

                        var ezra_table  = $('#' + $(this).val()).parents('div.ezra_table');

                        ezra_table.ezra_table('update_row', row_id, {
                            '.rownaam': rownaam,
                            '.rowid'  : uniqueid,
                            '.rowtype': 'objecttype'
                        });
                    });
                    // } ZTB

                    $('input[name="' + uniquehidden + '"]').val(uniqueid);

                    $('input[name="' + uniquehidden + '"]')
                        .closest('td')
                        .find('.description')
                        .html(rownaam);

                    $('#dialog').unbind( 'dialogclose');
                    $('#dialog').dialog('close');

                });

                container.find('#resultaten').html(newtable);
                container.accordion('option', 'active', 1);
            }
        );

        return false;
    });
}

function ezra_zaaktype_select_sjabloon_search(jaja) {

    // Search accordion
    $('.ztAccordion').each(function() {
        $(this).accordion({
            heightStyle: 'content'
        });
    });

    $('#search_bibliotheek_sjablonen').find('form').submit(function() {
        // Do Ajax call
        var serialized = $(this).serialize();
        var container  = $(this).closest('.ztAccordion');

        $.getJSON(
            '/beheer/bibliotheek/sjablonen/search?' + serialized,
            function(data) {
                var sjablonen = data.json;

                if (!data.json.length) {
                    container.find('#resultaten').html('Geen resultaten gevonden');
                }

                // Create table
                var newtable = $('<table class="sjabloon_resultaten"></table>');
                var newtable_header = $(
                    '<tr>'
                    + '<th class="td250">Naam</th>'
                    + '<th class="td250">Categorie</th>'
                    + '</tr>');

                newtable.append(newtable_header);

                // Create rows
                for (var i in sjablonen) {
                    var sjabloon=sjablonen[i];

                    var newrow = $(
                        '<tr class="select-row" id="sjabloon-id-' + sjabloon.id + '">'
                        + '<td class="td250 sjabloon_naam">' + sjabloon.naam + '</td>'
                        + '<td class="td150">' + sjabloon.categorie + '</td>'
                        + '</tr>'
                    );

                    newtable.append(newrow);
                }

                // Add some logic to the table, when clicked
                newtable.find('tr').click(function() {
                    var uniquehidden = container.find('input[name="uniquehidden"]').val();
                    var rownaam      = $(this).find('.sjabloon_naam').html();

                    var uniqueid = $(this).attr('id');
                    uniqueid = uniqueid.replace('sjabloon-id-', '');

                    // { NEW ZTB STYLE
                    container.find('input[name="row_id"]').each(function() {
                        // Shoot info to this row
                        var row_id      = $(this).val();

                        var ezra_table  = $('#' + $(this).val()).parents('div.ezra_table');

                        ezra_table.ezra_table('update_row', row_id, {
                            '.rownaam'      : rownaam,
                            '.rowid'    : uniqueid
                        });
                    });
                    // } ZTB

                    $('input[name="' + uniquehidden + '"]').val(uniqueid);

                    $('input[name="' + uniquehidden + '"]')
                        .closest('td')
                        .find('.description')
                        .html(rownaam);

                    $('#dialog').unbind( 'dialogclose');
                    $('#dialog').dialog('close');

                });

                container.find('#resultaten').html(newtable);
                container.accordion('option', 'active', 1);
            }
        );

        return false;
    });
}



// wordt aangeroepen vanuit de zoekfilters
function ezra_zaaktype_kenmerk_edit(add_elem) {
    var container = add_elem.closest('div');

    container.find('.edit').unbind().click(function() {
        var container   = $(this).closest('tr');
        var uniqueidr   = container.find('.ztAjaxTable_uniquehidden').attr('name');
        var uniqueidrval = container.find('.ztAjaxTable_uniquehidden').attr('value');

        $(this).attr('rel', 'callback: ezra_zaaktype_add_kenmerk_definitie_dialog; uniqueidr: ' + uniqueidr + '; uniqueidrval: ' + uniqueidrval);

//        ezra_dialog({
//            url:
//        },
//            'ezra_zaakype_add_kenmerk_definitie_dialog'
//        );
//        fireDialog($(this));

        return false;
    });
}



//
// when a new zaaktype kenmerk regel is created, automatically open the dialog
//
function ezra_zaaktype_regel_edit(add_elem) {
    var container = add_elem.closest('div');
    container.find('.edit').last().click();
}


//-------------------------------------------------------------------------------



function ezra_zaaktype_add_kenmerk_definitie_dialog() {
    $('#kenmerk_definitie').find('form').submit(function() {
        /* Do Ajax call */
        var serialized = $(this).serialize();
        var container  = $(this).closest('#kenmerk_definitie');

        $.post(
            '/zaaktype/status/kenmerken_definitie',
            serialized,
            function(data) {
                $('#dialog').dialog('close');
            }
        );

        return false;
    });
}


// TODO use callback structure instead of passing the id of the destination
function ezra_zaaktype_add_kenmerk_create(jaja) {
    // Search accordion
    $('.ztAccordion').each(function() {
        $(this).accordion({
            heightStyle: 'content'
        });
    });

    $('#kenmerk_definitie').find('form').unbind().submit(function() {

        var serialized = $(this).serialize();
        var container  = $(this).closest('#kenmerk_definitie');

        $.getJSON(
            '/beheer/bibliotheek/kenmerken/skip/0/bewerken?' + serialized + '&json_response=1',
            function(data) {
                var kenmerk     = data.json;

                var rownaam     = container.find('input[name="kenmerk_naam"]').val();

                var uniqueid    = kenmerk.id;

                container.find('input[name="row_id"]').each(function() {
                    var row_id      = $(this).val();

                    var ezra_table  = $('#' + $(this).val()).parents('div.ezra_table');
                    ezra_table.ezra_table('update_row', row_id, {
                        '.rownaam'  : rownaam,
                        '.rowid'    : uniqueid
                    });
                });

                $('#dialog').unbind( 'dialogclose');
                $('#dialog').dialog('close');
            }
        );

        return false;
    });
}

function ezra_zaaktype_select_kenmerk() {

    var clickhandler = function(clicked_obj) {
        var item_id     = clicked_obj.attr('id');
        var container   = clicked_obj.closest('.dialog-content').find('#search_bibliotheek_kenmerken form');
        var rownaam     = clicked_obj.find('.kenmerk_naam').html();

        var row_id      = container.find('input[name="row_id"]').val();

        var ezra_table  = $('#' + row_id).parents('div.ezra_table');

        ezra_table.ezra_table('update_row', row_id, {
            '.rownaam'      : rownaam,
            '.rowid'    : item_id
        });

    };

    ezra_select_kenmerk(clickhandler);
}

//
// initialize the search dialog for kenmerken. the server sends json search results,
// these are formatted here. when a row is clicked the callback function is called.
//
function ezra_select_kenmerk(callback) {
    $('.ztAccordion').each(function() {
        $(this).accordion({
            heightStyle: 'content'
        });
    });

    $('#search_bibliotheek_kenmerken').find('form').submit(function() {
        // Do Ajax call
        var serialized = $(this).serialize();
        var container  = $(this).closest('.ztAccordion');

        container.find('#resultaten').load(
            '/beheer/bibliotheek/kenmerken/search?' + serialized,
            function() {
                $('#resultaten tr').click(function() {
                    var obj = $(this);
                    if(callback) {
                        eval(callback)(obj);
                    }


                    $('#dialog').dialog({beforeclose: null});
                    $('#dialog').unbind( 'dialogclose');
                    $(this).closest('.ui-dialog-content').dialog('close');
                });
                container.accordion('option', 'active', 1);
            }
        );

        return false;
    });


    $('.ezra_bibliotheek_nieuw').click(function() {
        var obj = $(this);

        var row_id = obj.closest('.dialog-content').find('form input[name="row_id"]').val();

        $('#dialog').unbind( 'dialogclose');
        $('#dialog').dialog('close');

        var bibliotheek_categorie_id = '1';
        var query = 'form.ezra_zaaktypebeheer input[name="bibliotheek_categorie_id"]';
        if($(query).length) {
            bibliotheek_categorie_id = $(query).val();
        }
        var url = '/beheer/bibliotheek/kenmerken/'+bibliotheek_categorie_id+'/0/bewerken';
        ezra_dialog({
            url: url,
            title: 'Nieuw kenmerk',
            layout: 'xlarge',
            cgi_params: {
                row_id: row_id
            }
        },
        function() {
            ezra_zaaktype_add_kenmerk_create();
            $.ztWaitStop();
            ezra_basic_functions();
        });

        return false;
    });

}

/* JQuery functions */
function ezra_basic_zaaktype_functions() {
    $('.ezra_direct_finish').click(function() {
        var current_form = $(this).closest('form');
        current_form.find('input[name="destination"]').val('finish');
        if($(this).hasClass('novalidation')) {
            return true;
        }
        return zvalidate(current_form);
    });

    $('.ztAjaxTable').ztAjaxTable();

    /* Terms */
    $('.widget_term_select').change(function() {
        ezra_basic_widget_term($(this), 1);
    }).each(function() {
        ezra_basic_widget_term($(this));
    });

    /*
        sortable tabellen
    */
    $("table.sortable tbody").sortable({update:
            function(event,ui) {
                $("tr",this).each(
                    function( index, element ){
                        $("input",this).val(index+1);
                    }
                );

                $("tr",this).css({border: '1px solid #000'});
                $("tr",this).removeClass('lastrow');
                $("tr:last-child",this).addClass('lastrow');

                if($(this).parent('table').hasClass('search_query_table')) {
                    var params = $("table.search_query_table.sortable").closest('form').serialize();
                    $.getJSON('/search/dashboard?action=update_sort_order&' + params, function(data) {
                        if(data && data.json && data.json.result) {
//                            log('result: ' + data.json.result);
                        } else {
//                            log('error communicating with JSON backend');
                        }
                    });
                }
            }
        });
        $("table.sortable tbody ").disableSelection();



}

/*
var zaaktype_data = {};
function _load_zaaktypen(trigger,betrokkene_type,activate) {

    if (zaaktype_data[trigger] && zaaktype_data[trigger][betrokkene_type]) {
        if (activate) {
            _activate_zaaktype_autocomplete(activate);
        }
        return zaaktype_data[trigger][betrokkene_type];
    }

    $.getJSON(
        '/zaaktype/search',
        {
            jsbetrokkene_type: betrokkene_type,
            jstrigger: trigger,
            search: 1,
            json_response: 1
        },
        function(data) {
            var zaaktypen = data.json.zaaktypen;

            if (!zaaktype_data[trigger]) {
                zaaktype_data[trigger] = {};
            }

            zaaktype_data[trigger][betrokkene_type] = {
                'raw'   : [],
                'hash'  : {}
            };

            for (var i in zaaktypen) {
                var zaaktype=zaaktypen[i];

                zaaktype_data[trigger][betrokkene_type].raw.push(zaaktype.naam);
                zaaktype_data[trigger][betrokkene_type].hash[zaaktype.naam] = zaaktype.id;
            }

            if (activate) {
                _activate_zaaktype_autocomplete(activate);
            }
        }
    );
}


function _activate_zaaktype_autocomplete(activate) {
    $('.ezra_zaaktype_keuze_textbox').autocomplete('destroy');
    $('.ezra_zaaktype_keuze_textbox').autocomplete({
        autoFocus: true,
        minLength: 0,
        delay: 100,
        source: zaaktype_data[activate.trigger][activate.betrokkene_type].raw,
        select: function(event, ui) {
            var zaaktype_id = zaaktype_data[activate.trigger][activate.betrokkene_type].hash[ui.item.value];
            activate.formcontainer.find('.ezra_zaaktype_keuze input[name="zaaktype_id"]').val(zaaktype_id);
            activate.formcontainer.find('.ezra_zaaktype_keuze input.zaaktype_id_finder').val(zaaktype_id);
        }
    }); //.focus(function() {
        //$(this).autocomplete("search");
    //});

}
*/


// this function is called twice yield ajax congestion. if the second calling of this function
// is removed, this variable can be eliminated
var changed_aanvraag_type_yet = 0;
var previous_initiative;
function ezra_basic_zaak_intake() {
    $('form .zaak_aanmaken').unbind('refreshForm').bind('refreshForm', function() {
        var obj = $(this);

        /* Retrieve trigger and betrokkene_type */
        var ztc_trigger     =
            $('.zaak_aanmaken .ezra_trigger input[type="radio"]:checked').val();
        var betrokkene_type = 'medewerker';
        var ontvanger_type  = 'natuurlijk_persoon';

        current_class   = $('form .zaak_aanmaken').attr('class');

        // TRAC 1854: Clear aanvrager when switching initiative
        if(previous_initiative && previous_initiative != ztc_trigger) {
            $('.zaak_aanmaken #ezra_id_aanvrager input[name="betrokkene_naam"]').val('');
            $('.zaak_aanmaken #ezra_id_aanvrager input[name="ztc_aanvrager_id"]').val('');
        }
        previous_initiative = ztc_trigger;

        if (ztc_trigger == 'extern') {
            $('.zaak_aanmaken #ezra_id_ontvanger_type').hide();
            $('.zaak_aanmaken .ezra_id_bestemming,'
                + '.zaak_aanmaken #ezra_id_ontvanger').hide();
            $('.zaak_aanmaken #ezra_id_aanvrager_type').show();

            betrokkene_type = $('.zaak_aanmaken #ezra_id_aanvrager_type '
               + ' .ezra_betrokkene_type input[type="radio"]:checked').val();

            // Check if betrokkene_type matches the id of the betrokkene
            var betrokkene_identifier = $('#ezra_id_aanvrager input[name="ztc_aanvrager_id"]').val();

            if (betrokkene_identifier) {
                betrokkene_identifier = betrokkene_identifier.match(/betrokkene-(.*?)-/);

                if (betrokkene_identifier && betrokkene_identifier[1] != betrokkene_type) {
                    $('#ezra_id_aanvrager input[name="ztc_aanvrager_id"]').val('');
                    $('#ezra_id_aanvrager input[name="betrokkene_naam"]').val('');
                }
            }

            var aanvragers = $('.zaak_aanmaken #ezra_id_aanvrager .aanvragers');
            if (aanvragers.val().length && aanvragers.val().match('medewerker')) {
                $('.zaak_aanmaken #ezra_id_aanvrager .aanvragers').val('');
                $('.zaak_aanmaken #ezra_id_aanvrager .ezra_betrokkene_selector').val('');
                $('.zaak_aanmaken #ezra_id_aanvrager .ezra_betrokkene_autocomplete').val('');
            }

        } else {
            $('.zaak_aanmaken .ezra_id_bestemming').show();
            $('.zaak_aanmaken #ezra_id_aanvrager_type').hide();
            var bestemming  =
                $('.zaak_aanmaken .ezra_id_bestemming input[type="radio"]:checked').val();

            if (
                !$('.zaak_aanmaken #ezra_id_aanvrager .aanvragers').val() ||
                !$('.zaak_aanmaken #ezra_id_aanvrager .aanvragers').val().match('medewerker')
            ) {
                $('.zaak_aanmaken #ezra_id_aanvrager .aanvragers').val(
                    $('.zaak_aanmaken .ezra_id_value_medewerker_id').val()
                );
                $('.zaak_aanmaken #ezra_id_aanvrager .ezra_betrokkene_autocomplete').val(
                    $('.zaak_aanmaken .ezra_id_value_medewerker_naam').val()
                );

            }

            if (bestemming == 'extern') {
                $('.zaak_aanmaken #ezra_id_ontvanger').show();
                ontvanger_type = $('.zaak_aanmaken #ezra_id_ontvanger_type '
                   + ' .ezra_betrokkene_type input[type="radio"]:checked').val();

                // Check if betrokkene_type matches the id of the betrokkene
                var ontvanger_identifier = $('#ezra_id_ontvanger input[name="ontvanger"]').val();

                if (ontvanger_identifier) {
                    ontvanger_identifier = ontvanger_identifier.match(/betrokkene-(.*?)-/);

                    if (ontvanger_identifier && ontvanger_identifier[1] != ontvanger_type) {
                        $('#ezra_id_ontvanger input[name="ontvanger"]').val('');
                        $('#ezra_id_ontvanger input[name="betrokkene_naam"]').val('');
                    }
                }


                $('.zaak_aanmaken #ezra_id_ontvanger_type').show();
            } else {
                $('.zaak_aanmaken #ezra_id_ontvanger_type').hide();
                $('.zaak_aanmaken #ezra_id_ontvanger').hide();
            }
        }

        restore_remembered_zaaktype(ztc_trigger);

        /* Change betrokkene type in aanvrager selector */
        var rel = $('.zaak_aanmaken #ezra_id_aanvrager a').attr('rel');
        if (rel) {
            rel = rel.replace(
                /betrokkene_type: (.*?);/,
                'betrokkene_type: ' + betrokkene_type + ';'
            );

            $('.zaak_aanmaken #ezra_id_aanvrager a').attr('rel',rel);
        }
        rel = $('.zaak_aanmaken #ezra_id_ontvanger a').attr('rel');
        if (rel) {
            rel = rel.replace(
                /betrokkene_type: (.*?);/,
                'betrokkene_type: ' + ontvanger_type + ';'
            );

            $('.zaak_aanmaken #ezra_id_ontvanger a').attr('rel',rel);
        }

        var select_zaaktype = $('a.ezra_select_zaaktype');

        rel = select_zaaktype.attr('rel');
        rel = rel.replace(/betrokkene_type: (.*?),/, 'betrokkene_type: ' + betrokkene_type + ',');
        rel = rel.replace(/trigger: (.*?);/, 'trigger: ' + ztc_trigger + ';');
        select_zaaktype.attr('rel', rel);

        var betrokkene_type_aligned = betrokkene_type;
        if(betrokkene_type == 'bedrijf') {
            betrokkene_type_aligned = 'niet_natuurlijk_persoon';
        }
        select_zaaktype.ezra_search_box('update_options', {trigger: ztc_trigger, betrokkene_type: betrokkene_type_aligned});

        /* Make sure triggers are enabled */
        $('.zaak_aanmaken .ezra_trigger input[type="radio"],'
            + '.zaak_aanmaken #ezra_id_aanvrager_type input[type="radio"],'
            + '.zaak_aanmaken #ezra_id_ontvanger_type input[type="radio"],'
            + '.zaak_aanmaken .ezra_id_bestemming input[type="radio"]'
        ).unbind().change(function() {
            $('form .zaak_aanmaken').trigger('refreshForm');
        });

        /* Change classes */
        var element = $('form .zaak_aanmaken');
        var classes = element.attr('class').split(/\s+/);

        var pattern = /^refreshform-/;

        for(var i = 0; i < classes.length; i++){
          var className = classes[i];

          if(className.match(pattern)){
            element.removeClass(className);
          }
        }

        $('form .zaak_aanmaken').addClass(
            'refreshform-trigger-' + ztc_trigger
        ).addClass(
            'refreshform-betrokkene_type-' + betrokkene_type
        );

        $('.zaak_aanmaken').closest('form').unbind().submit(function() {
            return request_validation(
                $(this),
                null,
                {
                    events: {
                        submit: function (formelem) {
                            formelem.submit();
                            $('#ezra_nieuwe_zaak_tooltip').trigger({
                                type: 'nieuweZaakTooltip',
                                hide: 1,
                                keeploader: 1
                                }
                            );
                        }
                    }
                }
            );
        });


        $('.ezra_zaaktype_keuze .ezra_kies_zaaktype')
            .addClass('ezra_zaaktype_keuze-initialized');
    });

    if ($('form .zaak_aanmaken').length) { $('form .zaak_aanmaken').trigger('refreshForm'); }

    $('form .contactmoment_aanmaken').unbind('refreshForm').bind('refreshForm', function() {
        /* Retrieve trigger and betrokkene_type */
        var ztc_trigger     = 'extern';
        var betrokkene_type = 'natuurlijk_persoon';

        current_class   = $('form .contactmoment_aanmaken').attr('class');

        betrokkene_type = $('.contactmoment_aanmaken'
           + ' .ezra_betrokkene_type input[type="radio"]:checked').val();

        /* Change betrokkene type in aanvrager selector */
        var rel = $('.contactmoment_aanmaken .ezra_id_contact a').attr('rel');
        if (rel) {
            rel = rel.replace(
                /betrokkene_type: (.*?);/,
                'betrokkene_type: ' + betrokkene_type + ';'
            );

            $('.contactmoment_aanmaken .ezra_id_contact a').attr('rel',rel);
        }

        /* Make sure triggers are enabled */
        $('.contactmoment_aanmaken .ezra_betrokkene_type input[type="radio"]').unbind()
            .change(function() {
                $('form .contactmoment_aanmaken').trigger('refreshForm');
            });

        /* Change classes */
        var element = $('form .contactmoment_aanmaken');
        var classes = element.attr('class').split(/\s+/);

        var pattern = /^refreshform-/;

        for(var i = 0; i < classes.length; i++){
          var className = classes[i];

          if(className.match(pattern)){
            element.removeClass(className);
          }
        }

        $('form .contactmoment_aanmaken').addClass(
            'refreshform-trigger-' + ztc_trigger
        ).addClass(
            'refreshform-betrokkene_type-' + betrokkene_type
        );

        $('.contactmoment_aanmaken').closest('form').unbind().submit(function() {
            return request_validation(
                $(this),
                null,
                {
                    events: {
                        submit: function (formelem) {
                            formelem.submit();
                            $('#ezra_nieuwe_zaak_tooltip').trigger({
                                type: 'nieuweZaakTooltip',
                                hide: 1,
                                keeploader: 1
                                }
                            );

                            return true;
                        }
                    }
                }
            );
        });


        $('.ezra_zaaktype_keuze .ezra_kies_zaaktype')
            .addClass('ezra_zaaktype_keuze-initialized');
    });

    if ($('form .contactmoment_aanmaken').length) { $('form .contactmoment_aanmaken').trigger('refreshForm'); }

    $('form .contact_aanmaken').unbind('refreshForm').bind('refreshForm', function() {
        /* Make sure triggers are enabled */
        $('.contact_aanmaken .ezra_betrokkene_type input[type="radio"]').unbind()
            .change(function() {
                betrokkene_type = $('.contact_aanmaken'
                   + ' .ezra_betrokkene_type input[type="radio"]:checked').val();

                $('.ezra_nieuwe_zaak_tooltip-content').load(
                    $('.ezra_nieuwe_zaak_tooltip-content form').attr('action'),
                    {
                        'betrokkene_type': betrokkene_type
                    },
                    function () {
                        initializeEverything();
                    }
                );
            });


        $('.contact_aanmaken').closest('form').unbind().submit(function() {
            return request_validation(
                $(this),
                null,
                {
                    events: {
                        submit: function (formelem) {
                            $('#ezra_nieuwe_zaak_tooltip').trigger({
                                type: 'nieuweZaakTooltip',
                                hide: 1
                                }
                            );

                            $.getJSON(
                                formelem.attr('action'),
                                formelem.serialize(),
                                function(data) {
                                    var result = data.json,
                                        bericht = result.bericht;

                                    if(bericht) {
                                        var bodyScope = angular.element(document.body).scope();
                                        if(bodyScope) {
                                            bodyScope.$root.$broadcast('systemMessage', {
                                                content: result.bericht,
                                                type: 'info'
                                            });
                                        }
                                    }

                                    initializeEverything();
                                }
                            );
                        }
                    }
                }
            );
        });


        $('.ezra_zaaktype_keuze .ezra_kies_zaaktype')
            .addClass('ezra_zaaktype_keuze-initialized');
    });

    if ($('form .contact_aanmaken').length) { $('form .contact_aanmaken').trigger('refreshForm'); }


    $('.ezra_select_betrokkene input[type="text"]').attr('readonly','readonly');


    $('.ezra_case_next_phase_notification_send_date').attr('size','10')
        .datepicker({
            dateFormat: 'dd-mm-yy',
            changeYear: true,
            minDate: 0,
            'beforeShow': function(input, datepicker) {
                setTimeout(function() {
                    $('#ui-datepicker-div').css('zIndex', 10000);
                }, 250);
            }
        });

    $('.ezra_betrokkene_autocomplete').unbind('input').bind('input', function ( ) {
        $(this).closest('.ezra_objectsearch_betrokkene').find('.ezra_betrokkene_field_id').val('');
    });
}






function initialize_milestone_zaaktype_selector() {
    var selected_option = $('select.ezra_milestone_zaak_type option:selected');
    var selected_option_value = selected_option.val();

    var form = selected_option.closest('form');
    if(selected_option_value == 'vervolgzaak' || selected_option_value == 'vervolgzaak_datum') {
        form.find('.ezra_milestone_zaak_type_starten').show();
    } else {
        form.find('.ezra_milestone_zaak_type_starten').hide();
    }

    var starten_input = form.find('.ezra_milestone_zaak_type_starten').find('input');

    if (selected_option_value == 'vervolgzaak_datum') {
        starten_input
            .attr('size','10')
            .datepicker({
                dateFormat: 'dd-mm-yy',
                changeYear: true,
                'beforeShow': function(input, datepicker) {
                    setTimeout(function() {
                        $('#ui-datepicker-div').css('zIndex', 10000);
                    }, 250);
                }
            });

        form.find('.label_dagen').hide();

        if ( !starten_input.val().match(/^\d+\-\d+\-\d+$/) ) {
            starten_input.val('');
        }

    } else {
        starten_input
            .attr('size','4')
            .datepicker('destroy');

        form.find('.label_dagen').show();

        if (starten_input.val() && !starten_input.val().match(/^\d+$/) ) {
            starten_input.val('0')
        }
    }

    var subject_role = form.find('select[name="relaties_betrokkene_role_set"]').val();

    form.find('.ezra_milestone_zaak_betrokkene_custom_role').toggle(subject_role == 'Anders');

    var owner_type = form.find('select[name="relaties_eigenaar_type"]').val();

    $('#ezra_id_aanvrager,#ezra_id_aanvrager_type').toggle(owner_type == 'anders');

    if (owner_type == 'betrokkene') {
        form.find('.ezra_relatie_rcpt_betrokkene').show();
    }
    else {
        form.find('.ezra_relatie_rcpt_betrokkene').hide();
    }

    var authorized = form.find('input[name="relaties_betrokkene_authorized"]').prop('checked');

    form.find('input[name="relaties_betrokkene_notify"]').prop('disabled', !authorized);

    var relation_automatic_row = form.find('.ezra_milestone_case_relation_automatic');

    if (selected_option_value == 'deelzaak') {
        form.find('.ezra_milestone_zaak_deelzaak_only').show();
    } else {
        form.find('.ezra_milestone_zaak_deelzaak_only').hide();
    }

    if (form.find('.show_add_subject').prop('checked')) {
        form.find('.add_subject').show();
    } else {
        form.find('.add_subject').hide();
    }

    if (form.find('.show_add_subject_role').prop('checked')) {
        form.find('.add_subject_roles').show();
    }
    else {
        form.find('.add_subject_roles').hide();
    }

    if (form.find('.show_in_pip').prop('checked')) {
        form.find('.in_pip').show();
    } else {
        form.find('.in_pip').hide();
    }

    var milestone_last = form.find('input[name="milestone_last"]').val();

    function updateSidebarData ( ) {
        var caseViewCtrl = angular.element(form[0]).inheritedData('$zsCaseViewController');
        if(caseViewCtrl) {
            angular.element(form[0]).scope().$apply(function ( ) {
                caseViewCtrl.reloadData();
            });
        }
    }

    if(selected_option_value === 'deelzaak' && milestone_last) {
        if($.zaaknr) {
            //console.log('uncheck and disable checkbox on angular action accordion')
            updateSidebarData();
        } else {
            relation_automatic_row.hide();
        }
    } else {
        if($.zaaknr) {
            updateSidebarData();
        } else {
            relation_automatic_row.show();
        }
    }

}



function restore_remembered_zaaktype(ztc_trigger) {
    // get the remembered zaaktype information from the json candy bag
    var form                        = $('form[name="aanvrager_form"]');
    var remembered_zaaktype_json    = form.find('input[name="remembered_zaaktype"]').val()
    var prefill_zaaktype_json       = form.find('input[name="prefill_zaaktype"]').val()

    if(prefill_zaaktype_json) {
        var prefill_zaaktype     = eval('(' + prefill_zaaktype_json + ')');

        if(prefill_zaaktype[ztc_trigger]) {
            var prefill_zaaktype_id      = prefill_zaaktype[ztc_trigger].zaaktype_id;
            var prefill_zaaktype_titel   = prefill_zaaktype[ztc_trigger].zaaktype_titel;

            if(prefill_zaaktype_id && prefill_zaaktype_titel) {
                $('.zaak_aanmaken input[name="zaaktype_name"]').val(prefill_zaaktype_titel);
                $('.zaak_aanmaken input[name="zaaktype_id"]').val(prefill_zaaktype_id);
                $('.zaak_aanmaken input[name="remember_zaaktype"]').attr('checked',null);
            }
            return;
        }
    } else if (remembered_zaaktype_json) {
        var remembered_zaaktype     = eval('(' + remembered_zaaktype_json + ')');

        if(remembered_zaaktype[ztc_trigger]) {
            var remembered_zaaktype_id      = remembered_zaaktype[ztc_trigger].zaaktype_id;
            var remembered_zaaktype_titel   = remembered_zaaktype[ztc_trigger].zaaktype_titel;

            if(remembered_zaaktype_id && remembered_zaaktype_titel) {
                $('.zaak_aanmaken input[name="zaaktype_name"]').val(remembered_zaaktype_titel);
                $('.zaak_aanmaken input[name="zaaktype_id"]').val(remembered_zaaktype_id);
                $('.zaak_aanmaken input[name="remember_zaaktype"]').attr('checked','checked');
            }
            return;
        }
    }


    $('.zaak_aanmaken input[name="remember"]').attr('checked', null);

    // This will empty the zaaktype when changing an aanvrager when zaaktype it is filled
    // by hand. To prevent it to from emptiing, we comment out this code.

    // $('.zaak_aanmaken input[name="zaaktype_name"]').val('');
    // $('.zaak_aanmaken input[name="zaaktype_id"]').val('');
}



function ezra_basic_selects() {

    // LOOK BELOW FOR VERSION 3 EXAMPLE, this one sucks ass
    $(document).on('click', '.ezra_search_betrokkene, #ezra_input_search_betrokkene_intern, #ezra_input_search_betrokkene_extern', function() {

        var formcontainer   = $(this).closest('form');

        var options         = getOptions($(this).attr('rel'));

        var trigger         = formcontainer.find('input[name="ztc_trigger"]').val();

        var betrokkene_type;
        if (options['betrokkene_type']) {
            betrokkene_type     = options['betrokkene_type'];
        } else {
            if (formcontainer.find('input[name="betrokkene_type"]').attr('type') == 'radio') {
                betrokkene_type = formcontainer.find('input[name="betrokkene_type"]:checked').val();
            } else {
                betrokkene_type = formcontainer.find('input[name="betrokkene_type"]').val();
            }
        }

        var title           = $(this).attr('title');

        ezra_dialog({
            url : '/betrokkene/search',
            cgi_params: {
                jsfill: trigger,
                jstype: betrokkene_type,
                jsversion: 2
            },
            layout: "large",
            which: '#searchdialog'
        });

        return false;
    });


    // VERSION 3
    $(document).on('click', '.ezra_betrokkene_selector', function() {
        var obj = $(this);

        var formcontainer   = $(this).closest('form');
        var aelem;
        // Search for the href containing rel
        if (!$(this).attr('rel')) {
            aelem   = $(this).closest('div').find('a.ezra_betrokkene_selector');
        } else {
            aelem   = $(this);
        }

        var options         = getOptions(aelem.attr('rel'));
        var title           = aelem.attr('title');


        var betrokkene_type = getBetrokkeneType(obj.closest('div.ezra_objectsearch_betrokkene'));
        if (!options['selector_identifier']) { return false; }

        ezra_dialog({
            url: '/betrokkene/search',
            cgi_params: {
                ezra_client_info_selector_identifier: options['selector_identifier'],
                ezra_client_info_selector_naam: options['selector_naam'],
                betrokkene_type: betrokkene_type,
                jsversion: 3,
                allow_external_search: (options['allow_external_search'] ? 1 : 0)
            },
            layout: "large",
            title: obj.attr('title'),
            which: '#searchdialog'
        });
        return false;
    });

    $(document).on('change', '.ezra_org_eenheid', function() {
        var container = $(this).parents('div.ezra_select_betrokkene');

        container.find('input[name="ztc_aanvrager_id"]').val($(this).val());
    });
}


function trim(value) {
    value = value.replace(/^\s+/,'');
    value = value.replace(/\s+$/,'');
    return value;
}

function ezra_basic_widget_auth() {
    $('.widget_auth_select select.auth_select_ou').each(function() {
        var obj = $(this),
            container = $(this).closest('.widget_auth_select');


        container.find('select.auth_select_role').unbind('change').change(function ( ) {
            var selectedMatches = $(this).attr('class').match(/(selected-id)-(\d+)/),
                selectedClass = selectedMatches ? selectedMatches[2] : null;

            if(selectedClass) {
                $(this).removeClass(selectedClass);
            }

            $(this).addClass('selected-id-' + $(this).val());
        });

        $(this).unbind('change').change(function() {
            $(this).find(':selected').each(function() {
                var elementcontent = trim($(this).html());
                elementcontent = elementcontent.replace(/^(&nbsp;)*/,'');
                elementcontent = elementcontent.replace(/&amp;/,'&');
                var url = '/auth/retrieve_roles/' + $(this).val();

                if(obj.closest('.widget_auth_select').hasClass('ezra_auth_select_complete_afdeling')) {
                    url += '?select_complete_afdeling=1';
                }

                // Refresh roles based on scope data
                $.getJSON(
                    url,
                    function(data) {
                        var roles       = data.json.roles;
                        var role_select = container.find('select.auth_select_role');

                        /* Know what to select */
                        var behandelaar = null;
                        var selected    = null;
                        var selected_id = null;
                        var foundselected = false;

                        /* Find selected role */
                        var selected_matches = role_select.attr('class').match(/(selected-id)-(\d+)/);
                        if (selected_matches) {
                            selected_id     = selected_matches[2];
                        }


                        /* Remove old roles */
                        role_select.empty();
                        emptied = 1;

                        // propagate all roles
                        var localcount  = 0;
                        for (var i in roles) {
                            localcount = (localcount + 1);
                            var role=roles[i];

                            if (role.name == 'Behandelaar') {
                                behandelaar = role;
                            }

                            if (role.internal_id == selected_id) {
                                foundselected = true;
                            }

                            if (typeof(role.internal_id) != 'undefined') {
                                role_select.append(
                                    '<option value="' + role.internal_id + '"' + selected + ' >' + htmlEscape(role.name) +
                                    '</option>'
                                );
                            } else if (localcount > 1) {
                                role_select.append('<option value=""'
                                    + '>-----</option>');
                            }
                        }

                        role_select.find('option').each(function() {
                            if (!foundselected && $(this).html() == 'Behandelaar') {
                                $(this).attr('selected','selected');
                            } else if (foundselected) {
                                if ($(this).val() == selected_id) {
                                    $(this).attr('selected','selected');
                                }
                            }
                        });
                    }
                );
            });
        });

        $(this).change();
    });
}


function beheer_zaaktype_auth_add() {
    $('.element_tabel_auth thead').show();
    $('.element_tabel_auth .auth_message').hide();
}

function beheer_zaaktype_auth_delete() {
    var count = $('.element_tabel_auth table tr').length;
    if(count < 3) {
        $('.element_tabel_auth thead').hide();
        $('.element_tabel_auth .auth_message').show();
    }
}


function ezra_basic_widget_term(elem, changed) {
    widget_content  = elem.closest('div').find('.widget_term_content');
    widget_value    = elem.find(':selected').val();

    /* Remove value from datepicker */
    if (widget_value == 'einddatum') {
        widget_content.removeClass('veldoptie_numeric');
        widget_content.attr('size', '10');
        widget_content.datepicker({
            dateFormat: 'dd-mm-yy'
        });
        if (changed) {
            widget_content.val('');
            widget_content.datepicker('show');
        }
    } else {
        if (changed && widget_content.attr('size') != 2) {
            widget_content.val('');
        }
        widget_content.attr('size', '2');
        widget_content.datepicker('destroy');
        widget_content.addClass('veldoptie_numeric');
    }
}

function ezra_basic_zaak_functions() {

    $('.ezra_zaakinformatie_accordion').each( function() {
        var obj = $(this);

        // avoid strange behaviour resulting from multiple initializations. this will become
        // obsolete when the javascript code is reorganized so that when ajax loaded html is
        // treated with jquery handling no multiple handling occurs anymore.
        if(obj.hasClass('ui-accordion')) {
            return;
        }

        $(this).accordion({
            heightStyle: 'content',
            active: false,
            change: function(event,ui) {
                if (
                    ui.newHeader.hasClass('ezra_load_zaak_element') &&
                    !ui.newHeader.hasClass('ezra_load_zaak_element_loaded')
                ) {
                    ui.newHeader.find('img').show();

                    var match = ui.newHeader.attr('class').match(/zaak_nr_(\d+)/);
                    var zaaknr = match[1];

                    var element_url = '/zaak/' + zaaknr + '/view_element/' + ui.newHeader.attr('id');
                    if (ui.newHeader.hasClass('pip')) {
                        element_url = '/pip' + element_url;
                    }

                    ui.newContent.load(
                        element_url,
                        function(response,status,xhr) {
                            if (status!="error") {
                                ui.newHeader.find('img').hide();
                                if (ui.newHeader.hasClass('element_maps')) {
                                    ezra_openlayers();
                                }

                                ezra_basic_functions();
                            }
                        }
                    );

                    ui.newHeader.addClass('ezra_load_zaak_element_loaded');
                }
            },
            collapsible: true
        });
    });


    $('.submitWaiter').each(function() {
        $(this).find('form').submit(function() {
            $.ztWaitStart();
        });
    });
}

function ezra_document_functions() {
    $('.ezra_view_sjabloon form').unbind().submit(function() {
        //$('#dialog').dialog('close');
        return true;
    });

    $('.select_zt_document').unbind().change(function() {
        var selected    = $(this).find(':selected');
        var currentform = $(this).closest('form');

        var zaaknr      = $(this).attr('class');
        zaaknr          = zaaknr.replace(/.*zaak_(\d+).*/, '$1');
        var zaakdef     = 0;

        if (!zaaknr.match(/^\d+$/)) {
            if (zaaknr.match(/zaakdefinitie/)) {
                zaakdef = zaaknr.replace(/.*zaakdefinitie_(\d+).*/, '$1');
                zaaknr  = '';
            }
        }

        currentform.find('.document_constraint_false').hide();
        currentform.find('.document_constraint_true').hide();

        if ($(this).val()) {
            currentform.find('.document_constraint_true').show();

            $.getJSON('/zaak/' + (zaaknr ? zaaknr : 'documents') + '/get_catalogus_waarden/'
                + selected.val(),
                {
                    zaakdefinitie: zaakdef
                },
                function(data) {
                    var catalogus = data.json.catalogus;

                    currentform.find('.document_constraint_pip').html(
                        (catalogus.pip ? 'Ja' : 'Nee')
                    );
                    currentform.find('.document_constraint_verplicht').html(
                        (catalogus.verplicht ? 'Ja' : 'Nee')
                    );
                    currentform.find('.document_constraint_categorie').html(
                        catalogus.categorie
                    );


                }
            );
        } else {
            currentform.find('.document_constraint_false').show();
        }
    });

    $('.select_zt_document').change();

    ezra_basic_zaak_functions();
}


 /*

 */


function js_include(filename)
{
    var head = document.getElementsByTagName('head')[0];

    script = document.createElement('script');
    script.src = filename;
    script.type = 'text/javascript';

    head.appendChild(script)
}

function ezra_basic_functions() {
    actie_button_handling();


    veldoptie_handling();
    if ($('.ezramap_container').length) {
        ezra_openlayers();
    }

    ezra_document_functions();

    /* Documentenintake */
    $('.ztAjaxUpdate').ztAjaxUpdate();
    $('.ztSpinnerWait').submit(function() {
        $.ztWaitStart();
    });

    $("input[type=radio], input[type=checkbox]").css({border:0});


    // View betrokkene


    ezra_basic_beheer_functions();
    ezra_basic_zaak_intake();

    $('#regel_definitie').regel_editor();
}

function actie_button_handling() {
    if ($('.ezra_actie_button_handling').length) {
        $('.ezra_actie_button_handling').unbind('actieButton').bind('actieButton', function(options) {
            var navigation  = $(this).find('.ezra_actie_button_handling_navigation');
            var popover     = $(this).find('.ezra_actie_button_handling_popover');
            popover.find('.popover-tip').hide();

            if (options['hide']) {
                navigation.find('a').removeClass('button-active');
                navigation.removeClass('ezra_actie_button_handling-active');
                popover.removeClass('ezra_actie_button_handling-active');
                popover.find('div.popover').hide();
                popover.find('div.popover-tip').hide();
            }

            // initialize
            if ($(this).hasClass('ezra_actie_button_handling-initialized')) {
                return false;
            }

            navigation.find('.button-group a').unbind('click').click(function() {

                var href = $(this).attr('href');

                if (!href || !href.match(/^#/)) {
                    return true;
                }

                var dest = $('#popover_' + href.replace('#', ''));

                if (!dest.length) {
                    return false;
                }

                if ($(this).hasClass('button-active')) {
                    $('.ezra_actie_button_handling').trigger({
                        type: 'actieButton',
                        hide: 1
                    });
                    return false;
                }

                /* Remove active from other buttons */
                $(this).closest('div:not(.button-group)').find('.button-group').find('a').removeClass('button-active');

                /* Display none all destinations */
                popover.find('div.popover').hide();

                dest.show();
                $(this).addClass('button-active');
                navigation.addClass('ezra_actie_button_handling-active');
                popover.addClass('ezra_actie_button_handling-active');






                dest.find('a').click(function() {
                    $('.ezra_actie_button_handling').trigger({
                        type: 'actieButton',
                        hide: 1
                    });
                });

                return false;
            });




            $(this).addClass('ezra_actie_button_handling-initialized');
        });

        $('.ezra_actie_button_handling').trigger('actieButton');
    }


}


function veldoptie_handling() {
    /* multiple veldopties */
    $(document).off('click', '.veldoptie_multiple .add')
        .on('click', '.veldoptie_multiple .add', function() {
            var lastrow = $(this).closest('div').find('li:last');
            var clone = lastrow.clone();
            clone.find('input')
                .val('')
                .attr('selected', null)
                .attr('checked', null);
            clone.find('textarea').val('');
            clone.find('option').attr('selected',null);

            lastrow.after(clone);
            updateDeleteButtons();
            return false;
        });

    updateDeleteButtons();

    $(document).off('click', '.veldoptie_multiple .del')
        .on('click', '.veldoptie_multiple .del', function() {
            var myrow = $(this).closest('li'),
                parent = myrow.parent();

            myrow.remove();
            updateDeleteButtons();
            updateField(parent);

            return false;
        });
    /* ----------------- */

    /* ZS-15169: Don't allow incorrect input */
    $(document).on('keyup', '.veldoptie_valuta_field', function() {
        var val = $(this).val();

        val = val.replace(/[^0-9,]/, '');
        val = val.replace(/(,[0-9]{2}).+/, "$1");

        var number =  val.replace(/,/, '.');

        if (isNaN(number)) {
            $(this).val(0);
        }
        else {
            $(this).val(val);
        }
    });

    $('.veldoptie_datepicker').each(function() {
        var id  = $(this).attr('id'),
            options = {
                dateFormat: 'dd-mm-yy',
                changeYear: true,
                yearRange: '1900:c+20',
                regional: 'nl',
                'beforeShow': function(input, datepicker) {
                    setTimeout(function() {
                        $('#ui-datepicker-div').css('zIndex', 10000);
                    }, 250);
                },
                'onClose': function ( dateText ) {

                    var spl =
                        (dateText || '')
                            .split('-')
                            .map(Number)
                            .filter(function ( val ) { return !isNaN(val); }),
                        date = spl.length === 3 ? new Date(spl[2], spl[1] - 1, spl[0]) : null;

                    if (!date || isNaN(date.getTime())) {
                        $(this).datepicker(
                            'setDate',
                            date
                        );
                    }

                }
            };

        if ($(this).hasClass('veldoptie_datepicker_no_future')) {
            options.maxDate = 0;
        }

        $('#' + id).datepicker(options);
    });

    $('.veldoptie_datepicker_datelimit').each(function() {
        var id  = $(this).attr('id'),
            options = {
                dateFormat: 'dd-mm-yy',
                changeYear: true,
                yearRange: '1900:c+20',
                regional: 'nl',
                'beforeShow': function(input, datepicker) {
                    setTimeout(function() {
                        $('#ui-datepicker-div').css('zIndex', 10000);
                    }, 250);
                },
                'onClose': function ( dateText ) {

                    var spl =
                        (dateText || '')
                            .split('-')
                            .map(Number)
                            .filter(function ( val ) { return !isNaN(val); }),
                        date = spl.length === 3 ? new Date(spl[2], spl[1] - 1, spl[0]) : null;

                    if (!date || isNaN(date.getTime())) {
                        $(this).datepicker(
                            'setDate',
                            date
                        );
                    }

                }
            },
            datepicker = $('#' + id);

        var start = _datelimit_get_options(this, 'start');
        var end   = _datelimit_get_options(this, 'end');

        if (start.active) {
            options.minDate = start.date;
        }
        if (end.active) {
            options.maxDate = end.date;
        }

        datepicker.datepicker(options);

        // make sure all options are changed if datepicker has already been initialized

        _.each(options, function ( value, key ) {
            if(datepicker.datepicker('option', key) !== value) {
                datepicker.datepicker('option', key, value);
            }
        });

    });

    function _datelimit_get_options(some, thing) {
        var dl = {};
        dl.active = $(some).attr('datepicker_' + thing + '_active') || 0;
        if (dl.active) {
            dl.reference = $(some).attr('datepicker_' + thing + '_reference');
            dl.num = $(some).attr('datepicker_' + thing + '_num');
            dl.during = $(some).attr('datepicker_' + thing + '_during');
            dl.term = $(some).attr('datepicker_' + thing + '_term');
            dl.date = _datelimit_select_date(dl);
        }
        return dl;
    }

    function _datelimit_select_date(dl) {
            var date_object,
                time = Date.now();

            if(dl.reference !== 'current') {
                time = (function getTime ( ) {

                    var input = document.querySelector('input[name="' + dl.reference + '"]'),
                        t = NaN;

                    if(input && input.value) {
                        try {
                            t = jQuery.datepicker.parseDate('dd-mm-yy', input.value).getTime();
                        } catch ( error ) {

                        }
                    }

                    return t;

                })();
            }

            date_object = new Date(time);

            var d = dl.during;
            var num = Number(dl.num);

            if (dl.during == 'pre') {
                num *= -1;
            }

            if (dl.term == 'days') {
                date_object.setDate(date_object.getDate() + num);
            }
            else if (dl.term == 'weeks') {
                num *= 7;
                date_object.setDate(date_object.getDate() + num);
            }
            else if (dl.term == 'months') {
                date_object.setMonth(date_object.getMonth() + num)
            }
            else {
                date_object.setFullYear(date_object.getFullYear() + num)
            }

            return date_object;
    }

    $('.veldoptie_valuta').each(function() {
        veldoptie_valuta_calc($(this));
        $(this).find('input').keyup(function() {
            /* Recalculate valuta */
            parentdiv = $(this).closest('div');
            veldoptie_valuta_calc(parentdiv);
        });
    });

    $(document).on('keyup', '.veldoptie_text_uc', function() {
        var val = $(this).val(),
            replaced = val.toUpperCase();

        if(val !== replaced) {
            $(this).val(replaced);
        }
    });

    var bag_results = {};
    if ($('.veldoptie_bag_adres_container').length) {
        //alert($('.veldoptie_bag_adres_uitvoer input[type="text"]:disabled').length);


        $('.veldoptie_bag_adres.invoer.huisnummer').keydown(function() {
            var postcodefield   = $(this).closest('div.veldoptie_bag_adres_container').find('.veldoptie_bag_adres.invoer.postcode');
            var straatfield     = $(this).closest('div.veldoptie_bag_adres_container').find('.veldoptie_bag_adres.invoer.straatnaam');

            var uitvoercontainer = $(this).parents('div.veldoptie_bag_adres_container').find('.veldoptie_bag_adres_uitvoer');

            if (postcodefield.length) {
                if (postcodefield.val() && postcodefield.val().match(/^\d{4} ?\w{2}$/)) {
                    if ($(this).attr('disabled')) {
                        $(this).attr('disabled',null);
                    }
                } else {
                    $(this).attr('disabled','disabled');
                    alert('Voer een geldige postcode in a.u.b., in de vorm 1400AA');
                }

                /* Wipe data from adres */
                uitvoercontainer.find('.huisnummer').val('');
                uitvoercontainer.find('.bagid').val('');
                uitvoercontainer.find('.straatnaam').val('');
            }


            if (straatfield.length) {
                if (straatfield.val() && uitvoercontainer.find('.straatnaam').val()) {
                    if ($(this).attr('disabled')) {
                        $(this).attr('disabled',null);
                    }
                } else {
                    $(this).attr('disabled','disabled');
                    alert('Voer een geldige straatnaam in a.u.b.');
                }
            }

            return true;

        });

        $('.veldoptie_bag_adres.invoer.huisnummer').unbind('blur').blur(function() {
            var uitvoercontainer = $(this).closest('div.veldoptie_bag_adres_container')
            var itemvalue = $(this).val();

            if (!bag_select_result(uitvoercontainer, itemvalue)) {
                $(this).val('');
            }
        });

        $('.veldoptie_bag_adres.invoer.huisnummer').click(function() {
            var postcodefield   = $(this).closest('div.veldoptie_bag_adres_container').find('.veldoptie_bag_adres.invoer.postcode');
            var straatfield     = $(this).closest('div.veldoptie_bag_adres_container').find('.veldoptie_bag_adres.invoer.straatnaam');

            var uitvoercontainer = $(this).parents('div.veldoptie_bag_adres_container').find('.veldoptie_bag_adres_uitvoer');

            if (postcodefield.length) {
                if (postcodefield.val() && postcodefield.val().match(/^\d{4} ?\w{2}$/)) {
                    if ($(this).attr('disabled')) {
                        $(this).attr('disabled',null);
                    }
                } else {
                    $(this).attr('disabled','disabled');
                    alert('Voer een geldig postcode in a.u.b., in de vorm 1400AA');
                }
            }

            if (straatfield.length) {
                if (straatfield.val() && uitvoercontainer.find('.straatnaam').val()) {
                    if ($(this).attr('disabled')) {
                        $(this).attr('disabled',null);
                    }
                } else {
                    $(this).attr('disabled','disabled');
                    alert('Voer een geldige straatnaam in a.u.b.');
                }
            }

        });

        $('.veldoptie_bag_adres.invoer.straatnaam').keydown(function() {
            /* Wipe data from adres */
            var uitvoercontainer = $(this).parents('div.veldoptie_bag_adres_container').find('.veldoptie_bag_adres_uitvoer');

            uitvoercontainer.find('.bagid').val('');
            uitvoercontainer.find('.straatnaam').val('');
        });


        $('.veldoptie_bag_adres.invoer.postcode').keydown(function() {
            $(this).closest('div.veldoptie_bag_adres_container').find('.veldoptie_bag_adres.invoer.huisnummer').attr('disabled',null);
        });
        $('.veldoptie_bag_adres.invoer.straatnaam').keydown(function() {
            $(this).closest('div.veldoptie_bag_adres_container').find('.veldoptie_bag_adres.invoer.huisnummer').attr('disabled',null);
        });

        $('.veldoptie_bag_adres_container .del').unbind().click(function() {
            $(this).closest('tr').remove();
            return false;
        });


        $('.veldoptie_bag_adres_container .add').unbind().click(function() {
            var uitvoer_container   = $(this).parents('div.veldoptie_bag_adres_container').find('div.veldoptie_bag_adres_uitvoer');
            var adressen_table      = uitvoer_container.parents('div.veldoptie_bag_adres_container')
                .find('table.bag_adressen');

            var veldoptie_type      = uitvoer_container.find('.veldoptie_type').val();

            var straatnaam          = uitvoer_container.find('.straatnaam').val();
            var adres               = straatnaam;

            if (
                veldoptie_type == 'bag_adres' ||
                veldoptie_type == 'bag_adressen' ||
                veldoptie_type == 'bag_straat_adressen'
            ) {
                var nummeraanduiding    = uitvoer_container.find('.huisnummer').val();
                adres                   = adres + ' ' + nummeraanduiding;
            }

            var bagid               = uitvoer_container.find('.bagid').val();
            var bagid_veldoptie     = uitvoer_container.find('.bagid').attr('name');
            bagid_veldoptie         = bagid_veldoptie.replace('_not','');


            if (!bagid) {
                return false;
            }

            adressen_table.find('tr:last').removeClass('lastrow');

            adressen_table.find('tr:last')
                .after(
                    '<tr>' +
                        '<td class="straatnaam">' +
                            '<input type="hidden" name="' + bagid_veldoptie + '" value="' + bagid + '" />' + adres +
                        '</td>' +
                        '<td class="actie">' +
                            '<a href="#" class="icon icon-del del"></a>' +
                        '</td>' +
                    '</tr>'
                );

            adressen_table.find('tr:last .del').click(function() {
                $(this).closest('tr').remove();
                if(adressen_table.find('tr').length < 2) {
                    adressen_table.removeClass('bag_adressen_padding');
                }
                return false;
            });

            if(adressen_table.find('tr').length > 1) {
                adressen_table.addClass('bag_adressen_padding');
            }

            updateField(adressen_table.find('tr:last input'));

            return false;
        });


        $('.veldoptie_bag_adres.invoer.autocomplete').autocomplete(
           {
                source: function (tag,response) {
                    var qparams         = {};
                    var searchtype      = 'hoofdadres';
                    if (this.element.hasClass('huisnummer')) {
                        if (this.element
                            .closest('div.veldoptie_bag_adres_invoer').find('.postcode')
                            .length
                        ) {
                            qparams.postcode    = this.element
                                .closest('div.veldoptie_bag_adres_invoer').find('.postcode')
                                .val();
                            qparams.postcode    = qparams.postcode.replace(' ','');
                        }
                        var straatnaam_element = this.element
                            .closest('div.veldoptie_bag_adres_invoer').find('.straatnaam');

                        if (straatnaam_element.length) {
                            var straatnaam = straatnaam_element.val();
                            if (straatnaam.match(/>/)) {
                                var citystreet      = straatnaam.match(/^(.*?) > (.*?)$/);
                                // = straatnaam.replace(/.*\> /, "");
                                qparams.straatnaam  = citystreet[2];
                                qparams.woonplaats  = citystreet[1];
                            }
                        }
                        qparams.huisnummer  = this.element.val();
                    } else {
                        qparams.straatnaam  = this.element.val();
                        searchtype          = 'openbareruimte';
                    }

                    $.ajax({
                        url: '/gegevens/bag/search?json_response=1&searchtype=' + searchtype,
                        dataType: 'json',
                        data: qparams,
                        async: false,
                        success: function (data) {
                            var results = data.json.entries;

                            var responsen = [];

                            for (var i in results) {
                                var bag=results[i];

                                var responsetag = '';

                                if (searchtype == 'hoofdadres') {
                                    responsetag = String(bag.nummeraanduiding);
                                    straatnaam  = String(bag.woonplaats + ' > ' + bag.straatnaam);
                                } else {
                                    straatnaam  = String(bag.woonplaats + ' > ' + bag.straatnaam);
                                    responsetag = String(straatnaam);
                                }

                                responsen.push(responsetag);

                                bag_results[responsetag] = {
                                    identificatie: bag.identificatie,
                                    nummeraanduiding: String(bag.nummeraanduiding),
                                    straatnaam: straatnaam,
                                    woonplaats: bag.woonplaats
                                }
                            }

                            response(responsen);
                        }
                    });
                },
                select: function(event, ui) {
                    var obj = $(this);

                    var uitvoercontainer = obj.closest('div.veldoptie_bag_adres_container')
                        .find('.veldoptie_bag_adres_uitvoer');
                    bag_select_result(uitvoercontainer, ui.item.value);
                    var element = uitvoercontainer.find('input.veldoptie_bag_adres.bagid');

                    // TODO check for other bag widgets
                    if(
                        (obj.hasClass('ezra_veldoptie_type_bag_adres')          && obj.hasClass('huisnummer')) ||
                        (obj.closest('ezra_veldoptie_type_bag_straat_adres')    && obj.hasClass('huisnummer')) ||
                        (obj.hasClass('ezra_veldoptie_type_bag_openbareruimte') && obj.hasClass('straatnaam'))
                    ) {
                        if (element.attr('name').match(/kenmerk_id_\d+$/)) {
                            updateField(element);
                        }
                    }
                }
            }
        );
    }

    function bag_select_result(uitvoercontainer, itemvalue) {
        var veldoptie_type = uitvoercontainer.find('.veldoptie_type').val();

        if (!bag_results[itemvalue]) {
            var currenttr = uitvoercontainer.closest('tr');
            currenttr.find('.validator').addClass('invalid').show();

            var errormsg = '<span></span> Combinatie postcode + huisnummer niet gevonden';

            if (
                veldoptie_type == 'bag_straat_adres' || veldoptie_type == 'bag_straat_adressen'
            ) {
                errormsg = '<span></span> Adres onvolledig, of niet gevonden';
            }

            currenttr.find('.validate-content').html(errormsg);
            return false;
        }

        uitvoercontainer.find('.straatnaam').val(
            bag_results[itemvalue].straatnaam
        );

        var do_update = false;
        if (
            veldoptie_type == 'bag_adres' || veldoptie_type == 'bag_adressen' ||
            veldoptie_type == 'bag_straat_adres' || veldoptie_type == 'bag_straat_adressen'
        ) {
            if (bag_results[itemvalue].nummeraanduiding) {
                if (
                    bag_results[itemvalue].nummeraanduiding &&
                    bag_results[itemvalue].nummeraanduiding != 'null'
                ) {
                    uitvoercontainer.find('.huisnummer').val(
                        bag_results[itemvalue].nummeraanduiding
                    );
                    do_update = true;
                }
                uitvoercontainer.find('.bagid').val(
                    bag_results[itemvalue].identificatie
                );
            }
        } else {
            uitvoercontainer.find('.bagid').val(
                bag_results[itemvalue].identificatie
            );
        }

        var currenttr = uitvoercontainer.closest('tr');
        currenttr.find('.validator').addClass('invalid').hide();

        return true;
    }

}


//
// slightly different approach, html is created serverside, we only copy existing html
// that means we can't delete the last row.. we wouldn't have anything left to copy
//
function updateDeleteButtons() {
    $('.veldoptie_multiple').each( function() {
        var count = $(this).find('.del').length;
        if(count == 1) {
            $(this).find('.del').hide();
            $(this).find('.del-inactive').show();
        } else {
            $(this).find('.del').show();
            $(this).find('.del-inactive').hide();
        }
    });
}


var veldoptie_calc_update_in_timeout = {};

function veldoptie_valuta_calc(vname) {
    var eur = vname.find('input[name^="eur"]').val();
    var cnt = vname.find('input[name^="cnt"]').val();

    if (eur == '' && cnt == '') {
        vname.find('input[type="hidden"]').val(null);
        vname.find('.value').html(null);
        return;
    }

    if (!eur) {
        eur = 0;
    }

    if (!cnt) {
        cnt = 0;
    }

    var input_name = vname.find('input[type="hidden"]').attr('name');

    if (!veldoptie_calc_update_in_timeout[input_name]) {
        veldoptie_calc_update_in_timeout[input_name] = 1;
        setTimeout(
            function() {
                updateField(vname.find('input[type="hidden"]'));
                veldoptie_calc_update_in_timeout[input_name] = 0;
            },
            500
        );
    }

    if (eur == 0 && cnt == 0) {
        vname.find('input[type="hidden"]').val(0);
        vname.find('.value').html(0);
        return;
    }

    var valuta      = eur + '.' + cnt;
    var infovalue   = vname.find('.value');

    vname.find('input[type="hidden"]').val(valuta);

    var btwmatch = infovalue.attr('class').match(/btw(\d+)/);

    if (infovalue.hasClass('exclbtw')) {
        var checkbtwvalue = 100 + Number(btwmatch[1]);
        infovalue.html(Math.round((valuta * checkbtwvalue)) / 100);
    } else if (infovalue.hasClass('inclbtw')) {
        var checkbtwvalue = ((100 + Number(btwmatch[1])) / 100)
        infovalue.html(Math.round((valuta / checkbtwvalue) * 100) / 100);
    }
}

function openDialog(title, width, height) {
    alert('openDialog called, illegal now');
    return;
    if (!width) {
        width   = 960;
        height  = 'auto';
    }

    $('#dialog').dialog('option', 'width', width);
    $('#dialog').dialog('option', 'height', height);
    $('#dialog').dialog('option', 'maxHeight', 510);
    $('#dialog').dialog('option', 'resizable', false);
    $('#dialog').dialog('option', 'zIndex', 3200);
    $('#dialog').dialog('option', 'draggable', true);

    $('#dialog').data('title.dialog', title);
    $('#dialog').addClass('smoothness')/* .parent().css('position','fixed').end() */.dialog('open');


    $('#accordion').accordion({
        heightStyle: 'content'
    });

    ezra_tooltip_handling();
}

function openSearchDialog(title, width, height) {
    alert('opensearchdialog illegal');
    return;
    if (!width) {
        width   = 790;
        height  = 'auto';
    }

    $('#searchdialog').dialog('option', 'width', width);
    $('#searchdialog').dialog('option', 'height', height);

    $('#searchdialog').dialog('option', 'resizable', false);
    $('#searchdialog').dialog('option', 'maxHeight', 510);
    $('#searchdialog').dialog('option', 'zIndex', 3200);
    $('#searchdialog').dialog('option', 'draggable', true);

    $('#searchdialog').data('title.dialog', title);
    $('#searchdialog').addClass('smoothness')/* .parent().css('position','fixed').end() */.dialog('open');

    $('#accordion').accordion({
       heightStyle: 'content'
    });
}

function getOptions(options) {
    var rv = {};

    if (!options) { return {}; }

    /* Get sets */
    sets = options.split(/;/);

    for(i = 0; i < sets.length; i++) {
        set     = sets[i];
        keyval  = set.split(/:/);
        key     = keyval[0];
        key     = key.replace(/^\s+/g, '');
        key     = key.replace(/\s+$/g, '');
        value   = keyval[1];
        if (!value) { continue; }
        value   = value.replace(/^\s+/g, '');
        value   = value.replace(/\s+$/g, '');
        rv[key] = value;
    }

    return rv;
}

function fireDialog(elem, callback) {
alert('firedialog called, illegal');
return false;
    $.ztWaitStart();

    /* Load options */
    rel     = elem.attr('rel');
    options = getOptions(rel);

    title = elem.attr('title');
    url   = elem.attr('href');

    if (!callback) {
        if (options['callback']) {
            callback = options['callback'] + '();';
        }
    }
    /* Options ok, load popup */
    $('#dialog .dialog-content').load(
        url,
        options,
        function() {

            if (callback) {
                eval(callback);
            }
            $.ztWaitStop();

            ezra_basic_functions();
            ezra_basic_zaak_functions();

            /* ARGH DIRTY!!! */
            if (url.match(/update\/deelzaak/)) {
                initializeSubzaken();
            }

            $('form.zvalidate').submit(function() {
                return zvalidate($(this));
            });

            if ($('#dialog .dialog-content form').hasClass('hascallback')) {
                var callbackfunction = $('#dialog .dialog-content form input[name="callback"]').val();
                window[callbackfunction]($('#dialog .dialog-content form'));
            }

            openDialog(title, options['width'], options['height']);
        }
    );
}


function searchBetrokkene(elem) {
alert('searchBetrokkene');
    id      = elem.parent('div').attr('id');
    rel     = elem.attr('rel');
    dtitle  = elem.attr('title');

    if (!rel) {
        return false;
    }
    options = rel.split(/;/);

    /* Load options */
    container_id = id;
    btype        = options[0];

    $('#searchdialog .dialog-content').load(
        '/betrokkene/search',
        {
            jsfill: container_id,
            jstype: btype,
            jsversion: 2
        },
        function() {
            title = 'Zoek  (' + btype + ')';
            openSearchDialog(dtitle);
        }
    );
}


/*

When the page is loaded, event handlers are attached to elements. Some elements
can be dynamically reloaded using AJAX. These event handlers must be attached using
on(). The recommended construct is

$(document).on('click', '[ELEMENT SELECTOR]', function() { ... });

Anything that's not used in AJAX reloaded content can just go in $(document).ready();

*/
$(document).ready(function(){

    $(document).on('click', '.ezra_logging_csv_export', function() {
        var obj = $(this);
        var component = obj.closest('form').find('.ezra_logging_component').val();
        var filter    = obj.closest('form').find('.zaak_search_filter').val();

        document.location.href = '/beheer/logging/?view=csv&component=' + component + '&textfilter=' + filter;
        return false;
    });

    (function ( ) {

        // var activeTab;

        // $('#tabinterface').on('tabsshow', function ( event, ui) {
        //     activeTab = ui.panel.id;
        //     console.log(activeTab);

        // });

        var scope = angular.element(document.body).scope();
        if(scope) {

            scope.$on('casedocupdate', function ( ) {
                $('[id^=zaak-elements-fase].ezra_load_zaak_element_loaded').each(function ( ) {
                    $(this).removeClass('ezra_load_zaak_element_loaded');
                })
            });

        }
    })();

    initializeEverything();

});

function check_disable_hidden_field_option(element, name) {
    var hidden_element = element.closest('#kenmerk_definitie').find('input[name="' + name + '"]');
    if(element.is(':checked')) {
        hidden_element.attr('checked', false);
        hidden_element.attr('disabled', true);
    } else {
        hidden_element.removeAttr('disabled');
    }
}

function make_checkboxes_mutual_exclusive(first, second) {
    $('#kenmerk_definitie input[name="' + first + '"]').each( function() {
        var element = $(this);
        check_disable_hidden_field_option(element, second);
    });

    $('#kenmerk_definitie input[name="' + first + '"]').click( function() {
        var element = $(this);
        check_disable_hidden_field_option(element, second);
    });

}

// Called for each element in the form. Evaluates nodes with 'zsaction' attributes
function handle_zsaction(elem) {
    var classes     = elem.attr('class').split(/\s/);
    var container   = elem.closest('.zsaction-container');
    var fieldvalue  = elem.find(':selected').val();

    jQuery.each(classes, function(key,value) {
        var matches = value.match(/zsaction-(\w+)-([^-]+)-(\w+)-([\w-]+)$/);

        if (matches === null) {
            return;
        }

        var condition_type = matches[1];
        var condition_value = matches[2]; // fieldvalue
        var action_type = matches[3];
        var action_target = matches[4];

        var target_element = container.find('.zsaction-dest-' + action_target);

        if (target_element.length === 0) {
            throw new Error("Target element '" + action_target + "' for zsaction in '" + elem.context.name + "' is empty, corrupt action definition. Hit a developer.");
        }

        if (condition_type === 'when') {
            if (fieldvalue == condition_value) {
                if (action_type === 'show') {
                    var correspondentie_landcode = container.find('[name="correspondentie_landcode"]');
                    var correspondentie_landcode_value = correspondentie_landcode.val();

                    // setting default landcode only required when processing briefadres actions
                    if (elem.context.name === 'briefadres') {
                        // for all hidden (briefadres = true) correspondence address fields and if landcode is not set
                        if (
                               target_element.is(':hidden')
                            && fieldvalue === '1'
                            && (
                                   correspondentie_landcode_value == ''
                                || correspondentie_landcode_value === undefined
                                )
                            )
                        {
                            correspondentie_landcode.val('6030'); // 6030 = Nederland by default
                        }
                    }

                    target_element.show();
                } else if (action_type === 'hide') {
                    target_element.hide();
                    //clear all target_element's inputfield values
                    target_element.find('input,select').val(undefined);
                }
            }
        } else if (condition_type === 'whennot') {
            if (fieldvalue != condition_value) {
                if (action_type === 'show' && fieldvalue !== '') {
                    target_element.show();
                } else if (action_type === 'hide') {
                    target_element.hide();
                    // clear all target_element's inputfield values
                    target_element.find('input,select').val(undefined);
                }
            }
        }
    });
}

function initializeEverything(scope_obj) {

    function setUrlInputState ( el ) {
        var input = el.find('input'),
            button = el.find('button');

        if (input.val()) {
            button.removeAttr('disabled');
        } else {
            button.attr('disabled', 'disabled');
        }
    }

	// Open a link in a new window, without a window.opener
	//
	// It's done like this, and not using "window.open(url, target, options)"
	// because then Chrome opens new windows instead of new tabs.
	function openNewWindow(url) {
		var link = document.createElement('a');
		link.target = '_blank';
		link.href = url;
		link.rel = 'noopener';
		document.body.appendChild(link);
		link.click();
		link.parentNode.removeChild(link);
	}

    $('.ezra_url_input_type').on('change keypress focus paste textInput input', function () {
        setUrlInputState($(this));
    });

    $('.ezra_url_input_type').each(function ( ) {
        setUrlInputState($(this));
    });

    $('.zsaction').each(function ( ) {
        handle_zsaction($(this));

        $(this).change(function() { $('.zsaction').each(function() { handle_zsaction($(this)); }) });
    });

    $('.ezra_url_field_goto').click(function () {
        var url,
            obj = $(this),
            ezra_url_input_type;

        ezra_url_input_type = obj.closest('.ezra_url_input_type');

        if (ezra_url_input_type.find('input').length) {
            url = ezra_url_input_type.find('input').val();
        } else {
            url = obj.attr('rel');
        }

        // add an http:// prefix unless there already is one
        if (!url.match(/^http:\/\//) && !url.match(/^https:\/\//)) {
            url = 'http://' + url;
        }

        openNewWindow(url);
        return false;
    });

    if($('.ezra_rule_schedule_mail').length) {
        $('.ezra_rule_schedule_mail').each( function() {
            var schedule_moment_obj = $(this);
            var sending_moment = schedule_moment_obj.find('.ezra_rule_schedule_mail_sending_moment select').val();

            var dynamic_sending_obj = schedule_moment_obj.find('.ezra_regel_schedule_mail_dynamic');
            if(sending_moment == 'phase_transition') {
                dynamic_sending_obj.hide();
            } else {
                dynamic_sending_obj.show();
            }
        });
    }

    if($('#relatie_definitie').length) {
        var def = $('#relatie_definitie');

        initialize_milestone_zaaktype_selector();

        var role_set = def.find('select[name="relaties_betrokkene_role_set"]');

        role_set.change(function() {
            var value = $(this).val();
            var role_input = def.find('input[name="relaties_betrokkene_role"]');

            if(value == 'Anders') {
                role_input.val('');
            } else {
                role_input.val(value);
            }

            role_input.trigger('change');
        });

        def.find('input[name="relaties_betrokkene_role"]').change(function () {
            var role = $(this).val();

            if (role == '') {
                return;
            }

            var magic_field = def.find('input[name="relaties_betrokkene_prefix"]');
            var zaak_id = $('#zaak_id').prop('class');
            var url = "/beheer/bibliotheek/kenmerken/get_magic_string";
            var data = { naam: role };

            if (zaak_id) {
                url = "/zaak/" + zaak_id + "/update/betrokkene/suggestion";
                data = { rol: role };
            }

            $.ajax(url, {
                data: data,
                dataType: 'text',
            }).done(function (data) {
                magic_field.val(data.replace('_', ''));
            }).fail(function () {
                magic_field.val(role.toLowercase().replace(' ', ''));
            });
        });

        def.find('input, textarea, select').change(function () {
            initialize_milestone_zaaktype_selector();
        });
    }

    if($('.ezra_case_action_edit').length) {
        var form = $('.ezra_case_action_edit').find('form'),
            input = form.find('input[type="submit"]');

        /* Gorious hacks. jQuery doesn't submit the name/value for the submit
           button, because the form isn't actually submitted. Hijack the
           relevant data via a clickhandler on each submit button */


        input.unbind('click').click(function(e) {
            var me = $(this);

            me.closest('form').append($('<input />').attr({
                type: 'hidden',
                name: 'trigger',
                value: me.attr('name')
            }));

            return true;
        });

        form.unbind('submit').submit( function(e) {
            var data = form.serialize(),
                id = _.find(form.serializeArray(), { name: 'id' }).value,
                el = document.querySelector('[data-action-id="' + id + '"]'),
                ctrl = angular.element(el).inheritedData('$zsCaseActionController'),
                scope = angular.element(el).scope();

            safeApply(function ( ) {
                ctrl.setLoading(true);
            });

            form.closest('#dialog').dialog('close');

            function safeApply ( fn ) {
                if(scope && !scope.$$phase) {
                    scope.$apply(fn);
                } else {
                    fn();
                }
            }

            $.post(form.attr('action'), data, function(data) {

                safeApply(function ( ) {

                    ctrl.setLoading(false);

                    if(data.redirect) {
                        window.location.href = data.redirect;
                    }

                    if(data.flash_message) {
                        if(bodyScope) {
                            bodyScope.$broadcast('systemMessage', {
                                content: data.flash_message,
                                type: 'info'
                            });
                        }
                    }

                    (function ( ) {

                        var zaakViewEl = angular.element(document.querySelector('.zaakdossier')),
                            caseViewCtrl = zaakViewEl ? angular.element(zaakViewEl).data('$zsCaseViewController') : null;

                        if(caseViewCtrl) {
                            caseViewCtrl.reloadData();
                        }

                    })();


                    var action_type = form.find('input[name="action_type"]').val();
                    var trigger = form.find('input[name=trigger]').val();
                    if(trigger == 'execute' && action_type == 'case') {

                        if($('#hoofddeelzaken_table').length) {

                            $.ztWaitStart();

                            $('#hoofddeelzaken_table').load(window.location.href + ' #hoofddeelzaken_table', function() {
                                initializeEverything();

                                $.ztWaitStop();
                            });
                        } else {

                            initializeEverything();

                        }

                        var scope = angular.element($('[data-ng-controller="nl.mintlab.case.CaseRelationController"]')).scope();
                        if(scope) {
                            scope.reloadData();
                        }
                    }
                });


            }).fail(function ( ) {
                safeApply(function ( ) {
                    ctrl.setLoading(false);
                });
            });

            return false;
        });
    }

    make_checkboxes_mutual_exclusive('kenmerken_is_systeemkenmerk', 'kenmerken_value_mandatory');
    make_checkboxes_mutual_exclusive('kenmerken_value_mandatory', 'kenmerken_is_systeemkenmerk');

    $('.ezra_dropdown_init_hover').each(function ( ) {
        $(this).find('.dropdown').css('visibility', 'visible').addClass('visuallyhidden');
    });

    $('.add-tooltip[title]').qtip({
       position: {
          my: 'bottom center',
          at: 'top center',
          viewport: $(window),
          adjust: {
                 x: 0,
                 y: -3,
                 method: 'flip shift'
              }
        },
        show: {
            event: 'mouseenter',
            solo: true,
            effect: false
         },
         hide: {
            event: 'mouseleave'
         }
    });

    // necesito Angular!! Dario help!!
    $('.table-hide').hide();
    $('.ezra_show_advanced_option').toggle(function () {
        $('.table-hide').show();

        // allow a rel attribute with pseudo-JSON to override default text
        var options = getOptions($(this).attr('rel')),
            hide_text = options.hide_text || 'Verberg geavanceerde opties';

        $(this).find('span').text(hide_text);
        $(this).find('i').addClass('icon-show-less');
    },
    function () {
        $('.table-hide').hide();

        var options = getOptions($(this).attr('rel')),
            show_text = options.show_text || 'Toon geavanceerde opties';
        $(this).find('span').text(show_text);
        $(this).find('i').removeClass('icon-show-less');
    });


    $('.ezra_show_inactive_options').on('click', function () {
        var obj = $(this),
            input = obj.find('input.ezra_show_inactive_options_input'),
            options = getOptions(obj.attr('rel')),
            hide_text = options.hide_text,
            show_text = options.show_text,
            value = input.val() ? '' : '1';

        input.val(value);

        updateSearchFilters();
    });

    $('.sensitive-data').each(function () {
        var betrokkene_identifier =  $(this).attr('data-trigger-betrokkene_identifier');
        var trigger =  $(this).find('.sensitive-data-trigger');

        trigger.on('click', function () {
            $.get('/betrokkene/' + betrokkene_identifier + '/burgerservicenummer')
                .done(function(response) {
                    var bsn = response;

                    $('div[data-sensitive-data]').html(bsn);
                    trigger.hide();
                })
                .fail(function(response){
                    alert('Er is een fout opgetreden. Neem contact op met uw systeembeheerder.')
                });
        });
    });

    if($('#dialog .ezra_reschedule').length) {
        var reschedule_form = $('#dialog .ezra_reschedule');

        function reschedule_update(action, parameters) {
            reschedule_form.closest('.dialog-content').load(
                action,
                parameters,
                function() {
                    initializeEverything();
                }
            );
        }

        reschedule_form.find('.ezra_reschedule_delete').click( function() {
            reschedule_update($(this).attr('href'));
            return false;
        });

        reschedule_form.find('.ezra_reschedule_close').click( function() {
            reschedule_form.closest('#dialog').dialog('close');
            return false;
        });

        reschedule_form.submit( function() {
            reschedule_update(
                reschedule_form.attr('action'),
                reschedule_form.serialize()
            );

            return false;
        });
    }

//    if ($('.ezra_map').length) {
        load_ezra_map();
//    }


    if (
        $('.ezra_validate_next_phase').length &&
        !$('.ezra_validate_next_phase').hasClass('ezra_validate_next_phase-initialized')
    ) {
        $('.ezra_validate_next_phase').click( function() {
            var obj = $(this);

            var options = getOptions(obj.attr('rel'));
            var zaak_id = options.zaak_id;


            if(!zaak_id) {
                return; // TODO exception handling
            }

            $.ztWaitStart();

            $.getJSON(
                obj.attr('href') + '?nowrapper=1',
                function (data, textStatus) {
                    var result = data['result'][0];
                    if (result['success']) {
                        window.location = result['redirect'];
                    }
                }
            ).fail(function(response) {
                data = JSON.parse(response.responseText);
                result = data['result'][0];

                if (result.type === 'case/advance') {
                    ezra_dialog({
                        url         : obj.attr('href') + '_error?nowrapper=1',
                        cgi_params  : result.data[0],
                        title       : obj.attr('title'),
                        layout      : options['layout']
                    });
                } else {
                    // there is a number of TODO issues that come to mind here,
                    // i propose to implement the phase advancer in angularjs
                    // to circumvent all these. for now i will do a managable
                    // predictable albeit ugly fix.
                    var bodyScope = angular.element(document.body).scope();
                    if(bodyScope) {
                        bodyScope.$broadcast('systemMessage', {
                            content: result.messages[0],
                            type: 'error'
                        });
                    }
                    $.ztWaitStop();
                }
            });

            return false;

        });

        $('.ezra_validate_next_phase').addClass('ezra_validate_next_phase-initialized');
    }

    if($('input.ezra_regel_postcode').length) {

        var obj;
        var check_postcode = function(minlength) {
            if(!obj) {
                obj = $(this);
            }
            var value = obj.val();


            if(minlength && value.length < minlength) {
                obj.removeClass('error');
                return false;
            }

            if(!checkPostcode(value)) {
                obj.addClass('error');
            } else {
                obj.removeClass('error');
            }
        };

        var check_postcode_six = function() {
            obj = $(this);
            check_postcode(6);
        };

        $('input.ezra_regel_postcode').change( check_postcode );
        $('input.ezra_regel_postcode').keyup( check_postcode_six );
    }

    $('.clear_queue_ie').click(function() {
        var $button = $(this);
        var obj = $button.parents('.mintloader');
        var zaak_id = $('#zaak_id').attr('class');
        var upload_destination = obj.find('input[name="upload_destination"]').val();
        var url = zaak_id ?
            upload_destination + '/upload/remove_upload?kenmerk_id=' + obj.attr('id') :
            '/form/upload/remove_upload/?kenmerk_id=' + obj.attr('id');

        $.post(url, function (response) {
            var uploadResponse = obj.find('.uploaded_files_ie');
            uploadResponse.html(response);

            obj.find('.new_upload').empty();
            obj.find('.kiesbestand').attr('value', 'Kies bestand');
            obj.find('.clear_queue_ie').hide();
        });

        return false;
    });

    if($('.ezra_zaaktype_json').length) {

        $('form.ezra_zaaktype_json').submit( function() {
            var form = $(this);

            var serialized = form.serialize();
            serialized += '&json=1';

            $.getJSON(
                form.attr('action'),
                serialized,
                function() {
                    form.closest('#dialog').dialog('close');
                }
            );
            return false;
        });
    }

    if($('.ezra_captcha_reload').length) {

        $('.ezra_captcha_reload').click( function() {
            $('.ezra_captcha_image').attr('src', '/captcha?' + new Date().getTime())
            return false;
        });
    }


    if ($('.ezra_dagobert_editor').length && jQuery().wymeditor) {

        $('.ezra_dagobert_editor').each( function() {
            var obj = $(this);

            if(!obj.hasClass('ezra_dagobert_editor_initialized')) {
                installDagobertEditor(obj);
                obj.addClass('ezra_dagobert_editor_initialized');
            }

            obj.closest('form').find('input[type=submit]').each( function() {
                if(!$(this).hasClass('wymupdate')) {
                    $(this).addClass('wymupdate');
                }
            });
        });
    }




    if($('.ezra_publication_profile').length) {
        $('.ezra_publication_profile').change( function() {
            var form = $(this).closest('form').first();

            $('.ezra_publish_form_wrapper').load('/bulk/publish',
                form.serialize(),
                function() {
                    initializeEverything();
                }
            );
        });
    }

    if($('.ezra_publish_form_close').length) {
        $('.ezra_publish_form_close').submit( function() {
            $(this).closest('#dialog').dialog('close');
            return false;
        });
    }

    if($('.ezra_publish_form').length) {
        $('.ezra_publish_form').submit( function() {
            var form = $(this);
            if(form.hasClass('invalidation')) {
                return false;
            }
            form.addClass('invalidation');
            $('.ezra_publish_form .spinner-groot').css('visibility', 'visible');
            var serialized = form.serialize();
            serialized += '&commit=1';

            $('.ezra_publish_form_wrapper').load(
                form.attr('action'),
                serialized,
                function() {
                    initializeEverything();
                    form.removeClass('invalidation');
                }
            );
            return false;
        });
    }

    if($('input.ezra_change_allocation').length) {
        initAllocation();
        $('input.ezra_change_allocation').change( function() {
            initAllocation();
        });
    }


    if ($('div.element_tabel_auth').length) {
        ezra_search_opties();
    }

    $("table.sortable-element tbody").sortable(
        {
            items: 'tr',
            handle: '.drag',
            update: function(event,ui) {
                $("tr",this).each(
                    function( index, element ){
                        if(
                            !$(element).hasClass('ezra_table_row_template') &&
                            $(element).hasClass('ezra_table_row')
                        ) {
                            $(".roworder",this).val(index);
                        }
                    }
                );

                $(this).ezra_table('autosave', {
                    reload_milestones: 1
                });
            }
        }
    );

    (function ( ) {

        function setMailtypeValue ( ) {
            var value = $('#popup-send-mail[data-ezra]').find('[name="zaaktype_notificatie_id"]').val(),
                mailtype = $('#popup-send-mail[data-ezra]').find('input[name="mailtype"]');

            mailtype.val(value ? 'bibliotheek_notificatie' : 'specific_mail');

            if(!value) {
                $('#popup-send-mail[data-ezra] .specific_mail').show();
            } else {
                $('#popup-send-mail[data-ezra] .specific_mail').hide();
            }

        }

        if($('#popup-send-mail[data-ezra]').length) {

            $('#popup-send-mail[data-ezra] select[name="recipient_type"]').change(function() {
                if ($(this).attr('value') == 'medewerker') {
                    $('#popup-send-mail[data-ezra] .ezra_notificatie_rcpt_behandelaar').show();
                } else {
                    $('#popup-send-mail[data-ezra] .ezra_notificatie_rcpt_behandelaar').hide();
                }

                if ($(this).attr('value') == 'overig') {
                    $('#popup-send-mail[data-ezra] .ezra-email-recipient').show();
                } else {
                    $('#popup-send-mail[data-ezra] .ezra-email-recipient').hide();
                }

            });

            $('#popup-send-mail[data-ezra]').find('[name="zaaktype_notificatie_id"]').change(function ( ) {
                setMailtypeValue();
            });

            $('#popup-send-mail[data-ezra] .ezra-email-address-type').hide();

            $('.ezra-email-show').unbind('click').click(function ( ) {
                var type = $(this).data('address-type');
                $(this).hide();
                $('#popup-send-mail[data-ezra] .ezra-email-address-type-' + type).show();
            });

            setMailtypeValue();
        }


    })();

    // the first input field is a datepicker. it gets focus automatically, that's a little
    // undesirable so for this specific case the input loses focus.
    if($('.ezra_listactions_form input[name="registratiedatum"]').length) {
        $('.ezra_listactions_form input[name="registratiedatum"]').blur();
    }

    $('form.zvalidate').submit( function() {
        return zvalidate($(this));
    });

    if($('.ezra_zaakintake_link').length) {

        $('form.ezra_zaakintake_link').submit( function() {
            var form = $(this);
            var serialized = form.serialize();

            form.closest('.dialog-content').load(
                form.attr('action'),
                serialized,
                function() {
                    ezra_document_functions();
                    initializeEverything();
                }
            );
            return false;
        });
    }


    if($('.ezra_notificatie_preview').length) {
        $('.ezra_notificatie_preview').each( function() {
            var obj = $(this);

            obj.click(function() {
                ezra_dialog({
                    url     : obj.attr('href'),
                    title   : obj.attr('title'),
                    which   : '#thirddialog'
                });
                return false;
            });
        });
    }


    if($('.ezra_search_notificatie').length) {
        $('.ezra_search_notificatie').each( function() {
            var obj = $(this);

            // make this work on the text input field too
            var parent  = obj.closest('.ezra_bibliotheek_notificatie');
            var url     = parent.find('a.ezra_search_notificatie').attr('href');
            var title   = parent.find('a.ezra_search_notificatie').attr('title');

            obj.ezra_search_box({
                url     : url,
                title   : title,
                select_handler: function(selected_obj) {
                    var id      = selected_obj.attr('id').match(/notificatie_id-(\d+)/).pop();
                    var name    = selected_obj.find('td.notificatie_name').html();

                    parent.find('input[name="bibliotheek_notificaties_name"]').val(name);
                    parent.find('input[name="notificaties_bibliotheek_notificaties_id"]').val(id);
                }
            });
        });
    }


    if($('.ezra_zaaktype_add_zaak').length) {
        $('.ezra_zaaktype_add_zaak').each( function() {
            var obj = $(this);

            obj.ezra_search_box({
                url     : '/zaaktype/search',
                title   : 'Kies zaaktype',
                select_handler: function(selected_obj) {
                    var id = selected_obj.attr('id').match(/zaaktype_id-(\d+)/).pop();
                    var name = selected_obj.find('td').html();

                    var last_row = $('.element_tabel_relatie tr').last();
                    var existing = last_row.closest('table').find('input.ezra_table_row_edit_identifier[value="'+id+'"]');

                    if(existing.length) {
                        alert('Er is al een deelzaak met dit type. Elk type deelzaak kan maar een keer worden toegevoegd.');
                        last_row.closest('table').find('tr').last().remove();
                    } else {
                        last_row.find('.rownaam').html(name);
                        last_row.find('input.rowid').val(id);
                        last_row.find('.ezra_table_row_edit').click();
                    }
                }
            });
        });
    }




    if($('.add_search_filter #zaaktype.element').length) {
        $('.add_search_filter #zaaktype.element').each( function() {
            var obj = $(this);

            obj.ezra_search_box({
                url     : '/zaaktype/search',
                title   : 'Kies zaaktype',
                select_handler: function(selected_obj) {
                    var id = selected_obj.attr('id');
                    id = id.match(/zaaktype_id-(\d+)/).pop();

                    var data = 'action=update_filter&filter_type=zaaktype&nowrapper=1&value=' + id;
                    updateSearchFilters(data);
                },
                options : {
                    show_offline: 1
                }
            });
        });
    }



    if($('.status_definition_add_subzaak').length) {
        $('.status_definition_add_subzaak').each( function() {
            var obj = $(this);
            obj.ezra_search_box({
                url     : obj.attr('href'),
                title   : obj.attr('title'),
                select_handler: function(selected_obj) {
                    var id = selected_obj.attr('id');
                    id = id.match(/zaaktype_id-(\d+)/).pop();

                    var name = selected_obj.find('td').html();

                    var zt_row_id = clone_row2('#next_status_subzaak table', 'next_status_subzaak_subzaken');

                    var row_element = $('#' + zt_row_id);
                    row_element.find('input[name^="status_zaaktype_run"]').attr('checked', 'checked');
                    row_element.find('input[name^="status_zaaktype_open"]').attr('name', 'status_zaaktype_open');
                    row_element.find('.description').html(name);
                    row_element.find('input[name^="status_zaaktype_id"]').val(id);

                    ezra_basic_zaak_intake();
                }
            });
        });
    }

    initializeSelectZaaktype();


    if($('.ztAccordion').length) {
        $('.ztAccordion').accordion({
            heightStyle: 'content'
        });
    }

    initializeBetrokkeneSelect();

    if(scope_obj && scope_obj.find('.ezra_listactions').length) {
        enableListActions(scope_obj.find('.ezra_listactions'));
    }

    ezra_basic_functions();
    ezra_basic_zaak_functions();

    ezra_basic_widget_auth();


    $('.ezra_listactions_form').unbind('submit').submit( function() {
        var form = $(this);
        var selection = form.find('input[name="selection"]').val();
        var bodyScope = angular.element(document.body).scope();

        var execute_callback = function(obj) {
            var form = $('#dialog form.zvalidate');
            var url = form.attr('action');
            var serialized = form.serialize();

            if(bodyScope) {
                bodyScope.$apply(function ( ) {
                    bodyScope.$broadcast('zs.search.bulk.attempt');
                });
            }

            $.getJSON(
                url,
                serialized + '&commit=1',
                function(rawdata) {
                    if(rawdata.json.success) {
                        updateResults("zaken_results");
                    }
                    if(rawdata.json.redirect) {
                        window.location.href = rawdata.json.redirect;
                    } else if(rawdata.json.reload) {
                        window.location.reload();
                    }

                    var bodyScope = angular.element(document.body).scope();
                    if(bodyScope) {
                        bodyScope.$apply(function ( ) {
                            bodyScope.$broadcast('zs.search.bulk.success', rawdata.json.messages);
                        });
                    }

                }
            ).fail(function (response) {
                var result;
                try {
                    result = JSON.parse(response.responseText);
                } catch ( error ) {
                    result = [ {
                        messages: [
                            'Er ging iets fout bij het uitvoeren van deze actie. Probeer het later opnieuw.'
                        ]
                    }];
                }

                if (result.json && result.json.auth_error) {
                    bodyScope.$broadcast('systemMessage', {
                        content: "De nieuwe behandelaar is onbevoegd om deze zaak te behandelen.",
                        type: 'error'
                    });

                    $.ztWaitStop();
                } else if(bodyScope) {
                    bodyScope.$apply(function ( ) {
                        bodyScope.$broadcast('zs.search.bulk.error', result[0].messages);
                    });
                }
            });
            $('.ui-dialog-content').each( function() { $(this).dialog('close'); });
        }

        var confirmation_handler = function(obj) {
            obj.find('form').submit(function(){
                execute_callback(obj);
                return false;
            });
        };

        // for multiple items at once
        var submit_callback = function(container) {
            $.ztWaitStart();

            if(selection && (selection == 'selected_cases' || selection == 'search_results')) {
                ezra_dialog({
                    url: '/page/confirmation',
                    title: 'Bevestiging',
                    layout: 'small',
                    which: '#searchdialog',
                    cgi_params: {
                        message: 'U staat op het punt om wijzigingen door te voeren voor meerdere zaken. Dit kan niet meer ongedaan worden gemaakt. Weet u het zeker?'
                    }
                }, confirmation_handler);
            } else {
                execute_callback(form);
            }
        };

        return zvalidate($("#dialog form"), {
            callback: submit_callback
        });

    });

    $('.ezra_objectsearch_zaaktype').each(function() { initializeEzraZaaktypeSearch($(this)); });
    $('.ezra_objectsearch_case').each(function() { initializeEzraCaseSearch($(this)); });

    $('.ezra_objectsearch_zaaktype').each( function() {
        initializeEzraZaaktypeSearch($(this));
    });

    $('.ezra_objectsearch_product').each(function() {
        initializeEzraProductSearch($(this));
    });

    $('.ezra_objectsearch_kenmerk').each(function() {
        initializeEzraKenmerkSearch($(this));
    });

    $('.medewerkers').bind('addDeleteRow', function(event, options) {
        options['elem'].find('.ezra_medewerkers_delete_role').each(function() {
            if ($(this).hasClass('initialized')) {
                return true;
            }


            // $(this).closest('li')
            //     .mouseover(function() {
            //         $(this).find('.ezra_medewerkers_delete_role').css('visibility','visible');
            //     })
            //     .mouseout(function() {
            //         $(this).find('.ezra_medewerkers_delete_role').css('visibility','hidden');
            //     });

            $(this).click(function() {
                var role_dn = $(this).closest('li').find('input[name="entry_role_id"]').val();
                var entry_dn = $(this).closest('tr').find('input').val();
                var formbase = $(this).closest('form').attr('action');
                var role_row = $(this).closest('li');

                $.post(
                    formbase + '/delete_role_from',
                    {
                        role_dn: role_dn,
                        entry_dn: entry_dn
                    },
                    function(data) {
                        var result = data.json;

                        if (result.succes) {
                            role_row.remove();
                        }
                    },
                    'json'
                );

                return false;
            });

            $(this).addClass('initialized');
        });
    });

    $('.medewerkers .ezra_tree_table').not('.ezra_tree_table-initialized').each(function() {
        ezra_medewerker_load_triggers();
        $(this).find('.entry-group .ezra_tree_table_reload').click(function() {
            var clicked_elem = this;

            $('#ezra_medewerker_panel .spinner-groot').css('visibility','visible');

            $('#ezra_medewerker_panel').load(
                $(this).attr('href') + ' #ezra_medewerker_panel .panel_content',
                {
                    parent_identifier: $(this).closest('td').find('input').val()
                },
                function () {
                    ezra_medewerker_load_triggers(clicked_elem);
                }
            );

            return false;
        });

        $('.medewerkers').trigger('addDeleteRow',
            [
            {
                elem: $('.medewerkers .ezra_tree_table')
            }
            ]
        );

        $(this).treeTable(
            {
                initialState: "collapsed"
                //initialState: "expanded"
            }
        );

        // Configure draggable nodes
        $(".medewerkers .ezra_tree_table .entry-role-draggable").draggable({
          helper: "clone",
          opacity: .75,
          refreshPositions: true, // Performance?
          revert: "invalid",
          revertDuration: 300,
          scroll: true,
          appendTo: '.gebruikers'
        });

        $(".medewerkers .ezra_tree_table .entry-group").each(function() {
          $($(this).parents("tr")[0]).droppable({
            accept: ".entry-user-draggable",
            drop: function(e, ui) {
                formelem        = $(this).closest('form');
                var entry_dn    = $(ui.draggable).closest("div").find('input').val();
                var ou_dn       = $(this).find('input').val();
                var entry_draggable = $($(ui.draggable).closest("tr"));
                var entry_this  = this;


                $.getJSON(
                    formelem.attr('action') + '/move_to_ou',
                    {
                        ou_dn: ou_dn,
                        entry_dn: entry_dn
                    },
                    function(data) {
                        var result = data.json;

                        var bodyScope = angular.element(document.body).scope();

                        //$('.sysmessage').show();

                        //if (result.bericht) {
                        //    $('.sysmessage ul').html(
                        //        '<li>' + result.bericht + '</li>'
                        //    );
                        //}

                        if (result.succes) {
                            entry_draggable.remove();

                            if(bodyScope) {
                                bodyScope.$broadcast('systemMessage', {
                                    content: result.bericht,
                                    type: 'info'
                                });
                            }

                            update_inbox_number(formelem);
                        } else {
                            if(bodyScope) {
                                bodyScope.$broadcast('systemMessage', {
                                    content: result.bericht,
                                    type: 'error'
                                });
                            }
                        }
                    }
                );
            },
            hoverClass: "accept",
            over: function(e, ui) {
              if(this.id != $(ui.draggable.parents("tr")[0]).id && !$(this).is(".expanded")) {
                $(this).expand();
              }
            }
          });
        });


        // Make visible that a row is clicked
        $(".medewerker table.ezra_tree_table tbody tr").mousedown(function() {
          $("tr.selected").removeClass("selected"); // Deselect currently selected rows
          $(this).addClass("selected");
        });

        // Make sure row is selected when span is clicked
        $(".medewerker table.ezra_tree_table tbody tr span").mousedown(function() {
          $($(this).parents("tr")[0]).trigger("mousedown");
        });

        $(this).addClass('ezra_tree_table-initialized');
    });

    if ($('#ezra_nieuwe_zaak_tooltip')) {
        $('#ezra_nieuwe_zaak_tooltip')
            .unbind('nieuweZaakTooltip')
            .bind('nieuweZaakTooltip', function(options) {
                var obj = $(this);
                if (options['show'] && options['action']) {
                    if (options['action'].match(/\?/)) {
                        options['action'] += '&tooltip=1';
                    } else {
                        options['action'] += '?tooltip=1';
                    }

                    // scroll to top to prevent positioning issues with autocomplete box - Dario
                    $(document).scrollTop(0);

                    $('.custom_overlay').css('zIndex' , '3000').show();
                    $('.custom_overlay_loader').show();

                    /* Change button text */
                    if (
                        options['element'] &&
                        options['element'].attr('title')
                    ) {
                        $('.nieuwezaak_wrap a').html(
                            options['element'].attr('title')
                        );
                    }

                    /* Remove pulldown menu */
                    $('.nieuwe_zaak_extra_box').hide();

                    $('.ezra_nieuwe_zaak_tooltip-content').load(
                        options['action'],
                        function() {
                            initializeEverything(obj);
                            $('.custom_overlay_loader').hide();

                            $('#ezra_nieuwe_zaak_tooltip').show();
                            $('.nieuwezaak_wrap').addClass('active');
                        }
                    );

                }

                if (options['hide']) {
                    $('#ezra_nieuwe_zaak_tooltip').hide();

                    if (options['keeploader']) {
                        $('.custom_overlay_loader').show();
                    } else {
                        $('.custom_overlay').hide();
                    }
                    $('.nieuwezaak_wrap').removeClass('active');

                    $('.nieuwezaak_wrap a').html(
                        $('.nieuwezaak_wrap a').attr('title')
                    );
                }

                /* Because links can show up via ajax, we would like to be
                 * able to re-init these clicks
                 */
                $('.ezra_nieuwe_zaak_tooltip-show').unbind().click(function() {
                    $('#ezra_nieuwe_zaak_tooltip').trigger({
                        type: 'nieuweZaakTooltip',
                        show: 1,
                        action: $(this).attr('href')
                        }
                    );

                    return false;
                });
                $('.ezra_nieuwe_zaak_tooltip-hide').unbind().click(function() {
                    $('#ezra_nieuwe_zaak_tooltip').trigger({
                        type: 'nieuweZaakTooltip',
                        hide: 1
                        }
                    );
                });


                /* INITIALIZE THIS FUNCTIONALITY BELOW */

                /* Return when already initialized */
                if (
                    $('#ezra_nieuwe_zaak_tooltip')
                        .hasClass('ezra_nieuwe_zaak_tooltip-initialized')
                ) {
                    return true;
                }

                $('.ezra_nieuwe_zaak_tooltip-button').click(function() {
                    if ($(this).hasClass('active')) {
                        $('#ezra_nieuwe_zaak_tooltip').trigger({
                            type: 'nieuweZaakTooltip',
                            hide: 1
                            }
                        );
                    } else {
                        $('#ezra_nieuwe_zaak_tooltip').trigger({
                            type: 'nieuweZaakTooltip',
                            show: 1,
                            action: $(this).attr('href'),
                            element: $(this)
                            }
                        );
                    }

                    return false;
                });

                $(document).bind("click", function(event) {
                    var clicked = $(event.target);

                    if (
                        clicked.hasClass('custom_overlay')
                    ) {
                        $('#ezra_nieuwe_zaak_tooltip').trigger({
                            type: 'nieuweZaakTooltip',
                            hide: 1
                            }
                        );
                    }
                });

                $(document).keyup(function(e) {
                    if (e.keyCode == 27) {
                        $('#ezra_nieuwe_zaak_tooltip').trigger({
                            type: 'nieuweZaakTooltip',
                            hide: 1
                            }
                        );
                    }
                });


                $('#ezra_nieuwe_zaak_tooltip')
                    .addClass('ezra_nieuwe_zaak_tooltip-initialized');
            });

        $('#ezra_nieuwe_zaak_tooltip').trigger('nieuweZaakTooltip');
    }

    /* Dependency:
        Functions: post_dialog_by_ajax
    */
    if ($('#create_relatie').length) {
        /* Enable post dialog thru ajax, if ajax is not requested,
         * a return will be given and a normal post takes place
         */
        $('#create_relatie')
            .unbind('reloadSuggestion')
            .bind('reloadSuggestion', function() {
                if ($('.ezra_id_rol').val()) {
                    $('#create_relatie .ezra_id_magic_spinner')
                        .show();

                    $.ajax({
                        url: $('#create_relatie form').attr('action')
                            + '/suggestion',
                        data: {
                            rol: $('.ezra_id_rol').val(),
                            magic_string: $('.ezra_id_magic_string').val()
                        },
                        success: function(data) {
                            if (data == 'NOK') return false;
                            $('#create_relatie .ezra_id_magic_spinner').hide();

                            data = data.replace(/ /g,'_');
                            $('.ezra_id_magic_string').val(data);
                        }
                    });
                } else {
                    $('.ezra_id_magic_string').val('');
                }

                /* INITIALIZE THIS FUNCTIONALITY BELOW */

                /* Return when already initialized */
                if (
                    $('#create_relatie')
                        .hasClass('ezra_reloadsuggestion_initialized')
                ) {
                    return true;
                }

                $('#create_relatie').find('.ezra_id_magic_string, .ezra_id_rol').focus(
                    function() {
                        $('#create_relatie').trigger('reloadSuggestion');
                    }
                ).blur(
                    function() {
                        $('#create_relatie').trigger('reloadSuggestion');
                    }
                );

                $('#create_relatie')
                    .addClass('ezra_reloadsuggestion_initialized');

            });

        /* workaround for ZS-11423 */
        $('#create_relatie_other_role_input').val($('#create_relatie_role_set').val());

        $('#create_relatie').trigger('reloadSuggestion');

        $('#create_relatie_role_set').change(function() {
            var value = $(this).val();
            var role_input = $('#create_relatie_other_role_input');

            if(value == 'Anders') {
                role_input.val('');
                $('#create_relatie_other_role').show();
            } else {
                role_input.val(value);
                $('#create_relatie_other_role').hide();
            }

            $('#create_relatie').trigger('reloadSuggestion');
        });
    }

    (function ( ) {

        var originEl = $('[name="ztc_document_richting"]');

        function setVisibility ( ) {
            var origin = originEl.val(),
            isOriginDateVisible = [ 'Inkomend', 'Uitgaand' ].indexOf(origin) !== -1,
            dateEl = $('[name="ztc_document_datum"]').parents('tr');

            isOriginDateVisible ? dateEl.show() : dateEl.hide();
        }

        originEl
            .unbind('change')
            .bind('change', function ( ) {
                setVisibility();
            });

        setVisibility();

    })();

    if(scope_obj) {
        scope_obj.find('#search_results_accordion').accordion({heightStyle: 'content',collapsible: true});
        scope_obj.find('#search_results_accordion_all_results').accordion({heightStyle: 'content',collapsible: false, active: 0});
        scope_obj.find('#search_results_accordion_all_results').accordion('option', 'active', 1);
    } else {
        $('#search_results_accordion').accordion({heightStyle: 'content',collapsible: true, active: false});
        $('#search_results_accordion_all_results').accordion({heightStyle: 'content',collapsible: false, active: 0});
        $('#search_results_accordion_all_results').accordion('option', 'active', 1);
    }

    $('#ezra_betrokkene_zaken_accordion').accordion({heightStyle: 'content', collapsible: true});

    $('#search_results_accordion').bind('accordionchange', function(event, ui) {

        var oldContentID = ui.oldContent.attr('id');
        if(oldContentID) {
            var oldElement = $("#search_results_accordion #" + oldContentID + ' .zaken_filter_inner');
            oldElement.html('');
        }

        ui.newHeader.find('img').show();

        var newContentID = ui.newContent.attr('id');
        var form_selector = 'form[name=zaken_results]';

        var current_path = $(location).attr('pathname');

        var grouping_field = $('input[name=grouping_field]').val();
        var data = 'nowrapper=1&grouping_choice=' + newContentID + '&grouping_field=' + grouping_field;


        if(!newContentID || !grouping_field) {
            ui.newHeader.find('img').hide();
            return false;
        }

        $("#search_results_accordion #" + newContentID + ' .zaken_filter_wrapper').load(current_path + ' .zaken_filter_inner', data,
            function (responseText, textStatus, XMLHttpRequest) {
                if(textStatus == 'success') {
                    veldoptie_handling();
                    $(".progress-value").each(function() {
                        $(this).width( $(this).find('.perc').html() + "%");
                    });
                    var total_entries = $(form_selector + ' input[name=total_entries]').val();
                    if(total_entries) {
                        $(form_selector + ' span.total_entries').html(total_entries);
                    } else {
                        $(form_selector + ' span.total_entries').html('0');
                    }
                } else {
                    $(form_selector + ' .zaken_filter_inner').html('Er is iets misgegaan, laad de pagina opnieuw');
                }

                ui.newHeader.find('img').hide();
            });

        return false;

    });






    activateTakenAccordion();

    // animatie voortgang zaakdossier
    //
     $(".progress-value").each(function() {
         $(this).width( $(this).find('.perc').html() + "%");
    });


    $(".progress-time .current").each(function() {
        $(this).css('left', $(this).parents('.progress-time').find('.perc').html() +"%");
    });

    // end animatie voortgang zaakdossier




    $('select#keuzes').selectmenu({
        transferClasses: true,
        width: 130,
        style: 'dropdown',
        maxHeight: 120
    });

    load_selectmenu();
    load_selectmenu_medium();
    load_selectmenu_breedmenu();
    load_selectmenu_start_zaak();


    $('.menu-items').unbind().each(function() {
          var menuContent = $.cookie($(this).attr('id'));
          if (menuContent == "collapsed") {
              $("#" + $(this).attr('id')).hide();
              //$("#" + $(this).attr('id')).prev().children(".menuImgClose").addClass("menuIconOpen");
          }
      });

    $('select.replace-select-small').selectmenu({style:'dropdown',width:100,menuWidth:150});
    $('select.replace-select-small-popup').selectmenu({style:'popup',width:100});
    /* $('.form td:first-child').addClass('eerste') */;

    ezra_tooltip_handling();


    $.zaaknr = $('#zaak_id').attr('class');

    initialize_tabinterface();

    resizePanel();



    reload_aanvrager_wgts();


    reload_interne_aanvrager();



    /* Graph */
    $('#graph-img-line').each(function() {
        $('#graph-img-line .error').hide();
        graph   = $(this);
        href    = $(this).children('a:first');

        img     = new Image();
        $(img).load(function() {
            $(this).hide();
            $('#graph-img-line').removeClass('ajaxloader');
            $('#graph-img-line').append(this);
            $(this).fadeIn();
        }).error(function() {
            $('#graph-img-line').removeClass('ajaxloader');
            $('#graph-img-line .error').show();
        }).attr('src',href.attr('rel'));
    });

    $('#graph-img-pie').each(function() {
        $('#graph-img-pie .error').hide();
        graph   = $(this);
        href    = $(this).children('a:first');

        img     = new Image();
        $(img).load(function() {
            $(this).hide();
            $('#graph-img-pie').removeClass('ajaxloader');
            $('#graph-img-pie').append(this);
            $(this).fadeIn();
        }).error(function() {
            $('#graph-img-pie').removeClass('ajaxloader');
            $('#graph-img-pie .error').show();
        }).attr('src',href.attr('rel'));
    });

    trigger = $("#change_aanvragers").val();
    if (trigger) {
        show_aanvragers(trigger);
    }


    $('#change_aanvragers').change(function() {
        trigger = $("#change_aanvragers").val();
        show_aanvragers(trigger);
    });


    kenmerk_count = 0;
    function clone_kenmerk() {
        kenmerk = $('#kenmerk_template').clone();
        kenmerk.addClass('kenmerk_row');

        if (!kenmerk_count) {
            count = ($('.kenmerk_row').length + 1);
            kenmerk_count = count;
        } else {
            count = (kenmerk_count + 1);
            kenmerk_count = count;
        }

        kenmerk.attr('id', 'kenmerk_row_' + count);

        kenmerk.find('input').each(function() {
            $(this).attr('name', $(this).attr('name') + '_' + count);
        });
        kenmerk.find('select').each(function() {
            $(this).attr('name', $(this).attr('name') + '_' + count);
        });
        kenmerk.find('textarea').each(function() {
            $(this).attr('name', $(this).attr('name') + '_' + count);
        });
        kenmerk.find('.kenmerk_title').html('Kenmerk ' + count);

        kenmerk.find('.kenmerk_del').attr('id', 'kenmerk_del_' + count);
        kenmerk.show();

        $('#zaaktype_kenmerk_add').before(kenmerk);

        $('.kenmerk_del').click(function() {
            /* Get count */
            currentid = $(this).attr('id');
            currentcount = currentid.replace(/^kenmerk_del_/g, '');

            $('#kenmerk_row_' + currentcount).remove();
            return false;
        });

    }

    /* zaaktype/status hide everything */
    $('#status_row_template').hide();
    $('#status_row_definitions_template').hide();
    $('.status_row_definitions').hide();

    /* Help out by giving the first status */

    if ($('#status_row_template').length) {
        /* Magic, check for by De Don generated rows, and use this as a start */
        /* Count rows, and substract row_template + header + 1(so substract 1) */
        if ($('#status_rows').find('.status_name_row').length < 2) {
            add_status_row();
            add_status_row('afgehandeld');
        }

        /* Do not show status_row_add in first instance */
        /* $('#status_row_add').hide(); */

        $('#status_row_add').click(function() {
            add_status_row();

            return false;
        });

        $('.validate-ok').hide();

        /*
        $('input[name^="status_naam_"]').change(function() {
            closest = $(this).closest('tr');
            if ($(this).val()) {
                closest.find('.status_define').show();
            } else {
                closest.find('.status_define').hide();
            }
        });
        */

        init_status_actions();
    }

    $(".kies_zaaktype").click(function() {
        /* Get container */
        alert('kies_zaaktype');
        return false;
/*        var container = $(this).closest('div');

        var zt_row_id = container.attr('id');

        var zt_trigger = container.find('input[name="jstrigger"]').val();

        // Form container
        var form_container = $(this).closest('form');
        var zt_betrokkene_type = form_container.find('input[name="betrokkene_type"]:checked').val();

        var eoptions = new Array();

        eoptions['zt_trigger'] = zt_trigger;

        eoptions['zt_betrokkene_type'] = zt_betrokkene_type;

        select_zaaktype('#' + zt_row_id + ' input[name="zaaktype"]', '#' + zt_row_id + ' .zaaktype_keuze_description', null, eoptions);

        return false;
    */
    });

// Hoofd- en deelzaken
//
// Functies voor hoofd/deelzaken
//

    if ($('#hoofddeelzaken_table').length) {
        $('#hoofddeelzaken_table').treeTable(
            {
                // initialState: "collapsed"
                initialState: "expanded"
            }
        );
    }

    if ($('#spec_zaakinformatie_container').length) {
        dynamic_rows('#spec_zaakinformatie_container', 'spec_zaakinfo', 'input[name^="kenmerk_naam"]', 'load_zaakinformatie_kenmerken();');
    }

    if ($('#rol_kenmerken_container').length) {
        dynamic_rows('#rol_kenmerken_container', 'auth_roles', 'input[name^="role_group_id"]',null,'ezra_basic_zaaktype_functions();');
    }


    $('.change_betrokkene_type').change(function() {
        currentform = $(this).closest('form');

        currentform.find('a.search_betrokkene').attr('rel', $(this).val());
        currentform.find('#new_externe_aanvrager input[type="hidden"]').val(null);
        currentform.find('#new_externe_aanvrager input[type="text"]').val(null);

        currentform.find('#zaaktype_keuze_container_extern input[type="hidden"]').val(null);
        currentform.find('#zaaktype_keuze_container_extern .zaaktype_keuze_description').html(null);
    });

    $('table tbody tr:last-child').addClass('lastrow');





    $('.mintloader').mintloader({dragndrop: 1});


    // for webform/zaak kenmerken context - we want iframe behaviour from webform.js
    if(!$('.webform').length) {
        // For IE
        var hasXhr2 = window.XMLHttpRequest && ('upload' in new XMLHttpRequest());
        if(!hasXhr2) {
            $('.mintloader input:file').unbind('change').change( function() {
                submitFileUpload();
            });
        }
    }

    $('.ezra_objectsearch_betrokkene').each( function() {
        var obj = $(this);
        initializeEzraBetrokkeneSearch(obj);
    });

    if($('#login input[name=username]').length) {
        $('#login input[name=username]').focus();
    }

    if($('#import_configuration').length) {
        attachImportFormHandlers();
    }

    if($('#betrokkene_definitie').length) {
        var def = $('#betrokkene_definitie');

        def.find('select[name="standaard_betrokkenen_rol"]').change(function () {
            var role = $(this).val();

            if (role == '') {
                return;
            }

            var magic_field = def.find('input[name="standaard_betrokkenen_magic_string_prefix"]');
            var url = "/beheer/zaaktypen/magic_string";
            var data = { rol: role };

            $.ajax(url, {
                data: data,
                dataType: 'text',
            }).done(function (data) {
                magic_field.val(data.replace('_', ''));
            }).fail(function () {
                magic_field.val(role.toLowercase().replace(' ', ''));
            });
        });

        def.find('input, textarea, select').change(function () {
            initialize_milestone_zaaktype_selector();
        });
    }


    ezra_basic_zaaktype_functions();

    initialiseQuillEditors();

    $.ajax({
        url: '/api/v1/session/current'
    })
    .done(function( data ) {

        _.includes(data.result.instance.active_interfaces, 'overheidio_bag') ? initialiseAddressAutoComplete() : false;

    });

    if(scope_obj instanceof jQuery) {
        var bodyScope = angular.element(document.body).scope();
        if(bodyScope) {
            bodyScope.$root.$emit('legacyDomLoad', scope_obj);
        }
    }
}

// only run this function once, otherwise the accordion will collapse again.
// scenarion: ajax reload of a page, then a master initialize of the page.
var taken_accordion_active = 0;
function activateTakenAccordion() {
    if(taken_accordion_active) {
        return;
    }

    $('#accordion-taken').each(function() {
        var active  = 0;
        if ($('#accordion-taken .ui-accordion-content.ui-accordion-taken-kenmerken').length) {
            active = $('#accordion-taken .ui-accordion-content')
                .index('.ui-accordion-taken-kenmerken');
        }

        $(this).accordion({
            heightStyle: 'content',
            collapsible: true,
            active: active
        });
    });
    taken_accordion_active = 1;
}


function updateBetrokkeneSearch(obj) {

    var my_form = obj.closest('form');
    var serialized = my_form.serialize();
    var action = "/search/betrokkene";
    $('.spinner-groot').css('visibility', 'visible');


    $('.betrokkene_search_results_wrapper').load(action + ' .betrokkene_search_results_inner',
        serialized,
        function(responseText, textStatus, XMLHttpRequest) {
            initializeEverything();
            ezra_tooltip_handling();
            ezra_basic_functions();
        }
    );

    return false;
}

function ezra_medewerker_load_triggers(clicked_elem) {
    $('#ezra_medewerker_panel .spinner-groot').css('visibility','hidden');

    //$('.medewerkers .ezra_tree_table')
    $(".medewerkers .ezra_tree_table .entry-user-draggable").draggable({
      helper: "clone",
      opacity: .75,
      refreshPositions: true, // Performance?
      revert: "invalid",
      revertDuration: 300,
      //scroll: true,
      appendTo: '.gebruikers'
    });

    $('.medewerkers .ou-active').removeClass(
        'ou-active'
    );

    if (clicked_elem) {
        $(clicked_elem).closest('tr').addClass('ou-active');
    }

    $('.medewerkers').trigger('addDeleteRow',
        [
        {
            elem: $('.medewerkers .ezra_tree_table')
        }
        ]
    );


    $(".ezra_entryrol .accept_ou_group").click(function() {
        formelem        = $(this).closest('form');
        var options = getOptions($(this).attr('rel'));
        var row     = $(this).closest('tr');
        var auth_scope = $(this).parent().find('select');

        var entry_dn    = row.find('input').val();
        var ou_dn       = options['group_identifier'];

        $.getJSON(
            formelem.attr('action') + '/move_to_ou',
            {
                ou_dn: ou_dn,
                entry_dn: entry_dn,
                authorization_scope: auth_scope.val()
            },
            function(data) {
                var result = data.json;

                var bodyScope = angular.element(document.body).scope();

                if (result.succes) {
                    if(bodyScope) {
                        bodyScope.$broadcast('systemMessage', {
                            content: htmlEscape(result.bericht),
                            type: 'info'
                        });
                    }

                    row.remove();

                    update_inbox_number(formelem);


                } else {
                    if(bodyScope) {
                        bodyScope.$broadcast('systemMessage', {
                            content: htmlEscape(result.bericht),
                            type: 'error'
                        });
                    }
                }
            }
        );

        return false;

    });

    $(".medewerkers .ezra_tree_table .entry-user").each(function() {
      $($(this).parents("tr")[0]).droppable({
        accept: ".entry-role-draggable",
        drop: function(e, ui) {
            formelem        = $(this).closest('form');
            var role_dn     = $(ui.draggable).closest("div").find('input').val();
            var entry_dn    = $(this).find('input').val();
            var entry_rol   = $(this).find('.ezra_entryrol');

            $.getJSON(
                formelem.attr('action') + '/add_role_to',
                {
                    role_dn: role_dn,
                    entry_dn: entry_dn
                },
                function(data) {
                    var result = data.json;
                    //$('.sysmessage').show();

                    //if (result.bericht) {
                    //    $('.sysmessage ul').html(
                    //        '<li>' + result.bericht + '</li>'
                    //    );
                    //}
                    var bodyScope = angular.element(document.body).scope();

                    if (result.succes) {
                        var delhref='<a href="#" class="ezra_medewerkers_delete_role'
                            + ' row-actions right"><i class="icon-font-awesome icon-remove"></i></a>';
                        entry_rol.append('<li> <span class="entry_role">' + htmlEscape(result.role_name)
                            + '</span> ' + delhref + ' </li>');

                        if(bodyScope) {
                            bodyScope.$broadcast('systemMessage', {
                                content: htmlEscape(result.bericht),
                                type: 'info'
                            });
                        }
                    } else {
                        if(bodyScope) {
                            bodyScope.$broadcast('systemMessage', {
                                content: htmlEscape(result.bericht),
                                type: 'error'
                            });
                        }
                    }

                    $('.medewerkers').trigger('addDeleteRow',
                        [
                        {
                            elem: $('.medewerkers .ezra_tree_table')
                        }
                        ]
                    );
                }
            );

        },
        hoverClass: "accept",
        over: function(e, ui) {
          if(this.id != $(ui.draggable.parents("tr")[0]).id && !$(this).is(".expanded")) {
            $(this).expand();
          }
        }
      });
    });
}

function update_inbox_number(formelem) {
    var user_panel  = formelem.find('#ezra_medewerker_panel .ezra_tree_table');

    var nr_in_inbox = user_panel.find('tr').length;
    if (nr_in_inbox > 0) {
        formelem.find('.inbox_group').addClass('has_entries');
    } else {
        formelem.find('.inbox_group').removeClass('has_entries');
    }

    formelem.find('.inbox_counter').html(nr_in_inbox);
}




function initialize_tabinterface() {

    $('#tabinterface .ui-tabs-hide').removeClass('ui-tabs-hide');

    $('#tabinterface').tabs({
         ajaxOptions: {
            success: function( xhr, status ) {
                load_ezra_map();
            }
        },
        show: function(event, ui) {
            var zaak_id,
                scope,
                ui_panel_id = ui.panel.id,
                panel = $('#' + ui_panel_id);


            // trigger the loading of documents only on the first time
            // when the tabs is opened
            function loadElement() {
                var scope = angular.element(panel[0]).scope();
                scope.$apply(function () {
                    scope.elementRequested = true;
                });
            }

            if (ui_panel_id === 'zaak-elements-deelzaken') {
                var scope = angular.element(panel[0]).scope();

                // only load the first time
                if (!scope.elementRequested) {
                    panel.find('.spinner-groot').css('visibility', 'visible');
                    zaak_id = $('#zaak_id').attr('class');
                    $.get('/zaak/' + zaak_id + '/deelzaken', function (response) {
                        panel.find('.deelzaken_inner_html_content').html(response);
                        initializeEverything(panel);
                        panel.find('.spinner-groot').css('visibility', 'hidden');
                    });
                }
                loadElement();
            } else if (ui_panel_id === 'zaak-elements-timeline') {
                loadElement();
            }

            if(
                !panel.hasClass('ezra_load_zaak_element') ||
                panel.hasClass('ezra_load_zaak_element_loaded')
            ) {
                resizePanel();
                return;
            }


            var match = panel.attr('class').match(/zaak_nr_(\d+)/);
            var zaaknr = match[1];

            var element_url = '/zaak/' + zaaknr + '/view_element/' + ui_panel_id;

            if (panel.hasClass('pip')) {
                element_url = '/pip' + element_url;
            }


            if(ui_panel_id.match(/zaak-elements-fase-(\d+)/)) {
                var form = panel.find('form.webform');
                loadWebform({
                    form: form
                });

            }

            panel.addClass('ezra_load_zaak_element_loaded');
        }
    });

    // when you click a woz object item in a list, the page refreshes,
    // and the WOZ tab is not selected anymore. this re-selects the WOZ tab
    // this is definitely not a pretty to do this, but we are
    // replacing the GUI by angular code. the correct way would be
    // to reload the content for the selected tab using JSON data some
    // client-side template.
    if (document.location.href.match(/betrokkene.*woz_id=\d+/)) {
        $('#tabinterface').tabs("option", "active", 3);
    }

    $('#tabinterface-1').tabs('option', 'spinner', '');

    // ugly hack to open WOZ tab when clicking through. Please remove asap,
    // correct implementation depends on Angular 1.2 which should come out
    // Q4 2013.
    // This causes the 4rd (3-zero indexed) tab to be opened
    var queryRegex = /woz_id=(\d*)/g,
        queryResult = queryRegex.exec(location.search);

    if(queryResult) {
        $('#tabinterface').tabs('option', 'active', 3);
    }
}






function ezra_tooltip_handling() {
    $(".tooltip-test-wrap").hide();

    // no guidelines: see beheer/zaaktypes/relaties/edit.tt
    $('.form td').each(function() {
        var obj = $(this);
        if(!obj.hasClass('noguidelines')) {
            obj.addClass('guideline-hover');
        }
    });
    // Here, because when you go to a next or previous button, we don't want
    // to have a tooltip covering the button.
    // This is placed here for the 'nieuwe zaak popup', button 'volgende'
    $('.form-actions').hover(function() {
        $(".tooltip-test-wrap").hide();
    });


    $(".form tr").children(".guideline-hover").hover(
      function () {
        $(this).parent('tr').find('td').children(".tooltip-test-wrap").show();
      },
      function () {
        $(".tooltip-test-wrap").hide();
      }
    );


    $(".form tr").children(".guideline-hover").click( function () {
        $(".tooltip-test-wrap").hide()
        $(this).parent('tr').find('td').children(".tooltip-test-wrap").show();
    });


    $(".form tr").find('td').children(".tooltip-test-wrap").hover(
      function () {
        $(this).show();
      }
    );
}



/*
19-6-2012 obsolete? no references found. keep an eye out for eval(string) constructs.
function oldstyle_document_row_init() {
    $('#document_rows .document_row').each(function() {
        $(this).find('.document_row_del').click(function() {
            var thisrow = $(this).closest('tr');
            thisrow.remove();
            return false;
        });

        $(this).find('.add_kenmerken').click(function() {
            var thisrow = $(this).closest('tr');

            var questionname = thisrow.find('input[name^="document_description"]');
            $(this).attr('rel', 'destination: ' + questionname.attr('name'));

            fireDialog($(this), 'load_document_kenmerken();');
            return false;
        });
    });
}
*/


function load_zaakinformatie_kenmerken() {
    $('#kenmerk_definitie select[name="kenmerk_type"]').change(function() {
        if ($(this).find(':selected').hasClass('has_options')) {
            $('#kenmerk_definitie .multiple-options').show();
        } else {
            $('#kenmerk_definitie .multiple-options').hide();
        }

        if ($(this).find(':selected').hasClass('allow_default_value')) {
            $('#kenmerk_definitie .default-value').show();
        } else {
            $('#kenmerk_definitie .default-value').hide();
        }

        if ($(this).find(':selected').hasClass('file')) {
            $('#kenmerk_definitie .file-options').show();
        } else {
            $('#kenmerk_definitie .file-options').hide();
        }
    });

    $('#kenmerk_definitie select[name="kenmerk_type"]').change();

    $('#kenmerk_definitie select[name="kenmerk_document_key"]').change(function() {
        $('#kenmerk_definitie .file-options-unknown').show();
        $('#kenmerk_definitie .file-options-known').hide();
        if ($(this).find(':selected').val()) {
            $('#kenmerk_definitie .file-options-unknown').hide();
            $('#kenmerk_definitie .file-options-known').show();
            $('#kenmerk_definitie .mandatory_document').hide();
            $('#kenmerk_definitie .no_mandatory_document').hide();

            if ($(this).find(':selected').hasClass('mandatory')) {
                $('#kenmerk_definitie .mandatory_document').show();
            } else {
                $('#kenmerk_definitie .no_mandatory_document').show();
            }
        }
    });
    $('#kenmerk_definitie select[name="kenmerk_document_key"]').change();

    $('#kenmerk_definitie input[type="button"]').click(function() {
        /* Harvest results */
        var postdata = {};
        $('#kenmerk_definitie select').each(function() {
            postdata[$(this).attr('name')] = $(this).val();
        });
        $('#kenmerk_definitie input').each(function() {
            if (
                $(this).attr('type') == 'checkbox' ||
                $(this).attr('type') == 'radio'
            ) {
                if ($(this).attr('checked') == true) {
                    postdata[$(this).attr('name')] = $(this).attr('value');
                }
                return;
            }
            postdata[$(this).attr('name')] = $(this).val();
        });
        $('#kenmerk_definitie textarea').each(function() {
            postdata[$(this).attr('name')] = $(this).val();
        });

        $.post(
            '/zaaktype/specifiek/kenmerk_definitie',
            postdata,
            function() {
                $('#dialog').dialog('close');
            }
        );
    });
}

function init_dynamic_row_functions(container_id,edit,editcallback) {
    if (edit) {
        $(container_id + ' .edit').click(function() {
            var thisrow = $(this).closest('tr');

            var questionname = thisrow.find(edit);

            $(this).attr('rel', 'destination: ' + questionname.attr('name'));
alert('init_dynamic_row_functions');
            fireDialog($(this), editcallback);
            return false;
        });
    }

    $(container_id + ' .del').click(function() {
        var thisrow = $(this).closest('tr');
        thisrow.remove();
        return false;
    });
}

function dynamic_rows(container_id, name, edit, editcallback,rowcallback) {
    /* Hide */
    $(container_id + ' .dynamic_rows .row_template').hide();

    /* Make 'add' button work */
    $(container_id + ' .add_row').each(function() {
        if ($(this).data('events')) { return; }

        $(this).click(function() {
            var dynrowid = clone_row2(container_id + ' .dynamic_rows', name);

            init_dynamic_row_functions(container_id,edit, editcallback);

            if (rowcallback) {
                eval(rowcallback);
            }
            return false;
        });
    });

    init_dynamic_row_functions(container_id,edit, editcallback);
}


function load_subzaken_kenmerken() {
    $('#zaaktype_definitie form').submit(function() {
        /* Harvest results */
        var postdata = $(this).serialize();

        var formuri = $('input[name="zaaknr"]').val();

        $.post(
            '/zaak/' + formuri + '/status/next/zaaktype_kenmerken',
            postdata,
            function() {
                $('#dialog').dialog('close');
            }
        );

        return false;
    });
}

function load_document_kenmerken() {
    $('#document_definitie input[type="button"]').click(function() {
        /* Harvest results */
        var postdata = {};
        $('#document_definitie select').each(function() {
            postdata[$(this).attr('name')] = $(this).val();
        });
        $('#document_definitie input').each(function() {
            if (
                $(this).attr('type') == 'checkbox' ||
                $(this).attr('type') == 'radio'
            ) {
                if ($(this).attr('checked') == true) {
                    postdata[$(this).attr('name')] = $(this).attr('value');
                }
                return;
            }
            postdata[$(this).attr('name')] = $(this).val();
        });
        $('#document_definitie textarea').each(function() {
            postdata[$(this).attr('name')] = $(this).val();
        });

        $.post(
            '/zaaktype/algemeen/doc_definitie',
            postdata,
            function() {
                $('#dialog').dialog('close');
            }
        );
    });
}

function init_status_actions() {
    /* Make edit button work */
    $('.status_define').each(function() {
        if ($(this).data('events')) { return ; }

        $(this).click(function() {
            /* Get id */
            var closestid = $(this).closest('tr').attr('id');
            var rownr     = closestid.replace(/status_row_/g, '');

            $('#status_row_definitions_' + rownr).toggle();

            return false;
        });
    });

    /* Make edit button work */
    $('.status_name_row .del').each(function() {
        if ($(this).data('events')) { return ; }

        $(this).click(function() {
            /* Get id */
            var closesttr = $(this).closest('tr');
            var closestid = closesttr.attr('id');
            var rownr     = closestid.replace(/status_row_/g, '');

            $('#status_row_definitions_' + rownr).remove();
            closesttr.remove();

            return false;
        });
    });

    /* Reshake statusses */
    var num_statusses = 0;
    $('#status_rows').find('.status_count').each(function() {
        var closestid = $(this).closest('tr').attr('id');
        var rownr     = closestid.replace(/status_row_/g, '');
        $(this).html('<input type="hidden" name="status_nr_' + rownr + '" value="' + num_statusses + '" /><p>' + num_statusses + '</p>');
        num_statusses = num_statusses + 1;
    });

    /* Make sure we can save this definition */
    $('.status_row_definitions input[type="button"]').each(function() {
        if ($(this).data('events')) { return ; }

        $(this).click(function() {
            /* Do some saving here */
            $(this).closest('tr').hide();

            /* Validate row */
            /* $(this).closest('tr').find('.validate-ok').show(); */
            var defrowid = $(this).closest('tr').attr('id');
            var parentrowid     = defrowid.replace(/_definition/g, '');
            parentrowid     = parentrowid.replace(/rows/g, 'row');

            $('#' + parentrowid).find('.validate-ok').show();

            /* add_status_row(); */

            return false;
        });
    });

    $('.status_row_definitions').each(function() {
        var defid = $(this).closest('tr').attr('id');
        var currentrowid = $(this);
        /* defid     = defid.replace(/status_row_definitions_/g, ''); */

        dynamic_rows(
            '#' + defid + ' .checklist_container',
            'checklist',
            'input[name^="status_checklist_vraag"]',
            'load_checklist_antwoorden();'
        );

        // Make 'antwoorden' work
        dynamic_rows('#' + defid + ' .document_container','documenten', 'input[name^="status_document_name"]', 'load_document_kenmerken();');

        // Subzaken
        $('#' + defid + ' .subzaken .row_template').hide();

        // Make 'add' button work
        if (!$('#' + defid + ' .status_definition_add_subzaak').data('events')) {


            $('#' + defid + ' .status_definition_add_subzaak').unbind().click(function() {

                /* Clone row */
                var zt_row_id = clone_row2('#' + defid + ' .subzaken', defid + '_subzaken');

                /* Launch our zaaktype selection window */
                select_zaaktype('#' + zt_row_id + ' input[name^="status_zaaktype_id"]', '#' + zt_row_id + ' .description', '#' + zt_row_id);

                init_status_actions();

                return false;

            });
        }

        $('#' + defid + ' .subzaken .edit').click(function() {
            var thisrow = $(this).closest('tr');

            var questionname = thisrow.find('input[name^="status_zaaktype_id"]');

            $(this).attr('rel', 'destination: ' + questionname.attr('name'));

alert('subzaken edit');
            fireDialog($(this), 'load_zaaktype_kenmerken();');
            return false;
        });

        $('.subzaken .document_row_del').click(function() {
            /* Get count */
            var parentrow = $(this).closest('tr');

            parentrow.remove();

            return false;
        });


        dynamic_rows('#' + defid + ' .resultaatmogelijkheden_container','resultaten');

        /* Make sure we do not show afhandelstatus ;) */
        if ($('#' + defid  + ' [name^="status_type"]').length) {
            var statustype = $('#' + defid  + ' input[name^="status_type"]');
            if (statustype.val() == 'afhandelen') {
                /* Disable status_afhandel_block */
                $('#' + defid + ' .status_behandel_block').hide();
                $('#' + defid + ' .status_afhandel_block').show();
                $('#' + defid + ' .deelofsubs').html('vervolgzaken');
                $('#' + defid + ' .deelofsub').html('vervolgzaak');
            } else {
                $('#' + defid + ' .status_afhandel_block').hide();
                $('#' + defid + ' .status_behandel_block').show();
            }
        }


    });

    if ($('.status_count').length && ! cloned_rows['status_row']) {
        var numtrrows = $('.status_name_row').length;
        cloned_rows['status_row'] = numtrrows;
        numtrrows = $('.status_row_definitions').length;
        cloned_rows['status_row_definitions'] = numtrrows;
    }

    /* Make sure we define firststatus class */
    $('input[name="status_nr_1"]').each(function() {
        // Find container row
        var row_container   = $(this).closest('tr');

        // Get id from container
        var container_id    = row_container.attr('id');

        container_id        = container_id.replace(/.*(\d+)/, '$1');

        // Get row_definitie
        table_container     = row_container.closest('table');

        definitie           = table_container.find('#status_row_definitions_' + container_id);

        definitie.find('.firststatus').css('display', 'inline');
        definitie.find('.nextstatus').css('display', 'none');
    });

    $('.ztAjaxTable').ztAjaxTable();

}

var status_count = 1;
function add_status_row(type) {
    /* Clone template row */
    var newid    = clone_row('status_row');
    var newdefid = clone_row('status_row_definitions');

    $('#' + newdefid).hide();

    status_count = status_count + 1;
    /* Now make sure everything works in this definition */
    load_definition(newdefid);

    /* Make this status 'afgehandeld' */
    if (type == 'afgehandeld') {
        var oldinput = $('#' + newid).find('input[name^="status_naam"]');

        /* Some decorations */
        $('#' + newid).find('.input-naam').append('<p>Afgehandeld</p>');
        $('#' + newid).find('.input-naam')
            .append('<input type="hidden" name="' + oldinput.attr('name')
                        + '" value="Afgehandeld" />');

        /* Some class working */
        $('#' + newid).addClass('afgehandeld');

        /* And for future reference, drop the select box for choosing
         * type, and create a hidden containing this type */
        $('#' + newdefid).find('input[name^="status_type"]').val('afhandelen');

        oldinput.remove();
    }

    init_status_actions();
}

var cloned_rows2 = [];

//
// TODO this should be moved into a component. Error checking must be added.
//
function clone_row2(elemname, name, append) {

    // Clone
    clone  = $(elemname + ' .row_template').clone();
    parent_elem =  $(elemname + ' .row_template').parent();

    if (cloned_rows2[name]) {
        cloned_rows2[name] = cloned_rows2[name] + 1;
    } else {
        /* Magic, check for by Le Don generated rows, and use this as a start */
        /* Count rows, and substract row_template + header + 1(so substract 1) */
        var numtrrows = parent_elem.find('tr').length;
        cloned_rows2[name] = (numtrrows - 1);
    }

    // the new row must have a unique number
    count = cloned_rows2[name] + 1;

    // Change every input/select/textarea and append number
    formelements = Array('input','select','textarea','checkbox');
    for (var i = 0; i < formelements.length; i++) {
        clone.find(formelements[i]).each(function() {
            $(this).attr('name',
                $(this).attr('name') + '_' + count
            );
        });
    }

    // Change this id
    var newid     = name + '_' + count;

    clone.attr('id', newid);

    // Remove template class from clone
    clone.removeClass('row_template');

    // Make sure the delete button is gonna work
    clone.find('.del').click(function() {
        /* Get count */
        var parentrow = $(this).closest('tr');

        parentrow.remove();

        return false;
    });

    /* Append the clone */
    parent_elem.append(clone);

    /* Show the clone */
    clone.show();

    return newid;
}

function load_definition(defid) {

    return;
    /* Checklist */

    /* Hide */
    $('#' + defid + ' .checklist .row_template').hide();

    /* Make 'add' button work */
    $('#' + defid + ' .status_definition_add_vraag').click(function() {
        var vraagid = clone_row2('#' + defid + ' .checklist', defid + '_checklist');

        $('#' + vraagid + ' .add_antwoorden').click(function() {
            var thisrow = $(this).closest('tr');

            var questionname = thisrow.find('input[name^="status_checklist_vraag"]');
            $(this).attr('rel', 'destination: ' + questionname.attr('name'));
            $(this).attr('title',
                thisrow.find('input[name^="checklist_vraag"]').val()
            );

alert('Opening dialog 1');
            fireDialog($(this), 'load_checklist_antwoorden();');
            return false;
        });

        return false;
    });


    /* Subzaken */
    $('#' + defid + ' .subzaken .row_template').hide();


    /* Make 'add' button work */
    $('#' + defid + ' .status_definition_add_subzaak').click(function() {
        /* Clone row */
        var zt_row_id = clone_row2('#' + defid + ' .subzaken', defid + '_subzaken');

        /* Launch our zaaktype selection window */
        select_zaaktype('#' + zt_row_id + ' input[name^="status_zaaktype_id"]', '#' + zt_row_id + ' .description', '#' + zt_row_id);

        return false;
    });

    dynamic_rows('#' + defid + ' .resultaatmogelijkheden_container','resultaten');
}


//
// Show a dialog to search a zaaktype. This is
//
//
function select_zaaktype(dest_id, dest_descr, dest_row, eoptions) {
alert('illegal call to select_zaaktype');
return;
    if (dest_row && dest_id) {
        $('#searchdialog').dialog(
            'option',
            'beforeclose',
            function() {
                if (! $(dest_id).val()) {
                    $(dest_row).remove();
                }

                $('#searchdialog').dialog(
                    'option',
                    'beforeclose',
                    function() { }
                );
            }
        );
    }

    if (!eoptions) {
        eoptions = new Array;
    }

    ezra_dialog({
        url: '/zaaktype/search',
        cgi_params: {
            jsfillid: dest_id,
            jsfilldescr: dest_descr,
            jstrigger: eoptions['zt_trigger'],
            jsbetrokkene_type: eoptions['zt_betrokkene_type']
        },
        which : '#searchdialog',
        title : 'Zoek zaaktype'
    }, function() {

        $('#accordion').accordion({
            heightStyle: 'content'
        });

        load_zaaktype();
    });
}



function load_checklist_antwoorden() {
    $('#antwoord_invoertype').change(function() {
        if ($(this).find(':selected').hasClass('has_options')) {
            $('#antwoord_mogelijkheden').show();
        } else {
            $('#antwoord_mogelijkheden').hide();
        }
    });


    if ($('#antwoord_invoertype').find(':selected').hasClass('has_options')) {
        $('#antwoord_mogelijkheden').show();
    } else {
        $('#antwoord_mogelijkheden').hide();
    }

    $('#antwoord_definitie input[type="button"]').click(function() {
        /* Harvest results */
        var postdata = {};
        $('#antwoord_definitie select').each(function() {
            postdata[$(this).attr('name')] = $(this).val();
        });
        $('#antwoord_definitie input').each(function() {
            postdata[$(this).attr('name')] = $(this).val();
        });
        $('#antwoord_definitie textarea').each(function() {
            postdata[$(this).attr('name')] = $(this).val();
        });

        $.post(
            '/zaaktype/status/antwoorden',
            postdata,
            function() {
                $('#dialog').dialog('close');
            }
        );
    });
}

function load_zaaktype_kenmerken() {
    $('#zaaktype_definitie input[type="button"]').click(function() {
        /* Harvest results */
        var postdata = {};
        $('#zaaktype_definitie select').each(function() {
            postdata[$(this).attr('name')] = $(this).val();
        });

        $('#zaaktype_definitie input').each(function() {
            if (
                $(this).attr('type') == 'checkbox' ||
                $(this).attr('type') == 'radio'
            ) {
                if ($(this).attr('checked') == true) {
                    postdata[$(this).attr('name')] = $(this).attr('value');
                }
                return;
            }
            postdata[$(this).attr('name')] = $(this).attr('value');
        });
        $('#zaaktype_definitie textarea').each(function() {
            postdata[$(this).attr('name')] = $(this).val();
        });

        $.post(
            '/zaaktype/status/zaaktype',
            postdata,
            function() {
                $('#dialog').dialog('close');
            }
        );
    });
}

/*** Zaaktype Static Functions
 *
 * Functies voor zaaktype beheer, maar ook voor informatie van zaaktypes
 * (buiten zaaktype beheer)
 */

function load_zaaktype() {
    alert('illegal');
    return;
    $("#zoek_zaaktype form").submit(function(){
        $('#betrokkene_loader').removeClass('disabled');
        /*
        postdata = {
            search: 1,
            jsfill: "[% jsfill %]",
            jscontext: "[% jscontext %]",
            jstype: "[% jstype %]",
            jsversion: "[% jsversion %]",
            method: "[% method %]",
            url: "[% url %]"
        };
        */
        postdata = {};
        $("#zoek_zaaktype input").each(function(){
            if (
                $(this).attr('type') == 'checkbox' ||
                $(this).attr('type') == 'radio'
            ) {
                if ($(this).attr('checked') == true) {
                    postdata[$(this).attr('name')] = $(this).attr('value');
                }
                return;
            }
            postdata[$(this).attr('name')] = $(this).attr('value');
        });
        $("#zoek_zaaktype select").each(function(){
            chosenoption = $(this).find(':selected');
            postdata[$(this).attr('name')] = chosenoption.val();
        });

        $('#search_zaaktype_results').load(
            '/zaaktype/search',
            postdata,
            function() {
                $('#accordion').accordion('option', 'active', 1);
                $('#betrokkene_loader').addClass('disabled');

                refresh_zaaktype_search_rows();
            }
        );

        return false;

    });
}

function refresh_zaaktype_search_rows() {

    $("#search_zaaktype_results .zaaktype_keuze").on('click', function() {

        var jsfillid    = $("#zoek_zaaktype input[name='jsfillid']").val();
        var jsfilldescr = $("#zoek_zaaktype input[name='jsfilldescr']").val();

        var result_id    = $(this).find("input[name='zaaktype_id']").val();

        var result_name    = $(this).find("input[name='zaaktype_name']").val();
        var result_descr = $(this).find("input[name='zaaktype_descr']").val();

        // if the purpose of this dialog is to add a zaaktype filter to uitgebreid zoeken
        // do
        var search_filter_post = $("#zoek_zaaktype input[name='search_filter_post']").val();

           if(search_filter_post) {
               var zaak_type_id = $(this).find("input[name=zaaktype_id]").val();
            var data = 'action=update_filter&filter_type=zaaktype&nowrapper=1&value=' + zaak_type_id;
            updateSearchFilters(data);
            $('#dialog').dialog('close');
        }

        if (jsfillid && jsfilldescr) {
            $(jsfillid).val(result_id);
            if ($(jsfilldescr)[0].tagName == 'INPUT') {
                $(jsfilldescr).val(result_descr);
            } else {
                $(jsfilldescr).html(result_descr);
            }
        }

        // Close window
        $('#dialog').dialog('close');
    });
}


/*** clone rows
 *
 * This function should work as a sharm, make sure you place one row in this
 * table, with an id like: [name]_template, and hide this one.
 *
 * Create a delete row button somewhere, with the class:
 * [name]_del
 *
 * And everyhting should do what you think it should do
 */
var cloned_rows = Array();
function clone_row(name, append) {
    /* Clone */
    clone  = $('#' + name + '_template').clone();
    parent_elem =  $('#' + name + '_template').parent();

    if (cloned_rows[name]) {
        cloned_rows[name] = cloned_rows[name] + 1;
    } else {
        /* Magic, check for by De Don generated rows, and use this as a start */
        /* Count rows, and substract row_template + header + 1(so substract 1) */
        if (name == 'status_row') {
            var numtrrows = parent_elem.find('.status_name_row').length;
        } else {
            var numtrrows = parent_elem.find('.status_row_definitions').length;
        }
        cloned_rows[name] = numtrrows;
    }

    count = cloned_rows[name];

    /* Change every input/select/textarea and append number */
    formelements = Array('input','select','textarea','checkbox');
    for (var i in formelements) {
        clone.find(formelements[i]).each(function() {
            $(this).attr('name',
                $(this).attr('name') + '_' + count
            );
        });
    }

    /* Change this id */
    var currentid = clone.attr('id');
    var newid     = currentid.replace(/template$/g, count);

    clone.attr('id', newid);

    /* Check for a delete button */
    $(clone).find('.' + name + '_del').each(function() {
        $(this).attr('id', name + '_del_' + count);
    });

    /* Make sure the delete button is gonna work */
    clone.find('.' + name + '_del').click(function() {
        /* Get count */
        currentid = $(this).attr('id');
        currentcount = currentid.replace(/.*(\d+)$/g, '$1');

        $('#' + name + '_' + currentcount).remove();

        return false;
    });

    /* Append the clone */
    var afhandelrow = parent_elem.find('.afgehandeld');
    if (afhandelrow.length) {
        afhandelrow.before(clone);
    } else {
        parent_elem.append(clone);
    }

    /* Show the clone */
    clone.show();

    return newid;
}

/*
function load_status_definitions() {
    $('#document_row_template').hide();
    $('#status_def_document_add').click(function() {
        clone_row('document_row');
        return false;
    });
    $('#checklist_template').hide();
    $('#status_def_checklist_add').click(function() {
        clone_row('checklist');
        return false;
    });
}
*/


function load_search_basics() {
    $('.search_betrokkene').click(function() {
        searchBetrokkene($(this));

        return false;
    });

    $('form.zvalidate').submit(function() {
        return zvalidate($(this));
    });
}




function load_selectmenu() {
    $('select.replace-select').selectmenu({
        style:'dropdown',
        width:150
        //maxHeight: '150'
    });

    $('select.replace-select-lang').selectmenu({
        style:'dropdown',
        width:150
    });
}

function load_selectmenu_medium() {
    $('select.replace-select-medium').selectmenu({
        style:'dropdown',
        width:250
        //maxHeight: '150'
    });
}

function load_selectmenu_start_zaak() {
    // TODO: This complete widget should be removed from zaaksysteem, because:
    // A: It breaks the jquery .change() event (it does not work), and
    // B: Long lists do not get a "scroll" functionalitiy (it does not work).
    //
    // So, when you come here thinking you could use this functionalitiy, don't, leave it,
    // and wait until all of this code is shot into oblivion.


    // AGAIN: DO NOT UNCOMMENT THIS CODE, YOU WILL DIE, AND IT IS DEFINITELY NOT BTTW. ONLY
    // HERE FOR GREPPING PURPOSES. - michiel
    // $('select.replace-select-start-zaak').selectmenu({
    //     style:'dropdown',
    //     width:224
    // });
}


/* extra instantie van selectmenu met breder menu. Dit is nodig bij generieke categorie in "een zaaktype aanmaken". De categorieën zijn nl. erg lang… */
function load_selectmenu_breedmenu() {
    $('select.replace-select-breedmenu').selectmenu({
        style:'dropdown',
        width:310,
        menuWidth:500
    });
}

function load_selectmenu_import() {
    $('select.replace-select-breedmenu').selectmenu({
        style:'dropdown',
        width:185,
        menuWidth:500
    });
}

function load_selectmenu_auto_width() {
    $('.kenmerk > select').selectmenu({
        style:'dropdown'
    });
}


//function initializeSpinners() {
    /* kleine spinner
    var opts = {
      lines: 9, // The number of lines to draw
      length: 3, // The length of each line
      width: 2, // The line thickness
      radius:3, // The radius of the inner circle
      color: '#000', // #rbg or #rrggbb
      speed: 1, // Rounds per second
      trail: 100, // Afterglow percentage
      shadow: false // Whether to render a shadow
    };
    var target = document.getElementById('spinner');
    var spinner = new Spinner(opts).spin(target); */

    // grote spinner
/*
    var opts2 = {
      lines: 12, // The number of lines to draw
      length: 7, // The length of each line
      width: 4, // The line thickness
      radius:10, // The radius of the inner circle
      color: '#000', // #rbg or #rrggbb
      speed: 1, // Rounds per second
      trail: 60, // Afterglow percentage
      shadow: false // Whether to render a shadow
    };

    $('.spinner-groot').each(function() {
       var $this = $(this),
           data = $this.data();

        if (data.spinner) {
            data.spinner.stop();
            delete data.spinner;
        }

        data.spinner = new Spinner(opts2).spin(this);
    });
}
*/


var zaken_filter_form_name = '';


/** ready
 *
 */
$(document).ready(function(){

    ezra_basic_selects();
    ezra_zaaktypen_mijlpalen();
    ezra_zaaktypen_auth();
    //ezra_zaaktypen();

    // Default error handling on logged out etc
    $.ajaxSetup({
        error: function(xhr, statusCode, statusMessage) {
            if (
                statusMessage == 'Unauthorized' ||
                statusMessage == 'Forbidden'
            ) {
                window.location = '/';
                return true;
            }
        }
    });

    $("#accordion_search_filters").accordion({
        heightStyle: 'content',
        collapsible: true
    });


    activateCurrentSearchFilter();

    $(document).on('click', ".delete_search_filter", function(){
        $.ztWaitStart();
        $(this).parents('tr').remove();

        updateSearchFilters();
        return false;
    });


    $(document).on('click', '.delete_search_query', function(){
        var search_query_id = $(this).attr('id');
        var data = 'action=delete_search_query&search_query_id=' + search_query_id;


        var url = '/search/dashboard';
        if($('.ezra_show_more_search_queries').length) {
            url += '?show_more_search_queries=1'
        }

        if(confirm('Zoekopdracht verwijderen?')) {
            $('.search_filters_dashboard_wrap').load(url +
                ' .search_filters_dashboard_inner', data,
                function () {
                    $.ztWaitStop();
            });
        }

        return false;
    });


    $('#select_search_grouping_field').change(function(){
        $('form#search_query_grouping').submit();
    });

    $(document).on('click', '.submit_search_query_form.grouping_link a', function(){
        var element = $(this);
        var form = element.closest('form');

        form.attr('action', element.attr('href'));

        var destination = element.attr('id');
        $('input[name=destination]').val(destination);
        $('input[name=grouping_field]').remove();
        element.closest('form').submit();
        return false;
    });

    $(document).on('click', '.ignore_link', function() { return false });

    $(document).on('click', '.submit_search_query_form > ul > li > a', function(){
        var destination = $(this).attr('id');
        if(destination == 'results') {
            $('input[name=grouping_field]').val('');
        }
        $('input[name=destination]').val(destination);
        $(this).closest('form').submit();
        return false;
    });



// avoid multiple server requests when typing a word. only when no character has been typed for
// more than 400 ms. the server will be invoked
// TODO: certain keystrokes generate an event, but do not change the query string. only re-POST
// when the form has changed. this could also be done in updateResults
// TODO refactor using Jquery Autocomplete
    $('.zaak_search_filter').data('timeout', null).on('keydown', function(event) {

        // no enters, these trigger a form post
        if(event.which == 13) {
            return false;
        }

        // a backdoor approach to getting the form to the function, since the timeout construct doesn't allow
        // for parameters.
        zaken_filter_form_name = $(this).closest('form').attr('name');

        if($(this).closest('.search_wrap').find('input.zaak_search_filter').val().length) {
            var form = $('form[name=' + zaken_filter_form_name + ']');
            form.find('.icon-cancel-search').show().unbind('click').click( function() {
                $(this).closest('form').find('input.zaak_search_filter').val('');
                $(this).hide();
                var form_name = $(this).closest('form').attr('name');
                return updateResults(form_name);
            });
        }

        $(this).closest('form').find('.spinner-groot').css('visibility', 'visible');

        clearTimeout($(this).data('timeout'));
        $(this).data('timeout', setTimeout(updateResults, 400));
    });


    $(document).on('submit', '#update_search_filter', function(){
        return validateSearchFilters();
    });


    $(document).on('click', '.add_search_filter .element', function() {
        var obj = $(this);

        var filter_type = obj.attr('id');

        console.log(obj, filter_type);

        if(filter_type == 'disabled') return false;
        if(filter_type == 'zaaktype') return true;

        var search_query_id = $('form input[name=SearchQuery_search_query_id]').val();

        // show the dialog to add a new filter
        if(filter_type == 'kenmerk') {
            addKenmerkFilter(obj, search_query_id);
        } else if(filter_type == 'zaaktype') {

            alert('illegal');

        } else {
            show_filter_popup({
                filter_type : filter_type,
                filter_value: '',
                SearchQuery_search_query_id: search_query_id,
                title       : obj.attr('title')
            });
        }

        return false;
    });

    // any changes in the kenmerken should be communicated to the back-end immediately.
    $(document).on(
        'change',
        'form#search_filters select, form#search_filters input, form#search_filters textarea',
        function(event, ui) {
            // except with the flipping bag-adres autocomplete stuff. in that case, first let the autocomplete finish
            // and update.
            if(!$(this).hasClass('veldoptie_bag_adres')) {
                updateSearchFilters();
            }
    });



    $(".search_query_reset").click(function(){
        return confirm('Alle instellingen wissen?');
    });


    $(document).on('click', ".edit_search_filter", function(){
        var search_query_id = '';
        if( $('form input[name=SearchQuery_search_query_id]')) {
            search_query_id = $('form input[name=SearchQuery_search_query_id]').val();
        }

        show_filter_popup({
            filter_type : $(this).attr('type'),
            filter_value: $(this).attr('value'),
            SearchQuery_search_query_id: search_query_id,
            title       : $(this).attr('title')
        });
        return false;
    });


      $(".search_fields_selector .multiselect").multiselect({searchable: true});


    $('.mijlpaal_content').on('click', '.element_tabel_kenmerk .ezra_kenmerk_grouping_add', function() {

        $(this).parents('div.ezra_table').ezra_table('add_row', '.ezra_kenmerk_grouping_add');
        return false;
    });

    $('.mijlpaal_content').on('click', '.element_tabel_regel .ezra_regels_grouping_add', function() {

        $(this).parents('div.ezra_table').ezra_table('add_row', '.ezra_regels_grouping_add');
        return false;
    });

    $(document).on('click', 'input[name=search_query_cancel_button]', function() {
        $('#dialog').dialog('close');
        return false;
    });

    $(document).on('click', 'input[name=search_query_save_button]', function() {
        var value = $('input[name=search_query_edit_name_input]').val();
        $('#dialog').dialog('close');

        $('input[name=search_query_name_hidden]').val(value);
        var caller_form = $('input[name=search_query_name_hidden]').closest('form');


        $('<input>').attr({
            type: 'hidden',
            value: '1',
            name: 'save_settings'
        }).appendTo('form');

        caller_form.submit();
        return false;
    });


    $(document).on('click', '#new_search', function() {
        $(this).closest('form').find('input[name=action]').val('reset');
        $(this).closest('form').submit();
    });

    $("disabledtable.search_query_table.sortable").bind( "sortchange", function(event, ui) {


    });


    $(document).on('click', '.select_grouping_field', function() {
        var grouping_field = $(this).attr('id');
        $('input[name=grouping_field]').val(grouping_field);
        $('form#search_query_grouping').submit();
        return false;
    });


    $('.add_kenmerkveld').click(function(){

        ezra_dialog({
            url         : '/beheer/bibliotheek/kenmerken/search',
            cgi_params  : {
                ezra_client_info_selector_identifier: 'disabledfunctionality',
                ezra_client_info_selector_naam: 'disabledfunctionality',
                jsversion: 3
            },
            title       : 'Kies een kenmerk'
        }, function() {
                $('.ztAccordion').accordion({
                    heightStyle: 'content'
                });

                ezra_select_kenmerk(function(clicked_obj) { handle_kenmerk_selection(clicked_obj) });
                //ezra_zaaktype_select_kenmerk_search('handle_kenmerk_selection');
                veldoptie_handling();
            }
        );
        return false;
    });

    $(document).on('click', '.search_query_name .actions-nav-new', function() {
        $(this).closest('form').find('input[name=action]').val('reset');
        $(this).closest('form').submit();
        return false;
    });


    // if the search query id new, it has no id in the url
    // if existing, add id to url to select the correct server resource
    $(document).on('click', '.search_query_name .bewerk', function() {
        var input = $(this).closest('form').find('input[name="SearchQuery_search_query_id"]'),
            search_query_id = input.val();

        ezra_dialog({
            url     : search_query_id ? '/search/' + search_query_id + '/save' : '/search/save',
            title   : 'Zoekopdracht opslaan'
        });
        return false;
    });


    // 20120217: what the hell doet dit? Het is irritant bij het klikken op
    // een toelichting onder zaakbehandeling > taken > kenmerken. Dus klikken
    // op vraagteken en dan op de gegeven toelichting klikken.
//    $(document).on('click', '.dropdown', function() {
        // Workaround for above comment
//        if ($(this).find('.dropdown-content').length) { return false; }

//        $.ztWaitStart();
//    });


    var hoverIntentConfig = {
         over: showPreview, // function = onMouseOver callback (REQUIRED)
         timeout: 500, // number = milliseconds delay before onMouseOut
         out: hidePreview // function = onMouseOut callback (REQUIRED)
    };

    //$('.doc_intake_row .doc-preview-init').hoverIntent(hoverIntentConfig);
    $(document).on('click', '.doc-preview-init', function() {
    $(this).addClass('active');
    $(this).closest('.doc_intake_row').addClass('active');
        showPreview($(this));
    });




    $(document).on('click', '.search_query_access', function() {
        var current = $('input[name=search_query_access_hidden]').val();

        if(current == 'public') {
            $('input[name=search_query_access_hidden]').val('private');
            $(this).removeClass('search_query_access_public');
            $(this).addClass('search_query_access_private');
        } else {
            $('input[name=search_query_access_hidden]').val('public');
            $(this).removeClass('search_query_access_private');
            $(this).addClass('search_query_access_public');
        }
        return false;
    });


    $('.export_zaaktype').click(function() {
        document.location.href = '/beheer/zaaktypen/118/export';
    });
});


function hidePreview() {}

function showPreview(click_element) {
    //var my_offset = $(this).offset();
    var preview_container = click_element.closest('.document_row');
    var preview_div = preview_container.find('.doc_preview');
    //preview_div.offset({ top: my_offset.top + 10, left: my_offset.left + $(this).width()/2 - 80 });

    var document_id = preview_container.attr('id').match(/\d+/)[0];
    if(!document_id)
        return;

    if(preview_div.data('current_thumbnail') == document_id) {
        preview_div.css('visibility', 'visible');
        preview_div.addClass('doc_preview_visible');
        return;
    }

    preview_container.find('.doc_preview_pijl').css('visibility', 'visible');
    preview_div.css('visibility', 'visible');
    preview_div.addClass('doc_preview_visible');
    preview_div.find('img').show();

    var params = { document_id : document_id };
    if(preview_container.hasClass('dropped_document'))
        params.dropped_document = 1;

    preview_div.load(
        '/zaak/documentpreview',
        params,
        function(responseText, textStatus, XMLHttpRequest) {
            preview_div.data('current_thumbnail', document_id);
            if(textStatus == 'success') {
            } else {
               preview_div.html('<span>Er is een probleem opgetreden, ververs de pagina</span>');
            }
        }
    );
}


// TODO use json to avoid a page reload here. page reload = evil
function handle_kenmerk_selection(item) {
    var kenmerk_id = item.attr('id');


    $('input[name=additional_kenmerk]').val("kenmerk_id_" + kenmerk_id);
    $('form#search_presentation').submit();
    return false;
}

function addKenmerkFilter(obj, search_query_id) {

    ezra_dialog({
        url     : '/beheer/bibliotheek/kenmerken/search',
        title   : 'Kenmerk',
        which   : '#searchdialog'
    }, function() {
            var clickhandler = function(clicked_obj) {
            var item_id = clicked_obj.attr('id');

            updateSearchFilters("action=add_kenmerk_filter&bibliotheek_kenmerken_id=" + item_id);
        };

        ezra_select_kenmerk(clickhandler);
    });

}


function updateSearchFilters(data) {
    //initializeSpinners();
        $('.spinner-groot').css('visibility', 'visible');

    var my_form = $('form#search_filters');
    var search_query_id = $('form#search_filters input[name=SearchQuery_search_query_id]').val();
    if(!data) {
        data = my_form.serialize();
    }

    var url = "/search";
    if(search_query_id) {
        url = url + "/" + search_query_id;
    }

    if(!validateSearchFilters()) {
        return false;
    }
    $('.search_filters_wrap').load(url + ' .search_filters_inner', data,
        function () {
            $('#dialog').dialog('close');
            $("#accordion_search_filters").accordion({
                heightStyle: 'content',
                collapsible: true
            });
            activateCurrentSearchFilter();
            initialize_tabinterface();
            $('.ztAjaxTable').ztAjaxTable();

            $.ztWaitStop();

            if(data == 'action=show_kenmerken_filters' && $(".kenmerken_container .ztAjaxTable").length) {
                $(".kenmerken_container a.add.ztAjaxTable_add.float").click();
            }
            veldoptie_handling();
            initializeEverything();
    //        $("#accordion_search_filters").accordion('active', 0);
    });
}


function validateSearchFilters() {

    var filter_class = $('#update_search_filter input[name=filter_class]').val();

    if(filter_class == 'period') {
        var period_type = $("input[@name=period_type]:checked").val();
        if(period_type == 'specific') {
            var start_date = $('#update_search_filter input[name=start_date]').val();
            var end_date = $('#update_search_filter input[name=end_date]').val();
            if(!start_date && !end_date) {
                $('#update_search_filter #error_message').html('Geef een begin- of einddatum');
                return false;
            }
        }
    }

    if(filter_class == 'aanvrager') {
        var my_value = $("input[name=ztc_aanvrager_id]");
        var my_select_value = $("select[name=ztc_org_eenheid_id] :selected");

        if(!my_value.val() && !my_select_value.val()) {
            $('#update_search_filter #error_message').html('Kies een ' + filter_class);
            return false;
        }
    }

    return true;
}


function activateCurrentSearchFilter() {
    var current_filter_type = $('#search_filters input[name=current_filter_type]').val();
    if(current_filter_type && $('#search_filter_holder_' + current_filter_type).length &&
        !$('#search_filter_holder_' + current_filter_type + '.ui-state-active').length) {
        $('#accordion_search_filters').accordion('option', 'active', '#search_filter_holder_' + current_filter_type);
    }
}


/** zaaktypen_milestone_definitie functionaliteiten
 *
 */

/**
 *
 * DYNTABLE CONFIG CLASS
 *
 * init: Div $('.ezra_table').ezra_table();
 *
 * Table must consist of:
 * 1 table with class   : ezra_table_table
 * 1 teplate row, class : ezra_table_row_template
 * n rows to be ignored : ezra_table_row_ignore
 * n rows already there : ezra_table_row
 *
 * Class for row add button: ezra_table_row_add
 * Class for row del button: ezra_table_row_del
 * Class for unique input identifier: ezra_table_row_edit_identifier
 *
 * Possible callback:
 * Create an add button, like this:
 * <a href="#" class="ezra_table_row_add"
 *    rel="callback: callback_function" />
 *  It will receive:
 *    ezra_table object, added row, current rownumber
 *
 * Methods:
 * $('.ezra_table').ezra_table('add_row');
 *
 * Manual add a row
 */
(function($) {
    var methods = {
        init : function( options ) {
            var defaults    = {
                draggable: 1,
                num_rows: 0
            };

            var options     = $.extend(defaults, options);

            return this.each(function() {
                obj     = $(this);

                var $this   = $(this),
                    data    = $this.data('ezra_table');

                if (!data) {
                    table   = obj.find('.ezra_table_table');

                    // Hide rows
                    table.find('tr.ezra_table_row_ignore').hide();
                    table.find('tr.ezra_table_row_template').hide();

                    // Count visible rows
                    $(this).data('ezra_table', {
                        num_rows: table.find('tr.ezra_table_row').length
                    });

                    // Find row add href
                    obj.find('.ezra_table_row_add').unbind().click(function() {
                        obj.ezra_table('add_row');
                        return false;
                    });

                    // Find row del href
                    obj.find('.ezra_table_row_del').unbind().click(function() {
                        var ezra_table_table = $(this).closest('.ezra_table');
                        $(this).closest('tr').remove();
                        var options = obj.ezra_table('get_options', $(this).attr('rel'));
                        if(options.callback) {
                            window[options.callback]($(this),obj);
                        }
                        ezra_table_table.ezra_table('update_table_information');
                        obj.ezra_table('autosave',
                            {
                                reload_milestones: 1
                            }
                        );
                        return false;
                    });

                    // Find row edit href
                    obj.find('.ezra_table_row_edit').unbind().click(function() {
                        obj.ezra_table('edit_row', $(this));
                        return false;
                    });
                }
            });
        },
        update_table_information: function() {
            var obj = $(this);
            var data = obj.data('ezra_table');

            if (!data) return;

            // Make sure we update the element counter
            var number_of_rows  = obj.ezra_table('get_num_rows');
            var table_name      = obj.attr('class').replace(/.*element_tabel_([a-zA-Z]+).*/, '$1');

            $('#milestone_acc_' + table_name + ' span.number').html(number_of_rows);
        },
        autosave : function(options) {
            var obj = $(this);
            var form = obj.closest('form');

            if(form.find('p.header .status').length) {
                form.find('p.header .status').html(' Bezig met opslaan...');
            } else {
                form.find('p.header').append('<span class="status" style="color:#888"> Bezig met opslaan...</span>');
            }

            // Disabled drag and drop row behavior
            var drag_elements = form.find('td.drag');

            drag_elements.toggleClass('drag').removeClass('hover');

            // Disable breadcrumb menu
            var breadcrumb = obj.closest('.ztb').find('.form-actions-sticky');
            var breadcrumbButtons = breadcrumb.find('input');
            var breadcrumbLinks = breadcrumb.find('a');

            breadcrumb.css('opacity', '0.5');
            breadcrumbButtons.css('pointer-events', 'none');
            breadcrumbLinks.attr('disabled', 'disabled');

            // Disable edit/delete buttons
            var link_elements = form.find('a.ezra_table_row_edit, a.ezra_table_row_del');

            link_elements.css('pointer-events', 'none');

            // Overlay the screen with loader
            $.ztWaitStart();

            $.post(
                form.attr('action'),
                form.serialize() + "&autosave=1",
                function (data) {
                    form.find('p.header .status').html(' Opgeslagen');
                    $.ztWaitStop();

                    if (
                        options && options['reload_milestones']
                    ) {
                        var active_accordion = $( "#accordion_milestones" ).accordion( "option", "active" );

                        //obj.ezra_table('reset_order', form);
                        form.closest('#configureren').each(function() {
                            if (!$(this).attr('id')) {
                                return true;
                            }

                            $(this).load(
                                form.attr('action') + ' #configureren form',
                                function () {
                                    initializeEverything();

                                    ezra_basic_beheer_functions();
                                    ezra_zaaktypen_mijlpalen(
                                        {
                                            active_accordion: active_accordion
                                        }
                                    );

                                    //$( "#accordion_milestones" )
                                    //    .accordion('option','active',active_accordion);

                                }
                            );
                        });
                    }
                },
                'json'
            );
        },
        get_new_rownumber: function() {
            var $this = $(this),
                data = $this.data('ezra_table');

            var highest = 0;
            var table   = $(this).find('table');
            var obj     = $(this);

            table.find('.ezra_table_row').each(function() {
                var trclass = $(this).attr('id');
                if (!trclass.match(/_row_number_(\d+)$/)) {
                    return 0;
                }

                var rownumber    = trclass.replace(/.*_row_number_(\d+).*/g, '$1');
                if (parseInt(rownumber) > parseInt(highest)) {
                    highest = parseInt(rownumber);
                }
            });

            if (highest < 1) {
                returnval = 1;
            } else {
                returnval = parseInt(highest) + parseInt(1);
            }
            /* Last check, if row is lower than initial numrows, do numrows */
            if (typeof(data) !== 'undefined' && returnval <= data.num_rows) {
                return (data.num_rows + 1);
            }

            $(this).data('ezra_table',{
                num_rows: returnval
            });

            return returnval;
        },
        get_num_rows : function() {
            var table   = $(this).find('table');

            return table.find('.ezra_table_row').length;
        },
        get_options: function(get_options) {
            return getOptions(get_options);
        },
        add_row: function (different_add_row_class) {
            var $this = $(this),
                data = $this.data('ezra_table');

            var clone   = $(this).find('.ezra_table_row_template').clone(true);
            var parent  = $(this).find('.ezra_table_row_template').parent();

            var nextrownumber = $(this).ezra_table('get_new_rownumber');

            // Change row basics
            if (clone.attr('id').match('row_number')) {
                clone.attr('id', clone.attr('id') + '_' + nextrownumber);
            }

            clone.removeClass('ezra_table_row_template')
                .addClass('ezra_table_row');
            clone.addClass('ezra_table_row_number_' + nextrownumber);


            // Change every 'input/textarea/select field' to reflect rownumber
            clone.find('input, select, textarea').each(function() {
                $(this).attr('name',
                    $(this).attr('name') + '.' + nextrownumber
                );
            });

            clone.find('.roworder').val(nextrownumber);

            // show row
            clone.show();

            parent.append(clone);

            clone.find('select.replace-select').selectmenu({
                style:'dropdown',
                width:150,
                maxHeight: '150'
            });

            var obj = $(this);
            obj.ezra_table('update_table_information');

            // There could be a possible callback, RUN IT
            var add_row_class = '.ezra_table_row_add';
            if (different_add_row_class) {
                add_row_class = different_add_row_class;
            }

            obj.find(add_row_class).each(function() {
                var add_options = obj.ezra_table('get_options', $(this).attr('rel'));

                if (add_options.newcallback) {
                    obj.ezra_table('search_dialog',$(this),add_options.newcallback,clone);
                } else if (add_options.callback) {
                    window[add_options.callback]($(this),obj,clone,nextrownumber);
                }
            });

            return clone;
        },
        edit_row: function(editobj) {
            identifier    = editobj.parents('tr').find('.ezra_table_row_edit_identifier').val();
            rowidentifier = editobj.parents('tr').attr('id');
            rownumber     = editobj.parents('tr').find('.roworder').val();

            var fire_elem = editobj.clone();
            var new_rel   = fire_elem.attr('rel');

            var options   = editobj.parents('div.ezra_table').ezra_table('get_options', fire_elem.attr('rel'));

            options['row_id']       = rowidentifier;
            options['rownumber']    = rownumber;
            options['edit_id']      = identifier;


            $.ztWaitStart();
            var url = editobj.attr('href');

            var callback = function() {
                $.ztWaitStop();
                ezra_basic_functions();
                initializeEverything();

                /* Find edit button */
                $('#dialog form').submit(function() {
                    var form = $(this);
                    var serialized = form.serialize();

                    if(options['callback']) {
                        form.data('callback', options['callback']);
                    }

                    $.post(
                        url,
                        serialized,
                        function(data) {
                            if (options['callback']) {
                                window[options.callback](form, editobj, rowidentifier);
                            }
                            $('#dialog').dialog('close');
                        }
                    );

                    return false;
                });

            }

            ezra_dialog({
                url         : url,
                title       : editobj.attr('title'),
                layout      : options['ezra_dialog_layout'],
                cgi_params  : options
            }, callback);
        },
        search_dialog: function(addelem, callbackrequest, rowobj) {
            var rowidentifier = rowobj.attr('id');

            // Append some options to href object
            var fire_elem = addelem.clone();
            var new_rel   = fire_elem.attr('rel');
            if (new_rel) {
                new_rel = new_rel + '; '
            }

            new_rel = new_rel + 'row_id: ' + rowidentifier;
            fire_elem.attr('rel', new_rel);

            $('#dialog').bind( 'dialogclose', function (event,ui) {
                    var ezra_table_table = rowobj.closest('.ezra_table');
                    rowobj.remove();
                    ezra_table_table.ezra_table('update_table_information');
                }
            );

            // This code needs some clean up - the type of element ends up here. Gross.
            var layout_config = {
                regel     : 'large',
                kenmerk   : 'large',
                objecttype: 'mediumlarge-accordion',
                sjabloon  : 'mediumlarge-accordion',
                zaak      : 'medium-accordion'
            };

            var layout;
            if(callbackrequest && layout_config[callbackrequest]) {
                layout = layout_config[callbackrequest];
            }
            ezra_dialog({
                url     : fire_elem.attr('href'),
                title   : fire_elem.attr('title'),
                layout  : layout,
                cgi_params: getOptions(new_rel)
            }, function() {
                ezra_zaaktype_select_kenmerk();
                ezra_zaaktype_select_sjabloon_search();
                ezra_zaaktype_select_objecttype_search();
            });
        },
        update_row: function(rowid, data) {
            var update_row = $('#' + rowid);

            for (var rowident in data) {
                var content = data[rowident];

                element = update_row.find(rowident).each(function() {
                    if ($(this).get(0).tagName.toLowerCase() == 'input') {
                        $(this).val(content);
                    } else {
                        $(this).html(content);
                    }
                });
            }

            $(this).ezra_table('autosave', {
                reload_milestones: 1
            });

        }
    };

    $.fn.ezra_table = function( method ) {
        // Method calling logic
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.ezra_mijlpaal_configureren' );
        }
    };

})(jQuery);

/**
 *
 * MILESTONE CONFIG CLASS
 *
 * methods:
 * - none
 *
 */
(function($) {
    var methods = {
        init : function( options ) {
            var defaults    = {
                draggable: 0
            };

            var options     = $.extend(defaults, options);

            return this.each(function() {
                obj = $(this);

                if(obj.data('ezra_mijlpaal_configureren')) {
                    return;
                }

                obj.find('.add_element .element').each(function() {
                    if (options.draggable) {
                        $(this).draggable({
                            revert: true
                        })
                    }

                    $(this).click(function() {
                        methods.activate_element($(this));

                        return false;
                    });
                });


                if (options.draggable) {
                    $('.mijlpaal_content').droppable({
                        activeClass: "ui-state-hover",
                        accept: '.add_element',
                        drop: function(event,ui) {
                            methods.activate_element(ui.draggable);
                        }
                    });
                }

                obj.data('ezra_mijlpaal_configureren', 1);
            });
        },
        activate_element : function(element) {
            var element_id = element.attr('id');
            var rel = element.attr('rel');
            var options = getOptions(rel);

            var obj = $('.mijlpaal_content .element_tabel_' + element_id);

            obj.show();

            if (element_id == 'objecttype') {
                obj.ezra_table('add_row', '.ezra_table_row_add_objecttype');
            }
	    else if (element_id == 'text_block') {
		console.log("Adding row");
                obj.ezra_table('add_row', '.ezra_table_row_add_text_block');
	    }
            else {
                obj.ezra_table('add_row');
            }

            var accordion_id = "#milestone_acc_" + element_id;
            if (element_id == 'objecttype' || element_id == 'text_block') {
                accordion_id = "#milestone_acc_kenmerk";
            }
            else if (element_id == 'betrokkene') {
                accordion_id = "#milestone_acc_betrokkene";
            }

            if (!$(accordion_id).hasClass('ui-state-active')) {
                $('#accordion_milestones').accordion('option', 'active', accordion_id);
            }
        }
    };

    $.fn.ezra_mijlpaal_configureren = function( method ) {
        // Method calling logic
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.ezra_mijlpaal_configureren' );
        }
    };

})(jQuery);


function ezra_zaaktypen_mijlpaaldefinitie() {
    //$('.mijlpaal_definitie .ezra_table').ezra_table();

    $(document).on('click', '.ezra_ajax_action', function() {
        var options  = $(this).ezra_table('get_options', $(this).attr('rel'));

        if (options.action == 'add') {
            var formdata = $(this).parents('form').serialize();

            formdata = formdata.replace(/zaaktype_update=1&?/, '');

            var href = $(this).attr('href');

            $(this).parents('form')
                .find('.ezra_ajax_widget')
                .load(
                    $(this).attr('href'),
                    formdata + '&action=' + options['action'],
                    function() {
                        window.location = href;
                    }
                );
        } else if (options.action == 'del') {
            $(this).parents('tr').remove();
        }

        return false;
    });
}

function ezra_zaaktypen_mijlpaaldefinitie_addrow(addbutton, table, row) {
   // alert (row.attr('class'));


}




function ezra_zaaktypen_mijlpalen(options) {
    ezra_zaaktypen_mijlpaaldefinitie();

    $("#accordion_milestones").accordion({
        heightStyle: 'content',
        collapsible: true,
        active: (options && options['active_accordion'] ? options['active_accordion'] : 0)
    });


    if (!$('.mijlpaal_configureren').length) {
        return false;
    }

    $('.mijlpaal_configureren').ezra_mijlpaal_configureren();
    $('.mijlpaal_configureren .ezra_table').ezra_table();

    /* Move to other javascript ding */
    //$('.ezra_table_row_edit').click(function() {
    //    var current_row = $(this).parents('tr');
    //});

    /*
    $('.ezra_table_table th').click(function() {
            var table   = $(this).parents('table');

            table.find('tr.ezra_table_row').toggle(200);


    });
    */
}


function ezra_zaaktypen_auth() {
    if (!$('.auth_definitie').length) {
        return false;
    }

    $('.auth_definitie .ezra_table').ezra_table();
}

/* Text blocks/areas */

function ezra_text_block_add_dialog(addelem,obj,clone,nextrownumber) {
    clone.addClass('mijlpaal_element_rij_groep');

    rowidentifier = clone.attr('id');
    rownumber = nextrownumber;

    var current_edit_href   = clone.find('.ezra_table_row_edit').attr('href');

    console.log("current edit href", current_edit_href);
    current_edit_href       = current_edit_href.replace(/kenmerk\/bewerken/, 'kenmerkgroup/bewerken');

    clone.find('.ezra_table_row_edit').attr('rel', 'callback: ezra_text_block_update_row');
    clone.find('.ezra_table_row_edit').attr('href', current_edit_href);

    var fire_elem = addelem.clone();
    var new_rel   = '';

    new_rel = 'row_id: ' + rowidentifier + '; callback: ezra_text_block_add_dialog_submit;';
    new_rel = new_rel + '; rownumber: ' + rownumber + '; uniqueidr: ' + rowidentifier;

    fire_elem.attr('rel', new_rel);

    $('#dialog').bind( 'dialogclose', function (event,ui) {
            var ezra_table_table = clone.closest('.ezra_table');
            clone.remove();
            ezra_table_table.ezra_table('update_table_information');
        }
    );

    ezra_dialog({
        url         : fire_elem.attr('href'),
        title       : fire_elem.attr('title'),
        cgi_params  : {
            row_id      : rowidentifier,
            rownumber   : rownumber,
            uniqueidr   : rowidentifier
        }
    }, function() {
        ezra_text_block_add_dialog_submit();
    });
}

function ezra_text_block_update_row(formobj,editobj, rowid) {
    var currenttr = editobj.closest('tr');

    var ezra_table  = $('#' + rowid).parents('div.ezra_table');
    var rownaam     = formobj.find('[name="kenmerken_label"]').val();

    ezra_table.ezra_table('update_row', rowid, { '.rownaam': rownaam });
}

function ezra_text_block_add_dialog_submit() {
    $('#kenmerk_definitie').find('form').submit(function() {
        var text_block_content = $('#kenmerk_definitie').find('[name="kenmerken_text_content"]').val();
        if(text_block_content.length == 0) {
            alert('Vul een tekst in');
            return false;
        }

        // Do Ajax call
        var serialized = $(this).serialize();
        var container  = $(this).closest('#kenmerk_definitie');

        $.post(
            $(this).attr('action'),
            serialized,
            function(data) {
                var current_row_id = container.find('[name="uniqueidr"]').val();
                var ezra_table  = $('#' + current_row_id).parents('div.ezra_table');
                rownaam = container.find('[name="kenmerken_label"]').val();

                ezra_table.ezra_table('update_row', current_row_id, {
                    '.rownaam'      : rownaam
                });

                $('#dialog').unbind( 'dialogclose');
                $('#dialog').dialog('close');
            }
        );

        return false;
    });
}

/******************/

function ezra_kenmerk_grouping_add_dialog(addelem,obj,clone,nextrownumber) {
    //clone.find('span.rownaam').html('<b>Testgroup</b>');
    clone.addClass('ezra_kenmerk_grouping_group');
    clone.addClass('mijlpaal_element_rij_groep');

    rowidentifier = clone.attr('id');
    //rownumber     = rowidentifier.replace(/.*_(\d+)/, '$1');
rownumber = nextrownumber;
    var current_edit_href   = clone.find('.ezra_table_row_edit').attr('href');

    current_edit_href       = current_edit_href.replace(/kenmerk\/bewerken/, 'kenmerkgroup/bewerken');

    clone.find('.ezra_table_row_edit').attr('rel', 'callback: ezra_kenmerk_grouping_update_row');

    clone.find('.ezra_table_row_edit').attr('href', current_edit_href);

    /* Append some options to href object */
    var fire_elem = addelem.clone();
    var new_rel   = '';

    new_rel = 'row_id: ' + rowidentifier + '; callback: ezra_kenmerk_grouping_add_dialog_submit;';
    new_rel = new_rel + '; rownumber: ' + rownumber + '; uniqueidr: ' + rowidentifier;

    fire_elem.attr(
        'rel',
        new_rel
    );

    // Find hidden
    $('#dialog').bind( 'dialogclose', function (event,ui) {
            var ezra_table_table = clone.closest('.ezra_table');
            clone.remove();
            ezra_table_table.ezra_table('update_table_information');
        }
    );

    ezra_dialog({
        url         : fire_elem.attr('href'),
        title       : fire_elem.attr('title'),
        cgi_params  : {
            row_id      : rowidentifier,
            rownumber   : rownumber,
            uniqueidr   : rowidentifier
        }
    }, function() {
        ezra_kenmerk_grouping_add_dialog_submit();
    });
}

function ezra_regels_grouping_add_dialog(addelem,obj,clone,nextrownumber) {
    //clone.find('span.rownaam').html('<b>Testgroup</b>');
    clone.addClass('ezra_regels_grouping_group');
    clone.addClass('mijlpaal_element_rij_groep');

    rowidentifier = clone.attr('id');
    //rownumber     = rowidentifier.replace(/.*_(\d+)/, '$1');
rownumber = nextrownumber;
    var current_edit_href   = clone.find('.ezra_table_row_edit').attr('href');

    current_edit_href       = current_edit_href.replace(/regels\/bewerken/, 'regelgroup/bewerken');

    clone.find('.ezra_table_row_edit').attr('rel', 'callback: ezra_regels_grouping_update_row');

    clone.find('.ezra_table_row_edit').attr('href', current_edit_href);

    /* Append some options to href object */
    var fire_elem = addelem.clone();
    var new_rel   = '';

    new_rel = 'row_id: ' + rowidentifier + '; callback: ezra_regels_grouping_add_dialog_submit;';
    new_rel = new_rel + '; rownumber: ' + rownumber + '; uniqueidr: ' + rowidentifier;

    fire_elem.attr(
        'rel',
        new_rel
    );

    // Find hidden
    $('#dialog').bind( 'dialogclose', function (event,ui) {
            var ezra_table_table = clone.closest('.ezra_table');
            clone.remove();
            ezra_table_table.ezra_table('update_table_information');
        }
    );

    ezra_dialog({
        url         : fire_elem.attr('href'),
        title       : fire_elem.attr('title'),
        cgi_params  : {
            row_id      : rowidentifier,
            rownumber   : rownumber,
            uniqueidr   : rowidentifier
        }
    }, function() {
        ezra_regels_grouping_add_dialog_submit();
    });
}


function ezra_kenmerk_grouping_update_row(formobj,editobj, rowid) {
    var currenttr = editobj.closest('tr');

    var ezra_table  = $('#' + rowid).parents('div.ezra_table');
    var rownaam     = formobj.find('[name="kenmerken_label"]').val();

    ezra_table.ezra_table('update_row', rowid, {
        '.rownaam'      : rownaam
    });
}

function ezra_regels_grouping_update_row(formobj,editobj, rowid) {
    var currenttr = editobj.closest('tr');

    var ezra_table  = $('#' + rowid).parents('div.ezra_table');
    var rownaam     = formobj.find('[name="regels_label"]').val();

    ezra_table.ezra_table('update_row', rowid, {
        '.rownaam'      : rownaam
    });
}

function ezra_kenmerk_grouping_add_dialog_submit() {
    $('#kenmerk_definitie').find('form').submit(function() {
        // Do Ajax call
        var serialized = $(this).serialize();
        var container  = $(this).closest('#kenmerk_definitie');

        $.post(
            $(this).attr('action'),
            serialized,
            function(data) {
                var current_row_id = container.find('[name="uniqueidr"]').val();
                var ezra_table  = $('#' + current_row_id).parents('div.ezra_table');
                rownaam = container.find('[name="kenmerken_label"]').val();

                ezra_table.ezra_table('update_row', current_row_id, {
                    '.rownaam'      : rownaam
                });

                $('#dialog').unbind( 'dialogclose');
                $('#dialog').dialog('close');
            }
        );

        return false;
    });
}

function ezra_regels_grouping_add_dialog_submit() {
    $('#regels_definitie').find('form').submit(function() {
        // Do Ajax call
        var serialized = $(this).serialize();
        var container  = $(this).closest('#regels_definitie');

        $.post(
            $(this).attr('action'),
            serialized,
            function(data) {
                var current_row_id = container.find('[name="uniqueidr"]').val();
                var ezra_table  = $('#' + current_row_id).parents('div.ezra_table');
                rownaam = container.find('[name="regels_label"]').val();

                ezra_table.ezra_table('update_row', current_row_id, {
                    '.rownaam'      : rownaam
                });

                $('#dialog').unbind( 'dialogclose');
                $('#dialog').dialog('close');
            }
        );

        return false;
    });
}


function show_filter_popup(params) {

    // default fallback
    var url = '/search/filter/edit';

    var filter_type = params['filter_type'];

    if(filter_type == 'behandelaar' || filter_type == 'coordinator') {
         url = '/betrokkene/search';
        params = {
                ezra_client_info_selector_identifier: 'disabledfunctionality',
                ezra_client_info_selector_naam: 'disabledfunctionality',
                betrokkene_type: 'medewerker',
                jsversion: 3,
                search_filter_post: filter_type,
                title: filter_type == 'behandelaar' ? 'Behandelaar' : 'Coordinator'
        };
    } else if(filter_type == 'zaaktype') {
         alert('illegal');

    }


    ezra_dialog({
        url         : url,
        cgi_params  : params,
        title       : params['title']
    }, function() {
        $("#update_search_filter #start_date").datepicker();
        $("#update_search_filter #end_date").datepicker();

        var straatnaam = $("#update_search_filter .veldoptie_bag_adres_uitvoer input[name=bag_value_straatnaam]").val();
        $("#update_search_filter .veldoptie_bag_adres_invoer input[name=bag_value_straatnaam]").val(straatnaam);

        var huisnummer = $("#update_search_filter .veldoptie_bag_adres_uitvoer input[name=bag_value_huisnummer]").val();
        $("#update_search_filter .veldoptie_bag_adres_invoer input[name=bag_value_huisnummer]").val(huisnummer);

        $('#accordion').accordion({
            heightStyle: 'content'
        });
        veldoptie_handling();
//        load_zaaktype();
    });
}


function ezra_search_opties() {
    var ezra_table  = $('div.ezra_table').ezra_table();
}

function reload_aanvrager_wgts() {
    /* Onload, disable every aanvrager */
    $("#tab-wgt-aanvrager .wgt-betrokkene-natuurlijk_persoon_search").attr('style', 'display: none;');
    $("#tab-wgt-aanvrager .wgt-betrokkene-bedrijf_search").attr('style', 'display: none;');
    $("#tab-wgt-aanvrager .wgt-betrokkene-medewerker_search").attr('style', 'display: none;');

    $("input[name='betrokkene_type']").each(function() {
        if ($(this).attr('checked')) {
            $(".wgt-betrokkene-" + $(this).attr('value') + "_search").attr('style', 'display: inline;');
        }
    });
}

function loadBetrokkene(rel) {
alert("loadBetrokkene");
    if (!rel) {
        return false;
    }

    options = rel.split(/;/);

    /* Load options */
    id          = options[0];
    btype       = options[1];
    inputname   = options[2];
    context     = options[3];

    if (!id) {
        return false;
    }

    $('#dialog .dialog-content').load(
        '/betrokkene/search',
        {
            jsfill: $(this).attr('name'),
            jscontext: context,
            jstype: jstype
        },
        function() {
            title = 'Zoek betrokkene (' + jstype + ')';
            openDialog(title);
        }
    );

}


function reload_interne_aanvrager() {
    $('#intern_betrokkene_type').children('div').hide();

    $('#start_intern input[name="betrokkene_type"]:checked').each(function() {
        $('#new_interne_' + $(this).attr('value')).show();
    });
}


function show_aanvragers(trigger) {
    if (trigger == 'internextern') {
        $('.aanvragers_intern').show();
        $('.aanvragers_intern_hidden_field').hide();
        $('.aanvragers_intern_checkbox_field').show();
        $('.aanvragers_extern').show();
    } else if (trigger == 'intern') {
        $('.aanvragers_extern').hide();
        $('.aanvragers_intern').show();
        $('.aanvragers_intern_hidden_field').show();
        $('.aanvragers_intern_checkbox_field').hide();
    } else {
        $('.aanvragers_intern').hide();
        $('.aanvragers_extern').show();
    }
}



function initializeBetrokkeneSelect() {

    $('.ezra_select_betrokkene2_type input[type="radio"]').change( function() {

        var betrokkene_type = $('.ezra_select_betrokkene2_type input[type="radio"]:checked').val();
        var instance_id = $(this).closest('.ezra_select_betrokkene2_type').attr('id');

        var link_element = $('#' + instance_id + ' a.ezra_betrokkene_selector');
        var rel = link_element.attr('rel');
        if(rel) {
            rel = rel.replace(
                /betrokkene_type: (.*?);/,
                'betrokkene_type: ' + betrokkene_type + ';'
            );
            link_element.attr('rel', rel);
        }

    });
}


function initializeSelectZaaktype() {

    if(!$('.ezra_select_zaaktype').length)
        return;

    $('.ezra_select_zaaktype').each(function() {
        var obj = $(this);

        var callback = function(selected_obj) {
            var id = selected_obj.attr('id');
            id = id.match(/zaaktype_id-(\d+)/).pop();

            var name = selected_obj.find('td').html();
            var zaaktype_keuze = obj.closest('.ezra_zaaktype_keuze');
            zaaktype_keuze.find('input[name="zaaktype_id"]').val(id);
            zaaktype_keuze.find('input[name="zaaktype_name"]').val(name);

            // If the casetype has a preset client, prefill it.
            if(selected_obj.attr('data-preset-client-id')) {
                var subject = $('#ezra_id_aanvrager');

                if (!subject.find('input[name="ztc_aanvrager_id"]').val()) {
                    subject.find('input[name="betrokkene_naam"]').val(selected_obj.attr('data-preset-client-name'));
                    subject.find('input[name="ztc_aanvrager_id"]').val(selected_obj.attr('data-preset-client-id'));
                }
            }
        };

        var options = getOptions(obj.attr('rel'));

        obj.ezra_search_box({
            url             : obj.attr('href'),
            select_handler  : callback,
            title           : obj.attr('title'),
            options         : {
                trigger     : options['trigger'] || 'extern',
                active      : '1'
            }
        });
    });
}


function ezra_zaaktype_goto(obj) {
    var form = $('form.ezra_zaaktypebeheer');
    var destination = obj.attr('href');

    // finish is more of a view page, just let the link do it's job as Berners-Lee decreed it.
    if(form.hasClass('ezra_zaaktype_finish')) {
        return true;
    }

    form.find('input[name="destination"]').val(destination);
    zvalidate(form);
    return false;
}

function ezra_ztb_notificatie_update(form, editobj, rowidentifier) {
    var name = form.find('input[name="bibliotheek_notificaties_name"]').val();
    editobj.closest('tr').find('.ezra_ztb_notificatie_label').html(name);
}


function ezra_ztb_notificatie_edit(ezra_table,obj,clone,nextrownumber) {
    ezra_table.ezra_table('edit_row', clone.find('.ezra_table_row_edit'));
}

/* BACKPORT */
function ezra_ztb_betrokkene_edit(ezra_table,obj,clone,nextrownumber) {
    ezra_table.ezra_table('edit_row', clone.find('.ezra_table_row_edit'));
}

function ezra_ztb_betrokkene_update(form, editobj, rowidentifier) {
    var name = form.find('input[name="betrokkene_naam"]').val();
    editobj.closest('tr').find('.ezra_ztb_betrokkene_label').html(name);
    editobj.closest('tr').find('.ezra_ztb_betrokkene_name').val(name);
}

/* END BACKPORT */


function ezra_ztb_result_edit(ezra_table,obj,clone,nextrownumber) {
    ezra_table.ezra_table('edit_row', clone.find('.ezra_table_row_edit'));
}


function ezra_ztb_result_update(form, editobj, rowidentifier) {
    var label = form.find('input[name="resultaten_label"]').val();
    var resultaat = form.find('select[name="resultaten_resultaat"]').val();
    resultaat = resultaat.charAt(0).toUpperCase() + resultaat.slice(1);
    editobj.closest('tr').find('.ezra_ztb_result_label').html(label);
    editobj.closest('tr').find('.ezra_ztb_result_resultaat').html(resultaat);
}

function initializeCasetypePresetClient(obj) {
    if(obj.attr('checked')) {
        $('.ezra_preset_client').show();
    } else {
        $('.ezra_preset_client').hide();
    }
}



function load_betrokkene_timeline() {
    var panel_obj = $('#tabinterface .timeline');
    panel_obj.find('.timeline-content').html(ich.ezra_timeline());

    var betrokkene_exid = panel_obj.attr('rel');
    load_timeline(
        '/betrokkene/' + betrokkene_exid +'/timeline2' + window.location.search,
        panel_obj.find('.timeline-inner')
    );
}


function initAllocation() {
    var value = $('input.ezra_change_allocation:checked').val();

    /* self/behandelaar/group
     * if we want to assign to self, we disable everything
     */

    // The self hidden field
    var allocate_toself=document.getElementsByName("actie_automatisch_behandelen");

    if (value === 'self') {

        $('#ezra_owner_change_department').toggle(false);
        $('#ezra_id_behandelaar').toggle(false);
        $('#ezra_send_notification').toggle(false);
        $('#ezra_allocation_group').toggle(false);

        $('#ezra_allocation_toself').toggle(true);
        for (i=0; i < allocate_toself.length; i++) {
            allocate_toself[i].value = "1"
        }
    }
    else {
        var isBehandelaar = value === 'behandelaar';

        // only for behandelaar
        $('#ezra_owner_change_department').toggle(isBehandelaar);
        $('#ezra_id_behandelaar').toggle(isBehandelaar);
        $('#ezra_send_notification').toggle(isBehandelaar);

        // only for groups
        $('#ezra_allocation_group').toggle(!isBehandelaar);

        // Whatever the choice, we will not assign to self
        for (i=0; i < allocate_toself.length; i++) {
            allocate_toself[i].value = "0"
        }
        $('#ezra_allocation_toself').toggle(false);
    }

    // Make sure the input elements are hidden also
    if ($('#ezra_allocation_group').is(":visible")) {
        $('#ezra_allocation_group select').attr('disabled', null);
    } else {
        $('#ezra_allocation_group select').attr('disabled', 'disabled');
    }

    // Same with behandelaar
    if ($('#ezra_id_behandelaar').is(":visible")) {
        $('#ezra_id_behandelaar input').attr('disabled', null);
    } else {
        $('#ezra_id_behandelaar input').attr('disabled', 'disabled');
    }

}


function installDagobertEditor(obj) {
    //customize the tools menu with toolsItems option
    //'name' is the name of the command which will be executed
    //when the user clicks on the button
    //'title' is the button's title (automagically localized)
    //'css' is the button's class name
    var toolsItems = [
        {'name': 'Bold', 'title': 'Vet', 'css': 'wym_tools_strong'},
        {'name': 'Italic', 'title': 'Schuin', 'css': 'wym_tools_emphasis'},
        {'name': 'InsertOrderedList', 'title': 'Genummerde lijst',
            'css': 'wym_tools_ordered_list'},
        {'name': 'InsertUnorderedList', 'title': 'Lijst',
            'css': 'wym_tools_unordered_list'},
        {'name': 'P', 'title': 'Paragraaf', 'css': 'wym_containers_p wym_custom_tool'},
        {'name': 'H1', 'title': 'Kop 1', 'css': 'wym_icon_h1 wym_custom_tool'},
        {'name': 'H2', 'title': 'Kop 2', 'css': 'wym_icon_h2 wym_custom_tool'},
        {'name': 'H3', 'title': 'Kop 3', 'css': 'wym_icon_h3 wym_custom_tool'},
        {'name': 'Undo', 'title': 'Ongedaan maken', 'css': 'wym_tools_undo'},
        {'name': 'Redo', 'title': 'Opnieuw', 'css': 'wym_tools_redo'},
        {'name': 'CreateLink', 'title': 'Link', 'css': 'wym_tools_link'},
        {'name': 'Unlink', 'title': 'Link verwijderen', 'css': 'wym_tools_unlink'},
        {'name': 'InsertImage', 'title': 'Afbeelding', 'css': 'wym_tools_image'},
        {'name': 'Paste', 'title': 'Plakken',
            'css': 'wym_tools_paste'},
        {'name': 'Preview', 'title': 'Voorvertoning', 'css': 'wym_tools_preview'}
    ];

    if(obj.hasClass('simple')) {
        toolsItems = [
            {'name': 'InsertOrderedList', 'title': 'Genummerde lijst', 'css': 'wym_tools_ordered_list'},
            {'name': 'InsertUnorderedList', 'title': 'Lijst', 'css': 'wym_tools_unordered_list'},
            {'name': 'P', 'title': 'Paragraaf', 'css': 'wym_containers_p wym_custom_tool'},
            {'name': 'Undo', 'title': 'Ongedaan maken', 'css': 'wym_tools_undo'},
            {'name': 'Redo', 'title': 'Opnieuw', 'css': 'wym_tools_redo'},
            {'name': 'CreateLink', 'title': 'Link', 'css': 'wym_tools_link'},
            {'name': 'Unlink', 'title': 'Link verwijderen', 'css': 'wym_tools_unlink'},
            {'name': 'Paste', 'title': 'Plakken', 'css': 'wym_tools_paste'},
            {'name': 'Preview', 'title': 'Voorvertoning', 'css': 'wym_tools_preview'}
        ];
    }

    if(obj.hasClass('ezra_dagobert_editor_enlarge')) {
        toolsItems.push(
            {'name': 'Enlarge', 'title': 'Vergroten/verkleinen', 'css': 'wym_tools_enlarge wym_custom_tool'}
        );
    }

    var wym = obj.wymeditor({
       // stylesheet: '/tpl/zaak_v1/nl_NL/js/dagobert/wymeditor.css',
        iframeBasePath: '/tpl/zaak_v1/nl_NL/js/dagobert/iframe/',
        skinPath: '/tpl/zaak_v1/nl_NL/js/dagobert/skin/',
        jQueryPath: '/tpl/zaak_v1/nl_NL/js/jquery-1.7.1.min.js',
        //skin: 'zaaksysteem',

        toolsItems: toolsItems,
        dialogFeatures: "menubar=no,titlebar=no,toolbar=no,resizable=no,width=830,height=500,top=0,left=0",

        postInit: function(wym) {
            $.each(wym._options.toolsItems, function(index,value) {
                if (!value.css.match(/wym_custom_tool/)) { return true; }
                var html = '<li class="' + value.css + '">'
                         + '<a name="' + value.name + '" '
                         + 'href="#" ffffff '
                       + 'title="' + value.title + '" '
                         + " style='background-image:"
                         + " url(../images/"
                         + value.css +".png)'>"
                         + value.title
                         + "</a></li>";

                var css_identify = value.css.replace(/ ?wym_custom_tool/,'');

                jQuery(wym._box).find(
                    wym._options.toolsSelector + ' li.' + css_identify
                ).replaceWith($(html));

                jQuery(wym._box).find(
                    wym._options.toolsSelector + ' li.' + css_identify
                ).click(function() {
                    wym.container(value.name);
                    return false;
                });

                jQuery(wym._box).find('.wym_area_right').remove();

                jQuery(wym._box).find('.wym_area_main').css('margin-right','-2%');

                if(typeof register_wymeditor_poll == "function") {
                    register_wymeditor_poll(obj.parents('form'));
                }

                if(
                    value.css.match(/wym_tools_enlarge/) &&
                    !jQuery(wym._box).find('.wym_tools_enlarge').hasClass('wym_tools_click-initialized')
                ) {
                    jQuery(wym._box).find('.wym_tools_enlarge').addClass('wym_tools_click-initialized')

                    jQuery(wym._box).find('.wym_tools_enlarge').click( function() {
                        var skin_obj = $(this).closest('.wym_skin_default');
                        var me = $(this);

                        if(!skin_obj.hasClass('wym_skin_enlarge')) {
                            skin_obj.addClass('wym_skin_enlarge');
                        } else {
                            skin_obj.removeClass('wym_skin_enlarge');
                        }
                    });
                }
            });
        }
    });

    // when reloading the page with ajax, a new wymeditor instance is created. the
    // link between the wymeditor instance becomes shady. this index serves to reconnect
    // with the actual instance after a reload. see webform.js
    var wymeditor_index = WYMeditor.INSTANCES.length - 1;
    obj.attr('wymeditor_index', wymeditor_index);
}

function initialiseQuillEditors(context) {

    var container = context ? context : '';

    $(container +' .zs-rich-text-editor').each( function(index, editor) {

        var hiddenInput = $(editor)[0],
            html = hiddenInput.value,
            quillContainer = document.createElement('DIV'),
            id = new String('quill_'+hiddenInput.getAttribute('name')).replace(/\./g, '__'),
            features = hiddenInput.getAttribute('features') ? hiddenInput.getAttribute('features').split(' ') : null,
            toolbar,
            quill;


        toolbar = {
            toolbar: features ?
                features.map( function(feature) {
                    if (feature === 'bullet') {
                        return { list: 'bullet' };
                    }
                    if (feature === 'ordered') {
                        return { list: 'ordered' };
                    }
                    if (feature === 'header') {
                        return { header: [1, 2, 3, 4, 5, 6, false] };
                    }
                    return feature;
                })
                : null
        };

        quillContainer.setAttribute('id', id);

        hiddenInput.parentNode.insertBefore(quillContainer, hiddenInput[0]);
        hiddenInput.classList.remove('zs-rich-text-editor');

        quill = new Quill('#'+id, {
            modules: features ? toolbar : {},
            theme: 'snow'
        });

        if (html) {
            quill.pasteHTML(html);
        }

        quill.on('text-change', function(delta, oldDelta, source) {

            var contents = quill.container.children[0].innerHTML;

            if (contents === '<p><br></p>'
                || contents === '<p><br /></p>'
                || contents === '<p><br/></p>'
            ) {
                contents = '';
            }

            hiddenInput.value = contents;

        });

    });

    return true;

};



function initialiseAddressAutoComplete() {

    var fields = {};

    $('.zs-address-autocomplete').each( function() {

        var key = $(this).attr('name'),
            val = $(this).val();

        fields[key] = val;

    });

    lockFields();

    $('.zs-address-autocomplete-input').on('input', function() {

        var el = $(this),
            name = el[0].name,
            fieldType = _.first(name.match(/([a-z]+[-]+\b(correspondentie)+[_])|([a-z]+[-|_])/));

        fields[name] = el.val();

        fields.type = fieldType;

        getAddress(fields);

    });

    function fillFields(fields) {

        for (var key in fields) {

            $('input[name='+key+']').val(fields[key]);

        }

        lockFields();
    }

    function lockFields() {

        $('.zs-address-autocomplete-output').prop('readonly', true).removeClass('ng-dirty ng-invalid');

    }

    function getAddress(fields) {

        var postcode = fields[fields.type + 'postcode'] ? fields[fields.type + 'postcode'].replace(' ','') : '',
            number = fields[fields.type + 'huisnummer'];

        if (
            postcode && postcode.match(/[0-9]{4}[aA-zZ]{2}/g)
            && number && number.length
        ) {

            $.ajax({
                url: '/api/v1/address/search/?query:match:zipcode='+postcode+'&query:match:street_number='+number
            })
            .done(function( data ) {

                var result = _.first(data.result.instance.rows);

                if (result){

                    fields[fields.type + 'straatnaam'] = _.get(result, 'instance.street');
                    fields[fields.type + 'woonplaats'] = _.get(result, 'instance.city');

                    if (data.result.instance.rows.length === 1) {
                        fields[fields.type + 'huisnummertoevoeging' ] = _.get(result, 'instance.street_number_suffix');
                    }

                    fillFields(fields);

                } else {

                    $('.zs-address-autocomplete-output').prop('readonly', false).addClass('ng-dirty ng-invalid');

                }

            })
            .fail(function() {

                $('.zs-address-autocomplete-output').prop('readonly', false);

            });
        }
    }
}
