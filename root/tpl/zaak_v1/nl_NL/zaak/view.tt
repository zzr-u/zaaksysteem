[% USE Dumper %]
[% USE Scalar %]
[% USE JSON %]

[% isOwner = (zaak.behandelaar.gegevens_magazijn_id == c.user.uidnumber ? 'true' : 'false') %]

<div class="view_zaak zaakdossier[% IF zaak.is_afgehandeld %] zaakdossier-afgehandeld[% END %]" id="zaak[% zaak.id %]" data-ng-controller="nl.mintlab.case.CaseController" data-ng-init="isOwner=[% isOwner %];init('[% zaak.id %]');caseTypeId=[% zaak.zaaktype_node_id.get_column('zaaktype_id') %]" data-zs-case-view data-zs-case-id="[% zaak.id %]" data-zs-case-webform-rule-manager data-can-change="[% c.can_change %]">

    <div class="zaakdossier-tabs ui-tabs" id="tabinterface">
    
        <div class="ui-helper-clearfix zaakbehandeling zaakdossier-titel">
        
        <div class="zaak-info">

            <div class="zaak-info-inner clearfix">
                
                <h1 class="zaaktitel block-header-title">
                    [% zaak.id %]
                    [% IF pip %][% zaak.zaaktype_definitie.handelingsinitiator | ucfirst %][% END %]
                     [% zaak.zaaktype_node_id.titel | html %]

                     <span> - [% IF zaak.is_afgehandeld %]Afgehandeld: [% zaak.resultaat %][% ELSE %][% requested_fase.fase %][% END %]</span>
  
                    [% UNLESS pip %]
                </h1>

                <div class="zaak-meta clearfix">
                    <a 
                        href="[% c.uri_for('/betrokkene', zaak.aanvrager_object.ex_id, { 'gm' => 1, 'type' => zaak.aanvrager_object.btype } ) %]" 
                        class="zaak-meta-item" title="Informatie over de aanvrager" 
                        rel="title: Informatie over de aanvrager" 
                        data-zs-title="Aanvrager: [% zaak.aanvrager.naam %]"
                        target="_blank" rel="noopener"
                        ><i class="mdi mdi-account icon-lebensraum icon-blue"></i>[% zaak.aanvrager.naam %]
                    </a> 
            
                [% IF zaak.behandelaar && zaak.behandelaar_object.betrokkene_identifier %]
                    <a href="[% c.uri_for('/betrokkene/get/' _  zaak.behandelaar_object.betrokkene_identifier) %]" class="ezra_dialog zaak-meta-item" title="Informatie over de behandelaar" rel='zaak: [% zaak.nr %]; id: [% zaak.behandelaar_object.betrokkene_identifier %]; title: Informatie over de behandelaar' data-zs-title="Behandelaar: [% zaak.behandelaar.naam %]"><i class="mdi mdi-account-star-variant icon-lebensraum icon-orange"></i>[% zaak.behandelaar.naam %]</a>
                [% ELSE %]
                    <a
                    class="zaak-meta-item zaak-meta-item-action"
                    href="[% c.uri_for('/zaak/' _ zaak.id _ '/open') %]"
                    data-zs-title="Neem zaak in behandeling"
                    ><i class="mdi mdi-account-star-variant icon-viel-lebensraum icon-orange"></i>In behandeling nemen</a>
                [% END %]
                
                <span
                class="zaak-meta-item"
                data-zs-title="Afdeling: [% zaak.ou_object.omschrijving %]"
                ><i class="mdi mdi-source-fork icon-lebensraum icon-green"></i>[% zaak.ou_object.omschrijving %]
              </span>
    
                    <span
                        class="zaak-meta-item"
                        data-zs-title="Registratiedatum"
                        ><i class="mdi mdi-calendar icon-lebensraum icon-red"></i>[% zaak.registratiedatum.dmy %]
                    </span>
                    <span 
                        class="zaak-meta-item [% IF zaak.is_late %]zaak-is-over-streefdatum[%END %]"
                        data-zs-title="Streefafhandeldatum"
                        ><i class="mdi mdi-calendar-check icon-lebensraum icon-red"></i>[% zaak.streefafhandeldatum.dmy %]
                    </span>
                    

                    <a  class="ezra_dialog zaak-meta-item"
                        data-zs-title="Meer details van deze zaak"
                        href="/zaak/[% zaak.id %]/info" 
                        rel="title: Meer details van deze zaak; ezra_dialog_layout: mediumlarge-accordion"
                        ><i class="mdi mdi-information-outline icon-lebensraum icon-grey"></i>Meer details
                    </a>

                    [% PROCESS zaak/confidentiality.tt %]

                
                </div>
                
                <!-- end zaak_basics -->
                
            </div>
            
            <!-- end zaak-info-inner -->
        
        
            <!-- zaak-acties -->
        
            <div class="zaak-acties">
        
    
            [%
                NUM_BUTTONS = 3;

                IF (c.check_any_zaak_permission('zaak_edit', 'zaak_beheer'));
                    IF (!c.can_change || zaak.is_afgehandeld);
                        NUM_BUTTONS = (NUM_BUTTONS - 2);
                    END;
                ELSIF (c.check_any_zaak_permission('zaak_read'));
                    NUM_BUTTONS = 0;
                END;
            %]
            
                [% IF NUM_BUTTONS > 0 %]
                <div class="ezra_actie_button_handling">

                    <div role="navigation" class="button-group button_num_[% NUM_BUTTONS %] ezra_actie_button_handling_navigation clearfix">
                    
                    [% UNLESS !c.can_change || zaak.is_afgehandeld %]
                    <div class="button-group button-group-relative">
                        <a
                        title="Voeg Item Toe"
                        class="button button-secondary button-large dropdown-toggle"
                        href="#voegtoe">
                            <i class="icon-font-awesome icon-plus icon"></i>
                        </a>
                        <div class="ezra_actie_button_handling_popover">
                        
                            <div class="popover" id="popover_voegtoe">
                                
                                <div class="buttons">
                                [% UNLESS !c.can_change || zaak.is_afgehandeld %]
                                    <a
                                        title="Handmatig een document toevoegen"
                                        onclick="(function ( ) {
                                      -  
                                            $('.tab-documents > a').click();
                                        
                                            if(!!window.File) {
                                                $('.document-list-upload input').eq(0).click();
                                            } else {
                                                angular.element($('.document-list-upload').eq(1)[0]).triggerHandler('mouseover');
                                            }
                                            
                                        })();">Document</a>
                                    <a
                                        title="Sjabloon gebruiken"
                                        onclick="$('.document-list-create-from-template > button').eq(0).click();$('.tab-documents > a').click();"
                                        >Sjabloon</a>
                                    <a
                                        title="Contactmoment"
                                        class="ezra_nieuwe_zaak_tooltip-show"
                                        href="[%
                                            c.uri_for(
                                                '/contactmoment/create',
                                                {
                                                    zaak_id         => zaak.id,
                                                    betrokkene_type => zaak.aanvrager_object.btype,
                                                    betrokkene_naam => encode_utf8(zaak.aanvrager_object.naam),
                                                    betrokkene_id   => zaak.aanvrager_object.betrokkene_identifier
                                                }
                                            ) %]">Contactmoment</a>
                                    <a
                                        class="ezra_dialog"
                                        data-ng-click="openPopup()"
                                        data-zs-pip-authorized-add 
                                        data-zs-popup="'/html/case/relations/relations.html#authorization-add-dialog'"
                                        >Betrokkene</a>
                                    
                                    
                                    <a title="E-mail versturen" class="ezra_dialog" href="[% c.uri_for('/zaak/' _ zaak.id _ '/send_email') %]" rel="ezra_dialog_layout: xlarge">E-mail</a>
                                    <a class="ezra_dialog" data-ng-click="openPopup()" data-zs-planned-case-add data-zs-popup="'/html/case/view/planned-case-dialog.html'">
                                        Zaak inplannen
                                    </a>
                                    [% END %]
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    [% END %]
                    [% IF c.check_any_zaak_permission('zaak_edit', 'zaak_beheer') %]
                    <div class="button-group button-group-relative">
                        <a
                        title="Acties"
                        class="button button-secondary button-large dropdown-toggle"
                        href="#actie">
                            <i class="icon-font-awesome icon-cog icon"></i>
                        </a>
                        <div class="ezra_actie_button_handling_popover">
                        

                            
                            <div class="popover" id="popover_actie">
                                
                                <div class="buttons">
                                [% IF c.check_any_zaak_permission('zaak_edit', 'zaak_beheer') %]
                                    [% IF zaak.status != 'resolved' %]
                                        <a class="ezra_dialog" rel="ezra_dialog_layout: large" href="[% c.uri_for('/zaak/' _ zaak.nr _ '/update/allocation') %]?context=case" title="Toewijzing wijzigen">Toewijzing wijzigen</a>
                                        <a class="ezra_dialog" rel="ezra_dialog_layout: large" href="[% c.uri_for('/zaak/' _ zaak.nr _ '/update/' _ (zaak.status == 'stalled' ? 'resume' : 'opschorten' )) %]" title="[% IF zaak.status == 'stalled' %]Hervatten[% ELSE %]Opschorten[% END %]">[% IF zaak.status == 'stalled' %]Hervatten[% ELSE %]Opschorten[% END %]</a>
                                        <a class="ezra_dialog" title="Vroegtijdig afhandelen" data-ng-click="openPopup()" data-zs-scope data-zs-popup="'/html/case/view/resolve-case-early-dialog.html'">Vroegtijdig afhandelen</a>
                                        <a class="ezra_dialog" rel="ezra_dialog_layout: large" href="[% c.uri_for('/zaak/' _ zaak.nr _ '/update/verlengen') %]" title="Termijn wijzigen">Termijn wijzigen</a>
                                        <a href="[% c.uri_for('/zaak/' _ zaak.nr _ '/update/relatie') %]" class="ezra_dialog" rel="ezra_dialog: mediumlarge" title="Zaak relateren">Zaak relateren</a>
                                    [% END %]
                                        <a href="[% c.uri_for('/zaak/duplicate/' _ zaak.nr) %]" class="ezra_dialog" title="Zaak kopi&euml;ren" rel="ezra_dialog_layout: small">Zaak kopi&euml;ren</a>
                                        <a href="[% c.uri_for('/zaak/' _ zaak.nr _ '/scheduled_jobs') %]" class="ezra_dialog" title="Herplannen">Herplannen</a>
                                    [% IF c.check_any_user_permission('admin','beheerder') || c.check_any_zaak_permission('zaak_beheer') %]
                                        <a class="ezra_dialog" rel="ezra_dialog_layout: xlarge" href="[% c.uri_for('/zaak/' _ zaak.nr _ '/update/set_settings') %]" title="Beheeracties">Beheeracties</a>
                                    [% END %]
                                [% END %]
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                    [% END %]
                    [% UNLESS !c.can_change || zaak.is_afgehandeld %]
                    <div
                        class="button-group"
                        data-zs-title="<[caseView.canAdvance() ? '' : 'Niet alle velden zijn correct ingevuld.' ]>"
                    >

                        <button
                            title="Volgende fase"
                            class="button button-secondary button-large ezra_validate_next_phase"
                            href="[% c.uri_for('/zaak/' _ zaak.nr _ '/status/advance') %]"
                            rel="zaak_id: [% zaak.nr %]; layout: medium"
                            data-ng-disabled="!caseView.canAdvance()"
                            data-ng-class="{'ezra_validate_next_phase-disabled': !caseView.canAdvance() } "
                        >
                            <i class="icon-font-awesome icon-chevron-right icon"></i>
                        </button>
                    </div>
                    [% END %]
                    
                    </div>

                    [% UNLESS 1 || !c.can_change || zaak.is_afgehandeld %]
                        <a  
                            title="Volgende fase"
                            href="[% c.uri_for('/zaak/' _ zaak.nr _ '/status/advance') %]" 
                            class="ezra_validate_next_phase"
                            rel="zaak_id: [% zaak.nr %]; layout: mediumsmall"
                        >Advance</a>
                    [% END %]
                    
                </div>
                
                [% END # END NUM_BUTTONS %]
                     
            </div>
            
            <!-- end zaak-acties -->
                
            [% END %]
   
        </div>
        
         <!-- end zaak-info -->
        
           


    </div>
    
    <!-- end zaakdossier-titel -->

        <div class="tabs-header" data-ng-controller="nl.mintlab.case.CaseTabListController">
            <ul class="ui-rounded-corners-off ui-tabs-nav tabs-header">
                
                [% fasen = zaak.scalar.fasen %]
                
                [% WHILE (fase = fasen.next) %]
                    [% fase_classes = [] %]
                    [% fase_classes.push('ui-tabs-selected ui-state-active')
                         IF !c.req.params.tab && requested_fase.status == fase.status %]
                    [% fase_classes.push('done')
                        IF fase.status <= zaak.milestone %]
                    [% fase_classes.push('huidig')
                        IF fase.status == (zaak.milestone + 1) %]
                    [% fase_classes.push('afgehandeld')
                        IF zaak.is_afgehandeld && fase.status == (zaak.milestone + 1)%]
                    <li class="[% fase_classes.join(' ') %] fase" data-ng-controller="nl.mintlab.case.CaseTabController" data-ng-init="phaseId='[% fase.id %]';hasQueueRights='[% c.check_queue_coworker_changes ? 'false' : 'true' %]'" data-ng-class="{'fase-has-notifications': getNotificationCount(phaseId) > 0 }">
                        <a href="#zaak-elements-fase-[% fase.status %]" class="knop-text "  title="Klik om de fase '[% fase.fase %]' te bekijken">
                            [% IF fase.status <= zaak.milestone %]<i class="mdi mdi-checkbox-marked-circle"></i>[% ELSIF fase.status == (zaak.milestone + 1) %]<i class="mdi mdi-play-circle"></i>[% END %]
                            <span class="fase-label">[% fase.fase %]</span>
                            <span class="fase-counter counter" data-ng-show="hasQueueRights"><[getNotificationCount(phaseId)]></span>
                            [% PROCESS zaak/attribute_update_requests.tt %]
                        </a>
                    </li>
                [% END %]
                
                <li class="tab-documents tab-icon [% IF c.req.params.tab == 'docs' %]ui-tabs-selected ui-state-active[% END %]" data-ng-controller="nl.mintlab.case.CaseTabController" data-ng-init="phaseId='documents'" data-ng-class="{'fase-has-notifications': getNotificationCount(phaseId) > 0 }">
                    <a href="#zaak-elements-documents" class="ui-state-default ui-corner-top icons-zaakdossier" title="Documenten">
                        <span class="fase-label">Documenten</span>
                        <span class="document-counter counter"><[getNotificationCount(phaseId)]></span>
                    </a>
                </li>

                <li class="tab-timeline tab-icon [% IF c.req.params.tab == 'timeline' %]ui-tabs-selected ui-state-active[% END %]" data-ng-controller="nl.mintlab.case.CaseTabController" data-ng-init="phaseId='timeline'" data-ng-class="{'fase-has-notifications': getNotificationCount(phaseId) > 0 }">
                    <a href="#zaak-elements-timeline" class="ui-state-default ui-corner-top icons-zaakdossier" title="Timeline">
                        <span class="fase-label">Timeline</span>
                    </a>
                </li>

                <li class="ui-last tab-subcases tab-icon [% IF c.req.params.tab == 'deelzaken' %]ui-tabs-selected ui-state-active[% END %]" data-ng-controller="nl.mintlab.case.CaseTabController" data-ng-init="phaseId='deelzaken'" data-ng-class="{'fase-has-notifications': getNotificationCount(phaseId) > 0 }">
                    <a href="#zaak-elements-deelzaken" class="ui-state-default ui-corner-top icons-zaakdossier" title="Zaakrelaties">
                        <span class="fase-label">Deelzaken</span>
                    </a>
                </li>
                [% IF zaak.locatie_zaak %]
                    <li class="ui-last tab-subcases tab-icon [% IF c.req.params.tab == 'zaakadres' %]ui-tabs-selected ui-state-active[% END %]" data-ng-controller="nl.mintlab.case.CaseTabController" data-ng-init="phaseId='zaakadres'" data-ng-class="{'fase-has-notifications': getNotificationCount(phaseId) > 0 }">
                        <a 
                            href="/zaak/[% zaak.id %]/info/geolocatie" 
                            class="ui-state-default ui-corner-top icons-zaakadres map-loader-button" 
                            id="map-loader-button" 
                            title="Zaakadres"
                            >
                            <i class="icon-font-awesome icon-map-marker"></i>
                        </a>
                    </li>
                [% END %]
            </ul>
            <div class="tabs-header-saved" data-ng-show="!!caseView.getCase()" data-ng-class="{'tabs-header-saved-unsaved': caseView.isSaving() }">
                <span class="tabs-header-saved-label" data-ng-show="!caseView.isSaving()&&!caseView.hasUnsavedChanges()">Alle wijzigingen zijn opgeslagen</span>
                <span class="tabs-header-saved-label" data-ng-show="caseView.isSaving()">Bezig met opslaan&hellip;</span>
                <span class="tabs-header-saved-label" data-ng-show="!caseView.isSaving()&&caseView.hasUnsavedChanges()">Wachten met opslaan&hellip;.</span>
            </div>
        </div>
        
        [% fasen = fasen.reset # need to iterate again %]
        [% WHILE (current_phase = fasen.next) %]           
        <div class="ui-tabs-hide taken tab-inner-scroll taken-inner-scroll no-padding ezra_load_zaak_element zaak_nr_[% zaak.nr %]" id="zaak-elements-fase-[% current_phase.status %]" data-ng-controller="nl.mintlab.case.CasePhaseController" data-ng-init="phaseId='[% current_phase.id %]'"  data-zs-case-phase-view data-zs-case-phase-id="[% current_phase.id %]" data-zs-case-milestone="[% current_phase.status %]" data-zs-case-phase-name="[% current_phase.fase %]">
            <div class="has-icon tab-info">
                <i class="icon-tab-info icon icon-lebensraum icons-zaakdossier"></i> Je bekijkt de fase "[% current_phase.fase %]".                     
                [% IF current_phase.id == requested_fase.id && !zaak.is_afgehandeld %] Dit is momenteel de actieve fase van deze zaak.[% END %]
                [% IF current_phase.status <= zaak.milestone %] Deze fase is afgerond.[% END %]
            </div>

            [% PROCESS zaak/tasks.tt %]
        </div>

        [% END %]        
    
        <div class="ui-tabs-hide documents" id="zaak-elements-documents"  data-ng-controller="nl.mintlab.case.CasePhaseController" data-ng-init="phaseId='documents'">
            <div class="has-icon tab-info">
                <i class="icon-tab-info icon icon-lebensraum icons-zaakdossier"></i> Je bekijkt de documenten van deze zaak.
            </div>

            <div>
                [% PROCESS zaak/elements/documents.tt %]
            </div>
        </div>

        <div class="ui-tabs-hide zaak_nr_[% zaak.nr %]" id="zaak-elements-timeline"  data-ng-controller="nl.mintlab.case.CasePhaseController" data-ng-init="phaseId='timeline'">
            <div class="has-icon tab-info">
                <i class="icon-tab-info icon icon-lebensraum icons-zaakdossier"></i> Je bekijkt de timeline. Hier staan alle gebeurtenissen die gerelateerd zijn aan deze zaak.
            </div>
            
            <div data-ng-if="elementRequested">
                [% PROCESS zaak/elements/timeline.tt %]
            </div>
        </div>    
    
        <div class="ui-tabs-hide zaak_nr_[% zaak.nr %]" id="zaak-elements-deelzaken" data-ng-controller="nl.mintlab.case.CasePhaseController" data-ng-init="phaseId='deelzaken'">
            <div class="has-icon tab-info">
                <i class="icon-tab-info icon icon-lebensraum icons-zaakdossier"></i> Je bekijkt de deelzaken en/of gerelateerde zaken.
            </div>

            <div class="deelzaken deelzaken_inner_html_content"></div>
            <div class="spinner-groot"><div></div></div>
        </div>
    </div>
</div>

<script type="text/ng-template" id="/partials/case/attribute/docs/pdf-reader-popup.html">
    <div data-zs-modal class="flexpaper-modal" data-ng-init="title=modalTitle">
        <a class="button button-secondary button-xsmall flexpaper-download-button" href="<['/zaak/' + caseId + '/document/' + fileId + '/download/pdf']>">download</a>
        <iframe style="padding:0px;margin:0px;border:0px" data-ng-src="<[flexpaperUrl]>"></iframe>
    </div>
</script>

<script type="text/ng-template" id="/html/case/view/planned-case-dialog.html">
    <div data-zs-modal data-zs-modal-title="Voeg geplande zaak toe">
        <div data-zs-planned-case-edit data-is-create="true" data-submit="plannedCaseAdd.handleEditSubmit($values)" class="planned-case-dialog">
        </div>
    </div>
</script>

<script type="text/ng-template" id="/html/case/view/resolve-case-early-dialog.html">
    <div data-zs-modal data-zs-modal-title="Vroegtijdig afhandelen">
        [% PROCESS widgets/zaak/zaak_result.tt 
            RESOLVE_EARLY = 1
        %]
    </div>
</script>

<!-- END VIEW: zaak/view -->
